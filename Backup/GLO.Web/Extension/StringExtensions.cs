﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GLO.Data.Models;

namespace System.Web.Mvc
{
    public static class StringExtensions
    {
        public static string CTrim(this string s, int maxLength)
        {
            return s.CTrim(maxLength, true);
        }
        public static string CTrim(this string s, int maxLength, bool addEllipses)
        {
            s = s.ClearHTML();
            if (s.Length > maxLength)
            {
                s = s.Substring(0, s.Substring(0, maxLength).LastIndexOfAny(new char[] { ' ', '.', ',', '-', '/', ')', '(' }));

                if (addEllipses)
                {
                    if (s.Length > maxLength - 3)
                    {
                        s = s.Substring(0, maxLength - 3);
                    }
                    s = string.Format("{0}...", s);
                }
            }

            return s;
        }
        public static string ProfileUrl(this GLO_Users user)
        {
            string url = "<a href=\"{0}\" target=\"_blank\">{1}</a>";
            if (user.TypeID == 1)
                url = string.Format(url, string.Format("/Leader/Index?leaderid={0}",user.UserID), user.NickName);
            else if(user.TypeID==2)
                url = string.Format(url, string.Format("/Expert/MyValue?expertid={0}", user.UserID), user.NickName);
            else if(user.TypeID ==3)
                url = string.Format(url, string.Format("/Company/CompanyIndex?companyid={0}", user.UserID), user.NickName);
            
            return url;

        }
        public static string ToUrl(this string url)
        {
            if(url.StartsWith("www"))
                return "http://"+url;
            return url;
        }
    }
}