﻿$(document).ready(function () {
    // display default text for text fields
    $(".tool-field-text-default").focus(function (srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("tool-field-text-default-active");
            $(this).val("");
        }
    });

    $(".tool-field-text-default").blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("tool-field-text-default-active");
            $(this).val($(this)[0].title);
        }
    });

    $(".tool-field-text-default").blur();

});

function IsURL(str_url) {
    var RegUrl = new RegExp();
    RegUrl.compile("(http://|https://)*([A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$)");
    if (RegUrl.test(str_url)) {
        return (true);
    } else {
        return (false);
    }
}
function CheckUrl(str) {
    var RegUrl = new RegExp();
    RegUrl.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$"); //jihua.cnblogs.com
    if (!RegUrl.test(str)) {
        return false;
    }
    return true;
}


function CloseLightBox() {
    var div = document.getElementById('lightboxwrapper');
    if (div) div.style.display = 'none';
      return false;
}   


function GetQuerystring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

$.postJSONnoQS = function (url, data, callback, async, errorcallback) {
    /// <summary>Post JSON data to specified address</summary>
    /// <param name="url" type="String">URL to post to</param>
    /// <param name="data">JSON data to post</param>
    /// <param name="callback" type="Method">Callback to execute on response</param>

    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data == null ? '' : data,
        cache: false,
        async: async == null ? true : false,
        success: callback,
        error: errorcallback,
        dataType: 'json',
        beforeSend: function (xml) { xml.setRequestHeader("X-IsJson", "true"); clearHeaders(xml); }
    });
};

$.postJSON = function (url, data, callback, async, errorcallback, addqs) {
    /// <summary>Post JSON data to specified address</summary>
    /// <param name="url" type="String">URL to post to</param>
    /// <param name="data">JSON data to post</param>
    /// <param name="callback" type="Method">Callback to execute on response</param>

    // add data to URL, as sometimes data is lost in POST
    if (addqs || addqs == null) {
        var qs = data == null ? '' : $.param(data);
        url = url.indexOf("?") >= 0 ? jQuery.format("{0}&{1}", url, qs) : jQuery.format("{0}?{1}", url, qs);
    }
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data == null ? '' : (addqs || addqs == null) ? '' : data,
        cache: false,
        async: async == null ? true : false,
        success: callback,
        error: errorcallback,
        dataType: 'json',
        beforeSend: function (xml) { xml.setRequestHeader("X-IsJson", "true"); clearHeaders(xml); }
    });
};

$.postJSON2 = function (url, data, callback, returnType, addqs) {
    /// <summary>Post JSON data to specified address with application/json as content type</summary>
    /// <param name="url" type="String">URL to post to</param>
    /// <param name="data">JSON data to post</param>
    /// <param name="callback" type="Method">Callback to execute on response</param>

    // add data to URL, as sometimes data is lost in POST
    if (addqs || addqs == null) {
        var qs = data == null ? '' : $.param(data);
        url = url.indexOf("?") >= 0 ? jQuery.format("{0}&{1}", url, qs) : jQuery.format("{0}?{1}", url, qs);
    }
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data == null ? '' : (addqs || addqs == null) ? '' : data,
        cache: false,
        success: callback,
        dataType: returnType == null ? 'json' : returnType,
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (xml) { xml.setRequestHeader("X-IsJson", "true"); clearHeaders(xml); }
    });
};

$.postAjaxnoQS = function (url, data, dataType, async, callback, errorcallback) {
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data == null ? '' : data,
        cache: false,
        async: async == null ? true : false,
        success: callback,
        error: errorcallback,
        dataType: dataType,
        beforeSend: function (xml) { clearHeaders(xml); }
    });
};

$.postAjax = function (url, data, dataType, async, callback, errorcallback, addqs) {
    /// <summary>Post to specified URL, will not send the X-IsJson header, Action should return text or javascript</summary>
    /// <param name="url" type="String">URL to post to</param>
    ///<param name="data">JSON object to post</param>
    ///<Param name="callback" type="Method">Callback to be executed</param>

    // add data to URL, as sometimes data is lost in POST
    if (addqs || addqs == null) {
        var qs = data == null ? '' : $.param(data);
        url = url.indexOf("?") >= 0 ? jQuery.format("{0}&{1}", url, qs) : jQuery.format("{0}?{1}", url, qs);
    }

    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data == null ? '' : (addqs || addqs == null) ? '' : data,
        cache: false,
        async: async == null ? true : false,
        success: callback,
        error: errorcallback,
        dataType: dataType,
        beforeSend: function (xml) { clearHeaders(xml); }
    });
};

$.jsonForm = function (formid, callback) {
    var form = $('#' + formid);
    var postData = $(':input', form).serializeArray();
    var url = form.attr('action');
    $.postJSON(url, postData, callback);
}

jQuery.fn.delay = function (time, func) {
    return this.each(function () {
        setTimeout(func, time);
    });
};

jQuery.format = function (source, params) {
    if (arguments.length == 1)
        return function () {
            var args = jQuery.makeArray(arguments);
            args.unshift(source);
            return jQuery.format.apply(this, args);
        };
    if (arguments.length > 2 && params.constructor != Array) {
        params = jQuery.makeArray(arguments).slice(1);
    }
    if (params.constructor != Array) {
        params = [params];
    }
    jQuery.each(params, function (i, n) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
    });
    return source;
};


function setCookie(name, value) {
    document.cookie = name + "=" + escape(value) + "; path=/";
}

function getCookie(name) {
    var cookieValue = "";
    if (document.cookie.length > 0) {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = $.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = unescape(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function replaceQueryString(a, k, v) {
    var re = new RegExp("([?|&])" + k + "=.*?(&|$)", "i");
    if (a.match(re))
        return a.replace(re, '$1' + k + "=" + v + '$2');
    else {
        if (v != '') {
            if (a.indexOf('?') > 0)
                return a + '&' + k + "=" + v;
            else
                return a + '?' + k + "=" + v;
        } else
            return a;
    }
}

clearHeaders = function (xhr) { //get rid of unnecessary headers to make request a little lighter
    function safeSet(k, v) {
        try {
            xhr.setRequestHeader(k, v);
        } catch (e) { }
    }
    safeSet("User-Agent", null);
    safeSet("Accept", null);
    safeSet("Accept-Language", null);
    safeSet("Connection", "keep-alive");
    safeSet("Keep-Alive", null);
}

function randomString() {
    var chars = "0123456789abcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

function openWindow(mypage, myname, winprops) {
    if (myname == "") {
        myname = randomString();
    }
    win = window.open(mypage, myname, winprops)
    if (win) {
        if (parseInt(navigator.appVersion) >= 4) {
            win.focus();
        }
    } else {
        alert("popup blockers turned on");
    }
}

function NewWindow(mypage, myname, w, h, wint, winl, scroll) {
    winprops = 'height=' + h + ',width=' + w + ',top=' + wint + ',left=' + winl + ',scrollbars=' + scroll + ',toolbar=yes,status=yes,resizable'
    openWindow(mypage, myname, winprops);
}

function NewWindowNR(mypage, myname, w, h, wint, winl, scroll) {
    winprops = 'height=' + h + ',width=' + w + ',top=' + wint + ',left=' + winl + ',scrollbars=' + scroll + ',toolbar=no'
    openWindow(mypage, myname, winprops);
    return false;
}

function NewWindowNRPosition(mypage, myname, w, h, xOffset, yOffset, xScreen, yScreen, scroll, toolbar) {
    var screenH = screen.height;
    var screenW = screen.width;

    var xPos = xScreen + xOffset;
    var yPos = yScreen + yOffset;

    var xtemp = xPos + w + 30;
    var ytemp = yPos + h + 65;

    if (xtemp >= screenW) {
        xPos = screenW - w - 30;
    }
    if (ytemp >= screenH) {
        yPos = screenH - h - 65;
    }

    var winprops = 'height=' + h + ',width=' + w + ',left=' + xPos + ',top=' + yPos + ',scrollbars=' + scroll + ',toolbar=' + toolbar;
    openWindow(mypage, myname, winprops);
    return false;
}

function updateParent(page, w, h) {
    window.resizeTo(w, h)
}

$('.copytoclipboard').live('click', function () {
    var text = $(this).attr("text");
    if (window.clipboardData) {
        window.clipboardData.setData('text', text);
    }
    else {
        var clipboarddiv = document.getElementById('divclipboardswf');
        if (clipboarddiv == null) {
            clipboarddiv = document.createElement('div');
            clipboarddiv.setAttribute("name", "divclipboardswf");
            clipboarddiv.setAttribute("id", "divclipboardswf");
            document.body.appendChild(clipboarddiv);
        }
        clipboarddiv.innerHTML = '<embed src="clipboard.swf" FlashVars="clipboard=' +
        encodeURIComponent(text) + '" width="0" height="0" type="application/x-shockwave-flash"></embed>';
    }
    alert('The text is copied to your clipboard...');
    return false;
});

$('.toggle-container').live('click', function () {
    var container = $(this).siblings();
    container.toggle();

    if (container.is(":visible"))
        $(this).find(".tool-open-container:eq(0)").removeClass("tool-open-container").addClass("tool-close-container");
    else
        $(this).find(".tool-close-container:eq(0)").removeClass("tool-close-container").addClass("tool-open-container");
});

Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};


function isEmpty(s) {
    var whiteSpaceRegex = /^\s*$/;
    return (s == null || whiteSpaceRegex.test(s));
}

function isPhone(s) {
    var nums = "0123456789";
    var valid = false;
    var foundInvalid = false;
    var temp = stripExtraneousChars(s, false, false);

    for (var i = 0; i < temp.length; i++) {
        if (nums.indexOf(temp.charAt(i)) < 0) {
            foundInvalid = true;
            break;
        }
    }
    if (temp.length == 10 && !foundInvalid)
        valid = true;

    return valid;
}

function isEmail(s) {
    var emailRegex = /^([\w-]+)(\.[\w-]+)*@([a-z0-9]+)([\.-][a-z0-9]+)*(\.[a-z]{2,})$/i; // i flag means to ignore case.
    return emailRegex.test($.trim(s));
}


function alertbox(value, func1) {
    var close = function () {
        $(".layerwrap").remove();
        $(".layermask").remove();
        return false;
    };

    var mask = document.createElement("div");
    mask.className = 'layermask';

    var wrapper = document.createElement("div");
    wrapper.className = 'layerwrap';

    var header = document.createElement("div");
    header.className = 'layerhead';

    var span = document.createElement("span");
    span.innerHTML = 'Note';

    header.appendChild(span);

    var mainer = document.createElement("div");
    mainer.className = 'layermain';
    mainer.innerHTML = value;

    var footer = document.createElement("div");
    footer.className = 'layerfoot';

    var button = document.createElement("input");
    button.id = 'AlertBtnOK';
    button.className = 'bluebtn3';
    button.type = 'button';
    button.value = 'OK';

    if (func1) {
        button.onclick = func1;
        $('#AlertBtnOK').live('click', close);
    }
    else {
        button.onclick = close;
    }

    footer.appendChild(button);

    wrapper.appendChild(header);
    wrapper.appendChild(mainer);
    wrapper.appendChild(footer);
    document.body.appendChild(wrapper);
    document.body.appendChild(mask);
    return false;
}


function confirmbox(value, func1, func2) {
    var close = function () {
        $(".layerwrap").remove();
        $(".layermask").remove();
        return false;
    };

    var mask = document.createElement("div");
    mask.className = 'layermask';

    var wrapper = document.createElement("div");
    wrapper.className = 'layerwrap';

    var header = document.createElement("div");
    header.className = 'layerhead';

    var span = document.createElement("span");
    span.innerHTML = 'Note';

    header.appendChild(span);

    var mainer = document.createElement("div");
    mainer.className = 'layermain';
    mainer.innerHTML = value;

    var footer = document.createElement("div");
    footer.className = 'layerfoot';

    var button1 = document.createElement("input");
    button1.id = 'ConfirmBtnOK';
    button1.className = 'bluebtn3';
    button1.type = 'button';
    button1.value = 'OK';

    if (func1) {
        button1.onclick = func1;
        $('#ConfirmBtnOK').live('click', close);
    }
    else {
        button1.onclick = close;
    }

    var button2 = document.createElement("input");
    button2.id = 'ConfirmBtnCalcel';
    button2.className = 'bluebtn3';
    button2.type = 'button';
    button2.value = 'CANCEL';

    if (func2) {
        button2.onclick = func2;
        $('#ConfirmBtnCalcel').live('click', close);
    }
    else {
        button2.onclick = close;
    }

    footer.appendChild(button1);
    footer.appendChild(button2);

    wrapper.appendChild(header);
    wrapper.appendChild(mainer);
    wrapper.appendChild(footer);
    document.body.appendChild(wrapper);
    document.body.appendChild(mask);
    return false;
}

function confirmbox2(value, text1, text2, func1, func2) {
    var close = function () {
        $(".layerwrap").remove();
        $(".layermask").remove();
        return false;
    };

    var mask = document.createElement("div");
    mask.className = 'layermask';

    var wrapper = document.createElement("div");
    wrapper.className = 'layerwrap';

    var header = document.createElement("div");
    header.className = 'layerhead';

    var span = document.createElement("span");
    span.innerHTML = 'Note';

    header.appendChild(span);

    var mainer = document.createElement("div");
    mainer.className = 'layermain';
    mainer.innerHTML = value;

    var footer = document.createElement("div");
    footer.className = 'layerfoot';

    var button1 = document.createElement("input");
    button1.id = 'ConfirmBtnOK';
    button1.className = 'bluebtn3';
    button1.type = 'button';
    button1.value = text1;

    if (func1) {
        button1.onclick = func1;
        $('#ConfirmBtnOK').live('click', close);
    }
    else {
        button1.onclick = close;
    }

    var button2 = document.createElement("input");
    button2.id = 'ConfirmBtnCalcel';
    button2.className = 'bluebtn3';
    button2.type = 'button';
    button2.value = text2;

    if (func2) {
        button2.onclick = func2;
        $('#ConfirmBtnCalcel').live('click', close);
    }
    else {
        button2.onclick = close;
    }

    footer.appendChild(button1);
    footer.appendChild(button2);

    wrapper.appendChild(header);
    wrapper.appendChild(mainer);
    wrapper.appendChild(footer);
    document.body.appendChild(wrapper);
    document.body.appendChild(mask);
    return false;
}

function HpylinkAction(obj, message) {
    var url = $(obj).attr('href');

    confirmbox(message, function () {
        window.location = url
    });

    return false;
}