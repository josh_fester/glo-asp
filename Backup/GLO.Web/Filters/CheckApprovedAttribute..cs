﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using GLO.Services;
using StructureMap;

namespace GLO.Web
{
    public class CheckApprovedAttribute : ActionFilterAttribute
    {
        public IAccountService _accountservice{get;set;}

        public CheckApprovedAttribute()
        {
              _accountservice = ObjectFactory.GetInstance<AccountService>();
        }
 
        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (filterContext.Result is ViewResult)
            {
                var loginuser = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"] != null)
                {   
                    var expertid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
                    if (loginuser != null && loginuser.UserData.UserID == expertid)
                    {
                        if(loginuser.UserData.Approved==false)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notapporved");
                        else if(loginuser.UserData.IsPayMent!=1)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notpayment");
                    }
                    else
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=noright");
                                        
                }
                else if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"] != null)
                {
                    var companyid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"]);
                    if (loginuser != null && loginuser.UserData.UserID == companyid)
                    {
                        if (loginuser.UserData.Approved == false)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notapporved");
                        else if (loginuser.UserData.IsPayMent != 1)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notpayment");                         
                    }
                    else
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=noright");
                       
                }
                else if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"] != null)
                {
                    var leaderid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"]);
                    if (loginuser != null && loginuser.UserData.UserID == leaderid)
                    {
                        if (loginuser.UserData.Approved == false)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notapporved");
                    }

                }
            }
        }

        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            var loginuser = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
            if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"] != null)
            {
                var leaderid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"]);
                if (loginuser != null && loginuser.UserData.UserID == leaderid)
                {
                    if (loginuser.UserData.Approved == false)
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notapporved");
                }
                //else if (loginuser == null || loginuser.UserData.UserID != leaderid)
                //    filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=noright");
            }
        }

        public override void OnResultExecuted(System.Web.Mvc.ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);           
        }

        public override void OnResultExecuting(System.Web.Mvc.ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }     
    }
}