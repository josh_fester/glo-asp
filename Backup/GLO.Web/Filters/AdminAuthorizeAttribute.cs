﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using System.Web.Security;
using System.Net;
using GLO.Services;
using StructureMap;

namespace GLO.Web
{
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        protected IAccountService _accountservice;
        public string returnUrl { get; set; }
        public AdminAuthorizeAttribute()
        {
            _accountservice = ObjectFactory.GetInstance<IAccountService>(); 
        }
        /// <summary>
        /// rewrite
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            //if (filterContext.HttpContext.Request.IsAuthenticated)
            //    filterContext.HttpContext.Response.Redirect(string.Format("/home/index?returnUrl={0}", filterContext.HttpContext.Request.Url.PathAndQuery));
            //throw new Exception(string.Format("{0},sorry you don't have the right to access the page", string.Empty));

            //ger request controller and action
            //var controller = filterContext.RouteData.Values["controller"].ToString();
            //var action = filterContext.RouteData.Values["action"].ToString();
        }
        /// <summary>
        /// rewrite core method,if user is login,check the user from database
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpcontext)
        {
            bool contextresult = false;
            if (httpcontext.User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = _accountservice.GetUserByUserName(httpcontext.User.Identity.Name);
                    if (user == null || user.TypeID != 4)
                    {
                        FormsAuthentication.SignOut();
                    }
                    else
                    {
                        contextresult = true;
                    }
                }
                catch
                {
                    FormsAuthentication.SignOut();
                }
            }

            if (!contextresult)
            {
                httpcontext.Response.StatusCode = 403;
            }
            return contextresult;
        }
        /// <summary>
        /// if autorize fail, then redirect to login page
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            else
            {
                string path = filterContext.HttpContext.Request.Path;
                string strUrl = "/Account/Login";
                filterContext.HttpContext.Response.Write("<script>window.location.href='"+strUrl+"';</script>");

            }
        }
    }
}