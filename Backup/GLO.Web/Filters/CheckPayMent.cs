﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using GLO.Services;
using StructureMap;

namespace GLO.Web
{
    public class CheckPayMent : ActionFilterAttribute
    {
        public IAccountService _accountservice{get;set;}
        public CheckPayMent()
        {
            _accountservice = ObjectFactory.GetInstance<IAccountService>();
        }
        public bool IsCheck { get; set; }

        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request) != null)
            {
                var currentuser = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                var expertid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
                if (currentuser != null && IsCheck && currentuser.UserData.UserID == expertid)
                {
                    if (currentuser.UserData.Approved == false)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved");
                    }
                    else if(currentuser.UserData.IsPayMent!=1)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notpayment");
                    }
                   
                }
            }
        }
        public override void OnResultExecuted(System.Web.Mvc.ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);           
        }
        public override void OnResultExecuting(System.Web.Mvc.ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);          
        }
    }

}