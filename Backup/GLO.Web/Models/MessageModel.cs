﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using GLO.Data.Models;
namespace GLO.Web.Models
{
    public class MessageModel
    {
        public int MessageID { get; set; }

        public string MessageTitle { get; set; }

        public string MessageContent { get; set; }

        public int MessageTypeID { get; set; }

        public int SendUserID { get; set; }

        public string SendUserIcon { get; set; }

        public string SendUserNikeName { get; set; }

        public string SendUserUrl { get; set; }

        public int ReceiveID { get; set; }

        public string ReceiveUserID { get; set; }

        public string ReceiveUserNikeName { get; set; }

        public string ReceiveUrl { get; set; }

        public int AdminSendType { get; set; }

        public int MessageStatus { get; set; }

        public DateTime SendDate { get; set; }

        public List<GLO_Users> ReceiveUserList { get; set; }
    }
}