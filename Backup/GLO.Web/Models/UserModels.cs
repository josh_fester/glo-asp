﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using GLO.Data.Models;
using GLO.Web.Attributes;

namespace GLO.Web.Models
{
    public class RegisterModel
    {
        [Display(Name = "user name")]
        public string UserName { get; set; }


        public string NickName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "first name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression("^(?!First name$).+$", ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [Display(Name = "last name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression("^(?!Last name$).+$", ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The location is required.")]
        [Display(Name = "Location")]
        public string Location { get; set; }

        [Required]
        public int LocationID { get; set; }

        [Required(ErrorMessage = "The email is required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [RegularExpression(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email format is not correct.")]
        //[CheckEmail(ErrorMessage = "The email  has already been used.")]
        [Remote("IsExistEmail", "Account", ErrorMessage = "The email  has already been used.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The password is required.")]
        [StringLength(12, ErrorMessage = "The password must be within {2}-{1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The confirm password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string PhoneNumber3 { get; set; }

        [Required(ErrorMessage = "The check code is required.")]
        [Display(Name = "check code")]
        public string CheckCode { get; set; }

        [Required]
        [HiddenInput]
        public int UserType { get; set; }

        [Required]
        [Range(0, 1)]
        public int Sex { get; set; }

        [Required(ErrorMessage = "The industry is required.")]
        [Display(Name = "industry")]
        public int IndustryID { get; set; }

        public string Industry { get; set; }


        [Required]
        [Range(1, 1)]
        public int Policy { get; set; }
    }

    public class RegisterLeaderModel
    {
        [Display(Name = "user name")]
        public string UserName { get; set; }

        public string NickName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "first name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression("^(?!First name$).+$", ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [Display(Name = "last name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression("^(?!Last name$).+$", ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The location is required.")]
        [Display(Name = "Location")]
        public string Location { get; set; }

        [Required]
        public int LocationID { get; set; }

        [Required(ErrorMessage = "The email is required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [RegularExpression(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email format is not correct.")]
        //[CheckEmail(ErrorMessage = "The email  has already been used.")]
        [Remote("IsExistEmail", "Account", ErrorMessage = "The email  has already been used.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The password is required.")]
        [StringLength(12, ErrorMessage = "The password must be within {2}-{1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The confirm password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string PhoneNumber3 { get; set; }

        [Required(ErrorMessage = "The check code is required.")]
        [Display(Name = "check code")]
        public string CheckCode { get; set; }

        [Display(Name = "job title")]
        public string Title { get; set; }

        //[Required(ErrorMessage = "The industry is required.")]
        [Display(Name = "industry")]
        public string Industry { get; set; }

        public int IndustryID { get; set; }

        [Required]
        [HiddenInput]
        public int UserType { get; set; }

        [Required]
        [Range(1,1)]
        public int Identify { get; set; }

        [Required]
        [Range(1, 1)]
        public int Policy { get; set; }

        [Required]
        [Range(0, 1)]
        public int Sex { get; set; }

        public int ReceiveEmail { get; set; }
    }

    public class RegisterCompanyModel
    {
        public string UserName { get; set; }

        public string NickName { get; set; }

        [Required(ErrorMessage = "The location is required.")]
        [Display(Name = "Location")]
        public string Location { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [RegularExpression(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email format is not correct.")]
        [Remote("IsExistEmail", "Account", ErrorMessage = "The email  has already been used.")]
        public string Email { get; set; }

        [Required]
        [StringLength(12, ErrorMessage = "The password must be within {2}-{1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string CompanyName { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "The industry is required.")]
        [Display(Name = "industry")]
        public string Industry { get; set; }

        public int IndustryID { get; set; }

        [Required]
        public string PhoneNumber1 { get; set; }

        [Required]
        public string PhoneNumber2 { get; set; }

        [Required]
        public string PhoneNumber3 { get; set; }

        [DataType(DataType.Url)]
        [RegularExpression(@"(http://|https://)*([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$", ErrorMessage = "Website format is not correct.")]
        public string WebSite { get; set; }

        public string Revenue { get; set; }

        public string Size { get; set; }

        public int CompanyType { get; set; }

        public int LocationID { get; set; }

        [Required]
        [Range(1, 1)]
        public int Policy { get; set; }
 
    }

     public class ReceiveUserForAdminModel
     {
         public List<GLO_Users> CompanyList { get; set; }

         public List<GLO_Users> LeaderList { get; set; }

         public List<GLO_Users> ExpertList { get; set; }
     }

     public class ReceiveUserModel
     {
         public List<ReceiveUserInfoModel> TagUserList { get; set; }

         public List<ReceiveUserInfoModel> RecentContactList { get; set; }
     }
    
     public class ReceiveUserInfoModel
     {
         public int UserID { get; set; }

         public string NickName { get; set; }

         public string Icon { get; set; }

         public string Title { get; set; }

         public string Location { get; set; }
     }



     public class UserQueryModel
     {
         public string SearchText { get; set; }

         public IList<GLO_Users> QueryResult { get; set; }
     }

     public class TagUserModel
     {
         public bool ShowFunctions { get; set; }

         public int TagID { get; set; }

         public int UserID { get; set; }

         public string UserName { get; set; }

         public int TaggedUserID { get; set; }

         public int TagCategoryID { get; set; }

         public string Memo { get; set; }

         public string FunctionName { get; set; }

         public List<GLO_TagCategory> FunctionList { get; set; }

         public bool IsSearchPage { get; set; }
     }

     public class SecurityQuestionModel
     {
         public int UserID { get; set; }

         public int Question1 { get; set; }
         public int Question2 { get; set; }
         public int Question3 { get; set; }

         public string QuestionContent1 { get; set; }
         public string QuestionContent2 { get; set; }
         public string QuestionContent3 { get; set; }

         public string Answer1 { get; set; }
         public string Answer2 { get; set; }
         public string Answer3 { get; set; }

         public List<GLO_SecurityQuestion> QuestionList { get; set; }
     }

     public class ResumeModel
     {
         public int UserID { get; set; }

         public int JobID { get; set; }

         public string JobNO { get; set; }

         public string JobTitle { get; set; }

         public string ResumeName { get; set; }

         public int CompanyID { get; set; }

         public IList<GLO_Resumes> ResumeList { get; set; }
     }
}