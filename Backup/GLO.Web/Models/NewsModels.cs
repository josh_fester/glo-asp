﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
namespace GLO.Web.Models
{
    public class NewsModels
    {
        [Required]
        public int CategoryID{get;set;}
        [Required]
        public string Title{get;set;}
        [Required]
        public string Content{get;set;}
        public int ClickCount{get;set;}
        public DateTime CreateDate{get;set;}
    }
}