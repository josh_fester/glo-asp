﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GLO.Web.Models
{
    public class CompanyJob
    {
        public int UserID { get; set; }
        [Required]
        public string JobTitle { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string JobDetail { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}