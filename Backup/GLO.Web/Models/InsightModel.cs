﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GLO.Web.Models
{
    public class InsightPost
    {
        public int PostID { get; set; }

        [Required(ErrorMessage = "The title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The summary is required.")]
        public string Description { get; set; }

        public string PostContent { get; set; }

        public string PostTag { get; set; }

    }
}