﻿$.extend({
	ajaxCheckCode: function(o) {
		var defaults =
		{
			ajax: true,
			inputId: "#CheckCode",
			imageId: "#imgCheckCode",
			verifyUrl: "/CheckCode/Verify/",
			newImageUrl: "/CheckCode/New/",
			newImageCtrlIds: new Array("#imgCheckCode", "#changeImg"),
			verifyAfter: defaultVerifyAfter,
			newImageAfter: defaultNewImageAfter
		};
		o = $.extend(defaults, o || {});
		for (i = 0; i < o.newImageCtrlIds.length; i++) {
			$(o.newImageCtrlIds[i]).click(getNewImage);
		};

		if (o.ajax) {
			$(o.inputId).keydown(function(e) { $(this).attr('oldvalue', $(this).val()); });
			$(o.inputId).keyup(function(e) { var codeValue = $(this).val(); var oldCodeValue = $(this).attr("oldvalue"); doVerify(codeValue, oldCodeValue); });
		}

		function doVerify(codeValue, oldCodeValue) {
			if (codeValue == '' || codeValue == undefined) {
				o.verifyAfter(false);
				return;
			}
			if (codeValue == oldCodeValue) return;
			$.post(o.verifyUrl, { aCheckCode: codeValue }, function(resultData) { o.verifyAfter(resultData == 'true' || resultData == true); });
		};

		function defaultVerifyAfter(result) {
			var obj = $(o.inputId);
			if (obj != undefined) {
				if (result) {
					obj.css("color", "green");
				}
				else {
					obj.css("color", "red");
				}
			}
		};

		function defaultNewImageAfter() {
			var obj = $(o.inputId);
			if (obj != undefined) {
				obj.css("color", "black");
				//obj.focus();
			}
		};

		function getNewImage() {
			$(o.imageId).attr('src', o.newImageUrl + new Date().valueOf());
			$(o.inputId).val('');
			o.newImageAfter();
		};
	}
});
