﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap.Configuration.DSL;
using StructureMap;
using GLO.Services;
using GLO.Data;


namespace GLO.Web
{
    public static class Bootstrapper
    {

        public static void ConfigureStructureMap()
        {
            ObjectFactory.Initialize(x => {
                x.UseDefaultStructureMapConfigFile = false;
                x.IgnoreStructureMapConfig = true; 
                x.AddRegistry(new ToolsRegistry()); });
        }
    }
    //injection
    public class ToolsRegistry : Registry
    {
        public ToolsRegistry()
        {
            
            For<INewsService>()
                .Use<NewsService>();

            For<INewsRepository>()
                .Use<NewsRepository>();

            For<IQuickPollRepository>()
                .Use<QuickPollRepository>();

            For<IQuickPollService>()
                .Use<QuickPollService>();

            For<ISuccessRepository>()
                .Use<SuccessRepository>();

            For<ISuccessService>()
                .Use<SuccessService>();

            For<IPartnerRepository>()
              .Use<PartnerRepository>();

            For<IPartnerService>()
              .Use<PartnerService>();

            For<IAccountRepository>()
              .Use<AccountRepository>();

            For<ILeaderRepository>()
                .Use<LeaderRepository>();

            For <IExpertRepository>()
                .Use<ExpertRepository>();

            For<ICompanyRepository>()
                .Use<CompanyRepository>();

            For<ITagUserRepository>()
                .Use<TagUserRepository>();

            For<IMessageRepository>()
                .Use<MessageRepository>();

            For<IFAQRepository>()
                .Use<FAQRepository>();

            For<IDifferenceRepository>()
                .Use<DifferenceRepository>();

            For<IPolicyRepository>()
                .Use<PolicyRepository>();
            
            For<IFeedBackRepository>()
                .Use<FeedBackRepository>();

            For<ISolutionsRepository>()
                .Use<SolutionsRepository>();

            For<IAccountService>()
                .Use<AccountService>();

            For<ILeaderService>()
               .Use<LeaderService>();

            For<ICompanyService>()
               .Use<CompanyService>();

            For<IExpertService>()
               .Use<ExpertService>();

            For<ITagUserService>()
             .Use<TagUserService>();

            For<IMessageService>()
            .Use<MessageService>();

            For<IWebInfoRepository>()
                .Use<WebInfoRepository>();

            For<IWebInfoService>()
                .Use<WebInfoService>();           
            For<IFAQService>()
                .Use<FAQService>();

            For<IDifferenceService>()
                .Use<DifferenceService>();

            For<IPolicyService>()
                .Use<PolicyService>();

            For<IFeedBackService>()
                .Use<FeedBackService>();

            For<ISolutionsService>()
                .Use<SolutionsService>();

            For<ICompetenceRepository>()
                .Use<CompetenceRepository>();

            For<ICompetenceService>()
                .Use<CompetenceService>();

            For<IInsightRepository>()
               .Use<InsightRepository>();

            For<IInsightService>()
                .Use<InsightService>();


            //For<IWebPerformanceRepository>()
            //    .Use<SqlWebPerformanceRepository>();

            //For<IWebPerformanceService>()
            //    .Use<WebPerformanceService>();

            //For<IQuickPollRepository>()
            //    .Use<SqlQuickPollRepository>();

            //For<IQuickPollService>()
            //    .Use<QuickPollService>();

            //// Server Statistics
            //For<KBB.Tools.Data.WebPerformance.IServerStatisticsRepository>()
            //    .Use<KBB.Tools.Data.WebPerformance.SqlServerStatisticsRepository>();

            //For<KBB.Tools.Services.WebPerformance.IServerStatisticsService>()
            //    .Use<KBB.Tools.Services.WebPerformance.ServerStatisticService>();

            //// Security
            //For<KBB.Tools.Data.Security.IKBBToolsSecurityRepository>()
            //    .Use<KBB.Tools.Data.Security.SqlKBBToolsSecurityRepository>();

            //For<KBB.Tools.Services.Security.IKBBToolsSecurityService>()
            //    .Use<KBB.Tools.Services.Security.KBBToolsSecurityService>();
        }
    }
}