﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="GLO.Data.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Leader List -- Please select a user to enter
            </div>
            <div class="boxer_content">
                <div id="errorMessage"></div>
                <div class="leaderlist">
                   <table>
                        <tr>
                            <th colspan="2">Nick Name</th>
                            <th class="th_action">Action</th>
                        </tr>
                    <% foreach (var item in (List<GLO_Users>)ViewBag.LeaderList) { %>
                        <tr>
                            <td width="55">        
                                <div class="message_usericon">
                                 <% if (item.Approved)
                                    { %>
                                        <img src="/images/<%=!String.IsNullOrEmpty(item.GLO_Leader.LogoUrl)?item.GLO_Leader.LogoUrl:"user_none.gif" %>" />
                                        <%}
                                    else
                                    { %>
                                    <img src="/images/<%=!String.IsNullOrEmpty(item.GLO_Leader.LogoUrl)?item.GLO_Leader.LogoUrl:"user_none.gif" %>" />
                                        <%} %>
                                </div>
                            </td>
                            <td>
                                <%: Html.Raw(item.NickName)%>
                            </td>
                            <td class="txtcenter">
                                <a href="javascript:void(0);" class="hpyaction" onclick="Login('<%=item.UserID %>','<%=ViewBag.ReturnUrl==null?"":ViewBag.ReturnUrl %>');">Enter</a>
                            </td>
                        </tr>
                    <% } %>
                    </table>
                </div>
                 <script type="text/javascript">
                    function Login(id,returnurl) {
                        $.postJSON('<%=Url.Action("LeaderList","Account") %>', { "id": id, "returnUrl": returnurl }, function (data) {
                            if (data.Success == true) {
                                var url = '';
                                if (data.ResultUrl != null && data.ResultUrl != "") {
                                    url = data.ResultUrl;
                                }
                                else
                                    url = '<%=Url.Action("Index","Leader") %>' + "?leaderid=" + data.UserID;
                                window.location.href = url;

                                return false;
                            }
                            else {
                                $('#errorMessage').html(data.Result);
                                return false;
                            }
                        });
                    }

    
                </script>  
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
