﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RegisterModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterExpert
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
  
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
    <script src="<%=Url.Content("~/Scripts/jquery.cl.ajaxCheckCode.js") %>" type="text/javascript"></script>
    <style>
    .input-validation-error
    {
        border-color: Red;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("RegisterExpert", "Account", FormMethod.Post, new { id = "regfrm" }))
   {
       %>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(m=>m.UserType) %>
 <div class="glo_body">
    <div class="glo_boxer">
        <div class="boxer_headline">
               Welcome to the GLO community of Good Leaders Online<br />
               and thank you for your interest to join as a GLO expert!
        </div>
        <div class="register_user">
            <table>
                 <tr>
                    <td colspan="2" height="10">Kindly confirm your basic expert info: <span class="required">Fields with * are required fields!</span></td>
                </tr>
                 <tr>
                    <td width="125" class="toptext">
                        Email
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.Email, new { @class = "txtbox1", maxlength = "50" })%><span class="required">*</span>
                        <br />Email will be used to access your account
                        <br /><%: Html.ValidationMessageFor(model => model.Email)%>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.Password, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.Password) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Confirm password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Your name
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.FirstName, new { @class = "txtbox6", Value = "First name", maxlength = "30" })%>&nbsp;&nbsp;&nbsp;&nbsp;<%: Html.TextBoxFor(m => m.LastName, new { @class = "txtbox6", Value = "Last name", maxlength = "30" })%><span class="required">*</span>
                       <br /><%: Html.ValidationMessageFor(model => model.FirstName)%> &nbsp;&nbsp;&nbsp;
                        <%: Html.ValidationMessageFor(model => model.LastName)%>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Gender
                    </td>
                    <td height="45">
                        <input type="radio" name="Sex" value="0" <%if(Model.Sex==0){ %> checked="checked"<%} %> />Male <input type="radio" name="Sex" value="1" <%if(Model.Sex==1){ %> checked="checked"<%} %>/>Female<span class="required">*</span><br />
                        <span class="remind">Cannot be modified after registration.</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        location
                    </td>
                    <td>
                        <div class="multiple_box">
                            <%: Html.HiddenFor(model => model.LocationID, new { @class = "multiple_locationid" })%>
                            <input class="multiple_value" id="expertLocation" name="Location" type="text" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.Location)?"Select a location":Model.Location %>" />
                            <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                             <div class="multiple_wrapper">
                               <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                           </div> 
                        </div><span class="required">*</span>
                    </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Industry
                    </td>
                    <td>
                         <div class="multiple_box">
                            <%: Html.HiddenFor(model => model.IndustryID, new { @class = "multiple_id" })%>
                            <input class="multiple_value" id="Industry" name="Industry" type="text" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.Industry)?"Select an industry":Model.Industry %>" />
                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                            <div class="multiple_wrapper">
                                <%Html.RenderPartial("~/Views/Shared/IndustrySelect.ascx", (IList<GLO.Data.Models.GLO_IndustryCategory>)ViewBag.IndustryList); %>
                            </div>
                         </div><span class="required">*</span>
                    </td>
                </tr>
               
                <tr>
                    <td class="toptext">
                        Phone number
                    </td>
                    <td>
                    <%: Html.TextBoxFor(model => model.PhoneNumber1, new { @class = "txtbox2", Value="+", MaxLength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber2, new { @class = "txtbox2", MaxLength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber3, new { @class = "txtbox4", MaxLength = "11" })%>
                       
                    </td>
                </tr>                
                <tr>
                    <td class="toptext">
                        check code
                    </td>
                    <td>
                        <%:Html.TextBoxFor(m => m.CheckCode, new { MaxLength = "10", @class = "txtbox3", size = "50" })%>                        
                        <span><%:Html.CheckCodeImage()%></span> <a id="changeImg" href="#">change?</a><span class="required">*</span><br />
                        <%:Html.ValidationMessageFor(model => model.CheckCode)%>
                    </td>
                </tr>
            </table> 
        
            <div style="clear:both;padding:20px 0 0 10px;">                
                <div style="float:left;margin-right:5px;">
                    <%: Html.HiddenFor(model=>model.Policy) %>
                    <input type="checkbox" id="chkPolicy" />
                </div>
                I have <a href="javascript:void(0);" id="RegisterRead">read</a> and accept GLO's terms and conditions
            </div>
        </div>

        <div class="me_apply"><a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif"></a></div>
    </div>
 </div>
<% } %>

<script type="text/javascript">
    $(document).ready(function () {
        var locationError = '<%=ViewBag.LocationError.ToString() %>';
        if (locationError != "") {
            $('#expertLocation').parent().addClass("select_error");
        }

        var industryError = '<%=ViewBag.IndustryError.ToString() %>';
        if (industryError != "") {
            $('#Industry').parent().addClass("select_error");
        }

        $('#PhoneNumber1').blur(function () {
            if ($(this).val() == '') $(this).val('+');
        });

        $('#FirstName').focus(function () {
            if ($(this).val() == 'First name') $(this).val('');
        });
        $('#FirstName').blur(function () {
            if ($(this).val() == '') $(this).val('First name');
        });

        $('#LastName').focus(function () {
            if ($(this).val() == 'Last name') $(this).val('');
        });
        $('#LastName').blur(function () {
            if ($(this).val() == '') $(this).val('Last name');
        });

        $.ajaxCheckCode();
        $('#changeImg').click();

        $('#chkPolicy').click(function () {
            if ($(this).attr("checked")) {
                $(this).removeAttr("checked");
                $('#Policy').val('0');

                $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
                });
            }
            else {
                $('#Policy').val('0');
            }
        });
        if ($('#Policy').val() == "1") {
            $('#chkPolicy').attr("checked", "checked");
        }
        else {
            $('#chkPolicy').removeAttr("checked");
        }
        $("#btnSubmit").click(function () {
            if (!$('#chkPolicy').attr("checked")) {
                $('#chkPolicy').parent().addClass("select_error");
            }

            if ($('#expertLocation').val() == "" || $('#expertLocation').val() == "Select a location") {
                $('#expertLocation').parent().addClass("select_error");
                $("#LocationID").val("");
            }

            if ($('#Industry').val() == "" || $('#Industry').val() == "Select an industry") {
                $('#Industry').parent().addClass("select_error");
                $("#IndustryID").val("");
            }



            $("#regfrm").submit();
        });

        $("#RegisterRead").click(function () {
            $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>