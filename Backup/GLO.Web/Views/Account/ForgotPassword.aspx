﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ForgotPassword
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Get Your Password
            </div>
            <div class="boxer_content">
                <div class="login_wrap" style="margin-top:0;">
                    <div id="divselect">
                        <table>
                            <tr>
                                <td><b>Please choose a way to get your password</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="radio" name="ResetType" value="0" id="rbtnEmail" checked="checked" />Send Email<br />
                                    <input type="radio" name="ResetType" value="1" id="rbtnQuestion" />Security Question
                                </td>
                            </tr>
                            <tr>
                                <td><input id="SelectType" type="button" class="bluebtn2" value="Next" /></td>
                            </tr>
                        </table>
                    </div>
                    <div id="divemail" style="display:none">
                        <table>
                            <tr>
                                <td><b>Please enter your registered email</b></td>
                            </tr>
                            <tr>
                                <td>
                                <input type="text" id="UserName" value="e-mail address" onfocus="if (this.value=='e-mail address')this.value=''" onblur="if (this.value=='')this.value='e-mail address'" />
                                <input id="SendEmail" type="button" class="bluebtn2" value="Submit" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divquestion" style="display:none">
                        <table>
                            <tr>
                                <td><b>Please enter your registered email</b></td>
                            </tr>
                            <tr>
                                <td>
                                <input type="text" id="UserName1" value="e-mail address" onfocus="if (this.value=='e-mail address')this.value=''" onblur="if (this.value=='')this.value='e-mail address'" />
                                <input id="SecurityQuestionStep1" type="button" class="bluebtn2" value="Next" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#SelectType").click(function () {
                $("#divselect").hide();

                var type = $('input:radio[name=ResetType]:checked').val();
                if (type == 1) {
                    $("#divquestion").show();
                }
                else {
                    $("#divemail").show();
                }
            });
            $("#SendEmail").click(function () {
                var userName = $('#UserName').val();
                if (userName == "" || userName == "e-mail address") {
                    $("#UserName").addClass("input_error");
                    return false;
                }
                else {
                    $("#UserName").removeClass("input_error");
                }
                $.post("/Account/ForgotPassword", "userName=" + userName, function (data) {
                    if (data.Result) {
                        alertbox(data.Message, function () {
                            window.location.href = "/Account/SignOut";
                        });
                    }
                    else {
                        alertbox(data.Message, function () {
                            $('#UserName').val("");
                            $('#UserName').focus();
                            $('#rbtnEmail').click();
                            return false;
                        });
                    }
                }, 'json');
            });

            $("#SecurityQuestionStep1").click(function () {
                var userName = $("#UserName1").val().trim();
                var postData = 'userName=' + userName;
                if (userName == "" || userName == "e-mail address") {
                    $("#UserName1").addClass("input_error");
                    return false;
                }
                else {
                    $("#UserName1").removeClass("input_error");
                }
                $.post("/Account/CheckUserName", postData, function (data) {
                    if (!data) {
                        alertbox(userName + " is not exist.", function () {
                            $('#UserName1').val("");
                            $('#UserName1').focus();
                            $('#rbtnQuestion').click();
                            return false;
                        });
                        return false;
                    }
                    else {
                        AnswerSecurityQuestion(postData);
                    }
                });
            });
        });
        function AnswerSecurityQuestion(postData) {
            $.post("/Account/AnswerSecurityQuestion", postData, function (data) {
                $('#divquestion').html(data);
            });
        }
    </script>
</asp:Content>
