﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.LoginModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Login
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server"> 

    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Sign In
            </div>
            <div class="boxer_content">
                <div class="login_wrap">
                    <table>
                        <tr>
                            <td class="title"><b>Email Address</b></td>
                            <td>
                                <%: Html.TextBoxFor(model => model.UserName)%><br />
                                <%: Html.ValidationMessageFor(model => model.UserName) %>                            
                            </td>
                        </tr>
                        <tr>
                            <td class="title"><b>Password</b></td>
                            <td>
                                <%: Html.EditorFor(model => model.Password) %><br />
                                <%: Html.ValidationMessageFor(model => model.Password) %>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input id="chbRemember" type="checkbox" /> Remember
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" class="bluebtn2" value="Sign In" />&nbsp;&nbsp;&nbsp;<%: Html.ActionLink("Forgot Password?", "forgotpassword", "account")%>&nbsp;&nbsp;&nbsp;<%: Html.ActionLink("New member?", "RegisterGuide","Home")%></td>
                        </tr>
                        <tr>
                            <td></td><td id="error"><%=ViewBag.Message!=null?ViewBag.Message:""%></td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
<% } %>

<script type="text/javascript">
    $(document).ready(function () {
        $('.bluebtn2').click(function () {
            $('#error').empty();
        });
    });
</script>

</asp:Content>
