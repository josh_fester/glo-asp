﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RegisterCompanyModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterCompany
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">  
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("RegisterCompany", "Account", FormMethod.Post, new { id = "regfrm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
 <div class="glo_body">
<input type="hidden" name="CompanyType" id="CompanyType" value=<%=ViewBag.RegType%> />
    <div class="glo_boxer">
            <div class="boxer_headline">
               Thank You for confirming your ‘<%=ViewBag.RegTypeName%>’ membership!

            </div>
        <div class="register_user">
            <table>
                <tr>
                   <td colspan="2"><b>You are now only 3 steps away from exploring all its benefits.</b></td>
                    </tr>
                 <tr>
                    <td colspan="2">Kindly confirm your basic company info: <span class="required">Fields with * are required fields!</span></td>
                </tr>
                 <tr>
                    <td width="125" class="toptext">
                        Email
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.Email, new { @class = "txtbox1", maxlength = "50" })%><span class="required">*</span>                        
                        <br />Email will be used to access your account
                        <br /><%: Html.ValidationMessageFor(model => model.Email)%>
                    </td>
                </tr>              
                <tr>
                    <td class="toptext">
                        Password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.Password, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.Password) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Confirm password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Company name
                    </td>
                    <td>
                         <%: Html.TextBoxFor(model => model.CompanyName, new { @class = "txtbox1", maxlength = "50" })%><span class="required">*</span>
                         <br /><%: Html.ValidationMessageFor(model => model.CompanyName)%>
                    </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Company address
                    </td>
                    <td>
                         <%: Html.TextBoxFor(model => model.Address, new { @class = "txtbox1", maxlength = "100" })%><span class="required"></span>
                         <br /><%: Html.ValidationMessageFor(model => model.Address)%>
                    </td>
                </tr>
               
                <tr>
                    <td class="toptext">
                        Contact phone
                    </td>
                    <td>
                    <%: Html.TextBoxFor(model => model.PhoneNumber1, new { @class = "txtbox2", Value = "+", maxlength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber2, new { @class = "txtbox2", maxlength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber3, new { @class = "txtbox4", maxlength = "11" })%><span class="required">*</span>
                    <br /><span id="RequiredPhone" style="display:none">The Contact phone field is required.</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        location
                    </td>
                    <td>
                        <div class="multiple_box">
                             <%: Html.HiddenFor(model => model.LocationID, new { @class = "multiple_locationid" })%>
                            <input class="multiple_value" id="Location" name="Location" type="text" readonly="readonly" value="Select a location" />
                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="multiple_wrapper">
                               <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                           </div> 
                        </div><span class="required">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Industry
                    </td>
                    <td>
                     <div class="multiple_box">
                     <%: Html.HiddenFor(model => model.IndustryID, new { @class = "multiple_id" })%>
                        <input class="multiple_value" id="Industry" name="Industry" type="text" readonly="readonly" value="Select an industry" />
                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                        <div class="multiple_wrapper">
                            <%Html.RenderPartial("~/Views/Shared/IndustrySelect.ascx", (IList<GLO.Data.Models.GLO_IndustryCategory>)ViewBag.IndustryList); %>
                        </div>
                     </div>
                     <span class="required">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Website
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.WebSite, new { @class = "txtbox1" })%><br />
                         <%: Html.ValidationMessageFor(model => model.WebSite)%>
                         </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Employees
                    </td>
                    <td>
                     <div class="select_box">
                            <input class="select_value" id="drpSize" name="Size" type="text" readonly="readonly" value="1-5" />
                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="select_option">
                                <ul>
                                    <li>1-5</li>
                                    <li>6–50</li>
                                    <li>51–250</li>
                                    <li>251–1000</li>
                                    <li>1001–5000</li>
                                    <li>>5000</li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                  <tr>
                    <td class="toptext">
                        Revenues
                    </td>
                    <td>
                     <div class="select_box">
                            <input class="select_value" id="drpRevenue" name="Revenue" type="text" readonly="readonly" value="<1,000,000 USD" />
                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="select_option">
                                <ul>
                                   <li><1,000,000 USD</li>
                                   <li>1,000,001-10,000,000 USD</li>
                                   <li>10,000,001–50,000,000 USD</li>
                                   <li>50,000,001–250,000,000 USD</li>
                                   <li>250,000,001–1,000,000,000 USD</li>
                                   <li>>1,000,000,001 USD</li>                                                
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
              
            </table> 
            
            <div style="clear:both;padding:20px 0 0 10px;">
                <div style="float:left;margin-right:5px;">
                    <%: Html.HiddenFor(model => model.Policy)%>
                    <input type="checkbox" id="chkPolicy" />
                </div>
                I have <a href="javascript:void(0);" id="RegisterRead">read</a> and accept GLO's terms and conditions
            </div>

            </div>
        <div class="me_apply"><a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif"></a></div>
    </div>
 </div>
 <%} %>

 
<script type="text/javascript">
    $(document).ready(function () {
        var locationError = '<%=ViewBag.LocationError.ToString() %>';
        if (locationError != "") {
            $('#Location').parent().addClass("select_error");
        }

        var industryError = '<%=ViewBag.IndustryError.ToString() %>';
        if (industryError != "") {
            $('#Industry').parent().addClass("select_error");
        }

        $('#PhoneNumber1').blur(function () {
            if ($(this).val() == '') $(this).val('+');
        });

        $('#chkPolicy').click(function () {
            if ($(this).attr("checked")) {
                $(this).removeAttr("checked");
                $('#Policy').val('0');

                $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
                });
            }
            else {
                $('#Policy').val('0');
            }
        });
        if ($('#Policy').val() == "1") {
            $('#chkPolicy').attr("checked", "checked");
        }
        else {
            $('#chkPolicy').removeAttr("checked");
        }

        $("#PhoneNumber1").blur(function () {
            if (isEmpty($("#PhoneNumber1").val()) || isEmpty($("#PhoneNumber2").val()) || isEmpty($("#PhoneNumber3").val())) {
                $("#RequiredPhone").show();
            }
            else {
                $("#RequiredPhone").hide();
            }
        });
        $("#PhoneNumber2").blur(function () {
            if (isEmpty($("#PhoneNumber1").val()) || isEmpty($("#PhoneNumber2").val()) || isEmpty($("#PhoneNumber3").val())) {
                $("#RequiredPhone").show();
            }
            else {
                $("#RequiredPhone").hide();
            }
        });
        $("#PhoneNumber3").blur(function () {
            if (isEmpty($("#PhoneNumber1").val()) || isEmpty($("#PhoneNumber2").val()) || isEmpty($("#PhoneNumber3").val())) {
                $("#RequiredPhone").show();
            }
            else {
                $("#RequiredPhone").hide();
            }
        });

        $("#btnSubmit").click(function () {
            if (!$('#chkPolicy').attr("checked")) {
                $('#chkPolicy').parent().addClass("select_error");
            }

            if (isEmpty($("#Location").val()) || $("#Location").val() == "Select a location") {
                $("#Location").parent().addClass("select_error");
            }

            if (isEmpty($("#Industry").val()) || $("#Industry").val() == "Select an industry") {
                $("#Industry").parent().addClass("select_error");
            }

            if (isEmpty($("#PhoneNumber1").val()) || isEmpty($("#PhoneNumber2").val()) || isEmpty($("#PhoneNumber3").val())) {
                $("#RequiredPhone").show();
            }

            $("#regfrm").submit();
        });

        $("#RegisterRead").click(function () {
            $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");
            $(this).css("z-index", "1001");
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
            $(".select_box").css("z-index", "1000");
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");

            var value = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(value);
        });
    });
</script>


</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
