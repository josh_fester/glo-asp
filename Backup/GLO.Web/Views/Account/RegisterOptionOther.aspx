﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterOptionOther
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_body">
        <div class="glo_boxer">         
            <div class="boxer_headline">
                My organization is not based in China / HK
            </div>
    
            <table class="tbcompany">
                <tr>
                    <td><a href="/Account/RegisterCompany?RegType=5"><img src="/Styles/images/knowglo2.gif" /></a></td>
                    <td><img src="/Styles/images/company_reg1.gif" /></td>
                    <td><img src="/Styles/images/company_reg2.gif" /></td>
                    <td><img src="/Styles/images/company_reg3.gif" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><b>Build your<br />company profile</b></td>
                    <td><b>Access GLO<br />leader profiles</b></td>
                    <td><b>Post any one job<br />for 3 months</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <div class="boxtext">
                            <b>+  Get free online <em>wise</em> assessment of your team and candidates on GLO</b>
                        </div>
                    </td>
                </tr>
            </table>   
            <table class="tbcompany2">
                <tr>
                    <td colspan="3" class="other">
                        OR SELECT
                    </td>
                </tr>
                <tr>
                    <td width="230" align="right"><a href="/Account/RegisterCompany?RegType=6"><img src="/Styles/images/basic2.gif" /></a></td>
                    <td>
                        <div class="boxtext">
                            And receive the ‘get to know GLO’ value:<br />                           
                            
                            <ul>
                                <li>for 12 months instead of 3</li>
                            </ul>

                        </div>
                    </td>
                    <td width="140">
                        <div class="greytext">Now only 90 USD</div>
                    </td>
                </tr>
                <tr>
                    <td align="right"><a href="/Account/RegisterCompany?RegType=7"><img src="/Styles/images/sme1.gif" /></a></td>
                    <td>
                        <div class="boxtext">
                            And receive the ‘Basic GLO’ value:<br />
                            <ul>
                                <li>Including any 5 jobs simultaneously posted at any time during the year  </li>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <div class="greytext">Now only 190 USD</div>
                    </td>
                </tr>
            </table>

        </div>
</div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
