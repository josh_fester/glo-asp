﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterSuccess
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID; %>
    
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Thank you for completing your profile!
            </div>
            <div class="boxer_content">
                Our GLO management will review your profile and may contact you via the email 
                associated to this account for further information.<br><br>

                After we have reviewed and approved your membership as an HR expert in the
                GLO community, you will receive an email to activate your profile and will
                be requested to pay the annual membership fees of 300USD.<br><br>


                We again express our sincere thanks for your interest to join the GLO leadership community

                <br><br>
                Peter Buytaert<br>
			    CEO, GLO
                <br><br><br><br><br><br><br><br><br><br><br><br><br>

            </div>
        <div class="boxer_agree">
            I would like to continue to complete my profile <a href="/Expert/MyValue?expertid=<%=epxertid %>"><img src="/Styles/images/icon_apply.gif" /></a><br>
            Take me back to the home page <a href="/Home/Index"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>
    </div>
    </div>
</asp:Content>
