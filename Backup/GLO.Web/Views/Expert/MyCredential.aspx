﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
     <%=Model.NickName%>'s credentials
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script src="/ckeditor/ckeditor.js" type="text/javascript" ></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID; %>
   <div class="glo_manage">   	
    	<div class="nav_expert nav_expert_credential">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li class="active"><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsOwner"].ToString()) ? Html.ActionLink("My Dashboard", "DashBoard", new { expertid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>
        <div class="manage_boxer">         
            <%Html.RenderPartial("~/Views/Shared/Expert/ProfileLeft.ascx",Model.GLO_HRExpert);%> 
           <div class="boxer_info">
                <div class="info_container">
                    <div class="client_title">Client List</div>
                    <div class="client_content font">
                        <%=HttpUtility.HtmlDecode(Model.GLO_HRExpert.Credential) %>
                    </div>
                    <div class="qualify_title">Professional Qualifications</div>
                    <div class="qualify_content font">
                        <%=HttpUtility.HtmlDecode(Model.GLO_HRExpert.Qualification) %>
                    </div>
                </div>
                <div class="me_edit3">
                  <%if (bool.Parse(ViewData["IsOwner"].ToString()))
                    { %>
                <a href="javascript:void(0)" id="editCredential"><img src="/Styles/images/icon_edit.gif" /></a>
                <%} %>
                </div>             
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>
         <script type="text/javascript">
            var credentialsStatus='<%= Model.GLO_UserEx.CredentialsStatus %>';
             $(document).ready(function () {                              
                 $("#editCredential").click(function(){
                       $.getJSON('_MyCredential', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {                           
                              $("#lightboxwrapper").attr("style", "display:block");
                              $('#lightboxwrapper').empty().html(data.Html);
                       }); 
                 });
                  $("#editLeftInfo").click(function(){
                   $.getJSON('_EidtLeftInfo', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                       $("#lightboxwrapper").attr("style", "display:block");
                        $('#lightboxwrapper').empty().html(data.Html);
                    });
                    
                 });
                 
                if(credentialsStatus=="False")
                {
                    $("#editCredential").click();
                }
             });
        </script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
</div>
</asp:Content>