﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ComposeMessage
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Compose new message</h2>
     <%Html.RenderPartial("~/Views/Shared/Message/MessageEditForAdmin.ascx", (GLO.Web.Models.MessageModel)ViewBag.SendMessage);%>
     
<div id="lightboxwrapper">
</div>
</asp:Content>
