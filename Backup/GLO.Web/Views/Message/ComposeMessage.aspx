﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ComposeMessage
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_expert nav_expert_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue","Expert",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential","Expert",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence","Expert",new { expertid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation","Expert",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Expert",new { expertid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Expert/DashboardMenu.ascx",Model.GLO_HRExpert);%> 
                </div>

                <div class="board_list1">
                        <%Html.RenderPartial("~/Views/Shared/Message/MessageEdit.ascx", (GLO.Web.Models.MessageModel)ViewBag.SendMessage);%> 
                </div>
               

                <%--<div class="me_apply"><a href="javascript:void(0)" id="btnSend"><img src="/Styles/images/icon_apply.gif"></a></div>--%>
                
            </div>
        </div>
 </div>
<div id="lightboxwrapper">
</div>
</asp:Content>


