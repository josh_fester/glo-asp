﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyMessage
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>
                 <li><a href="<%=Url.Action("CompanyIndex","Company",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet","Company",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob","Company",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Company",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                 <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%>                        
                </div>
                <div class="board_list2">
                    <div class="message_info">                                  
                         <%if (ViewBag.Status == "0") {%>
                            <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count>0)
                                { %>
                                     <%Html.RenderPartial("~/Views/Shared/Message/ReceiveMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>  
                            <%}
                                else
                                { %>
                                <div style="text-align:center;">There is no emails in the inbox.</div>
                            <%} %> 
                         <%}else if (ViewBag.Status == "1"){ %>
                            <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                                { %>
                                <%Html.RenderPartial("~/Views/Shared/Message/SendMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%> 
                            <%}
                                else
                                { %>
                                <div style="text-align:center;">There is no emails in the sent box.</div>
                            <%} %>
                         <%}else if (ViewBag.Status == "2"){ %>
                            <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                                 { %>
                                <%Html.RenderPartial("~/Views/Shared/Message/ArchiveMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                            <%}
                                else
                                { %>
                                <div style="text-align:center;">There is no emails in the archive box.</div>
                            <%} %>
                         <%}else if (ViewBag.Status == "3"){ %>
                            <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                                 { %>
                                <%Html.RenderPartial("~/Views/Shared/Message/TrashMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                            <%}
                                else
                                { %>
                                <div style="text-align:center;">There is no emails in the trash box.</div>
                            <%} %>
                         <%}%>
                    </div>

                </div>
                <div class="board_content">
                	<div class="message_content">                     
                    
                    </div>
                </div>
                
            </div>
        </div>
 </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">

<div id="lightboxwrapper">
</div>
<div id="lightboxwrapper2">
    <div class="lightbox_mask"></div>
    <div class="lightbox_manage">
        <div class="lightbox_emailbox">
            <div id="lightbox_sendemail"></div>
            <div class="me_close"><a onclick="document.getElementById('lightboxwrapper2').style.display = 'none';return false;" href="#"><img src="/Styles/images/icon_close.gif"></a></div>
        </div>
    </div>
</div>
</asp:Content>
