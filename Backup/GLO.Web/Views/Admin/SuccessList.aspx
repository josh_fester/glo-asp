﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Success>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SuccessList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Success List</h2>


    <table class="tblist">
    <tr>
        <th width="150">
           User Name
        </th>
        <th width="250">
            Summary
        </th>        
        <th>
            <%: Html.DisplayNameFor(model => model.Description) %>
        </th>
        
        <th width="150">
            <%: Html.DisplayNameFor(model => model.CreateDate) %>
        </th>
        <th width="80">
            Status
        </th>
        <th width="130">Action</th>
    </tr>

<% if (Model != null)
   {
       foreach (var item in Model)
       { %>
    <tr>
        <td>
            <%=item.GLO_Users.NickName%>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Name)%>
        </td>
       
        <td>
            <%: Html.DisplayFor(modelItem => item.Description)%>
        </td>
       
        <td>
            <%: Html.DisplayFor(modelItem => item.CreateDate)%>
        </td>
        <td>
            <%=item.IsAble ? "Enable" : "Disable"%>
        </td>
        <td>
            <%if (item.IsAble){ %>
            <%: Html.ActionLink("Disable", "SuccessEdit", new { id = item.SuccessID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this Success？')" })%>
            <%}else {%>
            <%: Html.ActionLink("Enable", "SuccessEdit", new { id = item.SuccessID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this Success？')" })%>
            <%} %>
             | <%: Html.ActionLink("Delete", "SuccessDelete", new { id = item.SuccessID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this Success？')" })%>

        </td>
    </tr>
<% }
   } %>

</table>

</asp:Content>
