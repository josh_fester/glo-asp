﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Solutions>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SolutionsDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Solutions Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
            Category</td>
            <td>
                <%if (Model.CategoryID == 1)
                  { %>
                Online Top
                <%}
                  else if (Model.CategoryID == 2)
                  { %>
                Online Bottom
                <%}
                  else if (Model.CategoryID == 3)
                  { %>
                GLO Team Top
                <%}
                  else
                  { %>
                GLO Team Bottom
                <%} %>
            </td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Title)%></td>
            <td>
            <%: Html.DisplayFor(model => model.Title)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Content) %></td>
            <td>
            <%= HttpUtility.HtmlDecode(Model.Content)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>

    <p>
        <%: Html.ActionLink("Edit", "SolutionsEdit", new { id = Model.SolutionsID })%>
        |
        <%: Html.ActionLink("Back to List", "SolutionsList") %>
    </p>
</asp:Content>
