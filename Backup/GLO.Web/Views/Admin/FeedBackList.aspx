﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_FeedBack>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FeedBackList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>FeedBack List | <%: Html.ActionLink("Add FeedBack", "FeedBackAdd")%></h2>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %> 

<table class="tblist">
    <tr>
        <th width="80">
            Order Id
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Question) %>
        </th>
        <th width="80">
            <%: Html.DisplayNameFor(model => model.Status) %>
        </th>
        <th width="240">
        Action
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.OrderId) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Question) %>
        </td>
        <td>
            <%: item.Status==true?"Enable":"Disable" %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "FeedBackEdit", new { id = item.FeedBackID })%> |
            <%: Html.ActionLink("Details", "FeedBackDetails", new { id = item.FeedBackID })%> |
            <%: Html.ActionLink("Delete", "DeleteFeedBack", new { id = item.FeedBackID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this FeedBack？')" })%> |
            <% if (item.Status.Value)
               { %>
               <%: Html.ActionLink("Disable", "DisableFeedBack", new { id = item.FeedBackID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this FeedBack？')" })%>
            <%}
               else
               { %>
               <%: Html.ActionLink("Enable", "EnableFeedBack", new { id = item.FeedBackID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this FeedBack？')" })%>
            <%} %>
        </td>
    </tr>
<% } %>

</table>
<br />
<%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "FeedBackList", new { })%>
 <br />
</asp:Content>
