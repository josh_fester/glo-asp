﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Partner>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PartnerList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Partner List | <%: Html.ActionLink("Add Partner", "PartnerAdd")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    
    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Title
            </th>
            <th width="100">
                Image
            </th>
            <th width="80">
                <%: Html.DisplayNameFor(model => model.Status) %>
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Content) %>
            </td>
            <td>
                <img src="<%: Html.Raw(item.Image) %>" width="60" />
            </td>
            <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "PartnerEdit", new { id = item.PartnerID })%>
                |
                <%: Html.ActionLink("Details", "PartnerDetails", new { id = item.PartnerID })%>
                |
                <%: Html.ActionLink("Delete", "DeletePartner", new { id = item.PartnerID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this Partner？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisablePartner", new { id = item.PartnerID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this Partner？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnablePartner", new { id = item.PartnerID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this Partner？')" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "PartnerList", new { })%>
    <br />
</asp:Content>