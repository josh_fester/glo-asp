﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Policy>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CommonTextList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>CommonTextList</h2>
  <table class="tblist">
    <tr>
        <th>
             <%: Html.DisplayNameFor(model => model.PolicyTitle)%>
        </th>
       
        <th width="240">
        Action
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
      
        <td>
            <%: Html.DisplayFor(modelItem => item.PolicyTitle) %>
        </td>
        
        <td>
            <%: Html.ActionLink("Edit", "PolicyEdit", new { id = item.PolicyID })%>
          
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
