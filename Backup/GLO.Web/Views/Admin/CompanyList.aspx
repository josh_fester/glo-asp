﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.UserQueryModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Company List</h2>
<% using (Html.BeginForm("CompanyList", "Admin", FormMethod.Post, new { id = "frmSearch" }))
   { %>
    <div class="search_user">
        Company Name:<%:Html.EditorFor(m => m.SearchText)%>
        <input type="submit" id="btnSearch" value="Search" />
    </div>
<%}%>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    <table class="tblist">
        <tr>
     <th width="60">
            User ID
        </th>   
        <th>
            Company Name
        </th>      
        <th>
            Email
        </th>
        <th width="250">
        Company Type
        </th>
        <th width="120">
            Register Date
        </th>
        <th width="60">
            Approved
        </th>
        <th width="60">
            Lock
        </th>
        <th width="60">
            Payment
        </th>
        <th>
        Operation
        </th>
        </tr>
        <% foreach (var item in Model.QueryResult)
           {
               if (item.GLO_Company != null)
               {%>
        <tr>
            <td>
                <%: Html.ActionLink(item.UserID.ToString(), "CompanyIndex", "Company", new { companyid = item.UserID }, new { @target = "_blank" })%>
        </td>
        <td>
            <%: Html.ActionLink(item.GLO_Company.CompanyName, "CompanyIndex", "Company", new { companyid = item.UserID }, new { @target = "_blank" })%>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Email)%>
            </td> 
             <td>
         <%=item.GLO_Company.GLO_CompanyType.Organization%> &nbsp;<%=item.GLO_Company.GLO_CompanyType.CompanyLevel%>
        </td> 
            <td>
                <%=string.Format("{0:yyyy-MM-dd}", item.RegisterDate)%>
            </td>
            <td>
                <%: Html.CheckBoxFor(modelItem => item.Approved, new { onclick = "Approved(this);", id = item.UserID })%>
            </td>
            <td>         
                <%: Html.CheckBoxFor(modelItem => item.IsLock, new { onclick = "Lock('" + item.UserID + "');", id = "chkLock" + item.UserID })%>
            </td>
            <td>
            <%if(item.GLO_Company.CompanyType!=1 && item.GLO_Company.CompanyType!=5){%>         
                <%: Html.CheckBox("GLO_UserEx.IsPayMent", (item.GLO_UserEx.IsPayMent != null && item.GLO_UserEx.IsPayMent==0) ? false : true, new { onclick = "Payment('" + item.UserID + "');", id = "chkPayment" + item.UserID })%>
                <%}else{ %>
                -
                <%} %>
            </td>
            <td>
             <%: Html.ActionLink("Delete", "DeleteCompany", "Admin", new { id = item.UserID }, new
{
    onclick = "return HpylinkAction(this, 'Do you want to delete this company permanently?');"
})%>
            </td>
        </tr>
        <% }
           } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "CompanyList", new { })%>
    <br />
    <script type="text/javascript">
        function Approved(obj) {
            var message = 'Do you want to approve this user？';
            if (!obj.checked) {
                message = 'Do you want to unapprove this user？';
            }
            confirmbox(message,
            function () {
                $.postJSON('<%=Url.Action("ApprovedUser") %>', { id: obj.id }, function (data) {
                    if (!data) {
                        obj.checked = "";
                        return false;
                    }
                });
            }, 
            function () {
                if (obj.checked) {
                    obj.checked = "";
                }
                else {
                    obj.checked = "true";
                }
                return false;
            });

        }
        function Lock(id) {
            var checkBtn = $('#chkLock' + id);
            var message = 'Do you want to lock the malicious user？';
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to unlock the malicious user？';
            }
            confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("LockUser") %>', { id: id }, function (data) {
                if (!data) {
                    checkBtn.removeAttr("checked");
                    return false;
                }
            });
        },
        function () {
            if (checkBtn.attr("checked") == "checked") {
                checkBtn.removeAttr("checked");
            }
            else {
                checkBtn.attr("checked", "checked");
            }
            return false;
        });
    }

    function Payment(id) {
        var checkBtn = $('#chkPayment' + id);
        var message = 'Do you like to confirm payment for this user？';
        if (checkBtn.attr("checked") != "checked") {
            message = 'Do you like to confirm unpayment this user？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("PaymentUser") %>', { id: id }, function (data) {
                if (!data) {
                    checkBtn.removeAttr("checked");
                    return false;
                }
            });
        },
        function () {
            if (checkBtn.attr("checked") == "checked") {
                checkBtn.removeAttr("checked");
            }
            else {
                checkBtn.attr("checked", "checked");
            }
            return false;
        });
    }
    </script>
</asp:Content>
