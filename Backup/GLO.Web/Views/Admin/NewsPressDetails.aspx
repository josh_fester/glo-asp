﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsPressDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        News Press Details</h2>
    <table class="tbeditor">
        <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.DisplayFor(model => model.OrderId) %>
            </td>
        </tr>
        <tr>
            <td>
                Title
            </td>
            <td>
                <%: Html.DisplayFor(model => model.Title)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.DisplayNameFor(model => model.Content) %>
            </td>
            <td>
                <%= HttpUtility.HtmlDecode(Model.Content)%>
            </td>
        </tr>
        <tr>
            <td>
                Status
            </td>
            <td>
                <%if (Model.Status.Value)
                  { %>
                Enabled
                <%}
                  else
                  { %>
                Disabled
                <%} %>
            </td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "NewsPressEdit", new { id = Model.NewsID })%>
        |
        <%: Html.ActionLink("Back to List", "NewsPressList")%>
    </p>
</asp:Content>
