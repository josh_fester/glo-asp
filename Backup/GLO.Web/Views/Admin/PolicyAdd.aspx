﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Policy>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    TextAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add Text | <%: Html.ActionLink("Back to List", "PolicyList")%></h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
            <%: Html.ValidationMessageFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
            Type</td>
            <td>
            <%: Html.TextBoxFor(model => model.PolicyTitle, new { MaxLength = "1000" })%>
            <%: Html.ValidationMessageFor(model => model.PolicyTitle)%></td>
        </tr>
        <tr>
            <td>
            Content</td>
            <td>
            <%: Html.TextAreaFor(model => model.PolicyContent)%>      
            <script type="text/javascript">
                window.onload = function () {
                    CKEDITOR.replace('PolicyContent');
                };
            </script>
            <%: Html.ValidationMessageFor(model => model.PolicyContent) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>

</asp:Content>
