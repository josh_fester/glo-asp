﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Functions>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
FunctionList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Function List  | <%: Html.ActionLink("Add Function", "FunctionAdd", "Admin", new { functionid = Guid.NewGuid() }, new { target = "_self" })%></h2>
<table class="tblist">
        <tr>
           
             <th width="80">
               Order ID
            </th>       
            <th>
                Function Name
            </th>   
            <th width="150">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
              <td>
               
               <%=item.FunctionOrderID%>
            </td>
            
            <td>
               
               <%=item.FunctionName%>
            </td>
           
            <td>
                <%: Html.ActionLink("Edit", "FunctionAdd", new { functionid = item.FunctionID })%>
                |
                <%: Html.ActionLink("Delete", "FunctionDelete", new { functionid = item.FunctionID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this function？');" })%>
               
            </td>
        </tr>
        <% } %>
    </table>
    <br />

</asp:Content>
