﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Functions>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FunctionAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
     <%: Html.HiddenFor(model => model.FunctionID) %>

<h2>Add Function | <%: Html.ActionLink("Back to list", "FunctionList", "Admin")%></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

   <table class="tbeditor">
     
        <tr>
            <td>
                Function Name
            </td>
            <td>
                <%: Html.EditorFor(model => model.FunctionName) %>
            <%: Html.ValidationMessageFor(model => model.FunctionName)%>
            </td>
        </tr>
           <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.FunctionOrderID, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.FunctionOrderID) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>

</asp:Content>
