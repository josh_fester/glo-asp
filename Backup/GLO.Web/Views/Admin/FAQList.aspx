﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_FAQ>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>FAQ List | <%: Html.ActionLink("Add FAQ", "FAQAdd")%></h2>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %> 

    <table class="tblist">
    <tr>
        <th width="80">
            Order Id
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Question) %>
        </th>
        <th width="80">
            <%: Html.DisplayNameFor(model => model.Status) %>
        </th>
        <th width="240">
        Action
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.OrderId) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Question) %>
        </td>
        <td>
            <%: item.Status==true?"Enable":"Disable" %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "FAQEdit", new { id = item.FAQID })%> |
            <%: Html.ActionLink("Details", "FAQDetails", new { id = item.FAQID })%> |
            <%: Html.ActionLink("Delete", "DeleteFAQ", new { id = item.FAQID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this FAQ？')" })%> |
            <% if (item.Status.Value)
               { %>
               <%: Html.ActionLink("Disable", "DisableFAQ", new { id = item.FAQID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this FAQ？')" })%>
            <%}
               else
               { %>
               <%: Html.ActionLink("Enable", "EnableFAQ", new { id = item.FAQID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this FAQ？')" })%>
            <%} %>
        </td>
    </tr>
<% } %>

</table>
<br />
<%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "FAQList", new { })%>
 <br />
</asp:Content>
