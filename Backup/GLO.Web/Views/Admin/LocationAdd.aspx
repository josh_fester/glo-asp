﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Location>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LocationAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
  <%: Html.HiddenFor(model => model.ParentID) %>

<h2><%=Model.ParentID == 0 ? "Add Region" : "Add Country/City "%>  | <%: Html.ActionLink("Back to list", "LocationList", "Admin")%></h2>

   <table class="tbeditor">     
        <tr>
            <td>
                <%=Model.ParentID == 0 ? "Region" : "Country/City "%>
            </td>
            <td>
                <%: Html.EditorFor(model => model.LocationName) %>
            <%: Html.ValidationMessageFor(model => model.LocationName) %>
            <%: Html.ValidationMessageFor(model => model.LocationName)%>
            </td>
        </tr>
           <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.Oreder, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.Oreder) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>
</asp:Content>
