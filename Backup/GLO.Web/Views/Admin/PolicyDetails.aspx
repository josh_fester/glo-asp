﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Policy>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PolicyDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Text Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td> Order Id</td>
            <td>
            <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
           Title</td>
            <td>
            <%: Html.DisplayFor(model => model.PolicyTitle) %></td>
        </tr>
        <tr>
            <td>
            Content</td>
            <td>
            <%= HttpUtility.HtmlDecode(Model.PolicyContent)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "PolicyEdit", new { id = Model.PolicyID })%>
        |
        <%: Html.ActionLink("Back to List", "PolicyList")%>
    </p>
</asp:Content>