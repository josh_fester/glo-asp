﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsPressEdit
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%: System.Web.Optimization.BundleTable.Bundles.ResolveBundleUrl("~/Content/themes/base/css") %>"
        rel="stylesheet" type="text/css" />
    <h2>
        Edit News Press | <%: Html.ActionLink("Back to list", "NewsPressList", "Admin")%></h2>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(model => model.NewsID) %>
    <%: Html.HiddenFor(model => model.Status) %>
    <%: Html.HiddenFor(model => model.CategoryID) %>
    <table class="tbeditor">
        <tr>
            <td>
                <label for="OrderId">
                    Order Id</label>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.OrderId)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.Title) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.Title, new { MaxLength = "100" })%>
                <%: Html.ValidationMessageFor(model => model.Title)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.Content)%>
            </td>
            <td>
                <%: Html.TextAreaFor(model => model.Content)%>
                <script type="text/javascript">
                    window.onload = function () {
                        CKEDITOR.replace('Content');
                    };
                </script>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
</asp:Content>
