﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_NewsCategory>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    AddCategory
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>AddCategory</h2>


<% using (Html.BeginForm("AddCategory","News",FormMethod.Post)) { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
            <th colspan="2">GLO_NewsCategory</th>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.CategoryName) %></td>
            <td>
            <%: Html.EditorFor(model => model.CategoryName) %>
            <%: Html.ValidationMessageFor(model => model.CategoryName) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>


</asp:Content>