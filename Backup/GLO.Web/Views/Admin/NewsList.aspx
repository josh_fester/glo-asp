﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>News List</h2>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %>    


<table class="tblist">
    <tr>
        <th width="80">
            <%: Html.DisplayNameFor(model => model.CategoryID) %>
        </th>
        <th width="200">
            <%: Html.DisplayNameFor(model => model.Title) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Content) %>
        </th>
        <th width="80">
            <%: Html.DisplayNameFor(model => model.ClickCount) %>
        </th>
        <th width="80">
            <%: Html.DisplayNameFor(model => model.CreateDate) %>
        </th>
        <th width="150"></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.CategoryID) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Title) %>
        </td>
        <td>
            <div><%= HttpUtility.HtmlDecode(item.Content)%></div>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ClickCount) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CreateDate) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "NewsEdit", new { id=item.NewsID }) %> |
            <%: Html.ActionLink("Details", "Details","News", new { id=item.NewsID },null) %> |
            <%: Html.ActionLink("Delete", "NewsDelete", new { id=item.NewsID }) %>
        </td>
    </tr>
<% } %>

</table>
<br />
 <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "NewsList", new { })%>
 <br />
</asp:Content>
