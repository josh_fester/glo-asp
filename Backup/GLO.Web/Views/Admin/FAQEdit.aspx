﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_FAQ>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQEdit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Edit FAQ | <%: Html.ActionLink("Back to List", "FAQList") %></h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.FAQID) %>
        <%: Html.HiddenFor(model => model.Status) %>

    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
            <%: Html.ValidationMessageFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Question) %></td>
            <td>
            <%: Html.TextBoxFor(model => model.Question, new { MaxLength = "1000" })%>
            <%: Html.ValidationMessageFor(model => model.Question) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Answer) %></td>
            <td>
            <%: Html.TextAreaFor(model => model.Answer)%> 
             <script type="text/javascript">
                 window.onload = function () {
                     CKEDITOR.replace('Answer');
                 };               
            </script> 
            <%: Html.ValidationMessageFor(model => model.Answer) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>

<% } %>


</asp:Content>
