﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Users>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Admin List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Admin List | <%: Html.ActionLink("Add Admin", "AdminCreate", "Admin")%></h2>

<table class="tblist">
    <tr>
        <th>
            User Name
        </th>
        <th width="150">Action</th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.UserName) %>
        </td>
        <td>
           
            <%: Html.ActionLink(item.IsLock? "Unlock":"Lock", "AdminLocked", new { id = item.UserID })%> 
        </td>
    </tr>
<% } %>

</table>

</asp:Content>