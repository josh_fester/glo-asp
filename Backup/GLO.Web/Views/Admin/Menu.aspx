﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Menu</title>
    <link href="/Styles/admin.css" rel="stylesheet" type="text/css" />
    <script src="<%: System.Web.Optimization.BundleTable.Bundles.ResolveBundleUrl("~/Scripts/js") %>"></script>
</head>
<body>
                <div class="nav_box">
                    <div style="text-align:center;">
                        <a href="javascript:void(window.top.frmRight.location.reload())"><img src="/Styles/images/admin_refresh.gif" /></a>
                        <a href="javascript:void(0)" id="SignOut"><img src="/Styles/images/admin_logout.gif" /></a>
                        
                    </div>
                    <div>
                        <ul>
                            <li class="main">
                                <span>Config</span>
                                <ul>
                                    <li><%: Html.ActionLink("Admin List", "AdminList", "Admin", new { target = "frmRight" }) %></li>
                                    <li><%: Html.ActionLink("Add Admin", "AdminCreate", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Message</span>
                                <ul>
                                    <li><%: Html.ActionLink("Compose new message", "AdminComposeMessage", "Message", null, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Inbox", "AdminMessage", "Message", new { status = 0 }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Sent", "AdminMessage", "Message", new { status = 1 }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Archive", "AdminMessage", "Message", new { status = 2 }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Trash", "AdminMessage", "Message", new { status = 3 }, new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Company (<span class="item" id="companyCount"><%if (Session["UnApprovedCompanyCount"] == null){ %>0<%}else{ %><%=Session["UnApprovedCompanyCount"].ToString()%><%} %></span>)</span>
                                <ul>
                                    <li><%: Html.ActionLink("Pending Company List", "CompanyListNotApproved", "Admin", new { f="1" }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Company List", "CompanyList", "Admin", new { f = "1" }, new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Leader (<span class="item" id="leaderCount"><%if (Session["UnApprovedLeaderCount"] == null){ %>0<%}else{ %><%=Session["UnApprovedLeaderCount"].ToString()%><%} %></span>)</span>
                                <ul>
                                    <li><%: Html.ActionLink("Pending Leader List", "LeaderListNotApproved", "Admin", new { f = "1" }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Leader List", "LeaderList", "Admin", new { f = "1" }, new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Expert (<span class="item" id="expertCount"><%if (Session["UnApprovedExpertCount"] == null){ %>0<%}else{ %><%=Session["UnApprovedExpertCount"].ToString()%><%} %></span>)</span>
                                <ul>
                                    <li><%: Html.ActionLink("Pending Expert List", "ExpertListNotApproved", "Admin", new { f = "1" }, new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Expert List", "ExpertList", "Admin", new { f = "1" }, new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage News</span>
                                <ul>
                                    <li><%: Html.ActionLink("News List", "NewsEventsList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add News", "NewsEventsAdd", "Admin", new { target = "frmRight" })%></li>

                                    <li><%: Html.ActionLink("Events List", "NewsCalendarFile", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Event", "NewsCalenderFileAdd", "Admin", new { target = "frmRight" })%></li>
                                    
                                   
                                 <%--   <li><%: Html.ActionLink("News Press List", "NewsPressList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add News Press", "NewsPressAdd", "Admin", new { target = "frmRight" })%></li>--%>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Success</span>
                                <ul>
                                    <li><%: Html.ActionLink("Success List", "SuccessList", "Admin", new { target = "frmRight" })%></li>
                                 <%--  <li><%: Html.ActionLink("Add Success", "SuccessCreate", "Admin", new { target = "frmRight" })%></li>--%>
                                  <li><%: Html.ActionLink("Request Recommendation", "RequestRecommendation", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Partner</span>
                                <ul>
                                    <li><%: Html.ActionLink("Partner List", "PartnerList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Partner", "PartnerAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                           <%-- <li class="main">
                                <span>Manage Contact</span>
                                <ul>
                                    <li><%: Html.ActionLink("Contact List", "Contact", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Survey</span>
                                <ul>
                                    <li><%: Html.ActionLink("Survey List", "PollList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Survey", "PollTopicAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>--%>
                            <li class="main">
                                <span>Manage FAQ</span>
                                <ul>
                                    <li><%: Html.ActionLink("FAQ List", "FAQList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add FAQ", "FAQAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Difference</span>
                                <ul>
                                    <li><%: Html.ActionLink("Difference List", "DifferenceList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Difference", "DifferenceAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <%--<li class="main">
                                <span>Manage Policy</span>
                                <ul>
                                    <li><%: Html.ActionLink("Policy List", "PolicyList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Policy", "PolicyAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>--%>
                            <li class="main">
                                <span>Manage FeedBack</span>
                                <ul>
                                    <li><%: Html.ActionLink("FeedBack List", "FeedBackList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add FeedBack", "FeedBackAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <%--<li class="main">
                                <span>Manage Solutions</span>
                                <ul>
                                    <li><%: Html.ActionLink("Solutions List", "SolutionsList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Solutions", "SolutionsAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>--%>
                             <li class="main">
                                <span>Manage Location</span>
                                <ul>
                                    <li><%: Html.ActionLink("Location List","LocationList", "Admin", new { target = "frmRight"})%></li>
                                    <li><%: Html.ActionLink("Add Region", "LocationAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                            <li class="main">
                                <span>Manage Industry</span>
                                <ul>
                                    <li><%: Html.ActionLink("Industry List","IndustryList", "Admin", new { target = "frmRight"})%></li>
                                    <li><%: Html.ActionLink("Add Industry", "IndustryAdd", "Admin", new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                             <li class="main">
                                <span>Manage Function</span>
                                <ul>
                                    <li><%: Html.ActionLink("Function List","FunctionList", "Admin", new { target = "frmRight"})%></li>
                                    <li><%: Html.ActionLink("Add Function", "FunctionAdd", "Admin", new { functionid = Guid.NewGuid() }, new { target = "frmRight" })%></li>
                                </ul>
                            </li>
                             <li class="main">
                                <span>Manage Static Text</span>
                                <ul>
                                    <li><%: Html.ActionLink("Static Text List", "PolicyList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Static Text Add", "PolicyAdd", "Admin", new { target = "frmRight" })%></li>
                                    
                                </ul>
                            </li>
                               <li class="main">
                                <span>Manage Insights</span>
                                <ul>
                                    <li><%: Html.ActionLink("Survey Reports", "SurveyList", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Add Survey Report", "SurveyAdd", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Manage Content", "ApproveContent", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Manage Comment", "ApproveComment", "Admin", new { target = "frmRight" })%></li>
                                    <li><%: Html.ActionLink("Bulk Send Email", "BulkSendEmail", "Admin", new { target = "frmRight" })%></li>
                                    
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
    
<script type="text/javascript">

    $(document).ready(function () {
        $(".main").find("span").click(function () {
            if ($(this).parent().find("ul").css("display") == "block") {
                $(this).parent().find("ul").hide();
                $(this).parent().removeClass("active");
            }
            else {
                $(this).parent().find("ul").show();
                $(this).parent().addClass("active");
            }
        });
        SetUnApproveUserCount();

        $("#SignOut").click(function () {
            top.location.href = "/Account/SignOut";
        });
    });
    function SetUnApproveUserCount() {
        $.post("/Admin/SetUnApprovedUserCount", null, function (data) {
            $('#companyCount').html(data.companyCount)
            $('#leaderCount').html(data.leaderCount)
            $('#expertCount').html(data.expertCount)
        }, "json");
        setTimeout(function () {
            SetUnApproveUserCount();
        }, 600000);
    }
</script>

</body>
</html>
