﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Policy>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    TextList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Text List | <%: Html.ActionLink("Add Text", "PolicyAdd")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    
    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Type
            </th>
           <%-- <th width="80">
                <%: Html.DisplayNameFor(model => model.Status) %>
            </th>--%>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.PolicyTitle) %>
            </td>
           <%-- <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>--%>
            <td>
                <%: Html.ActionLink("Edit", "PolicyEdit", new { id = item.PolicyID })%>
                <%--|
                <%: Html.ActionLink("Details", "PolicyDetails", new { id = item.PolicyID })%>
                |
                <%: Html.ActionLink("Delete", "DeletePolicy", new { id = item.PolicyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this Policy？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisablePolicy", new { id = item.PolicyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this Policy？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnablePolicy", new { id = item.PolicyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this Policy？')" })%>
                <%} %>--%>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "PolicyList", new { })%>
    <br />
</asp:Content>