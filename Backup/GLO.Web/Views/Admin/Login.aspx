﻿
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.AdminLoginModel>" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Login</title>
    <link href="/Styles/admin.css" rel="stylesheet" type="text/css" />
</head>
<body style="background:#dddddd;">

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <div class="login_wrapper">
        <table>
            <tr>
                <th colspan="2">Master Sign In</th>
            </tr>
            <tr>
                <td>
                    <%: Html.LabelFor(model => model.UserName) %>
                </td>
                <td>
                    <%: Html.EditorFor(model => model.UserName) %>
                    <%: Html.ValidationMessageFor(model => model.UserName) %>
                </td>
            </tr>
            <tr>
                <td>
                    <%: Html.LabelFor(model => model.Password) %>
                </td>
                <td>                    
                    <%: Html.EditorFor(model => model.Password) %>
                    <%: Html.ValidationMessageFor(model => model.Password) %>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="Login" class="btn_submit" />
                </td>
            </tr>
        </table>
    </div>
<% } %>

<div>
</div>
</body>
</html>

