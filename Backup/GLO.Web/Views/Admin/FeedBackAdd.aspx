﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_FeedBack>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FeedBackAdd
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Add FeedBack | <%: Html.ActionLink("Back to List", "FeedBackList") %></h2>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
            <td>
            <label for="OrderId">
                Order Id</label></td>
            <td>
            <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "3" })%>
            <%: Html.ValidationMessageFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Question) %></td>
            <td>
            <%: Html.TextAreaFor(model => model.Question, new { rows="4",cols="36", MaxLength = "1000" })%>
            <%: Html.ValidationMessageFor(model => model.Question) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>

    <% } %>

</asp:Content>
