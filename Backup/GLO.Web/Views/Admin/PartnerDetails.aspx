﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Partner>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PartnerDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Partner Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
           Content</td>
            <td>
            <%: Html.DisplayFor(model => model.Content)%></td>
        </tr>
        <tr>
            <td>Description</td>
            <td>
            <%= HttpUtility.HtmlDecode(Model.Description)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
        <tr>
            <td>
            Image Title</td>
            <td>
            <%: Html.DisplayFor(model => model.Title) %></td>
        </tr>
        <tr>
            <td>
            Image</td>
            <td>
            <img src="<%: Html.Raw(Model.Image) %>" title="<%: Html.Raw(Model.Title) %>" /></td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "PartnerEdit", new { id = Model.PartnerID })%>
        |
        <%: Html.ActionLink("Back to List", "PartnerList")%>
    </p>
</asp:Content>