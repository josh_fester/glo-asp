﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_InsightPostComment>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ManageComment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Manage comment</h2>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %>    


<table class="tblist">
    <tr>
       <%-- <th width="80">
          ID
        </th>--%>
        <th>
           Content Title
        </th>
        <th>
          Comment 
        </th>
     <%--
        <th width="80">
          Approved
        </th>--%>
        <th width="150">Operation</th>
    </tr>

<% foreach (var item in Model)
   {%>
    <tr>
       <%-- <td>
            <%: Html.DisplayFor(modelItem => item.PostCommentID) %>
        </td>--%>
        <td>
            <%: Html.DisplayFor(modelItem => item.GLO_InsightPosts.Title) %>
        </td>
        <td>
            <div><%= HttpUtility.HtmlDecode(item.Comment)%></div>
        </td>
       <%-- <td>
            <%: Html.CheckBoxFor(modelItem => item.IsDeleted)%>
        </td>--%>
       
        <td>

            <%: Html.ActionLink("Delete", "CommentDelete", new { id = item.PostCommentID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this item？')" })%>
        </td>
    </tr>
<% } %>

</table>
<br />
 <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "ApproveComment", new { })%>
 <br />

</asp:Content>
