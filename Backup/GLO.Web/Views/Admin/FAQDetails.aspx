﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_FAQ>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        FAQ Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Question) %></td>
            <td>
            <%: Html.DisplayFor(model => model.Question) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Answer) %></td>
            <td>
            <%= HttpUtility.HtmlDecode(Model.Answer)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>

    <p>
        <%: Html.ActionLink("Edit", "FAQEdit", new { id = Model.FAQID })%>
        |
        <%: Html.ActionLink("Back to List", "FAQList") %>
    </p>
</asp:Content>
