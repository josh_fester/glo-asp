﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>
<%@ Import Namespace="GLO.Data.Models" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsEdit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Edit News</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
<% var categorylist = (IList<GLO_NewsCategory>)ViewData["categoryList"];
    %>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
        <%: Html.HiddenFor(model => model.NewsID) %>
    <table class="tbeditor">
        <tr>
            <td>
            <%: Html.LabelFor(model => model.CategoryID) %></td>
            <td>
         <select id="SelectCategory" name="SelectCategory">
          <%foreach (var list in categorylist)
          {%>
              <option value="<%=list.CategoryID%>" <%= list.CategoryID==Model.CategoryID?"selected=selected":"" %>> <%=list.CategoryName %></option>
          <%}
        %>
        </select></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Title) %></td>
            <td>
            <%: Html.EditorFor(model => model.Title) %>
            <%: Html.ValidationMessageFor(model => model.Title) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Content) %></td>
            <td>
             <textarea name="editor1"><%=Model.Content %></textarea>
             <script type="text/javascript">
                 window.onload = function () {
                     CKEDITOR.replace('editor1');
                 };               
            </script> </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>

<% } %>


</asp:Content>
