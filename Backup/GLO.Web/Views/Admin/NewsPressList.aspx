﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsPressList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        News Press List | <%: Html.ActionLink("Add News Press", "NewsPressAdd", "Admin")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Title
            </th>
            <th width="80">
                Status
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Title) %>
            </td>
            <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "NewsPressEdit", new { id = item.NewsID })%>
                |
                <%: Html.ActionLink("Details", "NewsPressDetails", new { id = item.NewsID })%>
                |
                <%: Html.ActionLink("Delete", "DeleteNewsPress", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this News？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisableNewsPress", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this News？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnableNewsPress", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this News？')" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "NewsPressList", new { })%>
    <br />
</asp:Content>
