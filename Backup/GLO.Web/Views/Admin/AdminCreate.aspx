﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.AdminRegisterModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    AdminCreate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add Admin | <%: Html.ActionLink("Back to List", "AdminList")%></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <table class="tbeditor">
        <tr>
            <td width="160">
                <%: Html.LabelFor(model => model.AdminName) %>
             </td>
            <td>
            <%: Html.EditorFor(model => model.AdminName) %>
            <%: Html.ValidationMessageFor(model => model.AdminName) %>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.AdminPassword) %>
            </td>
            <td>
                <%: Html.EditorFor(model => model.AdminPassword) %>
            <%: Html.ValidationMessageFor(model => model.AdminPassword) %>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.ConfirmPassword)%>
            </td>
            <td>
            <%: Html.EditorFor(model => model.ConfirmPassword)%>
            <%: Html.ValidationMessageFor(model => model.ConfirmPassword)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>


</asp:Content>
