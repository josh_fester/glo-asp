﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_InsightPosts>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   ManageContent
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Manage Content</h2>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %>    


<table class="tblist">
    <tr>
       <%-- <th width="80">
          ID
        </th>--%>
        <th>
            <%: Html.DisplayNameFor(model => model.Title) %>
        </th>
        <th>
           
        </th>
         <th width="230">
          Recommend for email template
        </th>
        <th width="80">
          Recommend
        </th>
        <th width="150">Operation</th>
    </tr>

<% foreach (var item in Model)
   {%>
    <tr>
       <%-- <td>
            <%: Html.DisplayFor(modelItem => item.PostID) %>
        </td>--%>
        <td>
            
             <%: Html.ActionLink(item.Title, "InsightDetail", "Insight", new { postid = item.PostID }, new { target = "_blank" })%>
        </td>
        <td>
            <%--<div><%= HttpUtility.HtmlDecode(item.PostContent)%></div>--%>
        </td>
         <td>
            <%: Html.CheckBoxFor(modelItem => item.IsEmailRecommend,new { onclick = "RecommendEmailTemplate(this);", id = item.PostID })%>
        </td>
        <td>
            <%: Html.CheckBoxFor(modelItem => item.IsRecommend,new { onclick = "Recommend(this);", id = item.PostID })%>
        </td>
       
        <td>
            
            <%: Html.ActionLink("Delete", "InsightContentDel", new { id = item.PostID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this item？')" })%>
        </td>
    </tr>
<% } %>

</table>
<br />
 <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "ApproveContent", new { })%>
 <br />
 <script>
     function Recommend(obj) {
         var message = 'Do you want to recommend this content？';
         if (!obj.checked) {
             message = 'Do you want to cancel recommedation？';
         }
         confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("RecommendContent") %>', { id: obj.id }, function (data) {
                if (!data) {
                    obj.checked = "";
                    return false;
                }
            });
        },
        function () {
            if (obj.checked) {
                obj.checked = "";
            }
            else {
                obj.checked = "true";
            }
            return false;
        });

    }
    function RecommendEmailTemplate(obj) {
        var message = 'Do you want to recommend this content for email template？';
        if (!obj.checked) {
            message = 'Do you want to cancel this recommedation？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("RecommendContentForEmail") %>', { id: obj.id }, function (data) {
                if (!data) {
                    obj.checked = "";
                    return false;
                }
            });
        },
        function () {
            if (obj.checked) {
                obj.checked = "";
            }
            else {
                obj.checked = "true";
            }
            return false;
        });

    }
 </script>
</asp:Content>
