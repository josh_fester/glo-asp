﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Wisdom Check Up
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO Wise Check Up
            </div>
            <div class="boxer_content">
                <div style="float:left;width:380px">
                    <img src="/Styles/images/wisecheckup.gif" />
                </div>
                
                <div style="float:left;width:370px;font-size:18px;">
                    Stand out from the crowd and take GLO’s online wise leadership Check Up.<br /><br />

The GLO Wise Check Up provides a score on 5 leadership attributes generally associated with <em>wise</em> leadership: integrity, self awareness; empathy; vision of the greater good of society and ability to change.<br /><br />

The test only takes 20 minutes max and the reported results can be kept confidential or added to your profile if you prefer. <br /><br />

            <div class="boxer_agree" style="padding:0;">
                <div class="hpylink_bluebtn">
                   <a href="<%=ViewBag.Survey %>" class="Register" style="font-size:16px">Take me to the GLO Wise Check Up</a>
                </div>
            </div>
                </div>

                
            </div>
        </div>
    </div>


</asp:Content>