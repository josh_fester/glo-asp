﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    MyRecommendList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_leader nav_leader_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index","Leader",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence","Leader",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation","Leader",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Leader",new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx");%>                  
                </div>
               <%Html.RenderPartial("~/Views/Shared/RecommendList.ascx", (List<GLO.Data.Models.GLO_Users>)ViewBag.RecommendThemList);%>
               <%Html.RenderPartial("~/Views/Shared/RecommendMeList.ascx", (List<GLO.Data.Models.GLO_Users>)ViewBag.RecommendMeList);%>
            </div>
        </div>
        <%: Html.HiddenFor(m=>m.UserID) %>
 </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">

</div>
</asp:Content>
