﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DashBoard
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_leader nav_leader_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index","Leader",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence","Leader",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation","Leader",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Leader",new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx",Model.GLO_Leader);%> 
                </div>
                <div class="board_list3">
                	<div class="list_title">I Tagged Them</div>
                    <div class="list_info">
                    	<table>
                        <%var taglist = (IList<GLO.Data.Models.GLO_TagUsers>)ViewBag.TagList; %>
                        <%foreach (var tag in taglist)
                          { %>

                          	<tr>
                                <td><a target="_blank" href="#"><%=tag.GLO_Users.UserName%></a></td>
                            </tr>

                        <%} %>
                    
                        </table>
                    </div>
                </div>
                <div class="board_list3">
                	<div class="list_title">We Tagged Each Other</div>
                     <% var tageachlist = (IList<GLO.Data.Models.GLO_Users>)ViewBag.TagEachOther; %>
                    
                    <div class="list_info">
                    	<table>
                        <%foreach (var tageach in tageachlist)
                          { %>
                        	<tr>
                                <td><a href="#"><%=tageach.UserName%></a></td>
                            </tr>
                        <%} %>
                        	
                        </table>
                    </div>
                </div>
                <div class="board_list3">
                <% var tagmelist = (IList<GLO.Data.Models.GLO_Users>)ViewBag.TheyTagme; %>
                	<div class="list_title">They Tagged Me</div>
                    <div class="list_info">
                    	<table>
                        <%foreach (var tagme in tagmelist)
                          { %>
                        	<tr>
                                <td><a href="#"><%=tagme.UserName %></a></td>
                            </tr>
                        <%} %>
                        	
                        </table>
                    </div>
                </div>
               
                
            </div>
        </div>
 </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
