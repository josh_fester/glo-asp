﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CurrentHunting
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <%--  <script type="text/javascript" src="/Js/jquery-latest.js"></script> --%>
      <script type="text/javascript" src="/Js/jquery.tablesorter.js"></script> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_list2">

                	<div id="ActiveTitle" class="list_title">Active</div>
                    <div id="ActiveInfo" class="list_info2">
                        <div class="info_order_area"></div>
                        
                        <div class="info_result_container">
                	        <table id="myTable" class="tborder">
                                 <thead> 
                                    <tr> 
                                        <th width="103" class="list_info_head head_active">By Subject</th> 
                                        <th width="103" class="list_info_head">By Location</th> 
                                        <th width="103" class="list_info_head head_last">By Start Date</th>  
                                    </tr> 
                                 </thead> 
                              <%int mark = 1; 
                                foreach (var job in Model.GLO_Company.GLO_HuntingRequest)
                                {
                                    if (!job.IsArchive)
                                    { %>
                                    <tr class="<%=mark==1?"message_active":"" %>"  id="ActiveHunting<%=job.HuntingID%>">
                                        <td>
                                            <a class="huntingtitle" x:huntingid="<%=job.HuntingID %>" href="javascript:void(0)"><%=job.SubJect%> </a>
                                        </td>
                                        <td width="70">
                                             <%=job.Location%>
                                        </td>
                                        <td width="75">
                                         <%=job.ContactDetail%>
                                        </td>                                     
                                    </tr>               
                                <% mark++;
                                    }
                                       }%> 
                                <tr style="display:none">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>                               
                            </table>
                        </div>


                    </div>
                    <div id="ArchiveTitle" class="list_title">Archive</div>
                    <div id="ArchiveInfo" class="list_info2" style="display:none">
                    <div class="info_result_container">
                         <table>                       
                              <%foreach (var job in Model.GLO_Company.GLO_HuntingRequest)
                                {
                                    if (job.IsArchive)
                                    {%>
                                    <tr id="Hunting<%=job.HuntingID%>">
                                        <td>
                                            <a class="huntingtitle" x:huntingid="<%=job.HuntingID %>" href="javascript:void(0)"><%=job.SubJect%> </a>
                                        </td>
                                        <td width="70">
                                             <%=job.Location%>
                                        </td>
                                        <td width="75">
                                         <%=job.ContactDetail%>
                                        </td>
                                        <td width="16"><a class="deletehunting" x:hungtingid="<%=job.HuntingID %>" href="javascript:void(0)"><img src="/Styles/images/icon_delete1.gif" /></a></td>                                        
                                    </tr>               
                                <% }
                                }%>                                
                            </table>
                    </div>
                    </div>            
                	<div class="assignment_add"><a href="javascript:void(0)" id="addHunting"><img src="/Styles/images/icon_add1.gif" /></a></div>
                </div>
                <div class="board_content">
                    <div class="content_info" style="margin-top:35px;height:580px;">
                        <% if(Model.GLO_Company.GLO_HuntingRequest.FirstOrDefault(x=>x.IsArchive == false)!=null)
                            Html.RenderPartial("~/Views/Shared/Company/HuntingDetail.ascx", Model.GLO_Company.GLO_HuntingRequest.FirstOrDefault(x=>x.IsArchive==false));%>

                    </div>
                </div>
                
                
            </div>
        </div>
 </div>
  
 
<script type="text/javascript">
    $(document).ready(function () {
      $("#myTable").tablesorter(); 

        $(".list_info_head").click(function () {
            $(".list_info_head").removeClass("head_active");
            $(this).addClass("head_active");
        });


        $("#ActiveTitle").click(function () {
            $("#ActiveInfo").show();
            $("#ArchiveInfo").hide();
            $("#ArchiveTitle").css("border-top", "1px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ActiveInfo').find(".huntingtitle:eq(0)").click();
        });
        $("#ArchiveTitle").click(function () {
            $("#ActiveInfo").hide();
            $("#ArchiveInfo").show();
            $("#ArchiveTitle").css("border-top", "0px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ArchiveInfo').find(".huntingtitle:eq(0)").click();
        });

        $(".huntingtitle").live("click",function () {
            $(".huntingtitle").parent().parent().removeClass("message_active");
            $(this).parent().parent().addClass("message_active");

            $.getJSON('_HuntingDetail', { "huntingid": $(this).attr("x:huntingid"), random: Math.random() }, function (data) {
                $('.content_info').empty().html(data.Html);
            });
        });

        $("#addHunting").click(function () {
            $.getJSON('_HuntingEdit', { "huntingid": -1, "companyid": <%=Model.UserID %>, random: Math.random() }, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        $(".deletehunting").click(function () {
            var huntID = $(this).attr("x:hungtingid");
            confirmbox("Do you want to permanently delete the headhunting program?", function () {                        
                 $("#Hunting"+huntID).remove();
                $.postJSONnoQS('<%=Url.Action("DeleteHunting") %>', { "huntingid": huntID }, function (data) {
                    return false;
                });     
                $('.content_info').empty().html("");
                $('#ArchiveInfo').find(".huntingtitle:eq(0)").click();
            });
         });
        

    });

   
</script>

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
