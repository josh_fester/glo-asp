﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyJob
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
    	<div class="nav_company nav_company_job">
            <ul>
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">
                    Life At</a></li>
                <li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
                <li class="active"><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">
                    Jobs</a></li>
                <li class="dashboard">
                <%=bool.Parse(ViewData["IsCompanyOwner"].ToString())? Html.ActionLink("Our Dashboard", "DashBoard", new { companyid = Model.UserID }).ToHtmlString() : ""%>
               </li>
            </ul>
        </div>
        <div class="manage_company_job">
            <div class="headsearch">
                        <table class="search_item">
                            <tr>
                                <td colspan="2" class="item_headline">
                                    Custom job search
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Keyword
                                </td>
                                <td>
                                    <input type="text" id="KeyWord" class="keyword" value="<%=ViewBag.Keyword%>">
                                </td>
                            </tr>
                          
                            <tr>
                                <td>
                                    Location
                                </td>
                                <td>
                                    <div class="multiple_box">
                                        <input class="multiple_value SelectLocation" id="SelectLocation" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Location)?ViewBag.Location:"All Location" %>' />
                                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />
                                        <div class="multiple_wrapper">
                                            <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                                        </div>    
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Function
                                </td>
                                <td>
                                    <div class="select_box">
                                        <input class="select_value" id="SelectFunction" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Func)?ViewBag.Location:"All Function" %>' />
                                        <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                                        <div class="select_option">
                                            <ul>
                                                <li>All Function</li>
                                                <%foreach (var category in Model.GLO_TagCategory)
                                                  {
                                                      if (category.GLO_Functions.IsDefaultValue == 1)
                                                      { %>
                                                <li><%=category.GLO_Functions.FunctionName%></li>
                                                <%}
                                                  } %>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="button" value="Search" class="bluebtn" onclick="return search();" />
                                </td>
                            </tr>
                        </table>
                        <% var joblist = (IList<GLO.Data.Models.GLO_CompanyJob>)ViewBag.JobList; %>
                        <div class="result_title">
                            We found <%= joblist!=null?joblist.Count:0%> Good Jobs.
                        </div>
                            <div class="result_container">
                                <%if (joblist != null && joblist.Count > 0)
                                  {%>
                                <table class="result">
                                    <%foreach (var job in joblist)
                                      { %>
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0)" x:jobid="<%=job.JobID %>" class="JobTitle">
                                                <%=job.JobTitle%></a>
                                        </td>
                                        <td>
                                            <%=job.Location%>
                                        </td>
                                    </tr>
                                    <%} %>
                                </table>
                                <%} %>
                            </div>
            
            </div>
            
           <div class="headsearch_jobs">
           <%if (joblist.FirstOrDefault() != null)
             {
                 Html.RenderPartial("~/Views/Shared/Company/JobDetail.ascx", joblist.FirstOrDefault());
            } %>
           </div>  
        </div>
        <div class="manage_footer2"></div>    
        
        <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          {%>  
            <%Html.RenderPartial("~/Views/Shared/Company/CompanyJobWidget.ascx", Model.GLO_Company); %>
        <%} %>
       
        <div class="me_edit"></div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select_box").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                $(this).find(".select_option").toggle();
            });
            /*close dropdown*/
            $(document).click(function () {
                $('.select_option').hide();
            });
            /*set value*/
            $(".select_option li").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();

                var value = $(this).text();
                $(this).parent().parent().parent().find(".select_value").val(value);
            });


            $(".JobTitle").click(function () {
                $.getJSON('/Home/_GetJobDetail', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                    {
                        $('.headsearch_jobs').html(data.Html);
                    }
                });
            });


           var func = '<%=ViewBag.Func %>';
           if (func != "")
           {
               $("#SelectFunction").val(func);
               $("#SelectFunction+.select_value").val(func);
            }
        });

        function search() {
            var location = $(".SelectLocation").val();
            var keyword = $("#KeyWord").val();
            var func = $("#SelectFunction").val();
            window.location.href = '<%=Url.Action("CompanyJob") %>' + "?companyid="+<%=Model.UserID %>+"&keyword=" + keyword + "&location=" + location + "&func=" + func;
            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
