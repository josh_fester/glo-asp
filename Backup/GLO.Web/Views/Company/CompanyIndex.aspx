﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.NickName%>'s profile
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="/ckeditor/ckeditor.js" type="text/javascript" ></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
       <%Html.RenderPartial("~/Views/Shared/UnregisteredUserLogin.ascx");%>
    	<div class="nav_company nav_company_name">
            <ul>
                <li class="active"><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">
                    Life At</a></li>
                <li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
                <li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
                <li class="dashboard"><%=bool.Parse(ViewData["IsCompanyOwner"].ToString())? Html.ActionLink("Our Dashboard", "DashBoard", new { companyid = Model.UserID }).ToHtmlString() : ""%></li>
            </ul>
        </div>
        <div class="manage_boxer2">
            <div class="company_info font">
                <div class="company_name"><%=Model.GLO_Company.CompanyName %></div>
                <%=Model.GLO_Company.CompanyCulture %>
             
            </div>
            <div class="company_caption">
                <div class="caption_item">    
                    <div class="caption_image"><%if (!string.IsNullOrEmpty(Model.GLO_Company.Image1)) {%><img src="/Images/<%=Model.GLO_Company.Image1 %>" /><%} %></div>
                    <div class="caption_name font"><%=Model.GLO_Company.Image1Name%></div>
                </div>
                <div class="caption_item">    
                    <div class="caption_image"><%if (!string.IsNullOrEmpty(Model.GLO_Company.Image2)) {%><img src="/Images/<%=Model.GLO_Company.Image2 %>" /><%} %></div>
                    <div class="caption_name font"><%=Model.GLO_Company.Image2Name%></div>
                </div>
                <div class="caption_item">    
                    <div class="caption_image"><%if (!string.IsNullOrEmpty(Model.GLO_Company.Image3)) {%><img src="/Images/<%=Model.GLO_Company.Image3 %>" /><%} %></div>
                    <div class="caption_name font"><%=Model.GLO_Company.Image3Name%></div>
                </div>
                <div class="caption_item">   
                    <div class="caption_image"><%if (!string.IsNullOrEmpty(Model.GLO_Company.Image4)) {%><img src="/Images/<%=Model.GLO_Company.Image4 %>" /><%} %></div>
                    <div class="caption_name font"><%=Model.GLO_Company.Image4Name%></div>
                </div> 
                <div class="upload_boxer">
                    <%if (!string.IsNullOrEmpty(Model.GLO_Company.PPTFileName))
                      { %>
                        <div style="float:left; margin:5px 0;">For more information.please see the file: </div><a href="/Upload/<%=Model.UserID %>/<%=Model.GLO_Company.PPTFileName %>"><%=Model.GLO_Company.PPTFileName%></a>
                        <%} %>
              </div>               
            </div>
        </div>  
        <div class="manage_footer2"></div>    

        
        <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          {%>  
            <%Html.RenderPartial("~/Views/Shared/Company/CompanyJobWidget.ascx", Model.GLO_Company); %>
        <%} %>

        <div class="me_edit">
        <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          { %>
        <a href="javascript:void(0)" id="editCompany"><img src="/Styles/images/icon_edit.gif" /></a>
        <%} %>
        </div>
    </div>
    <script type="text/javascript">
     var BasicInfoStatus='<%= ViewBag.BasicInfoStatus.ToString() %>';
     var FactSheetStatus='<%= ViewBag.FactSheetStatus.ToString() %>';
     var showExplain= (BasicInfoStatus=="False"&&FactSheetStatus=="False")?"True":"False" ;
    $(document).ready(function () {
       
         $("#editCompany").click(function(){
            $.get('_CompanyInfoEdit', { "companyid":<%=Model.UserID %>, random: Math.random() }, function (data) {              
                $("#lightboxwrapper").attr("style", "display:block");
                 $('#lightboxwrapper').empty().html(data.Html);
             }); 
        });
        $(".EditCompanyProfile").click(function () {
            $("#editCompany").click();
        });
         <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          {%>  
         if(showExplain=="True")
         {   
              $("#lightboxwrapper").attr("style", "display:block");
         }
         else if(BasicInfoStatus=="False")
          {
               $("#editCompany").click();
          }
          <%} %>
    });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper"> 

<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_build">
        Kindly fill out any of the optional fields.<br /><br />
      This additional info will provide our leader members with 
      a more in-depth understanding of your company,  as well as 
      improve leader candidate matching, successful on-boarding 
      and retention for jobs posted.
        <br /><br />
        <br /><br />
        <div class="message_center"><a class="EditCompanyProfile" href="javascript:void(0);">Build my company profile now!</a></div>
    </div>
</div>

</div>
</asp:Content>
