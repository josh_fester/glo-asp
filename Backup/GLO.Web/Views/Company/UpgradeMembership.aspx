﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    UpgradeMembership
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_right">
                    <div class="membership_user">
                        Your current membership: <%=Model.GLO_Company.GLO_CompanyType.Organization%> &nbsp;<%=Model.GLO_Company.GLO_CompanyType.CompanyLevel%>
                        <br />
                        Expire date is <%=Model.GLO_Company.ExpireDate.ToShortDateString()%>
                    </div>
                <%if (Model.GLO_Company.GLO_CompanyType.Organization != "China/HK")
                  { %>
                   <table id="chinaother" class="tbcompany2">
                        <tr>
                            <td colspan="3" class="package">
                                Select any of the below packages:
                            </td>
                        </tr>
                        <tr>
                            <td width="120" align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=6" target="_blank"><img src="/Styles/images/basic2.gif" /></a></td>
                            <td>
                                <div class="boxtext">
                                    And receive the ‘get to know GLO’ value:<br />                           
                            
                                    <ul>
                                        <li>for 12 months instead of 3</li>
                                    </ul>

                                </div>
                            </td>
                            <td width="135">
                                <div class="greytext">Now only 90 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=7" target="_blank"><img src="/Styles/images/sme1.gif" /></a></td>
                            <td>
                                <div class="boxtext">
                                    And receive the ‘Basic GLO’ value:<br />
                                    <ul>
                                        <li>Including any 5 jobs simultaneously posted at any time during the year  </li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <div class="greytext">Now only 190 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=8" target="_blank"><img src="/Styles/images/5job.gif" /></a></td>
                            <td>
                                <div class="boxtext">
                                    Add a 5 job slot package to any of the above memberships. <br />
                                    The job slots will be available until the end of your original membership term.                   

                                </div>
                            </td>
                            <td>
                                <div class="greytext">Now only 180 USD</div>
                            </td>
                        </tr>
                    </table>
                    
                <%}
                  else
                  { %>                 
                    <table id="chinahk" class="tbcompany2">
                        <tr>
                            <td colspan="3" class="package">
                                Select any of the below packages:
                            </td>
                        </tr>
                        <tr>
                            <td width="120" align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=2" target="_blank"><img src="/Styles/images/basic1.gif" /></a></td>
                            <td>
                        <div class="boxtext">
                            Receive the ‘get to know GLO’ value:<br />                           
                            
                            <ul>
                                <li>For twelve months instead of three</li>
                                <li>With support of GLO’s research team to select five leader candidates for one management position up to 45,000 USD (*)</li>
                            </ul>

                        </div>
                            </td>
                            <td width="140">
                                <div class="greytext">Now only 450 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=3" target="_blank"><img src="/Styles/images/sme1.gif" /></a></td>
                            <td>
                                <div class="boxtext">
                                    Receive the ‘Basic GLO’ value:<br />
                                    <ul>
                                        <li>Including any five jobs simultaneously posted at any time during the year</li>
                                        <li>With support of GLO’s research team to select ten leader candidates for two management position up to 45,000 USD (*)</li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <div class="greytext">Now only 850 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=4" target="_blank"><img src="/Styles/images/premium1.gif" /></a></td>
                            <td>
                        <div class="boxtext">
                            Receive the ‘SME’ GLO value:<br />
                            <ul>
                                <li>Including any ten  jobs simultaneously posted at any time during the year</li>
                                <li>With support of GLO’s research team to select thirty leader candidates for three management position up to 45,000 USD (*)</li>
                            </ul>                       

                        </div>
                            </td>
                            <td>
                                <div class="greytext">Now only 2580 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="/PayMent/PayMentDetail?companyid=<%=Model.UserID%>&&companytype=8" target="_blank"><img src="/Styles/images/5job.gif" /></a></td>
                            <td>
                                <div class="boxtext">
                                    Add a 5 job slot package to any of the above memberships. <br />
                                    The job slots will be available until the end of your original membership term.                   

                                </div>
                            </td>
                            <td>
                                <div class="greytext">Now only 180 USD</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>(*)annual gross salary<br />Including employer contributions</td>
                            <td></td>
                        </tr>
                    </table>
                <%} %>
                </div>

                <div class="me_apply">
                   <%-- <a href="javascript:void(0)" id="btnApply"><img src="/Styles/images/icon_apply.gif"></a>--%>
                </div>
                
            </div>
        </div>
 </div>

</asp:Content>
