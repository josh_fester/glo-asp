﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.NickName %>'s jobs search
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <script type="text/javascript" src="/Js/jquery.tablesorter.js"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_list2">
                	<div id="ActiveTitle" class="list_title">Active</div>
                    <div id="ActiveInfo" class="list_info2">
                        <div class="info_order_area"></div>
                        
                        <div class="info_result_container">
                	        <table id="myTable" class="tborder">
                                 <thead> 
                                    <tr> 
                                        <th width="103" class="list_info_head head_active">By Job Title</th> 
                                        <th width="103" class="list_info_head">By Location</th> 
                                        <th width="103" class="list_info_head head_last">By Start Date</th>  
                                    </tr> 
                                 </thead> 
                                <%int mark = 1; 
                                  foreach (var job in Model.GLO_Company.GLO_CompanyJob.Where(x => x.IsActive && x.IsPublish))
                                  { %>      
                                    <tr class="job_active <%=mark==1?"message_active":"" %>">
                                        <td>
                                            <a class="JobTitle" x:jobid="<%=job.JobID %>" href="javascript:void(0)"><%=job.JobTitle%> </a>
                                        </td>
                                        <td width="70">
                                             <%=job.Location%>
                                        </td>
                                        <td width="75">
                                         <%=job.PostDate%>
                                        </td>
                                    </tr>                                            
                                <% mark++;
                                    }%>
                                 <tr style="display:none">
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr> 
                            </table>
                        </div>


                    </div>
                    <div id="ArchiveTitle" class="list_title">Archive</div>
                    <div id="ArchiveInfo" class="list_info2 info_result_container" style="display:none">
                         <table>
                                <%foreach (var job in Model.GLO_Company.GLO_CompanyJob.Where(x => x.IsActive==false && x.IsPublish ==false && x.IsDelete == false))
                                { %>                              
                                    <tr id="<%=job.JobID %>">
                                        <td>
                                            <a class="JobTitle" x:jobid="<%=job.JobID %>" href="javascript:void(0)"><%=job.JobTitle%> </a>
                                        </td>
                                        <td width="70">
                                             <%=job.Location%>
                                        </td>
                                        <td width="75">
                                         <%=job.PostDate%>
                                        </td>
                                        <td width="16">
                                       <a href="javascript:void(0)" x:jobid="<%=job.JobID %>" class="deleteAD"><img src="/Styles/images/icon_delete1.gif" /></a>
                                        </td>
                                    </tr>                                            
                                <% }%>
                            </table>
                    </div>            
                	
                </div>
                <div class="board_content">
                    <div class="content_info" style="margin-top:35px;height:580px;">
                         <%if(Model.GLO_Company.GLO_CompanyJob.FirstOrDefault(x=>x.IsActive&&x.IsPublish)!=null)
                               Html.RenderPartial("~/Views/Shared/Company/JobDetailManage.ascx", Model.GLO_Company.GLO_CompanyJob.FirstOrDefault(x => x.IsActive && x.IsPublish));%>
                    </div>
                </div>
                
            </div>
        </div>
 </div>
 
<script type="text/javascript">
    $(document).ready(function () {
        $("#myTable").tablesorter();
        $(".list_info_head").click(function () {
            $(".list_info_head").removeClass("head_active");
            $(this).addClass("head_active");
        });
        $(".JobTitle").click(function () {
            $(".JobTitle").parent().parent().removeClass("message_active");
            $(this).parent().parent().addClass("message_active");

            $.getJSON('_GetJobDetailManage', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                {
                    $('.content_info').html(data.Html);
                }
            });
        });

        $(".deleteAD").click(function () {
            var jobID = $(this).attr("x:jobid");
            confirmbox("Do you want to permanently delete the job ad?", function () {
                $.post("DeleteJob", { jobid: jobID }, function (data) {
                    if (data) {
                        $('.content_info').empty().html("");
                        $("#ArchiveInfo").find("#" + jobID).remove();
                        $('#ArchiveInfo').find(".JobTitle:eq(0)").click();
                    }
                }, "json");
            });
        });

        $("#ActiveTitle").click(function () {
            $("#ActiveInfo").show();
            $("#ArchiveInfo").hide();
            $("#ArchiveTitle").css("border-top", "1px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ActiveInfo').find(".JobTitle:eq(0)").click();
        });
        $("#ArchiveTitle").click(function () {
            $("#ActiveInfo").hide();
            $("#ArchiveInfo").show();
            $("#ArchiveTitle").css("border-top", "0px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ArchiveInfo').find(".JobTitle:eq(0)").click();
        });


    });
</script>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
