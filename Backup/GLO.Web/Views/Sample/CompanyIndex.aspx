﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyIndex
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
        <div class="nav_company nav_company_name">
            <ul>
                <li class="active"><a href="/Sample/CompanyIndex">Life At</a></li>
                <li><a href="/Sample/FactSheet">Fact Sheet</a></li>
                <li><a href="/Sample/CompanyJob">Jobs</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
       <div class="manage_boxer2">
            <div class="company_info font">
                <div class="company_name">Good Leaders Online (GLO)</div>
                <p>
	GLO is a community of good leaders assuring good business</p>
<p>
	GLO profiles, assesses and develops ‘responsible’ leaders using a global portal and local communities to share knowledge on what matters in 21st century leadership with a vision to support the common good or society at large</p>
<p>
	GLOs registered leaders pledge to lead with integrity and consider the good of society at large in their quality decision-making for their respective organizations</p>
<p>
	Our&nbsp;company members&nbsp;have a positive impact&nbsp;intrinsic to their business solutions (e.g. life sciences, clean technology, impact investment, etc) or via embedded corporate social responsibility programs and shared value&nbsp;projects</p>
<p>
	The GLO team serves&nbsp;our community driven by a passion to develop careers&nbsp;of responsible leaders and a vision to building the leading global community of good leaders assuring good business</p>
<p>
	GLO employees understand that commitment to excellence, integrity and consideration of the good of society at large are essential values to achieve GLOs vision and purpose</p>
<p>
	Joining GLO is an opportunity to work with purpose, passion and to celebrate successes as we realize&nbsp;our vision</p>

             
            </div>
            <div class="company_caption">
                <div class="caption_item">    
                    <div class="caption_image"><img src="/Images/e454bd7a-74aa-467b-8436-e8f36b86e649.jpg"></div>
                    <div class="caption_name font"></div>
                </div>
                <div class="caption_item">    
                    <div class="caption_image"><img src="/Images/0ededfca-993b-4ed9-8cfa-730baa2f3c27.JPG"></div>
                    <div class="caption_name font"></div>
                </div>
                <div class="caption_item">    
                    <div class="caption_image"><img src="/Images/16ebf662-65de-44b9-97c4-4081eebbe61e.JPG"></div>
                    <div class="caption_name font"></div>
                </div>
                <div class="caption_item">   
                    <div class="caption_image"><img src="/Images/64d04430-13cb-46c4-b5f4-88a5a7bad2fc.JPG"></div>
                    <div class="caption_name font"></div>
                </div> 
                <div class="upload_boxer">
                    
                       <div style="float:left; margin:5px 0;">For more information.please see the file: </div><a href="/Upload/504/GLO Intro.pdf">GLO Intro.pdf</a>
                        
              </div>               
            </div>
        </div>
        <div class="manage_footer2"></div> 
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
