﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExpertCredential
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">   	
    	<div class="nav_expert nav_expert_credential">
        		<ul>
                <li class="userinfo"></li>
            	<li><a href="/Sample/ExpertValue">How I Add Value</a></li>
            	<li class="active"><a href="/Sample/ExpertCredential">My Credentials</a></li>
            	<li><a href="/Sample/ExpertCompetence">My Competencies</a></li>
            	<li><a href="/Sample/ExpertRecommendation">Recommendations</a></li>
            	<li class="dashboard">
                
                </li>
            </ul>
        </div>
       <div class="manage_boxer">         
            
  <div class="boxer_user">  		
    <div class=" box_user_private_view  "></div>

    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/e2962976-c468-4265-84ed-6634692cbcb5.jpg">
    </div>
    
        <div class="box_user_button">        
            
                    <input type="button" onclick="return false;" value="Tag" class="graybtn">
                
            <input type="button" onclick="return false;" value="Request Tag" class="graybtn">
                
          
        </div>
     
     </div>
    <div class="box_user_name">Raf Adams</div> 
    <div class="box_user_title">The Suited Monk</div>
    <div class="box_user_local">ShangHai</div>
    <div class="box_user_industry">Training</div>
    <div class="box_user_note">I'd like to see a world in which people are ''awake'', are able to reach their full potential in work and life. By full potential I mean spiritually rich and have aligned their Suit (external) and Monk (Internal) world.</div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <div class="value_item_active font">Integrity</div><div class="value_item_active font">Non Judgmental</div><div class="value_item_active font">Respect</div>

    </div>
    <div class="me_edit2">
    
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#TagUser").click(function () {
            $.getJSON('/Tag/GetTagCategory', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('/Message/SendMessageToUser', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#RequestRecommend").click(function () {
            $.postJSON('/Expert/RequestRecommend', { requestUserID: '538' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });

    function RequestTag(id) {
        $.post('/Tag/RequestTag', { requestUserID: id }, function (data) {
            if (data) {
                alertbox("Your request has been successfully sent!");
            }
        });
    }
</script> 
           <div class="boxer_info">
                <div class="info_container">
                    <div class="client_title">Client List</div>
                    <div class="client_content font">
                        <p>
	<strong>Pepsico, Mead Johnson, Philips, Alcatel-Lucent, Graco, Roquette, H&amp;M, BASF, SKF, Panasonic, Ikea, Shanghai United Family Hospital, Britisch Council Guangzhou, IHG.</strong></p>

                    </div>
                    <div class="qualify_title">Professional Qualifications</div>
                    <div class="qualify_content font">
                        
                    </div>
                </div>
                <div class="me_edit3">
                  
                </div>             
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
