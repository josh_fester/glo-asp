﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderNetwork
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
        <div class="nav_leader nav_leader_network">
            <ul>
                <li class="userinfo"></li>
                <li><a href="/Sample/LeaderIndex">My Interview</a></li>
                <li><a href="/Sample/LeaderCompetence">My Competencies</a></li>
                <li class="active"><a href="/Sample/LeaderNetwork">My Network</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
        <div class="manage_boxer">
            <div class="boxer_user">
                <div class=" box_user_private_on_view">
                </div>
                <div class="box_user_photo">
                    <img src="/Images/9860a607-0fbd-4e8c-a03d-dd5c3fb8a358.jpg">
                </div>
              
                <div class="box_user_name">
                    Peter Buytaert</div>
                <div class="box_user_title">
                    CEO</div>
                <div class="box_user_local">
                    ShangHai</div>
                <div class="box_user_industry">
                    Executive Search</div>
                <div class="box_user_note">
                    Building the leading global community of good leaders assuring good business</div>
                <div class="box_user_value">
                    <div class="box_user_value_title">
                        What values in life and career are important to you:</div>
                    <div class="value_item_active font">
                        Integrity</div>
                    <div class="value_item_active font">
                        Respect</div>
                    <div class="value_item_active font">
                        Excellence</div>
                </div>
                <div class="box_user_icon">
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index3.gif"><br>
                        Profile
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index4.gif"><br>
                        Recommend
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index5.gif"><br>
                        Interview
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index6_gray.gif"><br>
                        Online Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index7_gray.gif"><br>
                        Offline Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index8_gray.gif"><br>
                        WLDP
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index9_gray.gif"><br>
                        Coaching
                    </div>
                </div>
            </div>

            <div class="boxer_info">
                    <div class="r_map">
                        <div class="r_user_box">
                            <a class="RecommendDetail" x:recommendid="7" href="javascript:void(0)">
                                <img src="/Images/b81ea190-cf94-4d12-8c80-34af47b0a130.jpg"></a>
                            <div class="r_user_name">
                                <a class="RecommendDetail" x:recommendid="7" href="javascript:void(0)">Lynnette Chan</a></div>
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_box">
                            <img src="/Images/9860a607-0fbd-4e8c-a03d-dd5c3fb8a358.jpg">
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_none">
                        </div>
                        <div class="r_user_none">
                        </div>
                        
                        <div class="r_invite1"></div>
                        <div class="r_invite2"></div>
                        <div class="r_invite3"></div>
                        <div class="r_invite4"></div>
                        <div class="r_invite5"></div>
                        <div class="r_invite6"></div>
                        <div class="r_invite7"></div>
                        <div class="r_invite8"></div>
                        <div class="competence_top6">
                            <div class="headline_top6">
                                My top 6 competencies:</div>
                            <div class="content_top6" id="showCompetency">
                                <div class="value_item_active font">
                                    Strategy
                                </div>
                                <div class="value_item_active font">
                                    Startup
                                </div>
                                <div class="value_item_active font">
                                    Restructuring
                                </div>
                                <div class="value_item_active font">
                                    Leadership development
                                </div>
                                <div class="value_item_active font">
                                    Consulting
                                </div>
                                <div class="value_item_active font">
                                    Change management
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="RecommendDetailView" class="r_info">
                        <div class="r_info_name">
                            <a href="/leader/index?leaderid=490" target="_blank">Lynnette Chan</a></div>
                        <div class="r_info_item">
                            How do I like Peter Buytaert's vision?
                        </div>
                        <div class="r_info_content">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                        </div>
                        <div class="r_info_item">
                            My experience with Peter Buytaert's values</div>
                        <div class="r_info_content">
                            <div class="value_item"> Integrity</div>
                            <div class="value_item"> Respect</div>
                            <div class="value_item"> Excellence</div>
                        </div>
                        <div class="r_info_item">
                            Other values I have for Peter Buytaert</div>
                        <div class="r_info_content">
                        </div>
                        <div class="r_info_item">
                            My confirmation on Peter Buytaert's competencies</div>
                        <div class="r_info_content">
                            <div class="value_item">
                                Strategy</div>
                            <div class="value_item_active">
                                Startup</div>
                            <div class="value_item">
                                Restructuring</div>
                            <div class="value_item">
                                Leadership development</div>
                            <div class="value_item">
                                Consulting</div>
                            <div class="value_item">
                                Change management</div>
                        </div>
                        <div class="r_info_item">
                            Other competencies Peter Buytaert has</div>
                        <div class="r_info_content">
                            <div class="value_item_active">
                                Empathy</div>
                        </div>
                        <div class="r_info_item">
                            How Peter Buytaert makes a difference?</div>
                        <div class="r_info_content font">
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#EditRecommend").click(function () {
                                    window.location.href = "/Home/RecommendationEdit?userID=" + 484 + "";
                                });
                            });
</script>
                    </div>
            </div>
        </div>
                <div class="manage_footer">
                </div>
    </div>
</asp:Content>
