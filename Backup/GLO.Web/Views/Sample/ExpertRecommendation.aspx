﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExpertRecommendation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_expert nav_expert_recommendation">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="/Sample/ExpertValue">How I Add Value</a></li>
            	<li><a href="/Sample/ExpertCredential">My Credentials</a></li>
            	<li><a href="/Sample/ExpertCompetence">My Competencies</a></li>
            	<li  class="active"><a href="/Sample/ExpertRecommendation">Recommendations</a></li>
            	<li class="dashboard">
                
                </li>
            </ul>
        </div>
       <div class="manage_boxer">        
               
  <div class="boxer_user">  		
    <div class=" box_user_private_view  "></div>

    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/e2962976-c468-4265-84ed-6634692cbcb5.jpg">
    </div>
    
        <div class="box_user_button">        
            
                    <input type="button" onclick="return false;" value="Tag" class="graybtn">
                
            <input type="button" onclick="return false;" value="Request Tag" class="graybtn">
                
           
        </div>
     
     </div>
    <div class="box_user_name">Raf Adams</div> 
    <div class="box_user_title">The Suited Monk</div>
    <div class="box_user_local">ShangHai</div>
    <div class="box_user_industry">Training</div>
    <div class="box_user_note">I'd like to see a world in which people are ''awake'', are able to reach their full potential in work and life. By full potential I mean spiritually rich and have aligned their Suit (external) and Monk (Internal) world.</div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <div class="value_item_active font">Integrity</div><div class="value_item_active font">Non Judgmental</div><div class="value_item_active font">Respect</div>

    </div>
    <div class="me_edit2">
    
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#TagUser").click(function () {
            $.getJSON('/Tag/GetTagCategory', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('/Message/SendMessageToUser', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#RequestRecommend").click(function () {
            $.postJSON('/Expert/RequestRecommend', { requestUserID: '538' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });

    function RequestTag(id) {
        $.post('/Tag/RequestTag', { requestUserID: id }, function (data) {
            if (data) {
                alertbox("Your request has been successfully sent!");
            }
        });
    }
</script> 
           <div class="boxer_info">   
                    <div class="r_map">
                    
                            <div class="r_user_none"></div>
                        
                            <div class="r_user_none"></div>
                        
                            <div class="r_user_none"></div>
                        
                            <div class="r_user_none"></div>
                        
                               <div class="r_user_box">
                                 <img src="/Images/e2962976-c468-4265-84ed-6634692cbcb5.jpg">
                               </div>
                                <div class="r_user_none"></div>
                                
                            <div class="r_user_none"></div>
                        
                            <div class="r_user_none"></div>
                        
                            <div class="r_user_none"></div>
                        

                       
                         
                        <div class="r_invite1"></div>
                        <div class="r_invite2"></div>
                        <div class="r_invite3"></div>
                        <div class="r_invite4"></div>
                        <div class="r_invite5"></div>
                        <div class="r_invite6"></div>
                        <div class="r_invite7"></div>
                        <div class="r_invite8"></div>
                        <div class="competence_top6">
                            <div class="headline_top6">My top 6 competencies:</div>
                            
                                <div class="content_top6" id="showCompetency">               
                                    
                                </div>
                        </div>
                    </div>
                    <div id="RecommendDetailView" class="r_info">
                        <div class="r_info_mask">This is a sample</div>
                     
                        <div class="r_info_name">Steve Switky</div>
                        <div class="r_info_item">How do I like James' work?</div>
                        <div class="r_info_content">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great1.gif">
                            <img src="/Styles/images/icon_great2.gif">
                            <img src="/Styles/images/icon_great2.gif">
                            <img src="/Styles/images/icon_great2.gif">
                        </div>                      
                        <div class="r_info_item">My confirmation on James' competencies</div>
                        <div class="r_info_content">
                            <div class="value_item_active">business</div>
                            <div class="value_item_active">presentation</div>
                            <div class="value_item">forecasting</div>        
                            <div class="value_item_active">win-win</div>
                            <div class="value_item_active">consumer-center design</div>         
                        </div>
                        <div class="r_info_item">Other competencies James has</div>
                        <div class="r_info_content">
                            <div class="value_item_active">strategy</div>
                            <div class="value_item_active">japanese</div>
                            <div class="value_item_active">spanish</div>        
                            <div class="value_item_active">on-time</div>
                            <div class="value_item_active">detailed</div>      
                            <div class="value_item_active">innovative</div>         
                        </div>
                        <div class="r_info_item">How James makes a difference?</div>
                        <div class="r_info_content font">‘James doesn’t tell me what to do, but speed up my self-awareness discovery process as he guides me to speak and action on what I want to achieve for the company. If you want to be a leader who leads with wisdom, inner peace, and with a strong sense of purpose, James is your “catalyst”.”</div>
                    
                    </div>
                <div class="me_edit3">
                  
                </div>
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
