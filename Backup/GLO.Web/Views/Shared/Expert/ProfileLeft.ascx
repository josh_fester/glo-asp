﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HRExpert>" %>
   <% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>; %>
  <div class="boxer_user">  		
    <div class=" <%=Model.GLO_Users.IsPublish ? "box_user_private_on_view" : "box_user_private_view"%>  "></div>

    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>"/>
    </div>
    <%if (Request.IsAuthenticated && !bool.Parse(ViewData["IsOwner"].ToString()))
      { 
    %>
        <div class="box_user_button">        
            <%if (user.UserData.UserType != 2)
            { %>
          <%if(!bool.Parse(ViewBag.ShowTagButton.ToString())){%>
                    <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                <% }
                else
                {%>
                    <input type="button" id="TagUser" class="bluebtn" value="Tag" />  
            <%} %>
            <%if (!bool.Parse(ViewBag.ShowRequestTagButton.ToString()))
              {%>
            <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                <% }
                else
                {%>
            <input type="button" id="RequestTag" class="bluebtn" value="Request Tag" onclick="RequestTag('<%=Model.UserID %>')"/>
            <%} %>
            <%} %>
            <%if (user.UserData.UserType != 3){ %>
            <input type="button" id="RequestRecommend" class="bluebtn" value="Request referral" />
            <%} %>
            <input type="button" id="SendMessage" class="bluebtn" value="Send Email">
        </div>
     <%} %>
     </div>
    <div class="box_user_name"><%=Model.NickName %></div> 
    <div class="box_user_title"><%=Model.Title %></div>
    <div class="box_user_local"><%=Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName : ""%></div>
    <div class="box_user_industry"><%= Model.GLO_Users.GLO_UserIndustry.Count>0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName : "" %></div>
    <div class="box_user_note"><%=string.IsNullOrEmpty(Model.MyVision)?"":Model.MyVision.Replace("\n", "<br>") %></div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <%if(!string.IsNullOrEmpty(Model.ExpertValue1)){%><div class='value_item_active font'><%=Model.ExpertValue1%></div><%} %>
        <%if(!string.IsNullOrEmpty(Model.ExpertValue2)){%><div class='value_item_active font'><%=Model.ExpertValue2%></div><%} %>
        <%if(!string.IsNullOrEmpty(Model.ExpertValue3)){%><div class='value_item_active font'><%=Model.ExpertValue3%></div><%} %>

    </div>
    <div class="me_edit2">
    <%if (bool.Parse(ViewData["IsOwner"].ToString()))
      { %>
    <a href="javascript:void(0)" id="editLeftInfo"><img src="/Styles/images/icon_edit.gif" /></a>
    <%} %>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#TagUser").click(function () {
            $.getJSON('<%=Url.Action("GetTagCategory","Tag") %>', { id: '<%=Model.UserID %>', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: '<%=Model.UserID %>', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#RequestRecommend").click(function () {
            $.postJSON('<%=Url.Action("RequestRecommend","Expert") %>', { requestUserID: '<%=Model.UserID %>' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });

function RequestTag(id) {
    $.post('<%=Url.Action("RequestTag","Tag") %>', { requestUserID: id }, function (data) {
        if (data) {
            alertbox("Your request has been successfully sent!");
        }
    });
}
</script>