﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_ExpertProject>" %>
 <div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_box3">
        
        	<div class="nav_expert_edit nav_expert_edit_assignment">My Dashboard</div>
            <div class="box_assignment_edit">
                <div class="content_info">
                <table>
                        <tr>
                            <td><b>Client</b></td>
                            
                            <td><input type="text" class="txt_assignment font" id="assignment_client" value="<%=Model.ClientName %>" maxlength="100" /><span class="required">*</span></td>
                        </tr>
                        <tr>
                            <td><b>Project Name</b></td>
                            <td><input type="text" class="txt_assignment font" id="assignment_project" value=" <%=Model.ProjectName %>" maxlength="100" /><span class="required">*</span></td>
                        </tr
                        <tr>
                            <td><b>Project Scope</b></td>
                            <td><input type="text" class="txt_assignment font" id="assignment_scope" value=" <%=Model.Assignment %>" /></td>
                        </tr>
                            <tr>
                            <td><b>Start Date</b></td>
                            <td><input type="text" class="txt_assignment font" id="assignment_staredate" value="<%=String.Format("{0:yyyy-MM-dd}",Model.StartDate) %>" /><span class="required">*</span></td>
                        </tr>
                            <tr>
                            <td><b>End Date</b></td>
                            <td><input type="text" class="txt_assignment font" id="assignment_enddate" value="<%=String.Format("{0:yyy-MM-dd}",Model.EndDate) %>" /><span class="required">*</span></td>
                        </tr>
                            <tr>
                            <td><b>Fee Charged</b></td>
                            <td><input type="text" class="txt_assignment font" id="assignment_charge" value="<%=Model.Charge %>" /></td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td>
                            <select id="SelectStatus" name="AssignStatus">
                                        <option value="Initial contact">Initial contact</option>
                                        <option value="Proposal made">Proposal made</option>
                                        <option value="Agreement signed">Agreement signed</option>
                                        <option value="In process">In process</option>
                                        <option value="Completed">Completed</option>
                                        <option value="Cancelled">Cancelled</option>                                        
                                        </select>
                                           

                            </td>
                        </tr>
                            <tr>
                            <td><b>Comments</b></td>
                            <td><textarea class="area_assignment" id="assignment_comment"><%=Model.Comments %></textarea></td>
                        </tr>
                </table>
                </div>
            </div>
            
            <div class="me_apply"><a href="javascript:void(0)" id="btnAssignment"><img src="/Styles/images/icon_apply.gif"></a></div>
            <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>   
        </div>
    
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        <%if(Model.Status!=""){ %>
        $("#SelectStatus").val("<%=Model.Status %>");
        <%} %>
        $("#btnAssignment").click(function () {
             if(ValidateAssign() == false)
                return;
            var assignment = {
                          'ProjectID':'<%=Model.ProjectID %>',
                         'UserID':'<%=Model.UserID %>',
                         'ProjectName': $("#assignment_project").val(),
                         'ClientName': $("#assignment_client").val(),
                         'Assignment': $("#assignment_scope").val(),
                         'StartDate':$("#assignment_staredate").val(),
                         'EndDate': $("#assignment_enddate").val(),
                         'Charge': $("#assignment_charge").val(),
                         'ProjectRequest':'',
                         'Status':$("#SelectStatus").find("option:selected").text(),
                         'Comments': $("#assignment_comment").val()
                     };  
                   $.postJSON('<%=Url.Action("AssignmentAdd") %>', { assignmentdata:$.toJSON(assignment) }, function (data) {
                         window.location.reload();
                         return false;
                     });

                 });

                 $("#assignment_staredate").datepicker({
                     defaultDate: "+1w",
                    yearRange: '-29:+0',
                    maxDate: new Date(),
                     changeMonth: true,
                     changeYear: true,
                     dateFormat: "yy-mm-dd",
                     onClose: function (selectedDate) {
                         $("#assignment_enddate").datepicker("option", "minDate", selectedDate);
                     }
                 });
                 $("#assignment_enddate").datepicker({
                     defaultDate: "+1w",
                    yearRange: '-29:+0',
                    maxDate: new Date(),
                     changeMonth: true,
                     changeYear: true,
                     dateFormat: "yy-mm-dd",
                     onClose: function (selectedDate) {
                         $("#assignment_staredate").datepicker("option", "maxDate", selectedDate);
                     }
                 });
                 
                $("#assignment_project").live("blur", function(){
                    if(!isEmpty($(this).val())) $(this).removeClass("input_error");
                });
                $("#assignment_client").live("blur", function(){
                    if(!isEmpty($(this).val())) $(this).removeClass("input_error");
                });
                $("#assignment_staredate").live("change", function(){
                   if(isEmpty($(this).val()))
                   { 
                      $(this).addClass("input_error");
                   }
                   else
                   {
                      $(this).removeClass("input_error");
                   }
                });
                $("#assignment_enddate").live("change", function(){
                   if(isEmpty($(this).val()))
                   { 
                      $(this).addClass("input_error");
                   }
                   else
                   {
                      $(this).removeClass("input_error");
                   }
                });

                 function ValidateAssign()
                 { 
                   var error = new Array();
                   if(isEmpty($("#assignment_project").val()))
                   { 
                      $("#assignment_project").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#assignment_project").removeClass("input_error");
                   }

                   if(isEmpty($("#assignment_client").val()))
                   { 
                      $("#assignment_client").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#assignment_client").removeClass("input_error");
                   }
                    
                   if(isEmpty($("#assignment_staredate,#assignment_enddate").val()))
                   { 
                      $("#assignment_staredate,#assignment_enddate").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#assignment_staredate,#assignment_enddate").removeClass("input_error");
                   }

                   if(error.length>0)
                   return false;
                   else 
                   return true;
                 }


    });

</script>