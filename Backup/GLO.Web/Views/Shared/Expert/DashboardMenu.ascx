﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
  <% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>; %>
  <ul>
         <li class='<%=ViewBag.Select == "Assignment" ? "active" : ""%>'><a href="<%=Url.Action("DashBoard","Expert",new { expertid = user.UserData.UserID }) %>">My Current Assignment</a></li>
         <li class='<%=ViewBag.Select == "CompanyTag" ? "active" : ""%>'><a href="<%=Url.Action("ExpertTag","Tag",new { userID = user.UserData.UserID,type=3 }) %>">Company Tag</a></li>
         <li class='<%=ViewBag.Select == "LeaderTag" ? "active" : ""%>'><a href="<%=Url.Action("ExpertTag","Tag",new { userID = user.UserData.UserID,type=1 }) %>">Leader Tag</a></li>
         <li class='<%=ViewBag.Select == "MyRecommendList" ? "active" : ""%>'><a href="<%=Url.Action("MyRecommendList","Expert",new { }) %>">Expert contact</a></li>
         <li class='<%=ViewBag.Select == "Password" ? "active" : ""%>'><a href="<%=Url.Action("ChangePassword","Expert",new { expertid = user.UserData.UserID }) %>">Change Password</a></li>
         <li class="level"><div><a href="javascript:void(0)">Message</a><span id="UnReadMessageCount"><%if(Session["UnReadMessageCount"]==null){ %>0<%}else{ %><%=Session["UnReadMessageCount"].ToString()%><%} %></span></div>
             <ul>
                 <li class='<%=ViewBag.Select == "ComposeMessage" ? "active" : ""%>'><a href="<%=Url.Action("ComposeMessage","Message",new { expertid = user.UserData.UserID }) %>">Compose new message</a></li>
                 <li class='<%=ViewBag.Select == "Inbox" ? "active" : ""%>'><a href="<%=Url.Action("Message","Message",new { expertid = user.UserData.UserID,status=0 }) %>">Inbox</a></li>
                 <li class='<%=ViewBag.Select == "Sent" ? "active" : ""%>'><a href="<%=Url.Action("Message","Message",new { expertid = user.UserData.UserID,status=1 }) %>">Sent</a></li>
                 <li  class='<%=ViewBag.Select == "Archive" ? "active" : ""%>'><a href="<%=Url.Action("Message","Message",new { expertid = user.UserData.UserID, status =2 }) %>">Archive</a></li>
                 <li  class='<%=ViewBag.Select == "Trash" ? "active" : ""%>'><a href="<%=Url.Action("Message","Message",new { expertid = user.UserData.UserID, status =3 }) %>">Trash</a></li>
             </ul>
        </li>
            <li class='<%=ViewBag.Select == "ReceiveEmail" ? "active" : ""%>'><a href="<%=Url.Action("ReceiveEmail","Expert",new { expertid = user.UserData.UserID }) %>">Manage Notification</a></li>
        <li class='<%=ViewBag.Select == "InsightList" ? "active" : ""%>'>
            <div class="insight_message"><a href="<%=Url.Action("InsightList","Insight",new { userid = user.UserData.UserID }) %>">Insight Comment</a></div>
            <div class="insightinfo_box">
                <img src="/Styles/images/icon_info.png" alt="" title="" />
                
                 <div>
                    Share your leadership thoughts on the GLO insights page and edit/delete existing blog comments:<br />
                    <ul>
                        <li>Click on ‘new comment’ to start a new blog entry</li>
                        <li>Click on ‘edit’ or ‘delete’ to make changes to existing entries</li>
                    </ul>    
                 </div>
             </div>
             <div class="glo_fixer"></div>
        </li>
     </ul>

     <script type="text/javascript">
         $(document).ready(function () {
             $(".insightinfo_box img").hover(function () { $(".insightinfo_box div").show(); },
                function () { $(".insightinfo_box div").hide(); }
            );
         });
     </script>