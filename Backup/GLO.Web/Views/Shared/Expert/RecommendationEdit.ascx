﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HRExpert>" %>
     <div class="lightbox_mask"></div>
	    <div class="lightbox_manage">
            <div class="lightbox_box2">
        	    <div class="nav_expert_edit nav_expert_edit_recommendation">Recommendations</div>
                <div class="box_expert_edit">                       
                    <div class="r_map" id="recommend_list">       
                          <% var recommendlist = Model.GLO_Users.GLO_Recommendation;
                             var userid = Model.UserID;
                       int count = 0;
                       foreach(var recommend in recommendlist)
                       { 
                           if (count == 4)
                           { %>
                        
                        <div class="r_user_box">
                            <img src="/Images/<%=!String.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>" />
                        </div>
                        <div class="r_user_box">
                            <img src="/Images/<%=!String.IsNullOrEmpty(recommend.LogoUrl)?recommend.LogoUrl:"user_none.gif" %>" x:recommendid="<%=recommend.RecommendID %>" />
                            <div class="r_user_name"><%=recommend.UserName %></div>
                        </div>
                        <%}
                           else if (count < 8)
                           { %>
                        <div class="r_user_box">
                            <img src="/Images/<%=!String.IsNullOrEmpty(recommend.LogoUrl)?recommend.LogoUrl:"user_none.gif" %>" x:recommendid="<%=recommend.RecommendID %>" />
                            <div class="r_user_name"><%=recommend.UserName %></div>
                        </div>
                        <%}  count ++;} %>
                        <%if (count < 8)
                          {
                              for (int i = count; i < 8; i++)
                              {
                                  if (count == 4)
                                  {%>
                               <div class="r_user_box">
                                    <img src="/Images/<%=!String.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>" />
                               </div>
                               <div class="r_user_none">                           
                                    <div class="r_user_invite" onclick="return Invite();">Invite</div>
                               </div>
                        <%}
                                  else
                                  { %>
                                  
                            <div class="r_user_none">                           
                                <div class="r_user_invite" onclick="return Invite();">Invite</div>
                            </div>
                        <%} count++;
                              }
                          } %>
                         
                        <%  int k = 1;
                          foreach (var recommend in recommendlist)
                          {
                              if (k < 9)
                              {
                                  %>
                                <div class="r_invite_edit<%=k %> line<%=k %>">
                                    <a href="javascript:void(0)" x:id=<%=k-1 %> x:recommendid=<%=recommend.RecommendID %> class="RecommendDelete "><img src="/Styles/images/invite_delete<%=k>4?9-k:k %>.gif" /></a>
                                </div>
                        <%        
                              }
                              k++;
                          } %>
                        <%for (int i = k; i < 9; i++)
                          {
                              %>
                        <div class="r_invite_edit<%=i %>">
                            <a href="javascript:void(0)" onclick="return Invite();"> <img src="/Styles/images/invite_add<%=i>4?9-i:i %>.gif" /></a>
                        </div>
                        <%}%>
                    </div>
                    <div class="r_info" style="min-height:530px;">

                        <div class="r_info_name">Steve Switky</div>
                        <div class="r_info_item">How do I like James' work?</div>
                        <div class="r_info_content">
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                        </div>                      
                        <div class="r_info_item">My confirmation on James' competencies</div>
                        <div class="r_info_content">
                            <div class="value_item_active">business</div>
                            <div class="value_item_active">presentation</div>
                            <div class="value_item">forecasting</div>        
                            <div class="value_item_active">win-win</div>
                            <div class="value_item_active">consumer-center design</div>         
                        </div>
                        <div class="r_info_item">Other competencies James has</div>
                        <div class="r_info_content">
                            <div class="value_item_active">strategy</div>
                            <div class="value_item_active">japanese</div>
                            <div class="value_item_active">spanish</div>        
                            <div class="value_item_active">on-time</div>
                            <div class="value_item_active">detailed</div>      
                            <div class="value_item_active">innovative</div>         
                        </div>
                        <div class="r_info_item">How James makes a difference?</div>
                        <div class="r_info_content font">‘James doesn’t tell me what to do, but speed up my self-awareness discovery process as he guides me to speak and action on what I want to achieve for the company. If you want to be a leader who leads with wisdom, inner peace, and with a strong sense of purpose, James is your “catalyst”.”</div>

                    </div>
                </div>
            
                <div class="me_apply"><a id="btnSubmit" href="javascript:void(0);"><img src="/Styles/images/icon_apply.gif"></a></div>
                <div class="me_close2"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>    
             </div>
             

            <div id="lightboxinvite" class="lightboxinvite">
                <div class="invite_wrap">
                    Build your referral network inviting up to 8 clients to confirm your competencies and comment on how you have made a difference for them or their organizations
                    <br /><br />
                Choose a picture box and click on <b>invite</b> to start getting recommended.

                
    <div class="atfoot"><a onclick="return CloseLightBox()" href="javascript:void(0)">No, thank you, I will do this later.</a></div>
                
                </div>
            </div>
            
            <div id="lightboxinvite2" class="lightboxinvite">
                <div class="invite_wrap">
                    <div class="invite_line">
                        Email: <input id="email_name" class="text_invite font" />
                    </div>
                    <div class="invite_line">
                        Subject: <input id="email_title" class="text_invite font" value="Appreciate your recommendation"/>
                    </div>
                    <div class="invite_line">
                        Comment: <textarea id="email_content" class="area_invite font"></textarea>
                    </div>
                
                    <div class="me_apply"><a  href="javascript:void(0)" id="invite_recommend"><img src="/Styles/images/icon_apply.gif"></a></div>
                    <div class="me_close"><a onclick="return CloseInvite()" href="#"><img src="/Styles/images/icon_close.gif"></a></div> 
                </div>
            </div>

        </div>
        <input type="hidden" id="deleteRecommend" />

        <script type="text/javascript">

            function Invite() {
                var div1 = document.getElementById('lightboxinvite');
                var div2 = document.getElementById('lightboxinvite2');
                if (div1) div1.style.display = 'none';
                if (div2) div2.style.display = 'block';

                return false;
            }
            function OpenInvite() {
                var div1 = document.getElementById('lightboxinvite');
                var div2 = document.getElementById('lightboxinvite2');
                if (div1) div1.style.display = 'block';
                if (div2) div2.style.display = 'none';

                return false;
            }
            function CloseInvite() {
                var div1 = document.getElementById('lightboxinvite');
                var div2 = document.getElementById('lightboxinvite2');
                if (div1) div1.style.display = 'none';
                if (div2) div2.style.display = 'none';

                return false;
            }

            $(document).ready(function () {
                var ec = $("#divInviteMessage").html().replace(/<[^>]*>/g, "").replace(/\t/g, "");
                $("#email_content").val(ec);

                var div = document.getElementById('lightboxinvite');
                if (div) div.style.display = 'block';
                $(".RecommendDelete").click(function () {
                    var lineId = parseInt($(this).attr("x:id"))+1;
                    var className = $(this).parent().attr("class");
                    var recommendid = $(this).attr("x:recommendid");
                    var divIndex = 0;
                    divIndex = $(this).attr("x:id");
                    if (divIndex > 3)
                        divIndex = parseInt(divIndex) + 1;
                    var lineIndex = 1;
                    if (divIndex == 0 || divIndex == 8) {
                        lineIndex = 1;
                    }
                    else if (divIndex == 1 || divIndex == 7) {
                        lineIndex = 2;
                    }
                    else if (divIndex == 2 || divIndex == 6) {
                        lineIndex = 3;
                    }
                    else if (divIndex == 3 || divIndex == 5) {
                        lineIndex = 4;
                    }
                    var divBox = $("#recommend_list>div:eq(" + divIndex + ")");
                    var boxHtml = "<div class=\"r_user_invite\" onclick=\"return Invite();\">Invite</div>";
                    var lineHtml = "<a onclick=\"return Invite();\" href=\"javascript:void(0)\"><img src=\"/Styles/images/invite_add" + lineIndex + ".gif\"></a>";

                    var newID = $("#deleteRecommend").val() + recommendid + ";";
                    confirmbox("Do you want to disconnect this user?", function () {
                        divBox.removeClass();
                        divBox.addClass("r_user_none");
                        divBox.html(boxHtml);
                        $('.line' + lineId).html(lineHtml);
                        $("#deleteRecommend").val(newID);
                        $.postJSON('<%=Url.Action("DeleteRecommend") %>', { recommendid: $('#deleteRecommend').val() }, function (data) {
                        });
                    });
                });
                $("#invite_recommend").click(function () {
                    if (ValidateRecommend() == false)
                        return;
                    $.postJSON('<%=Url.Action("InviteRecommend") %>', { emailname: $("#email_name").val(), title: $("#email_title").val(), content: $("#email_content").val(), userid: "<%=userid %>" }, function (data) {
                        if (data == "True") {
                            confirmbox2("Your message was successfully sent!<br>Do you want to continue building recommendations?", "OK", "LATER", function () {
                                OpenInvite();
                                $("#email_name").val("");
                            }, function () {
                                CloseInvite();
                                CloseLightBox();
                            });
                        }
                        else {
                            alertbox(data);
                        }
                    });
                });
                $("#btnSubmit").click(function () {
                    $.postJSON('<%=Url.Action("DeleteRecommend") %>', { recommendid: $('#deleteRecommend').val() }, function (data) {
                        window.location.reload();
                    });
                });
            });


            function ValidateRecommend() {
                var error = new Array();
                if (isEmpty($("#email_name").val()) || !isEmail($("#email_name").val())) {
                    $("#email_name").addClass("input_error");
                    error.push("error")
                }
                else {
                    $("#email_name").removeClass("input_error");
                }

                if (isEmpty($("#email_title").val())) {
                    $("#email_title").addClass("input_error");
                    error.push("error")
                }
                else {
                    $("#email_title").removeClass("input_error");
                }

                if (isEmpty($("#email_content").val())) {
                    $("#email_content").addClass("input_error");
                    error.push("error")
                }
                else {
                    $("#email_content").removeClass("input_error");
                }

                if (error.length > 0)
                    return false;
                else
                    return true;
            }
</script>