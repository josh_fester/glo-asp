﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<GLO.Data.Models.GLO_HRExpert>>" %>
	<div class="content_list">
        <div class="list_item">
            <div class="item_title">HR Experts</div>
            <%--<%foreach (var expert in Model)
                { %>
                <div class="item_user">
                    <div class="user_icon"><a href="/Expert/MyValue?expertid=<%=expert.UserID %>" target="_blank" title="<%=expert.NickName %>"><img src="/images/<%=!String.IsNullOrEmpty(expert.LogoUrl)?expert.LogoUrl:"user_none.gif" %>" /></a></div>
                    <div class="user_name"><a href="/Expert/MyValue?expertid=<%=expert.UserID %>" target="_blank" title="<%=expert.NickName %>"><%=expert.NickName.Length > 30 ? expert.NickName.Substring(0, 28) + "..." : expert.NickName%></a></div>
                </div>
            <%} %>--%>            
                
                    <div class="item_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=526" target="_blank" title="Dasun Kim"><img src="/images/home_user4.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=526" target="_blank" title="Dasun Kim">Dasun Kim</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Raf Adams"><img src="/images/home_user9.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Raf Adams">Raf Adams</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Eliot Shin"><img src="/images/home_user11.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Eliot Shin">Eliot Shin</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Mike Thompson"><img src="/images/home_user3.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" target="_blank" title="Mike Thompson">Mike Thompson</a></div>
                    </div>

        </div>
        <div class="list_link">
            <%:Html.ActionLink("Create your expert profile", "RegisterLegal", new { type=2})%>
        </div>
        <div class="list_view">
            <a href="/Sample/ExpertValue" target="_blank">View a sample</a>
        </div>
 </div>
