﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Users>>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_selectuser">
    <div class="selectuser_box">
            <div class="selectuser_header">
                <div id="headcompany" class="selectuser_item item_active">Company</div>
                <div id="headleader" class="selectuser_item item_middle">Leader</div>
                <div id="headexpert" class="selectuser_item">Expert</div>
            </div>
            <div id="divcompany" class="selectuser_container">
                    <table id="tbcompany">
                        <tr>
                            <td width="130" class="tdheader">
                                Company Search
                            </td>
                            <td class="tdheader">
                                <input type="text" id="txtCompany" /><input type="button" value="Search" onclick="SearchCompany()" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Company
                            </td>
                            <td id="CompanyUsers">
                                <div class="selectuser_details">
                                <%foreach (var user in Model.Where(x => x.TypeID == 3).ToList())
                                  { %>
                                        <div class="selectuser_list">
                                            <div class="selectuser_icon"><img src="/images/<%=!String.IsNullOrEmpty(user.Icon)?user.Icon:"user_none.gif" %>" /><input type="checkbox" name="name" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" /></div>
                                            <div class="selectuser_name" title="<%=user.NickName%>"><%=user.NickName%></div>
                                            <div class="selectuser_name" title="<%=user.GLO_Company==null?"":user.GLO_Company.Industry%>"><%= user.GLO_Company==null?"":user.GLO_Company.Industry%></div>
                                            <div class="selectuser_name" title="<%=user.GLO_Company==null?"":user.GLO_Company.Website%>"><%= user.GLO_Company==null?"":user.GLO_Company.Website%></div>
                                        </div>
                                <%} %>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
            <div id="divleader" class="selectuser_container" style="display:none">
                    <table id="tbleader">
                        <tr>
                            <td width="130" class="tdheader">
                                Leader Search
                            </td>
                            <td class="tdheader">
                                <input type="text" id="txtLeader" /><input type="button" value="Search" onclick="SearchLeader()" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Leader
                            </td>
                            <td id="LeaderUsers">
                                <div class="selectuser_details">
                                <%foreach (var user in Model.Where(x => x.TypeID == 1).ToList())
                                  { %>
                                        <div class="selectuser_list">
                                            <div class="selectuser_icon"><img src="/images/<%=!String.IsNullOrEmpty(user.Icon)?user.Icon:"user_none.gif" %>" /><input type="checkbox" name="name" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" /></div>
                                            <div class="selectuser_name" title="<%=user.NickName%>"><%=user.NickName%></div>
                                            <div class="selectuser_name" title="<%=user.GLO_Leader.Title%>"><%=user.GLO_Leader.Title %></div>
                                            <div class="selectuser_name" title="<%=user.GLO_Leader.Location%>"><%=user.GLO_Leader.Location %></div>
                                        </div>
                                <%} %>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
            <div id="divexpert" class="selectuser_container" style="display:none">
                    <table id="tbexpert">
                        <tr>
                            <td width="130" class="tdheader">
                                Expert Search
                            </td>
                            <td class="tdheader">
                                <input type="text" id="txtExpert" /><input type="button" value="Search" onclick="SearchExpert()" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Expert
                            </td>
                            <td id="ExpertUsers">
                                <div class="selectuser_details">
                                <%foreach (var user in Model.Where(x => x.TypeID == 2).ToList())
                                  { %>
                                        <div class="selectuser_list">
                                            <div class="selectuser_icon"><img src="/images/<%=!String.IsNullOrEmpty(user.Icon)?user.Icon:"user_none.gif" %>" /><input type="checkbox" name="name" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" /></div>
                                            <div class="selectuser_name" title="<%=user.NickName%>"><%=user.NickName%></div>
                                            <div class="selectuser_name" title="<%=user.GLO_HRExpert.Title%>"><%=user.GLO_HRExpert.Title %></div>
                                            <div class="selectuser_name" title="<%=user.GLO_HRExpert.Location%>"><%=user.GLO_HRExpert.Location%></div>
                                        </div>
                                <%} %>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
        <div class="me_apply">
            <a href="javascript:void(0)" id="btnValue">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<div id="hidUsers">
    <%foreach (var user in Model.Where(x => x.TypeID == 3).ToList())
      { %>
    <input type="hidden" name="CompanyUser" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" x:icon="<%=user.Icon %>"  x:title="<%= user.GLO_Company==null?"":user.GLO_Company.Industry%>"  x:location="<%= user.GLO_Company==null?"":user.GLO_Company.Website%>"  />
    <%} %>
    <%foreach (var user in Model.Where(x => x.TypeID == 1).ToList())
      { %>
    <input type="hidden" name="LeaderUser" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" x:icon="<%=user.Icon %>"   x:title="<%=user.GLO_Leader.Title %>"  x:location="<%=user.GLO_Leader.Location %>" />
    <%} %>
    <%foreach (var user in Model.Where(x => x.TypeID == 2).ToList())
      { %>
    <input type="hidden" name="ExpertUser" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" x:icon="<%=user.Icon %>"   x:title="<%=user.GLO_HRExpert.Title %>"  x:location="<%=user.GLO_HRExpert.Location %>" />
    <%} %>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnValue").click(function () {
            var idStr = "";
            var nameStr = "";
            var html = $('#divReceive').html();
            $(':checkbox').each(function (i) {
                if ($(this).attr("checked") == "checked") {
                    if ($('#divReceive').find("#" + $(this).attr('x:userid')).length == 0) {
                        idStr += $(this).attr('x:userid') + ";";
                        nameStr += $(this).attr("x:username") + ";";
                        html += "<div class='value_item_active' id=\"" + $(this).attr('x:userid') + "\">" + $(this).attr("x:username") + "<div class=\"user_value_delete\"><a href=\"javascript:void(0);\" title=\"delete\" id=\"" + $(this).attr('x:userid') + "\"  onclick=\"DeleteUser(this);\"><img src=\"/Styles/images/icon_cancel.gif\" /></a></div></div>";
                    }
                }
            });
            if (html != "") {
                $("#divReceive").html(html);
            }
            CloseLightBox();
        });

        $("#headcompany").click(function () {
            $(".selectuser_item").removeClass("item_active");
            $(this).addClass("item_active");
            $("#divcompany").show();
            $("#divleader").hide();
            $("#divexpert").hide();
        });
        $("#headleader").click(function () {
            $(".selectuser_item").removeClass("item_active");
            $(this).addClass("item_active");
            $("#divcompany").hide();
            $("#divleader").show();
            $("#divexpert").hide();
        });
        $("#headexpert").click(function () {
            $(".selectuser_item").removeClass("item_active");
            $(this).addClass("item_active");
            $("#divcompany").hide();
            $("#divleader").hide();
            $("#divexpert").show();
        });
    });
    function SearchCompany() {
        var searchValue = $('#txtCompany').val().trim().toLocaleLowerCase();
        var newHtml = "";
        if (searchValue == "") {
            $("input[name='CompanyUser']").each(function () {
                newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
            });
        }
        else {
            $("input[name='CompanyUser']").each(function () {
                if ($(this).attr("x:username").toLocaleLowerCase().indexOf(searchValue) >= 0) {
                    newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
                }
            });
        }
        if (newHtml != "") {
            $('#CompanyUsers').empty().html(newHtml);
        }
        else {
            $('#CompanyUsers').empty().html("No match record.");
        }
    }
    function SearchLeader() {
        var searchValue = $('#txtLeader').val().trim().toLocaleLowerCase();
        var newHtml = "";
        if (searchValue == "") {
            $("input[name='LeaderUser']").each(function () {
                newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
            });
        }
        else {
            $("input[name='LeaderUser']").each(function () {
                if ($(this).attr("x:username").toLocaleLowerCase().indexOf(searchValue) >= 0) {
                    newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
                }
            });
        }
        if (newHtml != "") {
            $('#LeaderUsers').empty().html(newHtml);
        }
        else {
            $('#LeaderUsers').empty().html("No match record.");
        }
    }
    function SearchExpert() {
        var searchValue = $('#txtExpert').val().trim().toLocaleLowerCase();
        var newHtml = "";
        if (searchValue == "") {
            $("input[name='ExpertUser']").each(function () {
                newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
            });
        }
        else {
            $("input[name='ExpertUser']").each(function () {
                if ($(this).attr("x:username").toLocaleLowerCase().indexOf(searchValue) >= 0) {
                    newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
                }
            });
        }
        if (newHtml != "") {
            $('#ExpertUsers').empty().html(newHtml);
        }
        else {
            $('#ExpertUsers').empty().html("No match record.");
        }
    }
    function BuildHtml(userid, username, icon,title,location) {
        return "<div class=\"selectuser_list\"><div class=\"selectuser_icon\"><img src=\"/images/" + icon + "\" /><input type=\"checkbox\" name=\"name\" x:userid=\"" + userid + "\" x:username=\"" + username + "\" /></div><div class=\"selectuser_name\">" + username + "</div><div class=\"selectuser_name\">" + title + "</div><div class=\"selectuser_name\">" + location + "</div></div>";
    }
</script>
