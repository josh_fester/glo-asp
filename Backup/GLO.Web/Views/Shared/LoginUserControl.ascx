﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
 <div class="glo_toolbar">
    <span>Version: Beta</span>
     <a href="/">HOME</a>
    <a href="/Home/Concept" >THE GLO CONCEPT</a>
    <a href="/Home/Success" >GLO SUCCESSES</a>
    <a href="/Insight" >INSIGHTS</a>
    <a href="/Home/Statistics" >STATISTICS</a>
    <a href="/Home/FAQ" >FAQ</a>
    <a href="/Home/GLODifference" >GLO DIFFERENCE</a>
    <a href="/Home/Policy" >POLICY</a>
    <a href="/Home/FeedBack" >FEEDBACK</a>
    <a href="/Home/Partner" >PARTNERS</a>
    <a href="/Home/Contact" >CONTACT US</a>    
 </div>
 <script>
     (function (i, s, o, g, r, a, m) {
         i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
             (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
     ga('create', 'UA-42966152-1', 'glo-china.com');
     ga('send', 'pageview');
</script>
 
<div class="glo_header">
    <div class="header_logo">
        <a href="/home/index"><img src="/Styles/images/logo.gif" ></a> 
    </div>
  <% if (Request.IsAuthenticated)
     {
         var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
         var iconurl = String.Empty;
         var emailurl = String.Empty;
         if (user.UserData.UserType == 1)
         {
             iconurl = Url.Action("Index", "Leader", new { leaderid = user.UserData.UserID });
             emailurl = Url.Action("LeaderMessage", "Message", new { leaderid = user.UserData.UserID, status = 0 });
         }
         else if (user.UserData.UserType == 2)
         {
             iconurl = Url.Action("MyValue", "Expert", new { expertid = user.UserData.UserID });
             emailurl = Url.Action("Message", "Message", new { expertid = user.UserData.UserID, status = 0 });
         }
         else if (user.UserData.UserType == 3)
         {
             emailurl = Url.Action("CompanyMessage", "Message", new { companyid = user.UserData.UserID, status = 0 });
             iconurl = Url.Action("CompanyIndex", "Company", new { companyid = user.UserData.UserID });
         }
         else
         {
             iconurl = Url.Action("Index", "Admin");
             emailurl = Url.Action("Index", "Admin");
         }
         
         %>
    	<div class="header_logout">
             <%: Html.ActionLink("Log Out", "SignOut", "Account")%>
        </div>
    	<div class="header_message">
        	<a href="<%=emailurl %>"><img src="/Styles/images/icon_message.gif" /></a>
            <div class="message_number"><%if (Session["UnReadMessageCount"] == null)
                                          { %>0<%}
                                          else
                                          { %><%=Session["UnReadMessageCount"].ToString()%><%} %></div>
        </div>
    	<div class="header_user">
        	<a href="<%=iconurl %>"><img width="30px" height="30px" src="/Images/<%=!string.IsNullOrEmpty(user.UserData.LogoUrl)?user.UserData.LogoUrl:"user_none.gif" %>" />My Profile<%--<%= user.UserData.UserName%>--%></a>
        </div>
        <div class="search_box">
            
            <div class="dropdown_box">
            <%string stype = !string.IsNullOrEmpty(Request.QueryString["searchtype"]) ? Request.QueryString["searchtype"].ToString() : "Users";
              if (string.IsNullOrEmpty(stype))
              {
                  stype = user.UserData.UserType == 1 ? "Jobs" : "Users";

              } %>
                <input class="dropdown_value" id="SearchSelectType" type="text" readonly="readonly" value='<%=stype %>' />
                <img class="dropdown_arrow" src="/Styles/images/icon_arrow2.gif" />
            	<div class="dropdown_option">
                    <ul>
                        <li>Jobs</li>
                        <li>Users</li>
                    </ul>
                </div>
            </div>

            <div class="search_right">
            
                <input class="search_text" type="text" id="textKeyword" value="<%=Request.QueryString["keyword"]!=null?Request.QueryString["keyword"].ToString():"" %>" />
                <a id="HpySearch" href="javascript:void(0);"><img src="/Styles/images/icon_search2.gif" /></a>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                /*open dropdown*/
                $(".dropdown_box").click(function (event) {
                    event.stopPropagation();
                    $(".dropdown_option").hide();
                    $(this).find(".dropdown_option").toggle();
                });
                /*close dropdown*/
                $(document).click(function () {
                    $('.dropdown_option').hide();
                });
                /*set value*/
                $(".dropdown_option li").click(function (event) {
                    event.stopPropagation();
                    $(".dropdown_option").hide();

                    var value = $(this).text();
                    $(this).parent().parent().parent().find(".dropdown_value").val(value);
                });

                $("#HpySearch").click(function () {
                    var type = $(".dropdown_value").val();
                    var value = $("#textKeyword").val();
                    if (value == "") {
                        return false;
                    }
                    else {
                        if (type == "Jobs") {
                            window.location.href = '<%=Url.Action("JobSearch","Home") %>' + "?keyword=" + value + "&searchtype=" + type;
                        }
                        else {
                            window.location.href = '<%=Url.Action("HeaderSearch","Home") %>' + "?c=" + value + "&e=" + value + "&l=" + value + "&searchtype=" + type;
                        }
                        return false;

                    }
                });

                $('#textKeyword').keypress(function (e) {
                    var e = e || window.event;
                    if (e.keyCode == 13) {
                        $("#HpySearch").click();
                    }
                });

                SetUnReadMessageCount();
            });
            function SetUnReadMessageCount() {
                $.post("/Home/SetUnreadMessage", null, function (data) {
                    if ($('#UnReadMessageCount').html() != null) {
                        $('#UnReadMessageCount').html(data)
                    }
                    $('.message_number').html(data)
                }, "json");
                setTimeout(function () {
                    SetUnReadMessageCount();
                }, 300000);
            }
        </script>
<%}%>

    </div>
