﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_CompanyJob>" %>
<% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
    if (!string.IsNullOrEmpty(Model.JobTitle))
  { %>
<table class="ganeral">      
  <tr>
    <td colspan="2" class="ganeral_title">
    <%if ((Model.IsActive == false || Model.IsDelete == true || Model.ExpireDate < DateTime.Now)&&user.UserData.UserType ==1)
      { %>
      <span class="expiredjob">The job ad has expired.</span>
      <br />
      <%} %>
    <%=Model.JobTitle %></td>
    </tr>
    <tr>
    <td>
       <table>
            <tr>
                <td>Company:</td>
                <td><a class="font" href="/company/FactSheet?companyid=<%=Model.GLO_Company.UserID %>" target="_blank"><%=Model.GLO_Company.CompanyName %></a></td>
            </tr>
            <tr>
                <td>Job ID:</td>
                <td><span class="gray font"><%=Model.JobNO %></span></td>
            </tr>
           
            <tr>
                <td>Location:</td>
                <td><span class="gray font"><%=Model.Location %></span></td>
            </tr>
            <tr>
                <td>Industry:</td>
                <td><span class="gray font"><%=Model.Industry %></span></td>
            </tr>
            <tr>
                <td>Function:</td>
                <td><span class="gray font"><%=Model.JobFunction %></span></td>
            </tr>
             <%if (!string.IsNullOrEmpty(Model.PostDate))
              { %>
            <tr>
                <td>Posted:</td>
                <td><span class="gray font"><%=Model.PostDate%></span></td>
            </tr>
            <%} %>
            <%if (!string.IsNullOrEmpty(Model.Salary))
              { %>
            <tr>
                <td>Salary:</td>
                <td><span class="gray font"><%=Model.Salary%></span></td>
            </tr>
            <%} %>
        </table>
    </td>
    <td width="200" class="ganeral_logo">
        <a href="/company/FactSheet?companyid=<%=Model.GLO_Company.UserID %>" target="_blank"><img src="/Images/<%=!string.IsNullOrEmpty(Model.GLO_Company.LogoUrl)? Model.GLO_Company.LogoUrl : "user_none.gif" %>" /></a>
    </td>
    </tr>
</table>
<table class="detail">
    <tr>
        <td>
                      
                        <%if (!string.IsNullOrEmpty(Model.JobSummary))
                          { %>
                        <div class="info_headline">Job Summary</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.JobSummary) ? Model.JobSummary.Replace("\n", "<br>") : ""%></div>
                        <%} %>

                        <div class="info_headline">Key Responsibilities</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.KeyResponsibility)? Model.KeyResponsibility.Replace("\n","<br>"):"" %></div>
                        <div class="info_headline">Key Competencies</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.KeyCompetence)? Model.KeyCompetence.Replace("\n","<br>"):"" %></div>

                        <%if (!string.IsNullOrEmpty(Model.Education))
                          { %>
                        <div class="info_headline">Education</div>
                        <div class="info_description font"><%=Model.Education%></div>
                        <%}
      if (!string.IsNullOrEmpty(Model.ContactPerson))
      { %>
                        <div class="info_headline">Contact Person</div>
                        <div class="info_description font"><%=Model.ContactPerson%></div>
                        <%} %>

                        <%if (!string.IsNullOrEmpty(Model.DefinedTitle) && !string.IsNullOrEmpty(Model.DefinedContent))
                          { %>
                            <div class="info_headline"><%=Model.DefinedTitle %></div>
                            <div class="info_description font"><%=!string.IsNullOrEmpty(Model.DefinedContent)? Model.DefinedContent.Replace("\n","<br>"):"" %></div>
                        <%} %>
        </td>
        <td>
        </td>
    </tr>
</table>


<% 
   if(user.UserData.UserType==3){ %>
 <div class="jobactive_box">
<%if(Model.IsActive && Model.IsPublish)
  { %>
        <input type="button" id="RemoveJob" x:jobid="<%=Model.JobID %>" class="bluebtn2" value="Remove" />
<%} %>
<%if(!Model.IsActive)
  { %>
        <input type="button" id="ReactivateJob" x:jobid="<%=Model.JobID %>" class="bluebtn2" value="Reactivate" />
<%} %>

<input type="button" id ="EditJob" x:jobid="<%=Model.JobID %>" class="bluebtn2" style="display:none" value="Edit" />
</div>
<%}else{ %>

<div class="jobactive_box">

<input type="button" id="ReMoveTag" class="bluebtn2" x:jobid="<%=Model.JobID %>" value="Remove" />
<%if (Model.IsActive == false || Model.IsDelete == true || Model.ExpireDate < DateTime.Now)
  { %>
        

<%}
  else
  { %>
        <input type="button" id="SubmitResume" class="bluebtn2" x:jobid="<%=Model.JobID %>" value="Submit Resume" />
<%}%>
  </div>
  <%}%>
  
<%} %>


<script type="text/javascript">
    $(document).ready(function () {
        if ($("#HidJob").val() == "Jobslot") {
            $("#EditJob").css("display", "inline");
            $("#RemoveJob").css("display", "none");
        }

        $("#RemoveJob").click(function () {
            var jobID = $(this).attr("x:jobid");
            confirmbox("Do you want to remove the job ad to Archive folder? If yes, the job ad cannot be searched and doesn't show in Jobs page.", function () {
                $.postJSONnoQS('<%=Url.Action("JobRemove","Company") %>', { "jobid": jobID }, function (data) {
                    window.location.reload();
                    return false;
                });
            });
        });
        $("#ReactivateJob").click(function () {
            var jobID = $(this).attr("x:jobid");
            confirmbox("Do you want to reactivate the job ad?", function () {
                $.postJSONnoQS('<%=Url.Action("ReActiveJob","Company") %>', { "jobid": jobID }, function (data) {
                    if (data) {
                        alertbox("The job ad has been reactivated successfully.", function () {
                            window.location.reload();
                            return false;
                        });

                    }
                    else {
                        alertbox("You have no available job slot. Please purchase job slots or upgrade your membership first.");
                        return false;
                    }
                });
            });
        });

        $("#EditJob").click(function () {
            var jobID = $(this).attr("x:jobid");
            $.getJSON('_EditJob', { jobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        $("#ReMoveTag").click(function () {
            var jobID = $(this).attr("x:jobid");
            confirmbox("Do you want to remove the job ad?", function () {
                $.postJSONnoQS('<%=Url.Action("RemoveTagJob","Leader") %>', { "jobid": jobID }, function (data) {
                    if (data) {
                        alertbox("The job ad has been removed successfully.", function () {
                            window.location.reload();
                            return false;
                        });
                    }
                });

            });
        });

        $("#SubmitResume").click(function () {
            $.getJSON('<%=Url.Action("_SubmitResume","Leader") %>', { jobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>
