﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Company>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_useredit">
        <div class="nav_company_edit nav_company_edit_name">Life At</div>
        <div class="box_useredit">
            <div class="company_info">
                <div class="company_name">
                    <input id="CompanyName" class="font" type="text" maxlength="50" value="<%=!String.IsNullOrEmpty(Model.CompanyName)?Model.CompanyName:"Company Name" %>" onfocus="if (this.value=='Company Name')this.value=''" onblur="if (this.value=='')this.value='Company Name'" />
                </div>
                <div class="ckeditor_wrap">
                    <div id="editor1">
                    </div>
                    <script type="text/javascript">
                         var config = {
                             height: 310,
                             toolbar: 'Basic2'
                         };
                         var data = "You can use  this space to make your company’s culture and values stand out by answering basically following questions:<br>";
                         data += "<ul><li>What defines ‘culture‘ at my organization ?</li>";
                         data += "<li>What values are important for us as a team?</li>";
                         data += "<li>What CSR actions do we engage in?</li></ul>";
                         data += "On the right side, you can upload team photos and memorable pictures of team events and CSR activities<br><br>";
                         data += "If you like to share more detailed info, you can upload a PPT, PDF or word document.";


                            <% if(!string.IsNullOrEmpty(Model.CompanyCulture)){ %>    
                             data ='<%=Model.CompanyCulture.Replace("\n","").Replace("\r","").Replace("'", "")%>';
                             <%} %>                         
                         var editor = CKEDITOR.appendTo('editor1', config, data);                                               
                    </script>
                </div>
            </div>
            <div class="company_caption">
                <div class="caption_item">
                    <div class="caption_image">
                        <img id="Image1" src="/Images/<%=!string.IsNullOrEmpty(Model.Image1)?Model.Image1:"icon_blank.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.Image1)?Model.Image1:"" %>" />
                    </div>
                    <div class="caption_item_edit">                      
                        <input type="button" id="file_upload1" class="file_upload" value="Select Files" />
                         <input type="button" id="file_delete1" class="btngray" value="Delete image"/>
                    </div>
                    <div class="caption_name">                    
                        <input id="Image1Name" class="font" type="text" value="<%=Model.Image1Name %>" maxlength="30" />
                    </div>
                </div>
                <div class="caption_item">
                    <div class="caption_image">
                        <img id="Image2" src="/Images/<%=!string.IsNullOrEmpty(Model.Image2)?Model.Image2:"icon_blank.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.Image2)?Model.Image2:"" %>" />
                     </div>
                        <div class="caption_item_edit">                      
                        <input type="button" id="file_upload2" class="file_upload" value="Select Files" />
                         <input type="button" id="file_delete2" class="btngray" value="Delete image"/>
                    </div>
                    <div class="caption_name">
                         <input id="Image2Name" class="font" type="text" value="<%=Model.Image2Name %>" maxlength="30" />
                    </div>
                </div>
                <div class="caption_item">
                    <div class="caption_image">
                        <img id="Image3" src="/Images/<%=!string.IsNullOrEmpty(Model.Image3)?Model.Image3:"icon_blank.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.Image3)?Model.Image3:"" %>" />
                    </div>
                    <div class="caption_item_edit">                      
                        <input type="button" id="file_upload3" class="file_upload" value="Select Files" />
                         <input type="button" id="file_delete3" class="btngray" value="Delete image"/>
                    </div>
                    <div class="caption_name">
                        <input id="Image3Name" class="font" type="text" value="<%=Model.Image3Name %>" maxlength="30" />
                    </div>
                </div>
                <div class="caption_item">
                    <div class="caption_image">
                        <img id="Image4" src="/Images/<%=!string.IsNullOrEmpty(Model.Image4)?Model.Image4:"icon_blank.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.Image4)?Model.Image4:"" %>" />
                    </div>
                    <div class="caption_item_edit">                      
                        <input type="button" id="file_upload4" class="file_upload" value="Select Files" />
                        <input type="button" id="file_delete4" class="btngray" value="Delete image"/>
                    </div>
                    <div class="caption_name">
                         <input id="Image4Name" class="font" type="text" value="<%=Model.Image4Name %>" maxlength="30" />
                    </div>
                </div>
                 <div class="upload_boxer">
                        <input type="button" id="ppt_upload" value="Upload PPT file" />
                        <span id="pptName"><%=Model.PPTFileName%></span>
                        <a href="javascript:void(0);" style="margin-top:0;" title="delete" id="<%=Model.UserID %>" onclick="DeletePPT(this)"><img src="/Styles/images/icon_delete1.gif" /></a>
                    </div>
            </div>
        </div>
        <div class="me_apply">
            <a href="javascript:void(0)" id="btnApply">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close2">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function () {
            $("#btnApply").click(function () {
                if (isEmpty($("#CompanyName").val())) {
                    $("#CompanyName").addClass("input_error");
                    return;
                }
                else {
                    $("#CompanyName").removeClass("input_error");
                }

                var companyculture = editor.getData();            
                  var jsondata ={
                         'UserID':<%=Model.UserID %>,
                         'CompanyName':$("#CompanyName").val(),
                         'CompanyCulture':escape(companyculture),
                         'Image1':$("#Image1").attr("x:name"),
                         'Image2':$("#Image2").attr("x:name"),
                         'Image3':$("#Image3").attr("x:name"),
                         'Image4':$("#Image4").attr("x:name"),
                         'Image1Name':$("#Image1Name").val(),
                         'Image2Name':$("#Image2Name").val(),
                         'Image3Name':$("#Image3Name").val(),
                         'Image4Name':$("#Image4Name").val(),
                         'PPTFileName': $("#pptName").html()
                        
                   };
                $.postJSONnoQS('<%=Url.Action("CompanyInfoEdit") %>', { "jsondata": $.toJSON(jsondata) }, function (data) {
                    
                    if (data == "2") {
                        window.location.href = "/Company/FactSheet?companyid=" + <%=Model.UserID %>;
                    }
                    else if (data == "3") {
                        window.location.href = "/Company/RegisterThankYou?companyid=" + <%=Model.UserID %>;
                    }
                    else {
                        window.location.reload();
                        return false;
                    }
                });
            });
            
                $('#file_upload1').uploadify({
                    'height': 25,
                    'width': 90,
                    'auto': true,
                    'multi': false,
                    'buttonText': 'Select Photo',
                    'fileTypeExts': '*.gif; *.jpg; *.png',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/FileUpload/Upload?width=300&height=200',
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);
                        $("#Image1").removeAttr("src");
                        $("#Image1").removeAttr("x:name");
                        $("#Image1").attr("src",data.Urlpath);
                        $("#Image1").attr("x:name", data.SaveName); 
                        
                        $(".uploadify-queue").css('display','none');
                        $('#file_upload1').css('display','none');
                        $('#file_delete1').css('display','');
                    }
                });
                $('#file_delete1').click(function(){
                    $("#Image1").removeAttr("src");
                    $("#Image1").removeAttr("x:name");
                    $("#Image1").attr("src","/images/icon_blank.gif");
                    $("#Image1").attr("x:name", ""); 

                    $('#file_upload1').css('display','');
                    $('#file_delete1').css('display','none');
                });
                 
                var logoUrl1='<%=Model.Image1 %>';
                if (logoUrl1 != "" && logoUrl1.indexOf("icon_blank.gif") == -1)
                {
                    $('#file_upload1').css('display','none');
                    $('#file_delete1').css('display','');
                }
                else{
                    $('#file_upload1').css('display','');
                    $('#file_delete1').css('display','none');
                }
                     
                $('#file_upload2').uploadify({
                    'height': 25,
                    'width': 90,
                    'auto': true,
                    'multi': false,
                    'buttonText': 'Select Photo',
                    'fileTypeExts': '*.gif; *.jpg; *.png',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/FileUpload/Upload?width=300&height=200',
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);
                        $("#Image2").removeAttr("src");
                        $("#Image2").removeAttr("x:name");
                        $("#Image2").attr("src",data.Urlpath);
                        $("#Image2").attr("x:name", data.SaveName); 
                        
                        $(".uploadify-queue").css('display','none');
                        $('#file_upload2').css('display','none');
                        $('#file_delete2').css('display','');
                    }
                });
                $('#file_delete2').click(function(){
                    $("#Image2").removeAttr("src");
                    $("#Image2").removeAttr("x:name");
                    $("#Image2").attr("src","/images/icon_blank.gif");
                    $("#Image2").attr("x:name", ""); 

                    $('#file_upload2').css('display','');
                    $('#file_delete2').css('display','none');
                });
                 
                var logoUrl2='<%=Model.Image2 %>';
                if (logoUrl2 != "" && logoUrl2.indexOf("icon_blank.gif") == -1)
                {
                    $('#file_upload2').css('display','none');
                    $('#file_delete2').css('display','');
                }
                else{
                    $('#file_upload2').css('display','');
                    $('#file_delete2').css('display','none');
                }
                
                $('#file_upload3').uploadify({
                    'height': 25,
                    'width': 90,
                    'auto': true,
                    'multi': false,
                    'buttonText': 'Select Photo',
                    'fileTypeExts': '*.gif; *.jpg; *.png',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/FileUpload/Upload?width=300&height=200',
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);
                        $("#Image3").removeAttr("src");
                        $("#Image3").removeAttr("x:name");
                        $("#Image3").attr("src",data.Urlpath);
                        $("#Image3").attr("x:name", data.SaveName); 
                        
                        $(".uploadify-queue").css('display','none');
                        $('#file_upload3').css('display','none');
                        $('#file_delete3').css('display','');
                    }
                });
                $('#file_delete3').click(function(){
                    $("#Image3").removeAttr("src");
                    $("#Image3").removeAttr("x:name");
                    $("#Image3").attr("src","/images/icon_blank.gif");
                    $("#Image3").attr("x:name", ""); 

                    $('#file_upload3').css('display','');
                    $('#file_delete3').css('display','none');
                });
                 
                var logoUrl3='<%=Model.Image3 %>';
                if (logoUrl3 != "" && logoUrl3.indexOf("icon_blank.gif") == -1)
                {
                    $('#file_upload3').css('display','none');
                    $('#file_delete3').css('display','');
                }
                else{
                    $('#file_upload3').css('display','');
                    $('#file_delete3').css('display','none');
                }
                
                $('#file_upload4').uploadify({
                    'height': 25,
                    'width': 90,
                    'auto': true,
                    'multi': false,
                    'buttonText': 'Select Photo',
                    'fileTypeExts': '*.gif; *.jpg; *.png',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/FileUpload/Upload?width=300&height=200',
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);
                        $("#Image4").removeAttr("src");
                        $("#Image4").removeAttr("x:name");
                        $("#Image4").attr("src",data.Urlpath);
                        $("#Image4").attr("x:name", data.SaveName); 
                        
                        $(".uploadify-queue").css('display','none');
                        $('#file_upload4').css('display','none');
                        $('#file_delete4').css('display','');
                    }
                });
                $('#file_delete4').click(function(){
                    $("#Image4").removeAttr("src");
                    $("#Image4").removeAttr("x:name");
                    $("#Image4").attr("src","/images/icon_blank.gif");
                    $("#Image4").attr("x:name", ""); 

                    $('#file_upload4').css('display','');
                    $('#file_delete4').css('display','none');
                });
                 
                var logoUrl4='<%=Model.Image4 %>';
                if (logoUrl4 != "" && logoUrl4.indexOf("icon_blank.gif") == -1)
                {
                    $('#file_upload4').css('display','none');
                    $('#file_delete4').css('display','');
                }
                else{
                    $('#file_upload4').css('display','');
                    $('#file_delete4').css('display','none');
                }

                if($('#pptName').html()=="")
                {
                    $('.upload_boxer').find('a').css("display","none");
                }
                else
                {
                    $('.upload_boxer').find('a').css("display","");
                }

                $('#ppt_upload').uploadify({
                        'height': 25,
                        'width': 120,
                        'auto': true,
                        'multi': false,
                        'buttonText': 'Upload PPT/PDF file',
                        'fileTypeExts': '*.pptx;*.ppt;*.pdf;',
                        'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                        'uploader': '/Home/UploadPPT?id='+<%=Model.UserID %>,
                        'onUploadSuccess': function (file, data, response) {
                            eval("data=" + data);
                            if (data.Success) {
                                $('#pptName').html(data.SaveName);
                                $('.upload_boxer').find('a').css("display","");
                            }
                            else {
                                alertbox(data.Message)
                            }
                        }
                    });

        });

         function DeletePPT(obj) {
            confirmbox("Do you want to delete the file?", function () {     
                $('#pptName').html("");
                $('.upload_boxer').find('a').css("display","none");        
            });
        }
</script>
