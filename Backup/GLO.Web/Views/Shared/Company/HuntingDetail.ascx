﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HuntingRequest>" %>
<table class="ganeral ganeral2">
    <tr>
        <td colspan="2" class="ganeral_title">
            <%=Model.SubJect %>
        </td>
    </tr>
    <tr>
        <td width="200">
            Job Title:
        </td>
        <td class="gray font">
           <%=Model.JobTitle %>
        </td>
    </tr>
    <tr>
        <td>
            Location:
        </td>
        <td class="gray font">
            <%=Model.Location %>
        </td>
    </tr>
    <tr>
        <td>
            Responsible leader:
        </td>
        <td class="gray font">
            <%=Model.Company %>
        </td>
    </tr>
    <tr>
        <td>
            Executive search expert:
        </td>
        <td class="gray font">
            <%=Model.ContactPerson %>
        </td>
    </tr>
    <tr>
        <td>
            Start Date:
        </td>
        <td class="gray font">
              <%=Model.ContactDetail%>
        </td>
    </tr>
    <tr>
        <td>
            Notes:
        </td>
        <td class="gray font">
             <%=Model.Comment %>
        </td>
    </tr>
</table>
<div class="jobactive_box">
<%if(Model.IsArchive)
  { %>
        <input type="button" id="ReactivateHunting" x:huntingid="<%=Model.HuntingID %>" class="bluebtn" value="Reactivate" />
<%} else { %>
        <input type="button" id="RemoveHunting" x:huntingid="<%=Model.HuntingID %>" class="bluebtn" value="Remove" />
<%} %>

<input type="button" id ="EditHunting" x:huntingid="<%=Model.HuntingID %>" class="bluebtn" value="Edit" />
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#EditHunting").click(function () {
            var huntingid = $(this).attr("x:huntingid");
            var companyid = <%=Model.UserID %>;
                $.getJSON('_HuntingEdit', { "huntingid": huntingid, "companyid": companyid, random: Math.random() }, function (data) {
                        $('#lightboxwrapper').empty().html(data.Html);
                        $("#lightboxwrapper").attr("style", "display:block");
                });          
        });

         $("#RemoveHunting").click(function () {
            var huntingid = $(this).attr("x:huntingid");
            confirmbox("Do you want to remove the headhunting program to Achive folder?", function () {
               
                $.postJSONnoQS('<%=Url.Action("HuntingArchive") %>', { "huntingid": huntingid }, function (data) {
                    window.location.reload();
                    return false;
                });

            });                
        });

         $("#ReactivateHunting").click(function () {
            var huntingid = $(this).attr("x:huntingid");
            var huntingID = '<%=Model.HuntingID %>';
            var subject = '<%=Model.SubJect %>';
            var location = '<%=Model.Location %>';
            var contactDetail = '<%=Model.ContactDetail %>';
            var newHtml = "<td width=\"148\"><a class=\"huntingtitle\" x:huntingid=\""+ huntingID +"\" href=\"javascript:void(0)\">"+subject+"</a></td><td width=\"147\">"+location+"</td><td width=\"130\">"+contactDetail+"</td>"
            confirmbox("Do you want to reactivate the headhunting program?", function () {
             $("#Hunting"+huntingid).find("td:eq(3)").remove();
             $("#myTable").append("<tr  id=\"ActiveHunting"+huntingID+"\">"+newHtml+"</tr>");
             $("#Hunting"+huntingid).remove();
                $.postJSONnoQS('<%=Url.Action("ReActiveHunting") %>', { "huntingid": huntingid }, function (data) {
                    //window.location.reload();
                    $("#ArchiveTitle").click();
                    return false;
                });
            });                
        });
    
    });

</script>