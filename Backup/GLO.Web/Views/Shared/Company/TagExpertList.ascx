﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<GLO.Data.Models.GLO_TagUsers>>" %>

<%foreach (var taguser in Model)
  {
      if (taguser.GLO_Users1.TypeID == 2)
      { %>
<div class="select_info_line">
<div class="select_info_icon">
    <a href="/Expert/MyValue?expertid=<%=taguser.TaggedUserID %>" target="_blank">
        <img src="/Images/<%=taguser.GLO_Users1.Icon %>" /></a>
    <div class="select_info_name">    
        <%=taguser.GLO_Users1.NickName%></div>
</div>
    <div class="select_info_button">
        <input type="button" class="btn_select_info movetaguser" x:tagid="<%=taguser.TagID %>" value="Move" />
        <input type="button" x:tagid="<%=taguser.TagID %>" class="btn_select_info delete_tag" value="Delete" />
        <input type="button" class="btn_select_info messagesend" value="Message" x:userid="<%=taguser.TaggedUserID %>" />
    </div>
<div class="select_info_user">
    <div class="select_info_item">
        <%=taguser.GLO_Users1.GLO_HRExpert.NickName%>, <%=taguser.GLO_Users1.GLO_HRExpert.Title%>, <%=taguser.GLO_Users1.GLO_HRExpert.Location%>
    </div>
    <%if (!string.IsNullOrEmpty(taguser.TagNote)){ %>
        <div class="select_info_item">
            <%=taguser.TagNote%>
        </div>
    <%} %>
</div>
</div>
<%}
  } %>
<script type="text/javascript">
    $(document).ready(function () {
        /*open dropdown*/
        $(".delete_tag").click(function () {
            var tagID = $(this).attr("x:tagid");
            var object = $(this);
           
            confirmbox("Do you want to delete this user from your tag list", function () {
               
                $.postJSON('<%=Url.Action("DeleteTagUser") %>', { tagid: tagID }, function (data) {
                });
                $(object).parent().parent().parent().remove();
                return false;
            });
        });

        $(".messagesend").click(function () {
            $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: $(this).attr("x:userid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        $(".movetaguser").click(function () {
            $.getJSON('<%=Url.Action("_MoveTagUser","Tag") %>', { tagID: $(this).attr("x:tagid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>

