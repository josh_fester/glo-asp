﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_build">
    You can now complete your profile which includes:<br /><br />
        <ul>
            <li>How you add value to GLO’s community of leaders and companies (click on edit and add any PPT, photo or documents)</li>
            <li>Your credentials and experience</li>
            <li>Your specific competencies</li>
            <li>Your values and vision</li>
        </ul>
        <br />
After your membership is approved, you will be able to invite
former clients to recommend you and will gain access to your
dedicated dashboard to manage contacts and assignments 
        <br />
        <br />

        <div class="message_center"><a class="EditProfile" href="javascript:void(0);">Build my profile now!</a></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".EditProfile").click(function () {
            $("#editLeftInfo").click();
        });
    });
</script>
