﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div style="margin-left:20px;">
<div class="r_info_item" style="margin-top:5px;">Summary</div>
 <div class="r_info_content">
<textarea id="successsummary" class="success_summary"></textarea> 
</div>
 <div class="r_info_item">How GLO made a difference for me?</div>
 <div class="r_info_content">
 <textarea id="successdescript" class="success_difference"></textarea> 
 </div>
 <br />
 <input type="button" id="successadd" value="Submit" class="bluebtn2" />
 </div>
     <script type="text/javascript">
         $(document).ready(function () {
             $("#successadd").click(function () {
                 var recommend = $("#successdescript").val();
                 var summary = $("#successsummary").val();
                 var recommendData = {
                     'UserID': 0,
                     'Name': summary,
                     'Description': recommend,
                     'CreateDate': ""
                 };
                 if (summary == "") {
                     alertbox("Please enter summary.");
                     return false;
                 }
                 if (recommend == "") {
                     alertbox("Please enter your recommend.");
                     return false;
                 }
                 else {
                     $.postJSON('<%=Url.Action("SuccessRecommendAdd") %>', { jsondata: $.toJSON(recommendData) }, function (data) {
                         if (data) {
                             alertbox("Thank you for submitting your recommend.<br>The GLO team will respond to you shortly.", function () {
                                 $("#successdescript").val("");
                                 $("#successsummary").val("");
                                 return false;
                             });
                         }
                         else
                             window.location.href = "/Account/Login";
                     });
                 }
             });
         });
        </script>