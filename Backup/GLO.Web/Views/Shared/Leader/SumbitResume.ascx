﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.ResumeModel>" %>
    <link href="<%=Url.Content("~/JS/uploadify/uploadify.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%=Url.Content("~/JS/uploadify/jquery.uploadify.min.js") %>" type="text/javascript"></script>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_message">
        <table>
        
            <tr>
            <td>
            Job name
            </td>
            <td>
           <%=Model.JobTitle%>  
            </td>
            </tr>
             <tr id="trCurrentResume" style="<%=Model.ResumeList.Count==0?"display:none":""%>">
            <td>
            Current resume
            </td>
            <td>
            <span id="ResumeList">
            <%foreach(var resume in Model.ResumeList){ %>
            <input type="checkbox" name="checkbox" class="resume_list" checked="checked" x:resumename="<%=resume.ResumeName %>" /> <%=resume.ResumeName %>;
            <%} %>
            </span>
            </td>
            </tr>

            <tr>
                <td>Upload resume</td>
                <td>
                <div class="select_box1 upload_boxer">
                    <input type="button" id="upload_resume" class="btn_search" value="Select File" />
                    <div id="uploaded" style="clear:both;float:left;display:none;">
                         <span id="pptName"></span>
                         <a href="javascript:void(0);" title="delete" onclick="DeletePPT(this)"><img src="/Styles/images/icon_delete1.gif" /></a>
                     </div>
                </div>
                </td>
            </tr>
            <tr>
                <td>Comment</td>
                <td><textarea class="content" id="Comments" ></textarea><input id="Function" type="hidden" value="" /></td>
            </tr>
        </table>
        <div class="me_apply">
            <a id="btnSubmit" href="javascript:void(0);" x:companyid="<%=Model.CompanyID %>">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
            $('#upload_resume').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select File',
                'fileTypeExts': '*.doc;*.docx;*.ppt;*.pptx;*.pdf;*.rtf',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/UploadPPT?id='+<%=Model.UserID %>,
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    if (data.Success) {
                        $('#pptName').html(data.SaveName);
                        $('#uploaded').css("display", "");
                    }
                    else {
                        alertbox(data.Message)
                    }
                }
            });


        $("#btnSubmit").click(function () {
                var resume ="";
                $("input[name='checkbox']:checkbox:checked").each(function(){
                resume+=$(this).attr("x:resumename")+";";
                }); 
             var resumelist = $('#pptName').html()+";"+resume;
             if(resumelist.length<=1)
             {
                alertbox("You should select at least one resume.");
                return;
             }
             $.postJSON('<%=Url.Action("SubmitResume","Leader") %>', { companyid:<%=Model.CompanyID %>,jobid:<%=Model.JobID %>,jobtitle:"<%=Model.JobTitle%>",userid:<%=Model.UserID %>,resumename:resumelist,comments:$("#Comments").val()}, function (data) {
                   alertbox("Your request has been sent successfully.",function(){
                        CloseLightBox();
                   });
             });
        });
    });

       function DeletePPT(obj) {
            confirmbox("Do you want to delete the file?", function () {     
                $('#pptName').html("");
                $('#uploaded').css("display","none");        
            });
        }
</script>