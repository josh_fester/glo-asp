﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Leader>" %>
<% var leaderid = Model.UserID; %>
<div class="lightbox_mask"></div>
<div class="lightbox_manage">
        <div class="lightbox_note">
            It only takes 20 minutes to answer a series of generic interview questions which will substantially enhance your profile strength.<br /><br />
            These interview questions are generated from a list of most frequently asked questions by any employer.<br /><br />
            You can select and answer up to 10 questions from the list

        </div>
    <div class="lightbox_box2">
        <div class="nav_leader_edit nav_leader_edit_interview">My Interview</div>
        <div class="box_expert_edit">
            <div class="interview_edit">
            
            <div class="select_box select_box_interview">
                <input class="select_value" id="QuestionTitle" type="text" readonly="readonly" value="Please select an interview question" />
                <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                <div class="select_option">
                    <ul>
                            <%
                            int i = 1;
                            var answerList = Model.GLO_LeaderInterviewAnswer;
                            foreach (var question in (List<GLO.Data.Models.GLO_LeaderInterViewQuestion>)ViewBag.QuestionList)
                            { 
                                bool showQuestion = true;
                                foreach (var item in answerList)
                                {
                                    if (item.QuestionID == question.QuestionID)
                                    {
                                        showQuestion = false;
                                    }
                                }
                             %>
                                <li x:id="<%=question.QuestionID %>" style="<%=showQuestion?"":"display:none"%>">Q<%=i %>. <%=question.QuestionTitle %></li>  
                            <%
                            i++;
                            } %>
                    </ul>
                </div>
            </div>
            <div style="float:left;"><input id="NewQuestion" type="button" value="Answer another question" class="bluebtn" /></div>  

            <div id="interviewErrMessage" style="display:none;clear:both;padding-top:5px;color:Red;">You just can select up to 10 questions.</div>
            <div style="clear:both;padding-top:20px;">
                <div id="divQuestionList">     
                    <%foreach (var item in answerList)
                      {
                          if (item.QuestionID.ToString() != "da726e1a-bb79-4f8d-bfb5-0be6a0d3faab")
                          {%>   
                            <div id="<%=item.QuestionID %>" class="interview_line intervieweditor"><span id="Title<%=item.QuestionID %>"><%=item.Question %></span><img src="/Styles/images/icon_delete1.gif" title="delete this question" style="cursor:pointer;" /><br /><textarea class="font" id="Answer<%=item.QuestionID %>" onfocus="if (this.value=='Enter text here')this.value=''" onblur="if (this.value=='')this.value='Enter text here'"><%if (string.IsNullOrEmpty(item.Answer)){ %>Enter text here<%}else{ %><%=item.Answer%><%} %></textarea></div>
                    <%}
                          else
                          {
                              List<string> answer = item.Answer.Split('-').ToList();
                              %>
                          <div id="<%=item.QuestionID %>" class="interview_line intervieweditor"><span id="Title<%=item.QuestionID %>"><%=item.Question %></span><img src="/Styles/images/icon_delete1.gif" title="delete this question" style="cursor:pointer;" /><br /><table class="font"><tr><td></td><td>Current time spent (%)</td><td>Would like to spend (%)</td></tr><tr><td>Family</td><td><input type="text" id="Family1" value="<%=answer[0] %>"/></td><td><input type="text" id="Family2" value="<%=answer[1] %>"/></td></tr><tr><td>Job</td><td><input type="text" id="Job1" value="<%=answer[2] %>"/></td><td><input type="text" id="Job2" value="<%=answer[3] %>"/></td></tr><tr><td>Mindfulness & meditation</td><td><input type="text" id="Mindfulness1" value="<%=answer[4] %>"/></td><td><input type="text" id="Mindfulness2" value="<%=answer[5] %>"/></td></tr><tr><td>Fitness</td><td><input type="text" id="Fitness1" value="<%=answer[6] %>"/></td><td><input type="text" id="Fitness2" value="<%=answer[7] %>"/></td></tr></table></div>
                    <%}
                      }%>    
                        
                </div>                
            </div>
            </div>
        </div>
        <div class="me_apply"><a id="btnSubmit" href="javascript:void(0);"><img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close2"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>    
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();

            var text = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(text);

            if ($('#divQuestionList').find(".interview_line").length > 9) {
                $('#interviewErrMessage').css('display', '');
            }
            else {
                var id = $(this).attr("x:id");
                var title = $('#QuestionTitle').val();

                var html = "<div id=\"" + id + "\" class=\"interview_line intervieweditor\"><span id=\"Title" + id + "\">" + title + "</span><img src=\"/Styles/images/icon_delete1.gif\" title=\"delete this question\" style=\"cursor:pointer;\" /><br /><textarea class=\"font\" id=\"Answer" + id + "\" onfocus=\"if (this.value=='Enter text here')this.value=''\" onblur=\"if (this.value=='')this.value='Enter text here'\">Enter text here</textarea></div>";
                if (id == "da726e1a-bb79-4f8d-bfb5-0be6a0d3faab") {
                    html = "<div id=\"" + id + "\" class=\"interview_line intervieweditor\"><span id=\"Title" + id + "\">" + title + "</span><img src=\"/Styles/images/icon_delete1.gif\" title=\"delete this question\" style=\"cursor:pointer;\" /><br /><table class=\"font\"><tr><td></td><td>Current time spent (%)</td><td>Would like to spend (%)</td></tr><tr><td>Family</td><td><input type=\"text\" id=\"Family1\"/></td><td><input type=\"text\" id=\"Family2\"/></td></tr><tr><td>Job</td><td><input type=\"text\" id=\"Job1\"/></td><td><input type=\"text\" id=\"Job2\"/></td></tr><tr><td>Mindfulness & meditation</td><td><input type=\"text\" id=\"Mindfulness1\"/></td><td><input type=\"text\" id=\"Mindfulness2\"/></td></tr><tr><td>Fitness</td><td><input type=\"text\" id=\"Fitness1\"/></td><td><input type=\"text\" id=\"Fitness2\"/></td></tr></table></div>";
                }
                $('#divQuestionList').append(html);
                $('#QuestionTitle').val("Please select an interview question");

                $(".select_option li").each(function () {
                    if ($(this).attr("x:id") == id) {
                        $(this).css("display", "none");
                    }
                });
            }
        });

        $(".intervieweditor img").live("click", function () {
            var removetext = $(this).parent().find("span").text();

            $(".select_option li").each(function () {
                if ($(this).text() == removetext) {
                    $(this).css("display", "");
                }
            });

            $(this).parent().remove();

            if ($('#divQuestionList').find(".interview_line").length < 10) {
                $('#interviewErrMessage').css('display', 'none');
            }
        });
         $(".intervieweditor textarea").live("keyup", function () {
           if ($(this).val().length > 1000) {
                var temp = $(this).val().substring(0, 1000);
                $(this).val(temp);
            }
         });

        $(".intervieweditor input").live("keyup", function () {
            var text = $(this).val().replace(/[^\d\.]/g, "");
            $(this).val(text);

            if ($(this).val().substring(0, 3) == "100") {
                $(this).val("100");
            }
            else if (parseInt($(this).val()) > 100) {
                var temp = $(this).val().substring(0, 2);
                $(this).val(temp);
            }
            else if ($(this).val().indexOf(".") > -1 && $(this).val().length - $(this).val().indexOf(".") > 5) {
                var temp = $(this).val().substring(0, $(this).val().indexOf(".") + 5);
                $(this).val(temp);
            }

        });

        $("#NewQuestion").click(function (event) {
            event.stopPropagation();
            $(".select_box_interview .select_option").toggle();
        });

        $("#btnSubmit").click(function () {
            var leaderid=<%=Model.UserID %>;
            var questionID = "";
            var questions = "";
            var answers = "";
            $(".intervieweditor").each(function () {
                if ($(this).attr("id") == "da726e1a-bb79-4f8d-bfb5-0be6a0d3faab" && ($("#Family1").val() != "" || $("#Family2").val() != "" || $("#Job1").val() != "" || $("#Job2").val() != "" || $("#Mindfulness1").val() != "" || $("#Mindfulness2").val() != "" || $("#Fitness1").val() != "" || $("#Fitness2").val() != "")) {
                    answers += $("#Family1").val() + "-" + $("#Family2").val() + "-" + $("#Job1").val() + "-" + $("#Job2").val() + "-" + $("#Mindfulness1").val() + "-" + $("#Mindfulness2").val() + "-" + $("#Fitness1").val() + "-" + $("#Fitness2").val() + ";";
                    questionID += $(this).attr("id") + ";";
                    questions += $("#Title" + $(this).attr("id")).text() + ";";
                }
                else {
                    var answer = $("#Answer" + $(this).attr("id")).val() == "Enter text here" ? "" : $("#Answer" + $(this).attr("id")).val().trim();
                    if (answer != "") {
                        answers += answer + ";";
                        questionID += $(this).attr("id") + ";";
                        questions += $("#Title" + $(this).attr("id")).text() + ";";
                    }
                }
            });
            $.postJSONnoQS('<%=Url.Action("LeaderInterviewEdit") %>', { questionID: questionID, questions: questions, answers: answers }, function (data) {
                if (data == "1") {
                    $("#editLeftInfo").click();
                }
                else if (data == "4") {
                    window.location.href = "/Leader/MyCompetence?leaderid=" + "<%=leaderid %>";
                }
                else if (data == "5") {
                    window.location.href = "/Leader/RegisterSuccess?leaderid=" + "<%=leaderid %>";
                }
                else {
                    window.location.reload();
                    return false;
                }
            });
        });
    });
</script>