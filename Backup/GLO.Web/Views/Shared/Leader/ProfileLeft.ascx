﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Leader>" %>
   <% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
      bool showRequestRecommendButton = true;
      if (Request.IsAuthenticated)
      {
          foreach (var recommendUser in Model.GLO_Users.GLO_Recommendation1)
          {
              if (recommendUser.UserID.ToString() == user.UserData.UserID.ToString())
              {
                  showRequestRecommendButton = false;
              }
          }
          if (Model.UserID == user.UserData.UserID || user.UserData.UserType == 4)
          {
              showRequestRecommendButton = false;
          }
      }
      %>
  <div class="boxer_user">  
      		
    <div class=" <%=Model.GLO_Users.IsPublish ? "box_user_private_on_view" : "box_user_private_view"%>"></div>
    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>"/>
    </div>
    <div class="box_user_button">
    <%if (Request.IsAuthenticated && !bool.Parse(ViewBag.IsRecommend.ToString()))
      { %>
        <%if (!bool.Parse(ViewData["IsLeaderOwner"].ToString()))
        { 
        %>        
                 <%if (user.UserData.UserType != 1)
                   { %>
                <%if (!bool.Parse(ViewBag.ShowTagButton.ToString()))
                  {%>                
                        <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                    <% }
                  else
                  {%>
                        <input type="button" id="TagUser" class="bluebtn" value="Tag" />
                    <%} %>   
                    <%if (!bool.Parse(ViewBag.ShowRequestTagButton.ToString()))
                      {%>
            <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                <% }
                      else
                      {%>
            <input type="button" id="RequestTag" class="bluebtn" value="Request Tag" onclick="RequestTag('<%=Model.UserID %>')"/>
            <%} %>               
            <%} %>
            <%if (user.UserData.UserType !=3 )
              {
                 if (showRequestRecommendButton)
                {%>
                <input type="button" id="RequestRecommend" class="bluebtn" value="Request referral" />
                <% }
                 else
                {%> 
                <input type="button" class="graybtn" value="Request referral" onclick="return false;" />
                <%}%>
            <%} %>
            <input type="button" id="SendMessage" class="bluebtn" value="Send Email" />
            
        <%} %>
        <%if (bool.Parse(ViewData["IsLeaderOwner"].ToString()) || user.UserData.UserType != 1)
          { %>
        <input type="button" id="ViewResume" class="bluebtn" value="View Resume" />
        <%} %>
        <%} %>
    </div>
    </div>
    <div class="box_user_name"><%=Model.NickName %></div>
    <div class="box_user_title"><%=Model.Title %></div>

    <div class="box_user_local"><%=Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName : ""%></div>
    <div class="box_user_industry"><%= Model.GLO_Users.GLO_UserIndustry.Count>0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName : "" %></div>
    <div class="box_user_note"><%= string.IsNullOrEmpty(Model.Version)?"": Model.Version.Replace("\n", "<br>") %></div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <%if (string.IsNullOrEmpty(Model.LeaderValue1) && string.IsNullOrEmpty(Model.LeaderValue2) && string.IsNullOrEmpty(Model.LeaderValue3)){%>
            <div style="height:30px;"></div>
        <%} else {%>
            <%if(!string.IsNullOrEmpty(Model.LeaderValue1)){%><div class='value_item_active font'><%=Model.LeaderValue1%></div><%} %>
            <%if(!string.IsNullOrEmpty(Model.LeaderValue2)){%><div class='value_item_active font'><%=Model.LeaderValue2%></div><%} %>
            <%if(!string.IsNullOrEmpty(Model.LeaderValue3)){%><div class='value_item_active font'><%=Model.LeaderValue3%></div><%} %>
        <%} %>
    </div>
        <div class="box_user_icon">
                    <div class="icon_index">
                        <img src="/Styles/images/<%=(String.IsNullOrEmpty(Model.Industry) || String.IsNullOrEmpty(Model.Title)  || !Model.GLO_Users.Approved) ? "icon_index3_gray.gif" : "icon_index3.gif"%>" /><br />Profile 
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/<%=(Model.GLO_Users.GLO_Recommendation.Count==0) ? "icon_index4_gray.gif" : "icon_index4.gif"%>" /><br />Recommend 
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/<%=!Model.IsInterview ? "icon_index5_gray.gif" : "icon_index5.gif"%>" /><br />Interview
                    </div>                  
                    <div class="icon_index">                   
                        <img src='/Styles/images/<%=Model.OnlineAssessment || !string.IsNullOrEmpty(Model.Assessment)?  "icon_index6.gif":"icon_index6_gray.gif" %>' /><br />Online Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/<%=!Model.OfflineAssessment ? "icon_index7_gray.gif" : "icon_index7.gif"%>" /><br />Offline Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/<%=!Model.WALP ? "icon_index8_gray.gif" : "icon_index8.gif"%>" /><br />WLDP
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/<%=!Model.Coaching ? "icon_index9_gray.gif" : "icon_index9.gif"%>" /><br />Coaching
                    </div>
    </div>
    <div class="me_edit2">
        <%if (bool.Parse(ViewData["IsLeaderOwner"].ToString()))
          { %>
    <a href="javascript:void(0)" id="editLeftInfo"><img src="/Styles/images/icon_edit.gif" /></a>
    <%}  %>
    </div> 
</div>
<div id="resumesList" style="display:none;">   
        <div class="lightbox_mask">
        </div>
        <div class="lightbox_manage">
            <div class="lightbox_message"> 
                <div class="message_headline">Resume lists</div>
                <ul>
                    <%
                    var resumesList = Model.GLO_Users.GLO_Resumes;
                    if (resumesList.Count > 0)
                    {
                    foreach (var resume in resumesList)
                    {%>            
                    <li><a href="/upload/<%=Model.UserID %>/<%=resume.ResumeName%>"><%=resume.ResumeName%></a></li>
                    <%}
                    } %>
                </ul>
            <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
            </div>
        </div>
</div>
  <script type="text/javascript">
      $(document).ready(function () {                                        
          $("#editLeftInfo").click(function(){
            $.getJSON('/Leader/_EidtLeftInfo', { "leaderid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                $("#lightboxwrapper").attr("style", "display:block");
                $('#lightboxwrapper').empty().html(data.Html);
             });                  
         });
         $("#TagUser").click(function () {
            $.getJSON('<%=Url.Action("GetTagCategory","Tag") %>', {id:'<%=Model.UserID %>', random: Math.random() }, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: '<%=Model.UserID %>', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
         $("#ViewResume").click(function () {     
            $('#lightboxwrapper').empty().html($('#resumesList').html());
            $("#lightboxwrapper").attr("style", "display:block");
        });             
        $("#RequestRecommend").click(function () {
            $.postJSON('<%=Url.Action("RequestRecommend","Expert") %>', { requestUserID: '<%=Model.UserID %>' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });  
      });
    function RequestTag(id) {
        $.post('<%=Url.Action("RequestTag","Tag") %>', { requestUserID: id }, function (data) {
            if (data) {
                alertbox("Your request has been successfully sent!");
            }
        });
    }
 </script>