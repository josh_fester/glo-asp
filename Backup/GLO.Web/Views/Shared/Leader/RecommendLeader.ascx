﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<GLO.Data.Models.GLO_Leader>>" %>
<div class="content_list">
        <div class="list_item">
            <div class="item_title">Good Leaders</div>
            <%--<%foreach (var leader in Model)
                { %>
                <div class="item_user">
                    <div class="user_icon"><a href="/Leader/Index?leaderid=<%=leader.UserID %>" target="_blank" title="<%=leader.NickName %>"><img src="/images/<%=!String.IsNullOrEmpty(leader.LogoUrl)?leader.LogoUrl:"user_none.gif" %>" /></a></div>
                    <div class="user_name"><a href="/Leader/Index?leaderid=<%=leader.UserID %>" target="_blank" title="<%=leader.NickName %>"><%=leader.NickName.Length > 30 ? leader.NickName.Substring(0, 28) + "..." : leader.NickName %></a></div>
                </div>
            <%} %>--%>
                
                    <div class="item_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" target="_blank" title="Peter Buytaert"><img src="/images/home_user17.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" target="_blank" title="Peter Buytaert">Peter Buytaert</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=517" target="_blank" title="Erik Jia"><img src="/images/home_user18.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=517" target="_blank" title="Erik Jia">Erik Jia</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" target="_blank" title="Jay Tang"><img src="/images/home_user6.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" target="_blank" title="Jay Tang">Jay Tang</a></div>
                    </div>
                    <div class="item_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" target="_blank" title="Veronique Huberts"><img src="/images/home_user8.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" target="_blank" title="Veronique Huberts">Veronique Huberts</a></div>
                    </div>

        </div>
        <div class="list_link">
         <%:Html.ActionLink("Create your leader profile", "LeaderRegister", "Account", new { type = "1" }, new { })%>
        </div>
        <div class="list_view">
            <a href="/Sample/LeaderIndex" target="_blank">View a sample</a>
        </div>
  </div>
