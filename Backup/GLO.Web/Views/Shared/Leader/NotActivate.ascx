﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Leader>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_leader">
            <div style="clear:both;float:left;">
                Thank you for your interest to join GLO!<br /><br />
               
                You will now receive an email with a link to activate your membership application.
                 After your confirmation of this email with a click on the link provided, you will be guided back to the site. 
                 You can then complete and strengthen your profile via a series of action steps. 
                 Upon completion of each step the related icon in your profile action bar will be highlighted in blue as indicated below. 

                <br />
                <br />
            </div>
            <div class="box_user_icon">
                <div class="icon_index">
                    <img src="/Styles/images/icon_index3.gif" /><br />
                    Profile
                </div>
                <div class="icon_index">
                    <img src="/Styles/images/icon_index4.gif" /><br />
                    Recommend
                </div>
                <div class="icon_index">
                    <img src="/Styles/images/<%=!Model.IsInterview ? "icon_index5_gray.gif" : "icon_index5.gif"%>" /><br />Interview
                </div>                  
                <div class="icon_index">                   
                    <img src='/Styles/images/<%=!Model.OnlineAssessment ? "icon_index6_gray.gif" : "icon_index6.gif"%>' /><br />Online Assessment
                </div>
                <div class="icon_index">
                    <img src="/Styles/images/<%=!Model.OfflineAssessment ? "icon_index7_gray.gif" : "icon_index7.gif"%>" /><br />Offline Assessment
                </div>
                <div class="icon_index">
                    <img src="/Styles/images/<%=!Model.WALP ? "icon_index8_gray.gif" : "icon_index8.gif"%>" /><br />WLDP
                </div>
                <div class="icon_index">
                    <img src="/Styles/images/<%=!Model.Coaching ? "icon_index9_gray.gif" : "icon_index9.gif"%>" /><br />Coaching
                </div>
            </div>
            <div style="clear:both;float:left;">
                <ul>
                    <li>Complete your profile including your vision, values, competencies and take a generic interview online</li>
                    <li>Get recommended by up to 8 people who have made a significant difference in your career or life</li>
                    <li>Request a skype interview with a GLO consultant to validate your profile data and get feedback on your interview skills</li>
                    <li>Take the GLO wise leader online assessment</li>
                    <li>Join GLO’s half day wise leadership assessment center</li>
                    <li>Participate in GLO’s wise coaching program</li>
                    <li>Join GLO’s wise leadership development program in collaboration with ivy league graduate schools</li>
               </ul>
                <h3>Success and thank you for your leadership!</h3>
            </div>

        <div class="me_apply">
            <a href="javascript:void(0)" id="apply">
                <img src="/Styles/images/icon_apply.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#apply').click(function () {
            window.location.href = "/account/signout";
        });
    });
</script>