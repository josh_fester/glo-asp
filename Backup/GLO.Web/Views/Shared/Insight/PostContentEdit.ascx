﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.InsightPost>" %>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <div class=""></div>
    <table>      
        <tr>
            <td><%: Html.LabelFor(model => model.Title) %></td>
            <td> 
            <%: Html.TextBoxFor(model => model.Title, new { @style = "width:455px" })%>
            <%: Html.ValidationMessageFor(model => model.Title) %>
            <input type="hidden" id="PostID" name="PostID" value="<%=Model.PostID %>" />
            </td>
        </tr>
         <tr>
            <td>Summary</td>
            <td> 
            <textarea id="Description" style="width:455px; height:40px;" name="Description" data-val-required="The summary is required." data-val="true"><%=Model.Description%></textarea>

            <%: Html.ValidationMessageFor(model => model.Description)%>
            </td>
        </tr>
            <tr>
            <td>Tag</td>
            <td> 
            <%: Html.TextBoxFor(model => model.PostTag, new { @style = "width:455px;" })%>
            <%: Html.ValidationMessageFor(model => model.PostTag)%>
            Separate each tag with a ";"
            </td>
        </tr>
        <tr>
            <td><%: Html.LabelFor(model => model.PostContent) %></td>
            <td>
                <textarea id="editor1" name="editor1"><%=Model.PostContent.Replace("\n","").Replace("\r","")%></textarea>
                <script type="text/javascript">
                    window.onload = function () {
                        var config = {
                            width:470,
                            height: 260,
                            toolbar: 'Basic1'
                        };

                        CKEDITOR.replace('editor1', config);
                    };

                </script>
            </td>
        </tr>
      
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Save" class="btn_form" />
            </td>
        </tr>
    </table>
<% } %>
