﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Functions>" %>
 <div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_message">
            <table>
                <tr>
                    <td><b>Please input function name</b></td>
                </tr>
                <tr>
                    <td><input id="function" class="title" value="<%=Model.FunctionName %>" />   </td>
                </tr>
            </table>
            
               
            
            <div class="me_apply"><a href="javascript:void(0)" x:functionid="<%=Model.FunctionID%>" id="functionname_edit"><img src="/Styles/images/icon_apply.gif"></a></div>
            <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>   
                 
        </div>
    
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#functionname_edit").click(function () {
                if ($('#function').val().trim() == "") {
                    return false;
                }
                $.postJSON('<%=Url.Action("FunctionNameEdit","Tag") %>', { functionid: $(this).attr("x:functionid"), functionname: $("#function").val().trim() }, function (data) {
                    window.location.reload();
                    return false;
                });

            });
        });
</script>
  