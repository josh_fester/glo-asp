﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.TagUserModel>" %>
<script src="<%=Url.Content("~/Js/jquery.json-2.2.min.js") %>" type="text/javascript"></script>
<%: Html.HiddenFor(m=>m.TaggedUserID) %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_message">
    <h2>Tag</h2>
        <table>
            <tr>
            <td>
            Name
            </td>
            <td>
                <div class="greytext"><%=Model.UserName %></div>
            </td>
            </tr>
            <%if (Model.ShowFunctions)
              { %>
            <tr>
                <td>Function</td>
                <td>
                <div class="select_box">
                    <input class="select_value" type="text" readonly="readonly" value="Select a function" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option">
                        <ul>
                            <% foreach (var item in Model.FunctionList)
                               {
                            %>
                            <li x:categoryID="<%=item.TagCategoryID %>"><%=item.GLO_Functions.FunctionName%> <%if (item.GLO_TagUsers.Count == 0)
                                                                                                                { %> <img title="delete" id="<%=item.TagCategoryID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li><%} %>
                            <%} %>
                        </ul>
                    </div>
                </div>
                </td>
            </tr>
            <tr>
                <td>New Function</td>
                <td>
                <div class="user_value_add">
                    <input type="text" value="" class="txt_itemadd" id="txtFunctionName" maxlength="25" />
                    <input type="button" id="btnAddTagCategory" value="Add" class="bluebtn" />
                </div>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>Memo</td>
                <td><textarea value="" class="content" style="height:180px" id="txtTagNote" /><input id="Function" type="hidden" value="<%=Model.TagCategoryID %>" /></td>
            </tr>
        </table>
        <div class="me_apply">
            <a id="btnSubmit" href="javascript:void(0);">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        SetDropdown();
        $("#btnAddTagCategory").click(function () {
            var value = $('#txtFunctionName').val().trim();
            if (value == "") {
                return false;
            }
            $.postJSON('<%=Url.Action("AddTagCategory","Tag") %>', { "functionName": value }, function (data) {
                var html = "<li x:categoryID=\"" + data + "\">" + value + "<img title=\"delete\" id=\"" + data + "\" class=\"value_delete\" src=\"/Styles/images/icon_cancel.gif\" /></li>";
                if (data != "") {
                    var isExist = false;
                    $(".select_option li").each(function () {
                        if ($(this).attr("x:categoryID") == data) {
                            isExist = true;
                        }
                    });
                    if (!isExist) {
                        $('.select_option').children("ul").append(html);
                        SetDropdown();
                    }
                    $('#txtFunctionName').val("");
                }
                else {
                    alertbox("Add failed.");
                }
            });
        });
        $("#btnSubmit").click(function () {
            var isSearchPage = '<%=Model.IsSearchPage %>';
            var tagedUserID = $('#TaggedUserID').val();
            var categoryID = $("#Function").val();
            if (tagedUserID == "" || categoryID == "" || categoryID == "0") {
                alertbox("Please select a function.")
                return false;
            }
            var tagData = {
                'TaggedUserID': tagedUserID,
                'TagCategoryID': categoryID,
                'TagNote': $("#txtTagNote").val()
            };
            $.postJSON('<%=Url.Action("TagUser","Tag") %>', { tagData: $.toJSON(tagData) }, function (data) {
                if (data) {                    
                    if (isSearchPage == "True") {
                         window.location.href = "/Home/HeaderSearch?c=" + $('#txtCompanyKeyWord').val() + "&e=" + $('#txtExpertKeyWord').val() + "&l=" + $('#txtLeaderKeyWord').val() + "&searchtype=Users";
                    }
                    else {
                       
                        window.location.reload();
                    }
                }
            })
        });
    });
    function SetDropdown()
    {    
        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            var value = $(this).attr("x:categoryID");
            var text = $(this).text().replace(/<img([^>]*)>/gi, "");
            $(this).parent().parent().parent().find(".select_value").val(text);
            $('#Function').val(value);
        });
        $(".value_delete").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();

            var text = "All Function";
            $(this).parent().parent().parent().parent().find(".select_value").val(text);

            var id = $(this).attr("id");
            confirmbox("Do you want to delete the function?", function () {
                $.getJSON('<%=Url.Action("DeleteValue","Tag") %>', { "id": id, random: Math.random() }, function (data) {
                    if (data) {
                        $('.select_option li').each(function () {
                            if ($(this).attr("x:categoryID") == id) {
                                $(this).remove();
                            }
                        });
                    }
                });
            });
        });
    }
</script>
