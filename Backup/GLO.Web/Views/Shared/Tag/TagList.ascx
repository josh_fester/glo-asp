﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Users>" %>
<div class="board_list3">
    <div class="list_title">
        I Tagged Them</div>
    <div class="list_info3">
        <%var taglist = (IList<GLO.Data.Models.GLO_TagUsers>)ViewBag.TagList; %>
        <%foreach (var tag in taglist)
          {
              var user = tag.GLO_Users1;
        %>
            <div class="message_line">
                <div class="message_usericon">
                    <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                      {%>
                    <a href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                        <img src="/images/<%=user.Icon%>" /></a>
                    <%}
                      else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                      {%>
                    <a href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                        <img src="/images/<%=user.Icon%>" /></a>
                    <%}
                      else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                      {%>
                    <a href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                        <img src="/images/<%=user.Icon%>" /></a>
                    <% }%>
                </div>
                <div class="message_userinfo">
                    <table>
                        <tr>
                            <td>
                                <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                                  {%>
                                <a class="hyplink" href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                                    <%=user.NickName %></a>
                                <%}
                                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                                  {%>
                                <a class="hyplink" href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                                    <%=user.NickName %></a>
                                <%}
                                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                                  {%>
                                <a class="hyplink" href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                                    <%=user.NickName %></a>
                                <% }%>
                            </td>
                        </tr>
                        <tr>
                        <td>
                        <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                              {%>
                            <a class="hyplink" href="<%=user.GLO_Company.Website%>" target="_blank">
                                <%=user.GLO_Company.Website%></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                              {%>
                            <%= user.GLO_HRExpert.Location %>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                              {%>
                            <%= user.GLO_Leader.Location%>
                            <% }%>
                        </td>
                        </tr>
                    </table>
                </div>
                <div class="message_action" style="width:185px;margin-top:5px;">
                    <table>
                        <tr>
                            <td>
                                <%= tag.TagNote %>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="javascript:void(0)" class="messagesend" x:userid="<%=user.UserID %>">Send Message</a> |
                                <a href="javascript:void(0)" class="value_edit" x:tagid="<%=tag.TagID %>">
                                    Edit</a> | <a href="javascript:void(0)" class="value_delete" x:tagid="<%=tag.TagID %>">
                                        Delete</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <%}
        %>
    </div>
</div>
<div class="board_list3">
    <% var tagmelist = (IList<GLO.Data.Models.GLO_Users>)ViewBag.TheyTagme; %>
    <div class="list_title">
        They Tagged Me</div>
    <div class="list_info3">
        <%foreach (var user in tagmelist)
          { %>
        <div class="message_line">
            <div class="message_usericon">
                <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                  {%>
                <a href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                  {%>
                <a href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                  {%>
                <a href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <% }%>
            </div>
                <div class="message_userinfo">
                <table>
                    <tr>
                        <td>
                            <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                              {%>
                            <a class="hyplink" href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                              {%>
                            <a class="hyplink" href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                              {%>
                            <a class="hyplink" href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                              {%>
                            <a class="hyplink" href="<%=user.GLO_Company.Website%>" target="_blank">
                                <%=user.GLO_Company.Website%></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                              {%>
                            <%= user.GLO_HRExpert.Location %>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                              {%>
                            <%= user.GLO_Leader.Location%>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%}
        %>
    </div>
</div>
<div class="board_list3">
    <div class="list_title">
        We Tagged Each Other</div>
    <% var tageachlist = (IList<GLO.Data.Models.GLO_Users>)ViewBag.TagEachOther; %>
    <div class="list_info3">
        <%foreach (var user in tageachlist)
          {
        %>
        <div class="message_line">
            <div class="message_usericon">
                <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                  {%>
                <a href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                  {%>
                <a href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                  {%>
                <a href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <% }%>
            </div>
                <div class="message_userinfo">
                <table>
                    <tr>
                        <td>
                            <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                              {%>
                            <a class="hyplink" href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                              {%>
                            <a class="hyplink" href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                              {%>
                            <a class="hyplink" href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "3")
                              {%>
                            <a class="hyplink" href="<%=user.GLO_Company.Website%>" target="_blank">
                                <%=user.GLO_Company.Website%></a>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "2")
                              {%>
                            <%= user.GLO_HRExpert.Location %>
                            <%}
                              else if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                              {%>
                            <%= user.GLO_Leader.Location%>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%}
        %>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".value_delete").click(function () {
            var tagID = $(this).attr("x:tagid");

            confirmbox("Do you want to delete this user?", function () {
                $.post('<%=Url.Action("DeleteTaggedUser","Tag") %>', { "tagID": tagID, random: Math.random() }, function (data) {
                    if (data) {
                        window.location.reload();
                    }
                    else {
                        return false;
                    }
                });
            });
        });
        $(".messagesend").click(function () {
            $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: $(this).attr("x:userid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $(".value_edit").click(function () {
            $.getJSON('<%=Url.Action("EditTagCategory","Tag") %>', { tagID: $(this).attr("x:tagid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>