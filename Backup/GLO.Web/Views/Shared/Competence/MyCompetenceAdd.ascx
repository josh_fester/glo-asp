﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="lightbox_mask"></div>
<div class="lightbox_manage"> 
        <div class="lightbox_note">
            This page is a graphic representation of your resume. You can show three different timelines:
            <br /><br />
            <ul>
                <li>Your education</li>
                <li>Your professional career</li>
                <li>Your activities related to non for profit, charity, etc.</li>
            </ul>
            <br /><br />
            We also encourage to complete the optional  fields that describe in more detail your position, the competencies you applied and specific successes you have achieved during that assignment
            <br /><br />
            Only your current employment situation is required, the full resume can be updated later if you prefer

        </div>
 <div class="timeline_edit">
    <div class="timeline_arrow"></div>
    <div id="TimeLineAdd" >
        <div class="timeline_summary"></div>
        <div class="timeline_education"></div>
        <div class="timeline_contribution"></div>
        <div class="select_box select_box_type">
            <input class="select_value" id="SelectCompetenceType" type="text" readonly="readonly" value="Employment" />
            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
            <div class="select_option">
                <ul>
                    <li>Employment</li>
                    <li>Non-profit</li>
                    <li>Education</li>
                </ul>
            </div>
        </div>
        <div id="Employment">
            <input type="text" class="font" id="Company" value="Company" onfocus="if (this.value=='Company')this.value=''" onblur="if (this.value=='')this.value='Company'">
            <input type="text" class="font" id="JobTitle" value="Job Title" onfocus="if (this.value=='Job Title')this.value=''" onblur="if (this.value=='')this.value='Job Title'">
            <input type="text" class="font" id="JobIndustry" value="Job Industry" onfocus="if (this.value=='Job Industry')this.value=''" onblur="if (this.value=='')this.value='Job Industry'">
            <input type="text" class="font" id="JobStartDate" readonly="true" value="From" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <div class="emptycalendar">
                <input type="text" class="font" id="JobEndDate" value="To" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
                <span>Leave it blank means present</span>
            </div>
            <textarea class="font" id="LessonLearnd" onfocus="if (this.value=='Position description')this.value=''" onblur="if (this.value=='')this.value='Position description'">Position description</textarea>
            <textarea class="font" id="Competencies" onfocus="if (this.value=='Competence applied')this.value=''" onblur="if (this.value=='')this.value='Competence applied'">Competence applied</textarea>
            <textarea class="font" id="SuccessAchievement" onfocus="if (this.value=='Success achieved')this.value=''" onblur="if (this.value=='')this.value='Success achieved'">Success achieved</textarea>
        </div>
        <div id="Education" style="display:none">
            <input type="text" class="font" id="EduInstitute" value="Institute" onfocus="if (this.value=='Institute')this.value=''" onblur="if (this.value=='')this.value='Institute'">
            <input type="text" class="font" id="EduMajor" value="Major" onfocus="if (this.value=='Major')this.value=''" onblur="if (this.value=='')this.value='Major'">
            <input type="text" class="font" id="EduStartDate" readonly="true" value="From" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <div class="emptycalendar">
                <input type="text" class="font" id="EduEndDate"  value="To" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
                <span>Leave it blank means present</span>
            </div>
            <div class="select_box select_box_degree">
                <input class="select_value" id="EduDegree" type="text" readonly="readonly" value="Degree" />
                <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                <div class="select_option">
                    <ul>
                        <li>Bachelor</li>
                        <li>Master</li>
                        <li>PHD</li>
                        <li>Postdoctor</li>
                        <li>Other</li>
                    </ul>
                </div>
                
            </div>
            <textarea id="EduCourseTitle" class="font" onfocus="if (this.value=='Course Title')this.value=''" onblur="if (this.value=='')this.value='Course Title'">Course Title</textarea>
        </div>
            <div id="NonProfit" style="display:none">
            <input type="text" class="font" id="ConOrgnization" value="Organization" onfocus="if (this.value=='Organization')this.value=''" onblur="if (this.value=='')this.value='Organization'">
            <input type="text" class="font" id="ConJobTitle" value="Job Title" onfocus="if (this.value=='Job Title')this.value=''" onblur="if (this.value=='')this.value='Job Title'">
            <input type="text" class="font" id="ConStartDate" readonly="true" value="From" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <div class="emptycalendar">
                <input type="text" class="font " id="ConEndDate"  value="To" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
                <span>Leave it blank means present</span>
            </div>
            <textarea class="font" id="ConLessonLearnd" onfocus="if (this.value=='Jobs duties/ Lessons learned')this.value=''" onblur="if (this.value=='')this.value='Jobs duties/ Lessons learned'">Jobs duties/ Lessons learned</textarea>
            <textarea class="font" id="ConCompetencies" onfocus="if (this.value=='Competence applied')this.value=''" onblur="if (this.value=='')this.value='Competence applied'">Competence applied</textarea>
            <textarea class="font" id="ConSuccessAchievement" onfocus="if (this.value=='Success achieved')this.value=''" onblur="if (this.value=='')this.value='Success achieved'">Success achieved</textarea>
        </div>
    </div>
    <div class="me_apply"><a href="javascript:void(0)" id ="educationAdd"><img src="/Styles/images/icon_apply.gif" /></a></div>  
    <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div> 
                       
</div>
</div>
<input type="hidden" id="redirectUrl"/>
<script type="text/javascript">
    $(document).ready(function () {
        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();

            var value = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(value);

            if (value == "Education") {
                $("#Education").show();
                $("#Employment").hide();
                $("#NonProfit").hide();
            }
            else if (value == "Employment") {
                $("#Education").hide();
                $("#Employment").show();
                $("#NonProfit").hide();
            }
            else if (value == "Non-profit") {
                $("#Education").hide();
                $("#Employment").hide();
                $("#NonProfit").show();
            }
        });


        $(".emptycalendar").find("input").change(function () {
            if ($(this).val() == "To" || $(this).val() == "") {
                $(this).parent().find("span").css("display", "block");
            }
            else {
                $(this).parent().find("span").css("display", "none");
            }
        });

        $("#JobStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOtherMonths: true,
            onClose: function (selectedDate) {
                var oldVal = $("#JobEndDate").val();
                $("#JobEndDate").datepicker("option", "minDate", selectedDate);
                $("#JobEndDate").val(oldVal);
                $("#JobStartDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#JobEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOtherMonths: true,
            onClose: function (selectedDate) {
                var oldVal = $("#JobStartDate").val();
                if (Date.parse(selectedDate) > new Date()) {
                    $("#JobStartDate").datepicker("option", "maxDate", new Date());
                }
                else {
                    $("#JobStartDate").datepicker("option", "maxDate", selectedDate);
                }
                $("#JobStartDate").val(oldVal);
                $("#JobEndDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#EduStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#EduEndDate").val();
                $("#EduEndDate").datepicker("option", "minDate", selectedDate);
                $("#EduEndDate").val(oldVal);
                $("#EduStartDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#EduEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#EduStartDate").val();
                if (Date.parse(selectedDate) > new Date()) {
                    $("#EduStartDate").datepicker("option", "maxDate", new Date());
                }
                else {
                    $("#EduStartDate").datepicker("option", "maxDate", selectedDate);
                }
                $("#EduStartDate").val(oldVal);
                $("#EduEndDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#ConStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#ConEndDate").val();
                $("#ConEndDate").datepicker("option", "minDate", selectedDate);
                $("#ConEndDate").val(oldVal);
                $("#ConStartDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#ConEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#ConStartDate").val();
                if (Date.parse(selectedDate) > new Date()) {
                    $("#ConStartDate").datepicker("option", "maxDate", new Date());
                }
                else {
                    $("#ConStartDate").datepicker("option", "maxDate", selectedDate);
                }
                $("#ConStartDate").val(oldVal);
                $("#ConEndDate").removeClass("input_error");
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });




        $("#educationAdd").click(function () {
            var userid = $("#hidexpertid").val();
            var empdata = { 'UserID': ''
                            , 'Company': ''
                            , 'JobTitle': ''
                            , 'JobIndustry': ''
                            , 'StartDate': ''
                            , 'EndDate': ''
                            , 'SuccessAchievement': ''
                            , 'Competencies': ''
                            , 'LessonLearnd': ''
            };
            empdata.UserID = userid;
            empdata.Company = $("#Company").val();
            empdata.JobTitle = $("#JobTitle").val();
            empdata.JobIndustry = $("#JobIndustry").val();
            empdata.StartDate = $("#JobStartDate").val();
            empdata.EndDate = $("#JobEndDate").val() == "To" ? "" : $("#JobEndDate").val();
            empdata.SuccessAchievement = $("#SuccessAchievement").val() == "Success achieved" ? "" : $("#SuccessAchievement").val();
            empdata.Competencies = $("#Competencies").val() == "Competence applied" ? "" : $("#Competencies").val();
            empdata.LessonLearnd = $("#LessonLearnd").val() == "Position description" ? "" : $("#LessonLearnd").val();

            var edudata = { 'UserID': userid,
                'StartDate': $("#EduStartDate").val(),
                'EndDate': $("#EduEndDate").val() == "To" ? "" : $("#EduEndDate").val(),
                'Institute': $("#EduInstitute").val(),
                'Major': $("#EduMajor").val(),
                'Degree': $("#EduDegree").val(),
                'CourseTitle': $("#EduCourseTitle").val() == "Course Title" ? "" : $("#EduCourseTitle").val()
            };

            var condata = { 'UserID': userid,
                'Orgnization': $("#ConOrgnization").val(),
                'JobTitle': $("#ConJobTitle").val(),
                'StartDate': $("#ConStartDate").val(),
                'EndDate': $("#ConEndDate").val() == "To" ? "" : $("#ConEndDate").val(),
                'LessonLearnd': $("#ConLessonLearnd").val() == "Jobs duties/ Lessons learned" ? "" : $("#ConLessonLearnd").val(),
                'Competencies': $("#ConCompetencies").val() == "Competence applied" ? "" : $("#ConCompetencies").val(),
                'SuccessAchievement': $("#ConSuccessAchievement").val() == "Success achieved" ? "" : $("#ConSuccessAchievement").val()
            };

            var selecttype = $("#SelectCompetenceType").val();
            var jsondata = "";
            if (selecttype == "Education") {
                if (ValidateEducation() == false)
                    return;
                jsondata = $.toJSON(edudata);
            }
            else if (selecttype == "Employment") {
                if (ValidateEmployment() == false)
                    return;
                jsondata = $.toJSON(empdata);
            }
            else {
                if (ValidateOrg() == false)
                    return;
                jsondata = $.toJSON(condata);
            }
            $.postJSONnoQS('<%=Url.Action("CareerSummaryAdd","Competence") %>', { data: jsondata, type: selecttype }, function (data) {
                if (data.indexOf("RegisterSuccess") >= 0) {
                    $("#redirectUrl").val(data);
                }
                confirmbox2("Add more activities?","OK","LATER",
                function () {
                    $("#editProfile").click();
                },
                function () {
                    if ($("#redirectUrl").val() != "") {
                        window.location.href = $("#redirectUrl").val();
                    }
                    else {
                        window.location.href = data;
                    }
                });
            });
        });

        $("#Company").focusout(function () {
            if (isEmpty($("#Company").val()) || $("#Company").val() == "Company") {
                $("#Company").addClass("input_error");
            }
            else {
                $("#Company").removeClass("input_error");
            }
        });

        $("#JobTitle").focusout(function () {
            if (isEmpty($("#JobTitle").val()) || $("#JobTitle").val() == "Job Title") {
                $("#JobTitle").addClass("input_error");
            }
            else {
                $("#JobTitle").removeClass("input_error");
            }
        });

        $("#JobStartDate").focusout(function () {
            if (isEmpty($("#JobStartDate").val()) || $("#JobStartDate").val() == "From") {
                $("#JobStartDate").addClass("input_error");
            }
            else {
                $("#JobStartDate").removeClass("input_error");
            }
        });

//        $("#JobEndDate").focusout(function () {
//            if (isEmpty($("#JobEndDate").val()) || $("#JobEndDate").val() == "To") {
//                $("#JobEndDate").addClass("input_error");
//            }
//            else {
//                $("#JobEndDate").removeClass("input_error");
//            }
//        });

        $("#EduInstitute").focusout(function () {
            if (isEmpty($("#EduInstitute").val()) || $("#EduInstitute").val() == "Institute") {
                $("#EduInstitute").addClass("input_error");
            }
            else {
                $("#EduInstitute").removeClass("input_error");
            }
        });

        $("#EduMajor").focusout(function () {
            if (isEmpty($("#EduMajor").val()) || $("#EduMajor").val() == "Major") {
                $("#EduMajor").addClass("input_error");
            }
            else {
                $("#EduMajor").removeClass("input_error");
            }
        });

        $("#EduStartDate").focusout(function () {
            if (isEmpty($("#EduStartDate").val()) || $("#EduStartDate").val() == "From") {
                $("#EduStartDate").addClass("input_error");
            }
            else {
                $("#EduStartDate").removeClass("input_error");
            }
        });
//        $("#EduEndDate").focusout(function () {
//            if (isEmpty($("#EduEndDate").val()) || $("#EduEndDate").val() == "To") {
//                $("#EduEndDate").addClass("input_error");
//            }
//            else {
//                $("#EduEndDate").removeClass("input_error");
//            }
//        });
        $("#ConOrgnization").focusout(function () {
            if (isEmpty($("#ConOrgnization").val()) || $("#ConOrgnization").val() == "Organization") {
                $("#ConOrgnization").addClass("input_error");
            }
            else {
                $("#ConOrgnization").removeClass("input_error");
            }
        });
        $("#ConJobTitle").focusout(function () {
            if (isEmpty($("#ConJobTitle").val()) || $("#ConJobTitle").val() == "Job Title") {
                $("#ConJobTitle").addClass("input_error");
            }
            else {
                $("#ConJobTitle").removeClass("input_error");
            }
        });
        $("#ConStartDate").focusout(function () {
            if (isEmpty($("#ConStartDate").val()) || $("#ConStartDate").val() == "From") {
                $("#ConStartDate").addClass("input_error");
            }
            else {
                $("#ConStartDate").removeClass("input_error");
            }
        });
//        $("#ConEndDate").focusout(function () {
//            if (isEmpty($("#ConEndDate").val()) || $("#ConEndDate").val() == "To") {
//                $("#ConEndDate").addClass("input_error");
//            }
//            else {
//                $("#ConEndDate").removeClass("input_error");
//            }
//        });

    });

    function ValidateEmployment() {
        var error = new Array();
        if (isEmpty($("#Company").val()) || $("#Company").val() == "Company") {
            $("#Company").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Company").removeClass("input_error");
        }

        if (isEmpty($("#JobTitle").val()) || $("#JobTitle").val() == "Job Title") {
            $("#JobTitle").addClass("input_error");
            error.push("error")
        }
        else {
            $("#JobTitle").removeClass("input_error");
        }

        if (isEmpty($("#JobStartDate").val()) || $("#JobStartDate").val() == "From") {
            $("#JobStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#JobStartDate").removeClass("input_error");
        }

//        if (isEmpty($("#JobEndDate").val()) || $("#JobEndDate").val() == "To") {
//            $("#JobEndDate").addClass("input_error");
//            error.push("error")
//        }
//        else {
//            $("#JobEndDate").removeClass("input_error");
//        }

        if (error.length > 0)
            return false;
        else
            return true;
    }

    function ValidateEducation() {
        var error = new Array();
        if (isEmpty($("#EduInstitute").val()) || $("#EduInstitute").val() == "Institute") {
            $("#EduInstitute").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduInstitute").removeClass("input_error");
        }

        if (isEmpty($("#EduMajor").val()) || $("#EduMajor").val() == "Major") {
            $("#EduMajor").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduMajor").removeClass("input_error");
        }

        if (isEmpty($("#EduStartDate").val()) || $("#EduStartDate").val() == "From") {
            $("#EduStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduStartDate").removeClass("input_error");
        }

//        if (isEmpty($("#EduEndDate").val()) || $("#EduEndDate").val() == "To") {
//            $("#EduEndDate").addClass("input_error");
//            error.push("error")
//        }
//        else {
//            $("#EduEndDate").removeClass("input_error");
//        }

        if (error.length > 0)
            return false;
        else
            return true;
    }

    function ValidateOrg() {
        var error = new Array();
        if (isEmpty($("#ConOrgnization").val()) || $("#ConOrgnization").val() == "Organization") {
            $("#ConOrgnization").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConOrgnization").removeClass("input_error");
        }

        if (isEmpty($("#ConJobTitle").val()) || $("#ConJobTitle").val() == "Job Title") {
            $("#ConJobTitle").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConJobTitle").removeClass("input_error");
        }

        if (isEmpty($("#ConStartDate").val()) || $("#ConStartDate").val() == "From") {
            $("#ConStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConStartDate").removeClass("input_error");
        }

//        if (isEmpty($("#ConEndDate").val()) || $("#ConEndDate").val() == "To") {
//            $("#ConEndDate").addClass("input_error");
//            error.push("error")
//        }
//        else {
//            $("#ConEndDate").removeClass("input_error");
//        }

        if (error.length > 0)
            return false;
        else
            return true;
    }
</script>