﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.Competence>" %>

<%
    var IsManager = false;
    var loginuser = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
    if(loginuser.UserData.UserType ==1 && ViewData["IsLeaderOwner"]!=null && bool.Parse(ViewData["IsLeaderOwner"].ToString()))
    {
        IsManager = true;
    } 
    else if(loginuser.UserData.UserType ==2 && ViewData["IsOwner"]!=null && bool.Parse(ViewData["IsOwner"].ToString()))
    {
        IsManager = true;
    }
    var education = Model.EducationList;
    var careersummarylist = Model.CarreerSummaryList;
    var contribution = Model.ContributionList;
     
   var endyear = Model.EndYear;// endate.Value.Year;
   var startyear = Model.StartYear;// startdate.Value.Year;
   var yearspan = endyear - startyear + 1;
   
%>


<div class="competence_wrap">
<%
    List<int[]> ayears = new List<int[]>();
    var alayer2 = 0;
    var alayer3 = 0;
      
    foreach (var cont in contribution)
    {
        int nindex = 0;
          
        foreach (int[] item in ayears)
        {
            if (cont.StartDate.Value.Year > item[0] || (cont.StartDate.Value.Year == item[0] && cont.StartDate.Value.Month >= item[1]))
            {
                if (cont.StartDate.Value.Year < item[2] || (cont.StartDate.Value.Year == item[2] && cont.StartDate.Value.Month <= item[3]))
                {
                    nindex = item[4] + 1;
                }
            }
        }

        int[] tmpyear = { cont.StartDate.Value.Year, cont.StartDate.Value.Month, cont.EndDate.Value.Year, cont.EndDate.Value.Month, nindex };
        ayears.Add(tmpyear);
           
        var spanyear = cont.EndDate.Value.Year - cont.StartDate.Value.Year;
        var spanmonth = cont.EndDate.Value.Month - cont.StartDate.Value.Month;
        var width = spanyear * 54 + (int)((spanmonth + 1) * 4.5) - 2;
        var left = (cont.StartDate.Value.Year - startyear) * 54 + (int)((cont.StartDate.Value.Month - 1) * 4.5);

        if (cont.StartDate.Value.Year < startyear)
        {
            width = (cont.EndDate.Value.Year - startyear + 1) * 54 + (int)((cont.EndDate.Value.Month - 12) * 4.5) - 2;
            left = 0;
        }

        var top1 = 40;
        var top2 = 195;
        var top3 = 350;
        var left1 = 0;
        var left2 = 0;
        var left3 = 0;
        var width1 = 0;
        var width2 = 0;
        var width3 = 0;

        if (left < 540)
        {
            top1 += 11 * nindex;
            left1 = left;
              
            if (left + width <= 540)
            {
                width1 = width;
            }
            else if (left + width >= 1080)
            {
                top2 += 11 * alayer2;
                top3 += 11 * alayer3;
                alayer2 += 1;
                alayer3 += 1;
                width3 = left + width - 1080;
                width2 = 540;
                width1 = width - width3 - width2;
            }
            else
            {
                top2 += 11 * alayer2;
                alayer2 += 1;
                width2 = left + width - 540;
                width1 = width - width2;
            }
        }
        else if (left >= 1080)
        {
            top3 += 11 * nindex;
            left3 = left - 1080;
            width3 = width;
        }
        else
        {
            top2 += 11 * nindex;
            left2 = left - 540;

            if (left + width >= 1080)
            {
                top3 += 11 * alayer3;
                alayer3 += 1;
                width3 = left + width - 1080;
                width2 = width - width3;
            }
            else
            {
                width2 = width;
            }
        }

        if (left3 + width3 > 538) width3 = 538 - left3;
        if (left2 + width2 > 538) width2 = 538 - left2;
        if (left1 + width1 > 538) width1 = 538 - left1;
%>
 
    <%if (width3 > 0) { %>
      <div class="nonprofit" style="width:<%=width3 %>px;left:<%=left3%>px; top:<%=top3%>px;">
        <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">
            <div class="timeline_black font">
            <%=cont.Orgnization %>
            <br />
            <%=cont.JobTitle %>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",cont.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", cont.EndDate)%>
            </div>

            <div class="timeline_black">Jobs duties/ Lessons learned</div>
            <div class="timeline_gray font"><%=cont.LessonLearnd.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Competence applied</div>
            <div class="timeline_gray font"><%=cont.Competencies.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Success achieved</div>
            <div class="timeline_gray font"><%=cont.SuccessAchievement.Replace("\n", "<br>")%></div>

        <%if (IsManager)
          { %>
        <a href="javascript:void(0)" class="delete_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Delete</a>
         <a href="javascript:void(0)" class="edit_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Edit</a>
        <%} %>
        </div>
        </div>
    </div>
    <%}%>
    <%if (width2 > 0) { %>
    <div class="nonprofit" style="width:<%=width2 %>px; left:<%=left2%>px; top:<%=top2%>px;">
        <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">
            

            <div class="timeline_black font">
            <%=cont.Orgnization %>
            <br />
            <%=cont.JobTitle %>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",cont.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", cont.EndDate)%>
            </div>

            <div class="timeline_black">Jobs duties/ Lessons learned</div>
            <div class="timeline_gray font"><%=cont.LessonLearnd.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Competence applied</div>
            <div class="timeline_gray font"><%=cont.Competencies.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Success achieved</div>
            <div class="timeline_gray font"><%=cont.SuccessAchievement.Replace("\n", "<br>")%></div>

         <%if (IsManager)
           { %>
        <a href="javascript:void(0)" class="delete_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Delete</a>
        <a href="javascript:void(0)" class="edit_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Edit</a>
        <%} %>
        </div>
        </div>
    </div>
    <%} %>
    <%if (width1 > 0) { %>
    <div class="nonprofit" style="width:<%=width1 %>px; left:<%=left1%>px; top:<%=top1%>px;">
        <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            <%=cont.Orgnization %>
            <br />
            <%=cont.JobTitle %>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",cont.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", cont.EndDate)%>
            </div>

            <div class="timeline_black">Jobs duties/ Lessons learned</div>
            <div class="timeline_gray font"><%=cont.LessonLearnd.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Competence applied</div>
            <div class="timeline_gray font"><%=cont.Competencies.Replace("\n", "<br>")%></div>

            <div class="timeline_black">Success achieved</div>
            <div class="timeline_gray font"><%=cont.SuccessAchievement.Replace("\n", "<br>")%></div>

         <%if (IsManager)
           { %>
        <a href="javascript:void(0)" class="delete_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Delete</a>
        <a href="javascript:void(0)" class="edit_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Edit</a>
        <%} %>
        </div>
        </div>
    </div>
    <% } %>

<%} %>


<%
    List<int[]> byears = new List<int[]>();
    var blayer2 = 0;
    var blayer3 = 0;
      
    foreach (var edu in education)
    {
        int nindex = 0;

        foreach (int[] item in byears)
        {
            if (edu.StartDate.Value.Year > item[0] || (edu.StartDate.Value.Year == item[0] && edu.StartDate.Value.Month >= item[1]))
            {
                if (edu.StartDate.Value.Year < item[2] || (edu.StartDate.Value.Year == item[2] && edu.StartDate.Value.Month <= item[3]))
                {
                    nindex = item[4] + 1;
                }
            }
        }

        int[] tmpyear = { edu.StartDate.Value.Year, edu.StartDate.Value.Month, edu.EndDate.Value.Year, edu.EndDate.Value.Month, nindex };
        byears.Add(tmpyear);

        var spanyear = edu.EndDate.Value.Year - edu.StartDate.Value.Year;
        var spanmonth = edu.EndDate.Value.Month - edu.StartDate.Value.Month;
        var width = spanyear * 54 + (int)((spanmonth + 1) * 4.5) - 2;
        var left = (edu.StartDate.Value.Year - startyear) * 54 + (int)((edu.StartDate.Value.Month - 1) * 4.5);

        if (edu.StartDate.Value.Year < startyear)
        {
            width = (edu.EndDate.Value.Year - startyear + 1) * 54 + (int)((edu.EndDate.Value.Month - 12) * 4.5) - 2;
            left = 0;
        }

        var top1 = 78;
        var top2 = 233;
        var top3 = 388;
        var left1 = 0;
        var left2 = 0;
        var left3 = 0;
        var width1 = 0;
        var width2 = 0;
        var width3 = 0;

        if (left < 540)
        {
            top1 += 11 * nindex;
            left1 = left;

            if (left + width <= 540)
            {
                width1 = width;
            }
            else if (left + width >= 1080)
            {
                top2 += 11 * blayer2;
                top3 += 11 * blayer3;
                blayer2 += 1;
                blayer3 += 1;
                width3 = left + width - 1080;
                width2 = 540;
                width1 = width - width3 - width2;
            }
            else
            {
                top2 += 11 * blayer2;
                blayer2 += 1;
                width2 = left + width - 540;
                width1 = width - width2;
            }
        }
        else if (left >= 1080)
        {
            top3 += 11 * nindex;
            left3 = left - 1080;
            width3 = width;
        }
        else
        {
            top2 += 11 * nindex;
            left2 = left - 540;

            if (left + width >= 1080)
            {
                top3 += 11 * blayer3;
                blayer3 += 1;
                width3 = left + width - 1080;
                width2 = width - width3;
            }
            else
            {
                width2 = width;
            }
        }

        if (left3 + width3 > 538) width3 = 538 - left3;
        if (left2 + width2 > 538) width2 = 538 - left2;
        if (left1 + width1 > 538) width1 = 538 - left1;
%>
 
    <%if (width3 > 0) { %>
      <div class="tralning" style="width:<%=width3 %>px; left:<%=left3%>px; top:<%=top3%>px;">
          <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            <%=edu.Institute%>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",edu.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", edu.EndDate)%>
            </div>

            <div class="timeline_black">Major</div>
            <div class="timeline_gray font"><%= edu.Major%></div>

            <div class="timeline_black">Degree</div>
            <div class="timeline_gray font"><%= edu.Degree != "Degree" ? edu.Degree : ""%></div>

            <div class="timeline_black">Course title</div>
            <div class="timeline_gray font"><%= edu.CourseTitle %></div>

                <%if (IsManager)
                { %>
                <a href="javascript:void(0)" class="delete_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Delete</a>
                <a href="javascript:void(0)" class="edit_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Edit</a>
                <%} %>

          </div>
          </div>
      </div>
    <% } %>
    <%if (width2 > 0) { %>
      <div class="tralning" style="width:<%=width2 %>px; left:<%=left2%>px; top:<%=top2%>px;">
          <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            <%=edu.Institute%>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",edu.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", edu.EndDate)%>
            </div>

            <div class="timeline_black">Major</div>
            <div class="timeline_gray font"><%= edu.Major%></div>

            <div class="timeline_black">Degree</div>
            <div class="timeline_gray font"><%= edu.Degree != "Degree" ? edu.Degree : ""%></div>

            <div class="timeline_black">Course title</div>
            <div class="timeline_gray font"><%= edu.CourseTitle %></div>
      	                    

            <%if (IsManager)
            { %>
            <a href="javascript:void(0)" class="delete_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Delete</a>
            <a href="javascript:void(0)" class="edit_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Edit</a>
            <%} %>

          </div>
          </div>
      </div>
    <% } %>
    <%if (width1 > 0){ %>
      <div class="tralning" style="width:<%=width1 %>px;left:<%=left1%>px; top:<%=top1%>px;">
          <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            <%=edu.Institute%>
            <br />
            <%= string.Format("{0:yyyy-MM-dd}",edu.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", edu.EndDate)%>
            </div>

            <div class="timeline_black">Major</div>
            <div class="timeline_gray font"><%= edu.Major%></div>

            <div class="timeline_black">Degree</div>
            <div class="timeline_gray font"><%= edu.Degree != "Degree" ? edu.Degree : ""%></div>

            <div class="timeline_black">Course title</div>
            <div class="timeline_gray font"><%= edu.CourseTitle %></div>
      	    
                <%if (IsManager)
                { %>
                <a href="javascript:void(0)" class="delete_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Delete</a>
                <a href="javascript:void(0)" class="edit_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Edit</a>
                <%} %>

          </div>
          </div>
      </div>
    <% } %>

<%} %>




<%
    List<int[]> cyears = new List<int[]>();
    var clayer2 = 0;
    var clayer3 = 0;

    var careerindex = 0;    
      
    foreach (var careersummary in careersummarylist)
    {
        careerindex += 1;
        
        int nindex = 0;
        var mutpile2 = 0;
        var mutpile3 = 0;

        foreach (int[] item in cyears)
        {
            if (careersummary.StartDate.Value.Year > item[0] || (careersummary.StartDate.Value.Year == item[0] && careersummary.StartDate.Value.Month >= item[1]))
            {
                if (careersummary.StartDate.Value.Year < item[2] || (careersummary.StartDate.Value.Year == item[2] && careersummary.StartDate.Value.Month <= item[3]))
                {
                    nindex = item[4] + 1;
                }
            }
        }

        int[] tmpyear = { careersummary.StartDate.Value.Year, careersummary.StartDate.Value.Month, careersummary.EndDate.Value.Year, careersummary.EndDate.Value.Month, nindex };
        cyears.Add(tmpyear);

        var spanyear = careersummary.EndDate.Value.Year - careersummary.StartDate.Value.Year;
        var spanmonth = careersummary.EndDate.Value.Month - careersummary.StartDate.Value.Month;
        var width = spanyear * 54 + (int)((spanmonth + 1) * 4.5) - 2;
        var left = (careersummary.StartDate.Value.Year - startyear) * 54 + (int)((careersummary.StartDate.Value.Month - 1) * 4.5);

        if (careersummary.StartDate.Value.Year < startyear)
        {
            width = (careersummary.EndDate.Value.Year - startyear + 1) * 54 + (int)((careersummary.EndDate.Value.Month - 12) * 4.5) - 2;
            left = 0;
        }


        var top1 = 116;
        var top2 = 271;
        var top3 = 426;
        var left1 = 0;
        var left2 = 0;
        var left3 = 0;
        var width1 = 0;
        var width2 = 0;
        var width3 = 0;

        if (left < 540)
        {
            top1 += 11 * nindex;
            left1 = left;

            if (left + width <= 540)
            {
                width1 = width;
            }
            else if (left + width >= 1080)
            {
                top2 += 11 * clayer2;
                top3 += 11 * clayer3;
                clayer2 += 1;
                clayer3 += 1;
                width3 = left + width - 1080;
                width2 = 540;
                width1 = width - width3 - width2;
            }
            else
            {
                top2 += 11 * clayer2;
                clayer2 += 1;
                width2 = left + width - 540;
                width1 = width - width2;
            }
        }
        else if (left >= 1080)
        {
            top3 += 11 * nindex;
            left3 = left - 1080;
            width3 = width;
        }
        else
        {
            top2 += 11 * nindex;
            left2 = left - 540;

            if (left + width >= 1080)
            {
                top3 += 11 * clayer3;
                clayer3 += 1;
                width3 = left + width - 1080;
                width2 = width - width3;
            }
            else
            {
                width2 = width;
            }
        }

        if (left3 + width3 > 538) width3 = 538 - left3;
        if (left2 + width2 > 538) width2 = 538 - left2;
        if (left1 + width1 > 538) width1 = 538 - left1;
%>
 
 <%
        var showmode = 0;
        if (careerindex == careersummarylist.Count)
        {
            if (width1 > 0) showmode = 1;
            if (width2 > 0) showmode = 2;
            if (width3 > 0) showmode = 3;
        }          
 %>

    <%if (width3 > 0){ %>
    <div class="employment" style="width:<%=width3 %>px;left:<%=left3%>px; top:<%=top3%>px;">
            <div class="timeline_view <%=showmode ==3 ? "showtimeline" : ""%>">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    <%=careersummary.Company%>
                    <br />
                    <%= careersummary.JobTitle%>
                    <br />
                    <%= careersummary.JobIndustry%>
                    <br />
                    <%= string.Format("{0:yyyy-MM-dd}", careersummary.StartDate)%> to <%= string.Format("{0:yyyy-MM-dd}", careersummary.EndDate)%>
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font"><%= careersummary.LessonLearnd.Replace("\n", "<br>")%></div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font"><%= careersummary.Competencies.Replace("\n", "<br>")%></div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font"><%= careersummary.SuccessAchievement.Replace("\n", "<br>")%></div>

                 <%if (IsManager)
                   { %>
                    <a href="javascript:void(0)" class="delete_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Delete</a>
                    <a href="javascript:void(0)" class="edit_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Edit</a>
                 <%} %>
            </div>     
            </div>         
      </div>
    <% } %>
    <%if (width2 > 0){ %>
    <div class="employment" style="width:<%=width2 %>px;left:<%=left2%>px; top:<%=top2%>px;">
            <div class="timeline_view <%=showmode ==2 ? "showtimeline" : ""%>">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">
                
                <div class="timeline_black font">
                    <%=careersummary.Company%>
                    <br />
                    <%= careersummary.JobTitle%>
                    <br />
                    <%= careersummary.JobIndustry%>
                    <br />
                    <%= string.Format("{0:yyyy-MM-dd}", careersummary.StartDate)%> to <%= string.Format("{0:yyyy-MM-dd}", careersummary.EndDate)%>
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font"><%= careersummary.LessonLearnd.Replace("\n", "<br>")%></div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font"><%= careersummary.Competencies.Replace("\n", "<br>")%></div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font"><%= careersummary.SuccessAchievement.Replace("\n", "<br>")%></div>

                 <%if (IsManager)
                   { %>
                    <a href="javascript:void(0)" class="delete_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Delete</a>
                    <a href="javascript:void(0)" class="edit_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Edit</a>
                 <%} %>
            </div>     
            </div>         
      </div>
    <% } %>
    <%if (width1 > 0){ %>
     <div class="employment" style="width:<%=width1 %>px;left:<%=left1%>px; top:<%=top1%>px;">   
            <div class="timeline_view <%=showmode ==1 ? "showtimeline" : ""%>">
                    <div class="timeline_arrow2"></div>
                    <div class="timeline_info">
                    	
                        <div class="timeline_black font">
                            <%=careersummary.Company%>
                            <br />
                            <%= careersummary.JobTitle%>
                            <br />
                            <%= careersummary.JobIndustry%>
                            <br />
                            <%= string.Format("{0:yyyy-MM-dd}", careersummary.StartDate)%> to <%= string.Format("{0:yyyy-MM-dd}", careersummary.EndDate)%>
                        </div>

                        <div class="timeline_black">Position description</div>
                        <div class="timeline_gray font"><%= careersummary.LessonLearnd.Replace("\n", "<br>")%></div>

                        <div class="timeline_black">Competencies applied</div>
                        <div class="timeline_gray font"><%= careersummary.Competencies.Replace("\n", "<br>")%></div>

                        <div class="timeline_black">Success achieved</div>
                        <div class="timeline_gray font"><%= careersummary.SuccessAchievement.Replace("\n", "<br>")%></div>

                         <%if (IsManager)
                           { %>
                         <a href="javascript:void(0)" class="delete_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Delete</a>
                         <a href="javascript:void(0)" class="edit_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Edit</a>
                         <%} %>
                      </div>      
                    </div>   
      </div>
    <% } %>

<%} %>
      
                                                                                         

            <%for (int i = 0; i < 30; i++)
            { %>
            <div class="timeline_box  <%=i<yearspan?"box_active":"" %>  <%=i%10==0?"box_first ":"" %>  <%=i%10==9?"box_end ":"" %>">
            <div class="timeline_title"><%=startyear + i%></div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        <%} %>
    </div>
    <div class="competence_explain">
        <div class="explain_line">
            <div class="explain_icon1"></div><div class="explain_title">Employment</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon2"></div><div class="explain_title">Non-Profit</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon3"></div><div class="explain_title">Education</div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {

        //show last employment detail
        $(".showtimeline").parent().css("z-index", "109");
        $(".showtimeline").show();

        if (window.top != window.self) {
            var yyy = parseInt($(".showtimeline").parent().css("top")) + 65 + $(".showtimeline").height();
            if (yyy > 580) $(".showtimeline").addClass("timeline_view_up");
        }

        var xxx = parseInt($(".showtimeline").parent().css("left")) + $(".showtimeline").parent().width() / 2 + 140;
        var lll = $(".showtimeline").parent().width() / 2 - 140;
        var ccc = 115;

        if (window.top != window.self) {
            if (xxx > 568) {
                lll = lll - xxx + 568;
                ccc = 115 + xxx - 568;
            }
        }

        $(".showtimeline").css("left", lll + "px");
        $(".showtimeline").find(".timeline_arrow2").css("left", ccc + "px");


        //action
        $(".nonprofit").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            if (window.top != window.self) {
                var yy = parseInt($(this).css("top")) + 65 + $(this).find(".timeline_view").height();
                if (yy > 580) $(this).find(".timeline_view").addClass("timeline_view_up");
            }

            var xx = parseInt($(this).css("left")) + $(this).width() / 2 + 140;
            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            if (window.top != window.self) {
                if (xx > 568) {
                    ll = ll - xx + 568;
                    cc = 115 + xx - 568;
                }
            }

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "108");
            $(this).find(".timeline_view").hide();
        });

        $(".tralning").hover(function () {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            if (window.top != window.self) {
                var yy = parseInt($(this).css("top")) + 65 + $(this).find(".timeline_view").height();
                if (yy > 580) $(this).find(".timeline_view").addClass("timeline_view_up");
            }

            var xx = parseInt($(this).css("left")) + $(this).width() / 2 + 140;
            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            if (window.top != window.self) {
                if (xx > 568) {
                    ll = ll - xx + 568;
                    cc = 115 + xx - 568;
                }
            }

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "107");
            $(this).find(".timeline_view").hide();
        });

        $(".employment").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            if (window.top != window.self) {
                var yy = parseInt($(this).css("top")) + 65 + $(this).find(".timeline_view").height();
                if (yy > 580) $(this).find(".timeline_view").addClass("timeline_view_up");
            }

            var xx = parseInt($(this).css("left")) + $(this).width() / 2 + 140;
            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            if (window.top != window.self) {
                if (xx > 568) {
                    ll = ll - xx + 568;
                    cc = 115 + xx - 568;
                }
            }

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "106");
            $(this).find(".timeline_view").hide();
        });

        $(".timeline_view").hover(function () {
            $(this).show();

        }, function () {
            if ($(this).parent().attr('class') == 'nonprofit') {
                $(this).parent().css("z-index", "108");
            }
            else if ($(this).parent().attr('class') == 'tralning') {
                $(this).parent().css("z-index", "107");
            }
            else {
                $(this).parent().css("z-index", "106");
            }

            $(this).hide();
        });

        $('#editProfile').live('click', function () {
            $.getJSON('/Competence/_MyCompetenceAdd', { "expertid": 105, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        $(".delete_record").click(function () {
            var type = $(this).attr("x:type");
            var recordid = $(this).attr("x:recordid");

            confirmbox("Do you want to delete this record?", function () {
                $.postJSON('<%=Url.Action("CompetenceDelete","Competence") %>', { recordid: recordid, type: type }, function (data) {
                    window.location.reload();
                    return false;
                });
            });
        });

        $(".edit_record").click(function () {
            var recordid = $(this).attr("x:recordid");
            var type = $(this).attr("x:type");
            $.getJSON('/Competence/_MyCompetenceUpdate', { "recordid": recordid, "type": type, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
    function DeleteCompetency(obj) {
        $.getJSON('<%=Url.Action("DeleteCompetency","Competence") %>', { "competencyID": obj.id, random: Math.random() }, function (data) {
            if (data) {
                $('#showCompetency').find("#" + obj.id).remove();
                if ($('#showCompetency').find(".value_item_active").length < 6) {
                    $('#addCompetency').css('display', '');
                }
                else {
                    $('#addCompetency').css('display', 'none');
                }
            }
        });
    }
</script>