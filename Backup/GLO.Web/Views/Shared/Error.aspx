﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Error - My ASP.NET MVC Application
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    
<div class="glo_body">
 <div class="glo_boxer">
 
    <div style="padding:10px 20px;">
        <h2>Error Message</h2>
        An error occurred while processing your request.
    </div>
</div>
</div>
</asp:Content>
