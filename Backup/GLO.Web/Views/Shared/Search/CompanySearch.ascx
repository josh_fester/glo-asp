﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Users>>" %>
<table class="search_item">
    <tr>
        <td>
            Keyword
        </td>
        <td>
            <input type="text" id="txtCompanyKeyWord" class="keyword" value="<%=Request.QueryString["c"] %>">
        </td>
    </tr>
     <tr>
        <td>
            Location
        </td>
        <td>
            <div class="multiple_box">
                <input class="multiple_value" id="CompanyLocation" type="text" readonly="readonly" value="All Location" />
                <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                <div class="multiple_wrapper multiple_wrapper_location"></div> 
            </div>
        </td>
    </tr>
      <tr>
        <td>
            Industry
        </td>
        <td class="item_option_industry">
              <div class="multiple_box">
               <input class="multiple_value" id="Industry" type="text" readonly="readonly" value="All Industry" />
               <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
               <div class="multiple_wrapper multiple_wrapper_industry">                                                                                        
               </div>                                
           </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <input type="button" value="Search" class="bluebtn" id="CompanySearch" />
        </td>
    </tr>
</table>
<div class="result_title">
    We found
    <%=Model.Count() %>
    Companies.
</div>
<div class="result_container">
    <%foreach (var company in Model)
      { %>
    <div id="<%=company.UserID %>">
         <% bool showTagButton = true;
                foreach (var taggeduser in (List<GLO.Data.Models.GLO_TagUsers>)ViewBag.TagUserList)
              {
                  if (taggeduser.TaggedUserID == company.UserID)
                  {
                      showTagButton = false;
                  }
              }
                bool showRequestTagButton = true;
                foreach (var taggeduser in company.GLO_TagUsers)
                {
                    if (taggeduser.TaggedUserID.ToString() == ViewBag.UserID.ToString())
                    {
                        showRequestTagButton = false;
                    }
                }
                bool showRequestRecommendButton = true;
                foreach (var recommendUser in company.GLO_Recommendation1)
                {
                    if (recommendUser.UserID.ToString() == ViewBag.UserID.ToString())
                    {
                        showRequestRecommendButton = false;
                    }
                }
                var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
                if (user.UserData.UserType == 4 || user.UserData.Approved == false)
                {
                    showTagButton = false;
                    showRequestTagButton = false;
                    showRequestRecommendButton = false;
                }
              %>
        <div class="message_line">
            <div class="message_usericon">
             <% if (user.UserData.Approved)
                { %>
                <a href="javascript:void(0);" onclick="return OpenLightFrame('/company/companyindex?companyid=<%=company.UserID %>');">
                    <img src="/images/<%=!String.IsNullOrEmpty(company.Icon)?company.Icon:"user_none.gif" %>" /></a>
            <%}
                else
                { %>
                <img src="/images/<%=!String.IsNullOrEmpty(company.Icon)?company.Icon:"user_none.gif" %>" />
            <%} %>
            </div>
            <div class="message_action">
                <table>
                    <tr>
                        <td>
                        <% if(user.UserData.Approved){ %>
                            <a href="javascript:void(0);" onclick="return OpenLightFrame('/company/companyindex?companyid=<%=company.UserID %>');">
                            <%=company.NickName%></a>
                        <%} else{ %>
                         <%=company.NickName%>
                        <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= company.GLO_Company == null ? "" : company.GLO_Company.Website%>
                        </td>
                    </tr>
                        <tr>
                            <td>                            
                                <%if (ViewBag.UserType.ToString() != "3"){ %>
                                <%if (!showTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                                <% }
                                  else
                                {%>
                                    <input type="button" class="bluebtn" value="Tag" onclick="TagUser('<%=company.UserID %>')" />
                                <%} %>  
                                
                                <%if (!showRequestTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                                <% }
                                  else
                                {%>                              
                                  <input type="button" class="bluebtn" value="Request Tag" onclick="RequestTag('<%=company.UserID %>')" />
                                  <%} %> 
                                <%} %>   
                                <%if (ViewBag.UserType.ToString() != "3")
                                  {
                                  if(showRequestRecommendButton)
                                  { %>
                                <input type="button" class="bluebtn" value="Request referral" onclick="RequestRecommend('<%=company.UserID %>')" />                                
                                <% }
                                  else
                                {%> 
                                    <input type="button" class="graybtn" value="Request referral" onclick="return false;" />
                                <%} 
                                  }%>
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </div>
    <%} %>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $.getJSON('/Home/_GetLocationList', null, function (data) {
            {
                $('.multiple_wrapper_location').html(data.Html);
            }
        });
        $.getJSON('/Home/_GetIndustryList', null, function (data) {
            {
                $('.multiple_wrapper_industry').html(data.Html);
            }
        });
        $("#CompanySearch").click(function () {
            var keyWord = $("#txtCompanyKeyWord").val();
            var location = $("#CompanyLocation").val();
            if (location == "All Location") {
                location = "";
            }
            var industry = $("#Industry").val();
            if (industry == "All Industry")
                industry = "";
            var postData = 'keyword=' + keyWord + '&location=' + location + '&industry=' + industry.replace('&', '__');
            $.post("/Home/CompanySearch", postData, function (data) {
                $('#CompanyList').html(data);
                $("#txtCompanyKeyWord").val(keyWord);
                if (location != "")
                    $("#CompanyLocation").val(location);
                if (industry != "")
                    $("#Industry").val(industry);
            });
        });
    });
       
</script>
