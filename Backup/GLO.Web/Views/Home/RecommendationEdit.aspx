﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Recommendation>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RecommendationEdit
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">     
    <script src="<%=Url.Content("~/Scripts/jquery-ui-1.8.11.min.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <%  var user = Model.GLO_Users;
         var username = user.NickName;
         var type = user.TypeID;
         var url = "";
         if (user.TypeID == 1)
         {
             url = "/leader/index?leaderid=" + user.UserID;
         }
         else if (user.TypeID == 2)
         {
             url = "/expert/myvalue?expertid=" + user.UserID;
         }
         else if (user.TypeID == 3)
         {
             url = "/company/companyindex?companyid=" + user.UserID;
         }%>
    <%: Html.HiddenFor(m=>m.RecommendID) %>
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Welcome to the GLO community of Good Leaders Online and thank you for recommending <a href="<%=url %>" target="_blank"><%=username%></a>'s work
            </div>
            <div class="boxer_recommendatioon">
                <div class="r_info_myname">
                    My Name</div>
                <div class="r_info_content">
                <%string[] Name = Model.UserName.Split(' '); %>
                    <input class="txtbox_myname" type="text" id="MyFirstName" value="<%=Name[0] %>" />
                    <input class="txtbox_myname" type="text" id="MyLastName" value="<%=Name[1] %>" />
                </div>
                <div class="r_info_item">
                    How do I like <%=username%>'s <%=type==1?"vision":"work" %>?
                </div>
                <div class="r_info_content">
                <%for (int i = 0; i < 10; i++)
                  {
                      int count = int.Parse(Model.Vision);
                      if (i < count)
                      { %>
                <img src="/Styles/images/icon_great1.gif"  class="recommend_vision" x:index="<%=i+1 %>" style="cursor: pointer" />
                <%}
                      else
                      { %>
                <img src="/Styles/images/icon_great2.gif"  class="recommend_vision" x:index="<%=i+1 %>" style="cursor: pointer" />
                <%}
                  } %>
                </div>

                  <%string display = user.TypeID == 1 ? "inline" : "none";%>
                        <div style="display:<%=display%>">
                        <div class="r_info_item" id="recommend_values">My experience with <%=username%>'s values</div>
                         <div class="r_info_content" id="recommend_value">
                            <% Model.ExperienceValue = string.IsNullOrEmpty(Model.ExperienceValue) ? ";" : Model.ExperienceValue;
                                string[] experiencevalue = Model.ExperienceValue.Split(';');%>
                            <% foreach (var item in (List<GLO.Data.Models.GLO_Competencies>)ViewBag.Values)
                               {
                                   var classname = "value_item";
                                   foreach (var competenceId in experiencevalue)
                                   {
                                       if (item.CompetencyID.ToString() == competenceId)
                                       {
                                           classname = "value_item_active";
                                       }
                                   }
                            %>
                            <div class='<%=classname%>' onclick="ResetValue(this);" id="<%=item.CompetencyID %>">
                                <%=item.CompetencyContent %>
                            </div>
                            <%} %>                                 
                        </div>

                        <div class="r_info_item">Other values I have for <%=username%></div>
                        <div class="r_info_content" id="recommend_values_other"> 
                         <%
                              Model.OtherValue = string.IsNullOrEmpty(Model.OtherValue) ? ";" : Model.OtherValue;
                                string[] othervalue = Model.OtherValue.Split(';');%>
                            <% foreach (var item in (List<GLO.Data.Models.GLO_Competencies>)ViewBag.OtherValues)
                               {
                                   var classname = "value_item";
                                   foreach (var competenceId in othervalue)
                                   {
                                       if (item.CompetencyID.ToString() == competenceId)
                                       {
                                           classname = "value_item_active";
                            %>
                             <div class='<%=classname %>' id="<%=item.CompetencyID %>">
                        <div class="user_value_delete">
                            <a href="javascript:void(0);" title="delete" id="<%=item.CompetencyID %>" onclick="DeleteValues(this);">
                                <img src="/Styles/images/icon_cancel.gif" /></a></div>
                        <%=item.CompetencyContent %>
                    </div>

                            <%
                                       }
                                   }
                               }%>      
                        </div>  
                        <div id="addValues" class="user_value_add">
                            New Value:<br />
                            <input type="text" value="" class="txt_itemadd" id="txtValues" maxlength="20" /><input type="hidden" id="hidValuesCollection" /><input type="button" id="btnAddValues" value="Add" class="bluebtn" />
                        </div>  
                        </div>

                <div class="r_info_item">
                    My confirmation on <%=username%>'s competencies</div>
                <div class="r_info_content" id="recommend_competence">
                    <% 
                        string[] ocompetence = Model.Competence.Split(';');%>
                    <% foreach (var item in (List<GLO.Data.Models.GLO_Competencies>)ViewBag.CompetenciesList)
                       {
                           var classname = "value_item";
                           foreach (var competenceId in ocompetence)
                           {
                               if (item.CompetencyID.ToString() == competenceId)
                               {
                                   classname = "value_item_active";
                               }
                           }
                    %>
                    <div class='<%=classname%>' onclick="ResetValue(this);" id="<%=item.CompetencyID %>">
                        <%=item.CompetencyContent %>
                    </div>
                    <%} %>
                </div>
                <div class="r_info_item">
                    Other competencies
                    <%=username%>
                    has</div>
                <div class="r_info_content" id="recommend_other">
                    <% 
                        string[] othercompetence = Model.OtherCompetence.Split(';');%>  
                    <% foreach (var item in (List<GLO.Data.Models.GLO_Competencies>)ViewBag.OtherCompetenciesList)
                       {
                           var classname = "value_item";
                           foreach (var competenceId in othercompetence)
                           {
                               if (item.CompetencyID.ToString() == competenceId)
                               {
                                   classname = "value_item_active";                              
                           
                    %>
                    <div class='<%=classname %>' id="<%=item.CompetencyID %>">
                        <div class="user_value_delete">
                            <a href="javascript:void(0);" title="delete" id="<%=item.CompetencyID %>" onclick="DeleteCompetency(this);">
                                <img src="/Styles/images/icon_cancel.gif" /></a></div>
                        <%=item.CompetencyContent %>
                    </div>
                    <%}
                           }
                       }%>
                </div>
                <div id="addCompetency" class="user_value_add">
                    New Competency:<br />
                    <input type="text" value="" class="txt_itemadd" id="txtCompetencies" maxlength="20" /><input
                        type="hidden" id="hidValueID" /><input type="button" id="btnAddCompetencies" value="Add"
                            class="bluebtn" />
                </div>
                <div class="r_info_item">
                    How
                    <%=username%>
                    makes a difference?</div>
                <div class="r_info_content">
                    <textarea id="recommend_differnce" class="area_profile font"><%=Model.Different %></textarea></div>
                <input type="hidden" id="hid_vision" value="<%=Model.Vision %>" />
            </div>
            <div class="me_apply">
                <a href="javascript:void(0);" id="recommend_comment" x:type="2">
                    <img src="/Styles/images/icon_apply.gif" /></a></div>
        </div>
    </div>
    <script type="text/javascript"> 
        var nameArry = [];
            $(document).ready(function () {

            if (window.top != window.self) {
                var iheight = $('.glo_boxer').height();
                window.parent.$('.frameWrapper').css("height", (iheight + 50) + "px");
                window.parent.$('#lightboxwrapper').css("min-height", (iheight + 200) + "px");
            }

            $('#MyFirstName').focus(function () {
                if ($(this).val() == 'First name') $(this).val('');
            });
            $('#MyFirstName').blur(function () {
                if ($(this).val() == '') $(this).val('First name');
            });

            $('#MyLastName').focus(function () {
                if ($(this).val() == 'Last name') $(this).val('');
            });
            $('#MyLastName').blur(function () {
                if ($(this).val() == '') $(this).val('Last name');
            });

                if($('#recommend_other').find(".value_item_active").length==6)
                {
                    $('#addCompetency').css('display', 'none');
                }
                GetData();
                $("#txtCompetencies,#txtValues").autocomplete({
                    minLength: 1,
                    autoFocuus: true,
                    source: function (req, res) {
                        var name = req.term,
                                    result = [];
                        if ($.trim(req.term) != "") {
                            var searchResult = $.map(
                                        (name ? $.grep(nameArry, function (value) {
                                            return value.name.toString().toLocaleLowerCase().indexOf(name) >= 0;
                                        }) : nameArry),
                                        function (value) {
                                            return { label: value.name, tc: value.id };
                                        });
                            result = result.concat($.makeArray(searchResult));
                        }
                        res(result);
                    }
                });
                  $("#btnAddCompetencies").click(function () {
                        var userID = '<%=Model.UserID %>';
                        var value = $('#txtCompetencies').val().trim();
                        if (value == "") {
                            return false;
                        }
                        $.postJSON('<%=Url.Action("AddCompetency","Competence") %>', { userID: userID, value: value,type:2 }, function (data) {
                            var html = "<div class='value_item_active' id=\"" + data + "\">" + value + "<div class=\"user_value_delete\"><a href=\"javascript:void(0);\" title=\"delete\" id=\"" + data + "\"  onclick=\"DeleteCompetency(this);\"><img src=\"/Styles/images/icon_cancel.gif\" /></a></div></div>";
                            if (data != "") {
                                $('#hidValueID').val(data);
                                if ($('#recommend_other').find("#" + data).length == 0) {
                                    $('#recommend_other').append(html);
                                }
                                if($('#recommend_other').find(".value_item_active").length==6)
                                {
                                    $('#addCompetency').css('display', 'none');
                                }
                                $('#txtCompetencies').val("");
                            }
                            else {
                                alertbox("Add failed.");
                            }
                        });
                });
                 $("#btnAddValues").click(function () {
                        var userID = '<%=Model.UserID %>';
                        var value = $('#txtValues').val().trim();
                        if (value == "") {
                            return false;
                        }
                        $.postJSON('<%=Url.Action("AddCompetency","Competence") %>', { userID: userID, value: value,type:4 }, function (data) {
                            var html = "<div class='value_item_active' id=\"" + data + "\">" + value + "<div class=\"user_value_delete\"><a href=\"javascript:void(0);\" title=\"delete\" id=\"" + data + "\"  onclick=\"DeleteValues(this);\"><img src=\"/Styles/images/icon_cancel.gif\" /></a></div></div>";
                            if (data != "") {
                                $('#hidValuesCollection').val(data);
                                if ($('#recommend_values_other').find("#" + data).length == 0) {
                                    $('#recommend_values_other').append(html);
                                    $('#txtValues').val("");
                                }
                                if($('#recommend_values_other').find(".value_item_active").length==6)
                                {
                                    $('#btnAddValues').css('display', 'none');
                                }
                            }
                            else {
                                alertbox("Add failed.");
                            }
                        });
                });

                $("#recommend_comment").click(function () {
                    if ($('#MyFirstName').val()=="" ||$('#MyFirstName').val()=="First name") {
                        $("#MyFirstName").addClass("input_error");
                        return false;
                    }
                    else {
                        $("#MyFirstName").removeClass("input_error");
                    }
                    if ($('#MyLastName').val()==""||$('#MyLastName').val()=="Last name") {
                        $("#MyLastName").addClass("input_error");
                        return false;
                    }
                    else {
                        $("#MyLastName").removeClass("input_error");
                    }
                   var competence="";
                   var classname ="value_item_active";
                   $('#recommend_competence div').each(function(i){
                     if($(this).attr("class").trim().indexOf(classname)>=0)
                     {
                       competence +=$(this).attr("id");
                       competence +=";";
                     }                  
                   });
                 var othercompetence="";
                  $('#recommend_other div').each(function(i){
                     if($(this).attr("class").trim().indexOf(classname)>=0)
                     {
                       othercompetence +=$(this).attr("id");
                       othercompetence +=";";
                     }                  
                   });
                  var experiencevalue="";
                 $('#recommend_value div').each(function(i){
                     if($(this).attr("class").trim().indexOf(classname)>=0)
                     {
                       experiencevalue +=$(this).attr("id");
                       experiencevalue +=";";
                     }                  
                   });
                var othervalue="";
                 $('#recommend_values_other div').each(function(i){
                     if($(this).attr("class").trim().indexOf(classname)>=0)
                     {
                       othervalue +=$(this).attr("id");
                       othervalue +=";";
                     }                  
                   });
                 var recommenddata ={
                         'RecommendID':<%=Model.RecommendID %>,
                         'UserID':<%=Model.UserID %>,
                         'RecommendUserID':<%=Model.RecommendUserID %>,
                         'UserName':$("#MyFirstName").val()+" "+$("#MyLastName").val(),
                         'Vision':$("#hid_vision").val(),
                         'ExperienceValue':experiencevalue,
                         'OtherValue':othervalue,
                         'Competence':competence,
                         'OtherCompetence':othercompetence,
                         'Different':$("#recommend_differnce").val()
                         };
                    var userType=<%=Model.GLO_Users.TypeID %>
                    $.post('<%=Url.Action("RecommendationEdit") %>', { recommend:$.toJSON(recommenddata) }, function (data) {
                         alertbox("Successfully recommended!", function() {
                             if(userType=="2")
                             {
                                window.location.href = '<%=Url.Action("MyRecommendation","Expert") %>' + "?expertid=" + data.UserID;
                             }
                             else if(userType=="1"){
                                window.location.href = '<%=Url.Action("MyRecommendation","Leader") %>' + "?leaderid=" + data.UserID;
                             }
                             else if(userType=="3"){
                                window.location.href = '<%=Url.Action("CompanyIndex","Company") %>' + "?companyid=" + data.UserID;
                             }
                         });

                     });
                });
                $(".recommend_vision").click(function(){
                  var index=  $(this).attr("x:index")
                  $("#hid_vision").val(index);
                  for(i=1;i<11;i++)
                  {
                     if(i<index)
                     {
                          $(".r_info_content>img:eq(" + i + ")").attr("src","/Styles/images/icon_great1.gif")
                     }
                     else
                     {
                         $(".r_info_content>img:eq(" + i + ")").attr("src","/Styles/images/icon_great2.gif")
                     }
                  }
                });                
            });
            function ResetValue(obj) {
                if (obj.className.indexOf('value_item_active')> -1) {
                    obj.className = obj.className.replace('value_item_active', 'value_item');
                }
                else {
                    obj.className = obj.className.replace('value_item', 'value_item_active');
                }
            }
            function GetData() {
                var userID='<%=Model.UserID %>';
                $.post("/Competence/GetOtherCompetencyList", "userID="+userID, function (data) {
                    $.each(data, function () {
                        var nameitem = { name: this.Text, id: this.Value };
                        nameArry.push(nameitem);            
                    });
                });
            }            
            function DeleteCompetency(obj) {
                $.getJSON('<%=Url.Action("DeleteCompetency","Competence") %>', { "competencyID": obj.id, random: Math.random() }, function (data) {
                    if (data) {
                        $('#recommend_other').find("#" + obj.id).remove();
                        if($('#recommend_other').find("div[class='value_item'],div[class='value_item_active']").length<6)
                        {
                            $('#addCompetency').css('display', '');
                        }
                        else {
                            $('#addCompetency').css('display', 'none');
                        }
                    }
                });
            }
             function DeleteValues(obj) {
                $.getJSON('<%=Url.Action("DeleteCompetency","Competence") %>', { "competencyID": obj.id, random: Math.random() }, function (data) {
                    if (data) {
                        $('#recommend_values_other').find("#" + obj.id).remove();
                        if ($('#recommend_values_other').find(".value_item_active").length < 6) {
                            $('#btnAddValues').css('display', '');
                        }
                        else {
                            $('#btnAddValues').css('display', 'none');
                        }
                    }
                });
            }
    </script>
</asp:Content>
