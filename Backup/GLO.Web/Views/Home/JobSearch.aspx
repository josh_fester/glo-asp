﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<List<GLO.Data.Models.GLO_CompanyJob>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Jobs Search
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="KeyWordContent" runat="server">
    <meta name="keywords" content="China job portal,jobs online,China jobs online" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server"> 
 
<link href="<%=Url.Content("~/JS/uploadify/uploadify.css")%>" rel="stylesheet" type="text/css" />
     <script src="<%=Url.Content("~/JS/uploadify/jquery.uploadify.min.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <div class="search_wrapper">
    <%Html.RenderPartial("~/Views/Shared/UnregisteredUserLogin.ascx", new ViewDataDictionary {{"type","searchjob"}});%>
    	<div class="search_header1">Jobs</div>
        <div class="search_boxer1">      
            <div class="boxer1_left">
                        	<table class="search_item">
                            	<tr>
                                	<td colspan="2" class="item_headline">Custom job search</td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Keyword</td>
                                	<td class="item_option"><input type="text" id="KeyWord" class="keyword" value="<%=Request.QueryString["keyword"] %>"></td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Location</td>
                                	<td class="item_option">
                                        <div class="multiple_box">
                                            <input class="multiple_value searchlocation" id="SelectLocation" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Location)?ViewBag.Location:"All Location" %>' />
                                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />
                                            <div class="multiple_wrapper">
                                                <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                                            </div>    
                                        </div>
                                    </td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Job Function</td>
                                	<td class="item_option">
                                        <div class="select_box">
                                            <input class="select_value" id="SelectFunction" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Func)?ViewBag.Func:"All Function" %>' />
                                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
            	                            <div class="select_option">
                                                <ul>
                                                    <li>All Function</li>
                                                    <%foreach (var func in (List<GLO.Data.Models.GLO_Functions>)ViewBag.FuncList)
                                                      { %>
                                                    <li><%=func.FunctionName %></li>
                                                    <%} %>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                    <input type="button" value="Search" class="bluebtn" onclick="return search();" /> &nbsp;&nbsp;<a href="/Home/AdvanceJobSearch?keyword=<%=Request.QueryString["keyword"]%>" target="_self">Advance search</a>
                                    </td>
                                </tr>
                            </table>
                            <div class="result_title">
                                We found <%=Model.Count %> Good Jobs.
                            </div>
                            <div class="result_container">
                                <table class="result">
                                    <%foreach (var job in Model){ %>
                            	<tr>
                                	<td><a href="#" x:jobid="<%=job.JobID %>" class="JobTitle"><%=job.JobTitle %></a></td>
                                	<td><%=job.Location %></td>
                                </tr>
                                <%} %>
                                </table>
                            </div>
            </div>
           <div class="boxer1_jobinfo">
          

              <%Html.RenderPartial("~/Views/Shared/Company/JobDetail.ascx", Model.FirstOrDefault() == null ? new GLO.Data.Models.GLO_CompanyJob() : Model.FirstOrDefault());%>
           </div>  
        </div>
        <div class="search_footer1"></div>
    </div>

   <script type="text/javascript">
       $(document).ready(function () {
           /*open dropdown*/
           $(".select_box").click(function (event) {
               event.stopPropagation();
               $(".select_option").hide();
               $(this).find(".select_option").toggle();
           });
           /*close dropdown*/
           $(document).click(function () {
               $('.select_option').hide();
           });
           /*set value*/
           $(".select_option li").click(function (event) {
               event.stopPropagation();
               $(".select_option").hide();

               var value = $(this).text();
               $(this).parent().parent().parent().find(".select_value").val(value);
           });



           $(".JobTitle").click(function () {
               $.getJSON('_GetJobDetail', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                   {
                       $('.boxer1_jobinfo').html(data.Html);
                   }
               });
           });
          
           var location = '<%=Request.QueryString["location"] %>';
           if(location !="")
           {
               $("#SelectLocation").val(location);
               $("#SelectLocation+.select_value").val(location);
           }

           var func = '<%=Request.QueryString["func"] %>';
           if (func != "")
           {
               $("#SelectFunction").val(func);
               $("#SelectFunction+.select_value").val(func);
           }


       });

       function search()
       {
              var location = $(".searchlocation").val();
               var keyword = $("#KeyWord").val();
               var func = $("#SelectFunction").val();
               window.location.href = '<%=Url.Action("JobSearch") %>' + "?keyword=" + keyword + "&location=" + location + "&func=" + func+"&searchtype=Jobs";
               return false;        

       }

       
   </script>
<div id="lightboxwrapper">
        
</div>
</asp:Content>


