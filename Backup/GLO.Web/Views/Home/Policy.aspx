﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Policy>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Policy
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
               GLO Policy
            </div>
            <div class="boxer_content">
                
           <%=ViewBag.Policy%>

            </div> 
        </div>
    </div>
</asp:Content>
