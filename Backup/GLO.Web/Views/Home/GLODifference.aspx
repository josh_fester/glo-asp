﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Difference>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    GLO Difference
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO Difference
            </div>
            <div class="boxer_content">

                    <% foreach (var item in Model)
                       { %>
                    <div class="line_q2"><%= HttpUtility.HtmlDecode(item.DifferenceContent)%></div>
                    <%} %>

            </div>
            <div class="boxer_agree">
                <div class="hpylink_bluebtn">
                   <a href="/Home/RegisterGuide" class="Register">I WANT TO JOIN GLO</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
