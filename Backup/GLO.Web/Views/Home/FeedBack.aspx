﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_FeedBack>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FeedBack
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                We appreciate your feedback
            </div>
            <div class="boxer_content">
                <div id="FeedbackForm">
                    <div class="feedback_list">
                        GLO welcomes feedback. We should especially appreciate your comments on:</div>
                    <% foreach (var item in Model)
                       { %>
                                    <div class="line_item"> 
                    <div class="line_q2">
                    <%: Html.Raw(item.Question) %>
                    </div>
                </div>
                    <% } %>
                    <div class="feedback_below">
                        Please complete the feedback form below and email us. We shall contact you shortly.</div>
                    <div class="feedback_form">
                    <table>
                        <tr>
                            <td align="left">
                                <input type="text" class="txt_feedback_name" id="Name" value="Name" onfocus="if (this.value=='Name')this.value=''" onblur="if (this.value=='')this.value='Name'"/><br />                    
                                <input type="text" class="txt_feedback_email" id="Email" value="Email" onfocus="if (this.value=='Email')this.value=''" onblur="if (this.value=='')this.value='Email'"/><br />
                                <input type="text" class="txt_feedback_subject" id="Subject" value="Subject" onfocus="if (this.value=='Subject')this.value=''" onblur="if (this.value=='')this.value='Subject'"/><span class="required">*</span><br />
                                <textarea class="area_feedback_content" id="Content"  value="Feedback" onfocus="if (this.value=='Feedback')this.value=''" onblur="if (this.value=='')this.value='Feedback'">Feedback</textarea>
                            </td>
                            <td align="left" valign="bottom">
                                <input type="button" class="bluebtn2" value="Submit"/>
                            </td>
                        </tr>
                    </table>
                </div>
                </div>
                
                <div id="FeedbackSuccess" style="display:none;">Your feedback was successfully sent!</div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Subject").blur(function () {
                if (!isEmpty($(this).val()) || $(this).val() != "Subject") $(this).removeClass("input_error");
            });

            $(".bluebtn2").click(function () {
                var result = true;

                var email = $('#Email').val();
                var name = $('#Name').val();
                var subject = $('#Subject').val();
                var content = $('#Content').val();
                if (isEmpty(subject) || subject == "Subject") {
                    $("#Subject").addClass("input_error");
                    result = false;
                }
                else {
                    $("#Subject").removeClass("input_error");
                }

                if (result) {
                    $.post("/Home/FeedBackSendEmail", "Email=" + email + "&Name=" + name + "&Subject=" + subject + "&Content=" + content, function (data) {
                        if (data.Result) {
                            $("#FeedbackForm").hide();
                            $("#FeedbackSuccess").show();
                        }
                        else {
                            alertbox(data.Message);
                        }
                    }, 'json');
                }
                else {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
