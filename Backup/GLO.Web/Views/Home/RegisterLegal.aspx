﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterLegal
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Welcome to the GLO community of Good Leaders Online and
                <br />
                thank you for your interest to join as a HR Expert
            </div>
            <div class="boxer_content">
            <%=ViewBag.Content%>
             
            </div>
                
            <div style="padding:20px 0 30px 30px;">
                <div style="float:left;margin-right:5px;">
                    <input type="hidden" id="Policy" value="" />
                    <input type="checkbox" id="chkPolicy" />
                </div>
                I have <a href="javascript:void(0);" id="RegisterRead" class="Register">read</a> and agreed with the GLO's charter of Association. <br />
            </div>
            <div class="boxer_agree">
                <div class="hpylink_bluebtn">
                <a href="javascript:void(0);" id="RegisterNext" class="Register">I want to share my expertise</a>
                </div>
            </div>
        </div>
    </div>
    <div id="lightboxwrapper">
        
    </div>
    <input type="hidden" id="UserType" value="<%=ViewBag.Type %>" />
    
<script type="text/javascript">
    $(document).ready(function () {
        $("#RegisterRead").click(function () {
            $.getJSON('<%=Url.Action("_PolicyDetailExpert","Home") %>', null, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $('#chkPolicy').click(function () {
            if ($(this).attr("checked")) {
                $(this).removeAttr("checked");
                $('#Policy').val('0');

                $.getJSON('<%=Url.Action("_PolicyDetailExpert","Home") %>', null, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
                });
            }
            else {
                $('#Policy').val('0');
            }
        });
        $('#RegisterNext').click(function () {
            if (!$('#chkPolicy').attr("checked")) {
                $('#chkPolicy').parent().addClass("select_error");
            }
            else {
                window.location = "/Account/RegisterExpert?userType=<%=ViewBag.Type %>";
            }

            return false;
        });

        if ($('#Policy').val() == "1") {
            $('#chkPolicy').attr("checked", "checked");
        }
        else {
            $('#chkPolicy').removeAttr("checked");
        }

    });
</script>
</asp:Content>
