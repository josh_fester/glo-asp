﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    AccessForbidden
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_body">
 <div class="glo_boxer">
 
    <div style="padding:10px 20px;">
        <h2>Error Message</h2>

        &nbsp;you have no right access the leader.
    </div>
</div>
</div>

</asp:Content>
