﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_FAQ>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQ
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO FAQ
            </div>
            <div class="boxer_content">
                
                <div class="faq_image"><img src="/Styles/images/glo_faq.gif" /></div>
                
                    <% foreach (var item in Model)
                       { %>
                    <div>
                        <div class="line_q"><a href="javascript:void(0);"><%: Html.Raw(item.Question) %></a></div>
                        <div class="line_a"><%= HttpUtility.HtmlDecode(item.Answer)%></div>
                    </div>
                    <% } %>
                    
                <div>
                    <div class="line_q"><a href="javascript:void(0);">Do you have a question you would like to include?</a></div>
                    <div class="line_a">
                        <div>
                        Post it here and we will respond to you and consider for inclusion in the FAQ list when other GLO users raise similar questions:<br /><br />
                        <textarea id="FaqQuestion" class="textarea_faq"></textarea>
                        </div>
                        <div class="hpylink_bluebtn" style="margin-right:10px;">
                            <a href="javascript:void(0);" id="SubmitQuestion">Appreciate your response</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
        <script type="text/javascript">
            $(document).ready(function () {
                $(".line_a").first().css("display", "block");

                $(".line_q").click(function () {
                    if ($(this).parent().find(".line_a").css("display") == "block") {
                        $(this).parent().find(".line_a").hide();
                    }
                    else {
                        $(".line_a").hide();
                        $(this).parent().find(".line_a").show();
                    }
                });

                $("#SubmitQuestion").click(function () {
                    var question = $("#FaqQuestion").val();
                    var faqData = {
                        'OrderId': 0,
                        'Question': question,
                        'Answer': ""
                    };
                    if (question == "") {
                        alertbox("Please enter your question.");
                        return false;
                    }
                    else {
                        $.postJSON('<%=Url.Action("FAQSubmit","Home") %>', { jsondata: $.toJSON(faqData) }, function (data) {
                            if (data) {
                                alertbox("Thank you for submitting your question.<br>The GLO team will respond to you shortly.", function () {
                                    $("#FaqQuestion").val("");
                                    return false;
                                });
                            }
                        });
                    }
                });
            });
        </script>
</asp:Content>
