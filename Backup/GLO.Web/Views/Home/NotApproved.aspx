﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Not Approved
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="glo_body">
 <div class="glo_boxer">
    <div class="boxer_headline">
        Sorry but you do not have access!
    </div>
    <div class="boxer_content">
    <div class="font18"><%=ViewBag.Message %></div>
       <%-- Please complete your profile and be patient to wait for verification from administrator.--%>
                    <div class="displayright">
                      <%if (ViewBag.Type == "notpayment")
                        { %>
                       <input type="button" id="payment" class="permissionbtn" value="Pay now" /><br />
                      <%} %>
                        <input type="button" id="profile" class="permissionbtn" value="Back to my profile" /><br />
                        <input type="button" id="homepage" class="permissionbtn" value="Back to home page" />
                    </div>

    </div>
  </div>
</div>
  <% if (Request.IsAuthenticated)
     {
         var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
         var proifleurl = String.Empty;
         var paymenturl = String.Empty;
         if (user.UserData.UserType == 1)
         {
             proifleurl = Url.Action("Index", "Leader", new { leaderid = user.UserData.UserID });
         }
         else if (user.UserData.UserType == 2)
         {
             proifleurl = Url.Action("MyValue", "Expert", new { expertid = user.UserData.UserID });
             paymenturl = Url.Action("PayMentExpert", "PayMent");
         }
         else if (user.UserData.UserType == 3)
         {
             proifleurl = Url.Action("CompanyIndex", "Company", new { companyid = user.UserData.UserID });
             paymenturl = Url.Action("PayMentDetail", "PayMent", new { companyid = user.UserData.UserID});
         }
         else
         {
             proifleurl = Url.Action("Index", "Admin");
         }
         
  %>
<script type="text/javascript">
    $(document).ready(function () {

        $("#payment").click(function () {
            if (window.top != window.self) {
                window.parent.location = '<%=paymenturl%>';
            }
            else {
                window.location = '<%=paymenturl%>';
            }
        });
        $("#profile").click(function () {
            if (window.top != window.self) {
                window.parent.location = '<%=proifleurl%>';
            }
            else {
                window.location = '<%=proifleurl%>';
            }
        });
        $("#homepage").click(function () {
            if (window.top != window.self) {
                window.parent.location = '/home';
            }
            else {
                window.location = '/home';
            }
        });
    });
</script>
<%} %>
</asp:Content>