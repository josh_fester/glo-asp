﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Users Search
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="search_wrapper">
        <div class="search_header2"></div>
        <div class="search_boxer2">
            <%--Company--%>
            <div class="headsearch">
                <h2>Search Good Employers</h2>
                <div id="CompanyList">
                <%Html.RenderPartial("~/Views/Shared/Search/CompanySearch.ascx",(List<GLO.Data.Models.GLO_Users>)ViewBag.CompanyList);%> 
                </div>
            </div>
            <%--Leader--%>
            <div class="headsearch">
                <h2>Search Good Leaders</h2>
                <div id="LeaderList">
                <%Html.RenderPartial("~/Views/Shared/Search/LeaderSearch.ascx",(List<GLO.Data.Models.GLO_Users>)ViewBag.LeaderList);%> 
                </div>
            </div>
            <%--Expert--%>
            <div class="headsearch">
                <h2>Search HR Experts</h2>
                <div id="ExpertList">
                <%Html.RenderPartial("~/Views/Shared/Search/ExpertSearch.ascx",(List<GLO.Data.Models.GLO_Users>)ViewBag.ExpertList);%> 
                </div>
            </div>
        </div>
        <div class="search_footer2"></div>
    </div>
    <div id="lightboxwrapper">
    </div>
    <script type="text/javascript">
        function OpenLightFrame(url) {
            var wrap = document.getElementById('lightboxwrapper');
            wrap.innerHTML = '';

            var div1 = document.createElement('div');
            div1.className = 'lightboxframe';

            var div2 = document.createElement('div');
            div2.className = 'frameWrapper';

            var div3 = document.createElement('div');
            div3.className = 'frameContainer';

            var iframer = document.createElement('iframe');
            iframer.setAttribute("src", url);
            iframer.setAttribute("scrolling", "no");
            iframer.setAttribute("frameborder", "0");
            iframer.setAttribute("allowtransparency", "true");

            var div4 = document.createElement('div');
            div4.className = 'frameClose';

            var img = document.createElement('img');
            img.src = "/Styles/images/icon_close.gif";
            img.onclick = function () {
                $('#lightboxwrapper').css("min-height", "");
                return CloseLightBox();
            }

            div4.appendChild(img);
            div3.appendChild(iframer);
            div2.appendChild(div3);
            div2.appendChild(div4);
            div1.appendChild(div2);
            wrap.appendChild(div1);

            wrap.style.display = 'block';

            return false;
       
        }

        function TagUser(id) {
            $.getJSON('<%=Url.Action("GetTagCategory","Tag") %>', { id: id,searchpage:"1", random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        }

        function RequestTag(id) {
            $.post('<%=Url.Action("RequestTag","Tag") %>', { requestUserID: id }, function (data) {
                if (data) {
                    alertbox("Your request has been successfully sent!");
                }
            });
        }

        function RequestRecommend(id) {
            $.post('<%=Url.Action("RequestRecommend","Expert") %>', { requestUserID: id }, function (data) {
                if (data) {
                    alertbox("Your request has been successfully sent!");
                }
            });
        }
    </script>
</asp:Content>
