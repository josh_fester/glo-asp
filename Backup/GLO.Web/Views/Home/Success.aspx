﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Success>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   GLO success case
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
     
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO Success
            </div>
            
            <div class="boxer_content">    
                <div class="success_head">Our Founder Members</div>
                <div class="success_content">
                    <img src="/Images/success_logo1.gif" />
                    <img src="/Images/success_logo2.gif" />
                    <img src="/Images/success_logo3.gif" />
                    <img src="/Images/success_logo4.gif" />
                    <img src="/Images/success_logo5.gif" />
                    <img src="/Images/success_logo6.gif" />
                </div>
                <br />
                <div class="success_head">Other Clients</div>
                <div class="success_content">
                    <img src="/Images/success_logo7.gif" />
                    <img src="/Images/success_logo8.gif" />
                </div>
            </div>

            <div class="boxer_content" style="display:none;">          
                    <div class="r_map">                   
                         <% 
                       int count = 0;
                       foreach (var success in Model)
                       { 
                           if (count == 4)
                           { %>
                        
                        <div class="r_user_box">
                                <a href="javascript:void(0)" x:successid=0 class="successdetail" title="Please click here to recommend GLO">Recommend GLO</a>
                        </div>
                        <div class="r_user_box"> 
                            <a href="javascript:void(0)" x:successid=<%=success.SuccessID %> class="successdetail"><img src="/Images/<%=!string.IsNullOrEmpty(success.GLO_Users.Icon) ? success.GLO_Users.Icon : "user_none.gif"%> "></a>  
                            <div class="r_user_name"><a href="javascript:void(0)" x:successid=<%=success.SuccessID %> class="successdetail"><%=success.GLO_Users.NickName %></a></div>                       
                        </div>
                        <%}
                           else if (count < 8)
                           { %>
                        <div class="r_user_box"> 
                            <a href="javascript:void(0)" x:successid=<%=success.SuccessID %> class="successdetail"><img src="/Images/<%=!string.IsNullOrEmpty(success.GLO_Users.Icon) ? success.GLO_Users.Icon : "user_none.gif"%> "></a>  
                            <div class="r_user_name"><a href="javascript:void(0)" x:successid=<%=success.SuccessID %> class="successdetail"><%=success.GLO_Users.NickName %></a></div>                       
                        </div>
                        <%}  count ++;} %>

                        <%if (count < 8)
                          {
                              for (int i = count; i < 8; i++)
                              {
                                  if (count == 4)
                                  {%>
                                <div class="r_user_box">
                                <a href="javascript:void(0)" x:successid=0 class="successdetail" title="Please click here to recommend GLO" ><img alt="Please click here to recommend GLO" src="/Styles/images/logo.gif" /></a>
                               </div>
                               <div class="r_user_none"></div>
                                <%}
                                  else
                                  { %>
                            <div class="r_user_none"></div>
                        <%} count++;
                              }
                          } %>

                        
                       
                         
                        <div class="r_invite1"></div>
                        <div class="r_invite2"></div>
                        <div class="r_invite3"></div>
                        <div class="r_invite4"></div>
                        <div class="r_invite5"></div>
                        <div class="r_invite6"></div>
                        <div class="r_invite7"></div>
                        <div class="r_invite8"></div>
                       
                    </div>
                    <div id="successcontent" class="r_info">
                        <% if (Model.FirstOrDefault() != null)
                           { %>
                            <%Html.RenderPartial("~/Views/Shared/Success/SuccessDetail.ascx", Model.FirstOrDefault()); %>
                        <%} %>
                    </div>

            </div>

        </div>
    </div>
         <script type="text/javascript">
             $(document).ready(function () {

                if("<%=Request.QueryString["recommendid"] %>" != "")
                {
                     if("<%=Request.IsAuthenticated%>"=="False")
                     {
                         alertbox("Dear <%=ViewBag.RecommendUserNickName %>,<br/>Please login and recommend GLO, Thanks !", function(){
                          window.location.href = "/Account/Login?ReturnUrl=/Home/Success?recommendid=<%=Request.QueryString["recommendid"] %>";
                          return false;
                        });
                     }
                     else
                     {
                           $.getJSON('_SuccessDetail', { "successid":0, random: Math.random() }, function (data) {
                              $('#successcontent').empty().html(data.Html);
                          });
                     }
                 }




                 $(".successdetail").click(function () {
                     var successid = $(this).attr("x:successid");
                     if(successid == 0 && "<%=Request.IsAuthenticated%>"=="False")
                     {
                        window.location.href = "/Account/Login?ReturnUrl=/Home/Success";
                        return false;
                     }
                     $.getJSON('_SuccessDetail', { "successid":successid, random: Math.random() }, function (data) {
                       $('#successcontent').empty().html(data.Html);
                   });
                 });
             });
        </script>
</asp:Content>