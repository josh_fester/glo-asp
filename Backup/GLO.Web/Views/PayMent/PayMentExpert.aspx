﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PayMentExpert
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Payment details
            </div>
            <div class="boxer_content">
                
                <div style="width:440px;margin:auto;">
                <table>
                    <tr>
                        <td width="150">Membership type:</td>
                        <td><div class="graypaayment"><%=ViewBag.Nmae %></div></td>
                    </tr>
                    <tr>
                        <td>Membership fee:</td>
                        <td><div class="graypaayment"><%=ViewBag.Fee %> USD</div></td>
                    </tr>
                    <tr>
                        <td>Expiry date: </td>
                        <td><div class="graypaayment"><%=ViewBag.ExpireDate%></div></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Select payment options</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="radio" name="rdoPayment" x:type="paypal" checked="checked" /> Paypal<br />

                            <%--<input type="radio" name="rdoPayment" x:type="alipay" /> Alipay<br />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                                <input type="button" class="bluebtn2" value="Submit"/>&nbsp;&nbsp;<a href="/Expert/MyValue?expertid=<%=Model.UserID %>">I will pay later</a>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".bluebtn2").click(function () {
                var url = "";
                if ($('input[name="rdoPayment"]:checked').attr("x:type") == "paypal") {
                    url = "/PayMent/PayMentExpertPayPal?expertid=<%=Model.UserID %>";
                }
                else {
                    url = "/PayMent/AliPay";
                }
                window.location.href = url;
                return false;
            });
    });
</script>
</asp:Content>


