﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                <%: Html.DisplayFor(model => model.Title) %>
            </div>
            <div class="boxer_content">
                
    
        
    <div class="news_date">
        <%: Html.DisplayFor(model => model.CreateDate) %>
    </div>

    <div class="news_content">
        <%=HttpUtility.HtmlDecode(Model.Content)%>
    </div>

    
    <div class="news_back"><%: Html.ActionLink("Back to List", "Index") %></div>

            </div>
        </div>
    </div>

</asp:Content>
