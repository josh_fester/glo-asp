﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    In the news
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_content">
                <div class="news_fields">
                    <h2>GLO <span class="blue">NEWS</span></h2>
                    <div class="news_image"><img src="/Styles/images/insight_news.gif" /></div>
                    <div class="news_wrap">
                    <% 
                    var imageEvent = false;
                    foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList)
                    {
                        if (item.FileType != 3)
                        {
                            imageEvent = true;
                        }
                    } %>

                    <%if (imageEvent)
                      {%>
                      <div class="events_box">
                        <% foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList)
                           {
                               if (item.FileType != 3)
                               { %>
                               <div class="events_item">
                                    <div class="events_image">
                                        <a href="/News/PlayVideo/<%=item.NewsID %>">
                                            <img src="<%=item.FileType==2?"/Styles/images/movie.gif":item.FileUrl %>" />
                                        </a>
                                    </div>
                                    <div class="events_name">
                                        <%: Html.ActionLink(item.Title, "PlayVideo", "News", new { id = item.NewsID }, new { })%>
                                    </div>
                                </div>
                             <% }
                        } %>
                       </div>
                     <%} %>     
                     
                           <% foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList)
                           {
                               if (item.FileType == 3)
                               {
                                   item.FileUrl = !string.IsNullOrEmpty(item.FileUrl)? item.FileUrl.StartsWith("http") ? item.FileUrl : "http://" + item.FileUrl : "";
                                    %>
                                <div class="news_item">
                                        <a href="<%=item.FileUrl.ToUrl() %>" target="_blank"><%=item.Title %></a>
                                </div>
                            <% }
                           } %>
                    </div>
                </div>
                <div class="news_fields">
                    <h2>GLO <span class="blue">EVENTS</span></h2>
                    <div class="news_image"><img src="/Styles/images/insight_events.gif" /></div>
                    <div class="news_wrap">
	                <table align="center">
                    <% foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.CalenderFile)
                       { %>
	                        <tr>
                                <td align="left"><img src="/Styles/images/icon_dot.gif" /></td>
                                <td align="left"><a class="file" href="<%=item.FileUrl.ToUrl() %>" target="_blank"><%=item.Title %></a></td>
                                <td align="left"><a class="email" href="mailto:admin@glo-china.com?subject=Register me for the <%=item.Title.Replace("\"","'") %>&body=Thank you for your interest to join! Please kindly provide following info: Name, Company, Mobile number, Email.">RSVP</a></td>
                            </tr>
                    <%} %>
	                </table>
                    </div>
                </div>
                <div class="news_fields">
                    <h2>GLO <span class="blue">SURVEY</span></h2>       
                    <div class="news_image"><img src="/Styles/images/insight_survey.gif" /></div>    
                    <div class="news_wrap">                 
                    <% foreach (var item in (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey)
                       {
                           if (item.SurveyType == 2)
                           { %>
                        <div class="news_item">
                           <a href="<%=item.SurveyUrl.ToUrl() %>"><%=item.SurveyTitle%></a>
                        </div>
                    <% }
                       }%>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
    
    $(document).ready(function () {


    });
    </script>
</asp:Content>
