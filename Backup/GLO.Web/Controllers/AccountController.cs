﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Data.Models;
using GLO.Services;
using System.IO;
using System.Dynamic;
using GLO.Web.Common;
using System.Web.Security;
using System.Runtime.Remoting.Contexts;
using GLO.Web.Models;
using System.Web.Script.Serialization;
using System.Text;
using System.Web.UI;

namespace GLO.Web.Controllers
{
    public partial class AccountController : BaseController
    {
        private IAccountService _accountservice;
        private ILeaderService _leaderservice;
        private IExpertService _expertservice;
        private ICompanyService _companyservice;
        private ICompetenceService _competenceservice;
        private ITagUserService _tagUserService;
        private IMessageService _messageservice;
        private IWebInfoService _webinfoservice;
        private IPolicyService _policyservice;
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();

        #region Service
        public AccountController(IAccountService AccountService, IPolicyService PolicyService, ILeaderService LeaderService, IExpertService ExpertService, ICompanyService CompanyService, ICompetenceService CompetenceService, ITagUserService TagUserService, IMessageService MessageService, IWebInfoService WebInfoService)
        {
            _accountservice = AccountService;
            _policyservice = PolicyService;
            _leaderservice = LeaderService;
            _expertservice = ExpertService;
            _companyservice = CompanyService;
            _competenceservice = CompetenceService;
            _tagUserService = TagUserService;
            _messageservice = MessageService;
            _webinfoservice=WebInfoService;
        }
        #endregion

        #region Login
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            string loginIp = this.GetIP();

            if (ModelState.IsValid)
            {
                var username = model.UserName;
                var password = DEEncrypt.Encrypt(model.Password);
                LoginStatus loginresult = _accountservice.UserLogin(username, password, loginIp);
                switch (loginresult)
                {
                    case LoginStatus.Successed:
                        var user = _accountservice.GetUserByUserName(username);
                        var leaderEmailList = ConfigurationManager.AppSettings["LeaderEmail"].Split(';').ToList();
                        foreach (var leaderEmail in leaderEmailList)
                        {
                            if (user.UserName.ToLower() == leaderEmail.ToLower())
                            {
                                return Redirect(string.Format("/Account/LeaderList?email={0}&returnUrl={1}", user.UserName, returnUrl));
                            }
                        }
                        CookieHelper.SetCookie(user);
                        Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
                        if (Url.IsLocalUrl(returnUrl))
                            return Redirect(returnUrl);
                        else
                        {
                            if (user.TypeID == 2)
                            {
                                if (user.ExpireTime <= DateTime.Now)
                                    return Redirect(string.Format("/PayMent/PayMentExpert?expertid={0}", user.UserID));
                                return Redirect(string.Format("/Expert/MyValue?expertid={0}",user.UserID));
                            }
                            else if (user.TypeID == 1)
                            {
                                return Redirect(string.Format("/Leader/Index?leaderid={0}", user.UserID));
                            }
                            else if (user.TypeID == 3)
                            {
                                if (user.GLO_Company.CompanyType != 1 && user.GLO_Company.CompanyType != 5 && user.GLO_UserEx.IsPayMent!=1)
                                return Redirect(string.Format("/PayMent/PayMentDetail?companyid={0}&&companytype={1}", user.UserID, user.GLO_Company.CompanyType));
                                return Redirect(string.Format("/Company/CompanyIndex?companyid={0}", user.UserID));
                            }
                            else if (user.TypeID == 4)
                            {
                                return Redirect("/Admin/Index");
                            }
                            else
                            {
                                return Redirect("/home/index");
                            }
                        }
                    case LoginStatus.UserNotExisting:
                        ViewBag.Message = "Email is not existing";
                        break;
                    case LoginStatus.WrongPassWord:
                        ViewBag.Message = "Password is wrong";
                        break;
                    case LoginStatus.IsLock:
                        ViewBag.Message = "Your account has been blocked!<br/>Please click <a href='/Home/Contact'>here</a> to contact GLO admin.";
                        break;
                    case LoginStatus.IsNotApprroved:
                        ViewBag.Message = "User is not approved";
                        break;
                    case LoginStatus.LoginErrorPassFive:
                        ViewBag.Message = "Your have exceeded 5 tries. Please try an hour later.";
                        break;
                    default:
                        ViewBag.Message = "Unknow error";
                        break;
                }
            }
            return View();
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PaymentLogin(LoginModel model)
        {
            string resultUrl = "";
            if (!string.IsNullOrEmpty(model.UserName) && !string.IsNullOrEmpty(model.Password))
            {
                string loginIp = this.GetIP();
                LoginUserInfo LoginUserInfo = new LoginUserInfo();
                LoginStatus loginresult = _accountservice.UserLogin(model.UserName, DEEncrypt.Encrypt(model.Password), loginIp);
                int result = 0;
                switch (loginresult)
                {
                    case LoginStatus.Successed:
                        var user = _accountservice.GetUserByUserName(model.UserName);
                        LoginUserInfo.UserName = user.NickName;
                        LoginUserInfo.UserID = user.UserID;
                        LoginUserInfo.UserType = user.TypeID;
                        LoginUserInfo.LogoUrl = user.Icon;
                        LoginUserInfo.UserEmail = user.Email;
                        LoginUserInfo.UnReadEmail = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();

                        CookieHelper.SetCookie(user);
                        Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
                        if (user.TypeID == 2)
                        {
                            resultUrl = "/PayMent/PayMentExpert";
                        }
                        else if (user.TypeID == 1)
                        {
                            resultUrl = string.Format("/Leader/Index?leaderid={0}", user.UserID);
                        }
                        else if (user.TypeID == 3)
                        {
                            if (user.GLO_Company.CompanyType != 1 && user.GLO_Company.CompanyType != 5 && user.ExpireTime <= DateTime.Now)
                            {
                                resultUrl = string.Format("/PayMent/PayMentDetail?companyid={0}&&companytype={1}", user.UserID, user.GLO_Company.CompanyType);
                            }
                            else
                            {
                                resultUrl = string.Format("/Company/CompanyIndex?companyid={0}", user.UserID);
                            }
                        }
                        else
                        {
                            resultUrl = "/home/index";
                        }
                        break;
                    case LoginStatus.UserNotExisting:
                        result = 1;
                        break;
                    case LoginStatus.WrongPassWord:
                        result = 1;
                        break;
                    case LoginStatus.IsLock:
                        result = 2;
                        break;
                    case LoginStatus.LoginErrorPassFive:
                        result = 3;
                        break;
                    case LoginStatus.IsNotApprroved:
                        break;
                    default:
                        result = 9;
                        break;
                }
                if (LoginUserInfo.UserID > 0)
                {
                    return Json(new { Success = true, UserID = LoginUserInfo.UserID, UserType = LoginUserInfo.UserType, ResultUrl = resultUrl }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false, Result = result }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return null;
            }            
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecommenderLogin(string inviteid, string recommendid)
        {
            Session["InviteUserID"] = DEEncrypt.Decrypt(inviteid);
            ViewBag.InviteName = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(inviteid))).NickName;
            if (recommendid != null)
            {
                var user = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(recommendid)));
                ViewBag.RecommendName = user.NickName;
                if (user.TypeID == 1 && !string.IsNullOrEmpty(user.GLO_Leader.FirstName))
                    ViewBag.RecommendName = user.GLO_Leader.FirstName;
                else if (user.TypeID == 2 && !string.IsNullOrEmpty(user.GLO_HRExpert.FirstName))
                    ViewBag.RecommendName = user.GLO_HRExpert.FirstName;

                ViewBag.RecommendSex = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(inviteid))).Sex;
            }
            else
            {
                ViewBag.RecommendName = "";
                ViewBag.RecommendSex = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(inviteid))).Sex;
            }
            return View();
        }
        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RecommenderLogin(LoginModel model,string RecommendName,string InviteName,string RecommendSex)
        {
            ViewBag.InviteName = InviteName;
            ViewBag.RecommendName = RecommendName;
            ViewBag.RecommendSex = RecommendSex;
            if (ModelState.IsValid)
            {
                string loginIp = this.GetIP();
                var username = model.UserName;
                var password = DEEncrypt.Encrypt(model.Password);

                LoginStatus loginresult = _accountservice.UserLogin(username, password,loginIp);
                switch (loginresult)
                {
                    case LoginStatus.Successed:
                        var user = _accountservice.GetUserByUserName(username);
                        CookieHelper.SetCookie(user);
                        Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();

                        var inviteUser = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(Request.QueryString["inviteid"])));
                        if ((inviteUser.UserID == user.UserID))
                        {
                            return Redirect(string.Format("/Home/RegisterProfile?expertid={0}&errortype={1}", user.UserID,"1"));
                        }
                          var recommenduser = _accountservice.GetUserByUserID(int.Parse(DEEncrypt.Decrypt(Request.QueryString["inviteid"]))).GLO_Recommendation.Where(t => t.RecommendUserID == user.UserID).FirstOrDefault();
                          if (recommenduser != null)
                          {
                              return Redirect(string.Format("/Home/RegisterProfile?expertid={0}&errortype={1}", user.UserID,"2"));
                          }
                        return Redirect(string.Format("/Home/RegisterProfile?expertid={0}", user.UserID));
                    case LoginStatus.UserNotExisting:
                        ViewBag.Message = "Email is not existing";
                        break;
                    case LoginStatus.WrongPassWord:
                        ViewBag.Message = "Password is wrong";
                        break;
                    case LoginStatus.IsLock:
                        ViewBag.Message = "Your account has been blocked!<br/>Please click <a href='/Home/Contact'>here</a> to contact GLO admin.";
                        break;
                    case LoginStatus.IsNotApprroved:
                        ViewBag.Message = "User is not approved";
                        break;
                    case LoginStatus.LoginErrorPassFive:
                        ViewBag.Message = "Your have exceeded 5 tries. Please try an hour later.";
                        break;
                    default:
                        ViewBag.Message = "Unknow error";
                        break;
                }
            }
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult JsonLogin(string username, string password)
        {          
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                string loginIp = this.GetIP();
                GLO_Users user = new GLO_Users();
                LoginStatus loginresult = _accountservice.UserLogin(username, DEEncrypt.Encrypt(password),loginIp);
                int result = 0;
                switch (loginresult)
                {
                    case LoginStatus.Successed:
                        user = _accountservice.GetUserByUserName(username);    
                        var leaderEmailList = ConfigurationManager.AppSettings["LeaderEmail"].Split(';').ToList();
                        foreach (var leaderEmail in leaderEmailList)
                        {
                            if (user.UserName.ToLower() == leaderEmail.ToLower())
                            {
                                return Json(new { Success = true, Email = user.Email , ToLeaderList = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        CookieHelper.SetCookie(user);
                        Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
                        break;
                    case LoginStatus.UserNotExisting:
                        result = 1;
                        break;
                    case LoginStatus.WrongPassWord:
                        result = 2;
                        break;
                    case LoginStatus.IsLock:
                        result = 3;
                        break;
                    case LoginStatus.LoginErrorPassFive:
                        result = 4;
                        break;
                    case LoginStatus.IsNotApprroved:
                        result = 5;
                        break;
                    default:
                        result = 9;
                        break;
                }
                if (user!=null && user.UserID > 0)
                {
                    return Json(new { Success = true, UserID = user.UserID, UserType = user.TypeID }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false, Result = result }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return null;
            }

        }
        public ActionResult LoginMessage(int? id)
        {
            LoginModel model = new LoginModel();
            if (id.HasValue)
            {
                var user = _accountservice.GetUserByUserID(id.Value);
                if (user != null)
                {
                    var userEx = user.GLO_UserEx;
                    userEx.Activate = true;
                    _accountservice.UpdateUserEx(userEx);
                    model.UserName = user.UserName;
                }
                else
                {
                    return Redirect("/home/index");
                }
            }
            
            ViewBag.Message = "";
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoginMessage(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var username = model.UserName;
                var password = DEEncrypt.Encrypt(model.Password);
                string loginIp = this.GetIP();
                LoginStatus loginresult = _accountservice.UserLogin(username, password,loginIp);
                switch (loginresult)
                {
                    case LoginStatus.Successed:
                        var user = _accountservice.GetUserByUserName(username);
                        CookieHelper.SetCookie(user);
                        Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
                        if (Url.IsLocalUrl(returnUrl))
                            return Redirect(returnUrl);
                        else
                        {
                            if (user.TypeID == 2)
                            {
                                return Redirect(string.Format("/Expert/MyValue?expertid={0}", user.UserID));
                            }
                            else if (user.TypeID == 1)
                            {
                                return Redirect(string.Format("/Leader/Index?leaderid={0}", user.UserID));
                            }
                            else if (user.TypeID == 3)
                            {
                                if (user.GLO_Company.CompanyType != 1 && user.GLO_Company.CompanyType != 5 && user.GLO_Company.ExpireDate <= DateTime.Now)
                                    return Redirect(string.Format("/PayMent/PayMentDetail?companyid={0}&&companytype={1}", user.UserID, user.GLO_Company.CompanyType));
                                return Redirect(string.Format("/Company/Index?companyid={0}", user.UserID));
                            }
                            else if (user.TypeID == 4)
                            {
                                return Redirect("/Admin/Index");
                            }
                            else
                            {
                                return Redirect("/home/index");
                            }
                        }
                    case LoginStatus.UserNotExisting:
                        ViewBag.Message = "Email is not existing";
                        break;
                    case LoginStatus.WrongPassWord:
                        ViewBag.Message = "Password is wrong";
                        break;
                    case LoginStatus.IsLock:
                        ViewBag.Message = "Your account has been blocked!<br/>Please click <a href='/Home/Contact'>here</a> to contact GLO admin.";
                        break;
                    case LoginStatus.IsNotApprroved:
                        ViewBag.Message = "User is not approved";
                        break;
                    case LoginStatus.LoginErrorPassFive:
                        ViewBag.Message = "Your account has been locked for an hour, please try again an hour later.";
                        break;
                    default:
                        ViewBag.Message = "Unknow error";
                        break;
                }
            }
            return View(model);
        }
        #endregion

        #region Login out
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
        #endregion

        #region Register

        #region leader
        public ActionResult LeaderRegister()
        {
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            RegisterLeaderModel aModel = new RegisterLeaderModel();
            aModel.UserType = 1;
            aModel.Identify = 0;
            aModel.Policy = 0;
            aModel.ReceiveEmail = 1;
            return View(aModel);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]   
        public JsonResult IsExistEmail(string email)
        {
            bool valid = true;
            if (!ConfigurationManager.AppSettings["LeaderEmail"].Contains(email) && _accountservice.CheckEmail(email))
                valid = false;
            return Json(valid, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LeaderRegister(RegisterLeaderModel aModel)
        {
            var isSpecifyLeader = ConfigurationManager.AppSettings["LeaderEmail"].Contains(aModel.Email);
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";

            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            if (String.Compare(aModel.CheckCode, AccountSessionHelper.CheckCode, true) != 0)
            {
                ModelState.AddModelError("CheckCode", "Check code is wrong.");
            }
            if (aModel.LocationID==0)
            {
                ModelState.AddModelError("Location", "Location is required.");
                ViewBag.LocationError = "The location is required.";
            }
            if (!isSpecifyLeader && aModel.IndustryID == 0)
            {
                ModelState.AddModelError("Industry", "Industry is required.");
                ViewBag.IndustryError = "The industry is required.";
            }

            if (isSpecifyLeader)
            {
                var oldUser = _accountservice.GetUserByUserName(aModel.Email);
                aModel.Password = DEEncrypt.Decrypt(oldUser.PassWord);
                aModel.ConfirmPassword = DEEncrypt.Decrypt(oldUser.PassWord);
                ModelState.Remove("Password");
                ModelState.Remove("ConfirmPassword");
            }
            if (ModelState.IsValid)
            {
                GLO_Users user = new GLO_Users();
                user.UserName = aModel.Email;
                user.NickName = string.Format("{0} {1}",aModel.FirstName,aModel.LastName);
                user.Email = aModel.Email;
                user.PassWord = DEEncrypt.Encrypt(aModel.Password);
                user.TypeID = aModel.UserType;
                user.RegisterDate = DateTime.Now;
                user.Approved = isSpecifyLeader;
                user.Icon = "user_none.gif";
                user.Sex = aModel.Sex;
                user.ReceiveEmail = aModel.ReceiveEmail==1?true:false;
                user.PhoneNumber = string.Format("{0}-{1}-{2}", aModel.PhoneNumber1, aModel.PhoneNumber2, aModel.PhoneNumber3);
                _accountservice.UserRegister(user);
                _competenceservice.CompetencyAddForRegister(user.UserID,1);

                _competenceservice.CompetencyAddForRegister(user.UserID, 3);
                user = _accountservice.GetUserByUserID(user.UserID);

                GLO_Leader leader = new GLO_Leader();
                leader.UserID = user.UserID;
                leader.NickName = user.NickName;
                leader.FirstName = aModel.FirstName;
                leader.LogoUrl = user.Icon;
                leader.Location = aModel.Location;
                leader.Title = aModel.Title;
                leader.Industry = aModel.Industry != "Select an industry"?aModel.Industry:"";
                _leaderservice.LeaderRegister(leader);

                _tagUserService.FirstAddTagCategory(user.UserID, user.TypeID);

                GLO_UserLocation location = new GLO_UserLocation();
                location.LocationUserID = Guid.NewGuid();
                location.UserID = user.UserID;
                location.LID = aModel.LocationID;
                _accountservice.UpdateLocation(location);

                if (aModel.IndustryID != 0 && !string.IsNullOrEmpty(aModel.Industry))
                {
                    GLO_UserIndustry newData = new GLO_UserIndustry();
                    newData.UserIndustryID = Guid.NewGuid();
                    newData.UserID = user.UserID;
                    newData.IndustryID = aModel.IndustryID;
                    _accountservice.UpdateIndustry(newData);
                }

                CookieHelper.SetCookie(user);
                if (!isSpecifyLeader)
                {
                    SendRegisterConfirmEmail(user);
                    SendRegisterMessage(user);
                }

                var recommend = false;
                if (Session["InviteUserID"] != null)
                    recommend = true;
                var result =
                  new
                  {
                      result = true,
                      UserID = user.UserID,
                      TypeID = user.TypeID,
                      IsReommend = recommend
                  };
                var url = "";
                if (recommend)
                {
                    url = "/Home/RegisterProfile?expertid=" + user.UserID;
                }
                else
                {
                    url = "/Leader/Index?leaderid=" + user.UserID;
                }
                return Redirect(url);
            }
            else
            {
                return View(aModel);
            }
        }
        #endregion

        #region expert
        public ActionResult RegisterExpert(int userType)
        {
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            RegisterModel aModel = new RegisterModel();
            aModel.UserType = userType;
            return View(aModel);
        }
        [HttpPost]
        public ActionResult RegisterExpert(RegisterModel aModel)
        {
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            string errorMessage = string.Empty;
            if (String.Compare(aModel.CheckCode, AccountSessionHelper.CheckCode, true) != 0)
            {
                ModelState.AddModelError("CheckCode", "Check code is wrong!");
            }
            if (aModel.LocationID == 0)
            {
                ModelState.AddModelError("Location", "The location is required.");
                ViewBag.LocationError = "The location is required.";
            }
            if (aModel.IndustryID == 0)
            {
                ModelState.AddModelError("Industry", "The industry is required.");
                ViewBag.IndustryError = "The industry is required.";
            }
            if (ModelState.IsValid)
            {
                GLO_Users user = new GLO_Users();
                user.UserName = aModel.Email;
                user.NickName = string.Format("{0} {1}", aModel.FirstName, aModel.LastName);
                user.Email = aModel.Email;
                user.PassWord = DEEncrypt.Encrypt(aModel.Password);
                user.TypeID = aModel.UserType;
                user.RegisterDate = DateTime.Now;
                user.Approved = false;
                user.Icon = "user_none.gif";
                user.Sex = aModel.Sex;
                user.PhoneNumber = string.Format("{0}-{1}-{2}", aModel.PhoneNumber1, aModel.PhoneNumber2, aModel.PhoneNumber3);

                GLO_HRExpert expert = new GLO_HRExpert();
                expert.UserID = user.UserID;
                expert.NickName = user.NickName;
                expert.FirstName = aModel.FirstName;
                expert.LogoUrl = user.Icon;
                expert.Location = aModel.Location;

                GLO_UserIndustry industry = new GLO_UserIndustry();
                industry.UserIndustryID = Guid.NewGuid();
                industry.UserID = user.UserID;
                industry.IndustryID = aModel.IndustryID;

                GLO_UserLocation location = new GLO_UserLocation();
                location.LocationUserID = Guid.NewGuid();
                location.UserID = user.UserID;
                location.LID = aModel.LocationID;

                _accountservice.ExpertRegister(user, expert, industry,location);

                user = _accountservice.GetUserByUserID(user.UserID);

                CookieHelper.SetCookie(user);

                var recommend = false;
                if (Session["InviteUserID"] != null)
                    recommend = true;
                var result =
                  new
                  {
                      result = true,
                      UserID = user.UserID,
                      TypeID = user.TypeID,
                      IsReommend = recommend
                  };
                var url = "";
                if (recommend)
                {
                    url = "/Home/RegisterProfile?expertid=" + user.UserID;
                }
                else
                {
                    if (user.TypeID == 1)
                        url = "/Leader/Index?leaderid=" + user.UserID;
                    else if (user.TypeID == 2)
                        url = "/Expert/MyValue?expertid=" + user.UserID;
                    else
                        url = "/Company/CompanyIndex?companyid=" + user.UserID;
                }
                return Redirect(url);
            }
            else
            {
                return View(aModel);
            }
        }
        #endregion

        #region company register process
        public ActionResult RegisterWelocome()
        {
            return View();
        }
        public ActionResult RegisterOption()
        {
            return View();
        }
        public ActionResult RegisterOptionHK()
        {
            return View();
        }
        public ActionResult RegisterOptionOther()
        {
            return View();
        }
        public ActionResult RegisterCompany(int RegType = 1)
        {
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";

            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            ViewBag.RegTypeName = _companyservice.GetCompanyTypeByID(RegType).CompanyLevel;
            ViewBag.RegType = RegType;
            return View();
        }     
        [HttpPost]
        public ActionResult RegisterCompany(RegisterCompanyModel aModel, int RegType = 1)
        {
            ViewBag.LocationError = "";
            ViewBag.IndustryError = "";

            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            string errorMessage = string.Empty;
            if (_accountservice.CheckEmail(aModel.Email))
            {
                errorMessage = "The email  has already been used.";
                ModelState.AddModelError("Email", errorMessage);
            }
            if (aModel.IndustryID == 0)
            {
                errorMessage = "Please select an industry.";
                ModelState.AddModelError("Industry", errorMessage);
                ViewBag.IndustryError = "The industry is required.";
            }
            if (aModel.LocationID == 0)
            {
                errorMessage = "Please select a location.";
                ModelState.AddModelError("loaction", errorMessage);
                ViewBag.LocationError = "The location is required.";
            }
            if (ModelState.IsValid)
            {
                GLO_Users user = new GLO_Users();
                user.UserName = aModel.Email;
                user.NickName = aModel.CompanyName;
                user.Email = aModel.Email;
                user.PassWord = DEEncrypt.Encrypt(aModel.Password);
                user.TypeID = 3;
                user.RegisterDate = DateTime.Now;              
                user.Sex = 1;
                user.Approved = false;
                user.Icon = "user_none.gif";
                user.PhoneNumber = string.Format("{0}-{1}-{2}", aModel.PhoneNumber1, aModel.PhoneNumber2, aModel.PhoneNumber3);
                if (aModel.CompanyType == 1 || aModel.CompanyType == 5)
                {
                    user.ExpireTime = DateTime.Now.AddMonths(3);
                }
                _accountservice.UserRegister(user);
                user = _accountservice.GetUserByUserID(user.UserID);

                _tagUserService.FirstAddTagCategory(user.UserID, user.TypeID);

                GLO_Company company = new GLO_Company();
                company.UserID = user.UserID;
                company.LogoUrl = user.Icon;
                company.CompanyName = aModel.CompanyName;
                company.Revenue = aModel.Revenue != "Select a scope" ? aModel.Revenue : "";
                company.Size = aModel.Size != "Select a scale"?aModel.Size:"";
                company.Website = !string.IsNullOrEmpty(aModel.WebSite) ? ((aModel.WebSite.ToLower().IndexOf("http://") != 0 && aModel.WebSite.ToLower().IndexOf("https://") != 0) ? "http://" + aModel.WebSite : aModel.WebSite) : "";
                company.CompanyType = aModel.CompanyType;
                company.Industry = aModel.Industry != "Select an industry" ? aModel.Industry : "";
                company.ExpireDate = (aModel.CompanyType == 1 || aModel.CompanyType == 5) ? DateTime.Now.AddMonths(3) : DateTime.Now;
                _companyservice.CompanyAdd(company);
                if (company.CompanyType == 1 || company.CompanyType == 5)
                {
                    GLO_CompanyJob job = new GLO_CompanyJob();
                    job.UserID = company.UserID;
                    job.JobTitle = "JobTitle";
                    job.JobSummary = "";
                    job.IsActive = true;
                    job.ExpireDate = DateTime.Now.AddMonths(3);
                    _companyservice.JobAdd(job);

                    GLO_UserEx userex = user.GLO_UserEx;
                    userex.IsPayMent = 1;
                    _accountservice.UpdateUserEx(userex);
                }

                GLO_UserLocation location = new GLO_UserLocation();
                location.LocationUserID = Guid.NewGuid();
                location.UserID = user.UserID;
                location.LID = aModel.LocationID;
                _accountservice.UpdateLocation(location);

                GLO_UserIndustry newData = new GLO_UserIndustry();
                newData.UserIndustryID = Guid.NewGuid();
                newData.UserID = user.UserID;
                newData.IndustryID =aModel.IndustryID;
                _accountservice.UpdateIndustry(newData);

                CookieHelper.SetCookie(user);

                var recommend = false;
                if (Session["InviteUserID"] != null)
                    recommend = true;
                var url = "";
                if (recommend)
                {
                    url = "/Home/RegisterProfile?expertid=" + user.UserID;
                }
                else
                {
                    url = "/Company/CompanyIndex?companyid=" + user.UserID;
                }
                return Redirect(url);
            }
            ViewBag.RegType = aModel.CompanyType;
            ViewBag.RegTypeName = _companyservice.GetCompanyTypeByID(RegType).CompanyLevel;
            return View(aModel);
        }

        public ActionResult ActiveThankYou()
        {
            return View();
        }

        #endregion

        #endregion    

        #region Change Password
        public ActionResult ChangePassword()
        {
            ChangePasswordModel aModel = new ChangePasswordModel();
            aModel.UserID = LoginedUser.UserID;
            ViewBag.Message = "";
            return View(aModel);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel aModel)
        {
            try
            {
                var user = _accountservice.GetUserByUserID(aModel.UserID);
                if (user.PassWord != DEEncrypt.Encrypt(aModel.OldPassword))
                {
                    ModelState.AddModelError("OldPassword", "The old password is wrong.");
                }
                if (ModelState.IsValid)
                {
                    user.PassWord = DEEncrypt.Encrypt(aModel.NewPassword);
                    _accountservice.ChangePassWord(user);
                    ViewBag.Message = "Your password was successfully changed！";
                }
                else
                {
                    ViewBag.Message = "";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(aModel);
        }
        #endregion

        #region Forgot Password
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ForgotPassword(string userName)
        {
            try
            {
                var user = _accountservice.GetUserByUserName(userName);
                if (user != null)
                {
                    string username = user.NickName;
                    if (user.TypeID == 1)
                        username = user.GLO_Leader.FirstName;
                    else if (user.TypeID == 2)
                        username = user.GLO_HRExpert.FirstName;

                    string emailSubject = "GLO Remind you: please reset your login password";
                                        
                    StringBuilder emailContent = new StringBuilder();
                    
                    string ec = _policyservice.GetByID(14).PolicyContent;
                    ec = ec.Replace("{{{here}}}", "<a href=\"" + domain + "/account/resetpassword?id=" + DEEncrypt.Encrypt(user.UserID.ToString()) + "\">here</a>");

                    emailContent.Append("Dear " + username + ",<br/>");
                    emailContent.Append(ec);

                    //emailContent.AppendFormat("Please <a href='{0}/account/resetpassword?id={1}'>click here</a> to reset your password.<br/>Thanks,<br/>GLO Customer Support<br/>", domain, DEEncrypt.Encrypt(user.UserID.ToString()));

                    ExpertHelper.EMailSend(user.Email, emailSubject, emailContent.ToString(), "Admin");

                    return Json(new { Result = true, Message = "Message was successfully sent!" });
                }
                else
                {
                    return Json(new { Result = false, Message = userName + " is not exsit." });
                }
            }
            catch (Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }
        public ActionResult ResetPassword(string id)
        {
            ViewBag.Message = "";
            ChangePasswordModel aModel= new ChangePasswordModel();
            aModel.UserID = int.Parse(DEEncrypt.Decrypt(id));
            aModel.OldPassword = "123456";
            aModel.NewPassword = null;
            aModel.ConfirmPassword = null;
            return View(aModel);
        }
        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordModel aModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _accountservice.GetUserByUserID(aModel.UserID);
                    user.PassWord = DEEncrypt.Encrypt(aModel.NewPassword);
                    _accountservice.ChangePassWord(user);
                    FormsAuthentication.SignOut();
                    ViewBag.Message = "Your password was successfully changed! ";
                    return View(new ChangePasswordModel());
                }
                else
                {
                    ViewBag.Message = "";
                    return View(aModel);
                }
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View(aModel);
            }
        }
        #endregion

        #region Check user name is exist
        [HttpPost]
        public JsonResult CheckUserName(string userName)
        {
            if (_accountservice.CheckUserName(userName))
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
        #endregion

        #region Upload file
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    string fileExtension = Path.GetExtension(fileName); // extension file name
                    string saveName = Guid.NewGuid().ToString() + fileExtension; //rename file

                    fileData.SaveAs(filePath + saveName);
                    string virtualpathsecured = "/images/" + saveName;

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region set security question
        public JsonResult _SetSecurityQuestion(int userID)
        {
            var userEx = _accountservice.GetUserByUserID(userID).GLO_UserEx;
            SecurityQuestionModel aModel = new SecurityQuestionModel();
            aModel.UserID = userID;
            aModel.QuestionContent1 = "Please select a question";
            aModel.QuestionContent2 = "Please select a question";
            aModel.QuestionContent3 = "Please select a question";
            aModel.QuestionList = _accountservice.GetQuestionList();
            if (userEx != null)
            {
                aModel.Question1 = userEx.Question1.HasValue ? userEx.Question1.Value : 0;
                aModel.Question2 = userEx.Question2.HasValue ? userEx.Question2.Value : 0;
                aModel.Question3 = userEx.Question3.HasValue ? userEx.Question3.Value : 0;
                foreach (var item in aModel.QuestionList)
                {
                    if (aModel.Question1 != 0 && aModel.Question1 == item.QuestionID)
                    {
                        aModel.QuestionContent1 = item.QuestionContent;
                    }
                    if (aModel.Question2 != 0 && aModel.Question2 == item.QuestionID)
                    {
                        aModel.QuestionContent2 = item.QuestionContent;
                    }
                    if (aModel.Question3 != 0 && aModel.Question3 == item.QuestionID)
                    {
                        aModel.QuestionContent3 = item.QuestionContent;
                    }
                }
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/SetSecurityQuestion.ascx", aModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SetSecurityQuestion(SecurityQuestionModel aModel)
        {
            if (ModelState.IsValid)
            {
                var userEx = _accountservice.GetUserByUserID(aModel.UserID).GLO_UserEx;
                userEx.Question1 = aModel.Question1;
                userEx.Answer1 = aModel.Answer1;
                userEx.Question2 = aModel.Question2;
                userEx.Answer2 = aModel.Answer2;
                userEx.Question3 = aModel.Question3;
                userEx.Answer3 = aModel.Answer3;
                _accountservice.UpdateUserEx(userEx);
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
        [HttpPost]
        public PartialViewResult AnswerSecurityQuestion(string userName)
        {
            var userEx = _accountservice.GetUserByUserName(userName).GLO_UserEx;
            SecurityQuestionModel aModel = new SecurityQuestionModel();
            aModel.UserID = userEx.UserID;
            if (userEx.Question1.HasValue && userEx.Question1.Value != 0 && userEx.Question2.HasValue && userEx.Question2.Value != 0 && userEx.Question3.HasValue && userEx.Question3.Value != 0)
            {
                aModel.QuestionContent1 = userEx.GLO_SecurityQuestion.QuestionContent;
                aModel.QuestionContent2 = userEx.GLO_SecurityQuestion1.QuestionContent;
                aModel.QuestionContent3 = userEx.GLO_SecurityQuestion2.QuestionContent;
            }
            else
            {
                aModel.QuestionContent1 = "";
                aModel.QuestionContent2 = "";
                aModel.QuestionContent3 = "";
            }
            return PartialView("/Views/Shared/AnswerSecurityQuestion.ascx", aModel);
        }
        [HttpPost]
        public JsonResult CheckAnswer(SecurityQuestionModel aModel)
        {
            var userEx = _accountservice.GetUserByUserID(aModel.UserID).GLO_UserEx;
            string errQuestion = "";
            if (aModel.Answer1 != userEx.Answer1)
            {
                errQuestion += "1,";
            }
            if (aModel.Answer2 != userEx.Answer2)
            {
                errQuestion += "2,";
            }
            if (aModel.Answer3 != userEx.Answer3)
            {
                errQuestion += "3,";
            }
            if (string.IsNullOrEmpty(errQuestion))
            {
                return Json(new { result = true, errQuestion = "/account/resetpassword?id="+ DEEncrypt.Encrypt(aModel.UserID.ToString())});
            }
            else
            {
                return Json(new { result = false, errQuestion = errQuestion });
            }
        }
        #endregion

        #region send register email and message
        public void SendRegisterConfirmEmail(GLO_Users user)
        {


            string emailSubject = "Thank you for joining the GLO leadership community";
            StringBuilder emailContent = new StringBuilder();

            string username = user.GLO_Leader.FirstName;

            string ec = _policyservice.GetByID(9).PolicyContent;
            ec = ec.Replace("{{{here}}}", "<a href=\"" + domain + "/Account/LoginMessage?id=" + user.UserID + "\">here</a>");

            emailContent.Append("Dear " + username + ",<br/>");
            emailContent.Append(ec);

            //emailContent.Append("We appreciate your interest to join the GLO community of responsible leaders.<br/><br/>");
            //emailContent.AppendFormat("Kindly follow this link <a href=\"{0}/Account/LoginMessage?id={1}\">here</a> and complete minimum required fields of your profile. This will activate your account and provide you immediate access to search and view jobs, companies and experts online.<br/><br/>", domain, user.UserID);
            //emailContent.Append("Once we have reviewed and approved your profile, you will also gain access to your dedicated dashboard which you can use to manage job tags, company leader funnel applications and expert contacts.<br/><br/>");      
            //emailContent.Append("We appreciate your leadership!<br/><br/>");
            //emailContent.Append("Best regards<br/>Peter Buytaert<br/>CEO, GLO");

            ExpertHelper.EMailSend(user.Email, emailSubject, emailContent.ToString(),"");
        }

        public void SendRegisterMessage(GLO_Users user)
        {
            string emailSubject = "Thank you for joining the GLO leadership community";
            StringBuilder emailContent = new StringBuilder();
            string username = user.NickName;
            if (user.TypeID == 1)
                username = user.GLO_Leader.FirstName;
            else if (user.TypeID == 2)
                username = user.GLO_HRExpert.FirstName;

            emailContent.Append("Dear " + username + ",<br/>");
            emailContent.Append(_policyservice.GetByID(8).PolicyContent);

            //emailContent.Append("Thank you for joining the GLO leadership community of <em>good</em> leaders assuring <em>good</em> business!<br/>");
            //emailContent.Append("Joining GLO as a leader is absolutely free for all online services. There are no unexpected sales pitches to sell you a Premium or Gold membership. All our members have equal access rights and can:<br/>");
            //emailContent.Append("1.Build a unique GLO profile focused on your values, vision and core competencies<br/>");
            //emailContent.Append("2.Gain access to <em>good</em> jobs listed by member companies<br/>");
            //emailContent.Append("3.Take a free online assessment to evaluate your <em>wise</em> leadership attributes as a basis for further career development<br/>");
            //emailContent.Append("4.Find <em>good</em> company members with the possibility to tag and contact them<br/>");
            //emailContent.Append("5.Contact our pool of HR experts to assist your career development<br/><br/>");
            //emailContent.Append("Success and thank you for your <em>good</em> leadership!<br/><br/>");
            //emailContent.Append("Peter Buytaert<br/>");
            //emailContent.Append("CEO<br/>");

            GLO_Message message = new GLO_Message();
            message.MessageTitle = emailSubject;
            message.MessageContect = emailContent.ToString();
            message.SendDate = DateTime.Now;
            message.MessageTypeID = 2;
            _messageservice.MessageCreate(message, _accountservice.GetUserByUserName("Admin").UserID, user.UserID.ToString());
        }
        #endregion

        #region Leader list
        public ActionResult LeaderList(string email, string returnUrl = null)
        {
            ViewBag.LeaderList = _accountservice.GetAllLeaderListByEmail(email);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LeaderList(string email, string nikename, string password, string returnUrl)
        {
            string loginIp = this.GetIP();
            var message = string.Empty;
            LoginStatus loginresult = _accountservice.LeaderLogin(nikename, password, loginIp);
            switch (loginresult)
            {
                case LoginStatus.Successed:
                    var user = _accountservice.GetUserByNickName(nikename);
                    CookieHelper.SetCookie(user);
                    Session["UnReadMessageCount"] = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
                    if (Url.IsLocalUrl(returnUrl))
                        return Json(new { Success = true, ResultUrl = returnUrl }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        return Json(new { Success = true, UserID = user.UserID }, JsonRequestBehavior.AllowGet);
                    }
                case LoginStatus.UserNotExisting:
                    message = "Email is not existing";
                    break;
                case LoginStatus.WrongPassWord:
                    message = "Password is wrong";
                    break;
                case LoginStatus.IsLock:
                    message ="Your account has been blocked!<br/>Please click <a href='/Home/Contact'>here</a> to contact GLO admin.";
                    break;
                case LoginStatus.IsNotApprroved:
                    message = "User is not approved";
                    break;
                case LoginStatus.LoginErrorPassFive:
                    message = "Your have exceeded 5 tries. Please try an hour later.";
                    break;
                default:
                    message = "Unknow error";
                    break;
            }
            return Json(new { Success = false, Result = message }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
