﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Data;
using GLO.Services;
using GLO.Data.Models;
using System.Web.Script.Serialization;
using System.Runtime.Remoting.Contexts;
using System.Net;
using System.Net.Mail;
using GLO.Web.Models;
using GLO.Web.Common;
using System.Web.Security;
using System.Configuration;

namespace GLO.Web.Controllers
{
    [Authorize]
    [IsManagerAttribute]
    public class ExpertController : BaseController
    {
        #region Service
        private IExpertService _expertservice;
        private IAccountService _accountservice;
        private IMessageService _messageservice;
        private ICompetenceService _competenceservice;
        private ITagUserService _taguserservice;
        private IPolicyService _policyservice;
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();
        public ExpertController(IExpertService ExpertService, IPolicyService PolicyService, IAccountService AccountService, IMessageService MessageService, ICompetenceService CompetenceService, ITagUserService TagUserService)
        {
            _expertservice = ExpertService;
            _policyservice = PolicyService;
            _accountservice = AccountService;
            _messageservice = MessageService;
            _competenceservice = CompetenceService;
            _taguserservice = TagUserService;
        }
        #endregion

        #region Check Method
        public JsonResult CheckExpertStatus(GLO_UserEx userEx)
        {
            if (!userEx.BasicInfoStatus)
            {
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.ValueStatus)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.CredentialsStatus)
            {
                return Json("3", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.CompetenciesStatus)
            {
                return Json("4", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.RegisterSuccessStatus)
            {
                return Json("5", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("0", JsonRequestBehavior.AllowGet);
        }
        public void CheckTagButton(int userID, int UserType)
        {
            bool showTagButton = true;
            if (LoginedUser != null)
            {
                foreach (var taggeduser in _taguserservice.GetTaggedUserList(LoginedUser.UserID))
                {
                    if (taggeduser.TaggedUserID == userID)
                    {
                        showTagButton = false;
                    }
                }
                if (UserType == LoginedUser.UserType || LoginedUser.UserType == 4)
                {
                    showTagButton = false;
                }
            }
            ViewBag.ShowTagButton = showTagButton;
        }
        public void CheckRequestTagButton(int userID, int UserType)
        {
            bool showButton = true;
            if (LoginedUser != null)
            {
                foreach (var taggeduser in _taguserservice.GetTaggedUserList(userID))
                {
                    if (taggeduser.TaggedUserID == LoginedUser.UserID)
                    {
                        showButton = false;
                    }
                }
                if (UserType == LoginedUser.UserType || LoginedUser.UserType == 4)
                {
                    showButton = false;
                }
            }
            ViewBag.ShowRequestTagButton = showButton;
        }
        #endregion

        #region My Value
        [AllowAnonymous]
        public ActionResult MyValue(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            CheckTagButton(expert.UserID, expert.TypeID);
            CheckRequestTagButton(expert.UserID, expert.TypeID);
            return View(expert);
        }
        public ActionResult UpdateMyValue(int expertid, string myvalue, string imageleft, string imageright,string pptFile)
        {
            GLO_HRExpert expert = _accountservice.GetUserByUserID(expertid).GLO_HRExpert;
            expert.MyValue = HttpUtility.UrlDecode(myvalue);// HttpUtility.HtmlEncode(myvalue);
            expert.ValueImageLeft = imageleft;
            expert.VlaueInageRight = imageright;
            expert.PPTFileName = pptFile;
            _expertservice.HRExpertUpdate(expert);
            if (string.IsNullOrEmpty(pptFile))
            {
                DeleteFile("~/Upload/" + expertid + "/" + expert.PPTFileName+ "");
            }
            GLO_UserEx userEx = expert.GLO_Users.GLO_UserEx;
            if (userEx.ShowExplain || !userEx.ValueStatus)
            {
                userEx.ShowExplain = false;
                userEx.ValueStatus = true;
                _accountservice.UpdateUserEx(userEx);
            }
            return CheckExpertStatus(userEx);
        }
        #endregion

        #region My Credentials
        public ActionResult MyCredential(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            CheckTagButton(expert.UserID, expert.TypeID);
            CheckRequestTagButton(expert.UserID, expert.TypeID);
            return View(expert);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateCredential(int expertid, string credential, string qualification)
        {
            GLO_HRExpert expert = _accountservice.GetUserByUserID(expertid).GLO_HRExpert;
            expert.Credential = HttpUtility.UrlDecode(credential);
            expert.Qualification = HttpUtility.UrlDecode(qualification);
            _expertservice.HRExpertUpdate(expert);

            GLO_UserEx userEx = expert.GLO_Users.GLO_UserEx;
            if (userEx.ShowExplain || !userEx.CredentialsStatus)
            {
                userEx.ShowExplain = false;
                userEx.CredentialsStatus = true;
                _accountservice.UpdateUserEx(userEx);
            }
            return CheckExpertStatus(userEx);
        }

        #endregion

        #region My Compentence
        public ActionResult MyCompetence(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            Competence comp = new Competence();
            comp.CarreerSummaryList = expert.GLO_HRExpert.GLO_CareerSummary.OrderBy(x=>x.StartDate).ToList();
            comp.ContributionList = expert.GLO_HRExpert.GLO_Contribution.OrderBy(x=>x.StartDate).ToList();
            comp.EducationList = expert.GLO_HRExpert.GLO_Education.OrderBy(x=>x.StartDate).ToList();

            foreach (var career in comp.CarreerSummaryList)
            {
                if (career.EndDate == null)
                    career.EndDate = DateTime.Now;
            }
            foreach (var edu in comp.EducationList)
            {
                if (edu.EndDate == null)
                    edu.EndDate = DateTime.Now;
            }
            foreach (var con in comp.ContributionList)
            {
                if (con.EndDate == null)
                    con.EndDate = DateTime.Now;
            }

            comp.StartYear = ExpertHelper.GetStartMinYear(comp.CarreerSummaryList, comp.EducationList, comp.ContributionList);
            comp.EndYear = ExpertHelper.GetEndMaxYear(comp.CarreerSummaryList, comp.EducationList, comp.ContributionList);
            expert.GLO_HRExpert.ExpertCompetence = comp;
            ViewBag.UserID = expertid;
            ViewBag.CompetenciesList = expert.GLO_Competencies.Where(x => x.CompetencyType == 1).OrderBy(x => x.CreateTime).ToList();
            CheckTagButton(expert.UserID, expert.TypeID);
            CheckRequestTagButton(expert.UserID, expert.TypeID);
            return View(expert);
        }
        public JsonResult CompetenceDelete(int recordid, string type)
        {
            if (type == "Employment")
                _expertservice.CarerrSummaryDelete(recordid);
            else if (type == "Contribution")
                _expertservice.ContributionDelete(recordid);
            else if (type == "Education")
                _expertservice.EducationDelete(recordid);
            return Json(true);
        }
        public JsonResult _MyCompetenceUpdate(int recordid, string type)
        {

            var viewdata = new ViewDataDictionary();

            if (type == "Contribution")
            {
                var contribution = _competenceservice.GetContributionByID(recordid);
                viewdata.Add("Type", "Contribution");
                viewdata.Add("Contribution", contribution);
            }
            else if (type == "Education")
            {
                var education = _competenceservice.GetEducationByID(recordid);
                viewdata.Add("Type", "Education");
                viewdata.Add("Education", education);
            }
            else if (type == "Employment")
            {
                var career = _competenceservice.GetCareerByID(recordid);
                viewdata.Add("Type", "Employment");
                viewdata.Add("Employment", career);
            }


            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/MyCompetenceEdit.ascx", viewdata)
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CareerSummaryEdit(string jsondata, string type)
        {
            if (type == "Education")
            {
                GLO_Education education = DeserializeXmlT<GLO_Education>(jsondata);
                _competenceservice.EducationEidt(education);
            }
            else if (type == "Employment")
            {
                GLO_CareerSummary carerrsummary = DeserializeXmlT<GLO_CareerSummary>(jsondata);
                _competenceservice.CareerSummaryEdit(carerrsummary);
            }
            else
            {
                GLO_Contribution contribution = DeserializeXmlT<GLO_Contribution>(jsondata);
                _competenceservice.ContributionEdit(contribution);
            }
            return Json(true);
        }
        public ActionResult CareerSummaryAdd(string data, string type)
        {
            if (type == "Education")
            {
                GLO_Education education = DeserializeXmlT<GLO_Education>(data);
                _expertservice.EducationAdd(education);
            }
            else if (type == "Employment")
            {
                GLO_CareerSummary carerrsummary = DeserializeXmlT<GLO_CareerSummary>(data);
                _expertservice.CarerrSummaryAdd(carerrsummary);
                GLO_HRExpert expert = _accountservice.GetUserByUserID(carerrsummary.UserID).GLO_HRExpert;
                _expertservice.HRExpertUpdate(expert);

                GLO_UserEx userEx = expert.GLO_Users.GLO_UserEx;
                if (userEx.ShowExplain || !userEx.CompetenciesStatus)
                {
                    userEx.ShowExplain = false;
                    userEx.CompetenciesStatus = true;
                    _accountservice.UpdateUserEx(userEx);
                }

                return CheckExpertStatus(userEx);
            }
            else
            {
                GLO_Contribution contribution = DeserializeXmlT<GLO_Contribution>(data);
                _expertservice.ContributionAdd(contribution);
            }
            return Json(true);
        }
        #endregion

        #region My Recommendation
        [CheckPayMent(IsCheck = true)]
        public ActionResult MyRecommendation(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            foreach (var recommend in expert.GLO_Recommendation)
            {
                recommend.LogoUrl = _accountservice.GetUserByUserID(recommend.RecommendUserID).Icon;
            }

            ViewBag.CompetenciesList = expert.GLO_Competencies.OrderBy(x => x.CreateTime).ToList();
            ViewBag.InviteMessage = _policyservice.GetByID(15).PolicyContent;
            CheckTagButton(expert.UserID, expert.TypeID);
            CheckRequestTagButton(expert.UserID, expert.TypeID);
            return View(expert);
        }     
      
        #endregion

        #region My DashBoard
        [CheckApprovedAttribute]
        public ActionResult DashBoard(int expertid)
        {
            ViewBag.Select = "Assignment";
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            if (!expert.GLO_UserEx.BasicInfoStatus)
            {
                return RedirectToAction("MyValue", new { expertid = expertid });
            }
            else if (!expert.GLO_UserEx.ValueStatus)
            {
                return RedirectToAction("MyValue", new { expertid = expertid });
            }
            else if (!expert.GLO_UserEx.CredentialsStatus)
            {
                return RedirectToAction("MyCredential", new { expertid = expertid });
            }
            else if (!expert.GLO_UserEx.CompetenciesStatus)
            {
                return RedirectToAction("MyCompetence", new { expertid = expertid });
            }
            else if (!expert.GLO_UserEx.RegisterSuccessStatus)
            {
                return RedirectToAction("RegisterSuccess", new { expertid = expertid });
            }
            else
            {
                return View(expert);
            }
        }
        public JsonResult DeleteAssignment(int projectid)
        {
            _expertservice.ExpertProjectDelete(projectid);
            return Json(true);
        }
        public JsonResult AssignmentAdd(string assignmentdata)
        {
            GLO_ExpertProject assignment = DeserializeXmlAssignment(assignmentdata);
            if (assignment.ProjectID > 0)
                _expertservice.ExpertProjectEdit(assignment);
            else
                _expertservice.ExpertProjectAdd(assignment);
            return Json(true);
        }
        private static GLO_ExpertProject DeserializeXmlAssignment(string record)
        {
            if (String.IsNullOrEmpty(record)) return null;
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<GLO_ExpertProject>(record);
        }

        public ActionResult ReceiveEmail(int expertid)
        {
            var user = _accountservice.GetUserByUserID(expertid);
            ViewBag.Select = "ReceiveEmail";
            return View(user);
        }

        public JsonResult ReceiveEmailChange(int expertid, bool ReceiveEmail)
        {
            var user = _accountservice.GetUserByUserID(expertid);
            user.ReceiveEmail = ReceiveEmail;
            _accountservice.UserUpdate(user);
            return Json(true);
        }

        #endregion

        #region ChangePassword
        public ActionResult ChangePassword()
        {
            ViewBag.Select = "Password";
            ChangePasswordModel aModel = new ChangePasswordModel();
            aModel.UserID = LoginedUser.UserID;
            ViewBag.Message = "";
            return View(aModel);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel aModel)
        {
            try
            {
                var user = _accountservice.GetUserByUserID(aModel.UserID);
                if (user.PassWord != DEEncrypt.Encrypt(aModel.OldPassword))
                {
                    ModelState.AddModelError("OldPassword", "The old password is wrong.");
                }
                if (ModelState.IsValid)
                {
                    user.PassWord = DEEncrypt.Encrypt(aModel.NewPassword);
                    _accountservice.ChangePassWord(user);
                    FormsAuthentication.SignOut();
                    ViewBag.Message = "Your password was successfully changed！";
                }
                else
                {
                    ViewBag.Message = "";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(aModel);
        }
        #endregion

        #region My Recommend List
        public ActionResult MyRecommendList()
        {
            ViewBag.Select = "MyRecommendList";
            var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
            List<GLO_Users> recommendThemList = new List<GLO_Users>();
            foreach (var item in user.GLO_Recommendation1)
            {
                recommendThemList.Add(item.GLO_Users);
            }
            List<GLO_Users> recommendMeList = new List<GLO_Users>();
            foreach (var item in user.GLO_Recommendation)
            {
                recommendMeList.Add(item.GLO_RecommendUsers);
            }
            ViewBag.RecommendThemList = recommendThemList;
            ViewBag.RecommendMeList = recommendMeList;
            return View(user);
        }
        #endregion


        public ActionResult RegisterSuccess(int expertid)
        {
            GLO_Users user = _accountservice.GetUserByUserID(expertid);
            GLO_UserEx userEx = user.GLO_UserEx;
            userEx.RegisterSuccessStatus = true;
            _accountservice.UpdateUserEx(userEx);
            string Title = "Please approve this registered expert";
            string Content = string.Format("An expert is registered to GLO and finished profile pages, please click <a href=\"{0}/admin\" target=\"_blank\">here</a> to approve the expert.", domain);
            string ToMail = ConfigurationManager.AppSettings["EmailName"].ToString();
            ExpertHelper.EMailSend(ToMail, Title, Content, "");
            return View(user);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpertUpdate(string expert, string ispublish,int LocationID, int industryID)
        {
            GLO_HRExpert exp = DeserializeXmlExpert(expert);
            GLO_Users user = _accountservice.GetUserByUserID(exp.UserID);
            GLO_HRExpert experthr = user.GLO_HRExpert;
            experthr.NickName = exp.NickName;
            //experthr.Location = exp.Location;
            experthr.MyVision = exp.MyVision;
            experthr.Title = exp.Title;
            if (exp.LogoUrl != experthr.LogoUrl && experthr.LogoUrl != "user_none.gif")
            {
                DeleteFile("~/Images/" + experthr.LogoUrl + "");
            }
            experthr.LogoUrl = exp.LogoUrl;
            experthr.ExpertValue1 = exp.ExpertValue1;
            experthr.ExpertValue2 = exp.ExpertValue2;
            experthr.ExpertValue3 = exp.ExpertValue3;      
            _expertservice.HRExpertUpdate(experthr);
            
            user.NickName = exp.NickName;
            user.Icon = exp.LogoUrl;
            user.IsPublish = ispublish=="0"?false:true;
            _accountservice.UserUpdate(user);

            GLO_UserEx userEx = user.GLO_UserEx;
            if (userEx.ShowExplain || !userEx.BasicInfoStatus)
            {
                userEx.ShowExplain = false;
                userEx.BasicInfoStatus = true;
                _accountservice.UpdateUserEx(userEx);
            }
            if (industryID != 0)
            {
                GLO_UserIndustry newData = new GLO_UserIndustry();
                newData.UserID = LoginedUser.UserID;
                newData.IndustryID = industryID;
                _accountservice.UpdateIndustry(newData);
            }
            if (LocationID != 0)
            {
                GLO_UserLocation newData = new GLO_UserLocation();
                newData.UserID = LoginedUser.UserID;
                newData.LID = LocationID;
                _accountservice.UpdateLocation(newData);
            }

            CookieHelper.SetCookie(user);

            return CheckExpertStatus(userEx);
        }
                  

        public JsonResult InviteRecommend(string emailname, string title, string content)
        {
            if (emailname.ToLower() == LoginedUser.UserEmail.ToString().ToLower())
            {
                return Json("Invitation failed!<br/> Your can't invite youself.");
            }
            content = content.Replace("\n", "<br/>");
            string invitelink = "";
            var recommendUser = _accountservice.GetUserByUserName(emailname);
            if (recommendUser != null)
            {
                string invitelinkForMessage = string.Format("Please click <a href='/Home/RegisterProfile?inviteid={0}&expertid={1}'> here</a> to start building a recommendation.<br/><br/> Thanks!", LoginedUser.UserID, recommendUser.UserID); 
                string inviteMessage = content + "<br/>" + invitelinkForMessage + "<br/>" + LoginedUser.UserName;
                var user = _accountservice.GetUserByUserID(LoginedUser.UserID).GLO_Recommendation.Where(t => t.RecommendUserID == recommendUser.UserID).FirstOrDefault();
                if (user != null)
                {
                    return Json("Invitation failed!<br/>" + emailname + " already recommended you.");
                }

                GLO_Message message = new GLO_Message();
                message.MessageTitle = title;
                message.MessageContect = inviteMessage.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                message.SendDate = DateTime.Now;
                message.MessageTypeID = 2;
                _messageservice.MessageCreate(message, LoginedUser.UserID, recommendUser.UserID.ToString());

                invitelink = string.Format("Please click <a href='{2}/Account/RecommenderLogin?inviteid={0}&recommendid={1}'> here</a> to start building a recommendation.<br/> Thanks!", DEEncrypt.Encrypt(LoginedUser.UserID.ToString()), DEEncrypt.Encrypt(recommendUser.UserID.ToString()), domain);
                content = content + "<br/>" + invitelink + "<br/>" + LoginedUser.UserName;
            }
            else
            {
                invitelink = string.Format("Please click <a href='{2}/Home/RegisterGuide?inviteid={0}&type={1}'> here</a><br/> Thanks!", LoginedUser.UserID, LoginedUser.UserType,domain);
                content = content + "<br/>" + invitelink + "<br/>" + LoginedUser.UserName;
            }
            var loggeduser = _accountservice.GetUserByUserID(LoginedUser.UserID);
            ExpertHelper.EMailSend(emailname, title, content, loggeduser.Email, loggeduser.NickName);
            return Json("True");
        }

        public JsonResult RequestRecommend(int requestUserID)
        {
            var recommendUser = _accountservice.GetUserByUserID(requestUserID);
            string username = recommendUser.NickName;
            if (recommendUser.TypeID == 1)
                username = recommendUser.GLO_Leader.FirstName;
            else if (recommendUser.TypeID == 2)
                username = recommendUser.GLO_HRExpert.FirstName;

            if (recommendUser != null)
            {                
                string title = "Appreciate your recommendation";

                string ec = "";
                if (LoginedUser.UserType ==1)
                {
                    ec = _policyservice.GetByID(16).PolicyContent;
                }
                else
                {
                    ec = _policyservice.GetByID(17).PolicyContent;
                }

                ec = ec.Replace("{{{username}}}", LoginedUser.UserName);

                string inviteMessage = "Dear " + username + ",<br/>" + ec.Replace("{{{here}}}", "<a href=\"/Home/RegisterProfile?inviteid=" + LoginedUser.UserID + "&expertid=" + recommendUser.UserID + "\">here</a>");
                string content = "Dear " + username + ",<br/>" + ec.Replace("{{{here}}}", "<a href=\"" + domain + "/Account/RecommenderLogin?inviteid=" + DEEncrypt.Encrypt(LoginedUser.UserID.ToString()) + "&recommendid=" + DEEncrypt.Encrypt(recommendUser.UserID.ToString()) + "\">here</a>");
                
                GLO_Message message = new GLO_Message();
                message.MessageTitle = title;
                message.MessageContect = inviteMessage.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                message.SendDate = DateTime.Now;
                message.MessageTypeID = 1;
                _messageservice.SendSystemMessage(message, recommendUser.UserID.ToString());

                var loggeduser = _accountservice.GetUserByUserID(LoginedUser.UserID);
                ExpertHelper.EMailSend(recommendUser.Email, title, content, loggeduser.Email, loggeduser.NickName);
            }
            return Json("True");
        }
        [HttpPost]
        public JsonResult DeleteRecommend(string recommendid)
        {
            if (!string.IsNullOrEmpty(recommendid))
            {
                var idList = recommendid.Split(';').ToList();
                foreach (string id in idList)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        _expertservice.RecommendationDelete(int.Parse(id));
                    }
                }
            }
            return Json(true);
        }
        public JsonResult _EidtLeftInfo(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            ViewBag.GLO_ExpertRelational = expert.GLO_ExpertRelational.ToList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/ProfileLeftEdit.ascx", expert.GLO_HRExpert)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _EidtMyValue(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/MyValueEdit.ascx", expert.GLO_HRExpert)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _MyCredential(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/MyCredentialEdit.ascx", expert.GLO_HRExpert)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _EidtRecommendation(int expertid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            foreach (var recommend in expert.GLO_Recommendation)
            {
                recommend.LogoUrl = _accountservice.GetUserByUserID(recommend.RecommendUserID).Icon;
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/RecommendationEdit.ascx", expert.GLO_HRExpert)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _GetAssignmentDetail(int projectid)
        {
            GLO_ExpertProject project = _expertservice.GetProjectByID(projectid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/AssignmentDetail.ascx", project)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _RecommendDetail(int recommendid)
        {
            ViewBag.CompetenciesList = null;
            var recommend = _expertservice.GetRecommendationByID(recommendid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/RecommendationDetail.ascx", recommend)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _AssignmentEdit(int projectid, int expertid)
        {
            GLO_ExpertProject project = _expertservice.GetProjectByID(projectid);
            if (project == null)
            {
                project = new GLO_ExpertProject();
                project.UserID = expertid;
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Expert/AssignmentEdit.ascx", project)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        private static GLO_CareerSummary DeserializeXml(string record)
        {
            if (String.IsNullOrEmpty(record)) return null;
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<GLO_CareerSummary>(record);
        }
        private static GLO_HRExpert DeserializeXmlExpert(string record)
        {
            if (String.IsNullOrEmpty(record)) return null;
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<GLO_HRExpert>(record);
        }        
        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }
        
        public JsonResult AddExpertValue(string value)
        {
            string resultId = "";
            int valueID = 0;
            GLO_ExpertValueDic expertValueDic = _expertservice.GetExpertValueDicByValue(value);
            if (expertValueDic != null)
            {
                valueID = expertValueDic.ExpertValueID;
            }
            else
            {
                GLO_ExpertValueDic newExpertValueDic = new GLO_ExpertValueDic();
                newExpertValueDic.ExpertValue = value;
                newExpertValueDic.CreateTime = DateTime.Now;
                valueID = _expertservice.ExpertValueAdd(newExpertValueDic);
            }            
            return Json(_expertservice.ExpertRelationalAdd(LoginedUser.UserID, valueID));
        }
        [HttpPost]
        public JsonResult GetExpertValueList()
        {
            List<GLO_ExpertValueDic> aList = _expertservice.GetExpertValueList();
            var resultList = new SelectList(aList, "ExpertValueID", "ExpertValue", null);
            return Json(resultList);
        }
        public JsonResult DeleteValue(Guid id)
        {
            try
            {
                _expertservice.ExpertRelationalDelete(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public void DeleteFile(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                try
                {
                    //file path
                    string _filePath = Server.MapPath(filePath);
                    if (System.IO.File.Exists(_filePath))
                    {
                        System.IO.File.Delete(_filePath);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
