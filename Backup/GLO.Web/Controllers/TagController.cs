﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data.Models;
using System.Web.Script.Serialization;
using GLO.Web.Models;

namespace GLO.Web.Controllers
{
    [CheckApprovedAttribute]
    public class TagController : BaseController
    {

        private ITagUserService _tagUserService;
        private IAccountService _accountService;
        private IMessageService _messageService;
        public TagController(ITagUserService TagUserService, IAccountService AccountService,IMessageService MessageService)
        {
            _tagUserService = TagUserService;
            _accountService = AccountService;
            _messageService = MessageService;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TagUser(string tagData)
        {
            GLO_TagUsers newData = DeserializeXmlTagUsers(tagData);
            newData.UserID = LoginedUser.UserID;
            _tagUserService.AddTagUsers(newData);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateTagUser(string tagData)
        {
            GLO_TagUsers newData = DeserializeXmlTagUsers(tagData);
            _tagUserService.UpdateTagUsers(newData);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTagCategory(int id,string searchpage)
        {
            TagUserModel aModel = new TagUserModel();
            aModel.TaggedUserID = id;
            aModel.FunctionList = _tagUserService.GetTagCategoryList(LoginedUser.UserID);
            if (aModel.FunctionList.Count == 0)
            {
                _tagUserService.FirstAddTagCategory(LoginedUser.UserID, LoginedUser.UserType);
                aModel.FunctionList = _tagUserService.GetTagCategoryList(LoginedUser.UserID);
            }
            if (LoginedUser.UserType == 3)
            {
                aModel.ShowFunctions = true;
            }
            else
            {
                aModel.ShowFunctions = false;
                aModel.TagCategoryID = aModel.FunctionList[0].TagCategoryID;
            }
            aModel.UserName = _accountService.GetUserByUserID(id).NickName;
            if (!string.IsNullOrEmpty(searchpage) && searchpage =="1")
                aModel.IsSearchPage = true;
            else
                aModel.IsSearchPage = false;
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Tag/TagUser.ascx", aModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EditTagCategory(int tagID)
        {
            var tagUsers = _tagUserService.GetTagUsersByID(tagID);
            TagUserModel aModel = new TagUserModel();
            aModel.TagID = tagUsers.TagID;
            aModel.UserID = tagUsers.UserID;
            aModel.TaggedUserID = tagUsers.TaggedUserID;
            aModel.TagCategoryID = tagUsers.TagCategoryID;
            aModel.Memo = tagUsers.TagNote;
            if (LoginedUser.UserType == 3)
            {
                aModel.ShowFunctions = true;
                aModel.FunctionList = _tagUserService.GetTagCategoryList(tagUsers.UserID);
                aModel.FunctionName = tagUsers.GLO_TagCategory.GLO_Functions.FunctionName;
            }
            else
            {
                aModel.ShowFunctions = false;
            }
            aModel.UserName = tagUsers.GLO_Users1.NickName;
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Tag/EditTagUser.ascx", aModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _MoveTagUser(int tagID)
        {
            var tagUsers = _tagUserService.GetTagUsersByID(tagID);
            TagUserModel aModel = new TagUserModel();
            aModel.TagID = tagUsers.TagID;
            aModel.UserID = tagUsers.UserID;
            aModel.TaggedUserID = tagUsers.TaggedUserID;
            aModel.TagCategoryID = tagUsers.TagCategoryID;
            aModel.Memo = tagUsers.TagNote;
            if (LoginedUser.UserType == 3)
            {
                aModel.ShowFunctions = true;
                aModel.FunctionList = _tagUserService.GetTagCategoryList(tagUsers.UserID);
                aModel.FunctionName = tagUsers.GLO_TagCategory.GLO_Functions.FunctionName;
            }
            else
            {
                aModel.ShowFunctions = false;
            }
            aModel.UserName = tagUsers.GLO_Users1.NickName;
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Tag/MoveTagUser.ascx", aModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _TagFunctionEdit(string functionid)
        {
            GLO_Functions function = new GLO_Functions();               
            if (functionid !="-1")
                function = _tagUserService.GetFunctionByID(Guid.Parse(functionid));
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Tag/TagFunctionEdit.ascx", function)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FunctionNameEdit(string functionid, string functionname)
        {
            if (string.IsNullOrEmpty(functionname))
                return Json(false);
            GLO_Functions function = _tagUserService.GetFunctionByID(Guid.Parse(functionid));
            if (function== null)
            {
                GLO_Functions newFunction = new GLO_Functions();
                newFunction.FunctionID = Guid.NewGuid();
                newFunction.FunctionName = functionname;
                newFunction.FunctionType = LoginedUser.UserType;
                newFunction.IsDefaultValue = 0;
                newFunction.FunctionOrderID = 0;
                Guid functionID = _tagUserService.AddFunction(newFunction);

                GLO_TagCategory newData = new GLO_TagCategory();
                newData.UserID = LoginedUser.UserID;
                newData.FunctionID = functionID;
                string resultid =  _tagUserService.AddTagCategory(newData).ToString();
            }
            else
            {
                function.FunctionName = functionname;
                _tagUserService.FunctionEdit(function);
            }
            return Json(true);
        }
        public JsonResult AddTagCategory(string functionName)
        {
            string resutID = string.Empty;
            Guid functionID = Guid.Empty;
            if (!string.IsNullOrEmpty(functionName))
            {
                GLO_Functions oldFunction = _tagUserService.GetFunctionByName(functionName);
                if (oldFunction != null)
                {
                    functionID = oldFunction.FunctionID;
                }
                else
                {
                    GLO_Functions newFunction = new GLO_Functions();
                    newFunction.FunctionID = Guid.NewGuid();
                    newFunction.FunctionName = functionName;
                    newFunction.FunctionType = LoginedUser.UserType;
                    newFunction.IsDefaultValue = 0;
                    newFunction.FunctionOrderID = 0;
                    functionID = _tagUserService.AddFunction(newFunction);
                }
                GLO_TagCategory oldData = _tagUserService.GetTagCategoryByName(LoginedUser.UserID, functionID);
                if (oldData != null)
                {
                    resutID = oldData.TagCategoryID.ToString();
                }
                else
                {
                    GLO_TagCategory newData = new GLO_TagCategory();
                    newData.UserID = LoginedUser.UserID;
                    newData.FunctionID = functionID;
                    resutID = _tagUserService.AddTagCategory(newData).ToString();
                }
                return Json(resutID);
            }
            else
            {
                return Json(resutID);
            }
        }
        public JsonResult DeleteTagCategory(int categoryid)
        {
            var category = _tagUserService.GetTagCategoryList(LoginedUser.UserID).FirstOrDefault(x => x.TagCategoryID == categoryid);
            if (category != null && category.GLO_TagUsers.Count > 0)
            {
                return Json(false);
            }
            else
            {
                _tagUserService.DeleteTagCategory(categoryid);
                return Json(true);
            }
        }
        public JsonResult DeleteValue(int id)
        {
            try
            {
                _tagUserService.DeleteTagCategory(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteTaggedUser(int tagID)
        {
            try
            {
                _tagUserService.DeleteTagUser(tagID);
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        private static GLO_TagUsers DeserializeXmlTagUsers(string record)
        {
            if (String.IsNullOrEmpty(record)) return null;
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<GLO_TagUsers>(record);
        }

        #region Expert tag list
        public ActionResult ExpertTag(int userID, int type)
        {
            GLO_Users user = _accountService.GetUserByUserID(userID);
            ViewBag.Type = type;
            ViewBag.Select = type == 3 ? "CompanyTag" : "LeaderTag";
            var myTaggedList = _tagUserService.GetTagListByID(userID, type);
            var theyTaggedMeList = _tagUserService.TheyTagMe(userID, type);
            IList<GLO_Users> tageach = new List<GLO_Users>();
            foreach (var itag in myTaggedList)
            {
                foreach (var theytagme in theyTaggedMeList)
                {
                    if (itag.TaggedUserID == theytagme.UserID)
                    {
                        tageach.Add(theytagme);
                    }
                }
            }
            ViewBag.TagList = myTaggedList;
            ViewBag.TheyTagme = theyTaggedMeList;
            ViewBag.TagEachOther = tageach;;
            return View(user);
        }
        #endregion


        #region Leader tag list
        public ActionResult LeaderTag(int userID, int type)
        {
            GLO_Users user = _accountService.GetUserByUserID(userID);
            ViewBag.Type = type;
            ViewBag.Select = type == 3 ? "CompanyTag" : "ExpertTag";
            var myTaggedList = _tagUserService.GetTagListByID(userID, type);
            var theyTaggedMeList = _tagUserService.TheyTagMe(userID, type);
            IList<GLO_Users> tageach = new List<GLO_Users>();
            foreach (var itag in myTaggedList)
            {
                foreach (var theytagme in theyTaggedMeList)
                {
                    if (itag.TaggedUserID == theytagme.UserID)
                    {
                        tageach.Add(theytagme);
                    }
                }
            }
            ViewBag.TagList = myTaggedList;
            ViewBag.TheyTagme = theyTaggedMeList;
            ViewBag.TagEachOther = tageach; ;
            return View(user);
        }
        #endregion

        #region request tag
        [HttpPost]
        public JsonResult RequestTag(int requestUserID)
        {
            var requestUser = _accountService.GetUserByUserID(requestUserID);
            string url = string.Empty;
            if (LoginedUser.UserType == 3)
            {
                url = "/company/FactSheet?companyid=" + LoginedUser.UserID;
            }
            else if (LoginedUser.UserType == 2)
            {
                url = "/expert/myvalue?expertid=" + LoginedUser.UserID;
            }
            else if (LoginedUser.UserType == 1)
            {
                url = "/leader/index?leaderid=" + LoginedUser.UserID;
            }
            string username = requestUser.NickName;
            if (requestUser.TypeID == 1)
                username = requestUser.GLO_Leader.FirstName;
            else if (requestUser.TypeID == 2)
                username = requestUser.GLO_HRExpert.FirstName;

            GLO_Message message = new GLO_Message();
            message.MessageTitle = LoginedUser.UserName + " requested to be tagged";
            message.MessageContect = "Dear " + username + ",<br/><br/>" + LoginedUser.UserName + " has requested to be tagged for future contact. Kindly confirm  by visiting <a href=\"" + url + "\">" + LoginedUser.UserName + "</a>’s profile and clicking on the Tag button or contact them for further info.<br/><br/>Thanks<br/><br/>The GLO team";
            message.SendDate = DateTime.Now;
            message.MessageTypeID = 1;
            _messageService.SendSystemMessage(message,requestUserID.ToString());
            return Json(true);
        }
        #endregion
    }
}
