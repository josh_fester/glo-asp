﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using System.IO;
using GLO.Data.Models;
using System.Data.EntityModel;
using System.Web.Security;
using GLO.Web.Models;
using System.Web.Script.Serialization;
using GLO.Web.Common;



namespace GLO.Web.Controllers
{
    public class HomeController : BaseController
    {        
        #region Service
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();
        private IAccountService _accountservice;
        private ICompanyService _companyservice;
        private IExpertService _expertservice;
        private IFAQService _faqservice;
        private IDifferenceService _differenceservice;
        private IPolicyService _policyservice;
        private IPartnerService _partnerservice;
        private IFeedBackService _feedbackservice;
        private ILeaderService _leaderservice;
        private ICompetenceService _competenceservice;
        private ITagUserService _taguserservice;
        private IWebInfoService _webinfoservice;
        private ISolutionsService _solutionsService;
        private ISuccessService _successservice;
        public HomeController(IAccountService AccountService, ILeaderService LeaderService, ICompanyService CompanyService, 
            IExpertService ExpertService, IFAQService FAQService, IDifferenceService DifferenceService, 
            IPolicyService PolicyService, IPartnerService PartnerService, IFeedBackService FeedBackService, 
            ICompetenceService CompetenceService, ITagUserService TagUserService, IWebInfoService WebInfoService,
            ISolutionsService SolutionsService, ISuccessService SuccessService)
        {
            _accountservice = AccountService;
            _leaderservice = LeaderService;
            _companyservice = CompanyService;
            _expertservice = ExpertService;
            _faqservice = FAQService;
            _differenceservice = DifferenceService;
            _policyservice = PolicyService;
            _partnerservice = PartnerService;
            _feedbackservice = FeedBackService;
            _competenceservice = CompetenceService;
            _taguserservice = TagUserService;
            _webinfoservice = WebInfoService;
            _solutionsService = SolutionsService;
            _successservice = SuccessService;
        }
        #endregion

        public ActionResult Index()
        {
            if (LoginedUser != null && LoginedUser.UserID != 0)
            {
                ViewBag.Logined = true;
            }
            else
            {
                ViewBag.Logined = false;
            }
            ViewBag.RecommendLeader = _leaderservice.RecommendLeaderList(4);
            ViewBag.RecommendExpert = _expertservice.RecommendExpert(4);
            ViewBag.RecommendCompany = _companyservice.RecommendCompanyList(9);
            return View(_leaderservice.RecommendLeaderList(12));
        }

        public ActionResult Partner()
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _partnerservice.GetEnableList(100, 1, out RecordCount);
            return View(list);
        }

        [OutputCache(Duration=60)]
        public ActionResult Statistics()
        {
            var leader = _accountservice.GetAllLeaderList();
            var leaderbyindustry = leader.Where(x => x.GLO_UserIndustry.Count > 0);
            string leaderindustry = string.Empty;
            foreach (var led in leaderbyindustry.GroupBy(x => x.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName))
            {
                leaderindustry += string.Format("['{0}', {1}],", led.Key.ToString(), led.Count());
            }
            ViewBag.LeaderByIndustry = leaderindustry;
            var leaderbylocation = leader.Where(x => x.GLO_UserLocation.Count > 0);
            string leaderlocation = string.Empty;
            foreach (var led in leaderbylocation.GroupBy(x => x.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName))
            {
                leaderlocation += string.Format("['{0}', {1}],", led.Key.ToString(), led.Count());
            }
            ViewBag.LeaderByLocation = leaderlocation;

            var expert = _accountservice.GetAllExpertList();
            var expertbyindustry = expert.Where(x => x.GLO_UserIndustry.Count > 0);
            string expertindustry = string.Empty;
            foreach (var exp in expertbyindustry.GroupBy(x => x.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName))
            {
                expertindustry += string.Format("['{0}', {1}],", exp.Key.ToString(), exp.Count());
            }
            ViewBag.ExpertByIndustry = expertindustry;
            var expertbylocation = expert.Where(x => x.GLO_UserLocation.Count > 0);
            string expertlocation = string.Empty;
            foreach (var exp in expertbylocation.GroupBy(x => x.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName))
            {
                expertlocation += string.Format("['{0}', {1}],", exp.Key.ToString(), exp.Count());
            }
            ViewBag.ExpertByLocation = expertlocation;

            var company = _accountservice.GetAllCompanyList();                     
            var companybyindustry = company.Where(x => x.GLO_UserIndustry.Count>0);
            string companyindustry = string.Empty;
            foreach (var com in companybyindustry.GroupBy(x => x.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName))
            {
                companyindustry += string.Format("['{0}', {1}],", com.Key.ToString(), com.Count());
            }
            ViewBag.CompanyByIndustry = companyindustry;

            var companybyemployee = company.GroupBy(x => x.GLO_Company.Size);
            string companyemployee = string.Empty;
            foreach (var com in companybyemployee)
            {
                companyemployee += string.Format("['{0}', {1}],", com.Key, com.Count());
            }
            ViewBag.CompanyByEmployee = companyemployee;

            return View();
        }
        public ActionResult Success(string recommendid)
        {
            var successlist =_successservice.GetSuccessCaseList().Where(x=>x.IsAble).ToList();
            int RecommendId = 0;
            if (recommendid != null)
            {
                try
                {
                    string DecryptedRecommendId = DEEncrypt.Decrypt(recommendid);
                    RecommendId = int.Parse(DecryptedRecommendId);
                }
                catch { }
            }
            if (RecommendId != 0)
            {
                var recommendUser = _accountservice.GetUserByUserID(RecommendId);
                ViewBag.RecommendUserNickName = recommendUser.NickName;
            }
            else
            {
                ViewBag.RecommendUserNickName = "";
            }
            return View(successlist);
        }
        public JsonResult _SuccessDetail(int successid)
        {
            string control = successid == 0 ? "SuccessAdd" : "SuccessDetail";
            var success = _successservice.GetSuccessCaseByID(successid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Success/" + control + ".ascx", success)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SuccessRecommendAdd(string jsondata)
        {
            if (LoginedUser!=null && LoginedUser.UserID > 0)
            {
                GLO_Success success = DeserializeXmlT<GLO_Success>(jsondata);
                success.UserID = LoginedUser.UserID;
                success.CreateDate = DateTime.Now;
                success.IsAble = false;
                _successservice.AddSuccessCase(success);
                return Json(true);
            }
            else
                return Json(false);
        }
        public ActionResult Insight()
        {
            return View("Survey");
        }
        public ActionResult Solution()
        {
            ViewBag.RecommendLeader = _leaderservice.RecommendLeaderList(4);
            ViewBag.RecommendCompany = _companyservice.RecommendCompanyList(9);
            int RecordCount = 0;
            var allSolutionList = _solutionsService.GetEnableList(100, 1, out RecordCount);
            ViewBag.OnlineTopList = allSolutionList.Where(t => t.CategoryID == 1).OrderBy(t => t.OrderId).Skip(0).Take(8).ToList();
            ViewBag.OnlineBottomList = allSolutionList.Where(t => t.CategoryID == 2).OrderBy(t => t.OrderId).Skip(0).Take(8).ToList();
            ViewBag.OfflineTopList = allSolutionList.Where(t => t.CategoryID == 3).OrderBy(t => t.OrderId).Skip(0).Take(8).ToList();
            ViewBag.OfflineBottomList = allSolutionList.Where(t => t.CategoryID == 4).OrderBy(t => t.OrderId).Skip(0).Take(8).ToList();
            return View();
        }
        public ActionResult Assurance()
        {
            return View();
        }
        public ActionResult GLODifference()
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _differenceservice.GetEnableList(100, 1, out RecordCount);
            return View(list);
        }
        public ActionResult Policy(string t)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _policyservice.GetEnableList(100, 1, out RecordCount);
            ViewBag.Policy = _policyservice.GetByID(3).PolicyContent;
            return View(list);
        }
        public ActionResult AccessForbidden()
        {
            return View();
        }
        public ActionResult UserNotExist()
        {
            return View();
        }
        [Authorize]
        public ActionResult NotApproved(string type="notapproved")
        {
            ViewBag.Type = "notapproved";
            ViewBag.Message = @"The dashboard is for registered and approved users only. Once your membership is confirmed, 
                              you will have full access and can start using all the benefits of your customized dashboard. 
                              We look forward to seeing you here soon but ask for your patience as we review your application.";
            if (type == "notpayment")
            {
                ViewBag.Type = "notpayment";
                ViewBag.Message = "Only paying user can use this function. Please pay the membership fee first.";
            }
            else if (type == "islocked")
            {
                ViewBag.Message = "The user you are visiting has been blocked by administrator!";
            }
            else if (type == "notpublish")
            {
                ViewBag.Message = "The company you try to visit has opted not to make its information public.";
                if (LoginedUser.UserType == 1)
                    ViewBag.Message = "The company you try to visit has opted not to make its information public. You can however search jobs and tag them or send your resume to the company contacts listed.";              
            }
            else if (type == "unauthorized")
            {
                ViewBag.Message = "The user you are visiting has not been authorized yet.";
            }
            else if (type == "noright")
            {
                ViewBag.Message = "you have no right to access the page.";
            }
            else if (type == "notexist")
            {
                ViewBag.Message = "The user you are visiting is not existed.";
            }
            else if (type == "forbidden")
            {
                ViewBag.Message =@"GLO connects good leaders for good business.
                                We are not a social network and hence do not allow leaders to communicate with other leaders online.<br/> 
                                GLO regularly organizes offline events which our registered members can join to share experiences and thoughts.<br/> 
                                We also invite you to our GLO insights where you can share comments and other interesting news."; 
            }
            return View();
        }

        public JsonResult _PolicyDetail()
        {
            var policy = _policyservice.GetByID(3);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Policy.ascx", policy)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _PolicyDetailExpert()
        {
            var policy = _policyservice.GetByID(6);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/PolicyExpert.ascx",policy)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
  
        }
        public ActionResult FeedBack()
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _feedbackservice.GetEnableList(100, 1, out RecordCount);
            return View(list);
        }
        public ActionResult FAQ()
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _faqservice.GetEnableList(100, 1, out RecordCount);
            return View(list);
        }
        public JsonResult FAQSubmit(string jsondata)
        {
            GLO_FAQ faq = DeserializeXmlT<GLO_FAQ>(jsondata);
            faq.OrderId = 0;
            faq.Status = false;
            _faqservice.Add(faq);
            return Json(true);
        }
        public ActionResult Concept()
        {
            ViewBag.Conecpt = _policyservice.GetByID(2).PolicyContent;
            return View();
        }
        public ActionResult RegisterGuide(string from="")
        {
            RecommendUsers recommend = new RecommendUsers();
            recommend.Leaders = _leaderservice.RecommendLeaderList(4);
            recommend.Experts = _expertservice.RecommendExpert(4);
            recommend.Companies = _companyservice.RecommendCompanyList(9);
            if (!string.IsNullOrEmpty(Request.QueryString["inviteid"]))
            {
                Session["InviteUserID"] = Request.QueryString["inviteid"];
                ViewBag.NickName= _accountservice.GetUserByUserID(int.Parse(Request.QueryString["inviteid"].ToString())).NickName;
            }
            return View(recommend);
        }
        public ActionResult RegisterLegal()
        {
            ViewBag.Content =_policyservice.GetByID(1).PolicyContent;
            ViewBag.Type =Request.QueryString["type"].ToString();
            return View();
        }
        public JsonResult _RegisterUser(int userType)
        {
            RegisterModel user = new RegisterModel();
            user.UserType = userType;
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/RegisterUserControl.ascx", user)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RegisterProfile(string inviteid)
        {
            GLO_Recommendation recommend = new GLO_Recommendation();
            if (Session["InviteUserID"] == null)
            {
                recommend.UserID = int.Parse(inviteid);
                Session["InviteUserID"] = recommend.UserID;
            }
            else
            {
                if(!string.IsNullOrEmpty(inviteid) && int.Parse(inviteid) != int.Parse(Session["InviteUserID"].ToString()))
                    Session["InviteUserID"] = int.Parse(inviteid);

                recommend.UserID = int.Parse(Session["InviteUserID"].ToString());
            }
            recommend.RecommendUserID = int.Parse(Request.QueryString["expertid"].ToString());
            GLO_Users user = _accountservice.GetUserByUserID(recommend.UserID);
            recommend.GLO_Users = user;
            Session["NotRecommendID"] = recommend.RecommendUserID;
            ViewBag.CompetenciesList = user.GLO_Competencies.Where(x => x.CompetencyType == 1 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
            ViewBag.Values = user.GLO_Competencies.Where(x => x.CompetencyType == 3 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
            return View(recommend);
        }
        public ActionResult RecommendationEdit(int userID)
        {
            var user = _accountservice.GetUserByUserID(userID);
            GLO_Recommendation recommend = _accountservice.GetRecommendationByUserID(userID, LoginedUser.UserID);
            ViewBag.CompetenciesList = user.GLO_Competencies.Where(x => x.CompetencyType == 1 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
            ViewBag.OtherCompetenciesList = user.GLO_Competencies.Where(x => x.CompetencyType == 2).OrderBy(x => x.CreateTime).ToList();
            ViewBag.Values = user.GLO_Competencies.Where(x => x.CompetencyType == 3 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
            ViewBag.OtherValues = user.GLO_Competencies.Where(x => x.CompetencyType == 4).OrderBy(x => x.CreateTime).ToList();
            Session["InviteUserID"] = recommend.UserID;
            return View(recommend);
        }
        [HttpPost]
        public JsonResult RecommendationEdit(string recommend)
        {
            GLO_Recommendation recommedation = new GLO_Recommendation();
            recommedation = DeserializeXmlRecommendation(recommend);
            _accountservice.UpdateRecommendation(recommedation);
            var result =
            new
            {
                UserID = recommedation.UserID,
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RecommendationAdd(string recommend, int inviteUserID)
        {
            if (inviteUserID > 0)
            {
                GLO_Recommendation recommedation = new GLO_Recommendation();
                recommedation = DeserializeXmlRecommendation(recommend);
                recommedation.UserID = inviteUserID;
                _expertservice.RecommendationAdd(recommedation);

                Session["NotRecommendID"] = null;
             var result =
             new
             {
                 UserID = recommedation.RecommendUserID,
             };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your quintessential app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your quintessential contact page.";
            ViewBag.ContactUs = _policyservice.GetByID(5).PolicyContent;
            return View();
        }
        #region Upload
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    string fileExtension = Path.GetExtension(fileName); // extension file name
                    string saveName = Guid.NewGuid().ToString() + fileExtension; //rename file

                   // fileData.SaveAs(filePath + saveName);
                    string virtualpathsecured = "/images/" + saveName;
                    if (fileExtension == ".jpg" || fileExtension == ".gif" || fileExtension == ".png")
                    {
                        ZooAuto.ZoomAuto(fileData.InputStream, filePath + saveName, 186, 186, "GLO", "");
                    }
                    else
                    {
                        fileData.SaveAs(filePath + saveName);
                    }
                    return Json(new { Success = true, FileName = fileName, SaveName = saveName, Urlpath=virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadPPT(HttpPostedFileBase fileData, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Json(new { Success = false, Message = "please login again！" }, JsonRequestBehavior.AllowGet);
            }
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Upload/" + id + "/");
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    //else
                    //{
                    //    var fileList = Directory.GetFiles(filePath).ToList();
                    //    foreach (var file in fileList)
                    //    {
                    //        string oldFilename=file.Substring(file.LastIndexOf('\\')+1);
                    //        string fileExtension = oldFilename.Substring(oldFilename.LastIndexOf('.')+1);
                    //        if (fileExtension == "pptx" && oldFilename != fileName && System.IO.File.Exists(file))
                    //        {
                    //            System.IO.File.Delete(file);
                    //        }
                    //    }
                    //}

                    string virtualpathsecured = "/Upload/" + id +"/" + fileName;
                    
                    fileData.SaveAs(filePath + fileName);

                    return Json(new { Success = true, FileName = fileName, SaveName = fileName, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadResume(HttpPostedFileBase fileData, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Json(new { Success = false, Message = "please login again！" }, JsonRequestBehavior.AllowGet);
            }
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Upload/" + id + "/");
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    else
                    {
                        var fileList = Directory.GetFiles(filePath).ToList();
                        foreach (var file in fileList)
                        {
                            string oldFilename = file.Substring(file.LastIndexOf('\\') + 1);
                            if (oldFilename == fileName && System.IO.File.Exists(file))
                            {
                                return Json(new { Success = false, Message = "The file already exist." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    string virtualpathsecured = "/Upload/" + id + "/" + fileName;

                    fileData.SaveAs(filePath + fileName);
                    Guid saveID = _leaderservice.AddResume(int.Parse(id), fileName);
                    return Json(new { Success = true, FileName = fileName, SaveName = fileName,SaveID = saveID, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }        
        #endregion

        #region Search
        //[IsManagerAttribute]
        //[Authorize]
        public ActionResult JobSearch(string keyword,string location, string func)
        {
            var joblist = _companyservice.SearchJob(keyword, location, func);
            ViewBag.Location = !string.IsNullOrEmpty(location) ? location : string.Empty;
            ViewBag.Func = !string.IsNullOrEmpty(func) ? func : string.Empty;
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.FuncList = _taguserservice.GetSystemFunctionList();
            return View(joblist);
        }

        [Authorize]
        public ActionResult AdvanceJobSearch(string keyword, string location, string func, string industry,string education, string date)
        {
            ViewBag.IndustryList = _webinfoservice.GetIndustyByParentID(-1);
            ViewBag.Location = !string.IsNullOrEmpty(location) ? location : string.Empty;
            ViewBag.Func = !string.IsNullOrEmpty(func) ? func : string.Empty;
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.FuncList = _taguserservice.GetSystemFunctionList();
            ViewBag.Date = !string.IsNullOrEmpty(date) ? date : string.Empty;
            ViewBag.Education = !string.IsNullOrEmpty(education) ? education : string.Empty;
            industry = !string.IsNullOrEmpty(industry)?industry.Replace("__","&"):string.Empty;
            ViewBag.Industry = !string.IsNullOrEmpty(industry) ? industry : string.Empty;
            var joblist = _companyservice.SearchJob(keyword, location, func,industry,education,date);
            return View(joblist);
        }
        public JsonResult _GetJobDetail(int JobID)
        {
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/JobDetail.ascx", _companyservice.GetCompanyJobByID(JobID))
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _GetLocationList()
        {
            var locationlist = _webinfoservice.GetLocationByParentID(-1);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/LocationSelect.ascx", locationlist)
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _GetIndustryList()
        {
            var industrylist = _webinfoservice.GetIndustyByParentID(-1);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/IndustrySelect.ascx", industrylist)
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        //[IsManagerAttribute]
        public ActionResult HeaderSearch(string c, string e, string l)
        {
            if (LoginedUser.UserType == 3)
            {
                List<GLO_Users> resultLit = new List<GLO_Users>();
                var leaderList = _accountservice.SearchUserList(l, 1, null);
                foreach (var leader in leaderList)
                {
                    bool isAdd = true;
                    if (leader.GLO_Leader.GLO_LeaderKeyWord.Count > 0)
                    {
                        foreach (var companyKeyWord in leader.GLO_Leader.GLO_LeaderKeyWord)
                        {
                            if (LoginedUser.UserName.ToUpper().Contains(companyKeyWord.KeyWordContent.ToUpper()))
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                    {
                        resultLit.Add(leader);
                    }
                }
                ViewBag.LeaderList = resultLit;
            }
            else if (LoginedUser.UserType == 1)
            {
                ViewBag.LeaderList = new List<GLO_Users>();
            }
            else
            {
                ViewBag.LeaderList = _accountservice.SearchUserList(l, 1, null);
            }
            ViewBag.ExpertList = _accountservice.SearchUserList(e, 2, null);
            ViewBag.CompanyList = _accountservice.SearchUserList(c, 3, null);
            ViewBag.TagUserList = _taguserservice.GetTaggedUserList(LoginedUser.UserID);
            ViewBag.UserType = LoginedUser.UserType;
            ViewBag.UserID = LoginedUser.UserID;
            return View();
        }
        [HttpPost]
        public PartialViewResult ExpertSearch(string keyword,string location,string industry)
        {
            ViewBag.UserID = LoginedUser.UserID;
            ViewBag.UserType = LoginedUser.UserType;
            ViewBag.TagUserList = _taguserservice.GetTaggedUserList(LoginedUser.UserID);
            var result = _accountservice.SearchUserList(keyword, 2, location,industry.Replace("__","&"));
            return PartialView("/Views/Shared/Search/ExpertSearch.ascx",result);
        }
        [HttpPost]
        public PartialViewResult LeaderSearch(string keyword, string location, string industry)
        {
            ViewBag.UserID = LoginedUser.UserID;
            ViewBag.UserType = LoginedUser.UserType;
            ViewBag.TagUserList = _taguserservice.GetTaggedUserList(LoginedUser.UserID);
            List<GLO_Users> resultLit = new List<GLO_Users>();
            if (LoginedUser.UserType == 3)
            {
                var leaderList = _accountservice.SearchUserList(keyword, 1, location, industry.Replace("__", "&"));
                foreach (var leader in leaderList)
                {
                    bool isAdd = true;
                    if (leader.GLO_Leader.GLO_LeaderKeyWord.Count > 0)
                    {
                        foreach (var companyKeyWord in leader.GLO_Leader.GLO_LeaderKeyWord)
                        {
                            if (LoginedUser.UserName.ToUpper().Contains(companyKeyWord.KeyWordContent.ToUpper()))
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                    {
                        resultLit.Add(leader);
                    }
                }
            }
            else
            {
                resultLit = _accountservice.SearchUserList(keyword, 1, location);
            }
            return PartialView("/Views/Shared/Search/LeaderSearch.ascx", resultLit);
        }
        [HttpPost]
        public PartialViewResult CompanySearch(string keyword, string location, string industry)
        {
            ViewBag.UserID = LoginedUser.UserID;
            ViewBag.UserType = LoginedUser.UserType;
            ViewBag.TagUserList = _taguserservice.GetTaggedUserList(LoginedUser.UserID);
            var result = _accountservice.SearchUserList(keyword, 3, location, industry.Replace("__", "&"));
            return PartialView("/Views/Shared/Search/CompanySearch.ascx", result);
        }
        #endregion

        private static GLO_Recommendation DeserializeXmlRecommendation(string record)
        {
            if (String.IsNullOrEmpty(record)) return null;
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<GLO_Recommendation>(record);
        }


        [HttpPost]
        public JsonResult FeedBackSendEmail(string Email,string Name, string Subject, string Content)
        {
            try
            {
                string emailContent = "Email:" + Email + "<br/>Name:" + Name + "<br/>Subject:" + Subject + "<br/>Content:" + Content;
                ExpertHelper.EMailSend(System.Configuration.ConfigurationManager.AppSettings["FeedBackEmail"], "FeedBack", emailContent,"");
                return Json(new { Result = true, Message = "" });
            }
            catch (Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }


        [HttpPost]
        public JsonResult ContactUsSendEmail(string From, string Subject, string Content)
        {
            try
            {
                string emailContent = "From:" + From + "<br/>Subject:" + Subject + "<br/>Memo:" + Content;
                ExpertHelper.EMailSend(System.Configuration.ConfigurationManager.AppSettings["ContactUsEmail"], "ContactUs", emailContent,"");
                return Json(new { Result = true, Message = "" });
            }
            catch (Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }

        #region set unread message count
        [HttpPost]
        public JsonResult SetUnreadMessage()
        {
            int unReadCount = _accountservice.GetUserByUserID(LoginedUser.UserID).GLO_MessageReceive.Where(x => x.Status == 0).Count();
            Session["UnReadMessageCount"] = unReadCount;            
            return Json(unReadCount);
        }
        #endregion

        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }
    }
}
