﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data.Models;
using GLO.Web.Models;

namespace GLO.Web.Controllers
{
    [Authorize]
    [CheckApprovedAttribute]
    public class MessageController : BaseController
    {
        private IAccountService _accountservice;
        private IMessageService _messageservice;
        private ITagUserService _taguserservice;

        public MessageController(IAccountService AccountService, IMessageService MessageService, ITagUserService TagUserService)
        {
            _accountservice = AccountService;
            _messageservice = MessageService;
            _taguserservice = TagUserService;
        }



        #region Compose Message
        public ActionResult ComposeMessage(int expertid)
        {
            ViewBag.Select = "ComposeMessage";
            GLO_Users expert = _accountservice.GetUserByUserID(expertid);
            MessageModel message = new MessageModel();
            message.SendUserID = expertid;
            ViewBag.SendMessage = message;
            return View(expert);
        }
        public JsonResult SendMessageToUser(int id)
        {
            MessageModel message = new MessageModel();
            message.SendUserID = LoginedUser.UserID;
            message.ReceiveUserID = id.ToString();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/SendMessage.ascx", message)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SendMessage(int messageid, int senduserid, string receiveuserid, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            if (messageid != 0)
            {
                message.MessageID = messageid;
            }
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            _messageservice.MessageCreate(message, senduserid, receiveuserid);
            return Json(true);
        }
        public JsonResult EditMessage(int messageid, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            message.MessageID = messageid;
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            _messageservice.MessageUpdate(message);
            return Json(true);
        }
        public JsonResult SaveMessage(int senduserid, string receiveuserid, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            _messageservice.MessageSave(message, senduserid, receiveuserid);
            return Json(true);
        }
        public JsonResult _MessageDetail(int messageid, int receiveid)
        {
            var message = _messageservice.GetMessageByID(messageid);
            MessageModel resultModel = new MessageModel();
            resultModel.MessageTitle = message.MessageTitle;
            resultModel.MessageContent = message.MessageContect;
            resultModel.MessageID = message.MessageID;
            resultModel.SendDate = message.SendDate.Value;
            resultModel.ReceiveUserNikeName = GetReceiveUserNickName(message, 0);
            var messagereceive = _messageservice.GetMessageReceiveByID(receiveid);
            if (messagereceive.Status == 0)
            {
                messagereceive.Status = 1;
                _messageservice.MessageReceiveUpdate(messagereceive);
                var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
                CookieHelper.SetCookie(user);
            }
            if (message.MessageTypeID == 2)
            {
                resultModel.SendUserID = message.GLO_MessageSend.SendUserID;
                resultModel.SendUserNikeName = message.GLO_MessageSend.GLO_Users.NickName;
                resultModel.SendUserUrl = GetUserUrl(message.GLO_MessageSend.SendUserID, message.GLO_MessageSend.GLO_Users.TypeID);
            }
            else
            {
                resultModel.SendUserNikeName = "The GLO Team";
                resultModel.SendUserUrl = "javascript:void(0);";
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/MessageDetail.ascx", resultModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _SendMessageEdit(int messageid, string receiveid, string status)
        {
            var message = _messageservice.GetMessageByID(messageid);
            var newMessage = new MessageModel();
            newMessage.SendUserID = LoginedUser.UserID;
            if (status == "forward")
            {
                newMessage.MessageTitle = "FW:"+message.MessageTitle;
                newMessage.MessageContent = message.MessageContect;
            }
            else
            {
                if (receiveid == "all")
                {
                    newMessage.AdminSendType = 1;
                }
                else if (receiveid == "company")
                {
                    newMessage.AdminSendType = 2;
                }
                else if (receiveid == "leader")
                {
                    newMessage.AdminSendType = 3;
                }
                else if (receiveid == "expert")
                {
                    newMessage.AdminSendType = 4;
                }
                else
                {
                    newMessage.AdminSendType = 5;
                    newMessage.ReceiveUserList = new List<GLO_Users>();
                    string[] idArray = receiveid.Split(';');
                    foreach (var id in idArray)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            newMessage.ReceiveUserList.Add(_accountservice.GetUserByUserID(int.Parse(id.ToString())));
                        }
                    }
                }
                if (status == "reply")
                {
                    newMessage.MessageTitle = "RE:" + message.MessageTitle;
                    newMessage.MessageContent = "";
                }
                else
                {
                    newMessage.MessageTitle = message.MessageTitle;
                    newMessage.MessageContent = message.MessageContect;
                }
            }
            string pageName = "MessageEdit.ascx";
            if (LoginedUser.UserType == 4)
            {
                pageName = "MessageEditForAdmin.ascx";
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/" + pageName, newMessage)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _SendMessageDetail(int messageid,int listType)
        {
            var message = _messageservice.GetMessageByID(messageid);
            MessageModel resultModel = new MessageModel();
            resultModel.MessageTitle = message.MessageTitle;
            resultModel.MessageContent = message.MessageContect;
            resultModel.MessageID = message.MessageID;
            resultModel.SendDate = message.SendDate.Value;
            if (listType == 1)
            {
                resultModel.SendUserNikeName = message.GLO_MessageSend.GLO_Users.NickName;
            }
            else if (listType == 2)
            {
                resultModel.SendUserNikeName = message.GLO_MessageArchive.GLO_Users.NickName;
            }
            else if (listType == 3)
            {
                resultModel.SendUserNikeName = message.GLO_MessageTrash.ToList()[0].GLO_Users.NickName;
            }
            resultModel.ReceiveUserNikeName = GetReceiveUserNickNameForDetail(message, listType);
            string userControls = "SendMessageDetail.ascx";
            if (listType == 3 && message.GLO_MessageTrash.ToList()[0].SendUserID != LoginedUser.UserID)
            {
                userControls = "MessageDetail.ascx";
                var senduser = _accountservice.GetUserByUserID(message.GLO_MessageTrash.ToList()[0].SendUserID);
                resultModel.SendUserNikeName = senduser.NickName;
                resultModel.SendUserUrl = GetUserUrl(senduser.UserID, senduser.TypeID);
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/" + userControls, resultModel)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _TagUserList(int expertid)
        {
            ReceiveUserModel model = new ReceiveUserModel();
            model.TagUserList = new List<ReceiveUserInfoModel>();
            var taguserList = _taguserservice.GetTagListByID(expertid,0);
            foreach (var taguser in taguserList)
            {
                var user = taguser.GLO_Users1;
                model.TagUserList.Add(CreatReceiveUserInfoModel(user));
            }

            model.RecentContactList = new List<ReceiveUserInfoModel>();
            var contactList = _taguserservice.GetRecentContactList(expertid);
            foreach (var contact in contactList)
            {
                var user = contact.GLO_Users;
                model.RecentContactList.Add(CreatReceiveUserInfoModel(user));
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/TagUserList.ascx", model)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _UserList()
        {
            List<GLO_Users> users = _accountservice.GetAllUserList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/UserList.ascx", users)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ReceiveUserInfoModel CreatReceiveUserInfoModel(GLO_Users user)
        {
            ReceiveUserInfoModel receinfoModel = new ReceiveUserInfoModel();
            receinfoModel.UserID = user.UserID;
            receinfoModel.NickName = user.NickName;
            if (user.TypeID == 3)
            {
                receinfoModel.Icon = user.GLO_Company.LogoUrl;
                receinfoModel.Title = user.GLO_Company.Industry;
                receinfoModel.Location = user.GLO_Company.Website;
            }
            else if (user.TypeID == 1)
            {
                receinfoModel.Icon = user.GLO_Leader.LogoUrl;
                receinfoModel.Title = user.GLO_Leader.Title;
                receinfoModel.Location = user.GLO_Leader.Location;
            }
            else if (user.TypeID == 2)
            {
                receinfoModel.Icon = user.GLO_HRExpert.LogoUrl;
                receinfoModel.Title = user.GLO_HRExpert.Title;
                receinfoModel.Location = user.GLO_HRExpert.Location;
            }
            else
            {
                receinfoModel.Icon = user.Icon;
                receinfoModel.Title = "GLO Administrator";
                receinfoModel.Location = "GLO";
            }
            return receinfoModel;
        }
        #endregion

        #region Message index, trash, archive
        public ActionResult Message(int expertid, string status)
        {
            var user = _accountservice.GetUserByUserID(expertid);
            if (user.TypeID != 2)
            {
                if (user.TypeID == 1)
                {
                    return RedirectToAction("LeaderMessage", "Message", new { leaderid = expertid, status = status });
                }
                else if (user.TypeID == 3)
                {
                    return RedirectToAction("CompanyMessage", "Message", new { companyid = expertid, status = status });
                }
                else
                {
                    return RedirectToAction("AdminMessage", "Message", new { status = status });
                }
            }
            //else if (!user.GLO_UserEx.BasicInfoStatus)
            //{
            //    return RedirectToAction("MyValue","Expert", new { expertid = expertid });
            //}
            //else if (!user.GLO_UserEx.ValueStatus)
            //{
            //    return RedirectToAction("MyValue", "Expert", new { expertid = expertid });
            //}
            //else if (!user.GLO_UserEx.CredentialsStatus)
            //{
            //    return RedirectToAction("MyCredential", "Expert", new { expertid = expertid });
            //}
            //else if (!user.GLO_UserEx.CompetenciesStatus)
            //{
            //    return RedirectToAction("MyCompetence", "Expert", new { expertid = expertid });
            //}
            //else if (!user.GLO_UserEx.RegisterSuccessStatus)
            //{
            //    return RedirectToAction("RegisterSuccess", "Expert", new { expertid = expertid });
            //}
            else
            {
                GetMessageData(expertid, status);
                GLO_Users expert = _accountservice.GetUserByUserID(expertid);
                ViewBag.UserID = expert.UserID;
                return View(expert);
            }
        }

        public JsonResult MessageUpdate(int receiveid, int status)
        {
            GLO_MessageReceive messagereceive = _messageservice.GetMessageReceiveByID(receiveid);
            messagereceive.Status = byte.Parse(status.ToString());
            _messageservice.MessageReceiveUpdate(messagereceive);
            return Json(true);
        }
        #endregion

        #region  leader message
        public ActionResult LeaderComposeMessage(int leaderid)
        {
            ViewBag.Select = "ComposeMessage";
            GLO_Users leader = _accountservice.GetUserByUserID(leaderid);
            MessageModel message = new MessageModel();
            message.SendUserID = leaderid;
            ViewBag.SendMessage = message;
            return View(leader);
        }
        public ActionResult LeaderMessage(int leaderid, string status)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            if (user.TypeID != 1)
            {
                if (user.TypeID == 2)
                {
                    return RedirectToAction("Message", "Message", new { expertid = leaderid, status = status });
                }
                else if (user.TypeID == 3)
                {
                    return RedirectToAction("CompanyMessage", "Message", new { companyid = leaderid, status = status });
                }
                else
                {
                    return RedirectToAction("AdminMessage", "Message", new { status = status });
                }
            }
            else if (!user.GLO_UserEx.BasicInfoStatus)
            {
                return RedirectToAction("Index","Leader", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.InterViewStatus)
            {
                return RedirectToAction("Index", "Leader", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.CompetenciesStatus)
            {
                return RedirectToAction("MyCompetence", "Leader", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.RegisterSuccessStatus)
            {
                return RedirectToAction("RegisterSuccess", "Leader", new { leaderid = leaderid });
            }
            else
            {
                GetMessageData(leaderid, status);
                GLO_Users expert = _accountservice.GetUserByUserID(leaderid);
                ViewBag.UserID = expert.UserID;
                return View(expert);
            }
        }
        #endregion

        #region company message
        public ActionResult CompanyComposeMessage(int companyid)
        {
            ViewBag.Select = "ComposeMessage";
            GLO_Users company = _accountservice.GetUserByUserID(companyid);
            MessageModel message = new MessageModel();
            message.SendUserID = companyid;
            ViewBag.SendMessage = message;
            return View(company);
        }
        public ActionResult CompanyMessage(int companyid, string status)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(companyid);
            if (expert.TypeID != 3)
            {
                if (expert.TypeID == 1)
                {
                    return RedirectToAction("LeaderMessage", "Message", new { leaderid = companyid, status = status });
                }
                else if (expert.TypeID == 2)
                {
                    return RedirectToAction("Message", "Message", new { expertid = companyid, status = status });
                }
                else
                {
                    return RedirectToAction("AdminMessage", "Message", new { status = status });
                }
            }
            GetMessageData(companyid, status);
            ViewBag.UserID = expert.UserID;
            return View(expert);
        }
        #endregion

        #region admin message
        public ActionResult AdminComposeMessage()
        {
            GLO_Users expert = _accountservice.GetUserByUserID(LoginedUser.UserID);
            MessageModel message = new MessageModel();
            message.SendUserID = expert.UserID;
            ViewBag.SendMessage = message;
            return View(expert);
        }
        public ActionResult AdminMessage(string status)
        {
            ViewBag.IsAdmin = true;
            GetMessageData(LoginedUser.UserID, status);
            GLO_Users expert = _accountservice.GetUserByUserID(LoginedUser.UserID);
            ViewBag.UserID = expert.UserID;
            return View(expert);
        }
        public JsonResult AdminSendMessage(int messageid, int senduserid,int sendtype, string receiveuserid, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            if (messageid != 0)
            {
                message.MessageID = messageid;
            }
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            var userStr = GetUsers(sendtype);
            if(!string.IsNullOrEmpty(userStr))
            {
                receiveuserid = userStr;
            }
            if (sendtype == 1)
            {
                message.ReceiveUserType = 0;
            }
            else if (sendtype == 2)
            {
                message.ReceiveUserType = 3;
            }
            else if (sendtype == 3)
            {
                message.ReceiveUserType = 1;
            }
            else if (sendtype == 4)
            {
                message.ReceiveUserType = 2;
            }
            else
            {
                message.ReceiveUserType = 4;
            }
            _messageservice.MessageCreate(message, senduserid, receiveuserid);
            return Json(true);
        }
        public JsonResult AdminEditMessage(int messageid, int sendtype, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            message.MessageID = messageid;
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            _messageservice.MessageUpdate(message);
            return Json(true);
        }
        public JsonResult AdminSaveMessage(int senduserid, string receiveuserid, int sendtype, string title, string content)
        {
            GLO_Message message = new GLO_Message();
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            if (sendtype == 1)
            {
                receiveuserid = "all";
            }
            else if (sendtype == 2)
            {
                receiveuserid = "company";
            }
            else if (sendtype == 3)
            {
                receiveuserid = "leader";
            }
            else if (sendtype == 4)
            {
                receiveuserid = "expert";
            }
            if (sendtype == 1)
            {
                message.ReceiveUserType = 0;
            }
            else if (sendtype == 2)
            {
                message.ReceiveUserType = 3;
            }
            else if (sendtype == 3)
            {
                message.ReceiveUserType = 1;
            }
            else if (sendtype == 4)
            {
                message.ReceiveUserType = 2;
            }
            else
            {
                message.ReceiveUserType = 4;
            }
            _messageservice.MessageSave(message, senduserid, receiveuserid);
            return Json(true);
        }
        public string GetUsers(int sendtype)
        {
            string result = string.Empty;
            if (sendtype == 1)
            {
                var userList = _accountservice.GetAllUserList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    result += item + ";";
                }
            }
            else if (sendtype == 2)
            {
                var userList = _accountservice.GetAllCompanyList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    result += item + ";";
                }
            }
            else if (sendtype == 3)
            {
                var userList = _accountservice.GetAllLeaderList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    result += item + ";";
                }
            }
            else if (sendtype == 4)
            {
                var userList = _accountservice.GetAllExpertList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    result += item + ";";
                }
            }
            return result;
        }
        [HttpPost]
        public JsonResult GetUserList()
        {
            List<GLO_Users> aList = _accountservice.GetAllUserList();
            var resultList = new SelectList(aList, "UserID", "UserName", null);
            return Json(resultList);
        }
        #endregion

        #region Message Delete
        public JsonResult _SendMessageDelete(int messageid)
        {
            _messageservice.SendMessageDelete(messageid);
            return Json(true);
        }
        public JsonResult _ReceiveMessageDelete(int receiveid)
        {
            _messageservice.ReceiveMessageDelete(receiveid);
            return Json(true);
        }
        public JsonResult _ArchiveMessageDelete(int messageid)
        {
            _messageservice.ArchiveMessageDelete(messageid);
            return Json(true);
        }
        public JsonResult _TrashMessageDelete(int messageid)
        {
            _messageservice.TrashMessageDelete(messageid);
            return Json(true);
        }
        #endregion


        #region Get Message Data List
        public void GetMessageData(int userid,string status)
        {
            ViewBag.Status = status;
            ViewBag.Select = "Inbox";
            List<MessageModel> result = new List<MessageModel>();
            if (status == "0")
            {
                IList<GLO_MessageReceive> reList = _messageservice.GetReceiveMessageList(userid);
                foreach (var item in reList)
                {
                    var senduser = _accountservice.GetUserByUserID(item.SendUserID);
                    MessageModel msgModel = new MessageModel();
                    msgModel.MessageTypeID = item.GLO_Message.MessageTypeID;
                    msgModel.MessageID = item.MessageID;
                    msgModel.ReceiveID = item.ReceiveID;
                    msgModel.ReceiveUserID = senduser.UserID.ToString();
                    msgModel.SendUserID = item.ReceiveUserID;
                    msgModel.SendUserIcon = senduser.Icon;
                    msgModel.SendUserNikeName = senduser.NickName;
                    msgModel.MessageTitle = item.GLO_Message.MessageTitle;
                    msgModel.MessageContent = item.GLO_Message.MessageContect;
                    msgModel.MessageStatus = item.Status;
                    result.Add(msgModel);
                }
            }
            else if (status == "1")
            {
                IList<GLO_MessageSend> reList = _messageservice.GetSendMessageList(userid);
                foreach (var item in reList)
                {
                    var senduser = _accountservice.GetUserByUserID(item.SendUserID);
                    MessageModel msgModel = new MessageModel();
                    msgModel.MessageTypeID = item.GLO_Message.MessageTypeID;
                    msgModel.MessageID = item.MessageID;
                    msgModel.ReceiveUserID = senduser.UserID.ToString();
                    msgModel.SendUserID = item.SendUserID;
                    msgModel.SendUserIcon = senduser.Icon;
                    msgModel.MessageTitle = item.GLO_Message.MessageTitle;
                    msgModel.MessageContent = item.GLO_Message.MessageContect;
                    msgModel.ReceiveUserNikeName = GetReceiveUserNickName(item.GLO_Message,1);
                    result.Add(msgModel);
                }
                ViewBag.Select = "Sent";
            }
            else if (status == "2")
            {
                IList<GLO_MessageArchive> reList = _messageservice.GetArchiveMessageList(userid);
                foreach (var item in reList)
                {
                    var senduser = _accountservice.GetUserByUserID(item.SendUserID);
                    MessageModel msgModel = new MessageModel();
                    msgModel.MessageTypeID = item.GLO_Message.MessageTypeID;
                    msgModel.MessageID = item.MessageID;
                    if (!string.IsNullOrEmpty(item.ReceiveUserID))
                    {
                        msgModel.ReceiveUserID = item.ReceiveUserID;
                    }
                    msgModel.SendUserID = senduser.UserID;
                    msgModel.SendUserIcon = senduser.Icon;
                    msgModel.MessageTitle = item.GLO_Message.MessageTitle;
                    msgModel.MessageContent = item.GLO_Message.MessageContect;
                    msgModel.ReceiveUserNikeName = GetReceiveUserNickName(item.GLO_Message,2);
                    result.Add(msgModel);
                }
                ViewBag.Select = "Archive";
            }
            else if (status == "3")
            {
                IList<GLO_MessageTrash> reList = _messageservice.GetTrashMessageList(userid);
                foreach (var item in reList)
                {
                    MessageModel msgModel = new MessageModel();
                    msgModel.MessageTypeID = item.GLO_Message.MessageTypeID;
                    msgModel.MessageID = item.MessageID;
                    if (!string.IsNullOrEmpty(item.ReceiveUserID))
                    {
                        msgModel.ReceiveUserID = item.ReceiveUserID;
                    }
                    if (item.MessageType == 1)
                    {
                        var user = _accountservice.GetUserByUserID(item.SendUserID);
                        msgModel.SendUserNikeName = user.NickName;
                        msgModel.SendUserID = user.UserID;
                        msgModel.SendUserIcon = user.Icon;
                    }
                    else
                    {
                        var user = _accountservice.GetUserByUserID(item.UserID);
                        msgModel.SendUserNikeName = user.NickName;
                        msgModel.SendUserID = user.UserID;
                        msgModel.SendUserIcon = user.Icon;
                    }
                    msgModel.MessageTitle = item.GLO_Message.MessageTitle;
                    msgModel.MessageContent = item.GLO_Message.MessageContect;
                    msgModel.ReceiveUserNikeName = GetReceiveUserNickName(item.GLO_Message,3);
                    result.Add(msgModel);
                }
                ViewBag.Select = "Trash";
            }
            ViewBag.MessageList = result;
        }
        #endregion

        #region orther method
        public string GetUserUrl(int userID,int userType)
        {
            string url = string.Empty;
            if (userType == 1)
            {
                url = "/leader/index?leaderid=" + userID;
            }
            else if (userType == 2)
            {
                url = "/expert/myvalue?expertid=" + userID;
            }
            else if (userType == 3)
            {
                url = "/company/companyindex?companyid=" + userID;
            }
            return url;
        }
        public string GetReceiveUserNickName(GLO_Message message, int listType)
        {
            string result = "";
            if (message.ReceiveUserType == 0)
            {
                result = "All User";
            }
            else if (message.ReceiveUserType == 1)
            {
                result = "All Leader";
            }
            else if (message.ReceiveUserType == 2)
            {
                result = "All Expert";
            }
            else if (message.ReceiveUserType == 3)
            {
                result = "All Company";
            }
            else
            {
                if (listType == 0)
                {
                    var receiveMessageList = message.GLO_MessageReceive;
                    foreach (var receiveMessage in receiveMessageList)
                    {
                        result += receiveMessage.ReceiveUser.NickName + ";";
                    }
                    if (receiveMessageList.Count == 1)
                    {
                        result = result.Substring(0, result.Length - 1);
                    }
                }
                else
                {
                    var userIDList = new List<string>();
                    if (listType == 1)
                    {
                        userIDList = message.GLO_MessageSend.ReceiveUserID.Split(';').ToList();
                    }
                    else if (listType == 2)
                    {
                        userIDList = message.GLO_MessageArchive.ReceiveUserID.Split(';').ToList();
                    }
                    else if (listType == 3)
                    {
                        userIDList = message.GLO_MessageTrash.ToList()[0].ReceiveUserID.Split(';').ToList();
                    }
                    if (userIDList.Count > 0)
                    {
                        foreach (var id in userIDList)
                        {
                            if (!string.IsNullOrEmpty(id))
                            {  
                                result += _accountservice.GetUserByUserID(int.Parse(id)).NickName + ";";
                            }
                        }
                        if (userIDList.Count == 2)
                        {
                            result = result.Substring(0, result.Length - 1);
                        }
                    }
                }
            }
            return result;
        }

        public string GetReceiveUserNickNameForDetail(GLO_Message message, int listType)
        {
            string result = "";
            if (message.ReceiveUserType == 0)
            {
                result = "All User";
            }
            else if (message.ReceiveUserType == 1)
            {
                result = "All Leader";
            }
            else if (message.ReceiveUserType == 2)
            {
                result = "All Expert";
            }
            else if (message.ReceiveUserType == 3)
            {
                result = "All Company";
            }
            else
            {
                var userIDList = new List<string>();
                if (listType == 1)
                {
                    userIDList = message.GLO_MessageSend.ReceiveUserID.Split(';').ToList();
                }
                else if (listType == 2)
                {
                    userIDList = message.GLO_MessageArchive.ReceiveUserID.Split(';').ToList();
                }
                else if (listType == 3)
                {
                    userIDList = message.GLO_MessageTrash.ToList()[0].ReceiveUserID.Split(';').ToList();
                }
                if (userIDList.Count > 0)
                {
                    foreach (var id in userIDList)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            var userUrl = string.Empty;
                            var user = _accountservice.GetUserByUserID(int.Parse(id));
                            if (user.TypeID == 1)
                            {
                                userUrl = "<a href=\"/leader/index?leaderid=" + user.UserID +"\" target=\"_blank\">"+user.NickName+"</a>";
                            }
                            else if (user.TypeID == 2)
                            {
                                userUrl = "<a href=\"/expert/myvalue?expertid=" + user.UserID + "\" target=\"_blank\">" + user.NickName + "</a>";
                            }
                            else if (user.TypeID == 3)
                            {
                                userUrl = "<a href=\"/company/companyindex?companyid=" + user.UserID + "\" target=\"_blank\">" + user.NickName + "</a>";
                            }
                            else
                            {
                                userUrl = user.NickName;
                            }
                            result += userUrl + ";";
                        }
                    }
                    if (userIDList.Count == 2)
                    {
                        result = result.Substring(0, result.Length - 1);
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
