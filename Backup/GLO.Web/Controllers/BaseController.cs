﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Data.Models;
using GLO.Web.Models;

namespace GLO.Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        { 
        }
        public LoginUserInfo LoginedUser
        {
            get
            {
                var user = HttpContext.User as CustomPrincipal<LoginUserInfo>;
                if (user != null)
                    return user.UserData;
                else
                    return null;
            }
            set
            {
                LoginedUser = value;
            }
        }
        protected string GetIP()
        {
            string ip = string.Empty;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"]))
                ip = Convert.ToString(System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);
            if (string.IsNullOrEmpty(ip))
                ip = Convert.ToString(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            return ip;
        }

    }

}
