﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GLO.Data;
using GLO.Data.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace GLO.Web.Controllers
{
    public static class ExpertHelper
    {
        public static int GetStartMinYear(IList<GLO_CareerSummary> carerrsummary, IList<GLO_Education> education, IList<GLO_Contribution> contribution)
        {
            int startyear = 0;
            if (carerrsummary != null && carerrsummary.Count > 0)
            {
                startyear = carerrsummary.OrderBy(x => x.StartDate).FirstOrDefault().StartDate.Value.Year;
            }
            if (education != null && education.Count > 0)
            {
                int eyear = education.OrderBy(x => x.StartDate).FirstOrDefault().StartDate.Value.Year;
                if (startyear == 0)
                {
                    startyear = eyear;
                }
                else if (startyear > eyear)
                {
                    startyear = eyear;
                }
            }
            if (contribution != null && contribution.Count > 0)
            {
                int cyear = contribution.OrderBy(x => x.StartDate).FirstOrDefault().StartDate.Value.Year;
                if (startyear == 0)
                {
                    startyear = cyear;
                }
                else if (startyear > cyear)
                {
                    startyear = cyear;
                }
            }
           // if (startyear == 0)
                startyear = DateTime.Now.Year-29;
            return startyear;
        }
        public static int GetEndMaxYear(IList<GLO_CareerSummary> carerrsummary, IList<GLO_Education> education, IList<GLO_Contribution> contribution)
        {
            int endyear = 0;
            if (carerrsummary != null && carerrsummary.Count > 0)
            {
                endyear = carerrsummary.OrderByDescending(x => x.EndDate).FirstOrDefault().EndDate.Value.Year;
            }
            if (education != null && education.Count > 0)
            {
                int eyear = education.OrderByDescending(x => x.EndDate).FirstOrDefault().EndDate.Value.Year;
                if (endyear == 0)
                {
                    endyear = eyear;
                }
                else if (endyear < eyear)
                {
                    endyear = eyear;
                }
            }
            if (contribution != null && contribution.Count > 0)
            {
                int cyear = contribution.OrderByDescending(x => x.EndDate).FirstOrDefault().EndDate.Value.Year;
                if (endyear == 0)
                {
                    endyear = cyear;
                }
                else if (endyear < cyear)
                {
                    endyear = cyear;
                }
            }
            if (endyear == 0)
                endyear = DateTime.Now.Year;
            return endyear;
        }
        public static void EMailSend(string tomail, string mailtitle, string mailcontent, string customfrom, string signature = "Peter Buytaert, CEO")
        {
            try
            {
                string fromMail = ConfigurationManager.AppSettings["EmailName"].ToString(); //"admin@glo-china.com";
                string passWord = ConfigurationManager.AppSettings["EmailPassword"].ToString(); //"adminglo08042013";
                string smtpAddress = "smtpout.asia.secureserver.net";

                //string fromMail = "121035288@qq.com";
                //string passWord = "51805413wsf";
                //string smtpAddress = "smtp.qq.com";
                if (customfrom == "")
                    customfrom = fromMail;
                MailMessage myMail = new MailMessage(string.Format("{0}<{1}>", signature, customfrom), tomail, mailtitle, mailcontent);

                myMail.SubjectEncoding = System.Text.Encoding.UTF8;
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                myMail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Host = smtpAddress;
                client.Credentials = new NetworkCredential(fromMail, passWord);
                client.EnableSsl = false;
                client.Send(myMail); //send email


                //System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                //client.Host = "smtp.qq.com"; //"smtp.163.com";//使用163的SMTP服务器发送邮件
                //client.UseDefaultCredentials = true;
                //client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //client.Credentials = new System.Net.NetworkCredential("93665403", "1111wan_song_bai!@#1");//163的SMTP服务器需要用163邮箱的用户名和密码作认证，如果没有需要去163申请个,
                ////这里假定你已经拥有了一个163邮箱的账户，用户名为abc，密码为*******
                //System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
                //Message.From = new System.Net.Mail.MailAddress("93665403@qq.com");//这里需要注意，163似乎有规定发信人的邮箱地址必须是163的，而且发信人的邮箱用户名必须和上面SMTP服务器认证时的用户名相同
                ////因为上面用的用户名abc作SMTP服务器认证，所以这里发信人的邮箱地址也应该写为abc@163.com
                ////Message.To.Add("123456@gmail.com");//将邮件发送给Gmail
                //Message.To.Add("jhonwsb@163.com");//将邮件发送给QQ邮箱
                //Message.Subject = "测试标体";
                //Message.Body = "测试邮件体";
                //Message.SubjectEncoding = System.Text.Encoding.UTF8;
                //Message.BodyEncoding = System.Text.Encoding.UTF8;
                //Message.Priority = System.Net.Mail.MailPriority.High;
                //Message.IsBodyHtml = true;
                //client.Send(Message);
            }
            catch
            {

            }
        }
    }
}