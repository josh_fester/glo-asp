﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data.Models;
using System.Web.Script.Serialization;

namespace GLO.Web.Controllers
{
    public class CompetenceController : BaseController
    {
        private ICompetenceService _competenceservice;
        private IExpertService _expertservice;
        private IAccountService _accountservice;
        private IMessageService _messageservice;
        public CompetenceController(ICompetenceService CompetenceService, IExpertService ExpertService, IAccountService AccountService, IMessageService MessageService)
        {
            _competenceservice = CompetenceService;
            _expertservice = ExpertService;
            _accountservice = AccountService;
            _messageservice = MessageService;
        }

        public ActionResult CareerSummaryAdd(string data, string type)
        {
            int userType = LoginedUser.UserType;
            if (type == "Education")
            {
                GLO_Education education = DeserializeXmlT<GLO_Education>(data);
                _competenceservice.EducationAdd(education);
               
                GLO_UserEx userEx = _accountservice.GetUserByUserID(education.UserID).GLO_UserEx;
                if (userEx.ShowExplain || !userEx.CompetenciesStatus)
                {
                    userEx.ShowExplain = false;
                    userEx.CompetenciesStatus = true;
                    _accountservice.UpdateUserEx(userEx);
                }
                if (userType == 1)
                {
                    return CheckLeaderStatus(userEx);
                }
                else if (userType == 2)
                {
                    return CheckExpertStatus(userEx);
                }
            }
            else if (type == "Employment")
            {
                GLO_CareerSummary carerrsummary = DeserializeXmlT<GLO_CareerSummary>(data);
                _competenceservice.CarerrSummaryAdd(carerrsummary);
                GLO_UserEx userEx = _accountservice.GetUserByUserID(carerrsummary.UserID).GLO_UserEx;
                if (userEx.ShowExplain || !userEx.CompetenciesStatus)
                {
                    userEx.ShowExplain = false;
                    userEx.CompetenciesStatus = true;
                    _accountservice.UpdateUserEx(userEx);
                }
                if (userType == 1)
                {
                    return CheckLeaderStatus(userEx);
                }
                else if (userType == 2)
                {
                    return CheckExpertStatus(userEx);
                }
            }
            else
            {
                GLO_Contribution contribution = DeserializeXmlT<GLO_Contribution>(data);
                _competenceservice.ContributionAdd(contribution);
                GLO_UserEx userEx = _accountservice.GetUserByUserID(contribution.UserID).GLO_UserEx;
                if (userEx.ShowExplain || !userEx.CompetenciesStatus)
                {
                    userEx.ShowExplain = false;
                    userEx.CompetenciesStatus = true;
                    _accountservice.UpdateUserEx(userEx);
                }
                if (userType == 1)
                {
                    return CheckLeaderStatus(userEx);
                }
                else if (userType == 2)
                {
                    return CheckExpertStatus(userEx);
                }
            }
            if (userType == 1)
            {
                return Json("/Leader/MyCompetence?leaderid=" + LoginedUser.UserID, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("/Expert/MyCompetence?expertid=" + LoginedUser.UserID, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _MyCompetenceAdd(int expertid)
        {
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Competence/MyCompetenceAdd.ascx")
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _MyCompetenceUpdate(int recordid, string type)
        {

            var viewdata = new ViewDataDictionary();

            if (type == "Contribution")
            {
                var contribution = _competenceservice.GetContributionByID(recordid);
                //if (contribution.EndDate == null)
                //    contribution.EndDate = DateTime.Now;
                viewdata.Add("Type", "Contribution");
                viewdata.Add("Contribution", contribution);
            }
            else if (type == "Education")
            {
                var education = _competenceservice.GetEducationByID(recordid);
                //if (education.EndDate == null)
                //    education.EndDate = DateTime.Now;
                viewdata.Add("Type", "Education");
                viewdata.Add("Education", education);
            }
            else if (type == "Employment")
            {
                var career = _competenceservice.GetCareerByID(recordid);
                //if (career.EndDate == null)
                //    career.EndDate = DateTime.Now;
                viewdata.Add("Type", "Employment");
                viewdata.Add("Employment", career);
            }


            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Competence/MyCompetenceEdit.ascx", viewdata)
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult CareerSummaryEdit(string jsondata, string type)
        {
            if (type == "Education")
            {
                GLO_Education education = DeserializeXmlT<GLO_Education>(jsondata);

                _competenceservice.EducationEidt(education);
            }
            else if (type == "Employment")
            {
                GLO_CareerSummary carerrsummary = DeserializeXmlT<GLO_CareerSummary>(jsondata);
                _competenceservice.CareerSummaryEdit(carerrsummary);
            }
            else
            {
                GLO_Contribution contribution = DeserializeXmlT<GLO_Contribution>(jsondata);
                _competenceservice.ContributionEdit(contribution);
            }
            return Json(true);
        }
        public JsonResult CompetenceDelete(int recordid, string type)
        {
            if (type == "Employment")
                _competenceservice.CareerSummaryDel(recordid);
            else if (type == "Contribution")
                _competenceservice.ContributionDel(recordid);
            else if (type == "Education")
                _competenceservice.EducationDel(recordid);
            return Json(true);
        }
        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }

        #region Custom Competecncy
        public JsonResult AddCompetency(int userID,string value,int type)
        {
            Guid resultId = Guid.Empty;
            GLO_Competencies newCompetency = new GLO_Competencies();
            newCompetency.CompetencyID = Guid.NewGuid();
            newCompetency.UserID = userID;
            newCompetency.CompetencyContent = value;
            newCompetency.CompetencyType = type;
            newCompetency.CreateTime = DateTime.Now;
            resultId = _competenceservice.CompetencyAdd(newCompetency);
            return Json(resultId);
        }
        public JsonResult EditCompetency(string competencyID, string competencyContent , bool isFirstSave)
        {
            try
            {
                string[] idArray = competencyID.Split(';');
                string[] contentArray = competencyContent.Split(';');
                for (int i = 0; i < idArray.Length-1; i++)
                {
                    GLO_Competencies editCompetency = new GLO_Competencies();
                    editCompetency.CompetencyID = new Guid(idArray[i]);
                    editCompetency.CompetencyContent = contentArray[i];
                    _competenceservice.CompetencyEdit(editCompetency);
                }
                if (!isFirstSave)
                {
                    _messageservice.SendRecommendationsEmail(LoginedUser.UserID);
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteCompetency(Guid competencyID)
        {
            try
            {
                _competenceservice.CompetencyDelete(competencyID);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _CompetencyEdit(int userID)
        {
            List<GLO_Competencies> resultList = _competenceservice.GetCompetenciesList(userID).Where(x => x.CompetencyType == 1).OrderBy(x => x.OrderID).ToList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Competence/TopCompetencyEdit.ascx", resultList)
            };
            return Json(result, JsonRequestBehavior.AllowGet);             
        }
        [HttpPost]
        public JsonResult GetOtherCompetencyList(int userID)
        {
            List<GLO_Competencies> aList = _competenceservice.GetOrhterCompetenciesList(userID).ToList();
            var resultList = new SelectList(aList, "CompetencyID", "CompetencyContent", null);
            return Json(resultList);
        }
        #endregion

        public JsonResult CheckExpertStatus(GLO_UserEx userEx)
        {
            if (!userEx.CompetenciesStatus)
            {
                return Json("/Expert/MyCompetence?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.BasicInfoStatus)
            {
                return Json("/Expert/MyValue?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.ValueStatus)
            {
                return Json("/Expert/MyValue?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.CredentialsStatus)
            {
                return Json("/Expert/MyCredential?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.RegisterSuccessStatus)
            {
                return Json("/Expert/RegisterSuccess?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("/Expert/MyCompetence?expertid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckLeaderStatus(GLO_UserEx userEx)
        {
            if (!userEx.CompetenciesStatus)
            {
                return Json("/Leader/MyCompetence?leaderid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.BasicInfoStatus)
            {
                return Json("/Leader/Index?leaderid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.InterViewStatus)
            {
                return Json("/Leader/Index?leaderid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.RegisterSuccessStatus)
            {
                return Json("/Leader/RegisterSuccess?leaderid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("/Leader/MyCompetence?leaderid=" + userEx.UserID, JsonRequestBehavior.AllowGet);
        }
    }
}
