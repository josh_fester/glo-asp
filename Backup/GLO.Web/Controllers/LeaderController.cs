﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data;
using System.Dynamic;
using System.Web.Script.Serialization;
using GLO.Data.Models;
using GLO.Web.Models;
using System.IO;
using GLO.Web.Common;
using System.Web.Security;
using System.Text;
using System.Configuration;

namespace GLO.Web.Controllers
{

    [Authorize]
    [IsManagerAttribute]
    public partial class LeaderController : BaseController
    {
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();

        #region service
        private ILeaderService _leaderservice;
        private IAccountService _accountservice;
        private ITagUserService _taguserservice;
        private IMessageService _messageservice;
        private ICompetenceService _competenceservice;
        private IExpertService _expertservice;
        private ICompanyService _companyservice;
        private IPolicyService _policyservice;
        public LeaderController(ILeaderService LeaderService, IPolicyService PolicyService, IAccountService AccountService, ITagUserService TagUserService, IMessageService MessageService, ICompetenceService CompetenceService, IExpertService ExpertService, ICompanyService CompanyService)
        {
            _leaderservice = LeaderService;
            _policyservice = PolicyService;
            _accountservice = AccountService;
            _taguserservice = TagUserService;
            _messageservice = MessageService;
            _competenceservice = CompetenceService;
            _expertservice = ExpertService;
            _companyservice = CompanyService;
        }
        #endregion

        #region Check Method
        public JsonResult CheckExpertStatus(GLO_UserEx userEx)
        {
            if (!userEx.BasicInfoStatus)
            {
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.InterViewStatus)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.CompetenciesStatus)
            {
                return Json("4", JsonRequestBehavior.AllowGet);
            }
            else if (!userEx.RegisterSuccessStatus)
            {
                return Json("5", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("0", JsonRequestBehavior.AllowGet);
        }
        public void CheckTagButton(int userID, int UserType)
        {
            bool showTagButton = true;
            if (LoginedUser != null)
            {
                foreach (var taggeduser in _taguserservice.GetTaggedUserList(LoginedUser.UserID))
                {
                    if (taggeduser.TaggedUserID == userID)
                    {
                        showTagButton = false;
                    }
                }
                if (UserType == LoginedUser.UserType || LoginedUser.UserType == 4)
                {
                    showTagButton = false;
                }
            }
            ViewBag.ShowTagButton = showTagButton;
        }

        public void CheckRequestTagButton(int userID, int UserType)
        {
            bool showButton = true;
            if (LoginedUser != null)
            {
                foreach (var taggeduser in _taguserservice.GetTaggedUserList(userID))
                {
                    if (taggeduser.TaggedUserID == LoginedUser.UserID)
                    {
                        showButton = false;
                    }
                }
                if (UserType == LoginedUser.UserType || LoginedUser.UserType == 4)
                {
                    showButton = false;
                }
            }
            ViewBag.ShowRequestTagButton = showButton;
        }
        #endregion

        public ActionResult ReceiveEmail(int leaderid)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            ViewBag.Select = "ReceiveEmail";
            return View(user);
        }

        public JsonResult ReceiveEmailChange(int leaderid, bool ReceiveEmail)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            user.ReceiveEmail = ReceiveEmail;
            _accountservice.UserUpdate(user);
            return Json(true);
        }
        [AllowAnonymous]
        public ActionResult Index(int leaderid)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            ViewBag.GLO_ExpertRelational = user.GLO_ExpertRelational.ToList();
            CheckTagButton(user.UserID, user.TypeID);
            CheckRequestTagButton(user.UserID, user.TypeID);
            return View(user);
        }
        public ActionResult MyCompetence(int leaderid)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            Competence comp = new Competence();
            comp.CarreerSummaryList =  user.GLO_Leader.GLO_CareerSummary.OrderBy(x=>x.StartDate).ToList();
            comp.ContributionList = user.GLO_Leader.GLO_Contribution.OrderBy(x => x.StartDate).ToList();
            comp.EducationList = user.GLO_Leader.GLO_Education.OrderBy(x => x.StartDate).ToList();
            foreach (var career in comp.CarreerSummaryList)
            {
                if (career.EndDate == null)
                    career.EndDate = DateTime.Now;
            }
            foreach (var edu in comp.EducationList)
            {
                if (edu.EndDate == null)
                    edu.EndDate = DateTime.Now;
            }
            foreach (var con in comp.ContributionList)
            {
                if (con.EndDate == null)
                    con.EndDate = DateTime.Now;
            }
            comp.StartYear = ExpertHelper.GetStartMinYear(comp.CarreerSummaryList, comp.EducationList, comp.ContributionList);
            comp.EndYear = ExpertHelper.GetEndMaxYear(comp.CarreerSummaryList, comp.EducationList, comp.ContributionList);
            user.GLO_Leader.ExpertCompetence = comp;
            CheckTagButton(user.UserID, user.TypeID);
            CheckRequestTagButton(user.UserID, user.TypeID);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult MyRecommendation(int leaderid)
        {
            var user = _accountservice.GetUserByUserID(leaderid);
            CheckTagButton(user.UserID, user.TypeID);
            CheckRequestTagButton(user.UserID, user.TypeID);
            string StrHtml = _policyservice.GetByID(13).PolicyContent;
            string StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrHtml, "<[^>]+>", "");
            StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrNohtml, "&[^;]+;", " ");
            StrNohtml = StrNohtml.Replace("\t", "");
            ViewBag.InviteMessage = StrNohtml; 
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult Assessment(int leaderid)
        {
            ViewBag.Select = "Assessment";
            var user = _accountservice.GetUserByUserID(leaderid);
            return View(user);
        }
        public JsonResult DeleteAssessment()
        {
            var leader = _accountservice.GetUserByUserID(LoginedUser.UserID).GLO_Leader;
            leader.Assessment ="";
            _leaderservice.LeaderUpdate(leader);
            return Json(true);
        }
        [CheckApprovedAttribute]
        public ActionResult DashBoard(int leaderid)
        {
            ViewBag.Select = "CompanyTag";
            var user = _accountservice.GetUserByUserID(leaderid);
            if (!user.GLO_UserEx.BasicInfoStatus)
            {
                return RedirectToAction("Index", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.InterViewStatus)
            {
                return RedirectToAction("Index", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.CompetenciesStatus)
            {
                return RedirectToAction("MyCompetence", new { leaderid = leaderid });
            }
            else if (!user.GLO_UserEx.RegisterSuccessStatus)
            {
                return RedirectToAction("RegisterSuccess", new { leaderid = leaderid });
            }
            else
                return RedirectToAction("LeaderTag", "Tag", new { userID = leaderid, type = 3 });
        }
        public ActionResult JobTag(int leaderid)
        {
            ViewBag.Select ="JobTag";
            var user = _accountservice.GetUserByUserID(leaderid);
            return View(user);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LeaderProfileUpdate(string leaderdata, string ispublish, int LocationID, int industryID, bool HideProfile)
        {
            GLO_Leader lead = DeserializeXmlT<GLO_Leader>(leaderdata);
            GLO_Users user = _accountservice.GetUserByUserID(lead.UserID);
            GLO_Leader leader = user.GLO_Leader;
            leader.NickName = lead.NickName;
            leader.Location = lead.Location;
            leader.Version = lead.Version;
            leader.Title = lead.Title;
            leader.Industry = lead.Industry;
            if (lead.LogoUrl != leader.LogoUrl && leader.LogoUrl != "user_none.gif")
            {
                DeleteFile("~/Images/" + leader.LogoUrl + "");
            }
            leader.LogoUrl = lead.LogoUrl;
            leader.LeaderValue1 = lead.LeaderValue1;
            leader.LeaderValue2 = lead.LeaderValue2;
            leader.LeaderValue3 = lead.LeaderValue3;
            _leaderservice.LeaderUpdate(leader);

            bool isFirstSave = true;
            string[] valueContent = new string[] { leader.LeaderValue1, leader.LeaderValue2, leader.LeaderValue3 };
            var valuelist =  _competenceservice.GetCompetenciesList(leader.UserID).Where(x=>x.CompetencyType==3).OrderBy(x=>x.OrderID);
            
            int count = 0;
            foreach (var v in valuelist)
            {
                if (v.CompetencyContent != valueContent[count])
                {
                    if (v.CompetencyContent != "")
                        isFirstSave = false;
                    else if (leader.GLO_Users.GLO_Recommendation.Count > 0)
                        isFirstSave = false;
                    GLO_Competencies editCompetency = new GLO_Competencies();
                    editCompetency.CompetencyID = v.CompetencyID;
                    editCompetency.CompetencyContent = valueContent[count];
                    _competenceservice.CompetencyEdit(editCompetency);
                }
                count++;
            }
            if (!isFirstSave && leader.GLO_Users.GLO_Recommendation.Count>0)
            {
                _messageservice.SendRecommendationsEmail(LoginedUser.UserID);
            }

            #region Resume Management
            //var oldResumes = user.GLO_Resumes.ToList();
            //List<string> deleteResumesList = new List<string>();
            //if (!string.IsNullOrEmpty(delResumes))
            //{
            //    deleteResumesList = delResumes.Split(';').ToList();
            //}
            //if (!string.IsNullOrEmpty(resumes))
            //{
            //    var resumeList = resumes.Split(';').ToList();
            //    foreach (var oldresume in oldResumes)
            //    {
            //        var isDelete = true;
            //        foreach (var newresume in resumeList)
            //        {
            //            if (newresume == oldresume.ResumeName)
            //            {
            //                isDelete = false;
            //            }
            //        }
            //        if (isDelete)
            //        {
            //            deleteResumesList.Add(oldresume.ResumeName);
            //        }
            //    }
            //    _leaderservice.AddResumes(user.UserID, resumeList);
            //}
            //else
            //{
            //    foreach (var resume in oldResumes)
            //    {
            //        deleteResumesList.Add(resume.ResumeName);
            //    }
            //    _leaderservice.AddResumes(user.UserID, null);
            //}
            //string filePath = Server.MapPath("~/Upload/" + user.UserID + "/");
            //foreach (var file in deleteResumesList)
            //{
            //    if (System.IO.File.Exists(filePath+file))
            //    {
            //        System.IO.File.Delete(filePath + file);
            //    }
            //}
            #endregion

            GLO_UserEx userEx = user.GLO_UserEx;
            if (userEx.ShowExplain || !userEx.BasicInfoStatus)
            {
                userEx.ShowExplain = false;
                userEx.BasicInfoStatus = true;
                _accountservice.UpdateUserEx(userEx);
            }

            user.NickName = leader.NickName;
            user.Icon = leader.LogoUrl;
            user.IsPublish = ispublish == "0" ? false : true;
            user.HideProfile = HideProfile;
            if (leader.GLO_LeaderKeyWord.Count > 0)
            {
                user.IsPublish = false;
            }
            _accountservice.UserUpdate(user);

            if (LocationID != 0)
            {
                GLO_UserLocation newData = new GLO_UserLocation();
                newData.UserID = LoginedUser.UserID;
                newData.LID = LocationID;
                _accountservice.UpdateLocation(newData);
            }
            if (industryID != 0)
            {
                GLO_UserIndustry newData = new GLO_UserIndustry();
                newData.UserID = LoginedUser.UserID;
                newData.IndustryID = industryID;
                _accountservice.UpdateIndustry(newData);
            }

            CookieHelper.SetCookie(user);

            return CheckExpertStatus(userEx);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LeaderProfileHide(int UserID, bool HideProfile,bool ProfileSatus)
        {
            var user = _accountservice.GetUserByUserID(UserID);
            user.HideProfile = HideProfile;
            if (HideProfile == true && ProfileSatus == false)
            user.IsPublish = ProfileSatus;
            _accountservice.UserUpdate(user);
            return Json(true); 
        }
        public ActionResult LeaderInterviewEdit(string questionID,string questions,string answers)
        {
            List<string> questionIDList = questionID.Split(';').ToList();
            List<string> questionTitleList = questions.Split(';').ToList();
            List<string> answersList = answers.Split(';').ToList();
            List<GLO_LeaderInterviewAnswer> newList = new List<GLO_LeaderInterviewAnswer>();
            for (int i=0;i<questionIDList.Count-1;i++)
            {
                if (!string.IsNullOrEmpty(questionIDList[i]))
                {
                    GLO_LeaderInterviewAnswer interviewAnswer = new GLO_LeaderInterviewAnswer();
                    interviewAnswer.AnswerID = Guid.NewGuid();
                    interviewAnswer.UserID = LoginedUser.UserID;
                    interviewAnswer.QuestionID = new Guid(questionIDList[i]);
                    interviewAnswer.Question = questionTitleList[i];
                    interviewAnswer.Answer = answersList[i];
                    newList.Add(interviewAnswer);
                }
            }
            _leaderservice.AddInterview(LoginedUser.UserID,newList);

            var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
            GLO_UserEx userEx = user.GLO_UserEx;
            if (userEx.ShowExplain || !userEx.InterViewStatus)
            {
                userEx.ShowExplain = false;
                userEx.InterViewStatus = true;
                _accountservice.UpdateUserEx(userEx);
            }
            return CheckExpertStatus(userEx);
        }
        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }
        public PartialViewResult _MyInterView(int leaderid)
        {
            ViewBag.QuestionList = _leaderservice.GetInterviewQuestion();
            GLO_Users leader = _accountservice.GetUserByUserID(leaderid);
            return PartialView("/Views/Shared/Leader/MyInterviewEdit.ascx", leader.GLO_Leader);
        }
        
        public JsonResult _SubmitResume(int jobID)
        {
            GLO_CompanyJob companyjob = _companyservice.GetCompanyJobByID(jobID);
            ResumeModel resume = new ResumeModel();
            resume.UserID = LoginedUser.UserID;
            resume.JobID = jobID;
            resume.JobNO = companyjob.JobNO;
            resume.JobTitle = companyjob.JobTitle;
            resume.CompanyID = companyjob.UserID;
            resume.ResumeList = _accountservice.GetUserByUserID(LoginedUser.UserID).GLO_Resumes.ToList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/SumbitResume.ascx", resume)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SubmitResume(string companyid, string jobid,string jobtitle,int userid,string resumename,string comments)
        {
            GLO_Message message = new GLO_Message();
            var job = _companyservice.GetCompanyJobByID(int.Parse(jobid));
            message.MessageTitle = string.Format("Position applied for: {0} (Job ID={1})", jobtitle,job.JobNO);
            StringBuilder resumecontent = new StringBuilder();           
            string[] resumelist = resumename.Replace("\n","").Trim().Split(';');
            foreach (var resume in resumelist)
            {
                if (!string.IsNullOrEmpty(resume))
                    resumecontent.AppendFormat("<a href='/Upload/{0}/{1}'>{2}</a><br/>", userid, resume.Trim(), resume.Trim()); 
            }
           
            message.MessageContect = string.Format("View the resume: {0} <br/>Comments:<br/>{1}", resumecontent,comments);
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            _messageservice.MessageCreate(message, LoginedUser.UserID, companyid);

            var leader = _accountservice.GetUserByUserID(userid);
            var tagjob = leader.GLO_Leader.GLO_JobTag.FirstOrDefault(x => x.UserID == userid && x.JobID == int.Parse(jobid));
            if (tagjob != null)
            {              
                _leaderservice.UpdateTagJob(tagjob.JobTagID);
            }
            if (resumelist.Length == 2 && leader.GLO_Resumes.FirstOrDefault() == null)
            {
                 string fileName = resumelist[0].Trim() ;
                 Guid saveID = _leaderservice.AddResume(userid, fileName);
                
            }
            return Json(true);
        }
        public JsonResult TagJob(int jobid)
        {
            var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
            if (user.GLO_Leader.GLO_JobTag.FirstOrDefault(x => x.JobID == jobid && x.UserID == LoginedUser.UserID) != null)
            {
                return Json(false);
            }
            else
            {
                GLO_JobTag job = new GLO_JobTag();
                job.UserID = LoginedUser.UserID;
                job.JobID = jobid;
                _leaderservice.TagJob(job);
                return Json(true);
            }
        }
        public JsonResult RemoveTagJob(int jobid)
        {
            var tagjob = _leaderservice.GetJobTagByID(jobid, LoginedUser.UserID);
            _leaderservice.RemoveTagJob(tagjob.JobTagID);
            return Json(true);
        }
        public JsonResult _EidtLeftInfo(int leaderid)
        {
            GLO_Users leader = _accountservice.GetUserByUserID(leaderid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/ProfileLeftEdit.ascx", leader.GLO_Leader)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _EidtRecommendation(int leaderid)
        {
            GLO_Users expert = _accountservice.GetUserByUserID(leaderid);
            foreach (var recommend in expert.GLO_Recommendation)
            {
                recommend.LogoUrl = recommend.GLO_RecommendUsers.Icon;// _accountservice.GetUserByUserID(recommend.RecommendUserID).Icon;
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/RecommendationEdit.ascx", expert.GLO_Leader)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult _MyCompetenceUpdate(int recordid, string type)
        {
            
            var viewdata = new ViewDataDictionary();
            
            if (type == "Contribution")
            {
                var contribution = _competenceservice.GetContributionByID(recordid);
                viewdata.Add("Type", "Contribution");
                viewdata.Add("Contribution", contribution);
            }
            else if (type == "Education")
            {
                var education = _competenceservice.GetEducationByID(recordid);
                viewdata.Add("Type", "Education");
                viewdata.Add("Education", education);
            }
            else if (type == "Employment")
            {
                var career = _competenceservice.GetCareerByID(recordid);
                viewdata.Add("Type", "Employment");
                viewdata.Add("Employment", career);
            }
                

            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/MyCompetenceEdit.ascx", viewdata)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }

        public JsonResult CareerSummaryEdit(string jsondata, string type)
        {
            if (type == "Education")
            {
                GLO_Education education = DeserializeXmlT<GLO_Education>(jsondata);
                _competenceservice.EducationEidt(education);
            }
            else if (type == "Employment")
            {
                GLO_CareerSummary carerrsummary = DeserializeXmlT<GLO_CareerSummary>(jsondata);
                _competenceservice.CareerSummaryEdit(carerrsummary);
            }
            else
            {
                GLO_Contribution contribution = DeserializeXmlT<GLO_Contribution>(jsondata);
                _competenceservice.ContributionEdit(contribution);
            }
            return Json(true);
        }

        public void DeleteFile(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                try
                {
                    //file path
                    string _filePath = Server.MapPath(filePath);
                    if (System.IO.File.Exists(_filePath))
                    {
                        System.IO.File.Delete(_filePath);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public ActionResult RegisterSuccess(int leaderid)
        {
            GLO_Users user = _accountservice.GetUserByUserID(leaderid);
            GLO_UserEx userEx = user.GLO_UserEx;
            userEx.RegisterSuccessStatus = true;
            _accountservice.UpdateUserEx(userEx);
            string Title = "Please approve this registered leader";
            string Content = string.Format("A leader is registered to GLO and finished profile pages, please click <a href=\"{0}/admin\" target=\"_blank\">here</a> to approve the leader.", domain);
            string ToMail= ConfigurationManager.AppSettings["EmailName"].ToString();
            ExpertHelper.EMailSend(ToMail, Title, Content, "");
            return View(user);
        }

        public ActionResult TakeGLOWisdom()
        {
            ViewBag.Survey = ConfigurationManager.AppSettings["SurveyLink"].ToString();
            return View();
        }

        #region ChangePassword
        public ActionResult ChangePassword()
        {
            ViewBag.Select = "Password";
            ChangePasswordModel aModel = new ChangePasswordModel();
            aModel.UserID = LoginedUser.UserID;
            ViewBag.Message = "";
            return View(aModel);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel aModel)
        {
            try
            {
                var user = _accountservice.GetUserByUserID(aModel.UserID);
                if (user.PassWord != DEEncrypt.Encrypt(aModel.OldPassword))
                {
                    ModelState.AddModelError("OldPassword", "The old password is wrong.");
                }
                if (ModelState.IsValid)
                {
                    user.PassWord = DEEncrypt.Encrypt(aModel.NewPassword);
                    _accountservice.ChangePassWord(user);
                    FormsAuthentication.SignOut();
                    ViewBag.Message = "Your password was successfully changed！";
                }
                else
                {
                    ViewBag.Message = "";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(aModel);
        }
        #endregion

        #region My Recommend List
        public ActionResult MyRecommendList()
        {
            ViewBag.Select = "MyRecommendList";
            var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
            List<GLO_Users> recommendThemList = new List<GLO_Users>();
            foreach (var item in user.GLO_Recommendation1)
            {
                recommendThemList.Add(item.GLO_Users);
            }
            List<GLO_Users> recommendMeList = new List<GLO_Users>();
            foreach (var item in user.GLO_Recommendation)
            {
                recommendMeList.Add(item.GLO_RecommendUsers);
            }
            ViewBag.RecommendThemList = recommendThemList;
            ViewBag.RecommendMeList = recommendMeList;
            return View(user);
        }
        #endregion

        #region Not activate
        public JsonResult _NotActivateMessage()
        {
            var user = _accountservice.GetUserByUserID(LoginedUser.UserID).GLO_Leader;
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Leader/NotActivate.ascx", user)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region Key word
        public JsonResult AddKeyWord(string value)
        {
            Guid resultId = Guid.Empty;
            GLO_LeaderKeyWord keyWord = _leaderservice.GetKeyWordByContent(LoginedUser.UserID,value);
            if (keyWord != null)
            {
                resultId = keyWord.KeyWordID;
            }
            else
            {
                GLO_LeaderKeyWord newData = new GLO_LeaderKeyWord();
                newData.KeyWordID = Guid.NewGuid();
                newData.KeyWordContent = value;
                newData.UserID = LoginedUser.UserID;
                _leaderservice.AddKeyWord(newData);
                resultId = newData.KeyWordID;
            }
            return Json(resultId);
        }
        public JsonResult DeleteKeyWord(Guid id)
        {
            _leaderservice.DeleteKeyWord(id);
            return Json(true);
        }
        [HttpPost]
        public JsonResult GetCompanyList()
        {
            List<GLO_Company> aList = _companyservice.GetCompanyList();
            var resultList = new SelectList(aList, "UserID", "CompanyName", null);
            return Json(resultList);
        }
        #endregion

        #region Resume Management
        public JsonResult DeleteResume(string fileName, string resumeID)
        {
            if (!string.IsNullOrEmpty(fileName) || !string.IsNullOrEmpty(resumeID))
            {
                try
                {
                    _leaderservice.DeleteResume(new Guid(resumeID));
                    string filePath = Server.MapPath("~/Upload/" + LoginedUser.UserID + "/" + fileName);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    return Json(true);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }
            }
            return Json(false);
        }
        #endregion
    }

}
