﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/


CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:

    config.language = 'en';
    config.skin = 'kama';
    config.uiColor = '#d3d3d3';

    config.fullPage = false;
    config.resize_enabled = false;
    config.ignoreEmptyParagraph = true;
    config.pasteFromWordIgnoreFontFace = true;
    config.pasteFromWordKeepsStructure = false;
    config.pasteFromWordRemoveStyle = true;
    config.forcePasteAsPlainText = true;
    config.forceSimpleAmpersand = false;
    config.autoUpdateElement = true;
    config.entities = false;
    config.disableObjectResizing = true;
    config.startupFocus = true;

    config.tabSpaces = 4;
    config.height = 260;
    config.baseFloatZIndex = 10000;
    config.enterMode = CKEDITOR.ENTER_P;
    config.shinftEnterMode = CKEDITOR.ENTER_BR;

    config.removePlugins = 'elementspath';

    config.filebrowserWindowWidth = '800';
    config.filebrowserWindowHeight = '500'; 
    config.filebrowserBrowseUrl = '/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/ckeditor/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = '/ckeditor/ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = '/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = '/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';    

    config.toolbar_Basic1 = [
        ['Font', 'TextColor', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', 'Link', 'Unlink', 'HorizontalRule'],
        ['Undo', 'Redo','Source']
    ];
    config.toolbar_Basic2 = [
        ['Font', 'TextColor', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', 'Link', 'Unlink', 'HorizontalRule', 'Image'],
        ['Undo', 'Redo', 'Source']
    ];
};
