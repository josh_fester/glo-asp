﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Security.Principal;
using GLO.Web.Controllers;

namespace GLO.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.RouteExistingFiles = false;

            routes.MapRoute(
                name: "Insight",
                url: "{controller}/{action}/{postid}",
                defaults: new { controller = "Home", action = "Index", postid = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //  routes.MapRoute(
            //    name: "Leader",
            //    url: "{controller}/{action}/{leaderid}",
            //    defaults: new { controller = "Leader", action = "Index", leaderid = UrlParameter.Optional }
            //);

        }

        protected void Application_Start()
        {
            //for structure map
            Bootstrapper.ConfigureStructureMap();
            ControllerBuilder.Current.SetControllerFactory(
                new GLO.Web.Common.StructureMapControllerFactory()
            );


            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            BundleTable.Bundles.RegisterTemplateBundles();
        }
        public MvcApplication()
        {
            AuthorizeRequest += new EventHandler(MvcApplication_AuthorizeRequest);
        }
        void MvcApplication_AuthorizeRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            CustomPrincipal<LoginUserInfo>.TrySetUserInfo(app.Context);	
        }
    }
}