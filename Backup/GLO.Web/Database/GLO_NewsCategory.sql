CREATE TABLE GLO_NewsCategory ( 
	CategoryID int NOT NULL,
	CategoryName varchar(50) NOT NULL
)
;

ALTER TABLE GLO_NewsCategory ADD CONSTRAINT PK_GLO_NewsCategory 
	PRIMARY KEY CLUSTERED (CategoryID)
;



