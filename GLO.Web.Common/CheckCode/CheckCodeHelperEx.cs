﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace System.Web.Mvc
{
    public static class CheckCodeSettings
    {
        public static string DefaultImageId = "imgCheckCode";

        public static string DefaultImageAlt = "Check Code Img";

        public static string DefaultChangeImageId = "changeImg";

        public static string DefaultNewActionName = "New";

        public static string DefaultVerifyActionName = "Verify";

        public static string DefaultControllerName = "CheckCode";

        public static string DefaultCheckCodeInputControlId = "CheckCode";
    }

    public static class CheckCodeImageHelperEx
    {
        public static MvcHtmlString CheckCodeImage( this HtmlHelper aHtmlHelper, string aImageId, string aAlt
            , string aCssClass, IDictionary<string, object> aHtmlAttributes )
        {
            UrlHelper urlHelper=new UrlHelper(aHtmlHelper.ViewContext.RequestContext);
            string imgSrc = urlHelper.Action( CheckCodeSettings.DefaultNewActionName, CheckCodeSettings.DefaultControllerName, new { area="" } );
            return aHtmlHelper.Image( aImageId, imgSrc, aAlt, aCssClass, aHtmlAttributes );
        }

        public static MvcHtmlString CheckCodeImage( this HtmlHelper aHtmlHelper, string aImageId, string aAlt
            , string aCssClass, object aHtmlAttributes )
        {
            return CheckCodeImage( aHtmlHelper, aImageId, aAlt, aCssClass, new RouteValueDictionary( aHtmlAttributes ) );
        }

        public static MvcHtmlString CheckCodeImage( this HtmlHelper aHtmlHelper )
        {
            return CheckCodeImage( aHtmlHelper, CheckCodeSettings.DefaultImageId, 
                CheckCodeSettings.DefaultImageAlt, null,  (object) null );
        }

    }
}
