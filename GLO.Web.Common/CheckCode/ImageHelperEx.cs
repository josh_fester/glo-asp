﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace System.Web.Mvc
{
    public static class ImageHelperEx
    {
        public static MvcHtmlString Image( this HtmlHelper aHtmlHelper, string aImageId, string aSrc
            , string aTitle, string aCssClass, IDictionary<string, object> aHtmlAttributes )
        {
            var builder = new TagBuilder( "img" );

            builder.GenerateId( aImageId );

            UrlHelper urlHelper = new UrlHelper( aHtmlHelper.ViewContext.RequestContext );
            builder.MergeAttribute( "src", urlHelper.Content( aSrc ) );
            builder.MergeAttribute( "title", aTitle );
            builder.AddCssClass( aCssClass );
            builder.MergeAttributes( aHtmlAttributes );

            return MvcHtmlString.Create( builder.ToString( TagRenderMode.SelfClosing ) );
        }

        public static MvcHtmlString Image( this HtmlHelper aHtmlHelper, string aImageId, string aSrc
            , string aTitle, string aCssClass, object aHtmlAttributes )
        {
            return Image( aHtmlHelper, aImageId, aSrc, aTitle, aCssClass, new RouteValueDictionary( aHtmlAttributes ) );
        }
    }

}
