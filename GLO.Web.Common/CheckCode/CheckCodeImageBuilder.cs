﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace System.Web.Mvc
{
    public class CheckCodeImageBuilder
    {
        #region fields
        private const string _fillChar = "WWWWWWWWWWWWWWW";
        private string _CheckCode;
        private int _NoiseLineNumber = 25;
        private Color _NoiseLineColor = Color.Gray;
        private bool _NoiseLineRandomColor = true;
        private bool _NoiseLineRandomWidth = true;
        private int _NoisePixelNumber = 100;
        private Color _NoisePixelColor = Color.White;
        private bool _NoisePixelRandomColor = true;
        private bool _NoiseLineEnabled = true;
        private bool _NoisePixelEnabled = true;
        private Color _TextBeginColor = Color.Blue;
        private Color _TextEndColor = Color.DarkRed;
        private bool _FrameEnabled = true;
        private Color _FrameColor = Color.Black;
        private Font _TextFont;
        private bool _AutomaticNoise = true;
        #endregion

        #region constructor
        public CheckCodeImageBuilder( string aCheckCode )
        {
            if ( string.IsNullOrEmpty( aCheckCode.Trim( ) ) )
            {
                throw new Exception( "check code is not null or string empty." );
            }
            if ( aCheckCode.Length > 15 )
            {
                throw new Exception( "check code length is not bigger then 15." );
            }
            _TextFont = new Font( "Arial", 16, ( FontStyle.Bold | System.Drawing.FontStyle.Italic ) );
            _CheckCode = aCheckCode;
        }
        #endregion

        #region public properties
        public string CheckCode
        {
            get
            {
                return _CheckCode;
            }
            set
            {
                if ( _CheckCode == value )
                    return;
                _CheckCode = value;
            }
        }

        public int NoiseLineNumber
        {
            get
            {
                return _NoiseLineNumber;
            }
            set
            {
                if ( _NoiseLineNumber == value )
                    return;
                _NoiseLineNumber = value;
            }
        }

        public Color NoiseLineColor
        {
            get
            {
                return _NoiseLineColor;
            }
            set
            {
                if ( _NoiseLineColor == value )
                    return;
                _NoiseLineColor = value;
            }
        }

        public bool NoiseLineRandomColor
        {
            get
            {
                return _NoiseLineRandomColor;
            }
            set
            {
                if ( _NoiseLineRandomColor == value )
                    return;
                _NoiseLineRandomColor = value;
            }
        }

        public bool NoiseLineRandomWidth
        {
            get
            {
                return _NoiseLineRandomWidth;
            }
            set
            {
                if ( _NoiseLineRandomWidth == value )
                    return;
                _NoiseLineRandomWidth = value;
            }
        }

        public int NoisePixelNumber
        {
            get
            {
                return _NoisePixelNumber;
            }
            set
            {
                if ( _NoisePixelNumber == value )
                    return;
                _NoisePixelNumber = value;
            }
        }

        public Color NoisePixelColor
        {
            get
            {
                return _NoisePixelColor;
            }
            set
            {
                if ( _NoisePixelColor == value )
                    return;
                _NoisePixelColor = value;
            }
        }

        public bool NoisePixelRandomColor
        {
            get
            {
                return _NoisePixelRandomColor;
            }
            set
            {
                if ( _NoisePixelRandomColor == value )
                    return;
                _NoisePixelRandomColor = value;
            }
        }

        public Color TextBeginColor
        {
            get
            {
                return _TextBeginColor;
            }
            set
            {
                if ( _TextBeginColor == value )
                    return;
                _TextBeginColor = value;
            }
        }

        public Color TextEndColor
        {
            get
            {
                return _TextEndColor;
            }
            set
            {
                if ( _TextEndColor == value )
                    return;
                _TextEndColor = value;
            }
        }

        public bool FrameEnabled
        {
            get
            {
                return _FrameEnabled;
            }
            set
            {
                if ( _FrameEnabled == value )
                    return;
                _FrameEnabled = value;
            }
        }

        public Color FrameColor
        {
            get
            {
                return _FrameColor;
            }
            set
            {
                if ( _FrameColor == value )
                    return;
                _FrameColor = value;
            }
        }

        public Font TextFont
        {
            get
            {
                return _TextFont;
            }
            set
            {
                if ( _TextFont == value )
                    return;
                _TextFont = value;
            }
        }

        public bool AutomaticNoise
        {
            get
            {
                return _AutomaticNoise;
            }
            set
            {
                if ( _AutomaticNoise == value )
                    return;
                _AutomaticNoise = value;
            }
        }

        public bool NoiseLineEnabled
        {
            get
            {
                return _NoiseLineEnabled;
            }
            set
            {
                if ( _NoiseLineEnabled == value )
                    return;
                _NoiseLineEnabled = value;
            }
        }

        public bool NoisePixelEnabled
        {
            get
            {
                return _NoisePixelEnabled;
            }
            set
            {
                if ( _NoisePixelEnabled == value )
                    return;
                _NoisePixelEnabled = value;
            }
        }
        #endregion

        #region public methods
        public System.Drawing.Image ToImage( Size aImageSize )
        {
            Bitmap bmp = CreateBitmap( aImageSize );
            using ( Graphics g = Graphics.FromImage( bmp ) )
            {
                if ( _NoiseLineEnabled )
                {
                    FillNoiseLine( g, bmp );
                }

                FillCheckCode( g, bmp );

                if ( _NoisePixelEnabled )
                {
                    FillNoisePixel( g, bmp );
                }

                FillFrame( g, bmp );
                return bmp;
            }
        }

        public System.Drawing.Image ToImage( )
        {
            return ToImage( new Size( ) );
        }

        public MemoryStream ToMemoryStream( ImageFormat aFormat, Size aImageSize )
        {
            System.Drawing.Image image = ToImage( aImageSize );
            MemoryStream stream = new MemoryStream( );
            image.Save( stream, aFormat );
            return stream;
        }

        public MemoryStream ToMemoryStream( Size aImageSize )
        {
            return ToMemoryStream( ImageFormat.Gif, aImageSize );
        }

        public MemoryStream ToMemoryStream( )
        {
            return ToMemoryStream( ImageFormat.Gif, new Size( ) );
        }

        #endregion

        #region private methods

        private Bitmap CreateBitmap( Size aImageSize )
        {
            bool autoWidth = aImageSize.Width == 0;
            bool autoHeight = aImageSize.Height == 0;
            using ( Bitmap bmp = new Bitmap( 1, 1 ) )
            {
                using ( Graphics graph = Graphics.FromImage( bmp ) )
                {
                    string fillStr = _fillChar.Substring( 0, _CheckCode.Length );
                    SizeF imgSize = graph.MeasureString( fillStr, TextFont );
                    if ( autoWidth )
                    {
                        aImageSize.Width = Convert.ToInt32( imgSize.Width );
                    }
                    if (autoHeight)
                    {
                        aImageSize.Height = Convert.ToInt32( imgSize.Height );
                    }
                    return new Bitmap( bmp, aImageSize.Width, aImageSize.Height );
                }
            }
        }

        private Color GetRandomColor( )
        {
            return GetRandomColor( new Random( ) );
        }

        private Color GetRandomColor( Random aRandom )
        {
            int alpha = aRandom.Next( 255 );
            int r = aRandom.Next( 255 );
            int g = aRandom.Next( 255 );
            int b = aRandom.Next( 255 );
            return Color.FromArgb( r, g, b );
        }

        private void FillNoiseLine( Graphics aGraphics, System.Drawing.Image aImage )
        {
            Random random = new Random( );
            aGraphics.Clear( Color.White );
            int autoLineNumber = Math.Min( aImage.Width, aImage.Height ) / 5;
            if ( !_AutomaticNoise )
            {
                autoLineNumber = _NoiseLineNumber;
            }
            for ( int i = 0; i < autoLineNumber; i++ )
            {
                int x1 = random.Next( aImage.Width );
                int x2 = random.Next( aImage.Width );
                int y1 = random.Next( aImage.Height );
                int y2 = random.Next( aImage.Height );
                int lenWidth = 1;
                if ( _NoiseLineRandomWidth )
                {
                    lenWidth = random.Next( 1, 4 );
                }
                Color lineColor = _NoiseLineColor;
                if ( _NoiseLineRandomColor )
                {
                    lineColor = GetRandomColor( random );
                }
                aGraphics.DrawLine( new Pen( lineColor, lenWidth ), x1, y1, x2, y2 );
            }
        }

        private void FillCheckCode( Graphics aGraphics, System.Drawing.Image aImage )
        {
            Rectangle rect = new Rectangle( 0, 0, aImage.Width, aImage.Height );
            LinearGradientBrush brush = new LinearGradientBrush( rect, TextBeginColor, TextEndColor, 1.2f, true );
            using ( Graphics graph = Graphics.FromImage( aImage ) )
            {
                SizeF size = graph.MeasureString( _CheckCode, TextFont );
                aGraphics.DrawString( _CheckCode, TextFont, brush, ( aImage.Width - size.Width ) / 2, ( aImage.Height - size.Height ) / 2 );
            }
        }

        private void FillNoisePixel( Graphics aGraphics, Bitmap aImage )
        {
            Random random = new Random( );
            int num = Convert.ToInt32( aImage.Width * aImage.Height * 0.05 );
            if ( !_AutomaticNoise )
            {
                num = _NoisePixelNumber;
            }
            for ( int i = 0; i < num; i++ )
            {
                int x = random.Next( aImage.Width );
                int y = random.Next( aImage.Height );
                Color pixelColor = _NoisePixelColor;
                if ( _NoisePixelRandomColor )
                {
                    pixelColor = GetRandomColor( random );
                }
                aImage.SetPixel( x, y, pixelColor );
            }
        }

        private void FillFrame( Graphics aGraphics, System.Drawing.Image aImage )
        {
            aGraphics.DrawRectangle( new Pen( FrameColor ), 0, 0, aImage.Width - 1, aImage.Height - 1 );
        }
        #endregion
    }
}
