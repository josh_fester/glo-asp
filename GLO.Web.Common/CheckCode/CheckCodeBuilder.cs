﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Web.Mvc
{
    public static class CheckCodeBuilder
    {
        private static int _defaultCodeLength = 5;
        private static string _sourceString = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";

        public static string NewCode( )
        {
            return NewCode( _defaultCodeLength );
        }

        public static string NewCode( int aCodeLength )
        {
            Random random = new Random( );
            StringBuilder strBuilder = new StringBuilder( aCodeLength );
            for ( int i = 0; i < aCodeLength; i++ )
            {
                strBuilder.Append( _sourceString[ random.Next( _sourceString.Length - 1 ) ] );
            }
            return strBuilder.ToString( );
        }
    }
}
