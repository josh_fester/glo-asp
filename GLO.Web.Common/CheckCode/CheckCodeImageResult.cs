﻿using System;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Imaging;


namespace System.Web.Mvc
{
    public class CheckCodeImageResult : ActionResult
    {

        #region public properties
        public string CheckCode { get; set; }
        
        public Size ImageSize { get; set; }
        #endregion

        #region constructors
        public CheckCodeImageResult( string aCheckCode )
        {
            CheckCode = aCheckCode;
        }

        public CheckCodeImageResult( string aCheckCode, Size aImageSize )
        {
            CheckCode = aCheckCode;
            ImageSize = aImageSize;
        }

        public CheckCodeImageResult( string aCheckCode, int aImageWidth, int aImageHeight )
            : this( aCheckCode, new Size( aImageWidth, aImageHeight ) )
        {
        }
        #endregion

        #region overried methods
        public override void ExecuteResult( ControllerContext aContext )
        {
            CheckCodeImageBuilder imageBuilder = new CheckCodeImageBuilder( CheckCode );
            Bitmap image = (Bitmap) imageBuilder.ToImage( ImageSize );
            try
            {
                aContext.HttpContext.Response.ContentType = "image/Gif";
                image.Save( aContext.HttpContext.Response.OutputStream, ImageFormat.Gif );
            }
            finally
            {
                image.Dispose( );
            }
        }
        #endregion

    }
}
