using System;
using System.Web.Mvc;


namespace GLO.Web.Common
{
    public class CheckCodeController : Controller
    {
        #region private static fields
        private static int _DefaultCheckCodeLength = 5;
        private static int _DefaultImageWidth = 0;
        private static int _DefaultImageHeight = 0;
        public static readonly string CheckCodeKey = "Sys_CheckCode";
        #endregion

        #region public static properties
        public static int DefaultCheckCodeLength
        {
            get
            {
                return _DefaultCheckCodeLength;
            }
            set
            {
                if ( value < 1 || value > 15 )
                {
                    throw new Exception("DefaultCheckCodeLength,must between 1 to 15 ,default 5.");
                }
                _DefaultCheckCodeLength = value;
            }
        }

        public static int DefaultImageWidth
        {
            get
            {
                return _DefaultImageWidth;
            }
            set
            {
                if ( value < 0 || value > 200 )
                {
                    throw new Exception("DefaultImageWidth,must between 0 to 200,0 is auto width.");
                }
                _DefaultImageWidth = value;
            }
        }

        public static int DefaultImageHeight
        {
            get
            {
                return _DefaultImageHeight;
            }
            set
            {
                if ( value < 0 || value > 200 )
                {
                    throw new Exception("DefaultImageHeight,must between 0 and 200,0 is auto height");
                }
                _DefaultImageHeight = value;
            }
        }
        #endregion
        protected virtual string CheckCode
        {
            get
            {
                return ( (string) Session[ CheckCodeKey ] ) ?? String.Empty;
            }
            set
            {
                Session[ CheckCodeKey ] = value;
            }
        }

        public static void SetOptions( int aDefaultCheckCodeLength, int aDefaultImageWidth, int aDefaultImageHeight )
        {
            DefaultCheckCodeLength = aDefaultCheckCodeLength;
            DefaultImageWidth = aDefaultImageWidth;
            DefaultImageHeight = aDefaultImageHeight;
        }

        public virtual CheckCodeImageResult New( )
        {
            CheckCode = CheckCodeBuilder.NewCode( _DefaultCheckCodeLength );
            return new CheckCodeImageResult( CheckCode, _DefaultImageWidth, _DefaultImageHeight );
        }

        [HttpPost]
        public virtual ActionResult Verify( string aCheckCode )
        {
            bool flag = ( String.Compare( aCheckCode, CheckCode, true ) == 0 );
            return Json( flag );
        }
    }
}
