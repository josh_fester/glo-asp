﻿using System;
using System.Web.Mvc;
using StructureMap;
using System.Web;
using System.Web.Routing;

namespace GLO.Web.Common
{
    public class StructureMapControllerFactory : DefaultControllerFactory
    {

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            IController result = null;
            try
            {
                if (controllerType == null)
                    result = base.GetControllerInstance(requestContext, controllerType);
                else
                    result = ObjectFactory.GetInstance(controllerType) as Controller;
            }
            catch (StructureMapException)
            {
                System.Diagnostics.Debug.WriteLine(ObjectFactory.WhatDoIHave());
                throw;
            }

            return result;
        }
    }
}
