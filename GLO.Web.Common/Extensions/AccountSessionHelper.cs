﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using GLO.Web.Common;

namespace System.Web.Mvc
{
    public static class AccountSessionHelper
    {

        #region session keys

        #endregion

        #region private methods
        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }
        #endregion

        #region public properties
        public static string CheckCode
        {
            get
            {
                return ( (string) Session[ CheckCodeController.CheckCodeKey ] ) ?? String.Empty;
            }
            set
            {
                Session[ CheckCodeController.CheckCodeKey ] = value;
            }
        }
        #endregion

    }
}
