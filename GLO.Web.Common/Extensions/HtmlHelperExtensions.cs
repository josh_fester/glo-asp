﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {
        public static string Pager(this HtmlHelper helper, int pageBlock, int currentPage, int perPage, int totalItems, int? pageCap, string action)
        {
            return helper.Pager(pageBlock, currentPage, perPage, totalItems, pageCap, action, null);
        }

        public static string Pager(this HtmlHelper helper, int pageBlock, int currentPage, int perPage, int totalItems, int? pageCap, string action, object actionAttributes)
        {
            if (totalItems <= perPage) return string.Empty;
            int totalPages = (int)Math.Ceiling((decimal)totalItems / perPage);
            if (pageCap != null && totalPages > 100)
                totalPages = pageCap.Value;
            UrlHelper url = new UrlHelper(helper.ViewContext.RequestContext);
            StringBuilder sb = new StringBuilder();
            Func<int, string, string> createLink = delegate(int page, string text)
            {
                RouteValueDictionary rvd = url.GetBaseRouteValues();
                RouteValueDictionary overrideRVD = new RouteValueDictionary(actionAttributes);
                foreach (KeyValuePair<string, object> item in overrideRVD)
                    rvd[item.Key] = item.Value;

                TagBuilder t = new TagBuilder("a");
                if (page != currentPage)
                {
                    t.Attributes.Add("href", url.Action(action, rvd, new { p = page.ToString() }));
                    t.Attributes.Add("rel", url.Action(action, rvd, new { p = string.Empty }));
                }

                t.Attributes.Add("p", string.Format("{0}", page.ToString()));
                t.Attributes.Add("xpg", string.Format("xpg{0}", page.ToString()));
                t.Attributes.Add("xtp", totalPages.ToString());
                t.AddCssClass(page == currentPage ? "pagerLinkCurrent" : "pagerLink");
                t.InnerHtml = text;
                return t.ToString();
            };

            TagBuilder tb;
            if (currentPage != 1)
                sb.Append(createLink(currentPage - 1, "Previous"));
            else
            {
                tb = new TagBuilder("a");
                tb.AddCssClass("pagerLinkDisabled");
                tb.InnerHtml = "Previous";
                sb.Append(tb.ToString());
            }

            sb.Append(" | ");
            sb.AppendFormat(" {0} ", createLink(1, "1"));

            int rangeBlock = ((currentPage - 1) / pageBlock) * pageBlock;
            if (rangeBlock > currentPage)
                rangeBlock -= pageBlock;
            else if ((rangeBlock + pageBlock) < currentPage)
                rangeBlock += pageBlock;

            int startRange = (rangeBlock > 0) ? rangeBlock + 1 : 2;
            if (totalPages % pageBlock != 0 && totalPages > pageBlock && currentPage > totalPages / pageBlock * pageBlock)
            {
                startRange -= pageBlock - totalPages % pageBlock;
            }

            int endRange = (rangeBlock + pageBlock) > totalPages - 1 ? totalPages - 1 : rangeBlock + pageBlock;
            if (startRange > 2)
                sb.Append(" ... ");

            for (int i = startRange; i <= endRange; i++)
                sb.AppendFormat(" {0} ", createLink(i, i.ToString()));

            if (endRange < totalPages - 1)
                sb.Append(" ... ");

            if (totalPages > 1)
                sb.AppendFormat(" {0} ", createLink(totalPages, totalPages.ToString()));

            sb.Append(" | ");

            if (currentPage < totalPages)
                sb.Append(createLink(currentPage + 1, "Next"));
            else
            {
                tb = new TagBuilder("a");
                tb.AddCssClass("pagerLinkDisabled");
                tb.InnerHtml = "Next";
                sb.Append(tb.ToString());
            }

            return sb.ToString();
        }
        public static RouteValueDictionary GetBaseRouteValues(this UrlHelper helper)
        {
            RouteValueDictionary routingValues = new RouteValueDictionary();

            // add route values from existing route data (do before action route, so as not to overwrite the action/controller)
            IDictionary<string, object> existingRouteDataValues = (IDictionary<string, object>)helper.RequestContext.RouteData.Values;
            foreach (KeyValuePair<string, object> kvp in existingRouteDataValues ?? new Dictionary<string, object>())
            {
                routingValues[kvp.Key] = kvp.Value;
            }

            foreach (string key in helper.RequestContext.HttpContext.Request.QueryString.Keys)
            {
                if (!string.IsNullOrEmpty(key))
                    routingValues[key] = helper.RequestContext.HttpContext.Request.QueryString[key].ToString();
            }

            // remove the action and controller values
            routingValues.Remove("action");
            routingValues.Remove("controller");

            return routingValues;
        }
        public static string Action(this UrlHelper helper, string actionName, RouteValueDictionary rvd, object newrvd)
        {
            RouteValueDictionary routingValues = rvd;
            RouteValueDictionary newvalues = new RouteValueDictionary(newrvd);
            foreach (KeyValuePair<string, object> kvp in newvalues ?? new RouteValueDictionary())
            {
                if (routingValues.ContainsKey(kvp.Key))
                    routingValues[kvp.Key] = kvp.Value;
                else
                    routingValues.Add(kvp.Key, kvp.Value);
            }

            return helper.Action(actionName, routingValues);
        }

        public static void SetCookie(HttpContextBase context, string cookieName, string cookieValue, int cookieExpiration)
        {
            var cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddDays(cookieExpiration);
            cookie.Value = cookieValue;
            context.Response.Cookies.Add(cookie);
        }
        //set cookie
        public static void SetCookies(string userName, string userPwd)
        {
            HttpResponse response = HttpContext.Current.Response;
            if (response != null)
            {
                HttpCookie cookie = response.Cookies["userInfo"];
                if (cookie != null)
                {
                    cookie.Values.Set("userName", userName);
                    cookie.Values.Set("userPwd", userPwd);
                    cookie.Values.Set("check", "1");
                    cookie.Expires = DateTime.Now.AddDays(365);
                    response.SetCookie(cookie);
                }
            }

        }
        //delete cookie
        public static void DelCookies()
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request != null)
            {
                HttpCookie cookie = request.Cookies["userInfo"];
                if (cookie != null)
                {
                    cookie.Values.Remove("userPwd");
                    cookie.Values.Remove("check");
                    cookie.Expires = DateTime.Now.AddDays(365);
                }
                HttpResponse response = HttpContext.Current.Response;
                response.SetCookie(cookie);
            }
        }
        public static string Getcookie(string Name)
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request != null)
            {
                HttpCookie cookie = request.Cookies["userInfo"];
                if (cookie != null)
                {
                    return cookie[Name].ToString();
                }
            }
            return String.Empty;
        }
    }
}
