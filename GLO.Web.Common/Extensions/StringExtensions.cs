﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace System.Web.Mvc
{
    public static class StringExtensions
    {
        public static string Trim(this string s, int maxLength)
        {
            return s.Trim(maxLength, false);
        }

        public static string Trim(this string s, int maxLength, bool addEllipses)
        {
            s = s.ClearHTML();
            if (s.Length > maxLength)
            {
                int index = s.Substring(0, maxLength).LastIndexOfAny(new char[] { ' ', '.', ',', '-', '/', ')', '(' });
                if (index > -1)
                    s = s.Substring(0, index);

                if (addEllipses)
                {
                    if (s.Length > maxLength - 3)
                    {
                        s = s.Substring(0, maxLength - 3);
                    }
                    s = string.Format("{0}...", s);
                }
            }

            return s;
        }
        /// <summary>
        /// Remove all HTML tag from current string.
        /// </summary>
        /// <param name="s">current string</param>
        /// <returns>string</returns>
        public static string ClearHTML(this string s)
        {
            if (s != "")
            {
                Regex r = null;
                Match m = null;

                r = new Regex(@"<\/?[^>]*>", RegexOptions.IgnoreCase);
                for (m = r.Match(s); m.Success; m = m.NextMatch())
                {
                    s = s.Replace(m.Groups[0].ToString(), "");
                }
            }
            return s;
        }
    }

}
