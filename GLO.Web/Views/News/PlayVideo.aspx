﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PlayVideo
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script src="<%=Url.Content("~/Scripts/jquery.flash.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">                
                <%: Html.DisplayFor(model => model.Title) %>
            </div>
            <div class="boxer_content">
                <%: Html.HiddenFor(model=>model.FileUrl)%>
                <%if (Model.FileType == 2)
                  { %>
                <object id="bofang" classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6" width="700" height="430" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701standby=Loading Microsoft? Windows Media? Player components..." type="application/x-oleobject">
                    <param name="URL" value='<%= Model.FileUrl %>'/>
                    <param name="rate" value="1" />
                    <param name="balance" value="0" />
                    <param name="currentPosition" value="0" />
                    <param name="playCount" value="1" />
                    <param name="autoStart" value="-1" />
                    <param name="currentMarker" value="0" />
                    <param name="invokeURLs" value="-1" />
                    <param name="volume" value="50" />
                    <param name="mute" value="0" />
                    <param name="uiMode" value="mini" />
                    <param name="stretchToFit" value="true" />
                    <param name="windowlessVideo" value="false" />
                    <param name="enabled" value="-1" />
                    <param name="enableContextMenu" value="0" />
                    <param name="fullScreen" value="0" />
                </object>
                <%}
                  else
                  {%>
                 <div style="text-align:center;">
                    <img src="<%=Model.FileUrl %>" />
                  </div>
                  <%} %>
                  
                 <div style="margin:10px;text-align:center;">
                  <%=Model.Content %>
                  </div>

                  <div class="news_back" style="margin-tpo:30px;"><%: Html.ActionLink("Back to List", "Index") %></div>

            </div>
        </div>
    </div>
</asp:Content>
