﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Insights
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="box_insight">
            <div class="insight_left">            
              
                <div class="insight_line">
                    <div class="insight_head">CEO <span class="blue">PICKS</span></div>
                    <div class="insight_image">
                        <img src="/Styles/images/insight_head.png" />
                        <div class="insight_hyperlink">
                            <div class="hyperlink_left">
                                <ul>
                                    <li><a href="#">Why Values Matter</a></li>
                                    <li><a href="#">Responsibility and MBAs</a></li>
                                    <li><a href="#">Change Is The Only Constant</a></li>
                                </ul>
                            </div>
                            <div class="hyperlink_right">
                                <a href="#">read more &gt;</a>
                            </div>
                        </div>
                    </div>
                    <div class="insight_icon">
                        <img src="/Styles/images/insight_icon1.gif" /><sup>1</sup>
                        <img src="/Styles/images/insight_icon2.gif" /><sup>2</sup>
                        <img src="/Styles/images/insight_icon3.gif" /><sup>3</sup>
                        <img src="/Styles/images/insight_icon4.gif" /><sup>4</sup>
                    </div>
                </div>
                
                <div class="insight_line1">
                    <div class="insight_head">MORE IN <span class="blue">LEADERSHIP DISCUSSIONS</span></div>
                </div>
                
                <div class="insight_line2">
                    <div class="insight_content">
                        <table>
                            <tr>
                                <td class="tname"><a href="#">how ttttttttttttttt</a></td>
                                <td class="quote">
                                    <img src="/Styles/images/insight_icon5.gif" /><sup>4</sup>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="put">17 July 2013 15:24 / Administrator</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                GLO experts are independent professionals who are expected to be accountable for their communications and collaboration with any GLO user or client introduced via the GLO community. <br />  <br /> 
                                GLO is not liable for any disputes between the GLO expert and other members and clients of the GLO community.  <br />  <br /> 
                                Similarly, the GLO expert does not carry any responsibility for disputes not related to his/her own actions as an expert member. <br />  <br /> 
                                GLO is taking confidentiality and privacy of its members seriously.  <br />  <br /> 
                                Experts are requested not to overload GLO users with spam emails and repeated requests for contact other than what might be considered to be a reasonable level of engagement in introducing expert services.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="star">
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    &nbsp;&nbsp;Be the first one to rate this past
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="tag">
                                    Tag: Leadership; Self-awareness; Wisdom;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <div class="insight_line2">
                    <div class="insight_content">
                        <table>
                            <tr>
                                <td class="tname"><a href="#">how ttttttttttttttt</a></td>
                                <td class="quote">
                                    <img src="/Styles/images/insight_icon5.gif" /><sup>4</sup>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="put">17 July 2013 15:24 / Administrator</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                GLO experts are independent professionals who are expected to be accountable for their communications and collaboration with any GLO user or client introduced via the GLO community. <br />  <br /> 
                                GLO is not liable for any disputes between the GLO expert and other members and clients of the GLO community.  <br />  <br /> 
                                Similarly, the GLO expert does not carry any responsibility for disputes not related to his/her own actions as an expert member. <br />  <br /> 
                                GLO is taking confidentiality and privacy of its members seriously.  <br />  <br /> 
                                Experts are requested not to overload GLO users with spam emails and repeated requests for contact other than what might be considered to be a reasonable level of engagement in introducing expert services.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="star">
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    <img src="/Styles/images/icon_star.gif" />
                                    &nbsp;&nbsp;Be the first one to rate this past
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="tag">
                                    Tag: Leadership; Self-awareness; Wisdom;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <div class="insight_right">        
                
                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">NEWS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_news.gif" /></div>
                    <div class="insight_news">
                        <ul>
                            <li>GLO co-founder Mike Thompson speaks at GoogleUK HQ</li>
                            <li>GLO co-founder Mike Thompson speaks at GoogleUK HQ</li>
                        </ul>
                    </div>
                </div> 

                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">EVENTS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_events.gif" /></div>
                    <div class="insight_events">
                        <table>
                            <tr>
                                <td><a href="#">Get to know GLO</a></td>
                                <td class="rsvp"><a href="mailto:abc@abc.com">RSVP</a></td>
                            </tr>
                            <tr>
                                <td><a href="#">The Next Gen Leaders</a></td>
                                <td class="rsvp"><a href="mailto:abc@abc.com">RSVP</a></td>
                            </tr>
                        </table>
                    </div>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">GLO <span class="blue">SURVEY</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_survey.gif" /></div>
                    <div class="insight_survey">
                        How do our community members rate the competencies of wisdom?
                    </div>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">Active <span class="blue">Survey</span></div>
                    <div class="insight_note">Make your opinion count</div>
                    <div class="insight_survey">
                        <table>
                            <tr>
                                <td class="index">#1</td>
                                <td class="survey">
                                    <a href="#">The Wise Leadership Check-up</a><br />
                                    <span class="blue">Participate.</span> <span class="gray">550+ replies in the last 24 hours.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="index">#2</td>
                                <td class="survey">
                                    <a href="#">The Wise Leadership Check-up</a><br />
                                    <span class="blue">Participate.</span> <span class="gray">550+ replies in the last 24 hours.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> 

            </div>
            <div class="insight_fix"></div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            var h1 = $('.insight_left').height();
            var h2 = $('.insight_right').height();
            if (h1 < h2) $('.insight_left').css('height', h2 + 'px');
        });
    </script>
</asp:Content>