﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About - My ASP.NET MVC Application
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h1>About.</h1>
        <h2><%: ViewBag.Message %></h2>
    </hgroup>
        <div>
    <div class="Banner-field BannerImg" id="imageID">
    <img src="" alt=""/>
    </div>
<%--    <form id="ajaxUploadFormLogo" action="<%= Url.Action("UploadImage", "Home")%>"  method="post" enctype="multipart/form-data">
    <input id="file" name="file" type="file" value="" />
    <input id="ajaxUploadButton" type="submit" value="Upload"/>  
    </form>--%>
    

     <form id="ajaxUploadFormLogo" action="<%= Url.Action("UploadImage", "Home")%>" method="post"
                    enctype="multipart/form-data" style="width: 500px;">
                    <fieldset>
                        <legend>Upload New Logo</legend>
                        <input id="file" name="file" type="file" value="" />
                        <input id="Submit1" type="submit" value="Upload" />
                    </fieldset>
                    </form>
    
                 

</div>
    <article>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus tincidunt justo nec tempor. 
            Aliquam erat volutpat. Fusce facilisis ullamcorper consequat. Vestibulum non sapien lectus. Nam 
            mi augue, posuere at tempus vel, dignissim vitae nulla. Nullam at quam eu sapien mattis ultrices. 
            Quisque quis leo mi, at lobortis dolor. Nullam scelerisque facilisis placerat. Fusce a augue 
            erat, malesuada euismod dui. Duis iaculis risus id felis volutpat elementum. Fusce blandit iaculis 
            quam a cursus. Cras varius tincidunt cursus. Morbi justo eros, adipiscing ac placerat sed, posuere 
            et mi. Suspendisse vulputate viverra aliquet. Duis non enim a nibh consequat mollis ac tempor 
            lorem. Phasellus elit leo, semper eu luctus et, suscipit at lacus. In hac habitasse platea 
            dictumst. Duis dignissim justo sit amet nulla pulvinar sodales.
        </p>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus tincidunt justo nec tempor. 
            Aliquam erat volutpat. Fusce facilisis ullamcorper consequat. Vestibulum non sapien lectus. Nam 
            mi augue, posuere at tempus vel, dignissim vitae nulla. Nullam at quam eu sapien mattis ultrices. 
            Quisque quis leo mi, at lobortis dolor. Nullam scelerisque facilisis placerat. Fusce a augue 
            erat, malesuada euismod dui. Duis iaculis risus id felis volutpat elementum. Fusce blandit iaculis 
            quam a cursus. Cras varius tincidunt cursus. Morbi justo eros, adipiscing ac placerat sed, posuere 
            et mi. Suspendisse vulputate viverra aliquet. Duis non enim a nibh consequat mollis ac tempor 
            lorem. Phasellus elit leo, semper eu luctus et, suscipit at lacus. In hac habitasse platea 
            dictumst. Duis dignissim justo sit amet nulla pulvinar sodales.
        </p>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus tincidunt justo nec tempor. 
            Aliquam erat volutpat. Fusce facilisis ullamcorper consequat. Vestibulum non sapien lectus. Nam 
            mi augue, posuere at tempus vel, dignissim vitae nulla. Nullam at quam eu sapien mattis ultrices. 
            Quisque quis leo mi, at lobortis dolor. Nullam scelerisque facilisis placerat. Fusce a augue 
            erat, malesuada euismod dui. Duis iaculis risus id felis volutpat elementum. Fusce blandit iaculis 
            quam a cursus. Cras varius tincidunt cursus. Morbi justo eros, adipiscing ac placerat sed, posuere 
            et mi. Suspendisse vulputate viverra aliquet. Duis non enim a nibh consequat mollis ac tempor 
            lorem. Phasellus elit leo, semper eu luctus et, suscipit at lacus. In hac habitasse platea 
            dictumst. Duis dignissim justo sit amet nulla pulvinar sodales.
        </p>
         <textarea name="editor1"></textarea><script type="text/javascript">
                                                 window.onload = function () {
                                                     CKEDITOR.replace('editor1');
                                                 };
</script> 
    </article>
   
    <aside>
        <h3>Aside Title</h3>
        <p>
            Fusce facilisis ullamcorper consequat. Vestibulum non sapien lectus. Nam 
            mi augue, posuere at tempus vel, dignissim vitae nulla.
        </p>
        <ul>
            <li><%: Html.ActionLink("Home", "Index", "Home") %></li>
            <li><%: Html.ActionLink("About", "About", "Home") %>
                </li>
            <li><%: Html.ActionLink("Contact", "Contact", "Home") %></li>
        </ul>
    </aside>
<div id="show">
just for test
</div>
<input id="btnshow" type="button" value="showtest" />

<div id="dialog" title="Basic dialog">
    <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

<script language="javascript" type="text/javascript">

    $(document).ready(function () {

        $("#ajaxUploadFormLogo").ajaxForm({
            iframe: "true",
            success: function (response) {

                var msg = JSON.parse($(response).val());
                if (msg.status == "valid") {
                    if (msg.message == "File Already Exists On File Server") {
                        alertbox(msg.message);
                    }
                    $("#imageID").empty().append($("<img />").attr({ "src": msg.filesavedtosecured, "x:src": msg.filesavedto }));
                } else if (msg.status == "invalid") {
                    alertbox(msg.message);
                }

                $("#ajaxUploadFormLogo #file").val("");
            }
        });

        $("#btnshow").click(function () {
            art.dialog({
                //follow: document.getElementById('options-follow-demo-runCode'),
                content: $("#dialog").html(),
                lock:true
            });
        });


    });

  
</script>
</asp:Content>