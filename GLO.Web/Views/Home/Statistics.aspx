﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   GLO statistics
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>  
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var companybyindustry = new google.visualization.DataTable();
            companybyindustry.addColumn('string', 'Name');
            companybyindustry.addColumn('number', 'Count');
            companybyindustry.addRows([
              <%=ViewBag.CompanyByIndustry%>

        ]);
        
         var companybyemployee = new google.visualization.DataTable();
            companybyemployee.addColumn('string', 'Name');
            companybyemployee.addColumn('number', 'Count');
            companybyemployee.addRows([
             <%=ViewBag.CompanyByEmployee%>
        ]);

         var leaderbyindustry = new google.visualization.DataTable();
            leaderbyindustry.addColumn('string', 'Name');
            leaderbyindustry.addColumn('number', 'Count');
            leaderbyindustry.addRows([
              <%=ViewBag.LeaderByIndustry%>
        ]);

        var leaderbylocation = new google.visualization.DataTable();
            leaderbylocation.addColumn('string', 'Name');
            leaderbylocation.addColumn('number', 'Count');
            leaderbylocation.addRows([
              <%=ViewBag.LeaderByLocation%>
        ]);

         var expertbyindustry = new google.visualization.DataTable();
            expertbyindustry.addColumn('string', 'Name');
            expertbyindustry.addColumn('number', 'Count');
            expertbyindustry.addRows([
              <%=ViewBag.ExpertByIndustry%>
        ]);

        var expertbylocation = new google.visualization.DataTable();
            expertbylocation.addColumn('string', 'Name');
            expertbylocation.addColumn('number', 'Count');
            expertbylocation.addRows([
              <%=ViewBag.ExpertByLocation%>
        ]);

     
            var options = {
                candlestick: { hollowIsRising: false,
                    risingColor: { stroke: "Green" }
                },
                isHtml:true,
                legend:'none',
                width: 253, height: 260,

                title: 'By employee size',
                colors: ['#006993', '#0077A6', '#0084B7', '#008EC4', '#0098D2', '#79B5E1', '#B1D0EB']
            };


            options.title ="By industry";
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(leaderbyindustry, options);

            options.title ="By location";
            var chart1 = new google.visualization.PieChart(document.getElementById('chart_div1'));
            chart1.draw(leaderbylocation, options);

            options.title ="By industry";
            var chart2 = new google.visualization.PieChart(document.getElementById('chart_div2'));
            chart2.draw(companybyindustry, options);

            options.title ="By employee size";
            var chart3 = new google.visualization.PieChart(document.getElementById('chart_div3'));
            chart3.draw(companybyemployee, options);

             options.title ="By industry";
             var chart4 = new google.visualization.PieChart(document.getElementById('chart_div4'));
            chart4.draw(expertbyindustry, options);

             options.title ="By location";
            var chart5 = new google.visualization.PieChart(document.getElementById('chart_div5'));
            chart5.draw(expertbylocation, options);
        }  

        $(document).ready(function(){
         $("g").mouseover();
        });
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
               GLO Statistics
            </div>
            <div class="boxer_content">
                <div style="padding:0 0 20px 0;text-align:center;font-size:15px;">GLO statistics provides an overview of industry background<br />
and location of GLO leaders, companies and HR experts as well as<br />
a view by employee size of our company members 
</div>
                <div class="statistic_list">
                    <h2>Leaders</h2>
                    <div id="chart_div"></div> 
                    <div id="chart_div1"></div> 
                </div>

                <div class="statistic_list">
                    <h2>Companies</h2>
                    <div id="chart_div2"></div> 
                    <div id="chart_div3"></div> 
                </div>

                <div class="statistic_list">
                    <h2>Experts</h2>
                    <div id="chart_div4"></div> 
                    <div id="chart_div5"></div> 
                </div>         
            </div>
        </div>
    </div>
    </form>
</asp:Content>