﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Assurance
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO Assurance Program
            </div>
            <div class="assurance_wrap">
                <div class="assurance_title">
                The GLO Assurance Program is a progressive set of actions taken by our leader members to ascertain a<br />
                higher level of integrity of their profile data and to better assess a leader’s ‘wisdom’ in decision-making:
                </div>

                <div class="assurance_content">
                    <div class="assurance_left">
                        <img src="/Styles/images/assurance.gif" />
                    </div>
                    <div class="assurance_right">

                        <div class="assurance_item1">
                            <div class="assurance_line"><img src="/Styles/images/profile1.gif" /></div>
                            <div class="assurance_line"><b>Profile</b></div>
                            <div class="assurance_line">
                                Unique profile focused<br />
                                on values, vision and passion.<br />
                                Graphic CV and online interview.
                            </div>
                        </div>

                        <div class="assurance_item1">
                            <div class="assurance_line"><img src="/Styles/images/recommend1.gif" /></div>
                            <div class="assurance_line"><b>Recommend</b></div>
                            <div class="assurance_line">
                                Recommendations by a core<br />
                                8 people network who assess<br />
                                values and competencies.
                            </div>
                        </div>

                        <div class="assurance_item1">
                            <div class="assurance_line"><img src="/Styles/images/interview1.gif" /></div>
                            <div class="assurance_line"><b>Interview</b></div>
                            <div class="assurance_line">
                               Offline (Skype video) interview<br />
                                 to confirm profile ID and<br />
                                 review interview questions.
                            </div>
                        </div>

                        <div class="assurance_item2">
                            <div class="assurance_line"><img src="/Styles/images/online1.gif" /></div>
                            <div class="assurance_line"><b>Online Assessment</b></div>
                            <div class="assurance_line">
                                Online assessment of<br />
                                 <em>good</em> or <em>wise</em><br />
                                 decision-marking skills.
                            </div>
                        </div>
                        

                        <div class="assurance_item2">
                            <div class="assurance_line"><img src="/Styles/images/offline1.gif" /></div>
                            <div class="assurance_line"><b>Offline Assessment</b></div>
                            <div class="assurance_line">
                                Offline assessment of<br />
                                 <em>good</em> or <em>wise</em><br />
                                 decision-marking skills.
                            </div>
                        </div>

                        <div class="assurance_item3">
                            <div class="assurance_line"><img src="/Styles/images/wldp1.gif" /></div>
                            <div class="assurance_line"><b>WLDP Participation</b></div>
                            <div class="assurance_line">
                                Participation in our<br />
                                 four days Wise Leadership<br />
                                 Development Program.
                            </div>
                        </div>

                        <div class="assurance_item2">
                            <div class="assurance_line"><img src="/Styles/images/coaching1.gif" /></div>
                            <div class="assurance_line"><b>Coaching</b></div>
                            <div class="assurance_line">
                                Participation in GLO's<br />
                                 Wise Leadership<br />
                                 Coaching Program.
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>