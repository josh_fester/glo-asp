﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<List<GLO.Data.Models.GLO_CompanyJob>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Job advance search
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="search_wrapper">
    	<div class="search_header1">Jobs</div>
        <div class="search_boxer1">      
            <div class="boxer1_left">
                        	<table class="search_item">
                            	<tr>
                                	<td colspan="2" class="item_headline">Custom job search</td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Keyword</td>
                                	<td class="item_option"><input type="text" id="KeyWord" class="keyword" value="<%=Request.QueryString["keyword"] %>"></td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Location</td>
                                	<td class="item_option">
                                        <div class="multiple_box">
                                            <input class="multiple_value searchlocation" id="SelectLocation" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Location)?ViewBag.Location:"All Location" %>' />
                                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />
                                            <div class="multiple_wrapper">
                                                <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                                            </div>    
                                        </div>
                                    </td>
                                </tr>
                            	<tr>
                                	<td class="item_title">Function</td>
                                	<td class="item_option">
                                        <div class="select_box">
                                            <input class="select_value" id="SelectFunction" type="text" readonly="readonly" value='<%=!string.IsNullOrEmpty(ViewBag.Func)?ViewBag.Func:"All Function" %>' />
                                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
            	                            <div class="select_option">
                                                <ul>
                                                    <li>All Function</li>
                                                    <%foreach (var func in (List<GLO.Data.Models.GLO_Functions>)ViewBag.FuncList)
                                                      { %>
                                                    <li><%=func.FunctionName %></li>
                                                    <%} %>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                	<td class="item_title">Education</td>
                                	<td class="item_option"><input type="text" id="Education" class="keyword" value="<%=ViewBag.Education%>"></td>
                                </tr>
                                 <tr>
                                	<td class="item_title">Industry</td>
                                	<td class="item_option">
                                        <div class="multiple_box">
                                          <input class="multiple_value" id="Industry" name="Industry" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(ViewBag.Industry)?ViewBag.Industry:"All Industry" %>" />
                                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                                        <div class="multiple_wrapper">
                                            <%Html.RenderPartial("~/Views/Shared/IndustrySelect.ascx", (IList<GLO.Data.Models.GLO_IndustryCategory>)ViewBag.IndustryList); %>
                                        </div>
                                </div>
                                    </td>
                                </tr>
                                 <tr>
                                	<td class="item_title">Publish date</td>
                                	<td class="item_option"><input type="text" id="JobDate" class="keyword" readonly="readonly" value="<%=ViewBag.Date%>"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                    <input type="button" value="Search" class="bluebtn" onclick="return search();" />
                                    </td>
                                </tr>
                            </table>
                            <div class="result_title">
                                We found <%=Model.Count %> Good Jobs.
                            </div>
                            <div class="result_container" style="height:330px;">
                                <table class="result">
                                    <%foreach (var job in Model){ %>
                            	<tr>
                                	<td><a href="#" x:jobid="<%=job.JobID %>" class="JobTitle"><%=job.JobTitle %></a></td>
                                	<td><%=job.Location %></td>
                                </tr>
                                <%} %>
                                </table>
                            </div>
            </div>
           <div class="boxer1_jobinfo">
          <%Html.RenderPartial("~/Views/Shared/Company/JobDetail.ascx", Model.FirstOrDefault() == null ? new GLO.Data.Models.GLO_CompanyJob() : Model.FirstOrDefault());%>           
           </div>  
        </div>
        <div class="search_footer1"></div>
    </div>

   <script type="text/javascript">
       $(document).ready(function () {
           $("#JobDate").datepicker({
               defaultDate: "+1w",
               yearRange: '-29:+0',
               maxDate: new Date(),
               changeMonth: true,
               changeYear: true,
               dateFormat: "yy-mm-dd",
               showOtherMonths: true
           });
           /*open dropdown*/
           $(".select_box").click(function (event) {
               event.stopPropagation();
               $(".select_option").hide();
               $(this).find(".select_option").toggle();
           });
           /*close dropdown*/
           $(document).click(function () {
               $('.select_option').hide();
           });
           /*set value*/
           $(".select_option li").click(function (event) {
               event.stopPropagation();
               $(".select_option").hide();

               var value = $(this).text();
               $(this).parent().parent().parent().find(".select_value").val(value);
           });



           $(".JobTitle").click(function () {
               $.getJSON('_GetJobDetail', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                   {
                       $('.boxer1_jobinfo').html(data.Html);
                   }
               });
           });

           var location = '<%=Request.QueryString["location"] %>';
           if (location != "") {
               $("#SelectLocation").val(location);
               $("#SelectLocation+.select_value").val(location);
           }

           var func = '<%=Request.QueryString["func"] %>';
           if (func != "") {
               $("#SelectFunction").val(func);
               $("#SelectFunction+.select_value").val(func);
           }


       });

       function search() {
           var location = $(".searchlocation").val();
           var keyword = $("#KeyWord").val();
           var func = $("#SelectFunction").val();
           var date = $("#JobDate").val();
           var education = $("#Education").val();
           var industry = $("#Industry").val().replace("&","__");
           window.location.href = '<%=Url.Action("AdvanceJobSearch") %>' + "?keyword=" + keyword + "&location=" + location + "&func=" + func + "&industry="+industry+"&education="+education+"&date="+date+"&searchtype=Jobs";
           return false;

       }

       
   </script>
<div id="lightboxwrapper">
        
</div>
</asp:Content>


