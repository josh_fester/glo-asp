﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Partner>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Partner
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                GLO Partners
            </div>
            <div class="boxer_content">
                <% foreach (var item in Model)
                   { %>
                <div class="partner_line">
                    <div class="partner_logo">
                        <img src="<%: Html.Raw(item.Image) %>" title="<%: Html.Raw(item.Title) %>" /></div>
                    <div class="partner_tool">
                        <%: Html.Raw(item.Content) %></div>
                    <div class="partner_desc">
                        <%= HttpUtility.HtmlDecode(item.Description)%></div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
