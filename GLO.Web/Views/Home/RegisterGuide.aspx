﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RecommendUsers>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  GLO register guide
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
                 <%if (Request.QueryString["from"] != null)
                   { %>
                    <div class="boxer_headline">
                 Sorry! Only registered users can access GLO survey results and participate in our<br/> surveys online. Kindly join our growing community of good leaders online
                </div>
                <%}
                   else if (Request.QueryString["inviteid"] != null)
                   { %>
                <div class="boxer_headline">
                  Welcome to the GLO community of Good Leaders Online and <br />
                  thank you for recommending <%=ViewBag.NickName%>'s work<br />
                  Please register on GLO and proceed to share <%=ViewBag.NickName%>'s expertise<br/>
                  If you have registered with another email, please click <a href="<%=System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString() %>/Account/RecommenderLogin?inviteid=<%=GLO.Web.Common.DEEncrypt.Encrypt(Request.QueryString["inviteid"]) %>">here</a>.
                </div>
                <%}
                   else
                   { %>
                <div class="boxer_headline">
                Join the GLO community<br />
                connecting good leaders to good business
                </div>
                <%} %>       
                <%if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "1")
                  { %>
                <div class="boxer_content">      
                    <div class="reg_gray"></div> 
                    <%Html.RenderPartial("~/Views/Shared/Leader/RecommendLeader.ascx", Model.Leaders);%>
                    <%Html.RenderPartial("~/Views/Shared/Company/RecommendCompany.ascx", Model.Companies);%>
                    <%Html.RenderPartial("~/Views/Shared/Expert/RecommendExpert.ascx", Model.Experts);%>
                    <div class="contact_us">
                        or simply <a href="/Home/Contact">contact us</a> to inquire about our leadership
                        solutions</div>
                </div>
                <%}
                  else
                  { %>           
                <div class="boxer_content">        
                    <%Html.RenderPartial("~/Views/Shared/Company/RecommendCompany.ascx", Model.Companies);%>
                    <%Html.RenderPartial("~/Views/Shared/Leader/RecommendLeader.ascx", Model.Leaders);%>
                    <%Html.RenderPartial("~/Views/Shared/Expert/RecommendExpert.ascx", Model.Experts);%>
                    <div class="contact_us">
                        or simply <a href="/Home/Contact">contact us</a> to inquire about our leadership
                        solutions</div>
                </div>
                <%} %>

        </div>
    </div>
</asp:Content>


