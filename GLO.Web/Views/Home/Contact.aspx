﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Contact Us
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Contact Us
            </div>
            <div class="boxer_content">
                <div id="ContactForm">
                    <table>
                        <tr>
                            <td style="width:120px;">From</td>
                            <td><input type="text" class="txt_contact" value="Email" id="From" onfocus="if (this.value=='Email')this.value=''" onblur="if (this.value=='')this.value='Email'" /><span class="required">*</span></td>
                        </tr>
                        <tr>
                            <td>Subject</td>
                            <td><input type="text" class="txt_contact" value="Subject" id="Subject" onfocus="if (this.value=='Subject')this.value=''" onblur="if (this.value=='')this.value='Subject'" /><span class="required">*</span></td>
                        </tr>
                        <tr>
                            <td>Memo</td>
                            <td><textarea class="area_contact" id="Content"></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="button" class="bluebtn2" value="Submit" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <%=ViewBag.ContactUs %>
                            </td>
                        </tr>
                    </table>                
                </div>

                <div id="ContactSuccess" style="display:none;">Your message was successfully sent!</div>
            </div>
        </div>
    </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#From").blur(function () {
                    if (!isEmpty($(this).val()) || $(this).val() != "Email") $(this).removeClass("input_error");
                });
                $("#Subject").blur(function () {
                    if (!isEmpty($(this).val()) || $(this).val() != "Subject") $(this).removeClass("input_error");
                });

                $(".bluebtn2").click(function () {
                    var result = true;

                    var from = $('#From').val();
                    var subject = $('#Subject').val();
                    var content = $('#Content').val();

                    if (isEmpty(from) || from == "Email") {
                        $("#From").addClass("input_error");
                        result = false;
                    }
                    else {
                        $("#From").removeClass("input_error");
                    }

                    if (isEmpty(subject) || subject == "Subject") {
                        $("#Subject").addClass("input_error");
                        result = false;
                    }
                    else {
                        $("#Subject").removeClass("input_error");
                    }

                    if (result) {

                        $.post("/Home/ContactUsSendEmail", { From: from, Subject: subject, Content: content }, function (data) {
                            if (data.Result) {
                                $("#ContactForm").hide();
                                $("#ContactSuccess").show();
                            }
                            else {
                                alertbox(data.Message);
                            }
                        }, 'json');
                    }
                    else {
                        return false;
                    }
                });
            });
        </script>
</asp:Content>