﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Solution
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="solution_boxer">
            <div id="navSolution" class="solution_head solution_active1">
                <div class="solution_nav"><a id="navLeader" class="active" href="javascript:void(0);">Solutions for Good Leaders</a></div>
                <div class="solution_nav"><a id="navCompany" class="" href="javascript:void(0);">Solutions for Good Companies</a></div>
            </div>
            <div id="contentLeader" class="solution_content">
                        <div class="solution_column">
                            <div class="solution_leader_head">
                                Developing careers<br />
                                for responsible leaders
                            </div>
                            <div class="solution_leader_note">
                                At GLO, you are not just a ‘virtual’ leader profile<br />
                                but an extraordinary individual we like to know and<br />
                                continue to develop to make a difference.
                            </div>
                            <table>
                                <tr>
                                    <td><img src="/styles/images/offline1.gif" /></td>
                                    <td><img src="/styles/images/wldp1.gif" /></td>
                                    <td><img src="/styles/images/coaching1.gif" /></td>
                                </tr>
                                <tr>
                                    <td><b>Offline Assessment</b></td>
                                    <td><b>WLDP Participation</b></td>
                                    <td><b>Coaching</b></td>
                                </tr>
                                <tr>
                                    <td>Build better self<br />awareness and join<br /> a wise leadership<br /> assessment center</td>
                                    <td>Join our four days<br /> wise leadership<br /> development program<br /> in collaboration with<br /> an ivy league faculty</td>
                                    <td>Team up with our<br /> certified coaches<br /> to develop six core<br /> competencies of<br /> responsible leaders</td>
                                </tr>
                            </table>
                        </div>
                        <div class="solution_column2">
                            <div class="solution_line">
                                As a registered leader member you can freely explore an extensive list of actions to develop your responsible career:
                            </div>
                            <div class="solution_line2">
                                <b>Develop your confidential GLO profile</b><br />
                                Build a unique profile focused on your vision, values and passion.<br />
                                Complete your bespoke online interview, build a graphic CV and connect with a select eight people as character referees.
                            </div>
                            <div class="solution_line2">
                                <b>Find and apply for good jobs</b><br />
                                Search good executive and C-level jobs and apply online.
                            </div>
                            <div class="solution_line2">
                                <b>Connect with good companies</b><br />
                                Review good company profiles, including a view on their work culture and show your interest to be placed in their strategic succession funnel.
                            </div>

                            <div class="solution_line2">
                                <b>Build a target employer funnel</b><br />
                               Save company profiles in your dedicated employer target list and gradually reach out to show your responsible leadership.
                            </div>
                            <div class="solution_line2">
                                <b>Connect with GLO executive search</b><br />
                               Contact the GLO team for a Skype interview and raise your profile.
                            </div>
                            <div class="solution_line2">
                                <b>Connect with GLO certified experts</b><br />
                               View profiles of our GLO certified HR experts including coaches and connect with team to explore developing your leadership skills further.
                            </div>
                              <div class="solution_line2">
                                <b>Take the wise leadership check-up</b><br />
                               Take the wise leadership online check-up and get feedback on five core 'wisdom' related competencies.
                            </div>
                        </div>
            </div>
            <div id="contentCompany" class="solution_content" style="display:none">
                        <div class="solution_column">
                            <table>
                                <tr>
                                    <td><img src="/styles/images/search_user1.gif" /></td>
                                    <td><img src="/styles/images/search_resume1.gif" /></td>
                                </tr>
                                <tr>
                                    <td><b>GLO Executive Search</b></td>
                                    <td><b>GLO Resume Search</b></td>
                                </tr>
                                <tr>
                                    <td>Experience our unique approach <br /> to search assignments focused <br /> on finding responsible leaders <br /> who ensure quality decisions <br /> for your organization</td>
                                    <td>Outsource a resume research<br />for mid level positions <br /> and save time and money <br /> while leveraging our effective <br /> search approach</td>
                                </tr>
                                <tr>
                                    <td><img src="/styles/images/offline1.gif" /></td>
                                    <td><img src="/styles/images/wldp1.gif" /></td>
                                </tr>
                                <tr>
                                    <td><b>Offline Assessment</b></td>
                                    <td><b>WLDP Participation</b></td>
                                </tr>
                                <tr>
                                    <td>Build better self-awareness and<br /> join a wise leadership<br /> assessment center</td>
                                    <td>Have your Executives join our<br /> four days wise leadership<br /> development program in<br /> collaboration with<br /> an ivy league faculty</td>
                                </tr>
                                <tr>
                                    <td><img src="/styles/images/leadership1.gif" /></td>
                                    <td><img src="/styles/images/coaching1.gif" /></td>
                                </tr>
                                <tr>
                                    <td><b>GLO Strategic Leadership Funnel</b></td>
                                    <td><b>Coaching</b></td>
                                </tr>
                                <tr>
                                    <td>Engage the GLO team to build a<br />bespoke strategic leadership<br />succession funnel</td>
                                    <td>Team up with our certified<br /> coaches to develop six <em>wise</em><br /> competencies of your<br /> responsible leaders</td>
                                </tr>
                            </table>
                        </div>
                        <div class="solution_column2">
                            <div class="solution_line">
                                Experience our qualitative, time and cost saving approach to leadership search and development:
                            </div>
                            <div class="solution_line2">
                                <b>List a job</b><br />
                                Build your own job ads using a dedicated member dashboard online.<br />
                                Change jobs at any time free of change within your selected membership package quota.
                            </div>
                            <div class="solution_line2">
                                <b>Build your company profile</b><br />
                               Advertise your unique values and team culture and be visible to responsible leaders that share your vision and would like to add value.
                            </div>
                            <div class="solution_line2">
                                <b>Find a good leader</b><br />
                               Search profiles of our leader members and make fast assessments based on graphic CV representation. Save precious time by reviewing a leader's online interview and available wise leadership check-up info.
                            </div>
                            <div class="solution_line2">
                                <b>Build a function dedicated leadership funnel</b><br />
                                Tag leaders in you function dedicated strategic leadership funnel and build a succession plan for immediate contact when the need arises.
                            </div>
                             <div class="solution_line2">
                                <b>Manage your search</b><br />
                                Initiate an executive search or follow-up on open assignments via  your company profile dashboard.
                            </div>
                             <div class="solution_line2">
                                <b>Connect with GLO certified experts</b><br />
                                Search and contact GLO's pool of certified HR experts to assist coaching leadership and organizational development of you team.
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#navLeader').click(function () {
                $('#navSolution').removeClass('solution_active2');
                $('#navSolution').addClass('solution_active1');

                $('.solution_nav').find('a').removeClass('active');
                $(this).addClass('active');

                $('#contentCompany').hide();
                $('#contentLeader').show();
            });
            $('#navCompany').click(function () {
                $('#navSolution').removeClass('solution_active1');
                $('#navSolution').addClass('solution_active2');

                $('.solution_nav').find('a').removeClass('active');
                $(this).addClass('active');

                $('#contentLeader').hide();
                $('#contentCompany').show();
            });
        });
    </script>
</asp:Content>