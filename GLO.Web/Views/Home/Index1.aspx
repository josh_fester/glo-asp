﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page - My ASP.NET MVC Application
   
</asp:Content>

<asp:Content ID="indexFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
 <style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 225px; }
    #sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; width: 100px; height: 90px; font-size: 4em; text-align: center; cursor:pointer; }
    </style>
   GLO Navigation
<span><%= Context.User.Identity.Name%></span> <br />
IsAuthenticated: <%= Request.IsAuthenticated %> <br />
<%= Context.User.GetType().ToString() %> <br />
<%= Context.User.Identity.GetType().ToString() %> <br />
<br />
<br />
<hr />

<% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;  %>
<% if( user != null ) { %>
	<%= user.UserData.ToString()%>
    <%= user.UserData.UserName%>
    <% if (user.UserData.UserType == 0)
       {%>
       <%=Html.ActionLink("My profile","Index","Leader") %>
    <%}
       else if (user.UserData.UserType == 1)
       {%>
     <%=Html.ActionLink("My profile", "Index", "Expert")%>
    <%} else { %>
     <%=Html.ActionLink("My profile","Index","Company") %>

    <%} %>

<% } %>

<br />
    <script type="text/javascript">
        $(function () {
            $("#sortable").sortable();
            $("#sortable").disableSelection();
        });
    </script>

<ul id="sortable">
    <li class="ui-state-default"><img width="99px" alt="image" src="/images/5b18a41b-05b2-48a1-900d-b940c6b4c2a1.png"/></li>
    <li class="ui-state-default">2</li>
    <li class="ui-state-default"><img width="99px" alt="image" src="/images/5b18a41b-05b2-48a1-900d-b940c6b4c2a1.png"/></li>
    <li class="ui-state-default">4</li>

</ul>


</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
  GLO Index page
    <input id="btnTest" type="button" value="button" />
     <script language="javascript" type="text/javascript">
         $(document).ready(function () {
             $("#btnTest1").click(function () {
                 var TagName = "1";
                 var TagDestination = "2";
                 $.postJSON('<%=Url.Action("AddTag") %>', { "tagName": TagName, "tagDestination": TagDestination }, function (data) {
                     var row = "<li><a class='jqModel EditTag' href='#' Name='" + data.Name + "' Destination='" + data.Destination + "'>" + data.Name + "</a></li>";
                 });
             });


             $("#btnTest").click(function () {

                 var newdata = { 'OldPassword': ''
                            , 'NewPassword': ''
                            , 'ConfirmPassword': ''
                 };
                 newdata.OldPassword = "1111";
                 newdata.NewPassword = "2222";
                 newdata.ConfirmPassword = "333";

                 $.postJSONnoQS('<%=Url.Action("Update","Account") %>', { data: $.toJSON(newdata) }, function (data) {
                     alertbox("Register Success");
                 });



             });

         });
    </script>
    </form>
</asp:Content>
