﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyJob
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
        <div class="nav_company nav_company_job">
            <ul>
                <li><a href="/Sample/CompanyIndex">Life At</a></li>
                <li><a href="/Sample/FactSheet">Fact Sheet</a></li>
                <li class="active"><a href="/Sample/CompanyJob">Jobs</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
       <div class="manage_company_job">
            <div class="headsearch">
                        <table class="search_item">
                            <tbody><tr>
                                <td class="item_headline" colspan="2">
                                    Custom job search
                                </td>
                            </tr>
                            <tr>
                                <td class="item_title">
                                    Keyword
                                </td>
                                <td class="item_option">
                                    <input type="text" value="" class="keyword" id="KeyWord">
                                </td>
                            </tr>
                          
                            <tr>
                                <td class="item_title">
                                    Location
                                </td>
                                <td class="item_option">
                                    <div class="multiple_box">
                                        <input type="text" value="All Location" readonly="readonly" id="SelectLocation" class="multiple_value SelectLocation">
                                        <img src="/Styles/images/icon_arrow2.gif" class="multiple_arrow">
                                        <div class="multiple_wrapper">
                                            

<div class="multiple_location">
 <div class="location_left">
      <h2>Region List</h2>
      <div class="location_list">
      
            <a x:countryid="1" class="current" href="javascript:void(0);">China/Hong Kong/Taiwan </a>
            
            <a x:countryid="148" class="" href="javascript:void(0);">Asia</a>
            
            <a x:countryid="149" class="" href="javascript:void(0);">Middle East/North Africa</a>
            
            <a x:countryid="150" class="" href="javascript:void(0);">Europe</a>
            
            <a x:countryid="151" class="" href="javascript:void(0);">North America</a>
            
            <a x:countryid="152" class="" href="javascript:void(0);">Central America and the Caribbean</a>
            
            <a x:countryid="153" class="" href="javascript:void(0);">South America</a>
            
            <a x:countryid="154" class="" href="javascript:void(0);">Sub Saharan Africa</a>
            
            <a x:countryid="155" class="" href="javascript:void(0);">Oceania</a>
            
                                                   
      </div>
  </div>
  <div class="location_right">
      <h2>Country / City List</h2>
      <div class="location_list">
      
          <table style="display:table" id="country1" class="location_item">

              <tbody><tr>
                    <td><a x:id="43" href="javascript:void(0);">BeiJing</a></td>
                  
                    <td><a x:id="44" href="javascript:void(0);">TianJin</a></td>
                  
                    <td><a x:id="45" href="javascript:void(0);">ShangHai</a></td>
                  </tr><tr>
                    <td><a x:id="46" href="javascript:void(0);">ChongQing</a></td>
                  
                    <td><a x:id="47" href="javascript:void(0);">HeBei</a></td>
                  
                    <td><a x:id="48" href="javascript:void(0);">ShanXi</a></td>
                  </tr><tr>
                    <td><a x:id="49" href="javascript:void(0);">LiaoNing</a></td>
                  
                    <td><a x:id="50" href="javascript:void(0);">Jilin</a></td>
                  
                    <td><a x:id="51" href="javascript:void(0);">HeiLongJiang</a></td>
                  </tr><tr>
                    <td><a x:id="52" href="javascript:void(0);">JiangSu</a></td>
                  
                    <td><a x:id="53" href="javascript:void(0);">ZheJiang</a></td>
                  
                    <td><a x:id="54" href="javascript:void(0);">AnHui</a></td>
                  </tr><tr>
                    <td><a x:id="55" href="javascript:void(0);">FuJian</a></td>
                  
                    <td><a x:id="56" href="javascript:void(0);">JiangXi</a></td>
                  
                    <td><a x:id="57" href="javascript:void(0);">ShanDong</a></td>
                  </tr><tr>
                    <td><a x:id="58" href="javascript:void(0);">HeNan</a></td>
                  
                    <td><a x:id="59" href="javascript:void(0);">HuBei</a></td>
                  
                    <td><a x:id="60" href="javascript:void(0);">HuNan</a></td>
                  </tr><tr>
                    <td><a x:id="61" href="javascript:void(0);">GuangDong</a></td>
                  
                    <td><a x:id="62" href="javascript:void(0);">HaiNan</a></td>
                  
                    <td><a x:id="63" href="javascript:void(0);">SiChuan</a></td>
                  </tr><tr>
                    <td><a x:id="64" href="javascript:void(0);">GuiZhou</a></td>
                  
                    <td><a x:id="65" href="javascript:void(0);">YunNan</a></td>
                  
                    <td><a x:id="66" href="javascript:void(0);">ShanXi</a></td>
                  </tr><tr>
                    <td><a x:id="67" href="javascript:void(0);">GanSu</a></td>
                  
                    <td><a x:id="68" href="javascript:void(0);">QingHai</a></td>
                  
                    <td><a x:id="69" href="javascript:void(0);">Tibet</a></td>
                  </tr><tr>
                    <td><a x:id="70" href="javascript:void(0);">GuangXi</a></td>
                  
                    <td><a x:id="71" href="javascript:void(0);">Inner Mongolia</a></td>
                  
                    <td><a x:id="72" href="javascript:void(0);">NingXia</a></td>
                  </tr><tr>
                    <td><a x:id="73" href="javascript:void(0);">Sinkiang</a></td>
                  
                    <td><a x:id="74" href="javascript:void(0);">HongKong</a></td>
                  
                    <td><a x:id="75" href="javascript:void(0);">MaCao</a></td>
                  </tr><tr>
                    <td><a x:id="76" href="javascript:void(0);">TaiWan</a></td>
                  
                    <td><a x:id="77" href="javascript:void(0);">Diaoyu Islands</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country148" class="location_item">

              <tbody><tr>
                    <td><a x:id="156" href="javascript:void(0);">Bangladesh</a></td>
                  
                    <td><a x:id="157" href="javascript:void(0);">Bhutan</a></td>
                  
                    <td><a x:id="158" href="javascript:void(0);">Brunei</a></td>
                  </tr><tr>
                    <td><a x:id="162" href="javascript:void(0);">Cambodia</a></td>
                  
                    <td><a x:id="159" href="javascript:void(0);">India</a></td>
                  
                    <td><a x:id="160" href="javascript:void(0);">Indonesia</a></td>
                  </tr><tr>
                    <td><a x:id="161" href="javascript:void(0);">Japan</a></td>
                  
                    <td><a x:id="163" href="javascript:void(0);">Kazakhstan</a></td>
                  
                    <td><a x:id="164" href="javascript:void(0);">North Korea</a></td>
                  </tr><tr>
                    <td><a x:id="165" href="javascript:void(0);">South Korea</a></td>
                  
                    <td><a x:id="166" href="javascript:void(0);">Kyrgyzstan</a></td>
                  
                    <td><a x:id="167" href="javascript:void(0);">Laos</a></td>
                  </tr><tr>
                    <td><a x:id="168" href="javascript:void(0);">Malaysia</a></td>
                  
                    <td><a x:id="169" href="javascript:void(0);">Maldives</a></td>
                  
                    <td><a x:id="170" href="javascript:void(0);">Mongolia</a></td>
                  </tr><tr>
                    <td><a x:id="171" href="javascript:void(0);">Myanmar</a></td>
                  
                    <td><a x:id="172" href="javascript:void(0);">Nepal</a></td>
                  
                    <td><a x:id="173" href="javascript:void(0);">Philippines</a></td>
                  </tr><tr>
                    <td><a x:id="174" href="javascript:void(0);">Singapore</a></td>
                  
                    <td><a x:id="175" href="javascript:void(0);">Sri Lanka</a></td>
                  
                    <td><a x:id="176" href="javascript:void(0);">Tajikistan</a></td>
                  </tr><tr>
                    <td><a x:id="177" href="javascript:void(0);">Thailand</a></td>
                  
                    <td><a x:id="178" href="javascript:void(0);">Turkmenistan</a></td>
                  
                    <td><a x:id="179" href="javascript:void(0);">Uzbekistan</a></td>
                  </tr><tr>
                    <td><a x:id="180" href="javascript:void(0);">Vietnam</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country149" class="location_item">

              <tbody><tr>
                    <td><a x:id="181" href="javascript:void(0);">Afghanistan</a></td>
                  
                    <td><a x:id="182" href="javascript:void(0);">Algeria</a></td>
                  
                    <td><a x:id="183" href="javascript:void(0);">Azerbaijan</a></td>
                  </tr><tr>
                    <td><a x:id="184" href="javascript:void(0);">Bahrain</a></td>
                  
                    <td><a x:id="185" href="javascript:void(0);">Egypt</a></td>
                  
                    <td><a x:id="186" href="javascript:void(0);">Iran</a></td>
                  </tr><tr>
                    <td><a x:id="187" href="javascript:void(0);">Iraq</a></td>
                  
                    <td><a x:id="188" href="javascript:void(0);">Israel</a></td>
                  
                    <td><a x:id="189" href="javascript:void(0);">Jordan</a></td>
                  </tr><tr>
                    <td><a x:id="190" href="javascript:void(0);">Kuwait</a></td>
                  
                    <td><a x:id="191" href="javascript:void(0);">Lebanon</a></td>
                  
                    <td><a x:id="192" href="javascript:void(0);">Libya</a></td>
                  </tr><tr>
                    <td><a x:id="193" href="javascript:void(0);">Morocco</a></td>
                  
                    <td><a x:id="194" href="javascript:void(0);">Oman</a></td>
                  
                    <td><a x:id="195" href="javascript:void(0);">Pakistan</a></td>
                  </tr><tr>
                    <td><a x:id="196" href="javascript:void(0);">Qatar</a></td>
                  
                    <td><a x:id="197" href="javascript:void(0);">Saudi Arabia</a></td>
                  
                    <td><a x:id="198" href="javascript:void(0);">Somalia</a></td>
                  </tr><tr>
                    <td><a x:id="199" href="javascript:void(0);">Syria</a></td>
                  
                    <td><a x:id="200" href="javascript:void(0);">Tunisia</a></td>
                  
                    <td><a x:id="201" href="javascript:void(0);">Turkey</a></td>
                  </tr><tr>
                    <td><a x:id="202" href="javascript:void(0);">United Arab Emirates</a></td>
                  
                    <td><a x:id="203" href="javascript:void(0);">Yemen</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country150" class="location_item">

              <tbody><tr>
                    <td><a x:id="204" href="javascript:void(0);">Albania</a></td>
                  
                    <td><a x:id="205" href="javascript:void(0);">Andorra</a></td>
                  
                    <td><a x:id="206" href="javascript:void(0);">Armenia</a></td>
                  </tr><tr>
                    <td><a x:id="207" href="javascript:void(0);">Austria</a></td>
                  
                    <td><a x:id="208" href="javascript:void(0);">Belarus</a></td>
                  
                    <td><a x:id="209" href="javascript:void(0);">Belgium</a></td>
                  </tr><tr>
                    <td><a x:id="210" href="javascript:void(0);">Bosnia and Herzegovina</a></td>
                  
                    <td><a x:id="211" href="javascript:void(0);">Bulgaria</a></td>
                  
                    <td><a x:id="212" href="javascript:void(0);">Croatia</a></td>
                  </tr><tr>
                    <td><a x:id="213" href="javascript:void(0);">Cyprus</a></td>
                  
                    <td><a x:id="214" href="javascript:void(0);">Czech Republic</a></td>
                  
                    <td><a x:id="215" href="javascript:void(0);">Denmark</a></td>
                  </tr><tr>
                    <td><a x:id="216" href="javascript:void(0);">Estonia</a></td>
                  
                    <td><a x:id="217" href="javascript:void(0);">Finland</a></td>
                  
                    <td><a x:id="218" href="javascript:void(0);">France</a></td>
                  </tr><tr>
                    <td><a x:id="219" href="javascript:void(0);">Georgia</a></td>
                  
                    <td><a x:id="220" href="javascript:void(0);">Germany</a></td>
                  
                    <td><a x:id="221" href="javascript:void(0);">Greece</a></td>
                  </tr><tr>
                    <td><a x:id="222" href="javascript:void(0);">Hungary</a></td>
                  
                    <td><a x:id="223" href="javascript:void(0);">Iceland</a></td>
                  
                    <td><a x:id="224" href="javascript:void(0);">Ireland</a></td>
                  </tr><tr>
                    <td><a x:id="225" href="javascript:void(0);">Italy</a></td>
                  
                    <td><a x:id="226" href="javascript:void(0);">Kosovo</a></td>
                  
                    <td><a x:id="227" href="javascript:void(0);">Latvia</a></td>
                  </tr><tr>
                    <td><a x:id="228" href="javascript:void(0);">Liechtenstein</a></td>
                  
                    <td><a x:id="229" href="javascript:void(0);">Lithuania</a></td>
                  
                    <td><a x:id="230" href="javascript:void(0);">Luxembourg</a></td>
                  </tr><tr>
                    <td><a x:id="231" href="javascript:void(0);">Macedonia</a></td>
                  
                    <td><a x:id="232" href="javascript:void(0);">Malta</a></td>
                  
                    <td><a x:id="233" href="javascript:void(0);">Moldova</a></td>
                  </tr><tr>
                    <td><a x:id="234" href="javascript:void(0);">Monaco</a></td>
                  
                    <td><a x:id="235" href="javascript:void(0);">Montenegro</a></td>
                  
                    <td><a x:id="236" href="javascript:void(0);">Netherlands</a></td>
                  </tr><tr>
                    <td><a x:id="237" href="javascript:void(0);">Norway</a></td>
                  
                    <td><a x:id="238" href="javascript:void(0);">Poland</a></td>
                  
                    <td><a x:id="239" href="javascript:void(0);">Portugal</a></td>
                  </tr><tr>
                    <td><a x:id="240" href="javascript:void(0);">Romania</a></td>
                  
                    <td><a x:id="241" href="javascript:void(0);">Russia</a></td>
                  
                    <td><a x:id="242" href="javascript:void(0);">San Marino</a></td>
                  </tr><tr>
                    <td><a x:id="243" href="javascript:void(0);">Serbia</a></td>
                  
                    <td><a x:id="244" href="javascript:void(0);">Slovakia</a></td>
                  
                    <td><a x:id="245" href="javascript:void(0);">Slovenia</a></td>
                  </tr><tr>
                    <td><a x:id="246" href="javascript:void(0);">Spain</a></td>
                  
                    <td><a x:id="247" href="javascript:void(0);">Sweden</a></td>
                  
                    <td><a x:id="248" href="javascript:void(0);">Switzerland</a></td>
                  </tr><tr>
                    <td><a x:id="249" href="javascript:void(0);">Ukraine</a></td>
                  
                    <td><a x:id="250" href="javascript:void(0);">United Kingdom</a></td>
                  
                    <td><a x:id="251" href="javascript:void(0);">Vatican City</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country151" class="location_item">

              <tbody><tr>
                    <td><a x:id="252" href="javascript:void(0);">Canada</a></td>
                  
                    <td><a x:id="253" href="javascript:void(0);">Greenland</a></td>
                  
                    <td><a x:id="254" href="javascript:void(0);">Mexico</a></td>
                  </tr><tr>
                    <td><a x:id="255" href="javascript:void(0);">United States of America</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country152" class="location_item">

              <tbody><tr>
                    <td><a x:id="256" href="javascript:void(0);">Antigua and Barbuda</a></td>
                  
                    <td><a x:id="257" href="javascript:void(0);">The Bahamas</a></td>
                  
                    <td><a x:id="258" href="javascript:void(0);">Barbados</a></td>
                  </tr><tr>
                    <td><a x:id="259" href="javascript:void(0);">Belize</a></td>
                  
                    <td><a x:id="260" href="javascript:void(0);">Costa Rica</a></td>
                  
                    <td><a x:id="261" href="javascript:void(0);">Cuba</a></td>
                  </tr><tr>
                    <td><a x:id="262" href="javascript:void(0);">Dominica</a></td>
                  
                    <td><a x:id="263" href="javascript:void(0);">Dominican Republic</a></td>
                  
                    <td><a x:id="264" href="javascript:void(0);">El Salvador</a></td>
                  </tr><tr>
                    <td><a x:id="265" href="javascript:void(0);">Grenada</a></td>
                  
                    <td><a x:id="266" href="javascript:void(0);">Guatemala</a></td>
                  
                    <td><a x:id="267" href="javascript:void(0);">Haiti</a></td>
                  </tr><tr>
                    <td><a x:id="268" href="javascript:void(0);">Honduras</a></td>
                  
                    <td><a x:id="269" href="javascript:void(0);">Jamaica</a></td>
                  
                    <td><a x:id="270" href="javascript:void(0);">Nicaragua</a></td>
                  </tr><tr>
                    <td><a x:id="271" href="javascript:void(0);">Panama</a></td>
                  
                    <td><a x:id="272" href="javascript:void(0);">Saint Kitts and Nevis</a></td>
                  
                    <td><a x:id="273" href="javascript:void(0);">Saint Lucia</a></td>
                  </tr><tr>
                    <td><a x:id="274" href="javascript:void(0);">Saint Vincent and the Grenadines</a></td>
                  
                    <td><a x:id="275" href="javascript:void(0);">Trinidad and Tobago</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country153" class="location_item">

              <tbody><tr>
                    <td><a x:id="276" href="javascript:void(0);">Argentina</a></td>
                  
                    <td><a x:id="277" href="javascript:void(0);">Bolivia</a></td>
                  
                    <td><a x:id="278" href="javascript:void(0);">Brazil</a></td>
                  </tr><tr>
                    <td><a x:id="279" href="javascript:void(0);">Chile</a></td>
                  
                    <td><a x:id="280" href="javascript:void(0);">Colombia</a></td>
                  
                    <td><a x:id="281" href="javascript:void(0);">Ecuador</a></td>
                  </tr><tr>
                    <td><a x:id="282" href="javascript:void(0);">Guyana</a></td>
                  
                    <td><a x:id="283" href="javascript:void(0);">Paraguay</a></td>
                  
                    <td><a x:id="284" href="javascript:void(0);">Peru</a></td>
                  </tr><tr>
                    <td><a x:id="285" href="javascript:void(0);">Suriname</a></td>
                  
                    <td><a x:id="286" href="javascript:void(0);">Uruguay</a></td>
                  
                    <td><a x:id="287" href="javascript:void(0);">Venezuela</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country154" class="location_item">

              <tbody><tr>
                    <td><a x:id="288" href="javascript:void(0);">Angola</a></td>
                  
                    <td><a x:id="289" href="javascript:void(0);">Benin</a></td>
                  
                    <td><a x:id="290" href="javascript:void(0);">Botswana</a></td>
                  </tr><tr>
                    <td><a x:id="291" href="javascript:void(0);">Burkina Faso</a></td>
                  
                    <td><a x:id="292" href="javascript:void(0);">Burundi</a></td>
                  
                    <td><a x:id="293" href="javascript:void(0);">Cameroon</a></td>
                  </tr><tr>
                    <td><a x:id="294" href="javascript:void(0);">Cape Verde</a></td>
                  
                    <td><a x:id="295" href="javascript:void(0);">Central African Republic</a></td>
                  
                    <td><a x:id="296" href="javascript:void(0);">Chad</a></td>
                  </tr><tr>
                    <td><a x:id="297" href="javascript:void(0);">Comoros</a></td>
                  
                    <td><a x:id="298" href="javascript:void(0);">Republic of the Congo</a></td>
                  
                    <td><a x:id="299" href="javascript:void(0);">Democratic Republic of the Congo</a></td>
                  </tr><tr>
                    <td><a x:id="300" href="javascript:void(0);">Cote d'Ivoire</a></td>
                  
                    <td><a x:id="301" href="javascript:void(0);">Djibouti</a></td>
                  
                    <td><a x:id="302" href="javascript:void(0);">Equatorial Guinea</a></td>
                  </tr><tr>
                    <td><a x:id="303" href="javascript:void(0);">Eritrea</a></td>
                  
                    <td><a x:id="304" href="javascript:void(0);">Ethiopia</a></td>
                  
                    <td><a x:id="305" href="javascript:void(0);">Gabon</a></td>
                  </tr><tr>
                    <td><a x:id="306" href="javascript:void(0);">The Gambia</a></td>
                  
                    <td><a x:id="307" href="javascript:void(0);">Ghana</a></td>
                  
                    <td><a x:id="308" href="javascript:void(0);">Guinea</a></td>
                  </tr><tr>
                    <td><a x:id="309" href="javascript:void(0);">Guinea-Bissau</a></td>
                  
                    <td><a x:id="310" href="javascript:void(0);">Kenya</a></td>
                  
                    <td><a x:id="336" href="javascript:void(0);">Lesotho</a></td>
                  </tr><tr>
                    <td><a x:id="312" href="javascript:void(0);">Liberia</a></td>
                  
                    <td><a x:id="313" href="javascript:void(0);">Madagascar</a></td>
                  
                    <td><a x:id="314" href="javascript:void(0);">Malawi</a></td>
                  </tr><tr>
                    <td><a x:id="315" href="javascript:void(0);">Mali</a></td>
                  
                    <td><a x:id="316" href="javascript:void(0);">Mauritania</a></td>
                  
                    <td><a x:id="317" href="javascript:void(0);">Mauritius</a></td>
                  </tr><tr>
                    <td><a x:id="318" href="javascript:void(0);">Mozambique</a></td>
                  
                    <td><a x:id="319" href="javascript:void(0);">Namibia</a></td>
                  
                    <td><a x:id="320" href="javascript:void(0);">Niger</a></td>
                  </tr><tr>
                    <td><a x:id="321" href="javascript:void(0);">Nigeria</a></td>
                  
                    <td><a x:id="322" href="javascript:void(0);">Rwanda</a></td>
                  
                    <td><a x:id="323" href="javascript:void(0);">Sao Tome and Principe</a></td>
                  </tr><tr>
                    <td><a x:id="324" href="javascript:void(0);">Senegal</a></td>
                  
                    <td><a x:id="325" href="javascript:void(0);">Seychelles</a></td>
                  
                    <td><a x:id="326" href="javascript:void(0);">Sierra Leone</a></td>
                  </tr><tr>
                    <td><a x:id="327" href="javascript:void(0);">South Africa</a></td>
                  
                    <td><a x:id="328" href="javascript:void(0);">South Sudan</a></td>
                  
                    <td><a x:id="329" href="javascript:void(0);">Sudan</a></td>
                  </tr><tr>
                    <td><a x:id="330" href="javascript:void(0);">Swaziland</a></td>
                  
                    <td><a x:id="331" href="javascript:void(0);">Tanzania</a></td>
                  
                    <td><a x:id="332" href="javascript:void(0);">Togo</a></td>
                  </tr><tr>
                    <td><a x:id="333" href="javascript:void(0);">Uganda</a></td>
                  
                    <td><a x:id="334" href="javascript:void(0);">Zambia</a></td>
                  
                    <td><a x:id="335" href="javascript:void(0);">Zimbabwe</a></td>
                  </tr>  
                    
         </tbody></table>        
        
          <table style="" id="country155" class="location_item">

              <tbody><tr>
                    <td><a x:id="337" href="javascript:void(0);">Australia</a></td>
                  
                    <td><a x:id="338" href="javascript:void(0);">East Timor</a></td>
                  
                    <td><a x:id="339" href="javascript:void(0);">Fiji</a></td>
                  </tr><tr>
                    <td><a x:id="340" href="javascript:void(0);">Kiribati</a></td>
                  
                    <td><a x:id="341" href="javascript:void(0);">Marshall Islands</a></td>
                  
                    <td><a x:id="342" href="javascript:void(0);">Federated States of Micronesia</a></td>
                  </tr><tr>
                    <td><a x:id="343" href="javascript:void(0);">Nauru</a></td>
                  
                    <td><a x:id="344" href="javascript:void(0);">New Zealand</a></td>
                  
                    <td><a x:id="345" href="javascript:void(0);">Palau</a></td>
                  </tr><tr>
                    <td><a x:id="346" href="javascript:void(0);">Papua New Guinea</a></td>
                  
                    <td><a x:id="347" href="javascript:void(0);">Samoa</a></td>
                  
                    <td><a x:id="348" href="javascript:void(0);">Solomon Islands</a></td>
                  </tr><tr>
                    <td><a x:id="349" href="javascript:void(0);">Tonga</a></td>
                  
                    <td><a x:id="350" href="javascript:void(0);">Tuvalu</a></td>
                  
                    <td><a x:id="351" href="javascript:void(0);">Vanuatu</a></td>
                  </tr>  
                    
         </tbody></table>        
        
      </div>
  </div>
  <div class="me_close"><a href="javascript:void(0);" class="LocationClose"><img src="/Styles/images/icon_close.gif"></a></div>  
</div>

                                        </div>    
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="item_title">
                                    Job Function
                                </td>
                                <td class="item_option">
                                    <div class="select_box">
                                        <input type="text" value="All Function" readonly="readonly" id="SelectFunction" class="select_value">
                                        <img src="/Styles/images/icon_arrow2.gif" class="select_arrow">
                                        <div class="select_option">
                                            <ul>
                                                <li>All Function</li>
                                                
                                                <li>General Manager</li>
                                                
                                                <li>Plant Manager</li>
                                                
                                                <li>Operations Manager</li>
                                                
                                                <li>Marketing Manager</li>
                                                
                                                <li>Logistics Manager</li>
                                                
                                                <li>Finance Manager</li>
                                                
                                                <li>Accounting Manager</li>
                                                
                                                <li>HR Manager</li>
                                                
                                                <li>Managing Director</li>
                                                
                                                <li>Country Manager</li>
                                                
                                                <li>Procurement Manager</li>
                                                
                                                <li>IT Manager</li>
                                                
                                                <li>CEO</li>
                                                
                                                <li>CFO</li>
                                                
                                                <li>COO</li>
                                                
                                                <li>Controller</li>
                                                
                                                <li>Consultant</li>
                                                
                                                <li>Regional Manager</li>
                                                
                                                <li>Vice President</li>
                                                
                                                <li>President</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="button" onclick="return search();" class="btn_search" value="Search">
                                </td>
                            </tr>
                        </tbody></table>
                        
                        <div class="result_title">
                            We found 1 Good Jobs.
                        </div>
                            <div class="result_container">
                                
                                <table class="result">
                                    
                                    <tbody><tr>
                                        <td>
                                            <a class="JobTitle" x:jobid="27" href="javascript:void(0)">
                                                GLO Talent Connector</a>
                                        </td>
                                        <td>
                                            ShangHai
                                        </td>
                                    </tr>
                                    
                                </tbody></table>
                                
                            </div>
            
            </div>
            
           <div class="headsearch_jobs">
           
<table class="ganeral">      
  <tbody><tr>
    <td class="ganeral_title" colspan="2">GLO Talent Connector</td>
    </tr>
    <tr>
    <td>
        <table>
            <tbody><tr>
                <td>Company:</td>
                <td><a target="_blank" href="/company/FactSheet?companyid=504" class="font">Good Leaders Online (GLO)</a></td>
            </tr>
            <tr>
                <td>Job ID:</td>
                <td><span class="gray font">847366</span></td>
            </tr>
           
            <tr>
                <td>Location:</td>
                <td><span class="gray font">ShangHai</span></td>
            </tr>
            <tr>
                <td>Industry:</td>
                <td><span class="gray font">Executive Search</span></td>
            </tr>
            <tr>
                <td>Function:</td>
                <td><span class="gray font">Other</span></td>
            </tr>
             
            <tr>
                <td>Posted:</td>
                <td><span class="gray font">2013-06-04</span></td>
            </tr>
            
            <tr>
                <td>Salary:</td>
                <td><span class="gray font">100,000 - 150,000 RMB net salary per year </span></td>
            </tr>
            
        </tbody></table>
    </td>
    <td width="200" class="ganeral_logo">
        <a target="_blank" href="/company/FactSheet?companyid=504"><img src="/Images/c1860095-de1e-476d-aa3a-e9d54da185a2.gif"></a>
    </td>
    </tr>
</tbody></table>
<table class="detail">
    <tbody><tr>
        <td>
                      
                        
                        <div class="info_headline">Job Summary</div>
                        <div class="info_description font">Develop new business with intro of exec search platform to new clients for mid level management positions and project management of assignments</div>
                        

                        <div class="info_headline">Key Responsibilities</div>
                        <div class="info_description font">Business development<br>Project management of search assignments:<br>-Summary of client’s requirements and preparation of talent profile<br>- Guidance of researcher to identify right target candidate base<br>- Conduct candidate interviews (both phone and face-to-face)<br>- Ensure internal quality processes are correctly applied and documented<br>- Ensure payment collection<br>- Ensure quality of placements has priority over quantity to reach GLO’s objective to be trusted partner of choice for leadership selection</div>
                        <div class="info_headline">Key Competencies</div>
                        <div class="info_description font">-Minimum 3 years of HR or line management experience, with ideally specialization in finance, industrial, automotive or pharma/healthcare sector.<br>-Strong interest in leadership development, assessment, networking<br>-Ability to conduct leader interviews and interact professionally on B (mid management)-level with clients<br>-Strong listening skills and eager to ‘help’ people <br>-Work experience in social impact ventures, charity or leadership development platforms a strong plus<br>-Native Mandarin speaker with good knowledge of English<br>-High integrity and team player<br>-Motivated by the opportunity to make a difference with GLO’s solutions for the client and society at large<br>-High energy level and commitment to excellence<br>-Motivated to work in a multi-cultural, fun and high energy startup team with flexible work attitude   </div>

                        
                        <div class="info_headline">Education</div>
                        <div class="info_description font">Bachelor, Master preferred </div>
                        
                        <div class="info_headline">Contact Person</div>
                        <div class="info_description font">gracewang@glo-china.com</div>
                        
                            <div class="info_headline">GLO vision and values</div>
                            <div class="info_description font">The GLO team serves our community driven by a passion to develop careers of responsible leaders and a vision to building the leading global community of good leaders assuring good business<br>GLO employees understand that commitment to excellence, integrity and consideration of the good of society at large are essential values to achieve GLOs vision and purpose<br><br>Joining GLO is an opportunity to work with purpose, passion and to celebrate successes as we realize our vision</div>
                        
        </td>
        <td>
        </td>
    </tr>
</tbody></table>

           </div>  
        </div>
        <div class="manage_footer2"></div>   
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>