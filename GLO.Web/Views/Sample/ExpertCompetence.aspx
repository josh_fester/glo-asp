﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExpertCompetence
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_expert nav_expert_competence">
        		<ul>
                <li class="userinfo"></li>
            	<li><a href="/Sample/ExpertValue">How I Add Value</a></li>
            	<li><a href="/Sample/ExpertCredential">My Credentials</a></li>
            	<li  class="active"><a href="/Sample/ExpertCompetence">My Competencies</a></li>
            	<li><a href="/Sample/ExpertRecommendation">Recommendations</a></li>
            	<li class="dashboard">
                
                </li>
            </ul>
        </div>
       <div class="manage_boxer">        
               
  <div class="boxer_user">  		
    <div class=" box_user_private_view  "></div>

    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/e2962976-c468-4265-84ed-6634692cbcb5.jpg">
    </div>
    
        <div class="box_user_button">        
            
                    <input type="button" onclick="return false;" value="Tag" class="graybtn">
                
            <input type="button" onclick="return false;" value="Request Tag" class="graybtn">
                
           
        </div>
     
     </div>
    <div class="box_user_name">Raf Adams</div> 
    <div class="box_user_title">The Suited Monk</div>
    <div class="box_user_local">ShangHai</div>
    <div class="box_user_industry">Training</div>
    <div class="box_user_note">I'd like to see a world in which people are ''awake'', are able to reach their full potential in work and life. By full potential I mean spiritually rich and have aligned their Suit (external) and Monk (Internal) world.</div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <div class="value_item_active font">Integrity</div><div class="value_item_active font">Non Judgmental</div><div class="value_item_active font">Respect</div>

    </div>
    <div class="me_edit2">
    
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#TagUser").click(function () {
            $.getJSON('/Tag/GetTagCategory', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('/Message/SendMessageToUser', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#RequestRecommend").click(function () {
            $.postJSON('/Expert/RequestRecommend', { requestUserID: '538' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });

    function RequestTag(id) {
        $.post('/Tag/RequestTag', { requestUserID: id }, function (data) {
            if (data) {
                alertbox("Your request has been successfully sent!");
            }
        });
    }
</script> 
           <div class="boxer_info">          
                 


<div class="competence_wrap">

    <div style="width: 218px; left: 22px; top: 426px; z-index: 109;" class="employment">
            <div class="timeline_view showtimeline" style="display: block; left: -31px;">
                <div class="timeline_arrow2" style="left: 115px;"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    Weiss-Rohlig
                    <br>
                    Sales
                    <br>
                    Logistics
                    <br>
                    2004-06-23 to 2008-06-18
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font"></div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font"></div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font"></div>

                 
            </div>     
            </div>         
      </div>
    
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">1984</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1985</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1986</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1987</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1988</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1989</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1990</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1991</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1992</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    box_end ">
            <div class="timeline_title">1993</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">1994</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1995</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1996</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1997</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1998</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1999</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2000</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2001</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2002</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    box_end ">
            <div class="timeline_title">2003</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">2004</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2005</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2006</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2007</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2008</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box      ">
            <div class="timeline_title">2009</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box      ">
            <div class="timeline_title">2010</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box      ">
            <div class="timeline_title">2011</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box      ">
            <div class="timeline_title">2012</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box      box_end ">
            <div class="timeline_title">2013</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
    </div>
    <div class="competence_explain">
        <div class="explain_line">
            <div class="explain_icon1"></div><div class="explain_title">Employment</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon2"></div><div class="explain_title">Non-Profit</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon3"></div><div class="explain_title">Education</div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {

        //show last employment detail
        $(".showtimeline").parent().css("z-index", "109");
        $(".showtimeline").show();

        var lll = $(".showtimeline").parent().width() / 2 - 140;
        var ccc = 115;

        $(".showtimeline").css("left", lll + "px");
        $(".showtimeline").find(".timeline_arrow2").css("left", ccc + "px");

        //action
        $(".nonprofit").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "108");
            $(this).find(".timeline_view").hide();
        });

        $(".tralning").hover(function () {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "107");
            $(this).find(".timeline_view").hide();
        });

        $(".employment").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "106");
            $(this).find(".timeline_view").hide();
        });

        $(".timeline_view").hover(function () {
            $(this).show();

        }, function () {
            if ($(this).parent().attr('class') == 'nonprofit') {
                $(this).parent().css("z-index", "108");
            }
            else if ($(this).parent().attr('class') == 'tralning') {
                $(this).parent().css("z-index", "107");
            }
            else {
                $(this).parent().css("z-index", "106");
            }

            $(this).hide();
        });

    });
    </script>
                <div class="me_edit3">
                  
                </div>
        	</div>
        </div>
  <div class="manage_footer"></div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>