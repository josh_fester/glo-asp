﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExpertValue
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_expert nav_expert_how">
        	<ul>
                <li class="userinfo"></li>
            	<li class="active"><a href="/Sample/ExpertValue">How I Add Value</a></li>
            	<li><a href="/Sample/ExpertCredential">My Credentials</a></li>
            	<li><a href="/Sample/ExpertCompetence">My Competencies</a></li>
            	<li><a href="/Sample/ExpertRecommendation">Recommendations</a></li>
            	<li class="dashboard">
                
                </li>
            </ul>
        </div>
        <div class="manage_boxer">        
            
  <div class="manage_boxer">        
            
  <div class="boxer_user">  		
    <div class=" box_user_private_view  "></div>

    <div class="box_user_wrap">
    <div class="box_user_photo">
         <img src="/Images/e2962976-c468-4265-84ed-6634692cbcb5.jpg">
    </div>
    
        <div class="box_user_button">        
            
                    <input type="button" onclick="return false;" value="Tag" class="graybtn">
                
            <input type="button" onclick="return false;" value="Request Tag" class="graybtn">
                
          
        </div>
     
     </div>
    <div class="box_user_name">Raf Adams</div> 
    <div class="box_user_title">The Suited Monk</div>
    <div class="box_user_local">ShangHai</div>
    <div class="box_user_industry">Training</div>
    <div class="box_user_note">I'd like to see a world in which people are ''awake'', are able to reach their full potential in work and life. By full potential I mean spiritually rich and have aligned their Suit (external) and Monk (Internal) world.</div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <div class="value_item_active font">Integrity</div><div class="value_item_active font">Non Judgmental</div><div class="value_item_active font">Respect</div>

    </div>
    <div class="me_edit2">
    
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#TagUser").click(function () {
            $.getJSON('/Tag/GetTagCategory', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#SendMessage").click(function () {
            $.getJSON('/Message/SendMessageToUser', { id: '538', random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $("#RequestRecommend").click(function () {
            $.postJSON('/Expert/RequestRecommend', { requestUserID: '538' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });

    function RequestTag(id) {
        $.post('/Tag/RequestTag', { requestUserID: id }, function (data) {
            if (data) {
                alertbox("Your request has been successfully sent!");
            }
        });
    }
</script> 
           <div class="boxer_info">

                <div class="info_container">
                	<div class="view_value font">
                    <p>
	Raf is the author of <strong>The Suited Monk</strong> and partner of Raf Adams &amp; Company in cooperation with Prof. Mike Thompson from CEIBS.<br>
	<br>
	He spends most of his time speaking and teaching on the subject of authenticity in life and business at organizations such as CEIBS and TEDx. Prior to his current career as a professional speaker and coach, he worked for ten years in sales in an international shipping company.<br>
	<br>
	Raf was born in Belgium and has been living in Asia for the past six years. When he was 27 he experienced a spontaneous awakening which led him to his current career as an author, teacher and speaker specializing in corporate leadership.&nbsp;It was from his own experience and research that Raf created the Life Journey ? and GAP models which are completely unique in the coaching and personal growth sector.&nbsp;</p>
<p>
	His Suited Monk concept is based on the next generation of leadership development which is focused on self-knowledge, self-mastery and greater wisdom and compassion.</p>
<p>
	His articles have been published in Forbes India, The Economist Intelligence Unit and Korea Times.</p>
<p>
	Credentials</p>
<ul>
	<li>
		Guest speaker at CEIBS on Stress Management</li>
	<li>
		(China Europe International Business School)</li>
	<li>
		Advanced Corporate Coaching Certification WABC (480 hours)</li>
	<li>
		Corporate Coaching Masterclass Progress-U (40 hours)</li>
	<li>
		International Coach Association ICA (125 hours, ICF accredited)</li>
	<li>
		Book Author of ‘’The Suited Monk’’</li>
</ul>
<p>
	Expertise / Services Provided</p>
<ul>
	<li>
		&nbsp; The Suited Monk Workshop</li>
	<li>
		&nbsp; Executive Coaching</li>
	<li>
		&nbsp; Authentic &amp; Wise Leadership Seminars &amp; Workshops</li>
	<li>
		&nbsp; Keynote Speeches</li>
</ul>
<p>
	Corporate Experience</p>
<ul>
	<li>
		12 years of international experience including leadership positions in various industries</li>
</ul>
<p>
	International Exposure<br>
	Work-related travel to over 15 countries<br>
	Working experience in Europe and Asia (Hong Kong, China)</p>
<p>
	Raf’s coaching clients and workshop participants include: <strong>Medtronic, Pepsico, Mead Johnson, Hewlett Packard, Philips, Alcatel-Lucent, Graco, Roquette, Borg Warner, Ferrero, H&amp;M, BASF, SKF, Panasonic, Ikea, Shanghai United Family Hospital, Britisch Council Guangzhou, IHG.</strong></p>

                    </div>
                    <div class="upload_boxer">
                    
                    </div>
                	<div class="view_filearea">
                        <div class="filearea_item">
                            <img src="/images/d8d2479c-3ebf-4dd9-a8ab-9d8d8fbdd961.jpg">
                        </div>                            
                        <div class="filearea_item">
                            <img src="/images/9237f1cc-545e-4afa-8532-e06e785b74b0.JPG">
                        </div>
                    </div>
                </div>
                <div class="me_edit3">
                    
                </div>
        	</div>
        </div>

        </div>
                <div class="manage_footer">
                </div>
   </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>