﻿<!DOCTYPE html>

<html>
<head id="Head1">
<title>
	Good Leaders Online
</title>
    <meta name="viewport" content="width=device-width" />
    <meta name="keywords" content="Executive search,China global manager,executive coaching,responsible leader.management jobs,leadership development,executive assessment" /><meta name="description" content="GLO is a membership based leadership recruitment,assessment and development community targeting primarily China&#39;s global talents and responsible leaders overall. GLO develops careers of responsible leaders and consults companies to identify and attract them. GLO provides also assessments, coaching and leadership development programs in collaboration with a pool of associated certified human resources professionals. GLO is a high end quality job portal, purpose driven leadership network and executive search platform combined in one with the purpose to save cost, while improving hiring quality at faster speed for GLO&#39;s members." /><link href="/Styles/glo.css" rel="stylesheet" type="text/css" /><link href="/Styles/home.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/js?v=Rz12or0EbjC56kRNnme2l1fpZa8rm9yilHCkm4ahMSA1" type="text/javascript"></script>
    <script src="/Js/GLO.js" type="text/javascript"></script>
</head>
<body>
<div id="glo_wrapper">

 <div class="glo_toolbar">
    <span>Version: Beta</span>   
     <a href="/">HOME</a> 
    <a href="/Home/Concept" >THE GLO CONCEPT</a>
    <a href="/Home/Success" >GLO SUCCESSES</a>
    <a href="/Insight" >INSIGHTS</a>
    <a href="/Home/Statistics" >STATISTICS</a>
    <a href="/Home/FAQ" >FAQ</a>
    <a href="/Home/GLODifference" >GLO DIFFERENCE</a>
    <a href="/Home/Policy" >POLICY</a>
    <a href="/Home/FeedBack" >FEEDBACK</a>
    <a href="/Home/Partner" >PARTNERS</a>
    <a href="/Home/Contact" >CONTACT US</a>    
 </div>

	<div id="glo_error"></div>
    
    <div class="glo_body">
        <div class="body_left">
            <div class="left_leader">

                     
            
            
                <div class="leader_item">
                  <img src="/Images/home_user1.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user2.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user3.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user4.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user5.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user6.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user7.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user8.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user9.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user10.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user11.jpg" />
                </div>
            
                <div class="leader_item">
                  <img src="/Images/home_user12.jpg" />
                </div>
            
                
            </div>
            <div class="left_info">
                GLO enables responsible leaders to create value based profiles and to 
                connect to companies that do good through their business. Eligible 
                leaders join free and member companies access innovative 
                leadership search, assessment and development solutions
            </div>
        </div>
        <div class="body_right">                 
            <div class="right_login">
             
    	<div class="login_logout">
             <a href="/Account/SignOut">Log Out</a>
        </div>
    	<div class="login_message">
        	<a href="/Admin"><img src="/Styles/images/icon_message.gif" /></a>
            <div class="message_number">0</div>
        </div>
    	<div class="login_user">
        	<a href="/Admin"><img width="30px" height="30px" src="/Images/user_none.gif" />Admin</a>
        </div>
        <div class="login_search">
            
            <div class="dropdown_box">
            
                <input class="dropdown_value" id="SearchSelectType" type="text" readonly="readonly" value='Jobs' />
                <img class="dropdown_arrow" src="/Styles/images/icon_arrow2.gif" />
            	<div class="dropdown_option">
                    <ul>
                        <li>Jobs</li>
                        <li>Users</li>
                    </ul>
                </div>
            </div>

            <div class="search_right">            
                <input class="search_text" type="text" id="textKeyword" value="" />
                <a id="HpySearch" href="javascript:void(0);"><img src="/Styles/images/icon_search2.gif" /></a>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {

                /*open dropdown*/
                $(".dropdown_box").click(function (event) {
                    event.stopPropagation();
                    $(".dropdown_option").hide();
                    $(this).find(".dropdown_option").toggle();
                });
                /*close dropdown*/
                $(document).click(function () {
                    $('.dropdown_option').hide();
                });
                /*set value*/
                $(".dropdown_option li").click(function (event) {
                    event.stopPropagation();
                    $(".dropdown_option").hide();

                    var value = $(this).text();
                    $(this).parent().parent().parent().find(".dropdown_value").val(value);
                });

                $("#HpySearch").click(function () {
                    var type = $(".dropdown_value").val();
                    var value = $("#textKeyword").val();
                    if (value == "") {
                        return false;
                    }
                    else {
                        if (type == "Jobs") {
                            window.location.href = '/Home/JobSearch' + "?keyword=" + value + "&searchtype=" + type;
                        }
                        else {
                            window.location.href = '/Home/HeaderSearch' + "?c=" + value + "&e=" + value + "&l=" + value + "&searchtype=" + type;
                        }
                        return false;

                    }
                });

                $('#textKeyword').keypress(function (e) {
                    var e = e || window.event;
                    if (e.keyCode == 13) {
                        $("#HpySearch").click();
                    }
                });

                SetUnReadMessageCount();



            });
            function SetUnReadMessageCount() {
                $.post("/Home/SetUnreadMessage", null, function (data) {
                    if ($('#UnReadMessageCount').html() != null) {
                        $('#UnReadMessageCount').html(data)
                    }
                    $('.message_number').html(data)
                }, "json");
                setTimeout(function () {
                    SetUnReadMessageCount();
                }, 300000);
            }

        </script>

            </div>
            <div class="right_box">
    	        <div class="right_image">
                <img src="/Styles/images/glo_home.gif" /><br />
                Connecting good leaders for good business
                </div>
                <div class="right_action">
            <div class="action_info">   
                <a href="/Home/RegisterGuide">Join our growing community of responsible leaders,<br />
leading with integrity, passion and a vision<br />
to include the good of society in their decisions </a>

                </div>
                </div>
            </div>

            <div class="right_items">
                <div class="items_wrap"><a href="/Home/Solution" >GLO<br />Solutions</a></div>
                <div class="items_wrap"><a href="/Home/Assurance" >GLO<br />Assurance</a></div>
                <div class="items_wrap_last">
                <a href="http://fs30.formsite.com/peterbuytaertglo-chinacom/form2/index.html" target="_blank">GLO Wise<br />Check Up</a>
                </div>
            </div>

        </div>
        <div class="glo_fixer"></div>

        
        <div class="glo_hideboxer">
            <div class="hideboxer_list">
                    <div class="list_title">Good Employers</div>
                    

                        
                            <div class="list_user">
                                <div class="user_icon"><a href="/Company/CompanyIndex?companyid=504" title="Good Leaders Online(GLO)"><img src="/images/home_user13.jpg" /></a></div>
                                <div class="user_name"><a href="/Company/CompanyIndex?companyid=504" title="Good Leaders Online(GLO)">Good Leaders Online(GLO)</a></div>
                            </div>
                            <div class="list_user">
                                <div class="user_icon"><a href="/Company/CompanyIndex?companyid=504" title="Gangfen"><img src="/images/home_user14.jpg" /></a></div>
                                <div class="user_name"><a href="/Company/CompanyIndex?companyid=504" title="Gangfen">Gangfen</a></div>
                            </div>
                            <div class="list_user">
                                <div class="user_icon"><a href="/Company/CompanyIndex?companyid=1518" title="ShinetechChina"><img src="/images/home_user15.jpg" /></a></div>
                                <div class="user_name"><a href="/Company/CompanyIndex?companyid=1518" title="ShinetechChina">ShinetechChina</a></div>
                            </div>
                            <div class="list_user">
                                <div class="user_icon"><a href="/Company/CompanyIndex?companyid=504" title="CGL"><img src="/images/home_user16.jpg" /></a></div>
                                <div class="user_name"><a href="/Company/CompanyIndex?companyid=504" title="CGL">CGL</a></div>
                            </div>

            </div>
            <div class="hideboxer_list">
                    <div class="list_title">HR Experts</div>
                   
                
                    <div class="list_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=526" title="Dasun Kim"><img src="/images/home_user4.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=526" title="Dasun Kim">Dasun Kim</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" title="Raf Adams"><img src="/images/home_user9.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" title="Raf Adams">Raf Adams</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" title="Eliot Shin"><img src="/images/home_user11.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" title="Eliot Shin">Eliot Shin</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Expert/MyValue?expertid=538" title="Mike Thompson"><img src="/images/home_user3.jpg" /></a></div>
                        <div class="user_name"><a href="/Expert/MyValue?expertid=538" title="Mike Thompson">Mike Thompson</a></div>
                    </div>

            </div>
            <div class="hideboxer_list">
                    <div class="list_title">Good Leaders</div>
                              
                    
                    <div class="list_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" title="Peter Buytaert"><img src="/images/home_user17.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" title="Peter Buytaert">Peter Buytaert</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=517" title="Erik Jia"><img src="/images/home_user18.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=517" title="Erik Jia">Erik Jia</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" title="Jay Tang"><img src="/images/home_user6.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" title="Jay Tang">Jay Tang</a></div>
                    </div>
                    <div class="list_user">
                        <div class="user_icon"><a href="/Leader/Index?leaderid=484" title="Veronique Huberts"><img src="/images/home_user8.jpg" /></a></div>
                        <div class="user_name"><a href="/Leader/Index?leaderid=484" title="Veronique Huberts">Veronique Huberts</a></div>
                    </div>
                      
            </div>
            <div class="hideboxer_other">
                <div class="other_item_new"><a href="/Content/glo_ brochure_en.pdf" target="_blank">Tell me more<br />about GLO</a></div>
                <div class="other_hack"></div>
                <div class="other_item_search">
                    <div>Start your good<br />job search here</a></div>
                    <div>
                	        <input id="txtSearch" type="text" value="Title" class="search" >
                	        <a href="#"><img id="go-search" src="/Styles/images/icon_search.gif" alt="" title="" /></a></div>
                </div>
            </div>
        </div>

    </div>

<script type="text/javascript">

    function showError(mode) {
        var html = '';
        if (mode == 0) {
            html = '<b>Email Address and Password can not be blank.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
        }
        else if (mode == 1 || mode == 2) {
            html = '<b>Email Address or Password was incorrect.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
        }
        else if (mode == 3) {
            html = '<b>Your account has been blocked!</b><br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }
        else if (mode == 4) {
            html = '<b>Your have exceeded 5 tries.<br/> Please try an hour later.</b><br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/account/forgotpassword">Click here.</a>';
        }
        else if (mode == 9) {
            html = '<b>Unknow error!</b><br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }

        var error = document.getElementById('glo_error');
        error.style.display = 'block';
        error.innerHTML = '';
        error.onclick = function (event) {
            event.stopPropagation();
        }

        var div1 = document.createElement('div');
        div1.className = 'message';
        div1.innerHTML = html;
        error.appendChild(div1);
    }

    function check() {
        var username = $("#txtUsername").val();
        var password = $("#txtPassword").val();

        if (username == "" || password == "") {
            showError(0);
            return false;
        }
        else {

            $.postJSON('/Account/JsonLogin', { "username": username, "password": password }, function (data) {
                if (data.Success == true) {

                    if ($("#chbRemember").attr("checked")) {
                        setCookie("userName", $("#txtUsername").val(), 24, "/");
                        setCookie("password", $("#txtPassword").val(), 24, "/");
                    }
                    else {
                        deleteCookie("userName", "/");
                        deleteCookie("password", "/");
                    }


                    var url = '';
                    if (data.UserType == 1)
                        url = '/Leader' + "?leaderid=" + data.UserID;
                    else if (data.UserType == 2)
                        url = '/Expert/MyValue' + "?expertid=" + data.UserID;
                    else if (data.UserType == 4)
                        url = '/Admin';
                    else
                        url = '/Company/CompanyIndex' + "?companyid=" + data.UserID;
                    window.location.href = url;

                    return false;
                }
                else {
                    showError(data.Result);
                    return false;
                }
            });

        }
        return false;
    }


    function setCookie(name, value, hours, path) {

        var name = escape(name);
        var value = escape(value);
        var expires = new Date();
        expires.setTime(expires.getTime() + hours * 3600000);
        path = path == "" ? "" : ";path=" + path;
        _expires = (typeof hours) == "string" ? "" : ";expires=" + expires.toUTCString();
        document.cookie = name + "=" + value + _expires + path;
    }
    //get cookie value
    function getCookieValue(name) {

        var name = escape(name);
        // read cookie
        var allcookies = document.cookie;
        //search cookie
        name += "=";
        var pos = allcookies.indexOf(name);
        //if exists
        if (pos != -1) {                                             //if post value equal -1
            var start = pos + name.length;
            var end = allcookies.indexOf(";", start);
            if (end == -1) end = allcookies.length;
            var value = allcookies.substring(start, end);
            return (value);
        }
        else return "";
    }
    //delete cookie
    function deleteCookie(name, path) {
        var name = escape(name);
        var expires = new Date(0);
        path = path == "" ? "" : ";path=" + path;
        document.cookie = name + "=" + ";expires=" + expires.toUTCString() + path;
    }

    $(document).ready(function () {
        var userNameValue = getCookieValue("userName");
        if (userNameValue != "")
            $("#txtUsername").val(userNameValue);
        var passwordValue = getCookieValue("password");
        if (passwordValue != "")
            $("#txtPassword").val(passwordValue);

        $(document).click(function () {
            $('#glo_error').hide();
        });
        $('#go-search').live('click', function () {
            window.location.href = '/Home/JobSearch' + "?keyword=" + $('#txtSearch').val() + "&searchtype=Jobs";
            return false;
        });
        $('.login_sign a').click(function () {
            return check();
        });

        $('#txtUsername').keypress(function (e) {
            var e = e || window.event;
            if (e.keyCode == 13) {
                return check();
            }
        });
        $('#txtPassword').keypress(function (e) {
            var e = e || window.event;
            if (e.keyCode == 13) {
                return check();
            }
        });
        $('#txtSearch').keypress(function (e) {
            var e = e || window.event;
            if (e.keyCode == 13) {
                $('#go-search').click();
            }
        });

        $('#txtUsername').focus(function () {
            $(this).addClass('light');
            if ($(this).val() == 'EMAIL') $(this).val('');
        });
        $('#txtUsername').blur(function () {
            $(this).removeClass('light');
            if ($(this).val() == '') $(this).val('EMAIL');
        });
        $('#txtPassword').focus(function () {
            $(this).addClass('light');
            if ($(this).val() == 'PASSWORD') $(this).val('');
        });
        $('#txtPassword').blur(function () {
            $(this).removeClass('light');
            if ($(this).val() == '') $(this).val('PASSWORD');
        });
        $('#txtSearch').focus(function () {
            $(this).addClass('light');
            if ($(this).val() == 'Title') $(this).val('');
        });
        $('#txtSearch').blur(function () {
            $(this).removeClass('light');
            if ($(this).val() == '') $(this).val('Title');
        });
        $('#btnRegister').focus(function () {
            $(this).addClass('light');
        });
        $('#btnRegister').blur(function () {
            $(this).removeClass('light');
        });
        $('#btnRegister').click(function () {
            window.location = '/Home/RegisterGuide';
        });


    });
</script>  
     
     

 <div class="glo_footer">©Copyright Good Leaders Online (GLO) Ltd</div>
</div>
</body>
</html>
