﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderIndex
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
        <div class="nav_leader nav_leader_interview">
            <ul>
                <li class="userinfo"></li>
                <li class="active"><a href="/Sample/LeaderIndex">My Interview</a></li>
                <li><a href="/Sample/LeaderCompetence">My Competencies</a></li>
                <li><a href="/Sample/LeaderNetwork">My Network</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
        <div class="manage_boxer">
            <div class="boxer_user">
                <div class=" box_user_private_on_view">
                </div>
                <div class="box_user_photo">
                    <img src="/Images/9860a607-0fbd-4e8c-a03d-dd5c3fb8a358.jpg">
                </div>
           
                <div class="box_user_name">
                    Peter Buytaert</div>
                <div class="box_user_title">
                    CEO</div>
                <div class="box_user_local">
                    ShangHai</div>
                <div class="box_user_industry">
                    Executive Search</div>
                <div class="box_user_note">
                    Building the leading global community of good leaders assuring good business</div>
                <div class="box_user_value">
                    <div class="box_user_value_title">
                        What values in life and career are important to you:</div>
                    <div class="value_item_active font">
                        Integrity</div>
                    <div class="value_item_active font">
                        Respect</div>
                    <div class="value_item_active font">
                        Excellence</div>
                </div>
                <div class="box_user_icon">
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index3.gif"><br>
                        Profile
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index4.gif"><br>
                        Recommend
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index5.gif"><br>
                        Interview
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index6_gray.gif"><br>
                        Online Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index7_gray.gif"><br>
                        Offline Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index8_gray.gif"><br>
                        WLDP
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index9_gray.gif"><br>
                        Coaching
                    </div>
                </div>
            </div>

            <div class="boxer_info">
                <div class="info_container">
                    <div class="interview_line">
                        <span>How do you define ‘responsible leadership’?</span><br>
                        <p class="font">
                            Responsible leaders have the ability to make quality decisions that have the best
                            possible outcome for the organization, its stakeholders and the larger good of society.
                            They serve their organizations with integrity and take ownership over their decisions</p>
                    </div>
                    <div class="interview_line">
                        <span>Given the unlimited potential for self-development where would you like to be
                            in life and career in the next 5 &ndash; 10 years?</span><br>
                        <p class="font">
                            Travelling the world and making a difference in local communities of good leaders
                            building good business sharing insights on responsible leadership and its value
                            for business and society at large</p>
                    </div>
                    <div class="interview_line">
                        <span>If tough decisions need to be communicated across your team, what approach style
                            and process would you apply in order to get the message across?</span><br>
                        <p class="font">
                            Reveal the facts of reality with the choices at hand to take corrective action,
                            leading teams to draw their own conclusions on what is a fair decision to be made.
                            Ensure the best possible outcome for all stakeholders have been considered although
                            not neccesarly a good outcome for all. Communicate across all levels via personal
                            and face-to-face interaction wherever and whenever possible</p>
                    </div>
                    <div class="interview_line">
                        <span>How much time (%) do you spend per week on each of below and how much time would
                            you ideally like to spend?</span><br>
                        <p class="font">
                        </p>
                        <table class="font">
                            <tbody>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        Current time spent (%)
                                    </td>
                                    <td>
                                        Would like to spend (%)
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Family
                                    </td>
                                    <td>
                                        30
                                    </td>
                                    <td>
                                        40
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Job
                                    </td>
                                    <td>
                                        60
                                    </td>
                                    <td>
                                        40
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mindfulness &amp; meditation
                                    </td>
                                    <td>
                                        5
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fitness
                                    </td>
                                    <td>
                                        5
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>
                        </p>
                    </div>
                    <div class="interview_line">
                        <span>In your view, what are the top critical success factors to drive change in organisations?</span><br>
                        <p class="font">
                            1. Ensure that the change makes sense and adds value to the organization
                            <br>
                            2. A non-political, respected and committed senior team that walks the talk<br>
                            3. Working closely with the pioneer adapters to the change to convince the majority
                            <br>
                            4. Have a good communication plan and clarify the why and how consistently<br>
                            5. Celebrate successes along the way</p>
                    </div>
                    <div class="interview_line">
                        <span>What actions would you take in the first 100 days on a new job?</span><br>
                        <p class="font">
                            Listen, observe and learn in the first place. Take action on 'quick wins' that improve
                            the business or its organizational strength and assist to build respect of the team.
                            <br>
                            Share first observations and recommended actions with the team and define and communicate
                            a 'change' plan to ensure a highly motivated organization understands the purpose
                            and objectives it serves</p>
                    </div>
                    <div class="interview_line">
                        <span>What makes you a <em>good</em> leader?</span><br>
                        <p class="font">
                            I am personally still on the journey to being more consistently a 'good' leader.
                            A good leader has the ability to make quality decisions that have the best possible
                            outcome for their organizations and society at large. In order to achieve this,
                            in my view, they should lead with integrity and embed a vision of the common good
                            of society at large in their decisions. They have strong empathy and and ability
                            to change and drive change. They also have a heightened level of self awareness
                            and mindfulness expressing itself in humility. Last but not least, it is important
                            to have effective communication skills in order to ensure execution of decisions
                            by their teams</p>
                    </div>
                    <div class="interview_line">
                        <span>What are you passionate about?</span><br>
                        <p class="font">
                            Making a positive difference in other people's lives and building a better society
                            in the process. Developing excellent teams that build successful business with a
                            good purpose</p>
                    </div>
                    <div class="interview_line">
                        <span>What tasks would you prefer to be handled by others?</span><br>
                        <p class="font">
                            Organization of administrative tasks as other team members are just infinitely better
                            and more passionate about it</p>
                    </div>
                    <div class="interview_line">
                        <span>Give an example of an integrity dilemma that you have faced (no names need to
                            be mentioned). How did you manage it?</span><br>
                        <p class="font">
                            There have been quite a number of occasions where shortcuts in terms of 'time' and
                            'delivery on targets' could have been made via the extension of special benefits
                            to external stakeholders. I have consistently refused to apply these practices as
                            they are not consistent with our values, carry irresponsible risks and ultimately
                            result in a loss of respect by the people that can really make a difference for
                            your organization long term</p>
                    </div>
                </div>
            </div>
        </div>
                <div class="manage_footer">
                </div>
    </div>
</asp:Content>