﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FactSheet
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_manage">
        <div class="nav_company nav_company_factsheet">
            <ul>
                <li><a href="/Sample/CompanyIndex">Life At</a></li>
                <li class="active"><a href="/Sample/FactSheet">Fact Sheet</a></li>
                <li><a href="/Sample/CompanyJob">Jobs</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
        <div class="manage_boxer2"> 
            <div class="boxer_user">                       		
                <div class="box_user_photo2">            
                	<img src="/images/c1860095-de1e-476d-aa3a-e9d54da185a2.gif">
                </div>
                 <div class=" box_user_private_on_view2  "></div>            
                
                <div class="box_user_name">Good Leaders Online (GLO)</div>
                <div class="box_user_note">Building the leading global community of good leaders assuring good business</div>
                <div class="box_user_value">
                    <div>My organization’s values:</div>
                    <div class="box_user_value_custom">
                        
                             <div class="value_item_active font">Integrity</div>
                        
                        
                             <div class="value_item_active font">Excellence</div>
                        
                        
                             <div class="value_item_active font">Serving Society</div>
                        
                        
                    </div>
                </div>
            
        	</div>
           <div class="boxer_info boxer_company_sheet">
                        <div class="info_headline">Location</div>
                        <div class="info_description font">ShangHai</div>

                        <div class="info_headline">Industry</div>
                        <div class="info_description font">Executive Search</div>
                                    
                        <div class="info_headline">Employees</div>
                        <div class="info_description font">6&ndash;50</div>

                        <div class="info_headline">Revenue</div>
                        <div class="info_description font">&lt;1,000,000 USD</div>

                        <div class="info_headline">Awards</div>
                        <div class="info_description font"></div>
                                                                      
                        <div class="info_headline">Contact Us</div>
                        <div class="info_description font">Tel:  +86 21 51785258<br>Fax: +86 21 51785218</div>

                        <div class="info_headline">Website</div>
                        <div class="info_description font"><a target="_blank" href="http://www.glo-china.com">http://www.glo-china.com</a></div>
                            
                        <div class="info_contact">
                        <table class="tbcontact">
                            <tr>
                                <td>
                                    <div class="contact_box">
                                        <img src="/Images/user_none.gif">
                                    </div>                            
                                </td>
                                <td valign="bottom">
                                    <div class="contact_line font">Grace Wang</div>
                                    <div class="contact_line font"><a href="mailto:gracewang@glo-china.com">gracewang@glo-china.com</a></div> 
                                </td>
                                <td>
                                    <div class="contact_box">
                                        <img src="/Images/user_none.gif">
                                    </div>
                                </td>
                                <td valign="bottom">
                                    <div class="contact_line font">Dasun Kim</div>
                                    <div class="contact_line font"><a href="mailto:dasunkim@glo-china.com">dasunkim@glo-china.com</a></div> 
                                </td>
                            </tr>
                        </table>
                        </div>
               
        	</div>
            

        </div>
        <div class="manage_footer2"></div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
