﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderCompentence
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <input type="hidden" value="484" id="hidexpertid">
    <div class="glo_manage">
        <div class="nav_leader nav_leader_competence">
            <ul>
                <li class="userinfo"></li>
                <li><a href="/Sample/LeaderIndex">My Interview</a></li>
                <li class="active"><a href="/Sample/LeaderCompetence">My Competencies</a></li>
                <li><a href="/Sample/LeaderNetwork">My Network</a></li>
                <li class="dashboard"></li>
            </ul>
        </div>
        <div class="manage_boxer">        
               
  <div class="boxer_user">  
      		
    <div class=" box_user_private_on_view"></div>
    <div class="box_user_photo">
         <img src="/Images/9860a607-0fbd-4e8c-a03d-dd5c3fb8a358.jpg">
    </div>
 
    <div class="box_user_name">Peter Buytaert</div>
    <div class="box_user_title">CEO</div>

    <div class="box_user_local">ShangHai</div>
    <div class="box_user_industry">Executive Search</div>
    <div class="box_user_note">Building the leading global community of good leaders assuring good business</div>
    <div class="box_user_value">
        <div class="box_user_value_title">What values in life and career are important to you:</div>
        <div class="value_item_active font">Integrity</div><div class="value_item_active font">Respect</div><div class="value_item_active font">Excellence</div>
    </div>
        <div class="box_user_icon">
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index3.gif"><br>Profile 
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index4.gif"><br>Recommend 
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index5.gif"><br>Interview
                    </div>                  
                    <div class="icon_index">                   
                        <img src="/Styles/images/icon_index6_gray.gif"><br>Online Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index7_gray.gif"><br>Offline Assessment
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index8_gray.gif"><br>WLDP
                    </div>
                    <div class="icon_index">
                        <img src="/Styles/images/icon_index9_gray.gif"><br>Coaching
                    </div>
    </div>
</div>
<div style="display:none;" id="resumesList">   
        <div class="lightbox_mask">
        </div>
        <div class="lightbox_manage">
            <div class="lightbox_message"> 
                <div class="message_headline">Resume lists</div>
                <ul>
                                
                    <li><a href="/upload/484/Biodata_glo.docx">Biodata_glo.docx</a></li>
                    
                </ul>
            <div class="me_close">
            <a href="#" onclick="return CloseLightBox()">
                <img src="/Styles/images/icon_close.gif"></a></div>
            </div>
        </div>
</div>
           <div class="boxer_info">          
             
             
<div class="competence_wrap">

      <div class="nonprofit" style="width:506px;left:0px; top:350px;">
        <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">
            <div class="timeline_black font">
            Ministry of Trade of Belgium
            <br />
            Advisor
            <br />
            1999-05-01 to 2013-05-03
            </div>

            <div class="timeline_black">Jobs duties/ Lessons learned</div>
            <div class="timeline_gray font">Advisor to the Minister of Foreign Trade</div>

            <div class="timeline_black">Competence applied</div>
            <div class="timeline_gray font"></div>

            <div class="timeline_black">Success achieved</div>
            <div class="timeline_gray font"></div>

        
        </div>
        </div>
    </div>
    
    <div class="nonprofit" style="width:250px; left:288px; top:195px;">
        <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">
            

            <div class="timeline_black font">
            Ministry of Trade of Belgium
            <br />
            Advisor
            <br />
            1999-05-01 to 2013-05-03
            </div>

            <div class="timeline_black">Jobs duties/ Lessons learned</div>
            <div class="timeline_gray font">Advisor to the Minister of Foreign Trade</div>

            <div class="timeline_black">Competence applied</div>
            <div class="timeline_gray font"></div>

            <div class="timeline_black">Success achieved</div>
            <div class="timeline_gray font"></div>

         
        </div>
        </div>
    </div>
    
      <div class="tralning" style="width:264px;left:144px; top:78px;">
          <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            EHSAL Brussels
            <br />
            1986-09-01 to 1991-07-15
            </div>

            <div class="timeline_black">Major</div>
            <div class="timeline_gray font">Commercial Engineering</div>

            <div class="timeline_black">Degree</div>
            <div class="timeline_gray font">Master</div>

            <div class="timeline_black">Course title</div>
            <div class="timeline_gray font"></div>
      	    
                

          </div>
          </div>
      </div>
    
      <div class="tralning" style="width:7px; left:22px; top:388px;">
          <div class="timeline_view">
            <div class="timeline_arrow2"></div>
            <div class="timeline_info">

            <div class="timeline_black font">
            Insead
            <br />
            2004-06-15 to 2004-07-31
            </div>

            <div class="timeline_black">Major</div>
            <div class="timeline_gray font">Advanced Management Program</div>

            <div class="timeline_black">Degree</div>
            <div class="timeline_gray font">Other</div>

            <div class="timeline_black">Course title</div>
            <div class="timeline_gray font">Advanced Management Program</div>

                

          </div>
          </div>
      </div>
    
     <div class="employment" style="width:52px;left:414px; top:116px;">   
            <div class="timeline_view ">
                    <div class="timeline_arrow2"></div>
                    <div class="timeline_info">
                    	
                        <div class="timeline_black font">
                            Prince Albert Foundation
                            <br />
                            Fellow
                            <br />
                            Management training
                            <br />
                            1991-09-01 to 1992-08-31
                        </div>

                        <div class="timeline_black">Position description</div>
                        <div class="timeline_gray font">Market research Asian markets (including India) for diagnostic imaging systems in healthcare</div>

                        <div class="timeline_black">Competencies applied</div>
                        <div class="timeline_gray font">market research, strategy, cross cultural management</div>

                        <div class="timeline_black">Success achieved</div>
                        <div class="timeline_gray font">Identified and initiated new sales channels and OEM business partners in South Korea, Japan, India, Thailand and Philippines</div>

                         
                      </div>      
                    </div>   
      </div>
    
    <div class="employment" style="width:138px;left:0px; top:271px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">
                
                <div class="timeline_black font">
                    Agfa Japan
                    <br />
                    Marketing Manager
                    <br />
                    Healthcare
                    <br />
                    1992-09-01 to 1996-07-31
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Execution of Boston Consulting Group led new business strategy for digital solutions </div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">marketing, sales, change management</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Built direct sales and services team with innovative approach to local dealerships in second tier cities</div>

                 
            </div>     
            </div>         
      </div>
    
     <div class="employment" style="width:70px;left:468px; top:116px;">   
            <div class="timeline_view ">
                    <div class="timeline_arrow2"></div>
                    <div class="timeline_info">
                    	
                        <div class="timeline_black font">
                            Agfa Japan
                            <br />
                            Marketing Manager
                            <br />
                            Healthcare
                            <br />
                            1992-09-01 to 1996-07-31
                        </div>

                        <div class="timeline_black">Position description</div>
                        <div class="timeline_gray font">Execution of Boston Consulting Group led new business strategy for digital solutions </div>

                        <div class="timeline_black">Competencies applied</div>
                        <div class="timeline_gray font">marketing, sales, change management</div>

                        <div class="timeline_black">Success achieved</div>
                        <div class="timeline_gray font">Built direct sales and services team with innovative approach to local dealerships in second tier cities</div>

                         
                      </div>      
                    </div>   
      </div>
    
    <div class="employment" style="width:48px;left:139px; top:271px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">
                
                <div class="timeline_black font">
                    Perfect Print Services
                    <br />
                    General Manager
                    <br />
                    Photo retail operations
                    <br />
                    1996-08-01 to 1997-06-30
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Acquisition and restructuring of photo retail operations across Asia</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">M&A</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font"></div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:29px;left:0px; top:426px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    Agfa South East Asia
                    <br />
                    Managing Director
                    <br />
                    Healthcare, Graphics, Photo Imaging
                    <br />
                    1997-07-01 to 2004-07-31
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">S.E. Asia head of Agfa's operations and sales</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">executive management, sales management, business development, channel management</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Startup of digital solutions business, restructuring of regional operations, build a 50 employees and 10 million US$ direct sales business in 3 years from ground zero </div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:349px;left:189px; top:271px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">
                
                <div class="timeline_black font">
                    Agfa South East Asia
                    <br />
                    Managing Director
                    <br />
                    Healthcare, Graphics, Photo Imaging
                    <br />
                    1997-07-01 to 2004-07-31
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">S.E. Asia head of Agfa's operations and sales</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">executive management, sales management, business development, channel management</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Startup of digital solutions business, restructuring of regional operations, build a 50 employees and 10 million US$ direct sales business in 3 years from ground zero </div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:56px;left:31px; top:426px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    Agfa Korea
                    <br />
                    President
                    <br />
                    Agfa Healthcare
                    <br />
                    2004-08-01 to 2005-08-31
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Legal representative director of Agfa in South Korea and head of healthcare division</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">Factory management, sales management, executive management, M&A</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Acquisition of healthcare services business, startup of digital solutions sales in Korean market</div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:70px;left:90px; top:426px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    Agfa Graphics Asia
                    <br />
                    President
                    <br />
                    Graphics
                    <br />
                    2005-09-30 to 2006-12-31
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Head of sales and operations for Agfa's Graphics division. A 250 million US$ business employing 500 employees across the region</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">Executive management, M&A, channel management, sales management, restructuring</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Reduced operational expenses by 30% with turnaround of business in one year to positive EBITDA</div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:353px;left:162px; top:426px;">
            <div class="timeline_view ">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    China Global Leaders (CGL) Management Consulting
                    <br />
                    CEO
                    <br />
                    Management Consulting
                    <br />
                    2007-01-01 to 2013-07-25
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Founder of incubator services for foreign invested startups and restructuring consulting for established foreign investmed companies</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">Strategy, business development, startup management, restructuring</div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">Successfully assisted the startup in China of more than 20 technology companies and restructuring of 3 family and private equity invested organizations </div>

                 
            </div>     
            </div>         
      </div>
    
    <div class="employment" style="width:11px;left:504px; top:437px;">
            <div class="timeline_view showtimeline">
                <div class="timeline_arrow2"></div>
                <div class="timeline_info">

                <div class="timeline_black font">
                    Goodleadersonline (glo) Ltd
                    <br />
                    CEO
                    <br />
                    Leadership search and development
                    <br />
                    2013-05-01 to 2013-07-25
                </div>

                <div class="timeline_black">Position description</div>
                <div class="timeline_gray font">Building the leading global community of good leaders assuring good business</div>

                <div class="timeline_black">Competencies applied</div>
                <div class="timeline_gray font">Executive leadership<br>Startup management<br>Strategy<br><br></div>

                <div class="timeline_black">Success achieved</div>
                <div class="timeline_gray font">From conceptualization to online site, initial seed investment and team development in less than one year</div>

                 
            </div>     
            </div>         
      </div>
    
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">1984</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1985</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1986</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1987</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1988</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1989</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1990</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1991</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1992</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    box_end ">
            <div class="timeline_title">1993</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">1994</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1995</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1996</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1997</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1998</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">1999</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2000</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2001</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2002</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    box_end ">
            <div class="timeline_title">2003</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active  box_first   ">
            <div class="timeline_title">2004</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2005</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2006</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2007</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2008</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2009</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2010</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2011</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    ">
            <div class="timeline_title">2012</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
            <div class="timeline_box  box_active    box_end ">
            <div class="timeline_title">2013</div>
            <div class="timeline_icon"></div>
            <div class="timeline_content"></div>                    	
        </div>
        
    </div>


    <div class="competence_explain">
        <div class="explain_line">
            <div class="explain_icon1"></div><div class="explain_title">Employment</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon2"></div><div class="explain_title">Non-Profit</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon3"></div><div class="explain_title">Education</div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {

        //show last employment detail
        $(".showtimeline").parent().css("z-index", "109");
        $(".showtimeline").show();

        var lll = $(".showtimeline").parent().width() / 2 - 140;
        var ccc = 115;

        $(".showtimeline").css("left", lll + "px");
        $(".showtimeline").find(".timeline_arrow2").css("left", ccc + "px");

        //action
        $(".nonprofit").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "108");
            $(this).find(".timeline_view").hide();
        });

        $(".tralning").hover(function () {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "107");
            $(this).find(".timeline_view").hide();
        });

        $(".employment").hover(function (e) {
            $(".showtimeline").hide();
            $(".showtimeline").parent().css("z-index", "106");
            $(this).css("z-index", "109");
            $(this).find(".timeline_view").show();

            var ll = $(this).width() / 2 - 140;
            var cc = 115;

            $(this).find(".timeline_view").css("left", ll + "px");
            $(this).find(".timeline_view").find(".timeline_arrow2").css("left", cc + "px");
        }, function () {
            $(this).css("z-index", "106");
            $(this).find(".timeline_view").hide();
        });

        $(".timeline_view").hover(function () {
            $(this).show();

        }, function () {
            if ($(this).parent().attr('class') == 'nonprofit') {
                $(this).parent().css("z-index", "108");
            }
            else if ($(this).parent().attr('class') == 'tralning') {
                $(this).parent().css("z-index", "107");
            }
            else {
                $(this).parent().css("z-index", "106");
            }

            $(this).hide();
        });

    });
</script>
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>
</asp:Content>
