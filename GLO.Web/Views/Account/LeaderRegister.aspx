﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RegisterLeaderModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderRegister
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script src="<%=Url.Content("~/Scripts/jquery.cl.ajaxCheckCode.js") %>" type="text/javascript"></script>
    <style>
    .input-validation-error
    {
        border-color: Red;
    }
</style>
<% using (Html.BeginForm("LeaderRegister", "Account", FormMethod.Post, new { id = "regfrm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(m=>m.UserType) %>
 <div class="glo_body">
    <div class="glo_boxer">
        <div class="boxer_headline">               
                Welcome to the GLO community of Good Leaders Online<br />
                and thank you for your interest to join as a GLO leader
        </div>
        <div class="register_leader">
            <div class="register_left">
            <table>
                 <tr>
                    <td colspan="2" height="10"><div class="register_join">JOIN GLO FREE</div><span class="required">Fields with * are required fields!</span></td>
                </tr>
                 <tr>
                    <td width="125" class="toptext">
                        E-mail
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.Email, new { @class = "txtbox1", maxlength = "50" })%><span class="required">*</span>
                        <br />Email will be used to access your account<br /><%: Html.ValidationMessageFor(model => model.Email)%>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.Password, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.Password) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Confirm password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "txtbox1", maxlength = "20" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Your name
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.FirstName, new { @class = "txtbox6", Value = "First name", maxlength = "30" })%>&nbsp;&nbsp;&nbsp;&nbsp;<%: Html.TextBoxFor(m => m.LastName, new { @class = "txtbox6", Value = "Last name", maxlength = "30" })%><span class="required">*</span>
                        <br /><%: Html.ValidationMessageFor(model => model.FirstName)%> &nbsp;&nbsp;&nbsp;
                        <%: Html.ValidationMessageFor(model => model.LastName)%>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Gender
                    </td>
                    <td height="45">
                        <input type="radio" name="Sex" value="0" <%if(Model.Sex==0){ %> checked="checked"<%} %> />Male <input type="radio" name="Sex" value="1" <%if(Model.Sex==1){ %> checked="checked"<%} %>/>Female<span class="required">*</span><br />
                        <span class="remind">Cannot be modified after registration.</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        location
                    </td>
                    <td>
                        <div class="multiple_box">
                             <%: Html.HiddenFor(model => model.LocationID, new { @class = "multiple_locationid" })%>
                            <input class="multiple_value" id="Location" name="Location" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.Location) ? Model.Location : "Select a location"%>" />
                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="multiple_wrapper">
                               <%Html.RenderPartial("~/Views/Shared/LocationSelect.ascx", (IList<GLO.Data.Models.GLO_Location>)ViewBag.LocationList); %>
                           </div> 
                        </div><span class="required">*</span>
                    </td>
                </tr>
               
                <tr>
                    <td class="toptext">
                        Phone number
                    </td>
                    <td>
                    <%: Html.TextBoxFor(model => model.PhoneNumber1, new { @class = "txtbox2", Value = "+", MaxLength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber2, new { @class = "txtbox2", MaxLength = "10" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber3, new { @class = "txtbox4", MaxLength = "11" })%>
                       
                    </td>
                </tr>                
                <tr>
                    <td class="toptext">
                        check code
                    </td>
                    <td>
                        <%:Html.TextBoxFor(m => m.CheckCode, new { MaxLength = "6", @class = "txtbox3", size = "50" })%>                        
                        <span><%:Html.CheckCodeImage()%></span> <a id="changeImg" href="#">change?</a><span class="required">*</span><br /><%:Html.ValidationMessageFor(model => model.CheckCode)%>
                    </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Current job title
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.Title, new { @class = "txtbox1", maxlength = "50" })%>
                    </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Industry
                    </td>
                    <td>
                         <div class="multiple_box">
                          <%: Html.HiddenFor(model => model.IndustryID, new { @class = "multiple_id" })%>
                             <input class="multiple_value" id="Industry" name="Industry" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.Industry) ? Model.Industry : "Select an industry"%>" />
                            <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                            <div class="multiple_wrapper">
                                <%Html.RenderPartial("~/Views/Shared/IndustrySelect.ascx", (IList<GLO.Data.Models.GLO_IndustryCategory>)ViewBag.IndustryList); %>
                            </div>
                         </div><span class="required">*</span>
                    </td>
                </tr>
            </table> 
            </div>
            <div class="register_right">
            GLO’s vision to build the leading global  community for <em>good</em> leaders  assuring good business results from a firm belief of its founders that <em>good</em> leaders have integrity, are motivated to bring value to society at large and are change advocates for a better world.<br /><br />  
 
The GLO community provides career development services for leaders who identify themselves with these principles and are eager to continue their leadership development joining GLO’s assessment, coaching or wise leadership development programs.<br /><br />

Our members have generally more than 10 years management experience or at least a master degree or postgraduate education. Joining GLO is free and provides you:<br />
<ul>
    <li>A unique GLO profile focused on your values, vision and core competencies</li>
    <li>A free online assessment to evaluate your <em>wise</em> leadership attributes as a basis for further career development</li>
    <li>Access to <em>good</em> jobs</li>
    <li>Access to <em>good</em> company members and the possibility to tag and contact them</li>
    <li>Access to our pool of HR experts to assist your career development</li>
    <li>Substantial discounts when joining our coaching or <em>wise</em> leadership development programs  (initially only in China)</li>
</ul>

            </div>
            <div style="clear:both;padding:15px 0 0 0;font-size:13px;">
                <div style="clear:both;margin:1px 0;">
                <div style="float:left;margin-right:5px;">
                    <%: Html.HiddenFor(model=>model.Identify) %>
                    <input type="checkbox" id="chkIdentify" />
                </div>
                I identify myself with the principles of GLO to lead with integrity and purpose to contribute to society at large.<br />
                In particular, I confirm that my profile data are  a correct reflection of my past and present education and career.
                </div>
                
                <div style="clear:both;margin:1px 0;">
                <div style="float:left;margin-right:5px;">
                    <%: Html.HiddenFor(model=>model.Policy) %>
                    <input type="checkbox" id="chkPolicy" />
                </div>
                I have <a href="javascript:void(0);" id="RegisterRead">read</a> and accept GLO's terms and conditions
                </div>
                
                <%: Html.HiddenFor(model=>model.ReceiveEmail) %>
                <div style="clear:both;margin:1px 0;" id="divReceiveEmail">
                <div style="float:left;margin-right:5px;">
                    <input type="checkbox" id="chkReceiveEmail" checked="true" />
                </div>
                Yes, please send me email alerts and keep me up to date on GLO thought leadership events
                </div>

            </div>
        </div>
        <div class="me_apply"><a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif"></a></div>
    </div>
 </div>
<% } %>
<script type="text/javascript">
    var leaderEmail = '<%=ConfigurationManager.AppSettings["LeaderEmail"] %>';
    console.log('<%=ViewBag.consoleMessage.ToString() %>');
        $(document).ready(function () {
            var locationError = '<%=ViewBag.LocationError.ToString() %>';
            if (locationError != "") {
                $('#Location').parent().addClass("select_error");
            }

            var industryError = '<%=ViewBag.IndustryError.ToString() %>';
            if (industryError != "") {
                $('#Industry').parent().addClass("select_error");
            }

            $('#Email').blur(function () {
                if ($(this).val()!= "" && leaderEmail.indexOf($(this).val()) >= 0) {
                    $('#Password').val("Passed");
                    $('#ConfirmPassword').val("Passed");
                    $('#Password').attr("disabled", true);
                    $('#ConfirmPassword').attr("disabled", true);
                    $('#divReceiveEmail').css("display", "none");
                    $('#ReceiveEmail').val('0');
                    $('#Industry').parent().removeClass("select_error");
                    $("#IndustryID").val("169");
                } else {
                    $('#divReceiveEmail').css("display", "");
                    $('#Password').attr("disabled", false);
                    $('#ConfirmPassword').attr("disabled", false);
                    $('#Password').val("");
                    $('#ConfirmPassword').val("");
                }
            });

            $('#PhoneNumber1').blur(function () {
                if ($(this).val() == '') $(this).val('+');
            });

            $('#FirstName').focus(function () {
                if ($(this).val() == 'First name') $(this).val('');
            });
            $('#FirstName').blur(function () {
                if ($(this).val() == '') $(this).val('First name');
            });

            $('#LastName').focus(function () {
                if ($(this).val() == 'Last name') $(this).val('');
            });
            $('#LastName').blur(function () {
                if ($(this).val() == '') $(this).val('Last name');
            });

            $.ajaxCheckCode();
            $('#changeImg').click();
            $('#CheckCode').focusout(function () {
                $(this).removeClass("input-validation-error");
            });
            $('#chkIdentify').click(function () {
                if ($(this).attr("checked")) {
                    $('#Identify').val('1');
                    $(this).parent().removeClass("select_error");
                }
                else {
                    $('#Identify').val('0');
                }
            });
            $('#chkReceiveEmail').click(function () {
                if ($(this).attr("checked")) {
                    $('#ReceiveEmail').val('1');
                }
                else {
                    $('#ReceiveEmail').val('0');
                }
            });

            $('#chkPolicy').click(function () {
                if ($(this).attr("checked")) {
                    $(this).removeAttr("checked");
                    $('#Policy').val('0');

                    $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                        $('#lightboxwrapper').empty().html(data.Html);
                        $("#lightboxwrapper").attr("style", "display:block");
                    });
                }
                else {
                    $('#Policy').val('0');
                }
            });
            if ($('#Identify').val() == "1") {
                $('#chkIdentify').attr("checked", "checked");
            }
            else {
                $('#chkIdentify').removeAttr("checked");
            }
            if ($('#Policy').val() == "1") {
                $('#chkPolicy').attr("checked", "checked");
            }
            else {
                $('#chkPolicy').removeAttr("checked");
            }
            $("#btnSubmit").click(function () {

                if (!$('#chkIdentify').attr("checked")) {
                    $('#chkIdentify').parent().addClass("select_error");
                }
                if (!$('#chkPolicy').attr("checked")) {
                    $('#chkPolicy').parent().addClass("select_error");
                }
                if ($('#Location').val() == "" || $('#Location').val() == "Select a location") {
                    $('#Location').parent().addClass("select_error");
                    $("#LocationID").val("");
                }

                if ($("#Email").val() == "" || leaderEmail.indexOf($("#Email").val()) < 0) {
                    if ($('#Industry').val() == "" || $('#Industry').val() == "Select an industry") {
                        $('#Industry').parent().addClass("select_error");
                        $("#IndustryID").val("");
                    }
                }

                $("#regfrm").submit();
            });

            $("#RegisterRead").click(function () {
                $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
                });
            });
        });
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>