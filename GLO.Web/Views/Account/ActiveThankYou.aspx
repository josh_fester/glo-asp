﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.LoginModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ActiveThankYou
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("PaymentLogin", "Account", FormMethod.Post, new { id = "login" }))
   { %>
    <%: Html.ValidationSummary(true) %>
 <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Thank you joining the GLO community !
            </div>
            <%if (Request.QueryString["companytype"] == "1" || Request.QueryString["companytype"] == "5")
              { %>
            
            <div class="boxer_content">
            We appreciate your interest to join GLO and look forward to assist you!<br /><br />

            Simply confirm your username and password here and you will be guided to your company dashboard which will allow you to:<br />
            <ul>
                <li>Post jobs</li>
                <li>Tag and contact leaders and HR experts</li>
                <li>Build your function dedicated talent pipeline</li>
                <li>Book GLO search and assessment experts</li>
            </ul>
                <br>
                Success!<br><br>
                Peter Buytaert<br>
                CEO, GLO
                <br><br><br>
            </div>

            <%}
              else if(Request.QueryString["experttype"] == "1")
              { %>

              <div class="boxer_content">
               We appreciate your interest to  join GLO and look forward to assist you!<br /><br />

                Simply confirm your username and password here and you will be guided through our payment options to activate your ‘expert’ membership.
                <br><br>
                Success!<br><br>
                Peter Buytaert<br>
                CEO, GLO
                <br /><br /><br /><br /><br /><br /><br /><br />
            </div>

              <%}else{ %>
              <div class="boxer_content">
               We appreciate your interest to  join GLO and look forward to assist you!<br /><br />

                Simply confirm your username and password here and you will be guided through our payment options to activate your ‘basic’ membership.
                
                <br><br>
                Success!<br><br>
                Peter Buytaert<br>
                CEO, GLO
                <br /><br /><br /><br /><br /><br /><br /><br /><br />
            </div>
            <%} %>
            
        <div class="box_login">
            <div>Email Address <%: Html.EditorFor(model => model.UserName) %></div>
            <div>Password <%: Html.EditorFor(model => model.Password) %></div>
            <div id="errorMessage">
                <div class="message"></div>
            </div>
        </div>
        <div class="boxer_agree">
            <a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>
    </div>
    </div>
<% } %>
<script type="text/javascript">
    $(document).ready(function () {
        $('#UserName').blur(function () {
            $(this).removeClass('input_error');
        });

        $('#Password').blur(function () {
            $(this).removeClass('input_error');
        });

        $("#btnSubmit").click(function () {
            var username = $("#UserName").val();
            var password = $("#Password").val();

            if (username == "" || username == "Email Address") {
                $("#UserName").addClass("input_error");
            }
            if (password == "" || password == "Password") {
                $("#Password").addClass("input_error");
            }

            if (username == "" || password == "") {
                showError(0);
                return false;
            }
            else {

                $.postJSON('<%=Url.Action("PaymentLogin","Account") %>', { "username": username, "password": password }, function (data) {
                    if (data.Success == true) {
                        window.location.href = data.ResultUrl;
                        return false;
                    }
                    else {
                        showError(data.Result);
                        return false;
                    }
                });

            }
            return false;
        });
        
        $(document).click(function () {
            $('#errorMessage').hide();
        });

    });
    function showError(mode) {
        var html = '';
        if (mode == 0) {
            html = '<b>Email Address and Password can not be blank.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
        }
        else if (mode == 1) {
            html = '<b>Email Address or Password was incorrect.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
        }
        else if (mode == 2) {
            html = '<b>Your account has been blocked!<br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }
        else if (mode == 3) {
            html = '<b>Your have exceeded 5 tries.</b><br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Home/RegisterGuide">Click here.</a>';
        }
        else if (mode == 9) {
            html = '<b>Unknow error!</b><br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }
        $('#errorMessage .message').html(html);
        $('#errorMessage').show();
    }
</script>
</asp:Content>

