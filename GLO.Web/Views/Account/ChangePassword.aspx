﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.ChangePasswordModel>" %>

<asp:Content ID="Content4" ContentPlaceHolderID="TitleContent" runat="server">
    AddCategory
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<h2>Change password</h2>

                
            <% using (Html.BeginForm())
               { %>
            <%: Html.ValidationSummary(true) %>
            <%: Html.HiddenFor(m=>m.UserID) %>
            
    <table class="tbeditor">
        <tr>
            <th colspan="2">GLO_ChangePassword</th>
        </tr>
                    <tr>
                        <td width="140" class="toptext">
                            Old Password
                        </td>
                        <td>
                            <%: Html.Password("OldPassword", "", new { @class = "txtbox1", maxlength = "12" })%>
                            <%: Html.ValidationMessageFor(model => model.OldPassword)%>
                        </td>
                    </tr>
                    <tr>
                        <td width="140" class="toptext">
                            New Password
                        </td>
                        <td>
                            <%: Html.Password("NewPassword", "", new { @class = "txtbox1", maxlength = "12" })%>
                            <%: Html.ValidationMessageFor(model => model.NewPassword)%>
                        </td>
                    </tr>
                    <tr>
                        <td width="200" class="toptext">
                            Confirm New Password
                        </td>
                        <td>
                            <%: Html.Password("ConfirmPassword", "", new { @class = "txtbox1", maxlength = "12" })%>
                            <%: Html.ValidationMessageFor(model => model.ConfirmPassword)%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="Submit" class="btn_submit" id="btnChangePasswod" value="Submit" />
                        </td>
                    </tr>
                </table>

            <%} %>

    <script type="text/javascript">
        $(document).ready(function () {
            var message = '<%=ViewBag.Message %>';
            if (message != "") {
                alertbox(message, function () {
                    window.parent.location.href = "/Account/SignOut";
                });
            }
        });
    </script>
</asp:Content>
