﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.ChangePasswordModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ResetPassword
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">    
<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
<style>
.input-validation-error
{
    border-color: Red;
}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("ResetPassword", "Account", FormMethod.Post, new { id = "frm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(m=>m.UserID) %>
    <%: Html.HiddenFor(m=>m.OldPassword) %>
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Reset Your Password
            </div>
            <div class="boxer_content">
                <div class="login_wrap">
                <table>
                    <tr>
                        <td width="190" class="title">
                            <b>New Password</b>
                        </td>
                        <td>
                            <%: Html.Password("NewPassword", "", new { @class = "txtbox4", maxlength = "12" })%><br /><%: Html.ValidationMessageFor(model => model.NewPassword)%>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">
                            <b>Confirm Password</b>
                        </td>
                        <td>
                            <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "txtbox4", maxlength = "12" })%><br /><%: Html.ValidationMessageFor(model => model.ConfirmPassword)%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="submit" class="bluebtn2" id="btnChangePasswod" value="Submit" />
                        </td>
                    </tr>
                </table>
            </div>
            </div>
        </div>
    </div>
    <%} %>
<script type="text/javascript">
    $(document).ready(function () {
        var message = '<%=ViewBag.Message %>';
        if (message != "") {
            alertbox(message, function () {
                window.location.href = "/Home/index";
            }, function () {
                window.location.href = "/Home/index";
            });
        }
    });
</script>
</asp:Content>
