﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterThankYou
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Thank you for completing your profile!
            </div>
            <div class="boxer_content">
               Our GLO team will now review your profile and may contact you via the email associated with your account for more info.<br /><br /> 

                After we have reviewed and approved your membership, you will have full access to GLO’s member company and expert profiles, as well as <em>good</em> jobs online. You can also receive regular updates on our ‘thought leadership’ series.<br /><br />

                We again express our sincere thanks for your interest to join the GLO leadership community!
                <br><br>
                Peter Buytaert<br>
                CEO, GLO
                <br><br><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

            </div>
        <div class="boxer_agree">
            <a href="/Company/CompanyIndex?companyid=<%=Model.UserID %>"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>
    </div>
    </div>

</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
