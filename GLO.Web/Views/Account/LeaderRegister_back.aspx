﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderRegister
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>LeaderRegister</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>GLO_Users</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.UserName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.UserName) %>
            <%: Html.ValidationMessageFor(model => model.UserName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PassWord) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PassWord) %>
            <%: Html.ValidationMessageFor(model => model.PassWord) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.TypeID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.TypeID) %>
            <%: Html.ValidationMessageFor(model => model.TypeID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IndustryID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IndustryID) %>
            <%: Html.ValidationMessageFor(model => model.IndustryID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IsLock) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsLock) %>
            <%: Html.ValidationMessageFor(model => model.IsLock) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.RegisterDate) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.RegisterDate) %>
            <%: Html.ValidationMessageFor(model => model.RegisterDate) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Approved) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Approved) %>
            <%: Html.ValidationMessageFor(model => model.Approved) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
