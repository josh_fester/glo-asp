﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterLegal
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_body">
        <div class="body_boxer">       
            <div class="boxer_headline2">
                Welcome to the GLO community of Good Leaders Online<br />
                and thank you for your interest to join as a company member
            </div>
            <div class="boxer_content5">
                GLO takes its values and purpose seriously and commits to the highest quality standards. We therefore request all GLO community members (leaders, companies and HR experts) to accept our terms and conditions which in essence describe in detail:<br /><br />

                <ul class="alpha">
                    <li>That GLO is a membership based platform which has the right to refuse and decline access to any members who violate GLO’s quest for integrity and quality.</li>
                    <li>That GLO makes a best effort to maximize integrity of data available online, yet providing correct info ultimately is the moral obligation of its members.</li>
                    <li>That GLO’s assessment tools are based on well researched profiling yet should never be considered at face value and uniquely to assess a leader’s competencies.</li>
                    <li>That GLO …</li>
                </ul>
<br />
Please read our terms and conditions in detail <a href="javascript:void(0);" id="RegisterRead">here</a> and we always encourage any feedback that you may have and are always available to clarify where needed! <br /><br />

Thanks for accepting our terms and conditions and welcome to GLO!<br /><br />

Click to confirm and you will gain immediate access to your company profile page. Feel free to update the page further as we review your membership application, approval of which will be sent to your email contact provided. 
<br /><br />
Welcome! 
<input type="hidden" id="Policy" />
            </div>
            <div class="boxer_agree">
               <a id="RegisterNext" href="/Account/RegisterCompany?RegType=<%=ViewBag.RegType %>"
                    class="Register">
                    <img src="/Styles/images/icon_apply.gif" /></a></div>
        </div>
    </div>
    
<script type="text/javascript">
    $(document).ready(function () {
        $("#RegisterRead").click(function () {
            $.getJSON('<%=Url.Action("_PolicyDetail","Home") %>', null, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

    });
</script>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
