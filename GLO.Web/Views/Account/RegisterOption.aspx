﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterOption
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_body">
        <div class="glo_boxer">       
            <div class="boxer_headline">
                Welcome to the GLO community of Good Leaders Online<br />
                and thank you for your interest to join as a company member
            </div>
            <div class="boxer_content">
              We are delighted to have you join the GLO community of <em>good</em> leaders and <em>good</em> companies.<br /><br />

All our online leadership search and job ad solutions can be accessed worldwide. <br /><br />
Executive search, leadership assessment centers and development programs though are currently only available in China and Hong Kong.<br /><br />
 We therefore kindly request you to confirm the location of your organization to ensure we provide you membership solutions in line with our current team deployment.
<br /><br /><br /><br />

            </div>
            <div class="boxer_agree2">
                <a href="/Account/RegisterOptionHK"><img src="/Styles/images/chinahk.gif" /></a> &nbsp;&nbsp;&nbsp; <a href="/Account/RegisterOptionOther"><img src="/Styles/images/chinaother.gif" /></a>
            </div>
        </div>
           
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
