﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> 
RegisterWelcome
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="glo_boxer">       
            <div class="boxer_headline">
                Welcome to the GLO community of Good Leaders Online<br />
                and thank you for your interest to join as a company member
            </div>
            <div class="boxer_content">
               Our company members have access to our select pool of leaders with generally more than 8 years of professional experience, master or PhD and doctorate degrees.<br /><br />

GLO’s leaders have made a moral pledge to lead with integrity and to build value for both the organizations they serve as well as society at large (‘the common good’). <br /><br />

GLO connects <em>good</em> leaders to <em>good</em> companies<br /><br />

We invite you to explore GLO’s value by offering a 3 months “Get to know GLO” membership.  Once your membership is approved, you can start browsing leader profiles, post jobs, request your job candidates to take a free online ‘wise leadership’ assessment, as well as contact our GLO experts  to assist you in recruitment, assessments or leadership development. GLO works with a certified pool of member HR experts in various fields of coaching, leadership and organizational development.<br /><br />

If you would like to learn more about GLO’s comprehensive leadership selection, assessment and development 
solutions , kindly review our <a href="/Home/FAQ" target="_blank">FAQ</a> or click <a href="/Home/Solution" target="_blank">here</a> to get details on GLO company solutions.<br /><br />

Gaining access to your free membership is a simple 4 step approach:<br />
<ul class="decimal">
    <li>Select your free ‘Get to know GLO’ membership </li>
    <li>Provide basic company data</li>
    <li>Read and accept our terms and conditions</li>
    <li>Confirm your membership via email link provided by the GLO team after reviewing your application </li>
</ul>
<br />

            </div>
            <div class="boxer_agree">
                <div class="hpylink_bluebtn">
                   <a href="/Account/RegisterOption" class="Register">I WANT TO GET TO KNOW GLO</a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
