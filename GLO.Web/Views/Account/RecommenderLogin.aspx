﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.LoginModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Login
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <input type="hidden" name="RecommendName" value="<%=ViewBag.RecommendName%>" />
    <input type="hidden" name="InviteName" value="<%=ViewBag.InviteName%>" />
    <input type="hidden" name="RecommendSex" value="<%=ViewBag.RecommendSex%>" />
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Dear <%=ViewBag.RecommendName%>, <%=ViewBag.InviteName %> has joined GLO and invited you to recommend <%= ViewBag.RecommendSex.ToString()=="0"?"him":"her" %>. Kindly log in and complete the recommendation template provided. Thank You!
            </div>
            <div class="boxer_content">
                <div class="login_wrap">
                    <table>
                        <tr>
                            <td width="130" class="title"><b><%: Html.LabelFor(model => model.UserName) %></b></td>
                            <td>
                                <%: Html.EditorFor(model => model.UserName) %><br />
                                <%: Html.ValidationMessageFor(model => model.UserName) %>                            
                            </td>
                        </tr>
                        <tr>
                            <td class="title"><b><%: Html.LabelFor(model => model.Password) %></b></td>
                            <td>
                                <%: Html.EditorFor(model => model.Password) %><br />
                                <%: Html.ValidationMessageFor(model => model.Password) %>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" class="bluebtn2" value="Sign In" /></td>
                        </tr>
                        <tr>
                            <td></td><td id="error"><%=ViewBag.Message!=null?ViewBag.Message:""%></td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
<% } %>

<script type="text/javascript">
    $(document).ready(function () {
        $('.bluebtn2').click(function () {
            $('#error').empty();
        });
    });
</script>

</asp:Content>
