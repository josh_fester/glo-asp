﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.LoginModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LoginMessage
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">    
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Thank you for joining the GLO community !
            </div>
            <div class="boxer_content">
                Welcome to GLO and we look forward to assist your career development.
                <br />
                <br />
                Simply confirm your email address and password here and you will be guided to your unique
                profile page which you can now complete to stand out of the crowd as a GLO member.
                <br />
                <br />
                We also encourage you to invite up to 8 people who have made a significant difference
                in your career or personal development to recommend you and confirm your values
                and competencies.

                <br><br>
                Success!<br><br>
                Peter Buytaert<br>
                CEO, GLO
                <br /><br /><br /><br /><br />
            </div>


        <% using (Html.BeginForm("LoginMessage", "Account", FormMethod.Post, new { id = "loginfrm" }))
           {%>
        <%: Html.ValidationSummary(true)%>
        <div class="box_login">
            <div>
                <%: Html.TextBox("UserName", "Email Address", new { @class = "txtbox1", maxlength = "30", onfocus = "if (this.value=='Email Address')this.value=''", onblur = "if (this.value=='')this.value='Email Address'" })%><br />
            </div>
            <div>
                <%: Html.PasswordFor(m => m.Password, new { @class = "txtbox1", maxlength = "12", value = "Password", onfocus = "if (this.value=='Password')this.value=''", onblur = "if (this.value=='')this.value='Password'" })%><br />
            </div> 
            <div id="errorMessage">
                <div class="message"></div>
            </div>
        </div>
        <%} %>
        <div class="boxer_agree">
            <a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>

        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#UserName').blur(function () {
                $(this).removeClass('input_error');
            });

            $('#Password').blur(function () {
                $(this).removeClass('input_error');
            });

            $('#btnSubmit').click(function () {
                var username = $("#UserName").val();
                var password = $("#Password").val();

                if (username == "" || username == "Email Address") {
                    $("#UserName").addClass("input_error");
                }
                if (password == "" || password == "Password") {
                    $("#Password").addClass("input_error");
                }

                if (username == "" || username == "Email Address" || password == "" || password == "Password") {
                    showError(0);
                    return false;
                }
                else {

                    $.postJSON('<%=Url.Action("PaymentLogin","Account") %>', { "username": username, "password": password }, function (data) {
                        if (data.Success == true) {
                            window.location.href = data.ResultUrl;
                            return false;
                        }
                        else {
                            showError(data.Result);
                            return false;
                        }
                    });

                }
                return false;
            });

            $(document).click(function () {
                $('#errorMessage').hide();
            });
        });

        function showError(mode) {
            var html = '';
            if (mode == 0) {
                html = '<b>Email Address and Password can not be blank.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
            }
            else if (mode == 1) {
                html = '<b>Email Address or Password was incorrect.</b><br>Please try again.<br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Account/ForgotPassword">Click here.</a>';
            }
            else if (mode == 2) {
                html = '<b>Your account has been blocked!<br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
            }
            else if (mode == 3) {
                html = '<b>Your have exceeded 5 tries.</b><br><br>New member? <a href="/Home/RegisterGuide">Join us here.</a><br>Forgot password? <a href="/Home/RegisterGuide">Click here.</a>';
            }
            else if (mode == 9) {
                html = '<b>Unknow error!</b><br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
            }
            $('#errorMessage .message').html(html);
            $('#errorMessage').show();
        }
    </script>
</asp:Content>
