﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.NickName%>'s personal profile
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% int epxertid = Model.UserID; %>
   <div class="glo_manage">
    <%Html.RenderPartial("~/Views/Shared/UnregisteredUserLogin.ascx");%>
    	<div class="nav_leader nav_leader_interview">
        	<ul>
                <li class="userinfo"></li>
            	<li class="active"><a href="<%=Url.Action("Index",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsLeaderOwner"].ToString()) ? Html.ActionLink("My Dashboard", "DashBoard", new { leaderid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>
        <div class="manage_boxer">        
            <%Html.RenderPartial("~/Views/Shared/Leader/ProfileLeft.ascx",Model.GLO_Leader);%> 
           <div class="boxer_info">
                	
                    <div class="info_container">
                    
                    <%
                      foreach (var item in Model.GLO_Leader.GLO_LeaderInterviewAnswer)
                      {
                          string QuestionName = item.Question;
                          QuestionName = QuestionName.Replace("Q1.", "");
                          QuestionName = QuestionName.Replace("Q2.", "");
                          QuestionName = QuestionName.Replace("Q3.", "");
                          QuestionName = QuestionName.Replace("Q4.", "");
                          QuestionName = QuestionName.Replace("Q5.", "");
                          QuestionName = QuestionName.Replace("Q6.", "");
                          QuestionName = QuestionName.Replace("Q7.", "");
                          QuestionName = QuestionName.Replace("Q8.", "");
                          QuestionName = QuestionName.Replace("Q9.", "");
                          QuestionName = QuestionName.Replace("Q10.", "");
                          QuestionName = QuestionName.Replace("Q11.", "");
                          QuestionName = QuestionName.Replace("Q12.", "");
                          QuestionName = QuestionName.Replace("Q13.", "");
                          QuestionName = QuestionName.Replace("Q14.", "");
                          QuestionName = QuestionName.Replace("Q15.", "");
                          QuestionName = QuestionName.Replace("Q16.", "");
                          
                          
                          if (item.QuestionID.ToString() != "da726e1a-bb79-4f8d-bfb5-0be6a0d3faab")
                          {%>   
                            <div class="interview_line"><span><%=QuestionName%></span><br /><p class="font"><%=item.Answer.Replace("\n", "<br>")%></p></div>
                          <%}
                          else
                          {
                              List<string> answer = item.Answer.Split('-').ToList();
                              %>
                          <div class="interview_line"><span><%=QuestionName%></span><br /><p class="font"><table class="font"><tr><td></td><td>Current time spent (%)</td><td>Would like to spend (%)</td></tr><tr><td>Family</td><td><%=answer[0] %></td><td><%=answer[1] %></td></tr><tr><td>Job</td><td><%=answer[2] %></td><td><%=answer[3] %></td></tr><tr><td>Mindfulness & meditation</td><td><%=answer[4] %></td><td><%=answer[5] %></td></tr><tr><td>Fitness</td><td><%=answer[6] %></td><td><%=answer[7] %></td></tr></table></p></div>
                         <%}
                      }%>        

                    </div>

                <div class="me_edit3">
            <%if (bool.Parse(ViewData["IsLeaderOwner"].ToString()))
                { %>
                <a href="javascript:void(0)" id="editInterview"><img src="/Styles/images/icon_edit.gif" /></a>
               <%} %>
                </div>
        	</div>
        </div>
        <div class="manage_footer"></div>
   </div>
   <input id ="ImageMark" type="hidden" value ="1" />
  <script type="text/javascript">
    var isOwner='<%=ViewData["IsLeaderOwner"].ToString() %>';
    var basicInformationStatus='<%= Model.GLO_UserEx.BasicInfoStatus %>';
    var interViewStatus='<%= Model.GLO_UserEx.InterViewStatus %>';
    var showExplain='<%= Model.GLO_UserEx.ShowExplain %>';
    var activate='<%= Model.GLO_UserEx.Activate %>';
    $(document).ready(function () {
        $("#editInterview").click(function(){          
            $.post('/Leader/_MyInterView', { "leaderid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data);
                $("#lightboxwrapper").attr("style", "display:block");
            }); 
        });         
        if(isOwner=="True")
        {
            if(activate =="False"){
                $.getJSON('/Leader/_NotActivateMessage', { random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
                });
            }
            else if(showExplain=="True")
            {   
                $("#lightboxwrapper").attr("style", "display:block");
            }
            else if(basicInformationStatus=="False")
            {
                $("#editLeftInfo").click();
            }
            else if(interViewStatus=="False"){
                $("#editInterview").click();
            }
        }
        $(".EditProfile").click(function () {
            $("#editLeftInfo").click();
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
    <div class="lightbox_mask"></div>
<div class="lightbox_manage">
    <div class="lightbox_build">
        Kindly continue to complete your minimum profile information now for GLO to assess and approve your full access free membership:<br /><br />
        <ul>
            <li>Your vision and values as a leader</li>
            <li>Your competencies represented in a graphic timeline of your career, education and contribution to society</li>
            <li>YYour interview</li>
        </ul>
        <br />
        Meanwhile you can search jobs and view companies and experts online but activation of your personal dashboard with direct access to all site functionality is conditional to GLO profile approval.<br /><br />
        <div class="message_center"><a class="EditProfile" href="javascript:void(0);">Continue to build my profile now</a></div>
    </div>
</div>
</div>
</asp:Content>



