﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    JobTag
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="/Js/jquery.tablesorter.js"></script> 
 <div class="glo_manage">
    	<div class="nav_leader nav_leader_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx",Model.GLO_Leader);%> 
                </div>
                <div class="board_list2">
                    <div id="ActiveInfo" class="list_info2">
                        <div class="info_order_area"></div>

                        <div class="infor_tag_container">
                	    <table id="myTable" class="tborder">
                                <thead> 
                                <tr> 
                                    <th width="103" class="list_info_head head_active">By Job Title</th> 
                                    <th width="103" class="list_info_head">By Location</th> 
                                    <th width="103" class="list_info_head head_last">By Start Date</th>  
                                </tr> 
                                </thead> 
                        <%int mark = 1; 
                          foreach (var jobtag in Model.GLO_Leader.GLO_JobTag)
                          { %>

                          	<tr class="<%=mark==1?"message_active":"" %>">
                                <td> <a class="JobTitle" x:jobid="<%=jobtag.GLO_CompanyJob.JobID %>" href="javascript:void(0)"><%=jobtag.GLO_CompanyJob.JobTitle%></a></td>
                          
                             <td width="70">
                                             <%=jobtag.GLO_CompanyJob.Location%>
                              </td>
                              <td width="85">
                                         <%=jobtag.GLO_CompanyJob.PostDate%>
                                         
                                         <%if (jobtag.SendResume)
                                           { %>
                                            <img src="/Styles/images/icon_upload.gif" title="Resume submitted " />
                                         <%}
                                           else
                                           { %>
                                            <img src="/Styles/images/icon_noupload.gif" title="No resume submitted"/>
                                         <%} %>
                              </td>
                          </tr>

                        <%mark++;
                          } %>
                          <tr style="display:none">
                                <td></td>
                                <td></td>
                                <td></td>
                         </tr> 
                    
                        </table>
                        </div>
                    </div>
                </div>
              
                <div class="board_content">
                    <div class="content_info" style="margin-top:35px;height:580px;">
                    <%Html.RenderPartial("~/Views/Shared/Company/JobDetailManage.ascx", Model.GLO_Leader.GLO_JobTag.FirstOrDefault() == null ? new GLO.Data.Models.GLO_CompanyJob() : Model.GLO_Leader.GLO_JobTag.FirstOrDefault().GLO_CompanyJob);%>
                    </div>
                </div>
               
                
            </div>
        </div>
 </div>
 
<script type="text/javascript">
    $(document).ready(function () {
        $("#myTable").tablesorter();
        $(".list_info_head").click(function () {
            $(".list_info_head").removeClass("head_active");
            $(this).addClass("head_active");
        });
        $(".JobTitle").click(function () {
            $(".JobTitle").parent().parent().removeClass("message_active");
            $(this).parent().parent().addClass("message_active");
        });

        $(".JobTitle").click(function () {
            $(".JobTitle").parent().parent().removeClass("message_active");
            $(this).parent().parent().addClass("message_active");

            $.getJSON('/Company/_GetJobDetailManage', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                {
                    $('.content_info').html(data.Html);
                }
            });
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">       
</div>
</asp:Content>
