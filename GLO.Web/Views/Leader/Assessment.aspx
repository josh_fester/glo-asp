﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Assessment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_leader nav_leader_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx");%>                  
                </div>
               
                <div class="board_right">
                <div class="box_form">

                    <div style="padding:5px 0;">
                        <a href="/Leader/TakeGLOWisdom" target="_blank">Take the GLO Wise Check Up</a>
                    </div>
                    <div style="margin:20px 0 0 0;padding:5px 0;">
                        Assessment report: <a id="AssessmentFile" href="/Images/<%=Model.GLO_Leader.Assessment %>" target="_blank"> <%= !string.IsNullOrEmpty(Model.GLO_Leader.Assessment) ? "My Assessment" : ""%></a>
                        <%if (!string.IsNullOrEmpty(Model.GLO_Leader.Assessment))
                          { %>
                        <a class="deleteAssessment"  href="javascript:void(0)"><img src="/Styles/images/icon_delete1.gif"></a>
                        <%} %>
                   </div>
                   <div style="margin:10px 0 0 0;padding:5px 0;">
                        <div style="float:left;padding:5px 0;">Upload assessment report:</div>
                        <div style="float:left;width:100px;">
                            <input type="button" id="file_upload" class="graybtn" value="Select file"/>
                        </div>                         
                   </div>                    

                </div>
                </div>
                
            </div>
        </div>
 </div> 

<script type="text/javascript">
    $(document).ready(function () {
        $('#file_upload').uploadify({
            'height': 25,
            'width': 90,
            'auto': true,
            'multi': false,
            'buttonText': 'Select file',
            'fileTypeExts': '*.*;',
            'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
            'uploader': '/FileUpload/UploadAssessMent?userid='+<%=Model.UserID %>,
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                $("#AssessmentFile").attr("href", data.Urlpath);
                $("#AssessmentFile").html("My Assessment");
                window.location.reload();
            }
        });

        $(".deleteAssessment").click(function(){
               confirmbox('Do you want to delete this assessment?', function () {
                $.postJSONnoQS('<%=Url.Action("DeleteAssessment") %>', null, function (data) {
                    if (data) {
                        alertbox("The assessment has been deleted successfully.", function () {
                            window.location.reload();
                        });
                        return false;
                    }
                });  
        });
         });
    });
 </script>

</asp:Content>