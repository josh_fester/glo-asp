﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  <%=Model.NickName %>'s  MyCompetencies
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID;%>
    <input type="hidden" id="hidexpertid" value ="<%=epxertid %>" />   
   <div class="glo_manage">
    	<div class="nav_leader nav_leader_competence">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li class="active"><a href="<%=Url.Action("MyCompetence",new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsLeaderOwner"].ToString()) ? Html.ActionLink("My Dashboard", "DashBoard", new { leaderid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>
        <div class="manage_boxer">        
               <%Html.RenderPartial("~/Views/Shared/Leader/ProfileLeft.ascx",Model.GLO_Leader);%> 
           <div class="boxer_info">          
             <%Html.RenderPartial("~/Views/Shared/Competence/MyCompetenceView.ascx", Model.GLO_Leader.ExpertCompetence);%>
                <div class="me_edit3">
                        <%if (bool.Parse(ViewData["IsLeaderOwner"].ToString()))
                          { %>
                <a href="javascript:void(0)" id="editProfile"><img src="/Styles/images/icon_edit.gif" /></a>
            <%} %>
                </div>
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>
    <script type="text/javascript">
             var competenciesStatus='<%= Model.GLO_UserEx.CompetenciesStatus %>';
             $(document).ready(function () {
                 $("#editLeftInfo").click(function(){
                   $.getJSON('_EidtLeftInfo', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {                     
                       $("#lightboxwrapper").attr("style", "display:block");
                       $('#lightboxwrapper').empty().html(data.Html);
                    });                  
                 });
                 if(competenciesStatus=="False")
                {
                    $("#editProfile").click();
                }
             });
        </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>