﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_CompanyJob>" %>
 <%if (!Request.IsAuthenticated)
    {  %>
<%--    <div class="jobinfo_mask">
        <div class="jobinfo_build">
            We are sorry, only GLO registered members are able to see company profile details.<br /><br />
            Join our GLO leadership community now and gain access to 'good' jobs and connect with companies and HR experts.<br /><br />
            Building a leader profile is FREE and only takes 10 minutes to provide basic info.<br /><br />
            Companies and HR experts can also apply for GLO membership and build a profile online.<br /><br /><br />

            <div class="message_center"><a href="/Home/RegisterGuide">Build my profile now!</a></div>
        </div>
    </div>--%>
<%} %>

<%if (!string.IsNullOrEmpty(Model.JobTitle))
  { %>
<table class="ganeral">      
  <tr>
    <td colspan="2" class="ganeral_title"><%=Model.JobTitle %></td>
    </tr>
    <tr>
    <td>
        <table>
            <tr>
                <td>Company:</td>
                <td><a class="font" href="/company/FactSheet?companyid=<%=Model.GLO_Company.UserID %>" target="_blank"><%=Model.GLO_Company.CompanyName %></a></td>
            </tr>
            <tr>
                <td>Job ID:</td>
                <td><span class="gray font"><%=Model.JobNO %></span></td>
            </tr>
           
            <tr>
                <td>Location:</td>
                <td><span class="gray font"><%=Model.Location %></span></td>
            </tr>
            <tr>
                <td>Industry:</td>
                <td><span class="gray font"><%=Model.Industry %></span></td>
            </tr>
            <tr>
                <td>Function:</td>
                <td><span class="gray font"><%=Model.JobFunction %></span></td>
            </tr>
             <%if (!string.IsNullOrEmpty(Model.PostDate))
              { %>
            <tr>
                <td>Posted:</td>
                <td><span class="gray font"><%=Model.PostDate%></span></td>
            </tr>
            <%} %>
            <%if (!string.IsNullOrEmpty(Model.Salary))
              { %>
            <tr>
                <td>Salary:</td>
                <td><span class="gray font"><%=Model.Salary%></span></td>
            </tr>
            <%} %>
        </table>
    </td>
    <td width="200" class="ganeral_logo">
        <a href="/company/FactSheet?companyid=<%=Model.GLO_Company.UserID %>" target="_blank"><img src="/Images/<%=!string.IsNullOrEmpty(Model.GLO_Company.LogoUrl)? Model.GLO_Company.LogoUrl : "user_none.gif" %>" /></a>
    </td>
    </tr>
</table>
<table class="detail">
    <tr>
        <td>
                      
                        <%if (!string.IsNullOrEmpty(Model.JobSummary))
                          { %>
                        <div class="info_headline">Job Summary</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.JobSummary) ? Model.JobSummary.Replace("\n", "<br>") : ""%></div>
                        <%} %>

                        <div class="info_headline">Key Responsibilities</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.KeyResponsibility)? Model.KeyResponsibility.Replace("\n","<br>"):"" %></div>
                        <div class="info_headline">Key Competencies</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.KeyCompetence)? Model.KeyCompetence.Replace("\n","<br>"):"" %></div>

                        <%if (!string.IsNullOrEmpty(Model.Education))
                          { %>
                        <div class="info_headline">Education</div>
                        <div class="info_description font"><%=Model.Education%></div>
                        <%}
      if (!string.IsNullOrEmpty(Model.ContactPerson))
      { %>
                        <div class="info_headline">Contact Person</div>
                        <div class="info_description font"><%=Model.ContactPerson%></div>
                        <%} %>

                        <%if (!string.IsNullOrEmpty(Model.DefinedTitle) && !string.IsNullOrEmpty(Model.DefinedContent))
                          { %>
                            <div class="info_headline"><%=Model.DefinedTitle %></div>
                            <div class="info_description font"><%=!string.IsNullOrEmpty(Model.DefinedContent)? Model.DefinedContent.Replace("\n","<br>"):"" %></div>
                        <%} %>
        </td>
        <td>
        </td>
    </tr>
</table>
<%} %>
<% if (Request.IsAuthenticated)
   {
       var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>; 
       int tagjobid = Model.JobID;
       if (user.UserData.UserType == 1 && Model.JobID > 0 && user.UserData.Approved)
       {%>
<div class="resume_box">
<%if (Model.GLO_JobTag.FirstOrDefault(x => x.JobID == tagjobid && x.UserID == user.UserData.UserID) != null)
  { %>
            <input type="button" id="TaggedJob" class="graybtn" value="Tag Job" onclick="return false;" />

<%}
  else
  { %>
            <input type="button" id="TagJob"  class="bluebtn" x:jobid="<%=Model.JobID %>" value="Tag Job" />

<%} %>


        <input type="button" id="SubmitResume" class="bluebtn" x:jobid="<%=Model.JobID %>" value="Submit Resume" />
    </div>
<%}
   }%>


<script type="text/javascript">
    $(document).ready(function () {
        $("#TagJob").click(function () {
            $.postJSONnoQS('<%=Url.Action("TagJob","Leader") %>', { "jobid": $(this).attr("x:jobid") }, function (data) {
                if (data) {
                    alertbox("The job ad was successfully tagged!");
                    $('#TagJob').removeClass("bluebtn");
                    $('#TagJob').addClass("graybtn");
                    $('#TagJob').val("Tag Job");
                    $('#TagJob').attr("disabled", "disabled");
                }
                else {
                    alertbox("The job exists in your job tagged list!");
                }
                return false;
            });
        });

        $("#SubmitResume").click(function () {
            $.getJSON('<%=Url.Action("_SubmitResume","Leader") %>', { jobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                $("#lightboxwrapper").attr("style", "display:block");
                $('#lightboxwrapper').empty().html(data.Html);
            });
        });

    });     
</script>


