﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<GLO.Data.Models.GLO_TagUsers>>" %>

<%foreach(var taguser in Model){ %>
<div class="select_info_line">
<div class="select_info_icon">
    <a href="javascript:void(0)">
        <img src="/Images/<%=taguser.GLO_Users.Icon %>" /></a>
    <div class="select_info_name">
    
        <%=taguser.GLO_Users.NickName %></div>
</div>
<div class="select_info_user">
    <div class="select_info_item">
        <%=taguser.GLO_Users.GLO_Leader.NickName%>, <%=taguser.GLO_Users.GLO_Leader.Title%>, <%=taguser.GLO_Users.GLO_Leader.Location%></div>
    <div class="select_info_item">
        <%=taguser.TagNote %></div>
    <div class="select_info_button">
        <input type="button" class="btn_select_info" value="Email" />
        <input type="button" x:tagid="<%=taguser.TagID %>" class="btn_select_info delete_tag" value="Delete" />
    </div>
</div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        /*open dropdown*/
        $(".delete_tag").click(function () {
          if(confirm("Are you sure delete this user from your tag list"))
          {
              $(this).parent().parent().parent().remove();
              return false;
          }
        });
    });
</script>

