﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<GLO.Data.Models.GLO_TagCategory>>" %>
<ul>
                                    
<%foreach (var funciton in Model){ %>

   <li><%=funciton.GLO_Functions.FunctionName %></li>

<%} %>
</ul>

<script type="text/javascript">
    $(document).ready(function () {

        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();

            var value = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(value);

            if (value != "Select Job Function" && value != "All Function") {
                $(this).parent().parent().parent().removeClass("select_error");
            }

        });
    });
</script>