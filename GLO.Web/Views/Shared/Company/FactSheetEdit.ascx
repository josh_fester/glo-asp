﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Company>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_useredit">
        <div class="nav_company_edit nav_company_edit_factsheet">
            Fact Sheet</div>
        <div class="box_useredit">
            <div class="boxer_user">
             <% var classname = "box_user_private2";
           var ispublish = "0";
           if (Model.GLO_Users.IsPublish)
           {
               classname = "box_user_private_on2";
               ispublish = "1";

           } %>
        <div class="<%=classname%>" x:ispublish="<%=ispublish %>" id="publish_or_private" onclick="ResetPrivate(this);"></div>
         <div id="publish_descript" class="privatelayer">
         Please select to make your company profile visible to leaders and HR experts registered on the GLO community.<br />
    If not visible, they cannot tag you or request to be placed in your talent pipeline. <br />
    If blue your profile will be public, if grey it is set on private.<br />
    In any case, after registration, you will be able to manage your job ads via the dedicated dashboard.
         </div>
                <div class="box_user_photo2">
                    <img id="imageID" src="/Images/<%=!string.IsNullOrEmpty(Model.LogoUrl)? Model.LogoUrl:"user_none.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.LogoUrl)? Model.LogoUrl:"" %>" />
                    <div class="photo_item_edit">

                        <input type="button" id="file_upload" value="Select Photo" />
                        <input type="button" id="file_delete" class="btngray" value="Delete image"/>
                    </div>
                </div>
                <div class="box_user_detail">
                    <input id="CompanyName" class="txtbox_user1" value="<%=Model.CompanyName %>" onfocus="if (this.value=='Company Name')this.value=''" onblur="if (this.value=='')this.value='Company Name'" />
                    <textarea id="CompanyIntroduce" class="txtbox_user5"><%=!string.IsNullOrEmpty(Model.CompanyIntroduce) ? Model.CompanyIntroduce : "Share here your organization’s vision"%></textarea>
                </div>
                <div class="box_user_value">
                    <div class="box_user_value_title">My organization’s values:</div>
                    <div id="selectvalue" class="user_value_custom">
                     <%if (!string.IsNullOrEmpty(Model.CompanyValue1))
                          {
                              foreach (var value in Model.CompanyValue1.Split(';'))
                              {
                                  if (!string.IsNullOrEmpty(value))
                                  {%>
                             <div class="value_item_active font" x:value="<%=value %>"><%=value%>
                             <div class="user_value_delete"><a href="javascript:void(0);" title="delete"><img src="/Styles/images/icon_cancel.gif"></a></div>
                             </div>

                        
                        <%}
                              }
                          } %>
                    </div>
                </div>
                 <div class="user_value_add">
                    Custom:<input type="text" value="" class="txt_itemadd font" id="txtExpertValue" maxlength="30" /><input type="hidden" id="hidExpertValueID" /><input type="button" id="btnAddExpertValue" value="Add" class="bluebtn" />
                </div>

            </div>
            <div class="boxer_info boxer_company_sheet">
                <div class="info_company">

                                <div class="info_headline">
                                    Location</div>
                                <div class="info_description">
                                  <div class="multiple_box">
                                    <%: Html.Hidden("LocationID", Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().LID.ToString() : "0", new { @class = "multiple_locationid" })%>
                                    <input class="multiple_value font" id="Location" name="Location" type="text" readonly="readonly" value="<%= Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName : "Select a location"%>" />
                                    <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                                      <div class="multiple_wrapper multiple_wrapper_location"></div> 
                                  </div>                                  
                                </div>
                                
                                <div class="info_headline">
                                    Industry</div>
                                <div class="info_description">
                                  <div class="multiple_box">
                                    <%: Html.Hidden("IndustryID", Model.GLO_Users.GLO_UserIndustry.Count > 0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().IndustryID.ToString() : "0", new { @class = "multiple_id" })%>
                                   <input class="multiple_value font" id="Industry" type="text" readonly="readonly" value="<%= Model.GLO_Users.GLO_UserIndustry.Count>0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName : "Select an industry" %>" />
                                    <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                                      <div class="multiple_wrapper multiple_wrapper_industry"></div> 
                                  </div>                                  
                                </div>

                                <div class="info_headline">
                                    Employees</div>
                                <div class="info_description">
                                    <div class="select_box">
                                        <input class="select_value font" id="drpSize" type="text" readonly="readonly" value="<%=Model.Size %>" />
                                        <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                                        <div class="select_option font">
                                            <ul>
                                                <li>1-5</li>
                                                <li>6–50</li>
                                                <li>51–250</li>
                                                <li>251–1000</li>
                                                <li>1001–5000</li>
                                                <li>>5000</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="info_headline">
                                    Revenue</div>
                                <div class="info_description">
                                    <div class="select_box">
                                        <input class="select_value font" id="drpRevenue" type="text" readonly="readonly" value="<%=Model.Revenue %>" />
                                        <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                                        <div class="select_option font">
                                            <ul>
                                                <li><1,000,000 USD</li>
                                                <li>1,000,001-10,000,000 USD</li>
                                                <li>10,000,001–50,000,000 USD</li>
                                                <li>50,000,001–250,000,000 USD</li>
                                                <li>250,000,001–1,000,000,000 USD</li>
                                                <li>>1,000,000,001 USD</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="info_headline">
                                    Awards</div>
                                <div class="info_description">
                                    <textarea id="Awards" class="area_sheet font"><%=Model.Awards %></textarea>
                                </div>
                                <div class="info_headline">
                                    Contact Us</div>
                                <div class="info_description">
                                    <textarea id="ContactInfo" class="area_sheet font"><%=Model.ContactInfo %></textarea>
                                </div>
                                <div class="info_headline">
                                    Website</div>
                                <div class="info_description">
                                    <input type="text" id="Website" class="text_sheet font" value="<%=Model.Website %>"/>                                    
                                </div>
                        <div class="info_contact">
                        <table class="tbcontact" style="float:none">
                            <tr>
                                <td>
                                    <div class="contact_box" style="margin-left:25px;">  
                                       <img id="CPerson1Image" src="/Images/<%=!string.IsNullOrEmpty(Model.CPerson1Image)?Model.CPerson1Image:"user_none.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.CPerson1Image)?Model.CPerson1Image:"" %>" />
                                    </div>
                                    <div class="contact_item">
                                        <input type="button" id="upload_contact1" value="Select" />
                                        <input type="button" id="file_deletecontact1" class="btngray" value="Delete"/>
                                     </div>
                                </td>
                                <td valign="top">
                                    <div class="contact_line">
                                        <input type="text" class="font" id="CPerson1" value="<%=string.IsNullOrEmpty(Model.CPerson1) ? "contact person" : Model.CPerson1%>" onfocus="if (this.value=='contact person')this.value=''" onblur="if (this.value=='')this.value='contact person'" />
                                    </div>
                                    <div class="contact_line">
                                        <input type="text" class="font" id="CPerson1Email" value="<%=string.IsNullOrEmpty(Model.CPerson1Email) ? "contact email" : Model.CPerson1Email%>" onfocus="if (this.value=='contact email')this.value=''" onblur="if (this.value=='')this.value='contact email'" />
                                    </div>
                                </td>
                                <td>
                                    <div class="contact_box" style="margin-left:25px;">  
                                       <img id="Cperson2Image" src="/Images/<%=!string.IsNullOrEmpty(Model.Cperson2Image)?Model.Cperson2Image:"user_none.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.Cperson2Image)?Model.Cperson2Image:"" %>" />
                                    </div>
                                    <div class="contact_item">
                                        <input type="button" id="upload_contact2" value="Select" />
                                        <input type="button" id="file_deletecontact2" class="btngray" value="Delete"/>
                                    </div>
                                </td>
                                <td valign="top">
                                    <div class="contact_line">
                                        <input type="text" class="font" id="CPerson2" value="<%=string.IsNullOrEmpty(Model.CPerson2) ? "contact person" : Model.CPerson2%>" onfocus="if (this.value=='contact person')this.value=''" onblur="if (this.value=='')this.value='contact person'" />
                                    </div>
                                    <div class="contact_line">
                                        <input type="text" class="font" id="CPersom2Email" value="<%=string.IsNullOrEmpty(Model.CPersom2Email) ? "contact email" : Model.CPersom2Email%>" onfocus="if (this.value=='contact email')this.value=''" onblur="if (this.value=='')this.value='contact email'" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </div>

                </div>
            </div>
        </div>
        <div class="me_apply">
            <a href="javascript:void(0)" id="btnApply">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close2">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
          var nameArry = [];
        $(document).ready(function () {

        /*open dropdown*/
            $(".select_box").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                $(".select_box").css("z-index", "1000");
                $(this).css("z-index", "1001");
                $(this).find(".select_option").toggle();
            });
            /*close dropdown*/
            $(document).click(function () {
                $('.select_option').hide();
                $(".select_box").css("z-index", "1000");
            });
            /*set value*/
            $(".select_option li").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                $(".select_box").css("z-index", "1000");
                
                var text = $(this).text().replace(/<img([^>]*)>/gi, "");
                $(this).parent().parent().parent().find(".select_value").val(text);

            }); 
            
            $.post("/Expert/GetExpertValueList", null, function (data) {
                $.each(data, function () {
                    var nameitem = { name: this.Text, id: this.Value };
                    nameArry.push(nameitem);            
                });
            });
            
            $.getJSON('/Home/_GetLocationList', null, function (data) {
                        {
                            $('.multiple_wrapper_location').html(data.Html);
                        }
             });
            
            $.getJSON('/Home/_GetIndustryList', null, function (data) {
                    {
                        $('.multiple_wrapper_industry').html(data.Html);
                    }
            });

            $("#txtExpertValue").autocomplete({
                minLength: 1,
                autoFocuus: true,
                source: function (req, res) {
                    var name = req.term,
                                result = [];
                    if ($.trim(req.term) != "") {
                        var searchResult = $.map(
                                    (name ? $.grep(nameArry, function (value) {
                                        return value.name.toString().toLocaleLowerCase().indexOf(name) >= 0;
                                    }) : nameArry),
                                    function (value) {
                                        return { label: value.name, tc: value.id };
                                    });
                        result = result.concat($.makeArray(searchResult));
                    }
                    res(result);
                },
                select: function (event, ui) {
                    $('#hidExpertValueID').val(ui.item.tc);
                }
            });

             $("#btnAddExpertValue").click(function () {                              
                var value=$('#txtExpertValue').val().trim();
                if(value=="") return false;

                $.postJSON('<%=Url.Action("AddExpertValue","Expert") %>', { value:value}, function (data) {
                    if(data!=""){
                        var html= "<div class=\"value_item_active\" x:value=\""+ value +"\" x:relationalID=\""+ data +"\">"+value+"<div class=\"user_value_delete\"><a id=\""+ data +"\" title=\"delete\" href=\"javascript:void(0);\"><img src=\"/Styles/images/icon_cancel.gif\"></a></div></div>";
                        $("#selectvalue").append(html);
                        $('#txtExpertValue').val("");
                    }
                    else{
                        alertbox("Add failed.");
                    }
                });
            });
            
            /*delete value*/
            $(".user_value_delete a").live("click", function () {
             $(this).parent().parent().remove();
//                var id = $(this).attr("id");
//                $.getJSON('<%=Url.Action("DeleteValue","Expert") %>', { "id": id, random: Math.random() }, function (data) {
//                    if (data) {
//                        $('.value_item_active').each(function () {
//                            if ($(this).attr("x:relationalID") == id) {
//                                $(this).remove();
//                            }
//                        });
//                    }
//                });
            });

            $("#btnApply").click(function () {
                if (isEmpty($("#CompanyName").val())) {
                    $("#CompanyName").addClass("input_error");
                    return;
                }
                else {
                    $("#CompanyName").removeClass("input_error");
                }

                 if($("#Website").val()!=""&&!IsURL($("#Website").val()))
                  {        
                      $("#Website").addClass("input_error");
                      return false;
                  }
                  else{                  
                        $("#Website").removeClass("input_error");
                  }
                  var companyvalue="";
                  $('.user_value_custom .value_item_active').each(function () {
                          companyvalue += $(this).attr("x:value")+";";
                        });
                  var companyvalue1=companyvalue;
                  var companyvalue2="";//$('#ExpertValue2').val() == "Select" ? "" : $("#ExpertValue2").val();
                  var companyvalue3="";//$('#ExpertValue3').val() == "Select" ? "" : $("#ExpertValue3").val();
                  var website = $("#Website").val();
                  if (website!=""&&website.toLowerCase().indexOf("http://") != 0 && website.toLowerCase().indexOf("https://") != 0) {
                    website = "http://" + website;
                  }

                  var jsondata ={
                         'UserID':<%=Model.UserID %>,
                         'Industry':$("#Industry").val(),
                         'Size':$("#drpSize").val(),
                         'Revenue':$("#drpRevenue").val(),
                         'Awards':$("#Awards").val(),
                         'CompanyName':$("#CompanyName").val(),
                         'CompanyIntroduce':$("#CompanyIntroduce").val(),
                         'LogoUrl':$("#imageID").attr("x:name"),
                         'Website':website,
                         'ContactInfo':$("#ContactInfo").val(),
                         'CPerson1':$("#CPerson1").val()!="contact person"?$("#CPerson1").val():"",
                         'CPerson1Email':$("#CPerson1Email").val()!="contact email"?$("#CPerson1Email").val():"",
                         'CPerson1Image':$("#CPerson1Image").attr("x:name"),
                         'CPerson2':$("#CPerson2").val()!="contact person"?$("#CPerson2").val():"",
                         'CPersom2Email':$("#CPersom2Email").val()!="contact email"?$("#CPersom2Email").val():"",
                         'Cperson2Image':$("#Cperson2Image").attr("x:name"),
                         'CompanyValue1':companyvalue1,
                         'CompanyValue2':companyvalue2,
                         'CompanyValue3':companyvalue3
                        
                   };
                $.postJSONnoQS('<%=Url.Action("CompanyFactSheetEdit") %>', { "jsondata": $.toJSON(jsondata),ispublish:$("#publish_or_private").attr("x:ispublish"),LocationID:$("#LocationID").val(),IndustryID:$('#IndustryID').val() }, function (data) {
 
                    if (data == "1") {
                        window.location.href = "/Company/CompanyIndex?companyid=" + <%=Model.UserID %>;
                    }
                    else if (data == "3") {
                        window.location.href = "/Company/RegisterThankYou?companyid=" + <%=Model.UserID %>;
                    }
                    else {
                        window.location.reload();
                        return false;
                    }
                });
            });


            
            $('#file_upload').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/FileUpload/Upload?width=186&height=186',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    $("#imageID").removeAttr("src");
                    $("#imageID").removeAttr("x:name");
                    $("#imageID").attr("src",data.Urlpath)
                    $("#imageID").attr("x:name", data.SaveName);   
                             
                    $(".uploadify-queue").css('display','none');      
                    $('#file_upload').css('display','none');
                    $('#file_delete').css('display','');           
                }
            });
            $('#file_delete').click(function(){
                $("#imageID").removeAttr("src");
                $("#imageID").removeAttr("x:name");
                $("#imageID").attr("src","/Images/user_none.gif")
                $("#imageID").attr("x:name", ""); 

                $('#file_upload').css('display','');
                $('#file_delete').css('display','none');
            });
                 
            var logoUrl='<%=Model.LogoUrl %>';
            if (logoUrl != "" && logoUrl.indexOf("user_none.gif") == -1)
            {
                $('#file_upload').css('display','none');
                $('#file_delete').css('display','');
            }
            else{
                $('#file_upload').css('display','');
                $('#file_delete').css('display','none');
            }

            $("#publish_or_private").hover(function () {
            $(this).css("z-index", "9");
            $("#publish_descript").show();
           
            }, function () {
                $(this).css("z-index", "8");
                $("#publish_descript").hide();
            });

                
            $('#upload_contact1').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/FileUpload/Upload?width=50&height=50',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    $("#CPerson1Image").removeAttr("src");
                    $("#CPerson1Image").attr("src",data.Urlpath)
                    $("#CPerson1Image").attr("x:name", data.SaveName);                   
                        
                    $(".uploadify-queue").css('display','none');
                    $('#upload_contact1').css('display','none');
                    $('#file_deletecontact1').css('display',''); 
                }
            });
                $('#file_deletecontact1').click(function(){
                $("#CPerson1Image").removeAttr("src");
                $("#CPerson1Image").removeAttr("x:name");
                $("#CPerson1Image").attr("src","/Images/user_none.gif")
                $("#CPerson1Image").attr("x:name", ""); 

                $('#upload_contact1').css('display','');
                $('#file_deletecontact1').css('display','none');
                });
            var CPerson1Image='<%=Model.CPerson1Image %>';
            if (CPerson1Image != "" && CPerson1Image.indexOf("user_none.gif") == -1)
            {
                $('#upload_contact1').css('display','none');
                $('#file_deletecontact1').css('display','');
            }
            else{
                $('#upload_contact1').css('display','');
                $('#file_deletecontact1').css('display','none');
            }
                
            $('#upload_contact2').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/FileUpload/Upload?width=50&height=50',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    $("#Cperson2Image").removeAttr("src");
                    $("#CPerson2Image").removeAttr("x:name");
                    $("#Cperson2Image").attr("src",data.Urlpath)
                    $("#Cperson2Image").attr("x:name", data.SaveName);                         
                        
                    $(".uploadify-queue").css('display','none');
                    $('#upload_contact2').css('display','none');
                    $('#file_deletecontact2').css('display','');            
                }
            });
                $('#file_deletecontact2').click(function(){
                $("#Cperson2Image").removeAttr("src");
                $("#CPerson2Image").removeAttr("x:name");
                $("#Cperson2Image").attr("src","/Images/user_none.gif")
                $("#Cperson2Image").attr("x:name", ""); 

                $('#upload_contact2').css('display','');
                $('#file_deletecontact2').css('display','none');
                });
            var CPerson2Image='<%=Model.Cperson2Image %>';
            if (CPerson2Image != "" && CPerson2Image.indexOf("user_none.gif") == -1)
            {
                $('#upload_contact2').css('display','none');
                $('#file_deletecontact2').css('display','');
            }
            else{
                $('#upload_contact2').css('display','');
                $('#file_deletecontact2').css('display','none');
            }
        });

         function ResetPrivate(obj) {
            if (obj.className == 'box_user_private_on2') {
                obj.className = 'box_user_private2';
                $(obj).attr("x:ispublish","0");
            }
            else {
                obj.className = 'box_user_private_on2';
                 $(obj).attr("x:ispublish","1");
            }
        }

             
            
</script>
