﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HuntingRequest>" %>
 <div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_box3">
        
        	<div class="nav_company_edit nav_company_edit_assignment">Our Dashboard</div>
            <div class="box_assignment_edit">
                <div class="box_form samll_form">
                     <table>
                        <tr>
                          <td valign="top" width="110"><label for="UserName">Subject</label></td>
                            <td>
                          
                                <input type="text" class="txt_form font" value="<%=Model.SubJect %>"  id="Subject" maxlength="100"><span class="required">*</span><br>                                                      
                            </td>
                          </tr>
                         
                         <tr>
                            <td valign="top"><label for="Password">Job Title</label></td>
                            <td>
                            <input type="text" class="txt_form font" value="<%=Model.JobTitle %>" id="JobTitle" maxlength="100"><span class="required">*</span><br>
                           
                              
                            </td>
                        </tr>                     
                         <tr>
                            <td valign="top"><label for="Password">Location</label></td>
                            <td>
                                <input type="text" class="txt_form font" value="<%=Model.Location %>"  id="Location" maxlength="50"><br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="UserName">Responsible leader</label></td>
                            <td>  
                                <input type="text" class="txt_form font" value="<%=Model.Company %>" id="Company"><br>
                                                     
                            </td>
                        </tr> 
                         
                         <tr>
                            <td valign="top"><label for="Password">Executive search expert</label></td>
                            <td> 
                                <input type="text" class="txt_form font" value="<%=Model.ContactPerson %>"  id="ContactPerson"><br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Start date</label></td>
                            <td>
                           
                                <input type="text" class="txt_form font" value=" <%=Model.ContactDetail %>" readonly="readonly"  id="ContactDetail"><br>
                               
                            </td>
                        </tr>
                        
                         <tr>
                            <td valign="top"><label for="Password">Notes</label></td>
                            <td>                        
                                 <textarea id="Comment" class="area_form font"><%=Model.Comment %></textarea>
                               <br>
                               
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>  &nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="me_apply"><a href="javascript:void(0)" id="hunting_save"><img src="/Styles/images/icon_apply.gif"></a></div>
            <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>   
        </div>
    
    </div>
     <script type="text/javascript">
     $(document).ready(function () {
         $("#hunting_save").click(function () {
            if(ValidateHunting() == false){
                return;
            }
            var jsondata ={
                         'HuntingID':<%=Model.HuntingID %>,
                         'UserID':<%=Model.UserID %>,
                         'Subject':$('#Subject').val(),
                         'Company':$("#Company").val(),
                         'JobTitle':$("#JobTitle").val(),
                         'ContactDetail':$("#ContactDetail").val(),
                         'Location':$('#Location').val(),
                         'ContactPerson':$('#ContactPerson').val(),
                         'Comment':$('#Comment').val()
                   };
           var huntingID= '<%=Model.HuntingID %>';
           var isArchive = '<%=Model.IsArchive %>';
           var newHtml = "";
           if(isArchive=="False")
           {
             newHtml = "<td width=\"148\"><a class=\"huntingtitle\" x:huntingid=\""+ <%=Model.HuntingID %> +"\" href=\"javascript:void(0)\">"+$('#Subject').val()+"</a></td><td width=\"147\">"+$('#Location').val()+"</td><td width=\"130\">"+$("#ContactDetail").val()+"</td>"
             }else{
             newHtml = "<td><a class=\"huntingtitle\" x:huntingid=\""+ <%=Model.HuntingID %> +"\" href=\"javascript:void(0)\">"+$('#Subject').val()+"</a></td><td>"+$('#Location').val()+"</td><td>"+$("#ContactDetail").val()+"</td><td width=\"16\" style=\"padding-left:0;\"><a class=\"deletehunting\" x:hungtingid=\""+ <%=Model.HuntingID %> +"\" href=\"javascript:void(0)\"><img src=\"/Styles/images/icon_delete1.gif\" /></a></td>"
             }           
           $.postJSON('<%=Url.Action("HuntingEdit") %>', { jsondata:$.toJSON(jsondata) }, function (data) {   

                          if(isArchive=="False")
                        { 
                         $('#ActiveHunting'+huntingID).html(newHtml);
                         }else{               
                         $('#Hunting'+huntingID).html(newHtml); 
                         }
                        <%if(Model.HuntingID>0){ %>
                         $.getJSON('_HuntingDetail', { "huntingid":<%=Model.HuntingID %>, random: Math.random() }, function (data) {
                                        $('.content_info').empty().html(data.Html);
                                    });
                         CloseLightBox();
                         <%}else{ %>
                         window.location.reload();
                         <%} %>
                         return false;
                     });
         });

          $("#ContactDetail").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOtherMonths: true
        });

     });
      $("#Subject").live("blur", function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });
            $("#JobTitle").live("blur", function(){ 
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });

       function ValidateHunting()
                 { 
                   var error = new Array();
                   if(isEmpty($("#Subject").val()))
                   { 
                      $("#Subject").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#Subject").removeClass("input_error");
                   }

                   if(isEmpty($("#JobTitle").val()))
                   { 
                      $("#JobTitle").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#JobTitle").removeClass("input_error");
                   }
                                      
                   if(error.length>0)
                   return false;
                   else 
                   return true;
                 }
 </script>