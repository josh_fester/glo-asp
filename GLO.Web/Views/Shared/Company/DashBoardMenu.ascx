﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>; %>
<ul>
    <li class='<%=ViewBag.Select == "DashBoard" ? "active" : ""%>'><a href="<%=Url.Action("DashBoard","Company",new { companyid = user.UserData.UserID }) %>">Our Tagged Talents</a></li>
    <li class='<%=ViewBag.Select == "TagExpertList" ? "active" : ""%>'><a href="<%=Url.Action("TagExpertList","Company",new { companyid = user.UserData.UserID }) %>">Our Tagged Experts</a></li>
    <li class='<%=ViewBag.Select == "JobAdList" ? "active" : ""%>'><a href="<%=Url.Action("JobAdList","Company",new { companyid = user.UserData.UserID }) %>">Jobs Advertised</a></li>
    <li class='<%=ViewBag.Select == "PlaceJobAd" ? "active" : ""%>'><a href="<%=Url.Action("PlaceJobAd","Company",new { companyid = user.UserData.UserID }) %>">Place A Job Ad</a></li>
    <li class='<%=ViewBag.Select == "CurrentHunting" ? "active" : ""%>'><a href="<%=Url.Action("CurrentHunting","Company",new { companyid = user.UserData.UserID }) %>">Current Headhunting</a></li>
    <li class='<%=ViewBag.Select == "RequestHunting" ? "active" : ""%>'><a href="<%=Url.Action("RequestHunting","Company",new { companyid = user.UserData.UserID }) %>">Request Headhunting</a></li>
    <li class='<%=ViewBag.Select == "BookAssessment" ? "active" : ""%>'><a href="<%=Url.Action("BookAssessment","Company",new { companyid = user.UserData.UserID }) %>">Book An Assessment</a></li>
    <li class='<%=ViewBag.Select == "ChangePassword" ? "active" : ""%>'><a href="<%=Url.Action("ChangePassword","Company",new { companyid = user.UserData.UserID }) %>">Change Password</a></li>
    <li class="level"><div><a href="javascript:void(0)">Message</a><span id="UnReadMessageCount"><%if(Session["UnReadMessageCount"]==null){ %>0<%}else{ %><%=Session["UnReadMessageCount"].ToString()%><%} %></span></div>
        <ul>

             <li class='<%=ViewBag.Select == "ComposeMessage" ? "active" : ""%>'><a href="<%=Url.Action("CompanyComposeMessage","Message",new { companyid = user.UserData.UserID }) %>">Compose new message</a></li>
             <li class='<%=ViewBag.Select == "Inbox" ? "active" : ""%>'><a href="<%=Url.Action("CompanyMessage","Message",new { companyid = user.UserData.UserID,status=0 }) %>">Inbox</a></li>
             <li class='<%=ViewBag.Select == "Sent" ? "active" : ""%>'><a href="<%=Url.Action("CompanyMessage","Message",new { companyid = user.UserData.UserID,status=1 }) %>">Sent</a></li>
             <li  class='<%=ViewBag.Select == "Archive" ? "active" : ""%>'><a href="<%=Url.Action("CompanyMessage","Message",new { companyid = user.UserData.UserID, status =2 }) %>">Archive</a></li>
            <li  class='<%=ViewBag.Select == "Trash" ? "active" : ""%>'><a href="<%=Url.Action("CompanyMessage","Message",new { companyid = user.UserData.UserID, status =3 }) %>">Trash</a></li>
        </ul>
   </li>
   <li class='<%=ViewBag.Select == "ReceiveEmail" ? "active" : ""%>'><a href="<%=Url.Action("ReceiveEmail","Company",new { companyid = user.UserData.UserID }) %>">Manage Notification</a></li>
   <li class='<%=ViewBag.Select == "InsightList" ? "active" : ""%>'>
        <div class="insight_message"><a href="<%=Url.Action("InsightList","Insight",new { userid = user.UserData.UserID }) %>">Insight Comment</a></div>
         <div class="insightinfo_box">
            <img src="/Styles/images/icon_info.png" alt="" title="" />
                
                <div>
                Share your leadership thoughts on the GLO insights page and edit/delete existing blog comments:<br />
                <ul>
                    <li>Click on ‘new comment’ to start a new blog entry</li>
                    <li>Click on ‘edit’ or ‘delete’ to make changes to existing entries</li>
                </ul>    
                </div>
          </div>
             <div class="glo_fixer"></div>
   </li>
   <li class='<%=ViewBag.Select == "UpgradeMembership" ? "active" : ""%>'><a href="<%=Url.Action("UpgradeMembership","Company",new { companyid = user.UserData.UserID }) %>">Upgrade your membership</a></li>
</ul>

     <script type="text/javascript">
         $(document).ready(function () {
             $(".insightinfo_box img").hover(function () { $(".insightinfo_box div").show(); },
                function () { $(".insightinfo_box div").hide(); }
            );
         });
     </script>