﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_CompanyJob>" %>
<div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_box3">
        
        	<div class="nav_company_edit nav_company_edit_assignment">Our Dashboard</div>
            <div class="box_assignment_edit">
                <div id="jobform" class="box_form samll_form">
                    <table class="tb_form">
                        <tbody><tr>
                          <td valign="top"><label for="UserName">Job Title</label></td>
                            <td>
                            
                                <input type="text" class="txt_form2 font" value="<%=Model.JobTitle %>"  id="JobTitle" onfocus="if (this.value=='Job Title')this.value=''" onblur="if (this.value=='')this.value='Job Title'"><span class="required">*</span>
                            </td>
                          </tr>
                        <tr>
                            <td valign="top"><label for="Password">Location</label></td>
                            <td>
                                    <div class="multiple_box">
                                        <input class="multiple_value font" id="Location" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.Location)?Model.Location:"Select Location" %>" />
                                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                                        <div class="multiple_wrapper multiple_wrapper_location font"></div>                                
                                    </div>
                               <span class="required">*</span>
                            </td>
                        </tr>                      
                         <tr>
                            <td valign="top"><label for="Password">Industry</label></td>
                            <td>
                                    <div class="multiple_box">
                                        <input class="multiple_value font" id="Industry" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.Industry)?Model.Industry:"Select Industry" %>" />
                                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                                        <div class="multiple_wrapper multiple_wrapper_industry font">                                                                                        
                                        </div>                                
                                    </div>
                               <span class="required">*</span>
                            </td>
                        </tr>
                        <tr>
                                <td valign="top">
                                    Job Function
                                </td>
                                <td class="item_option">
                                    <div class="select_box">
                                        <input class="select_value font" id="Function" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.JobFunction)?Model.JobFunction:"Select Job Function" %>" />
                                        <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                                        <div class="select_option font">
                                            
                                        </div>
                                    </div>
                                    <span class="required">*</span>
                                </td>
                            </tr>
                         <tr>
                            <td valign="top"><label for="Password">PostDate</label></td>
                            <td>
                                <input type="text" class="txt_form2 font" value="<%=Model.PostDate %>" readonly="readonly"  id="PostDate"><span class="required">*</span>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Salary Level</label></td>
                            <td>
                            <input type="text" class="txt_form2 font" value="<%=Model.Salary %>" id="Salary">
                           
                              
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><label for="Password">Job Summary </label></td>
                            <td>
                                 <textarea id="JobSummary" class="area_form4 font"><%=Model.JobSummary %></textarea>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Key Responsibilities </label></td>
                            <td>
                                 <textarea id="KeyResponsibility" class="area_form4 font"><%=Model.KeyResponsibility %></textarea><span class="required">*</span>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Key Competencies </label></td>
                            <td>
                                 <textarea id="KeyCompetence" class="area_form4 font"><%=Model.KeyCompetence %></textarea><span class="required">*</span>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Education</label></td>
                            <td>
                            <input type="text" value="<%=Model.Education %>" id="Education" class="txt_form2 font">
                           
                              
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><label for="Password">Contact Person</label></td>
                            <td>
                            <input type="text" value="<%=Model.ContactPerson %>" class="txt_form2 font" id="ContactPerson">
                           
                              
                            </td>
                        </tr>
                        <tr>
                            <td>                         
                                <input type="text" id="DefinedTitle" class="txt_form2 font" style="width:100px" maxlength="25" value='<%=!string.IsNullOrEmpty(Model.DefinedTitle)?Model.DefinedTitle:"User-defined title" %>' onfocus="if (this.value=='User-defined title')this.value=''" onblur="if (this.value=='')this.value='User-defined title'">
                            </td>
                            <td>
                                <textarea id="DefinedContent" class="area_form4 font" onfocus="if (this.value=='User-defined content')this.value=''" onblur="if (this.value=='')this.value='User-defined content'"><%=!string.IsNullOrEmpty(Model.DefinedContent) ? Model.DefinedContent : "User-defined content"%></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="button" id="jobad_publish" value="Publish" class="btn_form">
                                <input type="button" id="jobad_save" value="Save" class="btn_form">
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
            
            <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>   
        </div>
    
    </div>

     <script type="text/javascript">
     $(document).ready(function () {
          $("#PostDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOtherMonths: true
        });
        $.getJSON('/Home/_GetLocationList', null, function (data) {
                    {
                        $('.multiple_wrapper_location').html(data.Html);
                    }
         });
         $.getJSON('/Home/_GetIndustryList', null, function (data) {
                    {
                        $('.multiple_wrapper_industry').html(data.Html);
                    }
         });
         $.getJSON('/Company/_FunctionList', {companyid:"<%=Model.UserID %>"}, function (data) {
                    {
                        $('.select_option').html(data.Html);
                    }
         });
         

          $("#jobad_save").click(function () {           
            EditJob(false);
            
         });
         $("#jobad_publish").click(function(){
           if(ValidateJob() == false)
                return;
            EditJob(true);
         });

            $("#JobTitle").live("blur", function(){
                if(!isEmpty($(this).val())&&$(this).val()!="Job Title") $(this).removeClass("input_error");
            });
            $("#KeyResponsibility").live("blur", function(){ 
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });
            $("#KeyCompetence").live("blur", function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });
            $("#DefinedTitle").live("blur", function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });
            $("#DefinedContent").live("blur", function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });

            $("#PostDate").live("change", function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");
            });

        });
        function EditJob(ispublish) { 
          var jsondata ={
                         'JobID':<%=Model.JobID %>,
                         'UserID':<%=Model.UserID %>,
                         'IsActive':'<%=Model.IsActive %>',
                         'JobTitle':$('#JobTitle').val(),
                         'Location':$("#Location").val()!="Select Location"?$("#Location").val():"",
                         'Industry':$('#Industry').val()!="Select Industry"?$('#Industry').val():"",
                         'PostDate':$('#PostDate').val(),
                         'Salary':$('#Salary').val(),
                         'KeyResponsibility':$('#KeyResponsibility').val(),
                         'KeyCompetence':$('#KeyCompetence').val(),
                         'JobSummary':$('#JobSummary').val(),
                         'Education':$('#Education').val(),
                         'JobFunction':($('#Function').val()!="Select Job Function"&&$('#Function').val()!="All Function")?$('#Function').val():"",
                         'ContactPerson':$('#ContactPerson').val(),
                         'DefinedTitle':$('#DefinedTitle').val()!="User-defined title"?$('#DefinedTitle').val():"",
                         'DefinedContent':$('#DefinedContent').val()!="User-defined content"?$('#DefinedContent').val():""
                   };
           $.postJSONnoQS('<%=Url.Action("JobEdit") %>', { jsondata:$.toJSON(jsondata),IsPublish:ispublish }, function (data) {
                window.location.reload();                       
                return false;
            });
        }

    function ValidateJob()
    { 
        var error = new Array();
        if(isEmpty($("#JobTitle").val())||$("#JobTitle").val()=="Job Title")
        { 
            $("#JobTitle").addClass("input_error");
            error.push("error")
        }
        else
        {
            $("#JobTitle").removeClass("input_error");
        }

        if(isEmpty($("#Location").val())||$("#Location").val()=="Select Location")
        { 
            $("#Location").parent().addClass("select_error");
            error.push("error")
        }
        else
        {
            $("#Location").parent().removeClass("select_error");
        }

        if(isEmpty($("#Industry").val())||$("#Industry").val()=="Select Industry")
        { 
            $("#Industry").parent().addClass("select_error");
            error.push("error")
        }
        else
        {
            $("#Industry").parent().removeClass("select_error");
        }

        if(isEmpty($("#Function").val())||$("#Function").val()=="Select Job Function"||$("#Function").val()=="All Function")
        { 
            $("#Function").parent().addClass("select_error");
            error.push("error")
        }
        else
        {
            $("#Function").parent().removeClass("select_error");
        }

        if(isEmpty($("#PostDate").val()))
        { 
            $("#PostDate").addClass("input_error");
            error.push("error")
        }
        else
        {
            $("#PostDate").removeClass("input_error");
        }

        if(isEmpty($("#KeyResponsibility").val()))
        { 
            $("#KeyResponsibility").addClass("input_error");
            error.push("error")
        }
        else
        {
            $("#KeyResponsibility").removeClass("input_error");
        }

        if(isEmpty($("#KeyCompetence").val()))
        { 
            $("#KeyCompetence").addClass("input_error");
            error.push("error")
        }
        else
        {
            $("#KeyCompetence").removeClass("input_error");
        }

        if($("#DefinedTitle").val()!="User-defined title"&&$("#DefinedContent").val()=="User-defined content")
        {
            $("#DefinedContent").addClass("input_error");
            error.push("error")
                      
        }
        else
        {
            $("#DefinedContent").removeClass("input_error");
        }

        if($("#DefinedTitle").val()=="User-defined title"&&$("#DefinedContent").val()!="User-defined content")
        {
            $("#DefinedTitle").addClass("input_error");
            error.push("error")
                      
        }
        else
        {
            $("#DefinedTitle").removeClass("input_error");
        }
                                      
        if(error.length>0)
        return false;
        else 
        return true;
    }

 </script>
   