﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Company>" %>
<%int count = 0;
  if (Model.GLO_CompanyJob != null)
      count = Model.GLO_CompanyJob.Where(x => x.IsActive && x.IsPublish && x.IsDelete == false && x.ExpireDate > DateTime.Now).Count();
  string keywork = Request.QueryString["keyword"];
%>
<div class="searchbar">
            <div>
                Job At <%=Model.CompanyName %>: There are currently <%=count %> open positions</div>
            <div>
                <input value='<%=!string.IsNullOrEmpty(keywork)?keywork:"senior software engineer shanghai"%>'  type="text" id="ckeyword" onfocus="if (this.value=='senior software engineer shanghai')this.value=''" onblur="if (this.value=='')this.value='senior software engineer shanghai'" />
                <a href="javascript:void(0)" id="jobsearch">
                    <img src="/Styles/images/icon_search.gif" alt="" title="" /></a>
            </div>
 </div>
  <script type="text/javascript">
      $(document).ready(function () {
          $("#jobsearch").click(function () {
              var value = $("#ckeyword").val();
              window.location.href = '<%=Url.Action("CompanyJob","Company") %>' + "?keyword=" + value+"&companyid=<%=Model.UserID %>"; 
          });

      });

  </script>