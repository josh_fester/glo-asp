﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Leader>" %>
 <script src="<%=Url.Content("~/Scripts/jquery-ui-1.8.11.min.js") %>" type="text/javascript"></script>
<div class="lightbox_mask">
</div>

<input type="hidden" id="RecommendCount" value="<%=Model.GLO_Users.GLO_Recommendation.Count%>" />
<div class="lightbox_manage">
        <div class="lightbox_note3">
            <div>
                GLO appreciates its users’ choice of confidentiality.<br />
Using the bar on the left, select a private (grey) or 
public (blue) profile. <br />
When public, your profile will 
randomly show in a list of good leaders accessed by 
visitors to the site. <br />
If private, only member companies 
and experts can see your profile.
<br /><br />
If you would like certain companies to be excluded 
from viewing your profile, please identify them here:<br />
                <div style="min-height:60px;">
                    <div id="keyWordList" class="r_info_content">
                        <% foreach (var item in Model.GLO_LeaderKeyWord.OrderBy(t=>t.KeyWordContent))
                           {   
                        %>
                        <div class="value_item_active">
                            <div class="user_value_delete">
                                <a href="javascript:void(0);" title="delete" id="<%=item.KeyWordID %>" onclick="DeleteKeyWord(this);"><img src="/Styles/images/icon_cancel.gif" /></a></div>
                            <%=item.KeyWordContent %>
                        </div>
                        <%}%>
                    </div>
                    <div id="addKeyWordDiv" class="user_value_add" style="padding-top:5px;">
                        Keyword:<input type="text" value="" class="txt_itemadd" id="txtKeyWord" maxlength="30" /><input type="button" id="btnAddKeyWord" value="Add" class="bluebtn" />
                    </div>
                </div>

                <div>
                    We also offer the possibility to keep your profile 
fully confidential and only visible to the GLO team.
If you prefer to be an anonymous member of the 

GLO community, please check the box here <input type="checkbox"  <%=Model.GLO_Users.HideProfile?"checked=\"true\"":""%>  id="chkConfidential" />
                </div>
            </div>
            <br />
            <div class="resume_container">
                <div class="resume_headline">Resume lists</div>
                <ul id="resumeName">
                <%
                    var resumesList = Model.GLO_Users.GLO_Resumes;
                    if (resumesList.Count > 0)
                    {
                        foreach (var resume in resumesList)
                        {%>            
                        <li x:name="<%=resume.ResumeName%>" x:id="<%=resume.ResumeID%>"><%=resume.ResumeName%><a href="javascript:void(0);" title="delete" onclick="DeleteResume(this)"><img src="/Styles/images/icon_delete1.gif" /></a></li>
                    <%}
                    } %>
                </ul>
            </div>

            <div class="message_arrow5"></div>
            <div class="message_arrow8"></div>
            <div class="me_apply">
                <a id="btnSubmit2" href="javascript:void(0);">
                    <img src="/Styles/images/icon_apply.gif"></a></div>
        </div>
    <div class="lightbox_box1">
        <% var classname = "box_user_private";
           var ispublish = "0";
           if (Model.GLO_Users.IsPublish)
           {
               classname = "box_user_private_on";
               ispublish = "1";

           } %>
        <div class="<%=classname%>" x:ispublish="<%=ispublish %>" id="publish_or_private"
            onclick="ResetPrivate(this);" title="Use this button to make your profile visible or private">
        </div>
        <div class="box_user_wrap">
        <div class="box_user_photo2">
            <img id="imageID" src="/Images/<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"" %>"/>
            
            <div class="photo_item_edit">
                <input type="button" id="file_upload" class="btngray" value="Select Photo"/>
                <input type="button" id="file_delete" class="btngray" value="Delete Photo"/>
            </div>
        </div>        
        <div class="box_user_button" style="bottom:10px;left:140px">  
            <input type="button" id="UploadResume" class="" value="Upload Resumes">            
        </div>
        </div>
        <div class="box_user_detail">
            <input type="text" class="txtbox_user1" id="NickName" value="<%=!String.IsNullOrEmpty(Model.NickName)?Model.NickName:"Your name"%>"
                onfocus="if (this.value=='Your name')this.value=''" onblur="if (this.value=='')this.value='Your name'" />
            <input type="text" class="txtbox_user2" id="Title" maxlength="50" value="<%=!String.IsNullOrEmpty(Model.Title)?Model.Title:"Job title" %>"
                onfocus="if (this.value=='Job title')this.value=''" onblur="if (this.value=='')this.value='Job title'"/>
                        
            <div class="multiple_user">
                <div class="multiple_box">
                <%: Html.Hidden("LocationID", Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().LID.ToString() : "0", new { @class = "multiple_locationid" })%>
                    <input class="multiple_value" id="Location" type="text" readonly="readonly" value="<%=!String.IsNullOrEmpty(Model.Location)?Model.Location:"Location" %>" />
                    <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                        <div class="multiple_wrapper multiple_wrapper_location"></div> 
                </div>                
            </div>
            <div class="multiple_user">
                <div class="multiple_box">
                <%: Html.Hidden("IndustryID", Model.GLO_Users.GLO_UserIndustry.Count > 0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().IndustryID.ToString() : "0", new { @class = "multiple_id" })%>
                    <input class="multiple_value" id="Industry" type="text" readonly="readonly" value="<%=!String.IsNullOrEmpty(Model.Industry)?Model.Industry:"Industry" %>" />
                    <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                        <div class="multiple_wrapper multiple_wrapper_industry"></div> 
                </div>                
            </div>
            

            <textarea class="txtbox_user3" id="MyVision" onfocus="if (this.value=='describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal')this.value=''"
                onblur="if (this.value=='')this.value='describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal'"><%=!String.IsNullOrEmpty(Model.Version) ? Model.Version : "describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal"%></textarea>
        </div>
        <div id="selectvalue" class="box_user_value">
            <div class="box_user_value_title">What values in life and career are important to you:</div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="LeaderValue1" type="text" readonly="readonly" x:value="<%=Model.LeaderValue1%>" value="<%= string.IsNullOrEmpty(Model.LeaderValue1) ? "Select" : Model.LeaderValue1%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x => x.ExpertValueID))
                               {
                            %>
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="LeaderValue2" type="text" readonly="readonly" x:value="<%=Model.LeaderValue2%>" value="<%= string.IsNullOrEmpty(Model.LeaderValue2) ? "Select" : Model.LeaderValue2%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x => x.ExpertValueID))
                               {
                            %>
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="LeaderValue3" type="text" x:value="<%=Model.LeaderValue3%>" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.LeaderValue3) ? "Select" : Model.LeaderValue3%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x=>x.ExpertValueID))
                               {
                            %>                            
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
        </div>
        <div class="user_value_add">
            Custom:<input type="text" value="" class="txt_itemadd font" id="txtExpertValue" maxlength="30" /><input type="hidden" id="hidExpertValueID" /><input type="button" id="btnAddExpertValue" value="Add" class="bluebtn" />
        </div>
        <div class="me_apply">
            <a id="btnSubmit" href="javascript:void(0);">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
        var nameArry = [];
        var companynameArry = [];
        $(document).ready(function () {
            $("#NickName").blur(function(){
                if (!isEmpty($(this).val())|| $(this).val()!="Your name") $(this).removeClass("input_error");
            });
            $("#Title").blur(function(){
                if (!isEmpty($(this).val())|| $(this).val()!="Job title") $(this).removeClass("input_error");
            });


            $.getJSON('/Home/_GetLocationList', null, function (data) {
                        {
                            $('.multiple_wrapper_location').html(data.Html);
                        }
             });
            $.getJSON('/Home/_GetIndustryList', null, function (data) {
                        {
                            $('.multiple_wrapper_industry').html(data.Html);
                        }
             });

            if($('#keyWordList').find(".value_item_active").length>4)
            {
                $('#addKeyWordDiv').css('display', 'none');
            }
            SetDropdown();
            GetData();       
            GetCompanyList();     
            $('#UploadResume').uploadify({
                    'height': 25,
                    'width': 110,
                    'auto': true,
                    'multi': true,
                    'buttonText': 'Upload Resumes',
                    'fileTypeExts': '*.doc;*.docx;*.ppt;*.pptx;*.pdf;*.rtf',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/Home/UploadResume?id='+<%=Model.UserID %>,
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);
                        if (data.Success) {
                            var newHtml = "<li x:name=\""+data.SaveName+"\" x:id=\""+data.SaveID+"\">"+data.SaveName+"<a href=\"javascript:void(0);\" title=\"delete\" onclick=\"DeleteResume(this)\"><img src=\"/Styles/images/icon_delete1.gif\" /></a></li>";
                            $('#resumeName').append(newHtml);
                            $('.upload_boxer').find('a').css("display","");
                        }
                        else {
                            alertbox(data.Message);
                        }
                    }
            });
            $("#chkConfidential").click(function(){       
                 if($("#chkConfidential").attr("checked"))
                  {
                     if ($("#publish_or_private").attr("x:ispublish") ==1) {             
                       $("#publish_or_private").attr("class","box_user_private");
                       $("#publish_or_private").attr("x:ispublish","0");
                     }
                  }
            });

            $("#txtExpertValue").autocomplete({
                minLength: 1,
                autoFocuus: true,
                source: function (req, res) {
                    var name = req.term,
                                result = [];
                    if ($.trim(req.term) != "") {
                        var searchResult = $.map(
                                    (name ? $.grep(nameArry, function (value) {
                                        return value.name.toString().toLocaleLowerCase().indexOf(name) >= 0;
                                    }) : nameArry),
                                    function (value) {
                                        return { label: value.name, tc: value.id };
                                    });
                        result = result.concat($.makeArray(searchResult));
                    }
                    res(result);
                },
                select: function (event, ui) {
                    $('#hidExpertValueID').val(ui.item.tc);
                }
            });
            $("#btnAddExpertValue").click(function () {
                var value=$('#txtExpertValue').val().trim();
                if(value=="")
                {
                    return false;
                }
                $.postJSON('<%=Url.Action("AddExpertValue","Expert") %>', { value:value}, function (data) {
                    var html= "<li x:relationalID=\""+ data +"\">" + value + "<img title=\"delete\" id=\"" + data + "\" x:value=\"" + value + "\" class=\"value_delete\" src=\"/Styles/images/icon_cancel.gif\" /></li>";
                    if(data!=""){
                    var isExist=false;
                        $(".select_option li").each(function(){
                            if($(this).attr("x:relationalID") == data)
                            {
                                isExist= true;
                            }
                        });
                        if(!isExist)
                        {
                            $('.select_option').children("ul").append(html);
                            SetDropdown();
                        }
                        $('#txtExpertValue').val("");
                        alertbox("Your customized value has been successfully added to the list.");
                    }
                    else{
                        alertbox("Add failed.");
                    }
                });
            });
            $("#txtKeyWord").autocomplete({
                minLength: 1,
                autoFocuus: true,
                source: function (req, res) {
                    var name = req.term,
                                result = [];
                    if ($.trim(req.term) != "") {
                        var searchResult = $.map(
                                    (name ? $.grep(companynameArry, function (value) {
                                        return value.name.toString().toLocaleLowerCase().indexOf(name) >= 0;
                                    }) : companynameArry),
                                    function (value) {
                                        return { label: value.name, tc: value.id };
                                    });
                        result = result.concat($.makeArray(searchResult));
                    }
                    res(result);
                },
                select: function (event, ui) {

                }
            });
            $("#btnAddKeyWord").click(function () {
                var value=$('#txtKeyWord').val().trim();
                if(value=="")
                {
                    return false;
                }
                $.postJSON('<%=Url.Action("AddKeyWord","Leader") %>', { value:value}, function (data) {
                    var html = "<div class='value_item_active'>" + value + "<div class=\"user_value_delete\"><a href=\"javascript:void(0);\" title=\"delete\" id=\"" + data + "\"  onclick=\"DeleteKeyWord(this);\"><img src=\"/Styles/images/icon_cancel.gif\" /></a></div></div>";
                    if (data != "") {
                        if ($('#keyWordList').find("#" + data).length == 0) {
                            $('#keyWordList').append(html);
                        }
                        if($('#keyWordList').find(".value_item_active").length>4)
                        {
                            $('#addKeyWordDiv').css('display', 'none');
                        }
                        $('#txtKeyWord').val("");
                    }
                    else {
                        alertbox("Add failed.");
                    }
                });
            });
            
              $("#btnSubmit2").click(function () {  
                   var hidProfile = false;
                   if($("#chkConfidential").attr("checked"))
                   {
                      hidProfile = true;
                   }
                   var status =$("#publish_or_private").attr("x:ispublish")==1?true:false;
                    $.postJSON('<%=Url.Action("LeaderProfileHide") %>', {UserID:<%=Model.UserID %>,HideProfile:hidProfile,ProfileSatus:status}, function (data) {
                         return false;
                     });
                              
                $('.lightbox_note3').hide();
              });

              $("#btnSubmit").click(function () {
                   if (ValidateInfo() == false)
                    return;
                   
                   var version = $('#MyVision').val()=="describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal"?"":$('#MyVision').val();

                   var expertId='<%=Model.UserID %>';
                   var expertdata ={
                         'UserID':<%=Model.UserID %>,
                         'NickName':$('#NickName').val(),
                         'LogoUrl':$("#imageID").attr("x:name"),
                         'Industry':$('#Industry').val(),
                         'Location':$('#Location').val(),
                         'Title':$('#Title').val(),
                         'Version':version,
                         'LeaderValue1':$("#LeaderValue1").val() == "Select" ? "" : $("#LeaderValue1").val(),
                         'LeaderValue2':$('#LeaderValue2').val() == "Select" ? "" : $("#LeaderValue2").val(),
                         'LeaderValue3':$('#LeaderValue3').val() == "Select" ? "" : $("#LeaderValue3").val()                      
                   };
                   var isFirstSave = true;
                   if($("#LeaderValue1").val()!=$("#LeaderValue1").attr("x:value")||$("#LeaderValue2").val()!=$("#LeaderValue2").attr("x:value")||$("#LeaderValue3").val()!=$("#LeaderValue3").attr("x:value"))
                       isFirstSave = false;
                   if($("#LeaderValue1").attr("x:value")==""&&$("#LeaderValue2").attr("x:value")==""&&$("#LeaderValue3").attr("x:value")=="")
                       isFirstSave = true;
                   if($("#RecommendCount").val()<=0)
                       isFirstSave = true;

                   var hidProfile = false;
                   if($("#chkConfidential").attr("checked"))
                   {
                      hidProfile = true;
                   }
                   if(isFirstSave==true)
                   {

                     $.postJSON('<%=Url.Action("LeaderProfileUpdate") %>', { leaderdata:$.toJSON(expertdata),ispublish:$("#publish_or_private").attr("x:ispublish"),LocationID:$("#LocationID").val(),industryID:$('#IndustryID').val(),HideProfile:hidProfile}, function (data) {
                         if(data=="2")
                         {
                             window.location.href="/Leader/Index?leaderid="+expertId;
                         }
                         else if(data=="4")
                         {
                             window.location.href="/Leader/MyCompetence?leaderid="+expertId;
                         }
                         else if(data=="5")
                         {
                             window.location.href="/Leader/RegisterSuccess?leaderid="+expertId;
                         }
                         else  if(data=="1")
                         {                            
                            window.location.href="/Leader/Index?leaderid="+expertId;
                         }
                         else
                           window.location.reload();
                     });
                  }
                  else
                  { 
                      confirmbox("Do you want to save changes? If yes, new recommendation message will send to current recommender and ask them to reconfirm your new competencies.", function () {
                         
                         $.postJSON('<%=Url.Action("LeaderProfileUpdate") %>', { leaderdata:$.toJSON(expertdata),ispublish:$("#publish_or_private").attr("x:ispublish"),LocationID:$("#LocationID").val(),industryID:$('#IndustryID').val(),HideProfile:hidProfile}, function (data) {
                         if(data=="2")
                         {
                             window.location.href="/Leader/Index?leaderid="+expertId;
                         }
                         else if(data=="4")
                         {
                             window.location.href="/Leader/MyCompetence?leaderid="+expertId;
                         }
                         else if(data=="5")
                         {
                             window.location.href="/Leader/RegisterSuccess?leaderid="+expertId;
                         }
                         else  if(data=="1")
                         {                            
                            window.location.href="/Leader/Index?leaderid="+expertId;
                         }
                         else
                           window.location.reload();
                     });
                           
                        });
                  }
                 });
                 
                $('#file_upload').uploadify({
                    'height': 25,
                    'width': 90,
                    'auto': true,
                    'multi': false,
                    'buttonText': 'Select Photo',
                    'fileTypeExts': '*.gif; *.jpg; *.png',
                    'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                    'uploader': '/FileUpload/Upload?width=186&height=186',
                    'onUploadSuccess': function (file, data, response) {
                        eval("data=" + data);

                        $("#imageID").removeAttr("src");
                        $("#imageID").removeAttr("x:name");
                        $("#imageID").attr("src",data.Urlpath);
                        $("#imageID").attr("x:name", data.SaveName); 

                        $(".uploadify-queue").css('display','none');
                        $('#file_upload').css('display','none');
                        $('#file_delete').css('display','');
                    }
                });

                 $('#file_delete').click(function(){
                    $("#imageID").removeAttr("src");
                    $("#imageID").removeAttr("x:name");
                    $("#imageID").attr("src","/Images/user_none.gif");
                    $("#imageID").attr("x:name", ""); 

                    $('#file_upload').css('display','');
                    $('#file_delete').css('display','none');
                 });
                 
                var logoUrl='<%=Model.LogoUrl %>';
                if (logoUrl != "" && logoUrl.indexOf("user_none.gif") == -1)
                {
                    $('#file_upload').css('display','none');
                    $('#file_delete').css('display','');
                }
                else{
                    $('#file_upload').css('display','');
                    $('#file_delete').css('display','none');
                }
        });
        function GetData() {
            $.post("/Expert/GetExpertValueList", null, function (data) {
                $.each(data, function () {
                    var nameitem = { name: this.Text, id: this.Value };
                    nameArry.push(nameitem);            
                });
            });
        }
        function ResetPrivate(obj) {
            if (obj.className == 'box_user_private_on') {             
              obj.className = 'box_user_private';
              $(obj).attr("x:ispublish","0");
            }
            else {
              if($("#chkConfidential").attr("checked"))
              {
                 alertbox("Please cancel visible to the GLO team first.");
                 return;
              }
                obj.className = 'box_user_private_on';
                 $(obj).attr("x:ispublish","1");
            }
        }
        function ValidateInfo() {
        var error = new Array();
        if (isEmpty($("#NickName").val())|| $("#NickName").val()=="Your name") {
            $("#NickName").addClass("input_error");
            error.push("error")
        }
        else {
            $("#NickName").removeClass("input_error");
        }

        if (isEmpty($("#Title").val()) || $("#Title").val()=="Job title") {
            $("#Title").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Title").removeClass("input_error");
        }

        if (isEmpty($("#Location").val()) || $("#Location").val()=="Location") {
            $("#Location").parent().addClass("select_error");
            error.push("error")
        }
        else {
            $("#Location").parent().removeClass("select_error");
        }

        if (error.length > 0)
            return false;
        else
            return true;
    }
       
    function SetDropdown()
    {    
       /*open dropdown*/
            $(".select_box").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                $(this).find(".select_option").toggle();
            });
            /*close dropdown*/
            $(document).click(function () {
                $('.select_option').hide();
            });
            /*set value*/
            $(".select_option li").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                
                var oldText=$(this).parent().parent().parent().find(".select_value").val();
                var text = $(this).text().replace(/<img([^>]*)>/gi, "");
                $(this).parent().parent().parent().find(".select_value").val(text);
                $(this).parent().parent().parent().removeClass('select_error');

                $(".select_option li").each(function(){
                    if($(this).text()==oldText)
                    {
                        $(this).css("display","")
                    }
                    if($(this).text()==text)
                    {
                        $(this).css("display","none")
                    }
                });   
            });                    
            /*delete value*/
            $(".value_delete").click(function(event) {
                event.stopPropagation();
                $(".select_option").hide();
                
                var text = "Value";
                var delValue = $(this).attr("x:value");
                var selValue = $(this).parent().parent().parent().parent().find(".select_value").val();
                if(delValue == selValue)
                $(this).parent().parent().parent().parent().find(".select_value").val(text);

                var id = $(this).attr("id");
                $.getJSON('<%=Url.Action("DeleteValue","Expert") %>', { "id": id, random: Math.random() }, function (data) {
                    if (data) { 
                        $('.select_option li').each(function(){
                            if($(this).attr("x:relationalID") == id)
                            {
                                $(this).remove();
                            }
                        });
                    }
                });
            });
    }
function DeleteResume(obj) {
    var deleteResumeName = $(obj).parent().attr('x:name');
    var deleteResumeId= $(obj).parent().attr('x:id');
    confirmbox("Do you want to delete the file？", function(){
        $.post("/Leader/DeleteResume", {fileName:deleteResumeName,resumeID:deleteResumeId}, function (data) {
            if(data)
            {
                $(obj).parent().remove();                
            }        
        });
    });
}
function DeleteKeyWord(obj) {
    $.post('<%=Url.Action("DeleteKeyWord","Leader") %>', { "id": obj.id, random: Math.random() }, function (data) {
        if (data) {
            $('#keyWordList').find("#" + obj.id).parent().parent().remove();
            if ($('#keyWordList').find(".value_item_active").length < 5) {
                $('#addKeyWordDiv').css('display', '');
            }
            else {
                $('#addKeyWordDiv').css('display', 'none');
            }
        }
    });
}
function GetCompanyList() {
    $.post("/Leader/GetCompanyList", null, function (data) {
        $.each(data, function () {
            var nameitem = { name: this.Text, id: this.Value };
            companynameArry.push(nameitem);            
        });
    });
}
</script>