﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var contribution = ViewData["Contribution"]!=null? (GLO.Data.Models.GLO_Contribution)ViewData["Contribution"]:new GLO.Data.Models.GLO_Contribution() ;
   var type = ViewData["Type"].ToString();
   var education = ViewData["Education"]!=null? (GLO.Data.Models.GLO_Education)ViewData["Education"]: new GLO.Data.Models.GLO_Education();
   var employment = ViewData["Employment"]!=null? (GLO.Data.Models.GLO_CareerSummary)ViewData["Employment"]: new GLO.Data.Models.GLO_CareerSummary();
   %>
<div class="lightbox_mask"></div>
<div class="lightbox_manage"> 
 <div class="timeline_edit">
    <div class="timeline_arrow"></div>
    <div id="TimeLineAdd" >
        <div class="timeline_summary"></div>
        <div class="timeline_education"></div>
        <div class="timeline_contribution"></div>
        <div class="select_box select_box_type" style="display:none">
            <input class="select_value" id="SelectType" type="text" readonly="readonly" value="<%=type %>" />
            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
            <div class="select_option">
                <ul>
                    <%if(type=="Employment"){ %>
                    <li>Employment</li>
                    <%}
                   else if (type == "Contribution")
                   { %>
                    <li>Non-profit</li>
                    <% }
                   else if (type == "Education")
                   {  %> 
                    <li>Education</li>
                   <%} %>
                </ul>
            </div>
        </div>
        <%if(type=="Employment"){                
               %>
        <div id="Employment">
            <input type="text" id="Company" value="<%=employment.Company %>" onfocus="if (this.value=='Company')this.value=''" onblur="if (this.value=='')this.value='Company'">
            <input type="text" id="JobTitle" value="<%=employment.JobTitle %>" onfocus="if (this.value=='Job Title')this.value=''" onblur="if (this.value=='')this.value='Job Title'">
            <input type="text" id="JobIndustry" value="Job Industry" onfocus="if (this.value=='Job Industry')this.value=''" onblur="if (this.value=='')this.value='Job Industry'">
            <input type="text" id="JobStartDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", employment.StartDate) %>" class="txtcalendar" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <input type="text" id="JobEndDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", employment.EndDate) %>" class="txtcalendar" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
            <textarea id="LessonLearnd" onfocus="if (this.value=='Position description')this.value=''" onblur="if (this.value=='')this.value='Position description'"><%=employment.LessonLearnd%></textarea>
            <textarea id="Competencies" onfocus="if (this.value=='Competence applied')this.value=''" onblur="if (this.value=='')this.value='Competence applied'"><%=employment.Competencies%></textarea>
            <textarea id="SuccessAchievement" onfocus="if (this.value=='Success achieved')this.value=''" onblur="if (this.value=='')this.value='Success achieved'"><%=employment.SuccessAchievement%></textarea>
        </div>
        <%}
          else if (type == "Education")
          {
                          
               %>
        <div id="Education">
            <input type="text" id="EduSchool" value="<%=education.School %>" onfocus="if (this.value=='School')this.value=''" onblur="if (this.value=='')this.value='School'">
            <input type="text" id="EduDegree" value="<%=education.Degree %>" onfocus="if (this.value=='Major')this.value=''" onblur="if (this.value=='')this.value='Major'">
            <input type="text" id="EduStartDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", education.StartDate) %>" class="txtcalendar" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <input type="text" id="EduEndDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", education.EndDate) %>" class="txtcalendar" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
            
            <div class="select_box select_box_degree">
                <input class="select_value" id="EduDegree" type="text" readonly="readonly" value="Degree" />
                <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                <div class="select_option">
                    <ul>
                        <li>Bachelor</li>
                        <li>Master</li>
                        <li>PHD</li>
                        <li>Postdoctor</li>
                        <li>Other</li>
                    </ul>
                </div>
                
            </div>
            <textarea id="EduLanguages" onfocus="if (this.value=='Lessons learned')this.value=''" onblur="if (this.value=='')this.value='Lessons learned'">Lessons learned</textarea>
            <textarea id="EduCertification" onfocus="if (this.value=='Success achieved')this.value=''" onblur="if (this.value=='')this.value='Success achieved'">Success achieved</textarea>
        </div>
        <% }
          else if (type == "Contribution")
          {             
              %>  
        <div id="NonProfit">
            <input type="text" id="ConOrgnization" value="<%=contribution.Orgnization %>" onfocus="if (this.value=='Organization')this.value=''" onblur="if (this.value=='')this.value='Organization'">
            <input type="text" id="ConJobTitle" value="<%=contribution.JobTitle %>" onfocus="if (this.value=='Job Title')this.value=''" onblur="if (this.value=='')this.value='Job Title'">
            <input type="text" id="ConStartDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", contribution.StartDate) %>" class="txtcalendar" onfocus="if (this.value=='From')this.value=''" onblur="if (this.value=='')this.value='From'">
            <input type="text" id="ConEndDate" readonly="true" value="<%=string.Format("{0:yyyy-MM-dd}", contribution.EndDate) %>" class="txtcalendar" onfocus="if (this.value=='To')this.value=''" onblur="if (this.value=='')this.value='To'">
            <textarea id="ConLessonLearnd" onfocus="if (this.value=='Jobs duties/ Lessons learned')this.value=''" onblur="if (this.value=='')this.value='Jobs duties/ Lessons learned'"><%=contribution.LessonLearnd%></textarea>
            <textarea id="ConCompetencies" onfocus="if (this.value=='Competence applied')this.value=''" onblur="if (this.value=='')this.value='Competence applied'"><%=contribution.Competencies%></textarea>
            <textarea id="ConSuccessAchievement" onfocus="if (this.value=='Success achieved')this.value=''" onblur="if (this.value=='')this.value='Success achieved'"><%=contribution.SuccessAchievement%></textarea>
        </div>
        <%} %>
    </div>
    <div class="me_apply"><a href="javascript:void(0)" id ="educationEdit"><img src="/Styles/images/icon_apply.gif" /></a></div>  
    <div class="me_close"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div> 
                       
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();

            var value = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(value);

            if (value == "Education") {
                $("#Education").show();
                $("#Employment").hide();
                $("#NonProfit").hide();
            }
            else if (value == "Employment") {
                $("#Education").hide();
                $("#Employment").show();
                $("#NonProfit").hide();
            }
            else if (value == "Non-profit") {
                $("#Education").hide();
                $("#Employment").hide();
                $("#NonProfit").show();
            }
        });


        $("#JobStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#JobEndDate").val();
                $("#JobEndDate").datepicker("option", "minDate", selectedDate);
                $("#JobEndDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#JobEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#JobStartDate").val();
                $("#JobStartDate").datepicker("option", "maxDate", selectedDate);
                $("#JobStartDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#EduStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#EduEndDate").val();
                $("#EduEndDate").datepicker("option", "minDate", selectedDate);
                $("#EduEndDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#EduEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#EduStartDate").val();
                $("#EduStartDate").datepicker("option", "maxDate", selectedDate);
                $("#EduStartDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#ConStartDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#ConEndDate").val();
                $("#ConEndDate").datepicker("option", "minDate", selectedDate);
                $("#ConEndDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });

        $("#ConEndDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                var oldVal = $("#ConStartDate").val();
                $("#ConStartDate").datepicker("option", "maxDate", selectedDate);
                $("#ConStartDate").val(oldVal);
            },
            beforeShow: function () {
                setTimeout(function () {
                    $('#ui-datepicker-div').css("z-index", 15234234);
                }, 100);
            }
        });




        $("#educationEdit").click(function () {
    
            var userid = $("#hidexpertid").val();
            var empdata = { 'CareerID':<%=employment.CareerID %>
                            ,'UserID': ''
                            , 'Company': ''
                            , 'JobTitle': ''
                            , 'StartDate': ''
                            , 'EndDate': ''
                            , 'SuccessAchievement': ''
                            , 'Competencies': ''
                            , 'LessonLearnd': ''
            };
            empdata.UserID = <%=employment.UserID %>;
            empdata.Company = $("#Company").val();
            empdata.JobTitle = $("#JobTitle").val();
            empdata.StartDate = $("#JobStartDate").val();
            empdata.EndDate = $("#JobEndDate").val();
            empdata.SuccessAchievement = $("#SuccessAchievement").val() == "Success achieved" ? "" : $("#SuccessAchievement").val();
            empdata.Competencies = $("#Competencies").val() == "Competence applied" ? "" : $("#Competencies").val();
            empdata.LessonLearnd = $("#LessonLearnd").val() == "Position description" ? "" : $("#LessonLearnd").val();

            var edudata = {
                'EducationID':<%=education.EducationID%>,
                'UserID': <%=education.UserID%>,
                'StartDate': $("#EduStartDate").val(),
                'EndDate': $("#EduEndDate").val(),
                'School': $("#EduSchool").val(),
                'Degree': $("#EduDegree").val(),
                'Languages': $("#EduLanguages").val() == "Lessons learned" ? "" : $("#EduLanguages").val(),
                'Certification': $("#EduCertification").val() == "Success achieved" ? "" : $("#EduCertification").val()
            };
            var condata = { 
                'ContributionID':<%=contribution.ContributionID %>,
                'UserID': <%=contribution.UserID %>,
                'Orgnization': $("#ConOrgnization").val(),
                'JobTitle': $("#ConJobTitle").val(),
                'StartDate': $("#ConStartDate").val(),
                'EndDate': $("#ConEndDate").val(),
                'LessonLearnd': $("#ConLessonLearnd").val() == "Jobs duties/ Lessons learned" ? "" : $("#ConLessonLearnd").val(),
                'Competencies': $("#ConCompetencies").val() == "Competence applied" ? "" : $("#ConCompetencies").val(),
                'SuccessAchievement': $("#ConSuccessAchievement").val() == "Success achieved" ? "" : $("#ConSuccessAchievement").val()
            };
            var selecttype = $("#SelectType").val();
            var jsondata = "";
            if (selecttype == "Education") {
                if (ValidateEducation() == false)
                    return;
                jsondata = $.toJSON(edudata);
            }
            else if (selecttype == "Employment") {
                if (ValidateEmployment() == false)
                    return;
                jsondata = $.toJSON(empdata);
            }
            else {
               // if (ValidateOrg() == false)
                //    return;
                jsondata = $.toJSON(condata);
            }
            $.postJSONnoQS('<%=Url.Action("CareerSummaryEdit") %>', { jsondata: jsondata, type: selecttype }, function (data) {
                window.location.reload();
            });
        });
    });

    function ValidateEmployment() {
        var error = new Array();
        if (isEmpty($("#Company").val()) || $("#Company").val() == "Company") {
            $("#Company").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Company").removeClass("input_error");
        }

        if (isEmpty($("#JobTitle").val()) || $("#JobTitle").val() == "Job Title") {
            $("#JobTitle").addClass("input_error");
            error.push("error")
        }
        else {
            $("#JobTitle").removeClass("input_error");
        }

        if (isEmpty($("#JobStartDate").val()) || $("#JobStartDate").val() == "From") {
            $("#JobStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#JobStartDate").removeClass("input_error");
        }

        if (isEmpty($("#JobEndDate").val()) || $("#JobEndDate").val()=="To") {
            $("#JobEndDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#JobEndDate").removeClass("input_error");
        }

        if (error.length > 0)
            return false;
        else
            return true;
    }

    function ValidateEducation() {
        var error = new Array();
        if (isEmpty($("#EduSchool").val()) || $("#EduSchool").val() == "School") {
            $("#EduSchool").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduSchool").removeClass("input_error");
        }

        if (isEmpty($("#EduDegree").val()) || $("#EduDegree").val() == "Major") {
            $("#EduDegree").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduDegree").removeClass("input_error");
        }

        if (isEmpty($("#EduStartDate").val()) || $("#EduStartDate").val()=="From") {
            $("#EduStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduStartDate").removeClass("input_error");
        }

        if (isEmpty($("#EduEndDate").val()) || $("#EduEndDate").val()=="To") {
            $("#EduEndDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#EduEndDate").removeClass("input_error");
        }
        if (error.length > 0)
            return false;
        else
            return true;
    }

    function ValidateOrg() {
        var error = new Array();
        if (isEmpty($("#ConOrgnization").val()) || $("#ConOrgnization").val() == "Organization") {
            $("#ConOrgnization").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConOrgnization").removeClass("input_error");
        }

        if (isEmpty($("#ConJobTitle").val()) || $("#ConJobTitle").val() == "Job Title") {
            $("#ConJobTitle").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConJobTitle").removeClass("input_error");
        }

        if (isEmpty($("#ConStartDate").val()) || $("#ConStartDate").val()=="From") {
            $("#ConStartDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConStartDate").removeClass("input_error");
        }

        if (isEmpty($("#ConEndDate").val()) || $("#ConEndDate").val() == "To") {
            $("#ConEndDate").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConEndDate").removeClass("input_error");
        }
        if (error.length > 0)
            return false;
        else
            return true;
    }
</script>