﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.ChangePasswordModel>" %>
<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
<style>
.input-validation-error
{
    border-color: Red;
}
</style>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(m=>m.UserID) %>
 <div class="box_form">
    <table>
            <tr>
                <td>Old Password</td>
                            
                <td><%: Html.Password("OldPassword", "", new { @class = "txt_form", maxlength = "12" })%><br /><%: Html.ValidationMessageFor(model => model.OldPassword)%></td>
            </tr>
            <tr>
                <td>New Password</td>
                <td><%: Html.Password("NewPassword", "", new { @class = "txt_form", maxlength = "12" })%><br /><%: Html.ValidationMessageFor(model => model.NewPassword)%></td>
            </tr>
            <tr>
                <td>Confirm New Password</td>
                <td><%: Html.Password("ConfirmPassword", "", new { @class = "txt_form", maxlength = "12" })%><br /><%: Html.ValidationMessageFor(model => model.ConfirmPassword)%></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="Submit" class="btn_form" id="btnChangePasswod" value="Submit" />&nbsp;&nbsp;&nbsp;<a href="/account/forgotpassword">Forgot Password?</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" id="SetQuestion">Set security question</a></td>
            </tr>
    </table>
</div>       
<%} %>

<script type="text/javascript">
    $(document).ready(function () {
        var message = '<%=ViewBag.Message %>';
        if (message != "") {
            alertbox(message, function () {
                window.location.href = "/Home/index";
            });
        }
        $("#SetQuestion").click(function () {
            $.getJSON('<%=Url.Action("_SetSecurityQuestion","Account") %>', { "userID": $('#UserID').val() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
    function ValidatePassword() {
        var error = new Array();
        if (isEmpty($("#oldpassword").val())) {
            $("#oldpassword").addClass("input_error");
            error.push("error")
        }
        else {
            $("#oldpassword").removeClass("input_error");
        }

        if (isEmpty($("#newpassword").val())) {
            $("#newpassword").addClass("input_error");
            error.push("error")
        }
        else {
            $("#newpassword").removeClass("input_error");
        }

        if (isEmpty($("#confirm_newpassword").val())) {
            $("#confirm_newpassword").addClass("input_error");
            error.push("error")
        }
        else {
            $("#confirm_newpassword").removeClass("input_error");
        }    

        if (error.length > 0)
            return false;
        else
            return true;
    }
</script>