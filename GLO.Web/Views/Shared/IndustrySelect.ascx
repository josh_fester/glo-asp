﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<GLO.Data.Models.GLO_IndustryCategory>>" %>
<div class="multiple_industry">
 <div class="industry_left">
      <h2>Industry Category</h2>
      <div class="industry_list">
      <%int industryIndex1 = 0; 
        foreach (var industry in Model.Where(x => x.ParentID == 0).OrderBy(m => m.OrderBy).ToList())
        {
        %>
            <a href="javascript:void(0);" class="<%=industryIndex1==0?"current":"" %>" x:industryid="<%=industry.IndustryID %>"><%=industry.IndustryName%></a>
            <%
            industryIndex1++;
        } %>                                                   
      </div>
  </div>
  <div class="industry_right">
      <h2>Industry List</h2>
      <div class="industry_list">
      <%int industryIndex2 = 0; 
        foreach (var industry in Model.Where(x => x.ParentID == 0).OrderBy(m => m.OrderBy).ToList())
        { %>
          <table class="industry_item" id="industry<%=industry.IndustryID%>" style="<%=industryIndex2==0?"display:table":"" %>">

              <%
              int i = 0;
              var list = Model.Where(x => x.ParentID == industry.IndustryID).OrderBy(x=>x.OrderBy).ToList();
              %>

              <%foreach (var childindustry in list)
              { %>

                  <%if (i % 3 == 0 || i == 0) { %><tr><%} %>
                    <td><a href="javascript:void(0);" x:id="<%=childindustry.IndustryID %>"><%=childindustry.IndustryName%></a></td>
                  <%if (i % 3 == 2 || i == list.Count - 1) { %></tr><%} %>

              <%
                i++;
              } %>  
                    
         </table>        
        <% 
            industryIndex2++;
         } %>
      </div>
  </div>
  <div class="me_close"><a class="IndustryClose" href="javascript:void(0);"><img src="/Styles/images/icon_close.gif"></a></div>  
</div>

<script type="text/javascript">

    $(document).ready(function () {
        //for industry start
        $(".multiple_box").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();
            $(this).find(".multiple_wrapper").toggle();
            if ($(window).height() < 640) $(this).find(".multiple_industry").css("margin-top", "10px");
        });
        $(document).click(function () {
            $('.multiple_wrapper').hide();
        });
        $(".industry_left a").click(function () {
            var industryid = $(this).attr("x:industryid");

            $(".industry_left a").removeClass('current');
            $(this).addClass('current');
            $(this).parents(".multiple_industry").find(".industry_item").hide();
            $(this).parents(".multiple_industry").find("#industry" + industryid).show();
        });
        $(".industry_item a").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();

            var value = $(this).text();
            var id = $(this).attr("x:id");
            $(this).parents(".multiple_box").find(".multiple_id").val(id);
            $(this).parents(".multiple_box").find(".multiple_value").val(value);
            $(this).parents(".multiple_box").removeClass("select_error");
        });
        $(".IndustryClose").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();
        });
        //for industry end
    });

</script>