﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Competencies>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="timeline_edit">
        <div class="timeline_arrow">
        </div>
        <div id="TimeLineAdd">
            <div class="timeline_summary">
            </div>
            <div class="timeline_education">
            </div>
            <div class="timeline_contribution">
            </div>
            <div id="Employment">
                <input type="text" id="CompetencyContent" value="<%=Model.CompetencyContent %>" maxlength="20">&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" id="Delete">Delete</a>
            </div>
        </div>
        <div class="me_apply">
            <a href="javascript:void(0)" id="btnSave">
                <img src="/Styles/images/icon_apply.gif" /></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnSave").click(function () {
            var competencyID = '<%=Model.CompetencyID %>';
            var competencyContent = $('#CompetencyContent').val().trim();
            var userID = '<%=Model.UserID %>';
            if (competencyID == "" || competencyContent == "") {
                return false;
            }
            $.postJSON('<%=Url.Action("EditCompetency","Competence") %>', { competencyID: competencyID, competencyContent: competencyContent }, function (data) {
                if (data) {
                    window.location.href = "/Expert/MyCompetence?expertid=" + userID;
                }
                else {
                    alertbox("Edit failed.");
                }
            });
        });
        $("#Delete").click(function () {
            confirmbox("Do you want to delete this Competence?", function () {
                var competencyID = '<%=Model.CompetencyID %>';
                var userID = '<%=Model.UserID %>';
                if (competencyID == "") {
                    return false;
                }
                else {
                    $.postJSON('<%=Url.Action("DeleteCompetency","Competence") %>', { competencyID: competencyID }, function (data) {
                        if (data) {
                            window.location.href = "/Expert/MyCompetence?expertid=" + userID;
                        }
                        else {
                            alertbox("Delete failed.");
                        }
                    });
                }
            });

        });
    });
</script>
