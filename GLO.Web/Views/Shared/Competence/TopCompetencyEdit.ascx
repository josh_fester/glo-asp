﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Competencies>>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="competence_editor">
        <div class="competence_arrow"></div>
        <table id="CompetencyEdit">
            <tr>
                <th>My top 6 competencies</th>
            </tr>
            <% 
                int i = 1;
                foreach (var item in Model)
                { %>
            <tr>
                <td id ="td<%=i %>">
                    <%=i %>.
                    <input class="font" type="text" id="<%=item.CompetencyID %>"  x:oldvalue="<%=item.CompetencyContent %>" value="<%=item.CompetencyContent %>" maxlength="50"/>
                    <a class="CompetencyValue" href="javascript:void(0);" title="Empty this field."><img src="/Styles/images/icon_delete1.gif" /></a>
                </td>
            </tr>
            <%
                    i++;
                } %>
        </table>
        <div class="me_apply">
            <a href="javascript:void(0)" id="Save">
                <img src="/Styles/images/icon_apply.gif" /></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var isFirstSave = true;
        var showConfrim = false;
        $('#CompetencyEdit').find(":text").each(function (i) {
            if ($(this).attr('x:oldvalue') != "") {
                isFirstSave = false;
            }
        });
        if ($('.r_user_box').length > 1) {
            showConfrim = true;
        }

        $('#CompetencyEdit').find(":text").blur(function () {
            if ($(this).val() != '') $(this).removeClass("input_error");
        });

        $("#Save").click(function () {
            var competencyID = "";
            var competencyContent = "";
            var html = "";
            var allEmpty = true;
            $('#CompetencyEdit').find(":text").each(function (i) {
                if ($(this).attr('x:oldvalue') != $(this).val()) {
                    competencyID += $(this).attr('id') + ";";
                    competencyContent += $(this).val() + ";";
                    html += "<div class='value_item_active'>" + $(this).val() + "</div>";
                }
                if ($(this).val() != "") {
                    allEmpty = false;
                }
            });
            if (isFirstSave) {
                if (competencyContent == "") {
                    $('#td1').find(":text").addClass("input_error");
                    return false;
                }
                else {
                    $('#CompetencyEdit').find(":text").removeClass("input_error");
                }
            }
            else {
                if (allEmpty || competencyContent == ";;;;;;") {
                    $('#td1').find(":text").addClass("input_error");
                    return false;
                }
                else {
                    $('#CompetencyEdit').find(":text").removeClass("input_error");
                }
            }
            if (!isFirstSave) {
                if (competencyContent != "") {
                    if (showConfrim) {
                        confirmbox("Do you want to save changes? If yes, new recommendation message will send to current recommender and ask them to reconfirm your new competencies.", function () {
                            $.getJSON('<%=Url.Action("EditCompetency","Competence") %>', { competencyID: competencyID, competencyContent: competencyContent, isFirstSave: false }, function (data) {
                                window.location.reload();
                            });
                        });
                    }
                    else {
                        $.getJSON('<%=Url.Action("EditCompetency","Competence") %>', { competencyID: competencyID, competencyContent: competencyContent, isFirstSave: false }, function (data) {
                            window.location.reload();
                        });
                    }
                }
                else {
                    CloseLightBox();
                }
            }
            else {
                $.getJSON('<%=Url.Action("EditCompetency","Competence") %>', { competencyID: competencyID, competencyContent: competencyContent, isFirstSave: true }, function (data) {
                    $(".map_note").hide();      
                    $('#showCompetency').html(html);
                    $("#editRecommend").click();
                });
            }

        });

        $(".CompetencyValue").click(function () {
            $(this).parent().find("input").val("");
        });
    });
</script>