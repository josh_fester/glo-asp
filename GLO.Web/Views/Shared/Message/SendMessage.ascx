﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.MessageModel>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_message">
        <div class="tag_line">
            <%: Html.HiddenFor(model => model.MessageID) %>
            <%: Html.HiddenFor(model => model.ReceiveUserID) %>
            <table>
                <tr>
                    <td>
                        Title
                    </td>
                    <td>
                        <input type="text" class="title" id="messagetitle" value="<%= Model.MessageTitle %>" maxlength="100" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Content
                    </td>
                    <td>
                        <textarea class="content" id="messagecontent"><%= Model.MessageContent%></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div class="me_apply">
            <a id="btnSend" href="javascript:void(0);">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
     $(document).ready(function () {
         $("#btnSend").click(function () {
             if (ValidateInfo() == false)
                    return;
             var receiveId=$('#ReceiveUserID').val()+";";
             $.postJSONnoQS('<%=Url.Action("SendMessage") %>', { messageid:<%=Model.MessageID %>, senduserid: <%=Model.SendUserID %>, receiveuserid: receiveId ,title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                alertbox("Your message was successfully sent！", function(){
                    CloseLightBox();
                });
             }); 
         });
     });
    function ValidateInfo() {
        var error = new Array();
        if (isEmpty($("#messagetitle").val())) {
            $("#messagetitle").addClass("input_error");
            error.push("error")
        }
        else {
            $("#messagetitle").removeClass("input_error");
        }
        if (isEmpty($("#messagecontent").val())) {
            $("#messagecontent").addClass("input_error");
            error.push("error")
        }
        else {
            $("#messagecontent").removeClass("input_error");
        }
        if (error.length > 0)
            return false;
        else
            return true;
    }
</script>
