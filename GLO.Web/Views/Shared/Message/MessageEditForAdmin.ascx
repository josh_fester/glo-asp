﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.MessageModel>" %>
<div class="box_form">
    <%: Html.HiddenFor(model => model.MessageID) %>

    <table class="tbeditor">
        <tr>
            <td>
                Send to
            </td>
            <td>
                <div class="form_receive">
                    <select id="SendType">
                        <option value="1" <%if(Model.AdminSendType==1){ %> selected <%} %>>All user</option>
                        <option value="2" <%if(Model.AdminSendType==2){ %> selected <%} %>>All company</option>
                        <option value="3" <%if(Model.AdminSendType==3){ %> selected <%} %>>All leader</option>
                        <option value="4" <%if(Model.AdminSendType==4){ %> selected <%} %>>All expert</option>
                        <option value="5" <%if(Model.AdminSendType==5){ %> selected <%} %>>Custom</option>
                    </select>
                    <input type="button" value="Select" id="SelectUser" class="btn_small" style="display: none;" />
                    <div id="divReceive" style="margin-top:5px;">
                        <%
                            if (Model.ReceiveUserList != null && Model.ReceiveUserList.Count > 0)
                            {
                                foreach (var receiveUser in Model.ReceiveUserList)
                                { %>
                          <div id="<%=receiveUser.UserID %>" class="value_item_active"><%=receiveUser.UserName%><div class="user_value_delete"><a id="<%=receiveUser.UserID %>" onclick="DeleteUser(this);" title="delete" href="javascript:void(0);"><img src="/Styles/images/icon_cancel.gif"></a></div></div>
                        <%}
                        } %>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Title
            </td>
            <td>
                <input type="text" class="txt_form" id="messagetitle" value="<%= Model.MessageTitle %>" maxlength="100" />
            </td>
        </tr>
        <tr>
            <td>
                Content
            </td>
            <td>
                <textarea class="area_form" id="messagecontent"><%= !string.IsNullOrEmpty(Model.MessageContent)?Model.MessageContent.Replace("<br/>","\r\n"):Model.MessageContent%></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input id="btnSend" type="button" value="Send" class="btn_submit" />
                <%if (ViewData["sender"] == null)
                 {%>
                <input id="btnSave" type="button" value="Save" class="btn_submit" />
                <%} %>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
     $(document).ready(function () {        
         $("#SelectUser").click(function(){
            $.getJSON('_UserList', { random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });                    
        });
         $("#SendType").change(function(){
            if($(this).val()=="5")
            {
                $('#SelectUser').css('display','');
            }
            else{
                $('#SelectUser').css('display','none');
            }
         });
         if($("#SendType").val()=="5")
            {
                $('#SelectUser').css('display','');
            }
            else{
                $('#SelectUser').css('display','none');
            }
         $("#btnSend").click(function () {     
             if($('#SendType').val()=="5" && $('#divReceive').html().trim()=="")
             {
                alertbox("You should select at least one contact！");
                return false;
             }             
             var receiveId="";
             $('#divReceive').find(".value_item_active").each(function(i){                
                receiveId +=$(this).attr('id');
                receiveId +=";";                 
            });
             $.postJSONnoQS('<%=Url.Action("AdminSendMessage") %>', { messageid:<%=Model.MessageID %>, senduserid: <%=Model.SendUserID %>, sendtype:$('#SendType').val(),receiveuserid: receiveId,title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                alertbox("Your message was successfully sent！", function(){
                    window.location.reload();
                });
             }); 
         });
         $("#btnSave").click(function () {         
             var receiveId="";
             $('#divReceive').find(".value_item_active").each(function(i){                
                receiveId +=$(this).attr('id');
                receiveId +=";";                 
            });
             if($('#MessageID').val() !="0")
             {    
                 $.postJSONnoQS('<%=Url.Action("AdminEditMessage") %>', { messageid: <%=Model.MessageID %>, sendtype:$('#SendType').val(),receiveuserid: receiveId,title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                   alertbox("Your message was successfully saved！", function(){
                        window.location.reload();
                   });
                 });              
             }
             else{       
                 $.postJSONnoQS('<%=Url.Action("AdminSaveMessage") %>', { senduserid: <%=Model.SendUserID %>, sendtype:$('#SendType').val(),receiveuserid: receiveId, title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                   alertbox("Your message was successfully saved！", function(){
                        window.location.reload();
                   });
                 }); 
             }
         });
     });
    function DeleteUser(obj) {                  
        $('#'+obj.id).remove();
	}
</script>
