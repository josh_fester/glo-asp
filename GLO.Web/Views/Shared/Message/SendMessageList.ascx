﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Web.Models.MessageModel>>" %>
<% int mark = 1; 
   foreach (var message in Model)
   {  %>
   <div id="<%=message.MessageID %>" class="<%=mark==1?"message_active":"" %>">
    <div class="message_line">
        <div class="message_action2">
            <table>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="messagedetail hyplink" x:messageid="<%=message.MessageID %>">
                            <%=message.MessageTitle==""?"No Subject":message.MessageTitle%></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="messagedetail hyplink" x:messageid="<%=message.MessageID %>" x:receiveid="<%=message.ReceiveID%>">To:<%=message.ReceiveUserNikeName%></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="messageedit" x:messageid="<%=message.MessageID %>" x:status="forward">Forward</a> | 
                        <a href="javascript:void(0)" class="messagedelete" x:messageid="<%=message.MessageID %>">Delete</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<%mark++;
   } %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".messagedetail").click(function () {
            $.getJSON('_SendMessageDetail', { "messageid": $(this).attr("x:messageid"), "listType": 1, random: Math.random() }, function (data) {
                $('.message_content').empty().html(data.Html);
            });

            var messageid = $(this).attr("x:messageid");
            $(".message_active").removeClass("message_active");
            $("#" + messageid).addClass("message_active");
        });
        $(".messageedit").click(function () {
            $.getJSON('_SendMessageEdit', { "messageid": $(this).attr("x:messageid"), "receiveid": "0", "status": $(this).attr("x:status") }, function (data) {
                $('#lightbox_sendemail').empty().html(data.Html);
                $("#lightboxwrapper2").attr("style", "display:block");
               // $('.message_content').empty().html(data.Html);
            });
        });
        $(".messagedelete,.messagearchive").click(function () {
            var messageid = $(this).attr("x:messageid");

            confirmbox("Do you want to delete this message?", function () {
                $.postJSON('<%=Url.Action("_SendMessageDelete") %>', { messageid: messageid }, function (data) {
                    if (data) {
                        $("#" + messageid).empty();
                        $('.message_content').empty();
                    }
                });            
            });
        });

        $(".message_action2 .messagedetail").first().click();
    });
</script>
