﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.MessageModel>" %>

<table class="message_detail">
    <tr>
        <td class="message_title" colspan="2">
            <%=Model.MessageTitle == "" ? "No Subject" : Model.MessageTitle%>
        </td>
    </tr>
    <tr>
        <td class="message_from">
            To:<%=Model.ReceiveUserNikeName%>
        </td>
        <td class="message_date">
            <%=Model.SendDate %>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="message_description">
            <%=Model.MessageContent.Replace("\n","<br/>") %>
        </td>
    </tr>
</table>
