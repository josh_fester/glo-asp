﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.MessageModel>" %>

<div class="box_form">
    <%: Html.HiddenFor(model => model.MessageID) %>
    <table>
        <tr>
            <td>
                <b>Send to</b>
            </td>
            <td>
                <div class="form_receive">
                    <input type="button" value="Select" id="SelectUser"/>
                    <div id="divReceive">
                        <%
                            if (Model.ReceiveUserList != null && Model.ReceiveUserList.Count > 0)
                            {
                                foreach (var receiveUser in Model.ReceiveUserList)
                                { %>
                          <div id="<%=receiveUser.UserID %>" class="value_item_active"><%=receiveUser.UserName%><div class="user_value_delete"><a id="<%=receiveUser.UserID %>" onclick="DeleteUser(this);" title="delete" href="javascript:void(0);"><img src="/Styles/images/icon_cancel.gif"></a></div></div>
                        <%}
                            } %>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <b>Title</b>
            </td>
            <td>
                <input type="text" class="txt_form" style="width:400px;" id="messagetitle" value="<%= Model.MessageTitle %>" maxlength="100" />
            </td>
        </tr>
        <tr>
            <td>
                <b>Content</b>
            </td>
            <td>
                <textarea class="area_form" style="width:400px;height:180px" id="messagecontent"><%= !string.IsNullOrEmpty(Model.MessageContent)?Model.MessageContent.Replace("<br/>","\r\n"):Model.MessageContent%></textarea>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="button" class="btn_form" id="btnSend" value="Send" />&nbsp;&nbsp;<input
                    type="button" class="btn_form" id="btnSave" value="Save" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
     $(document).ready(function () {
         $("#btnSend").click(function () {    
             if($('#divReceive').html().trim()=="")
             {
                alertbox("You should select at least one contact！");
                return false;
             }
             var receiveId="";
             $('#divReceive').find(".value_item_active").each(function(i){                
                receiveId +=$(this).attr('id');
                receiveId +=";";                 
            });
             $.postJSONnoQS('<%=Url.Action("SendMessage") %>', { "messageid": <%=Model.MessageID %>, "senduserid": <%=Model.SendUserID %>, "receiveuserid": receiveId ,"title": $('#messagetitle').val(), "content": $('#messagecontent').val() }, function (data) {
                alertbox("Your message was successfully sent！", function(){                    
                     $('#divReceive').html("");
                     $('#messagetitle').val("");
                     $('#messagecontent').val("");
                    window.location.reload();
                });
             }); 
         });
         $("#btnSave").click(function () {   
             var receiveId="";
             $('#divReceive').find(".value_item_active").each(function(i){                
                receiveId +=$(this).attr('id');
                receiveId +=";";                 
            });
             if($('#MessageID').val() !="0")
             {    
                 $.postJSONnoQS('<%=Url.Action("EditMessage") %>', { "messageid": <%=Model.MessageID %>, "receiveuserid": receiveId,"title": $('#messagetitle').val(), "content": $('#messagecontent').val() }, function (data) {
                   alertbox("Your message was successfully saved！", function(){                   
                         $('#divReceive').html("");
                         $('#messagetitle').val("");
                         $('#messagecontent').val("");
                        window.location.reload();
                   });
                 });              
             }
             else{       
                 $.postJSONnoQS('<%=Url.Action("SaveMessage") %>', { "senduserid": <%=Model.SendUserID %>, "receiveuserid": receiveId, "title":$('#messagetitle').val(), "content":$('#messagecontent').val()}, function (data) {
                   alertbox("Your message was successfully saved！", function(){                   
                         $('#divReceive').html("");
                         $('#messagetitle').val("");
                         $('#messagecontent').val("");
                        window.location.reload();
                   });
                 }); 
             }
         });
         $("#SelectUser").click(function(){
            $.getJSON('_TagUserList', { "expertid": <%=Model.SendUserID %> ,random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });                    
        });
     });
    function DeleteUser(obj) {                  
        $('#'+obj.id).remove();
	}
</script>
