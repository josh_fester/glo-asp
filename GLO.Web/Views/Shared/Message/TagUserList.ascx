﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.ReceiveUserModel>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="selectuser_box">
        <div class="selectuser_header">
            <div id="headrecent" class="selectuser_item item_active">Recent Contact</div>
            <div id="headuser" class="selectuser_item item_middle">Tagged Users</div>
        </div>
        <div id="divrecent" class="selectuser_container">
            <table id="tbrecent">
                    <tr>
                            <td width="130" class="tdheader">
                            Search
                        </td>
                        <td class="tdheader">
                            <input type="text" id="txtSearchContact" /><input type="button" value="Search" onclick="SearchContact()" />
                        </td>
                    </tr>
                    <tr>
                            <td valign="top">
                            Recent contact
                        </td>
                        <td>
                            <div id="ContactUsers" class="selectuser_details">
                            <%if (Model.RecentContactList.Count > 0)
                                { %>
                            <%foreach (var user in Model.RecentContactList)
                                { %>
                                    <div class="selectuser_list">
                                        <div class="selectuser_icon"><img src="/images/<%=!String.IsNullOrEmpty(user.Icon)?user.Icon:"user_none.gif" %>" /><input type="checkbox" name="name" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" /></div>
                                        <div class="selectuser_name" title="<%=user.NickName%>"><%=user.NickName%></div>
                                        <div class="selectuser_name" title="<%=user.Title%>"><%=user.Title%></div>
                                        <div class="selectuser_name" title="<%=user.Location%>"><%=user.Location%></div>
                                    </div>                            
                            <%}
                                }
                                else
                                {%>
                                No recent contact.
                                <%} %>
                            </div>
                        </td>
                    </tr>
                </table>
        
        </div>
        <div id="divuser" class="selectuser_container" style="display:none">
            <table id="tbuser">
                    <tr>
                            <td width="130" class="tdheader">
                            Search
                        </td>
                        <td class="tdheader">
                            <input type="text" id="txtSearchUser" /><input type="button" value="Search" onclick="SearchUser()" />
                        </td>
                    </tr>
                    <tr>
                            <td valign="top">
                            Tagged Users
                        </td>
                        <td id="TagedUsers"> 
                            <div class="selectuser_details">
                            <%if (Model.TagUserList.Count > 0)
                                { %>
                            <%foreach (var user in Model.TagUserList)
                                { %>
                                    <div class="selectuser_list">
                                        <div class="selectuser_icon"><img src="/images/<%=!String.IsNullOrEmpty(user.Icon)?user.Icon:"user_none.gif" %>" /><input type="checkbox" name="name" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" /></div>
                                        <div class="selectuser_name" title="<%=user.NickName%>"><%=user.NickName%></div>
                                        <div class="selectuser_name" title="<%=user.Title%>"><%=user.Title%></div>
                                        <div class="selectuser_name" title="<%=user.Location%>"><%=user.Location%></div>
                                    </div>                       
                                
                                <%}
                            }
                            else
                            {%>
                            No tagged user.
                            <%} %>
                            </div>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="me_apply"><a href="javascript:void(0)" id="btnValue"><img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close2"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<div id="hidUsers">
    <%foreach (var user in Model.RecentContactList)
      { %>
    <input type="hidden" name="ContactUser" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" x:icon="<%=user.Icon %>"  x:title="<%=user.Title %>"  x:location="<%=user.Location %>"  />
    <%} %>
    <%foreach (var user in Model.TagUserList)
      { %>
    <input type="hidden" name="TagedUser" x:userid="<%=user.UserID %>" x:username="<%=user.NickName %>" x:icon="<%=user.Icon %>"   x:title="<%=user.Title %>"  x:location="<%=user.Location %>"  />
    <%} %>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnValue").click(function () {
            var idStr = "";
            var nameStr = "";
            var html = $('#divReceive').html();
            $(':checkbox').each(function (i) {
                if ($(this).attr("checked") == "checked") {
                    if ($('#divReceive').find("#" + $(this).attr('x:userid')).length == 0) {
                        idStr += $(this).attr('x:userid') + ";";
                        nameStr += $(this).attr("x:username") + ";";
                        html += "<div class='value_item_active' id=\"" + $(this).attr('x:userid') + "\">" + $(this).attr("x:username") + "<div class=\"user_value_delete\"><a href=\"javascript:void(0);\" title=\"delete\" id=\"" + $(this).attr('x:userid') + "\"  onclick=\"DeleteUser(this);\"><img src=\"/Styles/images/icon_cancel.gif\" /></a></div></div>";
                    }
                }
            });
            if (html != "") {
                $("#divReceive").html(html);
            }
            CloseLightBox();
        });
        $("#headrecent").click(function () {
            $(".selectuser_item").removeClass("item_active");
            $(this).addClass("item_active");
            $("#divrecent").show();
            $("#divuser").hide();
        });
        $("#headuser").click(function () {
            $(".selectuser_item").removeClass("item_active");
            $(this).addClass("item_active");
            $("#divrecent").hide();
            $("#divuser").show();
        });
    });
    function SearchContact() {
        var searchValue = $('#txtSearchContact').val().trim().toLocaleLowerCase();
        var newHtml = "";
        if (searchValue == "") {
            $("input[name='ContactUser']").each(function () {
                newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
            });
        }
        else {
            $("input[name='ContactUser']").each(function () {
                if ($(this).attr("x:username").toLocaleLowerCase().indexOf(searchValue) >= 0) {
                    newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
                }
            });
        }
        if (newHtml != "") {
            $('#ContactUsers').empty().html(newHtml);
        }
        else {
            $('#ContactUsers').empty().html("No match record.");
        }
    }
    function SearchUser() {
        var searchValue = $('#txtSearchUser').val().trim().toLocaleLowerCase();
        var newHtml = "";
        if (searchValue == "") {
            $("input[name='TagedUser']").each(function () {
                newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
            });
        }
        else {
            $("input[name='TagedUser']").each(function () {
                if ($(this).attr("x:username").toLocaleLowerCase().indexOf(searchValue) >= 0) {
                    newHtml += BuildHtml($(this).attr("x:userid"), $(this).attr("x:username"), $(this).attr("x:icon"), $(this).attr("x:title"), $(this).attr("x:location"));
                }
            });
        }
        if (newHtml != "") {
            $('#TagedUsers').empty().html(newHtml);
        }
        else {
            $('#TagedUsers').empty().html("No match record.");
        }
    }
    function BuildHtml(userid, username, icon, title, location) {
        return "<div class=\"selectuser_list\"><div class=\"selectuser_icon\"><img src=\"/images/" + icon + "\" /><input type=\"checkbox\" name=\"name\" x:userid=\"" + userid + "\" x:username=\"" + username + "\" /></div><div class=\"selectuser_name\">" + username + "</div><div class=\"selectuser_name\">" + title + "</div><div class=\"selectuser_name\">" + location + "</div></div>";
    }
</script>
