﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<input type="hidden" id = "thisPageUrl" value="<%=HttpContext.Current.Request.RawUrl %>"/>
  <% if (Request.IsAuthenticated == false){%>
  <div class="login_mask">
                <div class="login_window">
                    <table>
                        <tr>
                            <th colspan="2"><%if (ViewData["type"] == "searchjob")
                                              { %>
                                Jobs are accessible to registered users only<br/>
                                Kindly sign in or join GLO free.<br />
                                <%}else { %>
                                 Profiles of companies, experts and leaders<br />
                                are accessible to registered users only!<br />
                                Kindly sign in or join GLO free.<br />
                                <%} %>
                            </th>
                        </tr>
                        <tr>
                            <td class="title"><b>Email Address</b></td>
                            <td>
                                <input type="text" id="txt_Username" /><br />                          
                            </td>
                        </tr>
                        <tr>
                            <td class="title"><b>Password</b></td>
                            <td>
                                <input type="password" id="txt_Password" /><br />  
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" id="btn_unregisterlogin" class="bluebtn2" value="Sign In" />&nbsp;&nbsp;&nbsp;<%: Html.ActionLink("Forgot Password?", "forgotpassword", "account")%>&nbsp;&nbsp;&nbsp;<%: Html.ActionLink("New member?", "RegisterGuide","Home")%></td>
                        </tr>
                        <tr>
                            <td></td><td id="error"></td>
                        </tr>
                    </table>

                </div>
        </div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_unregisterlogin').click(function () {
            var username = $("#txt_Username").val();
            var password = $("#txt_Password").val();

            if (username == "" || password == "") {
                $('#error').html("Please enter your email and password");
                return false;
            }
            else {

                $.postJSON('<%=Url.Action("JsonLogin","Account") %>', { "username": username, "password": password }, function (data) {
                    if (data.Success == true) {
                        if (data.ToLeaderList) {
                            url = '<%=Url.Action("LeaderList","Account") %>' + "?email=" + data.Email + "&returnUrl=" + $('#thisPageUrl').val();
                            window.location.href = url;
                        } else {
                            window.location.reload();
                        }
                        return false;
                    }
                    else {
                        var msg = "Email Address or Password was incorrect";
                        if (data.Result == 1) {
                            msg = "Email is not existing";
                        }
                        else if (data.Result == 2) {
                            msg = "Password is wrong";
                        }
                        else if (data.Result == 3) {
                            msg = "Your account has been blocked!<br/>Please click <a href='/Home/Contact'>here</a> to contact GLO admin";
                        }
                        else if (data.Result == 4) {
                            msg = "Your have exceeded 5 tries. Please try an hour later";
                        }
                        else if (data.Result == 5) {
                            msg = "User is not approved";
                        }

                        $('#error').html(msg);
                        return false;
                    }
                });

            }
            return false;
        });

        $('#txt_Password').keypress(function (e) {
            var e = e || window.event;
            if (e.keyCode == 13) {
                $("#btn_unregisterlogin").click();
            }
        });
    });
</script>  