﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.RegisterModel>" %>
<script src="<%=Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%=Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
<script src="<%=Url.Content("~/Scripts/jquery.cl.ajaxCheckCode.js") %>" type="text/javascript"></script>
<style>
    .input-validation-error
    {
        border-color: Red;
    }
</style>
<% using (Html.BeginForm("RegisterUser", "Account", FormMethod.Post, new { id = "frm" }))
   { %>
<%: Html.ValidationSummary(true) %>
<%: Html.HiddenFor(m=>m.UserType) %>
<div class="lightbox_mask">
</div>
<div class="lightbox_register">
    <table>
        <tr>
            <td width="140">
                email
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.Email, new { @class = "txtbox1", maxlength = "30" })%>
                <span class="required">*</span>
            </td>
        </tr>
        <tr>
            <td>
                your name
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.NickName, new { @class="txtbox1", maxlength="30" })%>
                <span class="required">*</span><br />
                4 to 30 letter characters and case insensitive.
            </td>
        </tr>
        <tr>
            <td>
                password
            </td>
            <td>
                <%: Html.PasswordFor(m => m.Password, new { @class="txtbox1", maxlength="12" })%>
                <span class="required">*</span><br />
                between 6 to 12 characters and case sensitive.
            </td>
        </tr>
        <tr>
            <td>
                confirm password
            </td>
            <td>
                <%: Html.PasswordFor(m => m.ConfirmPassword, new { @class="txtbox1", maxlength="12" })%>
                <span class="required">*</span><br />
            </td>
        </tr>
        <tr>
            <td>
                location
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.Location, new { @class="txtbox1", maxlength="30" })%>
                <span class="required">*</span>
            </td>
        </tr>
        <tr>
            <td>
                phone number
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.PhoneNumber1, new { @class = "txtbox2" })%>
                -
                <%: Html.TextBoxFor(m => m.PhoneNumber2, new { @class = "txtbox2" })%>
                -
                <%: Html.TextBoxFor(m => m.PhoneNumber3, new { @class = "txtbox2" })%>
            </td>
        </tr>
        <tr>
            <td>
                check code
            </td>
            <td>
                <%:Html.TextBoxFor(m => m.CheckCode, new { MaxLength = "10", @class = "txtbox2", size = "50" })%>
                <span class="required">*</span>
                <span><%:Html.CheckCodeImage()%></span> <a id="changeImg" href="#">change?</a>
                <br />
                <%:Html.ValidationMessageFor(model => model.CheckCode)%>
            </td>
        </tr>
        <tr>
            <td style="color:red;padding-top:10px;" colspan="2" id="errorMessage">
                <%: Html.ValidationMessageFor(m => m.Email)%><br />
                <%: Html.ValidationMessageFor(m => m.NickName)%><br />
                <%: Html.ValidationMessageFor(m => m.Password)%><br />
                <%: Html.ValidationMessageFor(m => m.ConfirmPassword)%><br />
                <%: Html.ValidationMessageFor(m => m.Location)%><br />
                <%: Html.ValidationMessageFor(m => m.CheckCode)%>
            </td>
        </tr>
    </table>
    <div class="me_apply">
        <a href="javascript:void(0)" id="register">
            <img src="/Styles/images/icon_apply.gif"></a></div>
    <div class="me_close">
        <a onclick="return CloseLightBox()" href="javascript:void(0)">
            <img src="/Styles/images/icon_close.gif"></a></div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxCheckCode();
        $('#changeImg').click();
        $("#register").click(function () {
            $('.input_error').each(function () {
                $(this).removeClass("input_error");
            });
            if (ValidateAssign() == false)
                return;
            var postData = $("#frm").serialize();
            $.post("/Account/RegisterUser", postData, function (data) {
                if (data.result == false) {
                    $('#changeImg').click();
                    alertbox(data.message);
                    return false;
                }
                else {
                    alertbox("Your registration was successful!");
                }

                var url = '';
                if (data.TypeID == 1)
                    url = '<%=Url.Action("DashBoard","Leader") %>' + "?leaderid=" + data.UserID;
                else if (data.TypeID == 2) {
                    if (data.IsReommend)
                    //RegisterSuccess
                        url = '<%=Url.Action("RegisterProfile","Home") %>' + "?expertid=" + data.UserID;
                    else
                        url = '<%=Url.Action("MyValue","Expert") %>' + "?expertid=" + data.UserID;
                }
                else
                    url = '<%=Url.Action("CompanyIndex","Company") %>' + "?companyid=" + data.UserID;
                window.location.href = url;
                return false;
            });
        });
    });
    function ValidateAssign() {
        var error = new Array();
        var message = "";

        if (isEmpty($("#Email").val())) {
            message += "The email is required.<br/>";
            $("#Email").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Email").removeClass("input_error");
        }

        if (isEmpty($("#NickName").val())) {
            message += "The your name is required.<br/>";
            $("#NickName").addClass("input_error");
            error.push("error")
        }
        else {
            $("#NickName").removeClass("input_error");
        }

        if (isEmpty($("#Password").val())) {
            message += "The password is required.<br/>";
            $("#Password").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Password").removeClass("input_error");
        }

        if (isEmpty($("#ConfirmPassword").val())) {
            message += "The confirm password is required.<br/>";
            $("#ConfirmPassword").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConfirmPassword").removeClass("input_error");
        }

        if ($("#Password").val() != $("#ConfirmPassword").val()) {
            message += "The password and confirm password do not match.<br/>";
            $("#ConfirmPassword").addClass("input_error");
            error.push("error")
        }
        else {
            $("#ConfirmPassword").removeClass("input_error");
        }

        if (isEmpty($("#Location").val())) {
            message += "The location is required.<br/>";
            $("#Location").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Location").removeClass("input_error");
        }

        /*if (isEmpty($("#CheckCode").val())) {
            message += "The check code is required.";
            $("#CheckCode").addClass("input_error");
            error.push("error")
        }
        else {
            $("#CheckCode").removeClass("input_error");
        }*/
        if (error.length > 0) {
            return false;
        }
        else
            return true;
    }
</script>
