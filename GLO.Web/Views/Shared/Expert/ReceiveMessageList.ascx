﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_MessageReceive>>" %>
<% foreach (var message in Model)
   {  %>
<div id="<%=message.ReceiveID %>">
    <div class="message_line">
        <div class="message_usericon">
            <a href="javascript:void(0)">
                <img src="/images/<%=message.GLO_Users.Icon %>" /></a>
        </div>
        <div class="message_action">
            <table>
                <tr>
                    <td>
                        <b>
                            <%=message.GLO_Users.NickName%></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="messagedetail" x:messageid="<%=message.MessageID %>"
                            x:receiveid="<%=message.ReceiveID%>">
                            <%=message.GLO_Message.MessageTitle%></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%if (ViewBag.Status == "0")
                          { %>
                        <a href="javascript:void(0)" class="messageedit" x:messageid="<%=message.GLO_Message.MessageID %>" x:receiveid="<%=message.ReceiveID%>">Reply</a> |
                        <%} %>
                        <a href="javascript:void(0)">Forward</a> | 
                        <%if (ViewBag.Status == "0")
                          { %>
                        <a href="javascript:void(0)" class="messagedelete" x:messageid="<%=message.ReceiveID %>" x:receiveid="<%=message.ReceiveID%>" x:status=2>Delete</a>
                        <%}
                          else
                          { %>
                        <a href="javascript:void(0)" class="messagedelete" x:messageid="<%=message.ReceiveID %>" x:receiveid="<%=message.ReceiveID%>" x:status=3>Delete</a>
                        <%} %>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".messagedetail").click(function () {
            $.getJSON('_SendMessageDetail', { "messageid": $(this).attr("x:messageid"), random: Math.random() }, function (data) {
                $('.message_content').empty().html(data.Html);
            });
        });
        $(".messagedelete,.messagearchive").click(function () {
            var receiveid = $(this).attr("x:receiveid");
            var status = $(this).attr("x:status");
            if (confirm("Are you sure delete this message?")) {
                if (status == "2") {
                    $.postJSON('<%=Url.Action("MessageUpdate") %>', { receiveid: receiveid, status: 2 }, function (data) {
                        if (data) {
                            $("#" + receiveid).empty();
                        }
                    });
                }
                else {
                    alert(receiveid)
                    $.postJSON('<%=Url.Action("_ReceiveMessageRealDelete") %>', { receiveid: receiveid }, function (data) {
                        if (data) {
                            $("#" + receiveid).empty();
                        }
                    });
                }
            }
        });
    });
</script>
