﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_MessageSend>" %>
<div class="box_form">
<%: Html.HiddenFor(model => model.MessageID) %>
<table>
    <tr>
        <td>
            <b>Send to</b>
        </td>
        <td>
            <div class="form_receive">
                <input type="text" class="txt_form" id="receive" readonly="true" x:sendid="" value="" />
                <div id="taguserlist" class="receive_wrapper">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <b>Title</b>
        </td>
        <td>
            <input type="text" class="txt_form" id="messagetitle" value="<%= Model.GLO_Message.MessageTitle %>" />
        </td>
    </tr>
    <tr>
        <td>
            <b>Content</b>
        </td>
        <td>
            <textarea class="area_form" id="messagecontent"><%= Model.GLO_Message.MessageContect%></textarea>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <input type="button" class="btn_form" id="btnSend" value="Send" />&nbsp;&nbsp;<input
                type="button" class="btn_form" id="btnSave" value="Save" />
        </td>
    </tr>
</table>
</div>
<script type="text/javascript">
     $(document).ready(function () {
         $("#btnSend").click(function () {         
             $.postJSONnoQS('<%=Url.Action("SendMessage") %>', { senduserid: <%=Model.UserID %>, receiveuserid: $("#receive").attr("x:sendid"),title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
               alert("Success");
             }); 
         });
         $("#btnSave").click(function () {  
            alert($('#MessageID').val())
             if($('#MessageID').val() !="0")
             {    
                 $.postJSONnoQS('<%=Url.Action("EditMessage") %>', { messageid: <%=Model.MessageID %>,title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                   alert("Success");
                   window.location.reload();
                 });              
             }
             else{       
                 $.postJSONnoQS('<%=Url.Action("SaveMessage") %>', { senduserid: <%=Model.UserID %>, title:$("#messagetitle").val(), content:$("#messagecontent").val()}, function (data) {
                   alert("Success");
                   window.location.reload();
                 }); 
             }
         });
         $(".selectuser").live("click",function(){
             $("#receive").val($(this).attr("x:username"));
             $("#receive").attr("x:sendid",$(this).attr("x:userid"));
         });

         $("#receive").click(function(){
            $("#taguserlist").show();
            $.getJSON('_TagUserList', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                $('#taguserlist').empty().html(data.Html);   
            });      
         });
     });
</script>
