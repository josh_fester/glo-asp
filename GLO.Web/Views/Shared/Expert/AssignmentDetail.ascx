﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_ExpertProject>" %>
	
<table>
        <tr>
            <td width="100"><b>Client</b></td>
            <td class="font"><%=Model.ClientName %></td>
        </tr>
        <tr>
            <td><b>Project Name</b></td>
            <td class="font"><%=Model.ProjectName %></td>
        </tr
        <tr>
            <td><b>Project Scope</b></td>
            <td class="font"><%=Model.Assignment%></td>
        </tr>
            <tr>
            <td><b>Start Date</b></td>
            <td class="font"><%=String.Format("{0:yyyy-MM-dd}",Model.StartDate)%></td>
        </tr>
            <tr>
            <td><b>End Date</b></td>
            <td class="font"><%=String.Format("{0:yyyy-MM-dd}",Model.EndDate) %></td>
        </tr>
            <tr>
            <td><b>Fee Charged</b></td>
            <td class="font"><%=Model.Charge %></td>
        </tr>
        <tr>
            <td><b>Status</b></td>
            <td class="font"><%=Model.Status %></td>
        </tr>
            <tr>
            <td><b>Comments</b></td>
            <td class="font"><%=Model.Comments.Replace("\n","<br/>") %></td>
        </tr>
</table>
<div class="me_apply"><a href="javascript:void(0)" id="editAssignment"><img src="/Styles/images/icon_edit.gif" /></a></div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#editAssignment").click(function () {
            $.getJSON('_AssignmentEdit', { "projectid": <%=Model.ProjectID %>,"expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>