﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HRExpert>" %>
<% int expertid = Model.UserID; %>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
    <div class="lightbox_note">
    <br /><br /><br /><br /><br /><br />
       Add here a list of clients with e.g. the scope of assignment or results achieved:<br />
E.g. <br />
GLO – Mindfulness workshop senior executives
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
       Add here a list of qualifications and achievements:<br />
E.g. <br />
Coach Certification (China Coaching Academy) - 2010

       
            <div class="message_arrow6"></div>
            <div class="message_arrow7"></div>
    </div>
    <div class="lightbox_box2">
        <div class="nav_expert_edit nav_expert_edit_credential">
            My Credentials</div>
        <div class="box_expert_edit">
            <div class="edit_container">
                <div class="client_title">
                    Client List</div>
                <div class="client_content">
                    <div id="editor1">
                    </div>
                    <script type="text/javascript">
                            var config1 = {
                                  height: 130,
                                 toolbar: 'Basic1'
                             };
                             var data1 ='';
                             <% if(!string.IsNullOrEmpty(Model.Credential)){ %>    
                             data1 ='<%=Model.Credential.Replace("\n","").Replace("\r","")%>';
                             <%}%>
                             var editor1 = CKEDITOR.appendTo('editor1', config1, data1);                         
                    </script>
                </div>
                <div class="qualify_title">
                    Professional Qualifications</div>
                <div class="qualify_content">
                    <div id="editor2">
                    </div>
                    <script type="text/javascript">
                            var config2 = {
                                  height: 140,
                                 toolbar: 'Basic1'
                             };
                             var data2 ='';
                             <% if(!string.IsNullOrEmpty(Model.Qualification)){ %>    
                             data2 ='<%=Model.Qualification.Replace("\n","").Replace("\r","")%>';
                             <%} %>
                             var editor2 = CKEDITOR.appendTo('editor2', config2, data2);                         
                    </script>
                </div>
            </div>
        </div>
        <div class="me_apply">
            <a href="javascript:void(0)" id="btnCredential">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close2">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () { 
              $("#btnCredential").click(function () {
                     var credential1 = editor1.getData();
                     var credential2 = editor2.getData();
                     var expertId='<%=expertid %>';
                     $.postJSONnoQS('<%=Url.Action("UpdateCredential") %>', { "expertid": expertId, "credential": escape(credential1), "qualification": escape(credential2) }, function (data) {
                            
//                                   <% if(string.IsNullOrEmpty(Model.Credential)){ %> 
//                                  var url = '';
//                                      url = '<%=Url.Action("RegisterSuccess") %>' + "?expertid=<%=expertid %>";                           
//                                  window.location.href = url;
//                                   <%} else{%>
//                                   window.location.reload();
//                                   <%} %>
//                                  return false;
                             if(data=="1" || data=="2")
                             {
                                 window.location.href = "/Expert/MyValue?expertid=" + expertId;
                             }
                             else if(data=="4")
                             {
                                 window.location.href="/Expert/MyCompetence?expertid="+expertId;
                             }
                             else if (data == "5") {
                                 window.location.href = "/Expert/RegisterSuccess?expertid=" + expertId;
                             }
                             else
                             {
                                 window.location.reload();
                                 return false;
                             }
                        });

                 });
    });
</script>
