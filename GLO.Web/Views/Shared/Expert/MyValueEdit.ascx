﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HRExpert>" %>
<% int expertid = Model.UserID; %>
  <div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_note">
            In this field you can describe your activities and in particular, how you add value to your clients:
            <br /><br />
            E.g. As a certified coach with more than 15 years of executive coaching assignments, I have made a 
            difference for many Fortune 500 companies’ executives, applying a customized coaching program 
            outlined in the PPT below.
            
            <br /><br /><br /><br /><br /><br /><br />
            You can upload a PPT/PDF file to show more information.
            <br /><br /><br /><br /><br /><br /><br />
            You can use these fields to upload photos that visualize your activities.

            <div class="message_arrow1"></div>
            <div class="message_arrow2"></div>
            <div class="message_arrow3"></div>
        </div>
        <div class="lightbox_box2">
       
        	<div class="nav_expert_edit nav_expert_edit_value">How I Add Value</div>
            <div class="box_expert_edit">
            	<div class="edit_container">
                	<div class="ckeditor_wrap">
                    <div id="editor1"></div>
                     <script type="text/javascript">
                         var config = {
                             height: 160,
                             toolbar: 'Basic1'
                         };
                         var data = '';
                          <% if(!string.IsNullOrEmpty(Model.MyValue)){ %>   
                         data ='<%=Model.MyValue.Replace("\n","").Replace("\r","")%>';
                         <%} %>
                                          
                         var editor = CKEDITOR.appendTo('editor1', config, data);                           	                                                                    
                  </script>
                    
                    </div>
                    <div class="upload_boxer">
                        <input type="button" id="ppt_upload" value="Upload file" />
                        <span id="pptName"><%=Model.PPTFileName%></span>
                        <a href="javascript:void(0);" style="margin-top:0;" title="delete" id="<%=expertid %>" onclick="DeletePPT(this)"><img src="/Styles/images/icon_delete1.gif" /></a>
                    </div>
                	<div class="edit_filearea" >
                        <div id="sortable">
                            <div class="filearea_item" id="imageLeft" >                        
                                <div class="filearea_item_edit">
                                    <input type="button" id="btnLeftUpload" class="btngray" value="Select Photo" />
                                    <input type="button" id="btnLeftDelete" class="btngray" value="Delete Photo"/><br />
                                    <span id="txtLeftInfo">You can use these fields to upload photos<br /> that visualize your activities.</span>
                                </div>
                                <img id="ImageLeftID" src ="/images/<%=!string.IsNullOrEmpty(Model.ValueImageLeft)?Model.ValueImageLeft:"icon_blank.gif"%>" x:name="<%=!string.IsNullOrEmpty(Model.ValueImageLeft)?Model.ValueImageLeft:""%>" />
                            </div>
                            <div class="filearea_item" id="imageRight" >                        
                                <div class="filearea_item_edit">
                                    <input type="button" id="btnRightUpload" class="btngray" value="Select Photo" />
                                    <input type="button" id="btnRightDelete" class="btngray" value="Delete Photo"/><br />
                                    <span id="txtRightInfo">You can use these fields to upload photos<br /> that visualize your activities.</span>
                                </div>                     
                                <img id="ImageRightID" src ="/images/<%=!string.IsNullOrEmpty(Model.VlaueInageRight)?Model.VlaueInageRight:"icon_blank.gif"%>" x:name="<%=!string.IsNullOrEmpty(Model.VlaueInageRight)?Model.VlaueInageRight:""%>" />
                            </div>
                        </div>                     
                    </div>
                </div>
            </div>
            <div class="me_apply"><a href="javascript:void(0)" id="btnValue"><img src="/Styles/images/icon_apply.gif"></a></div> 
            <div class="me_close2"><a onclick="return CloseLightBox()" href="#"><img src="/Styles/images/icon_close.gif"></a></div>     
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnValue").click(function () {
                var myvalue = editor.getData();
                var leftimage = $("#ImageLeftID").attr("x:name");
                var rightimage = $("#ImageRightID").attr("x:name");
                var expertId = '<%=expertid %>';
                var pptFile = $("#pptName").html();
                $.postJSONnoQS('<%=Url.Action("UpdateMyValue") %>', { "expertid": expertId, "myvalue": escape(myvalue), "imageleft": leftimage, "imageright": rightimage,"pptFile":pptFile }, function (data) {

                    if (data == "1") {
                        $("#editLeftInfo").click();
                    }
                    else if (data == "3") {
                        window.location.href = "/Expert/MyCredential?expertid=" + expertId;
                    }
                    else if (data == "4") {
                        window.location.href = "/Expert/MyCompetence?expertid=" + expertId;
                    }
                    else if (data == "5") {
                        window.location.href = "/Expert/RegisterSuccess?expertid=" + expertId;
                    }
                    else {
                        window.location.reload();
                        return false;
                    }
                });                
            });

            $('#ppt_upload').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Upload PPT/PDF file',
                'fileTypeExts': '*.ppt;*.pptx;*.pdf',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/UploadPPT?id='+<%=expertid %>,
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    if (data.Success) {
                        $('#pptName').html(data.SaveName);
                        $('.upload_boxer').find('a').css("display","");
                    }
                    else {
                        alertbox(data.Message)
                    }
                }
            });

            if($('#pptName').html()=="")
            {
                $('.upload_boxer').find('a').css("display","none");
            }
            else
            {
                $('.upload_boxer').find('a').css("display","");
            }
            
            $('#btnLeftUpload').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select Photo',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/FileUpload/Upload?width=370&height=260',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);                        

                    $("#ImageLeftID").removeAttr("src");
                    $("#ImageLeftID").removeAttr("x:name");
                    $("#ImageLeftID").attr("src",data.Urlpath);
                    $("#ImageLeftID").attr("x:name", data.SaveName); 

                    $(".uploadify-queue").css('display','none');
                    $('#txtLeftInfo').css('display','none');
                    $('#btnLeftUpload').css('display','none');
                    $('#btnLeftDelete').css('display','');
                }
            });
            $('#btnLeftDelete').click(function(){
                $("#ImageLeftID").removeAttr("src");
                $("#ImageLeftID").removeAttr("x:name");
                $("#ImageLeftID").attr("src","/Images/icon_blank.gif")
                $("#ImageLeftID").attr("x:name", ""); 
                                
                $('#txtLeftInfo').css('display','');
                $('#btnLeftUpload').css('display','');
                $('#btnLeftDelete').css('display','none');
            });

            var leftImage='<%=Model.ValueImageLeft %>';
            if (leftImage != "" && leftImage.indexOf("icon_blank.gif") == -1)
            {
                $('#txtLeftInfo').css('display','none');
                $('#btnLeftUpload').css('display','none');
                $('#btnLeftDelete').css('display','');
            }
            else{
                $('#txtLeftInfo').css('display','');
                $('#btnLeftUpload').css('display','');
                $('#btnLeftDelete').css('display','none');
            }
            
            $('#btnRightUpload').uploadify({
                'height': 25,
                'width': 90,
                'auto': true,
                'multi': false,
                'buttonText': 'Select Photo',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/FileUpload/Upload?width=370&height=260',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);

                    $("#ImageRightID").removeAttr("src");
                    $("#ImageRightID").removeAttr("x:name");
                    $("#ImageRightID").attr("src",data.Urlpath);
                    $("#ImageRightID").attr("x:name", data.SaveName); 

                    $(".uploadify-queue").css('display','none');
                    $('#txtRightInfo').css('display','none');
                    $('#btnRightUpload').css('display','none');
                    $('#btnRightDelete').css('display','');
                }
            });
            $('#btnRightDelete').click(function(){
                $("#ImageRightID").removeAttr("src");
                $("#ImageRightID").removeAttr("x:name");
                $("#ImageRightID").attr("src","/Images/icon_blank.gif");
                $("#ImageRightID").attr("x:name", ""); 
                
                $('#txtRightInfo').css('display','');
                $('#btnRightUpload').css('display','');
                $('#btnRightDelete').css('display','none');
            });

            var rightImage='<%=Model.VlaueInageRight %>';
            if (rightImage != "" && rightImage.indexOf("icon_blank.gif") == -1)
            {
                $('#txtRightInfo').css('display','none');
                $('#btnRightUpload').css('display','none');
                $('#btnRightDelete').css('display','');
            }
            else{
                $('#txtRightInfo').css('display','');
                $('#btnRightUpload').css('display','');
                $('#btnRightDelete').css('display','none');
            }
        });
        
        function DeletePPT(obj) {
            confirmbox("Do you want to delete the file？", function(){
                $('#pptName').html("");
                $('.upload_boxer').find('a').css("display","none");
            });
        }
  </script>
