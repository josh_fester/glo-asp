﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_HRExpert>" %>
 <script src="<%=Url.Content("~/Scripts/jquery-ui-1.8.11.min.js") %>" type="text/javascript"></script>
<div class="lightbox_mask">
</div>
<div class="lightbox_manage">
        <div class="lightbox_note2">
            <div>
                Select a private or public profile. <br /><br />
                When public, your profile will randomly show in a sample list of ‘good experts’ accessed by any visitors to the site. <br /><br />
                If private, only member users can see your profile.<br /><br />                
                If blue your profile will be public, if grey it is set on private.

            </div>

            <div class="message_arrow4"></div>
            <div class="me_apply">
                <a id="btnSubmit2" href="javascript:void(0);">
                    <img src="/Styles/images/icon_apply.gif"></a></div>
        </div>
    <div class="lightbox_box1">
        <% var classname = "box_user_private";
           var ispublish = "0";
           if (Model.GLO_Users.IsPublish)
           {
               classname = "box_user_private_on";
               ispublish = "1";

           } %>
        <div class="<%=classname%>" x:ispublish="<%=ispublish %>" id="publish_or_private"
            onclick="ResetPrivate(this);" title="Use this button to make your profile visible or private">
        </div>
        <div class="box_user_photo2">
            <img id="imageID" src="/Images/<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"user_none.gif" %>" x:name="<%=!string.IsNullOrEmpty(Model.LogoUrl)?Model.LogoUrl:"" %>"/>
            
            <div class="photo_item_edit">
                <input type="button" id="file_upload" class="btngray" value="Select Photo"/>
                <input type="button" id="file_delete" class="btngray" value="Delete Photo"/>
            </div>
        </div>
        <div class="box_user_detail">
            <input type="text" class="txtbox_user1" id="NickName" value="<%=!String.IsNullOrEmpty(Model.NickName)?Model.NickName:"Your name"%>"
                onfocus="if (this.value=='Your name')this.value=''" onblur="if (this.value=='')this.value='Your name'" />
            <input type="text" class="txtbox_user2" id="Title" value="<%=!String.IsNullOrEmpty(Model.Title)?Model.Title:"Job title" %>"
                onfocus="if (this.value=='Job title')this.value=''" onblur="if (this.value=='')this.value='Job title'" />
           
           
            <div class="multiple_user">
                <div class="multiple_box">
                    <%: Html.Hidden("LocationID", Model.GLO_Users.GLO_UserLocation.Count > 0 ? Model.GLO_Users.GLO_UserLocation.FirstOrDefault().LID.ToString() : "0", new { @class = "multiple_locationid" })%>
                    <input class="multiple_value" id="Location" type="text" readonly="readonly" value="<%=!String.IsNullOrEmpty(Model.Location)?Model.Location:"Location" %>" />
                    <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                        <div class="multiple_wrapper multiple_wrapper_location"></div> 
                </div>                
            </div>
            <div class="multiple_user">
                <div class="multiple_box">
                    <%: Html.Hidden("IndustryID", Model.GLO_Users.GLO_UserIndustry.Count > 0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().IndustryID.ToString() : "0", new { @class = "multiple_id" })%>
                    <input class="multiple_value" id="Industry" type="text" readonly="readonly" value="<%= Model.GLO_Users.GLO_UserIndustry.Count>0 ? Model.GLO_Users.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName : "Industry" %>" />
                    <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                        <div class="multiple_wrapper multiple_wrapper_industry"></div> 
                </div>                
            </div>

            <textarea class="txtbox_user3" id="MyVision" onfocus="if (this.value=='describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal')this.value=''"
                onblur="if (this.value=='')this.value='describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal'"><%=!String.IsNullOrEmpty(Model.MyVision)?Model.MyVision : "describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal"%></textarea>
        </div>
        <div id="selectvalue" class="box_user_value">
            <div class="box_user_value_title">What values in life and career are important to you:</div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="ExpertValue1" x:value="<%=Model.ExpertValue1%>" type="text" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.ExpertValue1) ? "Select" : Model.ExpertValue1%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x => x.ExpertValueID))
                               {
                            %>
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="ExpertValue2" type="text" x:value="<%=Model.ExpertValue2%>" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.ExpertValue2) ? "Select" : Model.ExpertValue2%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x => x.ExpertValueID))
                               {
                            %>
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>"  class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <div class="select_box select_box_value">
                    <input class="select_value font" id="ExpertValue3" type="text" x:value="<%=Model.ExpertValue3%>" readonly="readonly" value="<%= string.IsNullOrEmpty(Model.ExpertValue3) ? "Select" : Model.ExpertValue3%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option font">
                        <ul>
                            <li>Integrity</li>
                            <li>Respect</li>
                            <li>Excellence</li>
                            <li>Commitment</li>
                            <li>Family-work balance</li>
                            <li>Team work</li>
                            <li>Family</li>
                            <% foreach (var item in Model.GLO_Users.GLO_ExpertRelational.OrderBy(x=>x.ExpertValueID))
                               {
                            %>
                            <li x:relationalID="<%=item.RelationalID %>"><%=item.GLO_ExpertValueDic.ExpertValue %><img title="delete" id="<%=item.RelationalID %>" class="value_delete" src="/Styles/images/icon_cancel.gif" /></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
        </div>
        <div class="user_value_add">
            Custom:<input type="text" value="" class="txt_itemadd font" id="txtExpertValue" maxlength="30" /><input type="hidden" id="hidExpertValueID" /><input type="button" id="btnAddExpertValue" value="Add" class="bluebtn" />
        </div>
        
        <div class="me_apply">
            <a id="btnSubmit" href="javascript:void(0);">
                <img src="/Styles/images/icon_apply.gif"></a></div>
        <div class="me_close">
            <a onclick="return CloseLightBox()" href="#">
                <img src="/Styles/images/icon_close.gif"></a></div>
    </div>
</div>
<script type="text/javascript">
        var nameArry = [];
        $(document).ready(function () {
            $("#NickName").blur(function(){
                if (!isEmpty($(this).val())|| $(this).val()!="Your name") $(this).removeClass("input_error");
            });
            $("#Title").blur(function(){
                if (!isEmpty($(this).val())|| $(this).val()!="Job title") $(this).removeClass("input_error");
            });
            
            $.getJSON('/Home/_GetLocationList', null, function (data) {
                        {
                            $('.multiple_wrapper_location').html(data.Html);
                        }
             });
            $.getJSON('/Home/_GetIndustryList', null, function (data) {
                        {
                            $('.multiple_wrapper_industry').html(data.Html);
                        }
             });

            SetDropdown();
            GetData();
            $("#txtExpertValue").autocomplete({
                minLength: 1,
                autoFocuus: true,
                source: function (req, res) {
                    var name = req.term,
                                result = [];
                    if ($.trim(req.term) != "") {
                        var searchResult = $.map(
                                    (name ? $.grep(nameArry, function (value) {
                                        return value.name.toString().toLocaleLowerCase().indexOf(name) >= 0;
                                    }) : nameArry),
                                    function (value) {
                                        return { label: value.name, tc: value.id };
                                    });
                        result = result.concat($.makeArray(searchResult));
                    }
                    res(result);
                },
                select: function (event, ui) {
                    $('#hidExpertValueID').val(ui.item.tc);
                }
            });
            $("#btnAddExpertValue").click(function () {
                var value=$('#txtExpertValue').val().trim();
                if(value=="")
                {
                    return false;
                }
                $.postJSON('<%=Url.Action("AddExpertValue") %>', { value:value}, function (data) {
                    var html= "<li x:relationalID=\""+ data +"\">" + value + "<img title=\"delete\" id=\"" + data + "\" class=\"value_delete\" src=\"/Styles/images/icon_cancel.gif\" /></li>";
                    if(data!=""){
                    var isExist=false;
                        $(".select_option li").each(function(){
                            if($(this).attr("x:relationalID") == data)
                            {
                                isExist= true;
                            }
                        });
                        if(!isExist)
                        {
                            $('.select_option').children("ul").append(html);
                            SetDropdown();
                        }
                        $('#txtExpertValue').val("");
                        alertbox("Your customized value has been successfully added to the list.");                        
                    }
                    else{
                        alertbox("Add failed.");
                    }
                });
            });
            
              $("#btnSubmit2").click(function () {
                $('.lightbox_note2').hide();
              });

              $("#btnSubmit").click(function () {
                   if (ValidateInfo() == false)
                    return;

                   var version = $('#MyVision').val()=="describe here what is your ultimate objective in life. This field simply defines what you really stand for, how you want to make a difference, what is your ultimate goal"?"":$('#MyVision').val();
                   
                   var expertId='<%=Model.UserID %>';
                   var expertdata ={
                         'UserID':<%=Model.UserID %>,
                         'NickName':$('#NickName').val(),
                         'LogoUrl':$("#imageID").attr("x:name"),
                         'Location':$("#Location").val(),
                         'Title':$('#Title').val(),
                         'MyVision':version,
                         'ExpertValue1':$("#ExpertValue1").val() == "Select" ? "" : $("#ExpertValue1").val(),
                         'ExpertValue2':$("#ExpertValue2").val() == "Select" ? "" : $("#ExpertValue2").val(),
                         'ExpertValue3':$("#ExpertValue3").val() == "Select" ? "" : $("#ExpertValue3").val()
                   };
                   if ($('#Industry').val() == "" || $('#Industry').val() == "Industry") {
                        $('#IndustryID').val("0");
                    }
                     $.postJSON('<%=Url.Action("ExpertUpdate") %>', { expert:$.toJSON(expertdata),ispublish:$("#publish_or_private").attr("x:ispublish"),LocationID:$("#LocationID").val(),industryID:$('#IndustryID').val() }, function (data) {
                         if(data=="2")
                         {
                             window.location.href="/Expert/MyValue?expertid="+expertId;
                         }
                         else if(data=="3")
                         {
                             window.location.href="/Expert/MyCredential?expertid="+expertId;
                         }
                         else if(data=="4")
                         {
                             window.location.href="/Expert/MyCompetence?expertid="+expertId;
                         }
                         else if(data=="5")
                         {
                             window.location.href="/Expert/RegisterSuccess?expertid="+expertId;
                         }
                         else
                         {
                             window.location.href="/Expert/MyValue?expertid="+expertId;
                         }
                     });
                 });
                    $('#file_upload').uploadify({
                        'height': 25,
                        'width': 90,
                        'auto': true,
                        'multi': false,
                        'buttonText': 'Select Photo',
                        'fileTypeExts': '*.gif; *.jpg; *.png',
                        'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                        'uploader': '/FileUpload/Upload?width=186&height=186',
                        'onUploadSuccess': function (file, data, response) {
                            eval("data=" + data);

                            $("#imageID").removeAttr("src");
                            $("#imageID").removeAttr("x:name");
                            $("#imageID").attr("src",data.Urlpath);
                            $("#imageID").attr("x:name", data.SaveName); 

                            $(".uploadify-queue").css('display','none');
                            $('#file_upload').css('display','none');
                            $('#file_delete').css('display','');
                        }
                    });
                 
                 $('#file_delete').click(function(){
                    $("#imageID").removeAttr("src");
                    $("#imageID").removeAttr("x:name");
                    $("#imageID").attr("src","/Images/user_none.gif");
                    $("#imageID").attr("x:name", ""); 

                    $('#file_upload').css('display','');
                    $('#file_delete').css('display','none');
                 });
                 
                var logoUrl='<%=Model.LogoUrl %>';
                if (logoUrl != "" && logoUrl.indexOf("user_none.gif") == -1)
                {
                    $('#file_upload').css('display','none');
                    $('#file_delete').css('display','');
                }
                else{
                    $('#file_upload').css('display','');
                    $('#file_delete').css('display','none');
                }
 
        });
        function GetData() {
            $.post("/Expert/GetExpertValueList", null, function (data) {
                $.each(data, function () {
                    var nameitem = { name: this.Text, id: this.Value };
                    nameArry.push(nameitem);            
            });
        });
        }
        function ResetPrivate(obj) {
            if (obj.className == 'box_user_private_on') {
                obj.className = 'box_user_private';
                $(obj).attr("x:ispublish","0");
            }
            else {
                obj.className = 'box_user_private_on';
                 $(obj).attr("x:ispublish","1");
            }
        }
        function ValidateInfo() {
        var error = new Array();
        if (isEmpty($("#NickName").val())|| $("#NickName").val()=="Your name") {
            $("#NickName").addClass("input_error");
            error.push("error")
        }
        else {
            $("#NickName").removeClass("input_error");
        }

        if (isEmpty($("#Title").val()) || $("#Title").val()=="Job title") {
            $("#Title").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Title").removeClass("input_error");
        }

        if (isEmpty($("#Location").val()) || $("#Location").val()=="Location") {
            $("#Location").parent().addClass("select_error");
            error.push("error")
        }
        else {
            $("#Location").parent().removeClass("select_error");
        }


        if (error.length > 0)
            return false;
        else
            return true;
    }

    function SetDropdown()
    {    
       /*open dropdown*/
            $(".select_box").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                $(this).find(".select_option").toggle();
            });
            /*close dropdown*/
            $(document).click(function () {
                $('.select_option').hide();
            });
            /*set value*/
            $(".select_option li").click(function (event) {
                event.stopPropagation();
                $(".select_option").hide();
                
                var oldText=$(this).parent().parent().parent().find(".select_value").val();
                var text = $(this).text().replace(/<img([^>]*)>/gi, "");
                $(this).parent().parent().parent().find(".select_value").val(text);
                $(this).parent().parent().parent().removeClass('select_error');

                $(".select_option li").each(function(){
                    if($(this).text()==oldText)
                    {
                        $(this).css("display","")
                    }
                    if($(this).text()==text)
                    {
                        $(this).css("display","none")
                    }
                });   
            });                    
            /*delete value*/
            $(".value_delete").click(function(event) {
                event.stopPropagation();
                $(".select_option").hide();
                
                var text = "Value";
                var delValue = $(this).attr("x:value");
                var selValue = $(this).parent().parent().parent().parent().find(".select_value").val();
                if(delValue == selValue)
                $(this).parent().parent().parent().parent().find(".select_value").val(text);

                var id = $(this).attr("id");
                $.getJSON('<%=Url.Action("DeleteValue","Expert") %>', { "id": id, random: Math.random() }, function (data) {
                    if (data) { 
                        $('.select_option li').each(function(){
                            if($(this).attr("x:relationalID") == id)
                            {
                                $(this).remove();
                            }
                        });
                    }
                });
            });

    }
</script>
