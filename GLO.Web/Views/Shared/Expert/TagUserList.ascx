﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Users>" %>
 <table>
 <%foreach(var taguser in Model.GLO_TaggedUser)
   { %>
        <tr>
            <td><input type="radio" name="name" x:userid="<%=taguser.TagUserID %>" x:username="<%=taguser.GLO_Users1.UserName %>" class="selectuser" /></td><td><%=taguser.GLO_Users1.UserName %></td>
        </tr>
<%} %>
</table>
<div class="icon_close"><a id="btnCloseReceive" href="javascript:void()"><img src="/Styles/images/icon_delete2.gif" /></a></div>

 <script type="text/javascript">
     $(document).ready(function () {
         $("#btnCloseReceive").click(function () {
             $("#taguserlist").hide();
         });

         $(".selectuser").click(function () {
             $("#taguserlist").hide();
         });
         
     });
</script>