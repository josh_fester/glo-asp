﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_MessageSend>>" %>
<% foreach (var message in Model)
   {  %>
<div id="<%=message.MessageID %>">
    <div class="message_line">
        <div class="message_usericon">
            <a href="javascript:void(0)">
                <img src="/images/<%=message.GLO_Users.Icon %>" /></a>
        </div>
        <div class="message_action">
            <table>
                <tr>
                    <td>
                        <b>
                            <%=message.GLO_Users.NickName%></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="messagedetail" x:messageid="<%=message.GLO_Message.MessageID %>">
                            <%=message.GLO_Message.MessageTitle%></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%if (ViewBag.Status == "1")
                          { %>
                        <a href="javascript:void(0)" class="messageedit" x:messageid="<%=message.MessageID %>" x:status="forward">Forward</a> | 
                        <%} %>
                        <%if (ViewBag.Status == "2")
                          { %>
                        <a href="javascript:void(0)" class="messageedit" x:messageid="<%=message.MessageID %>" x:status="send">Send</a> | 
                        <%} %>
                        <a href="javascript:void(0)" class="messagedelete" x:messageid="<%=message.MessageID %>">Delete</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".messagedetail").click(function () {
            $.getJSON('_SendMessageDetail', { "messageid": $(this).attr("x:messageid"), random: Math.random() }, function (data) {
                $('.message_content').empty().html(data.Html);
            });
        });
        $(".messageedit").click(function () {
            $.getJSON('_SendMessageEdit', { "messageid": $(this).attr("x:messageid"), "status": $(this).attr("x:status") }, function (data) {
                $('.message_content').empty().html(data.Html);
            });
        });
        $(".messagedelete,.messagearchive").click(function () {
            var messageid=$(this).attr("x:messageid");
            if (confirm("Are you sure delete this message?")) {
                $.postJSON('<%=Url.Action("_SendMessageDelete") %>', { messageid: messageid }, function (data) {
                    if (data) {
                        $("#" + messageid).empty();
                    }
                });
            }
        });
    });
</script>
