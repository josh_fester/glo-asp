﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.Competence>" %>
<div class="competence_wrap">
<%
    var education = Model.EducationList;
    var careersummarylist = Model.CarreerSummaryList;
    var contribution = Model.ContributionList;
     
   var endyear = Model.EndYear;// endate.Value.Year;
   var year = Model.StartYear;// startdate.Value.Year;
   var yearspan = endyear - year+1;
   
%>
    <div class="competence_content">
    <%foreach (var cont in contribution)
      {
          var spanyear = cont.EndDate.Value.Year - cont.StartDate.Value.Year;
          var width = (spanyear+1) * 75;
          var left = (cont.StartDate.Value.Year - year) * 75;
          var top = 50;
          var cut = false;
          
          if (left >= 750 && left<1500)
          {
              left = left - 750;
              top = 230;
          }
          if (left >= 1500)
          {
              left = left - 1500;
              top = 410;
          }
          if(left<750 && left + width>750)
          {
              cut = true;
            //  top = 270;              
          }
          
          if (left < 1500 && left + width > 1500)
          {
              cut = true;
              top = 230;
          }
                 
    %>
 
    <%if (cut == true)
      { 
        var widthfirstrow = 750 - left;
          
     %>
      <div class="nonprofit" style="width:<%=widthfirstrow %>px; left:<%=left%>px; top:<%=top%>px;">
        <div class="timeline_arrow2"></div>
        <div class="timeline_view" style=" display:none; ">
        <%=cont.Orgnization %>
        <br />
        <%=cont.JobTitle %>
        <br />
        <%= string.Format("{0:yyyy-MM-dd}",cont.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", cont.EndDate)%>
        <br />
        <br />
        Jobs duties/ Lessons learned
        <br />
        <%=cont.LessonLearnd %>
        <br />
        <br />
        Competence applied
        <br />
        <%=cont.Competencies %>
        <br />
        <br />
        Success achieved
        <br />
        <%=cont.SuccessAchievement%>
        <br />

        <a href="javascript:void(0)" class="delete_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Delete</a>
         <a href="javascript:void(0)" class="edit_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Edit</a>
        </div>
    </div>
       <% width = width - widthfirstrow;
       left = 0;
       top = top+180;
      } %>

    <div class="nonprofit" style="width:<%=width %>px; left:<%=left%>px; top:<%=top%>px;">
        <div class="timeline_arrow2"></div>
        <div class="timeline_view" style=" display:none; ">
        <%=cont.Orgnization %>
        <br />
        <%=cont.JobTitle %>
        <br />
        <%= string.Format("{0:yyyy-MM-dd}",cont.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", cont.EndDate)%>
        <br />
        <br />
        Jobs duties/ Lessons learned
        <br />
        <%=cont.LessonLearnd %>
        <br />
        <br />
        Competence applied
        <br />
        <%=cont.Competencies %>
        <br />
        <br />
        Success achieved
        <br />
        <%=cont.SuccessAchievement %>
        <br />
        
        <a href="javascript:void(0)" class="delete_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Delete</a>
        <a href="javascript:void(0)" class="edit_record" x:type="Contribution" x:recordid="<%=cont.ContributionID %>">Edit</a>
        </div>
    </div>

    <%} %>


    <%foreach (var edu in education)
      {
          var spanyear = edu.EndDate.Value.Year - edu.StartDate.Value.Year;
          var width = (spanyear+1) * 75;
          var left = (edu.StartDate.Value.Year - year) * 75;
          var top = 70;
          var cut = false;
          
          if (left >= 750 && left<1500)
          {
              left = left - 750;
              top = 250;
          }
          if (left >= 1500)
          {
              left = left - 1500;
              top = 430;
          }
          if(left<750 && left + width>750)
          {
              cut = true;
            //  top = 270;              
          }
          
          if (left < 1500 && left + width > 1500)
          {
              cut = true;
              top = 250;
          }
                 
    %>
 
    <%if (cut == true)
      { 
        var widthfirstrow = 750 - left;
          
     %>
      <div class="tralning" style="width:<%=widthfirstrow %>px; left:<%=left%>px; top:<%=top%>px;">
          <div class="timeline_arrow2"></div> 
          <div class="timeline_view" style=" display:none; ">
      	    <%=edu.School%>
                            <br />
                    	  
                            <br />
                    	    <%= string.Format("{0:yyyy-MM-dd}",edu.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", edu.EndDate)%>
                            <br />
                            <br />
                            Degree
                            <br />
                              <%= edu.Degree %>
                            <br />
                            <br />
                            Lessons learned
                            <br />
                            <%= edu.Languages %>
                            <br />
                            <br />
                            Success achieved
                            <br />
                            <%= edu.Certification %>
                            <br />
                            <br />
                            <a href="javascript:void(0)" class="delete_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Delete</a>
                             <a href="javascript:void(0)" class="edit_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Edit</a>

          </div>
      </div>
      <% width = width - widthfirstrow;
       left = 0;
       top = top+180;
      } %>
      <div class="tralning" style="width:<%=width %>px; left:<%=left%>px; top:<%=top%>px;">
          <div class="timeline_arrow2"></div> 
          <div class="timeline_view" style=" display:none; ">
      	    <%=edu.School%>
                            <br />
                    	  
                            <br />
                    	    <%= string.Format("{0:yyyy-MM-dd}",edu.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", edu.EndDate)%>
                            <br />
                            <br />
                            Degree
                            <br />
                              <%= edu.Degree %>
                            <br />
                            <br />
                            Lessons learned
                            <br />
                            <%= edu.Languages %>
                            <br />
                            <br />
                            Success achieved 
                            <br />
                            <%= edu.Certification %>
                            <br />
                            <br />
                            <a href="javascript:void(0)" class="delete_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Delete</a>
                            <a href="javascript:void(0)" class="edit_record" x:type="Education" x:recordid="<%=edu.EducationID %>">Edit</a>

          </div>
      </div>
    <%} %>




    <%foreach (var careersummary in careersummarylist)
      {
          var spanyear = careersummary.EndDate.Value.Year - careersummary.StartDate.Value.Year;
          var width = (spanyear+1) * 75;
          var left = (careersummary.StartDate.Value.Year - year) * 75;
          var top = 90;
          var cut = false;
          
          if (left >= 750 && left<1500)
          {
              left = left - 750;
              top = 270;
          }
          if (left >= 1500)
          {
              left = left - 1500;
              top = 450;
          }
          if(left<750 && left + width>750)
          {
              cut = true;
            //  top = 270;              
          }
          
          if (left < 1500 && left + width > 1500)
          {
              cut = true;
              top = 270;
          }
                 
    %>
 
    <%if (cut == true)
      { 
        var widthfirstrow = 750 - left;
          
     %>
    <div class="employment" style="width:<%=widthfirstrow %>px; left:<%=left%>px; top:<%=top%>px;">
      <div class="emplyicon"></div>
         <div class="emplycontent">
                    <%=careersummary.Company%><br>
                    <%= careersummary.JobTitle%><br>
                    <%=string.Format("{0}-{1}", careersummary.StartDate.Value.Year, careersummary.EndDate.Value.Year)%> 
                     <div class="timeline_arrow2"></div>
                     <div class="timeline_view" style=" display:none; ">
                    	<%=careersummary.Company%>
                        <br />
                    	<%= careersummary.JobTitle%>
                        <br />
                    	<%= string.Format("{0:yyyy-MM-dd}", careersummary.StartDate)%> to <%= string.Format("{0:yyyy-MM-dd}", careersummary.EndDate)%>
                        <br />
                        <br />
                        Position description
                        <br />
                        <%= careersummary.LessonLearnd%>
                        
                        <br />
                        <br />
                        Competencies applied
                        <br />
                        <%= careersummary.Competencies%>
                        
                        <br />
                        <br />
                        Success achieved
                        <br />
                        <%= careersummary.SuccessAchievement%>
                        <br />

                         <a href="javascript:void(0)" class="delete_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Delete</a>
                         <a href="javascript:void(0)" class="edit_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Edit</a>
                    </div>                         
          </div>                    
      </div>
    <% width = width - widthfirstrow;
       left = 0;
       top = top+180;
      } %>

     <div class="employment" style="width:<%=width %>px; left:<%=left%>px; top:<%=top%>px;">
      <div class="emplyicon"></div>
         <div class="emplycontent">
                    <%=careersummary.Company %><br>
                    <%= careersummary.JobTitle %><br>
                    <%=string.Format("{0}-{1}",careersummary.StartDate.Value.Year,careersummary.EndDate.Value.Year) %> 
                     <div class="timeline_arrow2"></div>
                     <div class="timeline_view" style=" display:none; ">
                    	<%=careersummary.Company %>
                        <br />
                    	<%= careersummary.JobTitle %>
                        <br />
                    	<%= string.Format("{0:yyyy-MM-dd}",careersummary.StartDate) %> to <%= string.Format("{0:yyyy-MM-dd}", careersummary.EndDate)%>
                        <br />
                        <br />
                        Position description
                        <br />
                        <%= careersummary.LessonLearnd%>
                        
                        <br />
                        <br />
                        Competencies applied
                        <br />
                        <%= careersummary.Competencies%>
                        
                        <br />
                        <br />
                        Success achieved
                        <br />
                        <%= careersummary.SuccessAchievement%>
                        <br />
                         <a href="javascript:void(0)" class="delete_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Delete</a>
                         <a href="javascript:void(0)" class="edit_record" x:type="Employment" x:recordid="<%=careersummary.CareerID %>">Edit</a>
                      </div>                         
          </div>                    
      </div>

    <%} %>
      
                                                                                         

                     <%for (int i = 0; i < 30; i++)
                      { %>
                       <div class="timeline_box  <%=i%10==0?"box_first ":"" %>  <%=i<yearspan?"box_active":"" %>">
                        <div class="timeline_title"><%=year+i %></div>
                        <div class="timeline_icon"></div>
                        <div class="timeline_content"></div>                    	
                    </div>
                    <%} %>
                </div>

    <div class="competence_top5">
        <div>Top 5 competence</div>
        <div>            
            <div class="value_item_active">stategy</div>
            <div class="value_item_active">stategy</div>
            <div class="value_item_active">stategy</div>
            <div class="value_item_active">stategy</div>
            <div class="value_item_active">stategy</div>
        </div>
    </div>

    <div class="competence_explain">
        <div class="explain_line">
            <div class="explain_icon1"></div><div class="explain_title">Employment</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon2"></div><div class="explain_title">Non-Profit</div>
        </div>
        <div class="explain_line">
            <div class="explain_icon3"></div><div class="explain_title">Education</div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".nonprofit").hover(function () {
            $(this).css("z-index", "9");
            $(this).find(".timeline_view").show();
            $(this).find(".timeline_arrow2").show();
            $(this).find(".timeline_arrow2").css("left", "50%");
            var left = parseInt($(this).css('left'));
            if (left > 500) $(this).find(".timeline_view").css("left", (500 - left) + "px");
        }, function () {
            $(this).css("z-index", "8");
            $(this).find(".timeline_view").hide();
            $(this).find(".timeline_arrow2").hide();
        });

        $(".tralning").hover(function () {
            $(this).css("z-index", "9");
            $(this).find(".timeline_view").show();
            $(this).find(".timeline_arrow2").show();
            $(this).find(".timeline_arrow2").css("left", "50%");
            var left = parseInt($(this).css('left'));
            if (left > 500) $(this).find(".timeline_view").css("left", (500 - left) + "px");
        }, function () {
            $(this).css("z-index", "7");
            $(this).find(".timeline_view").hide();
            $(this).find(".timeline_arrow2").hide();
        });

        $(".emplycontent").hover(function () {
            $(this).parent().css("z-index", "9");
            $(this).find(".timeline_view").show();
            $(this).find(".timeline_arrow2").show();
            $(this).find(".timeline_arrow2").css("left", "50%");
            var left = parseInt($(this).parent().css('left'));
            if (left > 500) $(this).find(".timeline_view").css("left", (500 - left) + "px");

        }, function () {
            $(this).parent().css("z-index", "6");
            $(this).find(".timeline_view").hide();
            $(this).find(".timeline_arrow2").hide();
        });

        $('#editProfile').live('click', function () {
            $.getJSON('_MyCompetenceEdit', { "expertid": 105, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });

        $(".delete_record").click(function () {
            var type = $(this).attr("x:type");
            var recordid = $(this).attr("x:recordid");

            confirmbox("Are you sure delete this record?", function () {
                $.postJSON('<%=Url.Action("CompetenceDelete") %>', { recordid: recordid, type:type }, function (data) {
                    window.location.reload();
                    return false;
                });
            });
        });
        $(".edit_record").click(function () {
            var recordid = $(this).attr("x:recordid");
            var type = $(this).attr("x:type");
            //x:type="Contribution" x:recordid
            $.getJSON('_MyCompetenceUpdate', { "recordid": recordid, "type": type, random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>