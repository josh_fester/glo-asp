﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Recommendation>" %>
<% 
    var glouser = Model.GLO_Users;
    var recommendUser = Model.GLO_RecommendUsers;
    var username = Model.GLO_Users.NickName;
    int type = Model.GLO_Users.TypeID;
    var headName = Model.GLO_Users.NickName;
    List<GLO.Data.Models.GLO_Competencies> competenices = Model.GLO_Users.GLO_Competencies.Where(x => x.CompetencyType == 1 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
    List<GLO.Data.Models.GLO_Competencies> experience = Model.GLO_Users.GLO_Competencies.Where(x => x.CompetencyType == 3 && x.CompetencyContent != "").OrderBy(x => x.OrderID).ToList();
    List<GLO.Data.Models.GLO_Competencies> ortherCompetenices = Model.GLO_Users.GLO_Competencies.Where(x => x.CompetencyType == 2).OrderBy(x => x.CompetencyContent).ToList();
    List<GLO.Data.Models.GLO_Competencies> othervalues = Model.GLO_Users.GLO_Competencies.Where(x => x.CompetencyType == 4).OrderBy(x => x.CompetencyContent).ToList();
    
    
    var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
    if (recommendUser.TypeID == 1)
    {
        headName = "<a target=\"_blank\" href=\"/leader/index?leaderid=" + recommendUser.UserID + "\">" + Model.UserName + "</a>";
    }
    else if (recommendUser.TypeID == 2)
    {
        headName = "<a target=\"_blank\" href=\"/expert/myvalue?expertid=" + recommendUser.UserID + "\">" + Model.UserName + "</a>";
    }
    else if (recommendUser.TypeID == 3)
    {
        headName = "<a target=\"_blank\" href=\"/company/companyindex?companyid=" + recommendUser.UserID + "\">" + Model.UserName + "</a>";
    }
%>
<div class="r_info_name"><%=headName%></div>
<div class="r_info_item">
    How do I like <%=username%>'s <%=type==1?"vision":"work" %>?
</div>
<div class="r_info_content">
    <%for (int i = 0; i < 10; i++)
      {
          int count = int.Parse(Model.Vision);
          if (i < count)
          { %>
    <img src="/Styles/images/icon_great1.gif" />
    <%}
          else
          { %>
    <img src="/Styles/images/icon_great2.gif" />
    <%}
      } %>
</div>
<%if(glouser.TypeID==1){ %>
 <div class="r_info_item">My experience with <%=username%>'s values</div>
  <div class="r_info_content">
     <% 
      Model.ExperienceValue = string.IsNullOrEmpty(Model.ExperienceValue) ? ";" : Model.ExperienceValue;
        string[] experiencevalue = Model.ExperienceValue.Split(';');%>
    <% foreach (var item in experience)
       {
           var classname = "value_item";
           foreach (var competenceId in experiencevalue)
           {
               if (item.CompetencyID.ToString() == competenceId)
               {
                   classname = "value_item_active";
               }
           }
    %>
    <div class='<%=classname%>'>
        <%=item.CompetencyContent%></div>

    <%}%>
          
 </div>
 <div class="r_info_item">Other values I have for <%=username%></div>
 <div class="r_info_content">
       <%
      Model.OtherValue = string.IsNullOrEmpty(Model.OtherValue) ? ";" : Model.OtherValue;
        string[] othervalue = Model.OtherValue.Split(';');%>
    <% foreach (var item in othervalues)
       {
           foreach (var competenceId in othervalue)
           {
               if (item.CompetencyID.ToString() == competenceId)
               {
    %>
    <div class='value_item_active'>
        <%=item.CompetencyContent%></div>
    <%
               }
           }
       }%>              
 </div>   
  
<%} %>
<div class="r_info_item">
    My confirmation on <%=username%>'s competencies</div>
<div class="r_info_content">
    <% 
        string[] ocompetence = Model.Competence.Split(';');%>
    <% foreach (var item in competenices)
       {
           var classname = "value_item";
           foreach (var competenceId in ocompetence)
           {
               if (item.CompetencyID.ToString() == competenceId)
               {
                   classname = "value_item_active";
               }
           }
    %>
    <div class='<%=classname%>'>
        <%=item.CompetencyContent%></div>
    <%}%>
</div>
<div class="r_info_item">
    Other competencies
    <%=username%>
    has</div>
<div class="r_info_content">
    <% 
        string[] othercompetence = Model.OtherCompetence.Split(';');%>
    <% foreach (var item in ortherCompetenices)
       {
           foreach (var competenceId in othercompetence)
           {
               if (item.CompetencyID.ToString() == competenceId)
               {
    %>
    <div class='value_item_active'>
        <%=item.CompetencyContent%></div>
    <%
               }
           }
       }%>
</div>
<div class="r_info_item">
    How
    <%=username%>
    makes a difference?</div>
<div class="r_info_content font">
    <%=Model.Different %></div>
    <%if(user.UserData.UserID == Model.RecommendUserID){ %>
<div>
    <input id="EditRecommend" type="button" class="btn_search" value="Edit" />
</div>
<%} %>
<script type="text/javascript">
$(document).ready(function () {
    $("#EditRecommend").click(function () {
        window.location.href = "/Home/RecommendationEdit?userID=" + <%=Model.UserID %> + "";
    });
});
</script>
