﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Users>>" %>


<div class="board_list2">
    <div class="list_title">
        I Recommended Them</div>
    <div class="list_info3">
        <%foreach (var user in Model)
          {
              
        %>
        <div class="message_line">
            <div class="message_usericon">
                <%if (user.TypeID == 3)
                  {%>
                <a href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (user.TypeID == 2)
                  {%>
                <a href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <%}
                  else if (user.TypeID == 1)
                  {%>
                <a href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                    <img src="/images/<%=user.Icon%>" /></a>
                <% }%>
            </div>
            <div class="message_action">
                <table>
                    <tr>
                        <td>
                            <%if (user.TypeID == 3)
                              {%>
                            <a class="hyplink" href="/company/CompanyIndex?companyid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (user.TypeID == 2)
                              {%>
                            <a class="hyplink" href="/expert/myvalue?expertid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <%}
                              else if (user.TypeID == 1)
                              {%>
                            <a class="hyplink" href="/leader/index?leaderid=<%=user.UserID %>" target="_blank">
                                <%=user.NickName %></a>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%if (user.TypeID == 3)
                              {%>
                            <a class="hyplink" href="<%=user.GLO_Company.Website%>" target="_blank">
                                <%=user.GLO_Company.Website%></a>
                            <%}
                              else if (user.TypeID == 2)
                              {%>
                            <%= user.GLO_HRExpert.Location %>
                            <%}
                              else if (user.TypeID == 1)
                              {%>
                            <%= user.GLO_Leader.Location%>
                            <% }%>
                        </td>
                    </tr>                    
                    <tr>
                        <td><a href="javascript:void(0)" class="messagesend" x:userid="<%=user.UserID %>">Send Message</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%}
        %>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".messagesend").click(function () {
            $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: $(this).attr("x:userid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
    });
</script>