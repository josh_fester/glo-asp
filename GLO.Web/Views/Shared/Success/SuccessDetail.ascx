﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Success>" %>
 <div class="success_boxer">
     <div class="success_line">
        <div class="success_left">
             <div class="success_usericon">
                <img src="/images/<%=!String.IsNullOrEmpty(Model.GLO_Users.Icon)?Model.GLO_Users.Icon:"user_none.gif" %>" />
             </div>
             <div class="success_username"><%=Model.GLO_Users.NickName %></div>
             <%if(Model.GLO_Users.TypeID==1){ %>
             <div class="success_usertitle"><%=Model.GLO_Users.GLO_Leader.Title %></div>
             <%}else if(Model.GLO_Users.TypeID==2){ %>
             <div class="success_usertitle"><%=Model.GLO_Users.GLO_HRExpert.Title %></div>
             <%}else if(Model.GLO_Users.TypeID==3){ %>
            <%-- <div class="success_usertitle"><%=Model.GLO_Users.GLO_Company.CompanyName %></div>--%>
             <%} %>
        </div>
        <div class="success_right">
             <%=Model.Name %>
        </div>
     </div>
     <div class="success_desc"><%=Model.Description %></div>
 </div>
