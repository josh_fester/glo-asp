﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.SecurityQuestionModel>" %>
<script src="<%=Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%=Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
<style>
    .input-validation-error
    {
        border-color: Red;
    }
</style>
<% using (Html.BeginForm("SetSecurityQuestion", "Account", FormMethod.Post, new { id = "frmQuestion" }))
   { %>
<%: Html.ValidationSummary(true) %>
<%: Html.HiddenFor(m=>m.UserID) %>
<div class="lightbox_mask">
</div>
<div class="lightbox_question">
    <table>
        <tr>
            <td width="140">
                First question
            </td>
            <td>
                <div class="select_box select_box_question">
                    <%: Html.HiddenFor(m=>m.Question1) %>
                    <input class="select_value" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.QuestionContent1)?Model.QuestionContent1:"Please select a question"%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option">
                        <ul>
                            <% foreach (var item in Model.QuestionList)
                               {
                            %>
                            <li x:id="<%=item.QuestionID %>"><%=item.QuestionContent %></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <span class="required">*</span>
            </td>
        </tr>
        <tr>
            <td>
                First answer
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.Answer1, new { @class="txtbox5", maxlength="50" })%>
            </td>
        </tr>
        <tr>
            <td width="140">
                Second question
            </td>
            <td>
                <div class="select_box select_box_question">
                    <%: Html.HiddenFor(m=>m.Question2) %>
                    <input class="select_value" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.QuestionContent2)?Model.QuestionContent2:"Please select a question"%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option">
                        <ul>
                            <% foreach (var item in Model.QuestionList)
                               {
                            %>
                            <li x:id="<%=item.QuestionID %>"><%=item.QuestionContent %></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <span class="required">*</span>
            </td>
        </tr>
        <tr>
            <td>
                Second answer
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.Answer2, new { @class="txtbox5", maxlength="50" })%>
            </td>
        </tr>
        <tr>
            <td width="140">
                Third question
            </td>
            <td>
                <div class="select_box select_box_question">
                    <%: Html.HiddenFor(m=>m.Question3) %>
                    <input class="select_value" type="text" readonly="readonly" value="<%=!string.IsNullOrEmpty(Model.QuestionContent3)?Model.QuestionContent3:"Please select a question"%>" />
                    <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                    <div class="select_option">
                        <ul>
                            <% foreach (var item in Model.QuestionList)
                               {
                            %>
                            <li x:id="<%=item.QuestionID %>"><%=item.QuestionContent %></li>
                            <%} %>
                        </ul>
                    </div>
                </div>
                <span class="required">*</span>
            </td>
        </tr>
        <tr>
            <td>
                Third answer
            </td>
            <td>
                <%: Html.TextBoxFor(m => m.Answer3, new { @class="txtbox5", maxlength="50" })%>
            </td>
        </tr>
    </table>
    <div class="me_apply">
        <a href="javascript:void(0)" id="register">
            <img src="/Styles/images/icon_apply.gif"></a></div>
    <div class="me_close">
        <a onclick="return CloseLightBox()" href="javascript:void(0)">
            <img src="/Styles/images/icon_close.gif"></a></div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        SetDropdown();
        $("#register").click(function () {
            $('.input_error').each(function () {
                $(this).removeClass("input_error");
            });

            if (ValidateAssign() == false)
                return;
            var postData = $("#frmQuestion").serialize();
            $.post("/Account/SetSecurityQuestion", postData, function (data) {
                if (data.result == false) {
                    return;
                }
                else {
                    $('#Answer1').val("");
                    $('#Answer2').val("");
                    $('#Answer3').val("");
                    alertbox("Your security question was successfully saved！!");
                }
                CloseLightBox();
                return false;
            });
        });

        $(".txtbox5").blur(function () {

            if (!isEmpty($(this).val())) $(this).removeClass("input_error");
        });

    });
    function ValidateAssign() {
        var error = new Array();
        if (isEmpty($("#Question1").val()) || $("#Question1").val() == 0) {
            $("#Question1").parent().addClass('select_error');
            error.push("error")
        }
        else {
            $("#Question1").parent().removeClass('select_error');
        }
        if (isEmpty($("#Answer1").val())) {
            $("#Answer1").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Answer1").removeClass("input_error");
        }
        if (isEmpty($("#Question2").val()) || $("#Question2").val() == 0) {
            $("#Question2").parent().addClass('select_error');
            error.push("error")
        }
        else {
            $("#Question1").parent().removeClass('select_error');
        }
        if (isEmpty($("#Answer2").val())) {
            $("#Answer2").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Answer2").removeClass("input_error");
        }
        if (isEmpty($("#Question3").val()) || $("#Question3").val() == 0) {
            $("#Question3").parent().addClass('select_error');
            error.push("error")
        }
        else {
            $("#Question1").parent().removeClass('select_error');
        }
        if (isEmpty($("#Answer3").val())) {
            $("#Answer3").addClass("input_error");
            error.push("error")
        }
        else {
            $("#Answer3").removeClass("input_error");
        }
        if (error.length > 0) {
            return false;
        }
        else
            return true;
    }
    function SetDropdown() {
        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");
            $(this).css("z-index", "1001");
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
            $(".select_box").css("z-index", "1000");
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");

            var oldText = $(this).parent().parent().parent().find(":hidden").val();
            var text = $(this).text().replace(/<img([^>]*)>/gi, "");
            var value = $(this).attr("x:id");
            $(this).parent().parent().parent().find(".select_value").val(text);
            $(this).parent().parent().parent().find(":hidden").val(value);


            if (!isEmpty($("#Question1").val()) && $("#Question1").val() != 0) {
                $("#Question1").parent().removeClass('select_error');
            }
            if (!isEmpty($("#Question2").val()) && $("#Question2").val() != 0) {
                $("#Question2").parent().removeClass('select_error');
            }
            if (!isEmpty($("#Question3").val()) && $("#Question3").val() != 0) {
                $("#Question3").parent().removeClass('select_error');
            }

            $(".select_option li").each(function () {
                if ($(this).attr("x:id") == oldText) {
                    $(this).css("display", "");
                }
                if ($(this).attr("x:id") == value) {
                    $(this).css("display", "none");
                }
            });
        });
    }
</script>
