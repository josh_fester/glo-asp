﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Users>>" %>
<table class="search_item">
    <tr>
        <td>
            Keyword
        </td>
        <td>
            <input type="text" id="txtExpertKeyWord" class="keyword" value="<%=Request.QueryString["e"] %>">
        </td>
    </tr>
    <tr>
        <td>
            Location
        </td>
        <td>
            <div class="multiple_box">
                <input class="multiple_value" id="expertLocation" type="text" readonly="readonly" value="All Location" />
                <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                <div class="multiple_wrapper multiple_wrapper_location"></div> 
            </div>
        </td>
    </tr>
      <tr>
        <td>
            Industry
        </td>
        <td class="item_option_expert_industry">
              <div class="multiple_box">
               <input class="multiple_value" id="expertIndustry" type="text" readonly="readonly" value="All Industry" />
               <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
               <div class="multiple_wrapper_industry multiple_wrapper">                                                                                        
               </div>                                
           </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <input type="button" value="Search" class="bluebtn" id="ExpertSearch"/>
        </td>
    </tr>
</table>
<div class="result_title">
    We found
    <%=Model.Count() %>
    Experts.
</div>
<div class="result_container">
    <%foreach (var expert in Model)
      { %>
    <div id="<%=expert.UserID %>">
        <div class="message_line">            
            <% bool showTagButton = true;
               foreach (var taggeduser in (List<GLO.Data.Models.GLO_TagUsers>)ViewBag.TagUserList)
               {
                   if (taggeduser.TaggedUserID == expert.UserID)
                   {
                       showTagButton = false;
                   }                
               }

               bool showRequestTagButton = true;
               foreach (var taggeduser in expert.GLO_TagUsers)
               {
                   if (taggeduser.TaggedUserID.ToString() == ViewBag.UserID.ToString())
                   {
                       showRequestTagButton = false;
                   }
               }

               bool showRequestRecommendButton = true;
               foreach (var recommendUser in expert.GLO_Recommendation1)
               {
                   if (recommendUser.UserID.ToString() == ViewBag.UserID.ToString())
                   {
                       showRequestRecommendButton = false;
                   }
               }
               var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;

               if (expert.UserID.ToString() == ViewBag.UserID.ToString() || user.UserData.UserType == 4  || user.UserData.Approved == false)
               {
                   showTagButton = false;
                   showRequestTagButton = false;
                   showRequestRecommendButton = false;
               }
               %>
            <div class="message_usericon">
             <% if (user.UserData.Approved)
                { %>
                <a href="javascript:void(0);" onclick="return OpenLightFrame('/expert/myvalue?expertid=<%=expert.UserID %>');">
                    <img src="/images/<%=!String.IsNullOrEmpty(expert.GLO_HRExpert.LogoUrl)?expert.GLO_HRExpert.LogoUrl:"user_none.gif" %>" /></a>
            <%}
                else
                { %>
                <img src="/images/<%=!String.IsNullOrEmpty(expert.GLO_HRExpert.LogoUrl)?expert.GLO_HRExpert.LogoUrl:"user_none.gif" %>" />
            <%} %>
            </div>
            <div class="message_action">
                <table>
                    <tr>
                        <td>
                         <% if (user.UserData.Approved)
                            { %>
                            <a href="javascript:void(0);" onclick="return OpenLightFrame('/expert/myvalue?expertid=<%=expert.UserID %>');">
                            <%=expert.GLO_HRExpert.NickName%></a>
                         <%}
                            else
                            { %>
                            <%=expert.GLO_HRExpert.NickName%>
                         <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%=expert.GLO_HRExpert.Location%>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <%if (ViewBag.UserType.ToString() != "2"){ %>
                                <%if (!showTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                                <% }
                                  else
                                {%>
                                    <input type="button" class="bluebtn" value="Tag" onclick="TagUser('<%=expert.UserID %>')" />
                                <%} %> 
                                
                                <%if (!showRequestTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                                <% }
                                  else
                                {%>    
                                <input type="button" class="bluebtn" value="Request Tag" onclick="RequestTag('<%=expert.UserID %>')" />
                                <%} %>
                                <%} %>
                                <%if (ViewBag.UserType.ToString() != "3")
                                  {
                                      if (showRequestRecommendButton)
                                      { %>
                                <input type="button" class="bluebtn" value="Request referral" onclick="RequestRecommend('<%=expert.UserID %>')" />
                                <% }
                                      else
                                      {%> 
                                    <input type="button" class="graybtn" value="Request referral" onclick="return false;" />
                                <%}
                                  }%>
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </div>
    <%}
       %>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $.getJSON('/Home/_GetLocationList', null, function (data) {
            {
                $('.multiple_wrapper_location').html(data.Html);
            }
        });
        $.getJSON('/Home/_GetIndustryList', null, function (data) {
            {
                $('.multiple_wrapper_industry').html(data.Html);
            }
        });
        $("#ExpertSearch").click(function () {
            var keyWord = $("#txtExpertKeyWord").val();
            var location = $("#expertLocation").val();
            if (location == "All Location") {
                location = "";
            }
            var industry = $("#expertIndustry").val();
            if (industry == "All Industry")
                industry = "";

            var postData = 'keyword=' + keyWord + '&location=' + location+'&industry='+industry.replace('&','__');
            $.post("/Home/ExpertSearch", postData, function (data) {
                $('#ExpertList').html(data);
                $("#txtExpertKeyWord").val(keyWord);
                if (location != "")
                    $("#expertLocation").val(location);
                if (industry != "")
                    $("#expertIndustry").val(industry);
            });
        });
    });
</script>
