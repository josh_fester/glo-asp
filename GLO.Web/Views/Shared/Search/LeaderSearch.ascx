﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<GLO.Data.Models.GLO_Users>>" %>
<%if (ViewBag.UserType.ToString() == "1"){ %>
<div class="leader_mask"></div>
<%} %>
<table class="search_item">
    <tr>
        <td>
            Keyword
        </td>
        <td>
            <input type="text" id="txtLeaderKeyWord" class="keyword" value="<%=Request.QueryString["l"] %>">
        </td>
    </tr>
    <tr>
        <td>
            Location
        </td>
        <td>
            <div class="multiple_box">
                <input class="multiple_value" id="leaderLocation" type="text" readonly="readonly" value="All Location" />
                <img class="multiple_arrow " src="/Styles/images/icon_arrow2.gif" />
                <div class="multiple_wrapper multiple_wrapper_location"></div> 
            </div>
        </td>
    </tr>
      <tr>
        <td>
            Industry
        </td>
        <td class="item_option_leader_industry">
              <div class="multiple_box">
               <input class="multiple_value" id="leaderIndustry" type="text" readonly="readonly" value="All Industry" />
               <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
               <div class="multiple_wrapper multiple_wrapper_industry">                                                                                        
               </div>                                
           </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <input type="button" value="Search" class="bluebtn" id="LeaderSearch" />
        </td>
    </tr>
</table>
<div class="result_title">
    We found
    <%=Model.Count()%>
    Leaders.
</div>
<div class="result_container">
    <%foreach (var leader in Model)
      { %>
    <div id="<%=leader.UserID %>">
        <div class="message_line">
            <% bool showTagButton = true;
                foreach (var taggeduser in (List<GLO.Data.Models.GLO_TagUsers>)ViewBag.TagUserList)
              {
                  if (taggeduser.TaggedUserID == leader.UserID)
                  {
                      showTagButton = false;
                  }
              }

                bool showRequestTagButton = true;
                foreach (var taggeduser in leader.GLO_TagUsers)
                {
                    if (taggeduser.TaggedUserID.ToString() == ViewBag.UserID.ToString())
                    {
                        showRequestTagButton = false;
                    }
                }

                bool showRequestRecommendButton = true;
                foreach (var recommendUser in leader.GLO_Recommendation1)
                {
                    if (recommendUser.UserID.ToString() == ViewBag.UserID.ToString())
                    {
                        showRequestRecommendButton = false;
                    }
                }
                var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
                if (user.UserData.UserType == 4 || user.UserData.Approved == false)
                {
                    showTagButton = false;
                    showRequestTagButton = false;
                    showRequestRecommendButton = false;
                }
                %>
            <div class="message_usericon">
             <% if (user.UserData.Approved)
                { %>
                <a href="javascript:void(0);" onclick="return OpenLightFrame('/leader/index?leaderid=<%=leader.UserID %>');">
                    <img src="/images/<%=!String.IsNullOrEmpty(leader.GLO_Leader.LogoUrl)?leader.GLO_Leader.LogoUrl:"user_none.gif" %>" /></a>
                    <%}
                else
                { %>
                <img src="/images/<%=!String.IsNullOrEmpty(leader.GLO_Leader.LogoUrl)?leader.GLO_Leader.LogoUrl:"user_none.gif" %>" />
                    <%} %>
            </div>
            <div class="message_action">
                <table>
                    <tr>
                        <td>
                           <% if (user.UserData.Approved)
                              { %>
                            <a href="javascript:void(0);" onclick="return OpenLightFrame('/leader/index?leaderid=<%=leader.UserID %>');">
                            <%=leader.GLO_Leader.NickName%></a>
                            <%}
                              else
                              { %>
                              <%=leader.GLO_Leader.NickName%>
                            <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%=leader.GLO_Leader.Location%>
                        </td>
                    </tr>
                    
                        <tr>
                            <td>
                                <%if (ViewBag.UserType.ToString() != "1"){ %>
                                <%if (!showTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                                <% }
                                else
                                {%>
                                    <input type="button" class="bluebtn" value="Tag" onclick="TagUser('<%=leader.UserID %>')" />
                                <%} %>   
                                <%if (!showRequestTagButton)
                                  { %>
                                    <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                                <% }
                                  else
                                {%>   
                                <input type="button" class="bluebtn" value="Request Tag" onclick="RequestTag('<%=leader.UserID %>')" />
                                <%} %>   
                                <%} %>  
                                <%if (ViewBag.UserType.ToString() != "3")
                                  {
                                      if (showRequestRecommendButton)
                                      { %>
                                <input type="button" class="bluebtn" value="Request referral" onclick="RequestRecommend('<%=leader.UserID %>')" />                                
                                <% }
                                      else
                                      {%> 
                                    <input type="button" class="graybtn" value="Request referral" onclick="return false;" />
                                <%
                                      }
                                  } %>
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </div>
    <%} %>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $.getJSON('/Home/_GetLocationList', null, function (data) {
            {
                $('.multiple_wrapper_location').html(data.Html);
            }
        });
        $.getJSON('/Home/_GetIndustryList', null, function (data) {
            {
                $('.multiple_wrapper_industry').html(data.Html);
            }
        });
        $("#LeaderSearch").click(function () {
            var keyWord = $("#txtLeaderKeyWord").val();
            var location = $("#leaderLocation").val();
            if (location == "All Location") {
                location = "";
            }
            var industry = $("#leaderIndustry").val();
            if (industry == "All Industry")
                industry = "";

            var postData = 'keyword=' + keyWord + '&location=' + location + '&industry=' + industry.replace('&', '__');
            $.post("/Home/LeaderSearch", postData, function (data) {
                $('#LeaderList').html(data);
                $("#txtLeaderKeyWord").val(keyWord);
                if (location != "")
                    $("#leaderLocation").val(location);
                if (industry != "")
                    $("#leaderIndustry").val(industry);
            });
        });
    });
       
</script>
