﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Data.Models.GLO_Policy>" %>
<div class="lightbox_mask">
</div>
<div class="lightbox_register">
    <div class="policy_boxer">

  <%=Model.PolicyContent %>

    </div>
    <div class="me_apply">
        <a href="javascript:void(0)" id="readpolicy">
            <img src="/Styles/images/icon_apply.gif"></a>
    </div>
    <div class="me_close">
        <a href="javascript:void(0)" id="closepolicy">
            <img src="/Styles/images/icon_close.gif"></a></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#readpolicy").click(function () {
            $('#chkPolicy').attr("checked", "checked");
            $('#Policy').val('1');
            $('#chkPolicy').parent().removeClass("select_error");

            CloseLightBox();
        });
        $("#closepolicy").click(function () {
            $('#chkPolicy').attr("checked", "checked");
            $('#Policy').val('1');
            $('#chkPolicy').parent().removeClass("select_error");

            CloseLightBox();
        });
    });
</script>
