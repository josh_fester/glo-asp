﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GLO.Web.Models.SecurityQuestionModel>" %>
<% using (Html.BeginForm("CheckAnswer", "Account", FormMethod.Post, new { id = "frm" }))
   { %>
<%: Html.ValidationSummary(true) %>
<%: Html.HiddenFor(m=>m.UserID) %>
<table class="tbquestion">
    <tr>
        <td>
            <b>Please enter your Security Question Answers</b>
        </td>
    </tr>
    <tr>
        <td>
            <%=Model.QuestionContent1 %>
        </td>
    </tr>
    <tr>
        <td>
            <%: Html.TextBoxFor(m => m.Answer1, new { @class="txtbox1", maxlength="50" })%>
        </td>
    </tr>
    <tr>
        <td>
            <%=Model.QuestionContent2 %>
        </td>
    </tr>
    <tr>
        <td>
            <%: Html.TextBoxFor(m => m.Answer2, new { @class="txtbox1", maxlength="50" })%>
        </td>
    </tr>
    <tr>
        <td>
            <%=Model.QuestionContent3 %>
        </td>
    </tr>
    <tr>
        <td>
            <%: Html.TextBoxFor(m => m.Answer3, new { @class="txtbox1", maxlength="50" })%>
        </td>
    </tr>
    <tr>
        <td>
            <input id="SecurityQuestionStep2" type="button" class="bluebtn2" value="Next" />
        </td>
    </tr>
</table>
<% } %>

    <script type="text/javascript">
        $(document).ready(function () {
            var question1 = '<%=Model.QuestionContent1 %>'
            var question2 = '<%=Model.QuestionContent2 %>'
            var question3 = '<%=Model.QuestionContent3 %>'
            if (isEmpty(question1) || isEmpty(question2) || isEmpty(question3)) {
                $("#divselect").show();
                $("#divquestion").hide();
                $("#divemail").hide();
                $('#rbtnQuestion').attr("disabled", "disabled");
                $('#rbtnEmail').attr("checked", "checked");

                alertbox("You haven’t set the security question.");
            }

            $(".txtbox1").blur(function () {
                if (!isEmpty($(this).val())) $(this).removeClass("input_error");
            });

            $("#SecurityQuestionStep2").click(function () {
                $('.input_error').each(function () {
                    $(this).removeClass("input_error");
                });
                if (ValidateAssign() == false)
                    return;
                var postData = $("#frm").serialize();
                $.post("/Account/CheckAnswer", postData, function (data) {
                    if (data.result) {
                        window.location.href = data.errQuestion;
                    }
                    else {
                        var temp = data.errQuestion.split(',');
                        if (temp.length == 2) {
                            alertbox("The answer is wrong.")
                        }
                        else {
                            alertbox("Answers are wrong.")
                        }

                        $.each(temp, function () {
                            if (this == "1") {
                                $("#Answer1").addClass("input_error");
                            }
                            else if (this == "2") {
                                $("#Answer2").addClass("input_error");
                            }
                            else if (this == "3") {
                                $("#Answer3").addClass("input_error");
                            }
                        });
                        return false;
                    }
                });
            });
        });
        function AnswerSecurityQuestion(postData) {
            $.post("/Account/AnswerSecurityQuestion", postData, function (data) {
                $('#divquestion').html(data);
            });
        }

        function ValidateAssign() {
            var error = new Array();
            if (isEmpty($("#Answer1").val())) {
                $("#Answer1").addClass("input_error");
                error.push("error")
            }
            else {
                $("#Answer1").removeClass("input_error");
            }
            if (isEmpty($("#Answer2").val())) {
                $("#Answer2").addClass("input_error");
                error.push("error")
            }
            else {
                $("#Answer2").removeClass("input_error");
            }
            if (isEmpty($("#Answer3").val())) {
                $("#Answer3").addClass("input_error");
                error.push("error")
            }
            else {
                $("#Answer3").removeClass("input_error");
            }
            if (error.length > 0) {
                return false;
            }
            else
                return true;
        }
    </script>