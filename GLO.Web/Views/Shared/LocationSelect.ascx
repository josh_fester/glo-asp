﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<GLO.Data.Models.GLO_Location>>" %>

<div class="multiple_location">
 <div class="location_left">
      <h2>Region List</h2>
      <div class="location_list">
      <%int localIndex1 = 0;
        foreach (var country in Model)
        {
            if (country.ParentID == 0)
            { %>
            <a href="javascript:void(0);" class="<%=localIndex1==0?"current":"" %>" x:countryid="<%=country.LID %>"><%=country.LocationName %></a>
            <%
                localIndex1++;
            }           
        } %>
                                                   
      </div>
  </div>
  <div class="location_right">
      <h2>Country / City List</h2>
      <div class="location_list">
      <%int localIndex2 = 0;
        foreach (var country in Model.Where(x => x.ParentID == 0).ToList())
        { %>
          <table class="location_item" id="country<%=country.LID%>" style="<%=localIndex2==0?"display:table":"" %>">

              <%
            int i = 0;
            var list = Model.Where(x => x.ParentID == country.LID).ToList();
              %>

              <%foreach (var city in list)
                { %>

                  <%if (i % 3 == 0 || i == 0)
                    { %><tr><%} %>
                    <td><a href="javascript:void(0);" x:id="<%=city.LID%>"><%=city.LocationName%></a></td>
                  <%if (i % 3 == 2 || i == list.Count - 1)
                    { %></tr><%} %>

              <%
                    i++;
                } %>  
                    
         </table>        
        <% 
            localIndex2++;
        } %>
      </div>
  </div>
  <div class="me_close"><a class="LocationClose" href="javascript:void(0);"><img src="/Styles/images/icon_close.gif"></a></div>  
</div>

<script type="text/javascript">

    $(document).ready(function () {
        //for location start
        $(".multiple_box").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();
            $(this).find(".multiple_wrapper").toggle();
            if ($(window).height() < 640) $(this).find(".multiple_location").css("margin-top", "10px");
        });
        $(document).click(function () {
            $('.multiple_wrapper').hide();
        });
        $(".location_left a").click(function () {
            var countryid = $(this).attr("x:countryid");

            $(".location_left a").removeClass('current');
            $(this).addClass('current');
            $(this).parents(".multiple_location").find(".location_item").hide();
            $(this).parents(".multiple_location").find("#country" + countryid).show();
        });
        $(".location_item a").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();
            var value = $(this).text();

            var id = $(this).attr("x:id");
            $(this).parents(".multiple_box").find(".multiple_locationid").val(id);

            $(this).parents(".multiple_box").find(".multiple_value").val(value);
            $(this).parents(".multiple_box").removeClass("select_error");
        });
        $(".LocationClose").click(function (event) {
            event.stopPropagation();
            $(".multiple_wrapper").hide();
        });
        //for location end
    });

</script>