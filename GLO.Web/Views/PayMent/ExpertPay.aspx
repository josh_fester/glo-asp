﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Confirm Payment
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="glo_body">
    
        <div class="glo_boxer">

            <div id="IsPaymented" style="display:block">
                <div class="boxer_headline">
                    You are now a GLO HR expert!
                </div>
                <div class="boxer_content">
                   Thanks for your payment. You can now start experiencing all the benefits of being 
    a GLO HR expert.<br /><br />
    Meanwhile, we invite you to  <a class="hpyblank" href="/Expert/MyValue?expertid=<%=Model.UserID %>" target="_blank">further complete your profile</a> or <a class="hpyblank" href="/Expert/MyRecommendation?expertid=<%=Model.UserID %>" target="_blank">build recommendations of clients</a>

                </div>
           </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#payment").click(function () {
            $.postJSON('<%=Url.Action("PayMent","Expert") %>', { expertid: <%=Model.UserID %> }, function (data) {
                alertbox("Payment success", function(){
                    $("#NotPaymented").hide();
                    $("#IsPaymented").show();
                });
            });
        })
        $("#profile").click(function(){
            window.location = '/Expert/MyValue?expertid=<%=Model.UserID %>';
        });
        $("#homepage").click(function(){
            window.location = '/home';
        });
    });
</script>
</asp:Content>
