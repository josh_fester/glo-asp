﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PayMentSuccess
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Payment status
            </div>
            <div class="boxer_content">
                <%=ViewBag.Message%>
            </div>
        </div>
    </div>

</asp:Content>
