﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsEventsDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Events Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td width="200">
            Order Id</td>
            <td>
            <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
           Title</td>
            <td>
            <%: Html.DisplayFor(model => model.Title)%></td>
        </tr>
        <tr>
            <td>
            File Url</td>
            <td>
            <%: Html.DisplayFor(model => model.FileUrl) %></td>
        </tr>
        <%if (Model.FileType != 3) { %>
        <tr>
            <td>
            Description</td>
            <td>
            <%: Html.DisplayFor(model => model.Content) %></td>
        </tr>
        <%} %>
        <tr>
            <td>
            <%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "NewsEventsEdit", new { id = Model.NewsID })%>
        |
        <%: Html.ActionLink("Back to List", "NewsEventsList")%>
    </p>
</asp:Content>