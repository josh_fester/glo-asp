﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Location>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LocationEdit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
     <%: Html.HiddenFor(model => model.LID) %>
     <h2><%=Model.ParentID == 0 ? "Edit Region" : "Edit Country/City "%>  | <%: Html.ActionLink("Back to list", "LocationList", "Admin")%></h2>
    <table class="tbeditor">
     
        <tr>
            <td>
                <%=Model.ParentID == 0 ? "Region" : "Country/City "%>
            </td>
            <td>
                <%: Html.EditorFor(model => model.LocationName) %>
            <%: Html.ValidationMessageFor(model => model.LocationName) %>
            </td>
        </tr>
           <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.Oreder, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.Oreder) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>

   <%-- <fieldset>
        <legend>GLO_Location</legend>

        <%: Html.HiddenFor(model => model.LID) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ParentID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ParentID) %>
            <%: Html.ValidationMessageFor(model => model.ParentID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.LocationName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.LocationName) %>
            <%: Html.ValidationMessageFor(model => model.LocationName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Oreder) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Oreder) %>
            <%: Html.ValidationMessageFor(model => model.Oreder) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>--%>
<% } %>


</asp:Content>
