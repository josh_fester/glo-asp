﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_PollTopic>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PollList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Poll List | <%: Html.ActionLink("Add Poll Topic", "PollTopicAdd", "Admin")%></h2>


    <table class="tblist">
    <tr>
        <th>
            <%: Html.DisplayNameFor(model => model.PollTopic) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.PollContent) %>
        </th>
        <th width="150">
            <%: Html.DisplayNameFor(model => model.CreateDate) %>
        </th>
        <th width="85">
            <%: Html.DisplayNameFor(model => model.IsMultipile) %>
        </th>
        <th width="85">
            <%: Html.DisplayNameFor(model => model.IsActive) %>
        </th>
        <th width="200"></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.PollTopic) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.PollContent) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CreateDate) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsMultipile) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsActive) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { id=item.PollID }) %> |
            <%: Html.ActionLink("Add Item", "PollItemAdd", new { id = item.PollID })%> |
            <%: Html.ActionLink("Review", "PollReview", new { id = item.PollID })%>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>