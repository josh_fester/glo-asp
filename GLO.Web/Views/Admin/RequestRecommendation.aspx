﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Request Recommendation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Request Recommendation  | <%: Html.ActionLink("Back to list", "SuccessList", "Admin")%></h2>

 <%
     var data = new ViewDataDictionary();
     data.Add("sender", ViewData["Sender"]);
     Html.RenderPartial("~/Views/Shared/Message/MessageEditForAdmin.ascx", (GLO.Web.Models.MessageModel)ViewBag.SendMessage, data); 
 %> 

 <div id="lightboxwrapper">
</div>
</asp:Content>