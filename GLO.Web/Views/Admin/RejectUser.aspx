﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RejectUser
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Send Email</h2>
<%using (Html.BeginForm("RejectUser", "Admin", FormMethod.Post, new { id = "frm" })){ %>
<table class="tbeditor">
        <tbody><tr>
            <td>
                Send to
            </td>
            <td>
                <div class="form_receive">
                    <%=ViewBag.UserName %>
                    <input type="button" style="display: none;" class="btn_small" id="SelectUser" value="Select">
                    <div style="margin-top:5px;" id="divReceive">
                        
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Title
            </td>
            <td>
                <input type="text" maxlength="100" value="" id="Title" name="Title" class="txt_form">
            </td>
        </tr>
        <tr>
            <td>
                Content
            </td>
            <td>
                <textarea id="Content" name="Content" class="area_form"></textarea>
            </td>
        </tr>
        <tr>
            <td class="tdsubmit" colspan="2">
                <input type="button" class="btn_submit" value="Send" id="btnSend">
            </td>
        </tr>
    </tbody></table>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".btn_submit").click(function () {
            $.postJSON('<%=Url.Action("RejectUser") %>', { id: <%=ViewBag.UserID %>, title:$("#Title").val(),content:$("#Content").val() }, function (data) {
                if (data) {
                    alertbox("The email has been successfully sent to the user.");
                }
            });
        });
    });
</script>
</asp:Content>
