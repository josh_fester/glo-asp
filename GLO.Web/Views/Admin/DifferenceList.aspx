﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Difference>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DifferenceList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Difference List | <%: Html.ActionLink("Add Difference", "DifferenceAdd")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>

    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Difference Content
            </th>
            <th width="80">
                <%: Html.DisplayNameFor(model => model.Status) %>
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%= HttpUtility.HtmlDecode(item.DifferenceContent)%>
            </td>
            <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "DifferenceEdit", new { id = item.DifferenceID })%>
                |
                <%: Html.ActionLink("Details", "DifferenceDetails", new { id = item.DifferenceID })%>
                |
                <%: Html.ActionLink("Delete", "DeleteDifference", new { id = item.DifferenceID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this Difference？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisableDifference", new { id = item.DifferenceID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this Difference？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnableDifference", new { id = item.DifferenceID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this Difference？');" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "DifferenceList", new { })%>
    <br />

    <script type="text/javascript">
    </script>
</asp:Content>