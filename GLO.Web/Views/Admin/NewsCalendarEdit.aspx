﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsCalendarEdit
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%: System.Web.Optimization.BundleTable.Bundles.ResolveBundleUrl("~/Content/themes/base/css") %>"
        rel="stylesheet" type="text/css" />
    <h2>
        Edit News Calendar | <%: Html.ActionLink("Back to List", "NewsCalendarList")%></h2>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(model => model.NewsID) %>
    <%: Html.HiddenFor(model => model.Status) %>
    <%: Html.HiddenFor(model => model.CategoryID) %>
    <table class="tbeditor">
        <tr>
            <td>
                Category
            </td>
            <td>
                <%: Html.DropDownListFor(model => model.CalendarCategory, ViewBag.CategoryList as SelectList)%>
                <%: Html.ValidationMessageFor(model => model.CalendarCategory)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.Title) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.Title, new { MaxLength = "100" })%>
                <%: Html.ValidationMessageFor(model => model.Title)%>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
            </td>
            <td>
                <%: Html.TextBox("CalendarStartDate", Model.CalendarStartDate.Value.ToString("yyyy-MM-dd"), new { @readonly = "true" })%>
                <%: Html.ValidationMessageFor(model => model.CalendarStartDate)%>
            </td>
        </tr>
        <tr>
            <td>
                End Date
            </td>
            <td>
                <%: Html.TextBox("CalendarEndDate", Model.CalendarEndDate.Value.ToString("yyyy-MM-dd"), new { @readonly = "true" })%>
                <%: Html.ValidationMessageFor(model => model.CalendarEndDate)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#CalendarStartDate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                onClose: function (selectedDate) {
                    $("#CalendarEndDate").datepicker("option", "minDate", selectedDate);
                },
                beforeShow: function () {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css("z-index", 15234234);
                    }, 100);
                }
            });

            $("#CalendarEndDate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                onClose: function (selectedDate) {
                    $("#CalendarStartDate").datepicker("option", "maxDate", selectedDate);
                },
                beforeShow: function () {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css("z-index", 15234234);
                    }, 100);
                }
            });
        });
    </script>
</asp:Content>
