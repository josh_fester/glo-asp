﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Partner>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PartnerAdd
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Add Partner | <%: Html.ActionLink("Back to List", "PartnerList")%></h2>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
            <td>
            <label for="OrderId">
                Order Id</label></td>
            <td>
            <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
            <%: Html.ValidationMessageFor(model => model.OrderId)%></td>
        </tr>
        <tr>
            <td>
            Summary</td>
            <td>
            <%: Html.TextBoxFor(model => model.Content, new { MaxLength = "500" })%>
            <%: Html.ValidationMessageFor(model => model.Content) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Description) %></td>
            <td>
            <%: Html.TextAreaFor(model => model.Description)%>
            <script type="text/javascript">
                window.onload = function () {
                    CKEDITOR.replace('Description');
                };               
            </script>
            <%: Html.ValidationMessageFor(model => model.Description) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Image) %></td>
            <td class="UploadFile">
                
            <%: Html.TextBoxFor(model => model.Image)%>
            <input type="button" id="file_upload" class="bluebtn" value="Upload File">
            <div class="Banner-field BannerImg" id="imageID" style="clear:both;margin-top:5px;">
                <img src="" alt="" />
            </div>
            <div style="display: none">
                <a href="javascript:$('#file_upload').uploadify('upload');">上传</a>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#file_upload').uploadify({
                        'height': 25,
                        'width': 120,
                        'auto': true,
                        'multi': true,
                        'buttonText': 'Upload File',
                        'fileTypeExts': '*.gif; *.jpg; *.png;*.rmvb;*.avi;*.mpeg;*.wmv',
                        'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                        'uploader': '/Home/Upload',
                        'onUploadSuccess': function (file, data, response) {
                            eval("data=" + data);
                            if (data.Success) {
                                    $("#imageID").empty().append($("<img />").attr({ "src": data.Urlpath, "x:src": data.Urlpath }));
                                    $("#Image").val(data.Urlpath);
                            }
                            else {
                                alertbox(data.Message);
                            }
                        }
                    });
                });
            </script>
            </td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Title) %></td>
            <td>
            <%: Html.EditorFor(model => model.Title) %>
            <%: Html.ValidationMessageFor(model => model.Title) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>

    <% } %>

</asp:Content>