﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsCalendarList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        News List | <%: Html.ActionLink("Add News", "NewsCalendarAdd", "Admin")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    <table class="tblist">
        <tr>
            <th>
                Title
            </th>
            <th width="320">
                Category
            </th>
            <th width="150">
                Start Date
            </th>
            <th width="150">
                End Date
            </th>
            <th width="80">
                Status
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.Title) %>
            </td>
            <td>
                <%if (item.CalendarCategory == 1)
                  { %>
                Next GLO LDP
                <%}
                  else if (item.CalendarCategory == 2)
                  { %>
                Next GLO assessment
                <%}
                  else if (item.CalendarCategory == 3)
                  { %>
                Upcoming webinars
                <%}
                  else if (item.CalendarCategory == 4)
                  { %>
                Next GLO Thought Leadership Series Meeting
                <%}
                  else
                  { %>
                Other
                <%} %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.CalendarStartDate) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.CalendarEndDate)%>
            </td>
            <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "NewsCalendarEdit", new { id = item.NewsID })%>
                |
                <%: Html.ActionLink("Details", "NewsCalendarDetails", new { id = item.NewsID })%>
                |
                <%: Html.ActionLink("Delete", "DeleteNewsCalendar", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this News？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisableNewsCalendar", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this News？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnableNewsCalendar", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this News？')" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "NewsCalendarList", new { })%>
    <br />
</asp:Content>
