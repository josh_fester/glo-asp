﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_SurveyReport>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Survey reports | Add survey report
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Survey reports | <%: Html.ActionLink("Add survey report", "SurveyAdd")%></h2>
  <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Title
            </th>
            <th width="100">
                Category
            </th>
            <th width="80">
                Status
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderID) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.SurveyTitle) %>
            </td>
            <td>
                <%: item.SurveyType == 1? "Pdf/Doc" : "Url" %>
            </td>
            <td>
                <%: item.Status == true ? "Enable" : "Disable"%>
            </td>
            <td>
            <%: Html.ActionLink("Edit", "SurveyEdit", new { id = item.SurveyID }, null)%>
            |
               
                <%: Html.ActionLink("Delete", "DeleteSurvey", new { id = item.SurveyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this item？')" })%>
                |
                <% if (item.Status)
                   { %>
                <%: Html.ActionLink("Disable", "DisableSurvey", new { id = item.SurveyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this item？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnableSurvey", new { id = item.SurveyID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this item？')" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "SurveyList", new { })%>
    <br />

</asp:Content>
