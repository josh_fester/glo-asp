﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_SurveyReport>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SurveyAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add survey report | <%: Html.ActionLink("Back to survey report list", "SurveyList")%></h2>
 <script type="text/javascript" src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
<% using (Html.BeginForm("SurveyAdd", "Admin", FormMethod.Post, new { id = "frm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
    <table class="tbeditor">
        <tr>
            <td width="200">
                <label for="OrderId">
                    Order Id</label>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.OrderID, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.OrderID)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.SurveyTitle) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.SurveyTitle, new { MaxLength = "200" })%>
                <%: Html.ValidationMessageFor(model => model.SurveyTitle)%>
            </td>
        </tr>
        <tr>
            <td>
                File Category
            </td>
            <td>                
                <input type="radio" name ="SurveyType" id="rbtnImage" checked="checked" value="1"/>Pdf/Doc
                <input type="radio" name ="SurveyType" id="rbtnUrl" value="2"/>Url
            </td>
        </tr>
        <tr>
            <td>
                File Url
            </td>
            <td class="UploadFile">
                <%: Html.TextBoxFor(model => model.SurveyUrl)%>
                
                <input type="button" id="UploadFile" class="bluebtn" value="Upload File" /><br /><br />
                <%: Html.ValidationMessageFor(model => model.SurveyUrl)%><br />
                <span id="UploadSpan">You can enter the URL address directly or upload a file.</span>
            </td>
        </tr>
         <tr id="TrContent">
            <td>
               Description
            </td>
            <td>
                <%: Html.TextAreaFor(model => model.SurveryInfo, new { MaxLength = "1000" })%>
                <%: Html.ValidationMessageFor(model => model.SurveryInfo)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#UploadFile').uploadify({
                'height': 25,
                'width': 120,
                'auto': true,
                'multi': true,
                'buttonText': 'Upload File',
                'fileTypeExts': '*.pdf; *.doc; *.docx',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/Upload',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    if (data.Success) {
                        $("#SurveyUrl").val(data.Urlpath);
                    }
                    else {
                        alertbox(data.Message);
                    }
                }
            });

            $('#rbtnImage').click(function () {
                $("#UploadFile").css("display", "block");
                $("#UploadSpan").css("display", "block");

            });

            $('#rbtnUrl').click(function () {
                $("#UploadFile").css("display", "none");
                $("#UploadSpan").css("display", "none");
            });

            $('#Submit').click(function () {
                $("#frm").submit();
            });
        });
    </script>

</asp:Content>
