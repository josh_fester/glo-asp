﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_FeedBack>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FeedBackDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        FeedBack Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td> Order Id</td>
            <td><%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td><%: Html.DisplayNameFor(model => model.Question) %></td>
            <td> <%: Html.DisplayFor(model => model.Question) %></td>
        </tr>
        <tr>
            <td><%: Html.DisplayNameFor(model => model.Status) %></td>
            <td><%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "FeedBackEdit", new { id = Model.FeedBackID })%>
        |
        <%: Html.ActionLink("Back to List", "FeedBackList") %>
    </p>
</asp:Content>