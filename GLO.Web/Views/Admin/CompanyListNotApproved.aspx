﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.UserQueryModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Pending Company List</h2>
<% using (Html.BeginForm("CompanyListNotApproved", "Admin", FormMethod.Post, new { id = "frmSearch" }))
   { %>
    <div class="search_user">
        Company Name:<%:Html.EditorFor(m => m.SearchText)%> 
        <input type="submit" id="btnSearch" value="Search" />
    </div>
<%}%>
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %>    

<table class="tblist">
    <tr>
     <th width="60">
            User ID
        </th>   
        <th>
            Company Name
        </th>      
        <th>
            Email
        </th>
        <th>
        Company Type
        </th>
        <th width="100">
            Register Date
        </th>
        <th width="60">
            Reject
        </th>
        
        <th width="60">
            Approved
        </th>
        <th width="60">
            Lock
        </th>
    </tr>

<% foreach (var item in Model.QueryResult)
   {
       if (item.GLO_Company != null){ %>
    <tr>
         <td>
            <%: Html.ActionLink(item.UserID.ToString(), "CompanyIndex", "Company", new { companyid = item.UserID }, new { @target = "_blank" })%>
        </td>
        <td>
            <%: Html.ActionLink(item.GLO_Company.CompanyName, "CompanyIndex", "Company", new { companyid = item.UserID }, new { @target = "_blank" })%>
        </td>      
        <td>
            <%: Html.DisplayFor(modelItem => item.Email) %>
        </td>  
        <td>
         <%=item.GLO_Company.GLO_CompanyType.Organization%> &nbsp;<%=item.GLO_Company.GLO_CompanyType.CompanyLevel%>
        </td>   
        <td>
            <%=string.Format("{0:yyyy-MM-dd}",item.RegisterDate) %>
        </td>
         <td>
              <%: Html.ActionLink("Reject", "RejectUser", new { id = item.UserID })%>
        
        </td>
        <td>         
            <%: Html.CheckBoxFor(modelItem => item.Approved, new { onclick = "Approved(this);", id = item.UserID })%>
        </td>
        <td>         
            <%: Html.CheckBoxFor(modelItem => item.IsLock, new { onclick = "Lock('" + item.UserID + "');", id = "chkLock"+item.UserID })%>
        </td>
    </tr>
<% }} %>

</table>
<br />
 <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "CompanyListNotApproved", new { })%>
 <br />
                
<script type="text/javascript">
    function Approved(obj) {
        var message = 'Do you want to approve this user？';
        if (!obj.checked) {
            message = 'Do you want to unapprove this user？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("ApprovedUser") %>', { id: obj.id }, function (data) {
                if (!data) {
                    obj.checked = "";
                    return false;
                }
                else {
                    window.top.frmRight.location.reload();
                }
            });
        },
        function () {
            obj.checked = "";
            return false;
        });

    }
    function Lock(id) {
        var checkBtn = $('#chkLock' + id);
        var message = 'Do you want to lock the malicious user？';
        if (checkBtn.attr("checked") != "checked") {
            message = 'Do you want to unlock the malicious user？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("LockUser") %>', { id: id }, function (data) {
                if (!data) {
                    checkBtn.removeAttr("checked");
                    return false;
                }
            });
        },
        function () {
            if (checkBtn.attr("checked") == "checked") {
                checkBtn.removeAttr("checked");
            }
            else {
                checkBtn.attr("checked", "checked");
            }
            return false;
        });
    }
</script>
</asp:Content>
