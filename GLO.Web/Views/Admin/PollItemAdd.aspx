﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_PollItem>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PollItemAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add Poll Item | <%: Html.ActionLink("Back to list", "PollList", "Admin")%></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary(true)%>
    
    <table class="tbeditor">
        <tr>
            <td>
            <%: Html.LabelFor(model => model.PollID)%></td>
            <td>
            <%: Html.EditorFor(model => model.PollID)%>
            <%: Html.ValidationMessageFor(model => model.PollID)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.ChoiceName)%></td>
            <td>
            <%: Html.TextBoxFor(model => model.ChoiceName, new { @class = "required email" })%>
            <%: Html.ValidationMessageFor(model => model.ChoiceName)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.ChoiceCount)%></td>
            <td>
            <%: Html.EditorFor(model => model.ChoiceCount)%>
            <%: Html.ValidationMessageFor(model => model.ChoiceCount)%></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>


<script type="text/javascript">
    $(document).ready(function () {
        $("#CreatePoll").click(function () {
            var postData = $("form").serialize();
            $.postJSON('<%=Url.Action("PollTopicAddTest") %>', postData, function (data) {
                //var row = "<li><a class='jqModel EditTag' href='#' Name='" + data.Name + "' Destination='" + data.Destination + "'>" + data.Name + "</a></li>";
            });
            alertbox(postData);
        });


    });
</script>

</asp:Content>
