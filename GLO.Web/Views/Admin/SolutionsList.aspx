﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Solutions>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SolutionsList
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Solutions List | <%: Html.ActionLink("Add Solutions", "SolutionsAdd")%></h2>
    <%var pageNumber = (int)ViewData["p"];
      var perPage = (int)ViewData["perPage"];
      var RecordCount = (int)ViewData["RecordCount"];    
    %>
    <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th width="150">
                Category
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Title) %>
            </th>
            <th width="80">
                <%: Html.DisplayNameFor(model => model.Status) %>
            </th>
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%if (item.CategoryID == 1)
                  { %>
                Online Top
                <%}
                  else if (item.CategoryID == 2)
                  { %>
                Online Bottom
                <%}
                  else if (item.CategoryID == 3)
                  { %>
                GLO Team Top
                <%}
                  else
                  { %>
                GLO Team Bottom
                <%} %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Title)%>
            </td>
            <td>
                <%: item.Status==true?"Enable":"Disable" %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "SolutionsEdit", new { id = item.SolutionsID })%>
                |
                <%: Html.ActionLink("Details", "SolutionsDetails", new { id = item.SolutionsID })%>
                |
                <%: Html.ActionLink("Delete", "DeleteSolutions", new { id = item.SolutionsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this Solutions？')" })%>
                |
                <% if (item.Status.Value)
                   { %>
                <%: Html.ActionLink("Disable", "DisableSolutions", new { id = item.SolutionsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to disable this Solutions？')" })%>
                <%}
                   else
                   { %>
                <%: Html.ActionLink("Enable", "EnableSolutions", new { id = item.SolutionsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to enable this Solutions？')" })%>
                <%} %>
            </td>
        </tr>
        <% } %>
    </table>
    <br />
    <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "SolutionsList", new { })%>
    <br />
</asp:Content>
