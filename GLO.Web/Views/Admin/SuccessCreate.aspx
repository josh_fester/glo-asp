﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Success>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SuccessCreate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add Success  | <%: Html.ActionLink("Back to list", "SuccessList", "Admin")%></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
        <td>
            <%: Html.LabelFor(model => model.Name) %>
        </td>
        <td>
            <%: Html.EditorFor(model => model.Name) %>
            <%: Html.ValidationMessageFor(model => model.Name) %>
        </td>
        </tr>
        <tr>

        <td>
            <%: Html.LabelFor(model => model.Description) %>
        </td>
        <td>
            <%: Html.TextAreaFor(model => model.Description)%>
            <%: Html.ValidationMessageFor(model => model.Description)%>
        </td>
        </tr>
        <tr>
        <td>
            <%: Html.LabelFor(model => model.CreateDate) %>
        </td>
        <td>
            <%: Html.EditorFor(model => model.CreateDate, new { @readonly = "true" })%>
            <%: Html.ValidationMessageFor(model => model.CreateDate) %>
        </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>

<% } %>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#CreateDate").datepicker({
                defaultDate: "+1w",
                yearRange: '-29:+0',
                maxDate: new Date(),
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                onClose: function (selectedDate) {
                    $("#CalendarEndDate").datepicker("option", "minDate", selectedDate);
                },
                beforeShow: function () {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css("z-index", 15234234);
                    }, 100);
                }
            });

        });
    </script>


</asp:Content>