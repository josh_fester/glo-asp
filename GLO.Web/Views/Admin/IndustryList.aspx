﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_IndustryCategory>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Industry Category
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Industry List  | <%: Html.ActionLink("Add Industry", "IndustryAdd", "Admin")%></h2>

<table class="tblist">
        <tr>
            
             <th width="80">
                Order ID
            </th>         
            <th>
                Industry Category
            </th>  
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model.OrderBy(x=>x.OrderBy).ToList())
           { %>
        <tr>
            <td>
            <%=item.OrderBy %>
            </td>
           
            <td>
              
                <%=item.ParentID == 0 ? Html.ActionLink(item.IndustryName, "IndustryList", new { parentid = item.IndustryID }).ToHtmlString() : item.IndustryName%>
            </td>
           
            <td>
                <%: Html.ActionLink("Edit", "IndustryEdit", new { id = item.IndustryID })%>
                |
                  <%if (item.ParentID == 0)
                  { %>

                <%: Html.ActionLink("Add Sub-Industry", "IndustryAdd", new { parenetid = item.IndustryID })%>
                |
                <%} %>
                <%: Html.ActionLink("Delete", "IndustryDelete", new { id = item.IndustryID }, new { onclick = "return HpylinkAction(this, '" + (item.ParentID == 0 ? "Do you want to delete this industry category and its child industry?" : "Do you want to delete this industry？") + "');"
})%>
                
               
            </td>
        </tr>
        <% } %>
    </table>

</asp:Content>
