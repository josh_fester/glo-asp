﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsCalendarDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        News Calendar Details</h2>
    <table class="tbeditor">
        <tr>
            <td>
                Category
            </td>
            <td>
                <%if (Model.CalendarCategory == 1)
                  { %>
                Next GLO LDP
                <%}
                  else if (Model.CalendarCategory == 2)
                  { %>
                Next GLO assessment
                <%}
                  else if (Model.CalendarCategory == 3)
                  { %>
                Upcoming webinars
                <%}
                  else if (Model.CalendarCategory == 4)
                  { %>
                Next GLO Thought Leadership Series Meeting
                <%}
                  else
                  { %>
                Other
                <%} %>
            </td>
        </tr>
        <tr>
            <td>
                Title
            </td>
            <td>
                <%: Html.DisplayFor(model => model.Title)%>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
            </td>
            <td>
                <%: Model.CalendarStartDate.Value.ToString("yyyy-MM-dd") %>
            </td>
        </tr>
        <tr>
            <td>
                End Date
            </td>
            <td>
                <%: Model.CalendarEndDate.Value.ToString("yyyy-MM-dd")%>
            </td>
        </tr>
        <tr>
            <td>
                Status
            </td>
            <td>
                <%if (Model.Status.Value)
                  { %>
                Enabled
                <%}
                  else
                  { %>
                Disabled
                <%} %>
            </td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "NewsCalendarEdit", new { id = Model.NewsID })%>
        |
        <%: Html.ActionLink("Back to List", "NewsCalendarList")%>
    </p>
</asp:Content>
