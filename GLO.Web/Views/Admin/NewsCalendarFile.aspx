﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsCalendarFile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>
        Events List | <%: Html.ActionLink("Add Event", "NewsCalenderFileAdd", "Admin")%></h2>
 <table class="tblist">
        <tr>
            <th width="80">
                Order Id
            </th>
            <th>
                Title
            </th>
            <th>
                File Url
            </th>
           
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.OrderId) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Title) %>
            </td>
         
            <td>
                <%: Html.DisplayFor(modelItem => item.FileUrl) %>
            </td>
      
            <td>
            
                <%: Html.ActionLink("Edit", "NewsCalenderFileEdit", new { id = item.NewsID })%>
                |
                <%: Html.ActionLink("Delete", "DeleteNewsCalendarFile", new { id = item.NewsID }, new { onclick = "return HpylinkAction(this, 'Are you confirmed to delete this News？')" })%>
             
            </td>
        </tr>
        <% } %>
    </table>

</asp:Content>
