﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.UserQueryModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <%var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];    
    %>  
<h2>Leader List</h2>
<% using (Html.BeginForm("LeaderList", "Admin", FormMethod.Post, new { id = "frmSearch" }))
   { %>
    <div class="search_user">
        Nick Name:<%:Html.EditorFor(m => m.SearchText)%>
        <input type="submit" id="btnSearch" value="Search" />
    </div>
<%}%>
<table class="tblist">
    <tr>
        <th width="50">
            User ID
        </th>   
        <th>
            Nick Name
        </th>      
        <th>
            Email
        </th>
        <th width="100">
            Register Date
        </th>
        <th width="55">
            Approved
        </th>        
        <th width="45">
            Lock
        </th>
        <th width="600">Action</th>
        <th>Operation</th>
    </tr>

<% foreach (var item in Model.QueryResult) { %>
    <tr>
     <td>
           
            <%: Html.ActionLink(item.UserID.ToString(), "Index", "Leader", new { leaderid = item.UserID }, new { @target = "_blank" })%>
        </td>
        <td>
            <%: Html.ActionLink(item.NickName, "Index", "Leader", new { leaderid = item.UserID }, new { @target="_blank"})%>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Email) %>
        </td>      
        <td>
            <%=string.Format("{0:yyyy-MM-dd}",item.RegisterDate) %>
        </td>
        <td>
            <%: Html.CheckBoxFor(modelItem => item.Approved, new { onclick = "Approved(this);", id = item.UserID })%>
        </td>
        <td>         
            <%: Html.CheckBoxFor(modelItem => item.IsLock, new { onclick = "Lock('" + item.UserID + "');", id = "chkLock"+item.UserID })%>
        </td>
        <td> 
          Interview 
            <%: Html.CheckBoxFor(modelItem => item.GLO_Leader.IsInterview, new { onclick = "ChangeProfileStatus('" + item.UserID + "','Interview');", id = "Interview" + item.UserID })%>|
            Online Assessment 
            <%: Html.CheckBoxFor(modelItem => item.GLO_Leader.OnlineAssessment, new { onclick = "ChangeProfileStatus('" + item.UserID + "','Online');", id = "Online" + item.UserID })%>|
           Offline Assessment
            <%: Html.CheckBoxFor(modelItem => item.GLO_Leader.OfflineAssessment, new { onclick = "ChangeProfileStatus('" + item.UserID + "','Offline');", id = "Offline" + item.UserID })%> |
           WLDP
            <%: Html.CheckBoxFor(modelItem => item.GLO_Leader.WALP, new { onclick = "ChangeProfileStatus('" + item.UserID + "','WLDP');", id = "WLDP" + item.UserID })%>|
           Coaching
            <%: Html.CheckBoxFor(modelItem => item.GLO_Leader.Coaching, new { onclick = "ChangeProfileStatus('" + item.UserID + "','Coaching');", id = "Coaching" + item.UserID })%>
        </td>
         <td>
             <%: Html.ActionLink("Delete", "DeleteLeader", "Admin", new { id = item.UserID }, new
{
    onclick = "return HpylinkAction(this, 'Do you want to delete this leader permanently?');"
})%>
            </td>
    </tr>
<% } %>

</table>
<br />
 <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "LeaderList", new { })%>
 <br />

<script type="text/javascript">
    function Approved(obj) {
        var message = 'Do you want to approve this user？';
        if (!obj.checked) {
            message = 'Do you want to unapprove this user？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("ApprovedUser") %>', { id: obj.id }, function (data) {
                if (!data) {
                    obj.checked = "";
                    return false;
                }
            });
        }, 
        function () {
            if (obj.checked) {
                obj.checked = "";
            }
            else {
                obj.checked = "true";
            }
            return false;
        });

    }


    function ChangeProfileStatus(id, type) {
        var message = "";
        var checkBtn = $('#'+type + id);
        if (type == "Interview") {
            message = "Do you want to light up the Interview icon?";
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to undo light up the Interview icon?';
            }
        }
        else if (type == "Online") {
            message = "Do you want to light up the Online Assessment icon?";
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to undo light up the Online Assessment icon?';
            }
        }
        else if (type == "Offline") {
            message = "Do you want to light up the Offline Assessment icon?";
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to undo light up the Offline Assessment icon?';
            }
        }
        else if (type == "WLDP") {
            message = "Do you want to light up the WLDP icon?";
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to undo light up the WLDP icon?';
            }
        }
        else if (type == "Coaching") {
            message = "Do you want to light up the Coaching icon?";
            if (checkBtn.attr("checked") != "checked") {
                message = 'Do you want to undo light up the Coaching icon?';
            }
        }

        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("ChangeProfileStatus") %>', { id: id, type: type }, function (data) {
                if (!data) {
                    checkBtn.removeAttr("checked");
                    return false;
                }
            });
        },
        function () {
            if (checkBtn.attr("checked") == "checked") {
                checkBtn.removeAttr("checked");
            }
            else {
                checkBtn.attr("checked", "checked");
            }
            return false;
        });
    }


    function Lock(id) {
        var checkBtn = $('#chkLock' + id);
        var message = 'Do you want to lock the malicious user？';
        if (checkBtn.attr("checked") != "checked") {
            message = 'Do you want to unlock the malicious user？';
        }
        confirmbox(message,
        function () {
            $.postJSON('<%=Url.Action("LockUser") %>', { id: id }, function (data) {
                if (!data) {
                    checkBtn.removeAttr("checked");
                    return false;
                }
            });
        },
        function () {
            if (checkBtn.attr("checked") == "checked") {
                checkBtn.removeAttr("checked");
            }
            else {
                checkBtn.attr("checked", "checked");
            }
            return false;
        });
    }
</script>
</asp:Content>
