﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Partner>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PartnerCreate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>PartnerCreate</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

        
    <table class="tbeditor">
        <tr>
            <th colspan="2">GLO_Partner</th>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Name) %></td>
            <td>
            <%: Html.EditorFor(model => model.Name) %>
            <%: Html.ValidationMessageFor(model => model.Name) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Content) %></td>
            <td>
            <%: Html.EditorFor(model => model.Content) %>
            <%: Html.ValidationMessageFor(model => model.Content) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Description) %></td>
            <td>
            <%: Html.EditorFor(model => model.Description) %>
            <%: Html.ValidationMessageFor(model => model.Description) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Image) %>
             <%: Html.EditorFor(model => model.Image, null,new {style="width:10px;"})%></td>
            <td>
 <div class="Banner-field BannerImg" id="imageID">
    <img src="" alt=""/>
    </div>
           <div>
    <input type="file" id="file_upload" name="file_upload" />
</div>
<div style="display:none">
    <a href="javascript:$('#file_upload').uploadify('upload');">上传</a>
</div>
    <script type="text/javascript">
        $(function () {
            $('#file_upload').uploadify({
                'auto': true,
                'multi': false,
                'buttonImage': '<%=Url.Content("~/JS/uploadify/browse-btn.png")%>',
                'fileTypeDesc': '图片文件',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/Upload',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    // alert(data.Urlpath);
                    // alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                    $("#imageID").empty().append($("<img />").attr({ "src": data.Urlpath, "x:src": data.Urlpath }));
                    $("#Image").val(data.Urlpath);
                }


            });
        });
</script></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Title) %></td>
            <td>
            <%: Html.EditorFor(model => model.Title) %>
            <%: Html.ValidationMessageFor(model => model.Title) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>

</asp:Content>
