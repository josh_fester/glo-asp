﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Difference>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DifferenceAdd
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Add Difference | <%: Html.ActionLink("Back to List", "DifferenceList")%></h2>
    <% using (Html.BeginForm())
       { %>
    <%: Html.ValidationSummary(true) %>
    <table class="tbeditor">
        <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.OrderId) %>
            </td>
        </tr>
        <tr>
            <td>
                Difference Content
            </td>
            <td>
                <%: Html.TextAreaFor(model => model.DifferenceContent)%>
                <script type="text/javascript">
                    window.onload = function () {
                        CKEDITOR.replace('DifferenceContent');
                    };
                </script>
                <%: Html.ValidationMessageFor(model => model.DifferenceContent) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
</asp:Content>
