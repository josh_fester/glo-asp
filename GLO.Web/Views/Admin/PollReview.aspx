﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_PollItem>>" %>
<%@ Import Namespace="GLO.Data.Models" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PollReview
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Poll Review | <%: Html.ActionLink("Back to list", "PollList", "Admin")%></h2>
<% var polltopic = (GLO_PollTopic)ViewData["PollTopic"];
    %>

<table class="tblist">
   

<% foreach (var item in Model) { %>
    <tr>
    <td>
      <% if (polltopic.IsMultipile!=true)
       { %>
       <input id="Radio1" class="rdItem" x:itemid=<%=item.ItemID %> name="Item" type="radio" value="<%=item.ItemID %>"/>
       <%}
       else
       { %>
        <input id="Checkbox1" name="Item" type="checkbox" value="<%=item.ItemID %>" />
       <%} %>
    </td>
      
        <td>
            <%: Html.DisplayFor(modelItem => item.ChoiceName) %>
        </td>
        <td>
        <span style="background-color:Green; width:<%=item.ChoiceCount*100*3/Model.Sum(x => x.ChoiceCount)%>px; display:block">&nbsp;</span> 
        </td>
        <td>
        <%=item.ChoiceCount*100/Model.Sum(x => x.ChoiceCount)  %>%
        </td>
       
        
    </tr>
<% } %>

</table>


<br />
 <p><input id="PollSubmit" type="submit" x:pollid =<%=polltopic.PollID %> value="Submit" /></p>
 <script type="text/javascript">
     $(document).ready(function () {
         $("#PollSubmit").click(function () {
             var pollid = $(this).attr("x:pollid");
             var arr = new Array();
             var item = $('input:radio[name="Item"]:checked').val();
             if (item != null) {
                 arr.push(item);
             }
             $('input:checkbox[name="Item"]:checked').each(function () {
                 arr.push($(this).val());
             });
             if (arr.length > 0) {
                 $.postJSON('<%=Url.Action("PollVote") %>', { "itemid": arr.toString() }, function (data) {
                     if (data) alertbox("Success");
                 });

             }
             else {
                 alertbox("pleas select one itme at least!");              
            }

         });

     });
 </script>
</asp:Content>