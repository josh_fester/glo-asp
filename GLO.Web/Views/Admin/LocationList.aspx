﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_Location>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Country List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Location List  | <%: Html.ActionLink("Add Region", "LocationAdd", "Admin")%></h2>

<table class="tblist">
        <tr>
            
             <th width="80">
                Order ID
            </th>  
            <th>
                Region
            </th>       
            <th width="240">
                Action
            </th>
        </tr>
        <% foreach (var item in Model.OrderBy(x=>x.Oreder).ToList())
           { %>
        <tr>
           
            <td>
            <%=item.Oreder %>
            </td>
            <td>
               
               <%=item.ParentID == 0 ? Html.ActionLink(item.LocationName, "LocationList", new { parentid = item.LID }).ToHtmlString() : item.LocationName%>
            </td>
           
            <td>
                <%: Html.ActionLink("Edit", "LocationEdit", new { id = item.LID })%>
                |
                <%if (item.ParentID == 0)
                  { %>
                <%: Html.ActionLink("Add Country/City", "LocationAdd", new { parenetid = item.LID })%>
                |
                <%} %>
                <%: Html.ActionLink("Delete", "LocationDelete", new { id = item.LID }, new
{
    onclick = "return HpylinkAction(this, '" + (item.ParentID == 0 ? "Do you want to delete this country and its  city or district?" : "Do you want to delete this city or district?") + "');"
})%>
               
            </td>
        </tr>
        <% } %>
    </table>

</asp:Content>
