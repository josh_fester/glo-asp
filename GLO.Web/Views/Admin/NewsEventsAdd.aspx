﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_News>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NewsAdd
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Add News | <%: Html.ActionLink("Back to list", "NewsEventsList", "Admin")%></h2>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>
<% using (Html.BeginForm("NewsEventsAdd", "Admin", FormMethod.Post, new { id = "frm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
    <table class="tbeditor">
        <tr>
            <td width="200">
                <label for="OrderId">
                    Order Id</label>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.OrderId)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.LabelFor(model => model.Title) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.Title, new { MaxLength = "100" })%>
                <%: Html.ValidationMessageFor(model => model.Title)%>
            </td>
        </tr>
        <tr>
            <td>
                File Category
            </td>
            <td>                
                <input type="radio" name ="FileType" id="rbtnImage" checked="checked" value="1"/>Image
                <input type="radio" name ="FileType" id="rbtnVideo" value="2"/>Video
                <input type="radio" name ="FileType" id="rbtnUrl" value="3"/>Url
            </td>
        </tr>
        <tr>
            <td>
                File Url
            </td>
            <td class="UploadFile">
                <%: Html.TextBoxFor(model => model.FileUrl)%>
                
                <input type="button" id="UploadFile" class="bluebtn" value="Upload File"><br /><br />
                <%: Html.ValidationMessageFor(model => model.FileUrl)%><br />
                <span id="UploadSpan">You can enter the URL address directly or upload a file.</span>
            </td>
        </tr>
         <tr id="TrContent">
            <td>
               Description
            </td>
            <td>
                <%: Html.TextAreaFor(model => model.Content, new { MaxLength = "1000" })%>
                <%: Html.ValidationMessageFor(model => model.Content)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#UploadFile').uploadify({
                'height': 25,
                'width': 120,
                'auto': true,
                'multi': true,
                'buttonText': 'Upload File',
                'fileTypeExts': '*.gif; *.jpg; *.png;*.rmvb;*.avi;*.mpeg;*.wmv',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/Upload',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);
                    if (data.Success) {
                        $("#FileUrl").val(data.Urlpath);
                    }
                    else {
                        alertbox(data.Message);
                    }
                }
            });

            $('#rbtnImage').click(function () {
                $("#UploadFile").show();
                $("#UploadSpan").show();
                $("#TrContent").show();
            });

            $('#rbtnVideo').click(function () {
                $("#UploadFile").show();
                $("#UploadSpan").show();
                $("#TrContent").show();
            });

            $('#rbtnUrl').click(function () {
                $("#UploadFile").hide();
                $("#UploadSpan").hide();
                $("#TrContent").hide();
            });

            $('#Submit').click(function () {
                $("#frm").submit();
            });
        });
    </script>
</asp:Content>
