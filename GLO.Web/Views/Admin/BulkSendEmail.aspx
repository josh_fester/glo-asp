﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    BulkSendEmail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>BulkSendEmail</h2>
 
  <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
  <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
<% using (Html.BeginForm())
   {%>
    <%: Html.ValidationSummary(true) %>
    <table class="tbeditor">
        <tr>
            <td width="200">
                <label for="OrderId">
                    Send Type</label>
            </td>
            <td>
                 <select id="SendType" name="SendType">
    <option value="0">All user</option>
    <option value="1">All leader</option>
    <option value="2">All expert</option>
    <option value="3">All company</option>                                           
  </select>
            </td>
        </tr>
        <tr>
            <td>
               Title
            </td>
            <td>
               <input type="text" class="txt_form" id="emailTitle" name="emailTitle" value="" maxlength="100" />
               <span class="field-validation-valid" data-valmsg-replace="true" data-valmsg-for="emailTitle"></span>

       
            </td>
        </tr>   
         <tr id="TrContent">
            <td>
               Email Template
            </td>
            <td>
               <textarea id="editor1" name="editor1"><%=ViewBag.Content.Replace("\n", "").Replace("\r", "")%></textarea>
                <script type="text/javascript">
                    window.onload = function () {
                        var config = {
                            width: 970,
                            height: 1200,
                            toolbar: 'Basic1'
                        };

                        CKEDITOR.replace('editor1', config);
                    };

                </script>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Bulk Send" class="btn_submit" />
            </td>
        </tr>
    </table>
    <% } %>
</asp:Content>
