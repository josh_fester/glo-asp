﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Difference>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DifferenceDetails
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Difference Details</h2>
        
    <table class="tbeditor">
        <tr>
            <td>Order Id</td>
            <td> <%: Html.DisplayFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>Difference Content</td>
            <td> <%= HttpUtility.HtmlDecode(Model.DifferenceContent)%></td>
        </tr>
        <tr>
            <td><%: Html.DisplayNameFor(model => model.Status) %></td>
            <td>
            <%if (Model.Status.Value)
              { %>
            Enabled
            <%}
              else
              { %>
            Disabled
            <%} %></td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "DifferenceEdit", new { id = Model.DifferenceID })%>
        |
        <%: Html.ActionLink("Back to List", "DifferenceList")%>
    </p>
</asp:Content>