﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_PollTopic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PollTopicAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Add Poll Topic | <%: Html.ActionLink("Back to list", "PollList", "Admin")%></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
    <table class="tbeditor">
        <tr>
            <td>
            <%: Html.LabelFor(model => model.PollTopic) %></td>
            <td>
            <%: Html.EditorFor(model => model.PollTopic) %>
            <%: Html.ValidationMessageFor(model => model.PollTopic) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.PollContent) %></td>
            <td>
            <%: Html.EditorFor(model => model.PollContent) %>
            <%: Html.ValidationMessageFor(model => model.PollContent) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.CreateDate) %></td>
            <td>
            <%: Html.EditorFor(model => model.CreateDate) %>
            <%: Html.ValidationMessageFor(model => model.CreateDate) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.IsMultipile) %></td>
            <td>
            <%: Html.EditorFor(model => model.IsMultipile) %>
            <%: Html.ValidationMessageFor(model => model.IsMultipile) %></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.IsActive) %></td>
            <td>
            <%: Html.EditorFor(model => model.IsActive) %>
            <%: Html.ValidationMessageFor(model => model.IsActive) %></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Create" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>

</asp:Content>
