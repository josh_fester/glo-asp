﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Management Platform </title>
    <link href="/Styles/admin.css" rel="stylesheet" type="text/css" />
    <script src="<%: System.Web.Optimization.BundleTable.Bundles.ResolveBundleUrl("~/Scripts/js") %>"></script>
</head>
<body>
<% 
   var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
%>
    <table class="master_wrap">
        <tr>
            <td colspan="2" class="master_head">
                <div class="head_logo">
                    <a href="/Home"><img src="/Styles/images/logo.gif" /></a>
                    <b>Welcome</b> <%=user.UserData.UserName %> [ <a href="/Account/ChangePassword" target="frmRight">Change Password</a>, <a href="/Account/SignOut">Log Out</a> ]
                </div>
                <div class="head_nav">
                    <ul>
                        <li class="active"><a href="/Admin/Index">Manage</a></li>
                        <li><a href="/Home">Home Page</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td class="master_nav">
                <iframe id="frmLeft" name="frmLeft" class="master_frm" src="/Admin/Menu" frameborder="0"></iframe>
            </td>
            <td>
                <iframe id="frmRight" name="frmRight" class="master_frm" src="/Admin/Main" frameborder="0"></iframe>
            </td>
        </tr>
    </table>
    
<script type="text/javascript">

    $(document).ready(function () {
        $(".master_frm").css("height", document.body.clientHeight - 125 + "px");
    });
    $(window).resize(function () {
        $(".master_frm").css("height", document.body.clientHeight - 125 + "px");
    });
</script>

</body>
</html>
