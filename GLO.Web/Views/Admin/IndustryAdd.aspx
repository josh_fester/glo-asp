﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_IndustryCategory>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    IndustryAdd
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
  <%: Html.HiddenFor(model => model.ParentID) %>

  <h2><%=Model.ParentID == 0 ? "Add Industry Category " : "Add Sub-Industry"%> | <%: Html.ActionLink("Back to list", "IndustryList", "Admin")%></h2>

   <table class="tbeditor">
        <tr>
            <td>
                <%=Model.ParentID == 0 ? "Category Name" : "Item Name"%> 
            </td>
            <td>
                <%: Html.EditorFor(model => model.IndustryName) %>
            <%: Html.ValidationMessageFor(model => model.IndustryName) %>
            </td>
        </tr>
           <tr>
            <td>
                Order Id
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.OrderBy, new { MaxLength = "5" })%>
                <%: Html.ValidationMessageFor(model => model.OrderBy) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
                <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>
<% } %>
</asp:Content>
