﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Solutions>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SolutionsEdit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Edit Solutions | <%: Html.ActionLink("Back to List", "SolutionsList") %></h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.SolutionsID) %>
        <%: Html.HiddenFor(model => model.Status) %>

    <table class="tbeditor">
        <tr>
            <td>
            Order Id</td>
            <td>
            <%: Html.TextBoxFor(model => model.OrderId, new { MaxLength = "5" })%>
            <%: Html.ValidationMessageFor(model => model.OrderId) %></td>
        </tr>
        <tr>
            <td>
                Category
            </td>
            <td>
                <%: Html.DropDownListFor(model => model.CategoryID, ViewBag.CategoryList as SelectList)%>
                <%: Html.ValidationMessageFor(model => model.CategoryID)%>
            </td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Title) %></td>
            <td>
            <%: Html.TextBoxFor(model => model.Title, new { MaxLength = "1000" })%>
            <%: Html.ValidationMessageFor(model => model.Title)%></td>
        </tr>
        <tr>
            <td>
            <%: Html.LabelFor(model => model.Content) %></td>
            <td>
            <%: Html.TextAreaFor(model => model.Content)%> 
             <script type="text/javascript">
                 window.onload = function () {
                     CKEDITOR.replace('Content');
                 };               
            </script> 
            <%: Html.ValidationMessageFor(model => model.Content)%></td>
        </tr>
        <tr>
            <td colspan="2" class="tdsubmit">
            <input type="submit" value="Save" class="btn_submit" />
            </td>
        </tr>
    </table>

<% } %>


</asp:Content>
