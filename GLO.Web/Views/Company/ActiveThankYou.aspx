﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.LoginModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ActiveThankYou
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("PaymentLogin", "Account", FormMethod.Post, new { id = "login" }))
   { %>
    <%: Html.ValidationSummary(true) %>
 <div class="glo_body">
        <div class="body_boxer">
            <div class="boxer_headline2">
                Thank you joining the GLO community !
            </div>
            <%if (Request.QueryString["companytype"] == "1" || Request.QueryString["companytype"] == "5")
              { %>
            
            <div class="boxer_content2">
            We appreciate your interest to join GLO and look forward to assist you!<br /><br />

            Simply confirm your username and password here and you will be guided to your company dashboard which will allow you to:<br />
            <ul>
                <li>Post jobs</li>
                <li>Tag and contact leaders and HR experts</li>
                <li>Build your function dedicated talent pipeline</li>
                <li>Book GLO search and assessment experts</li>
            </ul>

            </div>

            <%}
              else
              { %>
              <div class="boxer_content2">
               We appreciate your interest to  join GLO and look forward to assist you!<br /><br />

                Simply confirm your username and password here and you will be guided through our payment options to activate your ‘basic’ membership.

            </div>
            <%} %>
            
        <div class="box_regard">
            Success!<br>
            Peter Buytaert<br>
            CEO, GLO<br><br>
        </div>
        <div class="box_login">
            <div>Username <%: Html.EditorFor(model => model.UserName) %></div>
            <div>Password<%: Html.EditorFor(model => model.Password) %></div>
        </div>
        <div id="errorMessage"></div>
        <div class="boxer_agree">
            <a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>
    </div>
    </div>
<% } %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnSubmit").click(function () {
            var username = $("#UserName").val();
            var password = $("#Password").val();

            if (username == "" || password == "") {
                showError(0);
                return false;
            }
            else {

                $.postJSON('<%=Url.Action("PaymentLogin","Account") %>', { "username": username, "password": password }, function (data) {
                    if (data.Success == true) {
                        window.location.href = data.ResultUrl;
                        return false;
                    }
                    else {
                        showError(data.Result);
                        return false;
                    }
                });

            }
            return false;
        });
    });
    function showError(mode) {
        var html = '';
        if (mode == 0) {
            html = '<b>Your user name and password can not be null.</b><br>Please try again.';
        }
        else if (mode == 1) {
            html = '<b>Your user name or password was incorrect.</b><br>Please try again.';
        }
        else if (mode == 2) {
            html = '<b>Your account has been blocked!<br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }
        else if (mode == 3) {
            html = '<b>Your have exceeded 5 tries.</b><br>Please <a href="#">click here</a> to reset your password.';
        }
        else if (mode == 9) {
            html = '<b>Unknow error!<br/>Please click <a href="/Home/Contact">here</a> to contact GLO admin.';
        }
        $('#errorMessage').html(html);
    }
</script>
</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
