﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.ChangePasswordModel>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ChangePassword
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx");%> 
                </div>
                <div class="board_right">
                     <%Html.RenderPartial("~/Views/Shared/ChangePassoword.ascx",Model);%> 
                </div>
               
                
            </div>
        </div>
         <%: Html.HiddenFor(m=>m.UserID) %>
 </div>

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">

</div>
</asp:Content>
