﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    BookAssessment
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
 <div class="glo_manage">
        <div class="nav_company nav_company_dashboard">
            <ul>
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">
                    Life At</a></li>
                <li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
                <li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
                <li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
        <div class="dbboxer">
            <div class="border_inner">
                <div class="board_nav">
                    <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx", Model.GLO_Company);%>
                </div>
                <div class="board_list1">
                    <div class="box_form">
                        <table>
                            <tr>
                                <td valign="top">
                                    <label for="UserName">
                                        Subject</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="Subject"><span class="required">*</span><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="UserName">
                                        Company</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="Company"><span class="required">*</span><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="Password">
                                        For who</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="ObjectPerson"><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="Password">
                                        Location</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="Location"><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="Password">
                                        Contact person</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="ContactPerson"><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="Password">
                                        Contact details</label>
                                </td>
                                <td>
                                    <input type="text" class="txt_form" value="" id="ContactDetail"><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label for="Password">
                                        Notes</label>
                                </td>
                                <td>
                                    <textarea id="Comment" class="area_form2"></textarea>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <input type="button" id="assessment_send" value="Submit" class="btn_form">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
     $(document).ready(function () {
         $("#assessment_send").click(function () {
         if(ValidateAssessment() == false)
                return;
            var jsondata ={
                         'UserID':<%=Model.UserID %>,
                         'Subject':$('#Subject').val(),
                         'Company':$("#Company").val(),
                         'ObjectPerson':$("#ObjectPerson").val(),
                         'Location':$('#Location').val(),
                         'ContactPerson':$('#ContactPerson').val(),
                         'ContactDetail':$('#ContactDetail').val(),
                         'Comment':$('#Comment').val()
                   };
           $.postJSON('<%=Url.Action("AssessmentAdd") %>', { jsondata:$.toJSON(jsondata) }, function (data) {                         
                         alertbox("Your request has been successfully sent！", function(){
                            $(":text").val("");
                            $('#Comment').val("");
                            window.location.reload();
                         });
                         return false;
                     });
         });

         $("#Subject").blur(function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");         
         });
         $("#Company").blur(function(){
                if(!isEmpty($(this).val())) $(this).removeClass("input_error");            
         });

     });
             function ValidateAssessment()
                 { 
                   var error = new Array();
                   if(isEmpty($("#Subject").val()))
                   { 
                      $("#Subject").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#Subject").removeClass("input_error");
                   }

                   if(isEmpty($("#Company").val()))
                   { 
                      $("#Company").addClass("input_error");
                      error.push("error")
                   }
                   else
                   {
                      $("#Company").removeClass("input_error");
                   }
                                      
                   if(error.length>0)
                   return false;
                   else 
                   return true;
                 }
    </script>
</asp:Content>
