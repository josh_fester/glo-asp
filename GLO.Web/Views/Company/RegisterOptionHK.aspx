﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterOptionHK
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_body">
        <div class="body_boxer">       
            <table class="tbcompany">
                <tr>
                    <td><a href="/Company/RegisterLegal?RegType=1"><img src="/Styles/images/knowglo2.gif" /></a></td>
                    <td><img src="/Styles/images/company_reg1.gif" /></td>
                    <td><img src="/Styles/images/company_reg2.gif" /></td>
                    <td><img src="/Styles/images/company_reg3.gif" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><b>Build your<br />company profile</b></td>
                    <td><b>Access GLO<br />leader profiles</b></td>
                    <td><b>Post any one job<br />for 3 months</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <div class="boxtext">
                            <b>+  Get free online ‘wise’ assessment of your team and candidates on GLO</b>
                        </div>
                    </td>
                </tr>
            </table>   
            <table class="tbcompany2">
                <tr>
                    <td colspan="3" class="other">
                        OR SELECT
                    </td>
                </tr>
                <tr>
                    <td width="300" align="right"><a href="/Company/RegisterLegal?RegType=2"><img src="/Styles/images/basic1.gif" /></a></td>
                    <td>
                        <div class="boxtext">
                            And receive the ‘get to know GLO’ value:<br />                           
                            
                            <ul>
                                <li>For 12 months instead of 3</li>
                                <li>With support of GLO’s research team to select
                                 30 resumes for max 3 positions below 40,000 USD
                                 annual compensation</li>
                            </ul>

                        </div>
                    </td>
                    <td width="200">
                        <div class="greytext">Now only 180 USD</div>
                    </td>
                </tr>
                <tr>
                    <td width="300" align="right"><a href="/Company/RegisterLegal?RegType=3"><img src="/Styles/images/sme1.gif" /></a></td>
                    <td>
                        <div class="boxtext">
                            And receive the ‘Basic GLO’ value:<br />
                            <ul>
                                <li>Including any 5 jobs simultaneously posted at any time during the year  </li>
                                <li>With support of GLO’s research team to select
                                 50 resumes for max 5 positions below 40,000
                                 USD annual compensation</li>
                            </ul>
                        </div>
                    </td>
                    <td width="200">
                        <div class="greytext">Now only 380 USD</div>
                    </td>
                </tr>
                <tr>
                    <td width="300" align="right"><a href="/Company/RegisterLegal?RegType=4"><img src="/Styles/images/premium1.gif" /></a></td>
                    <td>
                        <div class="boxtext">
                            And receive the ‘SME GLO’ value:<br />
                            <ul>
                                <li>With an additional  25% discount on executive search
                              assignment fees</li>
                            </ul>                       

                        </div>
                    </td>
                    <td width="200">
                        <div class="greytext">Now only 980 USD</div>
                    </td>
                </tr>
            </table>

        </div>
</div>


</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
