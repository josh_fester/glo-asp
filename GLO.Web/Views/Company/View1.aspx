﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RegisterCompanyModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    View1
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>View1</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>RegisterCompanyModel</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.UserName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.UserName) %>
            <%: Html.ValidationMessageFor(model => model.UserName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NickName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NickName) %>
            <%: Html.ValidationMessageFor(model => model.NickName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Location) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Location) %>
            <%: Html.ValidationMessageFor(model => model.Location) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ConfirmPassword) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ConfirmPassword) %>
            <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Address) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Address) %>
            <%: Html.ValidationMessageFor(model => model.Address) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Industry) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Industry) %>
            <%: Html.ValidationMessageFor(model => model.Industry) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhoneNumber1) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PhoneNumber1) %>
            <%: Html.ValidationMessageFor(model => model.PhoneNumber1) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhoneNumber2) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PhoneNumber2) %>
            <%: Html.ValidationMessageFor(model => model.PhoneNumber2) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhoneNumber3) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PhoneNumber3) %>
            <%: Html.ValidationMessageFor(model => model.PhoneNumber3) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.WebSite) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.WebSite) %>
            <%: Html.ValidationMessageFor(model => model.WebSite) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Revenue) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Revenue) %>
            <%: Html.ValidationMessageFor(model => model.Revenue) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Size) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Size) %>
            <%: Html.ValidationMessageFor(model => model.Size) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Employees) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Employees) %>
            <%: Html.ValidationMessageFor(model => model.Employees) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
