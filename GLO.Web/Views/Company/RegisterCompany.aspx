﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.RegisterCompanyModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterCompany
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("RegisterCompany", "Company", FormMethod.Post, new { id = "regfrm" }))
   {%>
    <%: Html.ValidationSummary(true) %>
 <div class="glo_body">
<input type="hidden" name="CompanyType" id="CompanyType" value=<%=ViewBag.RegType%> />
    <div class="body_boxer">
            <div class="boxer_headline2">
               Thank You for confirming your ‘<%=ViewBag.RegTypeName%>’ membership!

            </div>
        <div class="lightbox_register3">
            <table>
                <tr>
                   <td colspan="2" height="10">You are now only 3 steps away from exploring all its benefits.</td>
                    </tr>
                 <tr>
                    <td colspan="2" height="10">Kindly confirm your basic company info:</td>
                </tr>
                 <tr>
                    <td width="140" class="toptext">
                        Email
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.Email, new { @class = "txtbox1" })%><span class="required">*</span>
                        <%: Html.ValidationMessageFor(model => model.Email)%>
                    </td>
                </tr>              
                <tr>
                    <td class="toptext">
                        Password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.Password, new { @class = "txtbox1" }) %><span class="required">*</span>
                        <%: Html.ValidationMessageFor(model => model.Password) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Confirm password
                    </td>
                    <td>
                        <%: Html.PasswordFor(model => model.ConfirmPassword, new { @class = "txtbox1" })%><span class="required">*</span>
                        <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Company name
                    </td>
                    <td>
                         <%: Html.TextBoxFor(model => model.CompanyName, new { @class = "txtbox1" })%><span class="required">*</span>
                         <%: Html.ValidationMessageFor(model => model.CompanyName)%>
                    </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Company address
                    </td>
                    <td>
                         <%: Html.TextBoxFor(model => model.Address, new { @class = "txtbox1" })%><span class="required">*</span>
                         <%: Html.ValidationMessageFor(model => model.Address)%>
                    </td>
                </tr>
               
                <tr>
                    <td class="toptext">
                        Contact phone
                    </td>
                    <td>
                    <%: Html.TextBoxFor(model => model.PhoneNumber1, new { @class = "txtbox2" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber2, new { @class = "txtbox2" })%> -
                    <%: Html.TextBoxFor(model => model.PhoneNumber3, new { @class = "txtbox4" })%><span class="required">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Industry
                    </td>
                    <td>
                     <div class="multiple_box">
                        <input class="multiple_value" id="Industry" name="Industry" type="text" readonly="readonly" value="Select an industry" />
                        <img class="multiple_arrow" src="/Styles/images/icon_arrow2.gif" />  
                        <div class="multiple_wrapper">
                            <%Html.RenderPartial("~/Views/Shared/IndustrySelect.ascx", (IList<GLO.Data.Models.GLO_IndustryCategory>)ViewBag.IndustryList); %>
                        </div>
                     </div>
                     <span class="required">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="toptext">
                        Website
                    </td>
                    <td>
                        <%: Html.TextBoxFor(model => model.WebSite, new { @class = "txtbox1" })%>
                         <%: Html.ValidationMessageFor(model => model.WebSite)%>
                         </td>
                </tr>
                 <tr>
                    <td class="toptext">
                        Employees
                    </td>
                    <td>
                     <div class="select_box">
                            <input class="select_value" id="drpSize" name="Size" type="text" readonly="readonly" value="Select a scale" />
                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="select_option">
                                <ul>
                                    <li>1-5</li>
                                    <li>6–50</li>
                                    <li>51–250</li>
                                    <li>251–1000</li>
                                    <li>1001–5000</li>
                                    <li>>5000</li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                  <tr>
                    <td class="toptext">
                        Revenues
                    </td>
                    <td>
                     <div class="select_box">
                            <input class="select_value" id="drpRevenue" name="Revenue" type="text" readonly="readonly" value="Select a scope" />
                            <img class="select_arrow" src="/Styles/images/icon_arrow2.gif" />
                            <div class="select_option">
                                <ul>
                                    <li>1-5</li>
                                    <li>6–50</li>
                                    <li>51–250</li>
                                    <li>251–1000</li>
                                    <li>1001–5000</li>
                                    <li>>5000</li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
              
            </table> 
            </div>
        <div class="me_apply"><a href="javascript:void(0)" id="btnSubmit"><img src="/Styles/images/icon_apply.gif"></a></div>
    </div>
 </div>
 <%} %>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnSubmit").click(function () {
            if (isEmpty($("#Industry").val()) || $("#Industry").val() == "Select an industry") {
                $("#Industry").parent().addClass("select_error");
            }

            $("#regfrm").submit();
        });

        /*open dropdown*/
        $(".select_box").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");
            $(this).css("z-index","1001");
            $(this).find(".select_option").toggle();
        });
        /*close dropdown*/
        $(document).click(function () {
            $('.select_option').hide();
            $(".select_box").css("z-index", "1000");
        });
        /*set value*/
        $(".select_option li").click(function (event) {
            event.stopPropagation();
            $(".select_option").hide();
            $(".select_box").css("z-index", "1000");

            var value = $(this).text();
            $(this).parent().parent().parent().find(".select_value").val(value);
        });
    });
</script>

</asp:Content>
