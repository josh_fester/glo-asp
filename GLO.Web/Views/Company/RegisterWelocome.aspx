﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="body_boxer">       
            <div class="boxer_headline2">
                Welcome to the GLO community of Good Leaders Online<br />
                And thank you for your interest to join as a company member
            </div>
            <div class="boxer_content5">
               Our company members have access to our select pool of leaders which have generally more than 10 years executive leadership experience and/or are graduates with a master or post graduates with a PhD or Doctorate degree. <br /><br />

<b>GLO’s leaders have made a moral pledge that one stands for integrity with a strong interest to not only build value for companies but for ‘the common good’</b>(making quality decisions that support multiple bottom-lines: shareholder value, sustainability, societal interests):   GLO connects ‘good’ leaders to ‘good’ companies. <br /><br />

We invite ‘good’ companies to explore GLO’s value by offering a free 3 months ‘Get to know GLO’ membership. <br /><br />

Upon registration, you can start browsing our ‘good’ leaders, post jobs, request a candidate’s online ‘wise leadership’ assessment and contact our GLO certified HR experts.<br /><br />

If you would like to learn more about GLO’s comprehensive leadership selection, assessment and development solutions, kindly review our <a href="/Home/FAQ" target="_blank">FAQ</a> or click here to get details on <a href="/Home/Solution" target="_blank">GLO company solutions</a>.<br /><br />

Gaining access to your GLO free membership is a simple 4 step approach:<br />
<ul class="decimal">
    <li>Select your free ‘Get to know GLO’ membership or more elaborate solutions that you may prefer.</li>
    <li>Provide your basic company data.</li>
    <li>Read and accept our one page terms and conditions.</li>
    <li>Confirm your membership via email link provided by GLO after validating your membership and start using GLO! </li>
</ul>

            </div>
            <div class="boxer_agree3">
                <a href="/Company/RegisterOption"><img src="/Styles/images/knowglo1.gif" /></a>
            </div>
        </div>
    </div>

</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
