﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    RegisterThankYou
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_body">
        <div class="glo_boxer">
            <div class="boxer_headline">
                Thank you for completing your company profile!
            </div>
            <div class="boxer_content">
                Our GLO team will review your membership application and may contact you via the email associated with your account for further info. <br><br>

                After your membership is approved, kindly activate the confirmation link we will provide to your email address and follow the instructions to gain full access to GLO.<br><br>

                We again express our sincere thanks for your interest to join the GLO leadership community and look forward to assisting your organization!

                <br /><br />
                Peter Buytaert<br>
			    CEO, GLO
                <br><br><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        <div class="boxer_agree">
            <a href="/Company/CompanyIndex?companyid=<%=Model.UserID %>"><img src="/Styles/images/icon_apply.gif" /></a>
        </div>
    </div>
    </div>

</asp:Content>

