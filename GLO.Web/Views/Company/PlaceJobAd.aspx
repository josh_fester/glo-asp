﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PlaceJobAd
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_list1">
                
                    <div class="jobad_box"><a href="/Company/CurrentJobAd?companyid=<%=Model.UserID %>">Select from current job slots</a></div>
                    <div class="jobad_box"><a href="/Company/UpgradeMembership?companyid=<%=Model.UserID %>">Upgrade or buy a new 5 job slots</a></div>

                	<div class="box_form">
                    <table class="tb_form" style="display:none">
                        <tbody><tr>
                          <td valign="top"><label for="UserName">Job Title</label></td>
                            <td>
                                <input type="text" class="txt_form font" value=""  id="JobTitle"><br>                                                      
                            </td>
                          </tr>
                          <tr>
                            <td valign="top"><label for="UserName">Job ID</label></td>
                            <td>
                                <input type="text" class="txt_form font" value="" id="JobID"><br>
                                                     
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><label for="Password">Location</label></td>
                            <td>
                                <input type="text" class="txt_form font" value="" id="Location"><br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Industry</label></td>
                            <td>
                                <input type="text" class="txt_form font" value=""  id="Industry"><br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">PostDate</label></td>
                            <td>
                                <input type="text" class="txt_form font" value="" readonly="readonly"  id="PostDate"><br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Salary Level</label></td>
                            <td>
                            <input type="text" class="txt_form font" value="" id="Salary"><br>
                           
                              
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Key Responsibilities </label></td>
                            <td>
                                 <textarea id="KeyResponsibility" class="area_form3 font"></textarea>
                               <br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Key Competencies </label></td>
                            <td>
                                 <textarea id="KeyCompetence" class="area_form3 font"></textarea>
                               <br>
                               
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><label for="Password">Job Summary </label></td>
                            <td>
                                 <textarea id="JobSummary" class="area_form3 font"></textarea>
                               <br>
                               
                            </td>
                        </tr>
                         <tr>
                            <td valign="top"><label for="Password">Education</label></td>
                            <td>
                            <input type="text" value="" id="Education" class="txt_form font"><br>
                           
                              
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><label for="Password">Contact Person</label></td>
                            <td>
                            <input type="text" value="" class="txt_form font" id="ContactPerson"><br>
                           
                              
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="button" id="jobad_save" value="Publish" class="btn_form">  &nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" value="Cancel" class="btn_form"></td>
                        </tr>
                    </tbody></table>

                </div>
                </div>
               
                
            </div>
        </div>
 </div>
  <script type="text/javascript">
     $(document).ready(function () {
         $("#jobad_save").click(function () {
            var jsondata ={
                         'UserID':<%=Model.UserID %>,
                         'JobTitle':$('#JobTitle').val(),
                         'JobNO':$("#JobID").val(),
                         'Location':$("#Location").val(),
                         'Industry':$('#Industry').val(),
                         'PostDate':$('#PostDate').val(),
                         'Salary':$('#Salary').val(),
                         'KeyResponsibility':$('#KeyResponsibility').val(),
                         'KeyCompetence':$('#KeyCompetence').val(),
                         'JobSummary':$('#JobSummary').val(),
                         'Education':$('#Education').val(),
                         'ContactPerson':$('#ContactPerson').val()
                   };
           $.postJSON('<%=Url.Action("JobAdd") %>', { jsondata:$.toJSON(jsondata) }, function (data) {
                         alertbox("Sussess");
                         return false;
                     });
         });

          $("#PostDate").datepicker({
            defaultDate: "+1w",
            yearRange: '-29:+0',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOtherMonths: true
        });

     });
 </script>
</asp:Content>