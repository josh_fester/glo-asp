﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyProfile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>CompanyProfile</h2>

<fieldset>
    <legend>GLO_Users</legend>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.UserName) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.UserName) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.PassWord) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.PassWord) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Email) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Email) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.TypeID) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.TypeID) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.IndustryID) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IndustryID) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.IsLock) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsLock) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.RegisterDate) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.RegisterDate) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Approved) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Approved) %>
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Edit", "Edit", new { id=Model.UserID }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
