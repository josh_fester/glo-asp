﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
 PayMent Detail
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="/Styles/reg.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="glo_body">
        <div class="body_boxer">
            <div class="boxer_headline4">
                Payment details
            </div>
            <div class="boxer_content4">
                
                <table>
                    <tr>
                        <td width="180">Membership type:</td>
                        <td><div class="graypaayment"><%=ViewBag.Nmae %></div></td>
                    </tr>
                    <tr>
                        <td>Membership fee:</td>
                        <td><div class="graypaayment"><%=ViewBag.Fee %>$</div></td>
                    </tr>
                    <tr>
                        <td colspan="2">Expiry date: <%=ViewBag.ExpireDate%></td>
                    </tr>
                    <tr>
                        <td colspan="2">Select payment options</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="radio" name="rdoPayment" checked="checked" /> Paypal<br />
                            <input type="radio" name="rdoPayment" /> 1111<br />
                            <input type="radio" name="rdoPayment" /> 2222<br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                                <input type="button" class="btnpayment" value="Submit"/>&nbsp;&nbsp;<a href="/Company/CompanyIndex?companyid=<%=Model.UserID %>">I will pay later</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%var type = Request.QueryString["companytype"]; %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".btnpayment").click(function () {
            $.postJSON('<%=Url.Action("PayMent") %>', { companyid: <%=Model.UserID %>,companytype:<%=type %> }, function (data) {
                alertbox("PayMent Successful,now you can edit you job ads");
                return false;
            });
            
        });


    });
</script>
</asp:Content>
