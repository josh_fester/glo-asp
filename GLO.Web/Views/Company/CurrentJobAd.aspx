﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CurrentJobAd
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <%--<script type="text/javascript" src="/Js/jquery-latest.js"></script> --%>
      <script type="text/javascript" src="/Js/jquery.tablesorter.js"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_list2">
                	
                    <div id="ActiveInfo" class="list_info2">
                       
                        <div class="info_order_area"></div>
                        
                        <div class="info_result_container" style="height:600px;">
                	        <table id="myTable" class="tborder">
                                 <thead> 
                                    <tr> 
                                        <th width="30" class="list_info_head" width="20">Slot</th> 
                                        <th width="145" class="list_info_head" style="text-align:left">&nbsp;&nbsp;Job Title</th>
                                        <th width="60" class="list_info_head">Job ID</th>
                                        <th width="75" class="list_info_head head_last">Expire Date</th>
                                    </tr> 
                                 </thead>
                                <% int i = 1; 
                                    foreach (var job in Model.GLO_Company.GLO_CompanyJob)
                                   {
                                       if (job.IsDelete == false && job.IsActive == true )
                                       {  %>     
                                    <tr class="<%=job.IsPublish?"job_active":"" %> <%=i==1?"message_active":"" %>">
                                        <td width="20"><%=i%></td>
                                        <td>
                                            <a class="JobTitle" x:jobid="<%=job.JobID %>" href="javascript:void(0)"><%=job.JobTitle%> </a>
                                        </td>
                                        <td width="50">
                                             <%=job.JobNO%>
                                        </td>
                                        <td width="65">
                                         <%= string.Format("{0:yyyy-MM-dd}", job.ExpireDate)%>
                                        </td>
                                    </tr>                                            
                                <%i++;
                                       }
                                   }%>
                            </table>
                        </div>


                    </div>
                             
                	
                </div>
                <div class="board_content">
                    <div class="content_info" style="margin-top:35px;height:580px;">
                         <% if(Model.GLO_Company.GLO_CompanyJob.FirstOrDefault(x=>x.IsActive) != null)
                                Html.RenderPartial("~/Views/Shared/Company/JobDetailManage.ascx", Model.GLO_Company.GLO_CompanyJob.FirstOrDefault(x => x.IsActive));
                         %>
                    </div>
                </div>
                
            </div>
        </div>
 </div>
 <input type="hidden" id="HidJob" value="Jobslot" />
<script type="text/javascript">
    $(document).ready(function () {
        $("#myTable").tablesorter(); 
        $(".list_info_head").click(function () {
            $(".list_info_head").removeClass("head_active");
            $(this).addClass("head_active");
        });
        $(".JobTitle").click(function () {
            $(".JobTitle").parent().parent().removeClass("message_active");
            $(this).parent().parent().addClass("message_active");

            $.getJSON('_GetJobDetailManage', { JobID: $(this).attr("x:jobid"), random: Math.random() }, function (data) {
                {
                    $('.content_info').html(data.Html);
                }
            });
        });


        $("#ActiveTitle").click(function () {
            $("#ActiveInfo").show();
            $("#ArchiveInfo").hide();
            $("#ArchiveTitle").css("border-top", "1px solid #ABAEB7");
        });
        $("#ArchiveTitle").click(function () {
            $("#ActiveInfo").hide();
            $("#ArchiveInfo").show();
            $("#ArchiveTitle").css("border-top", "0px solid #ABAEB7");
        });


    });
</script>

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
