﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   <%=Model.NickName %>'s fact sheet
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <% var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
      bool showRequestRecommendButton = true;
      foreach (var recommendUser in Model.GLO_Recommendation1)
      {
          if (recommendUser.UserID.ToString() == user.UserData.UserID.ToString())
          {
              showRequestRecommendButton = false;
          }
      }
      if(user.UserData.UserType ==4)
          showRequestRecommendButton = false;
      %>
      
    <div class="glo_manage">
    	<div class="nav_company nav_company_factsheet">
        	<ul>
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li class="active"><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li ><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsCompanyOwner"].ToString())? Html.ActionLink("Our Dashboard", "DashBoard", new { companyid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>       
        <div class="manage_boxer2"> 
            <div class="boxer_user">     
            
                <div class=" <%=Model.IsPublish ? "box_user_private_on_view2" : "box_user_private_view2"%>  "></div> 
                <div class="box_user_wrap">                 		
                <div class="box_user_photo2">            
                	<img src="/images/<%=!string.IsNullOrEmpty(Model.GLO_Company.LogoUrl)? Model.GLO_Company.LogoUrl : "user_none.gif" %>">
                </div>
                <%if (!bool.Parse(ViewData["IsCompanyOwner"].ToString()))
                      { 
                    %>
                 <div class="box_user_button">
                 <%if (user.UserData.UserType != 3)
                   { %>
                  <%if (!bool.Parse(ViewBag.ShowTagButton.ToString()))
                    {%>
                            <input type="button" class="graybtn" value="Tag" onclick="return false;" />
                        <% }
                    else
                    {%>
                            <input type="button" id="TagUser" class="bluebtn" value="Tag" />
                        <%} %>   
                    <%if (!bool.Parse(ViewBag.ShowRequestTagButton.ToString()))
                      {%>
                <input type="button" class="graybtn" value="Request Tag" onclick="return false;" />
                    <% }
                      else
                      {%>
                    <input type="button" id="RequestTag" class="bluebtn" value="Request Tag"  onclick="RequestTag('<%=Model.UserID %>')"/>
                    <%} %>   
                    <%if (user.UserData.UserType != 3)
                      { 
                          if (showRequestRecommendButton)
                {%>
                    <input type="button" id="RequestRecommend" class="bluebtn" value="Request referral" />
                    
                <% }
                 else
                {%> 
                <input type="button" class="graybtn" value="Request referral" onclick="return false;" />
                <%}%>
                    <%} %>
                    <%} %>
                    <input type="button" id="SendMessage" class="bluebtn" value="Send Message">
                </div>                 
                <%} %>
                </div> 
                <div class="box_user_name"><%=Model.GLO_Company.CompanyName %></div>
                <div class="box_user_note"><%=string.IsNullOrEmpty(Model.GLO_Company.CompanyIntroduce)?"":Model.GLO_Company.CompanyIntroduce.Replace("\n", "<br>") %></div>
                <div class="box_user_value">
                    <div>My organization’s values:</div>
                    <div class="box_company_value_custom">
                        <%if (!string.IsNullOrEmpty(Model.GLO_Company.CompanyValue1))
                          {
                              
                              foreach (var value in Model.GLO_Company.CompanyValue1.Split(';'))
                              {
                                  if (!string.IsNullOrEmpty(value))
                                  {%>
                             <div class="value_item_active font"><%=value%></div>
                        
                        <%}
                              }
                          } %>
                    </div>
                </div>
            
        	</div>
           <div class="boxer_info boxer_company_sheet">
                <div class="info_company">

                        <div class="info_headline">Location</div>
                        <div class="info_description font"><%= Model.GLO_UserLocation.Count > 0 ? Model.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName : ""%></div>

                        <div class="info_headline">Industry</div>
                        <div class="info_description font"><%=Model.GLO_UserIndustry.Count > 0 ? Model.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName : ""%></div>
                                    
                        <div class="info_headline">Employees</div>
                        <div class="info_description font"><%=Model.GLO_Company.Size %></div>

                        <div class="info_headline">Revenue</div>
                        <div class="info_description font"><%=Model.GLO_Company.Revenue %></div>

                        <div class="info_headline">Awards</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.GLO_Company.Awards) ? Model.GLO_Company.Awards.Replace("\n","<br>") : ""%></div>
                                                                      
                        <div class="info_headline">Contact Us</div>
                        <div class="info_description font"><%=!string.IsNullOrEmpty(Model.GLO_Company.ContactInfo) ? Model.GLO_Company.ContactInfo.Replace("\n","<br>") : ""%></div>

                        <div class="info_headline">Website</div>
                        <div class="info_description font"><%=(!string.IsNullOrEmpty(Model.GLO_Company.Website)&&Model.GLO_Company.Website!="http://")? string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", Model.GLO_Company.Website, Model.GLO_Company.Website):""%></div>
                            
                        <div class="info_contact">
                        <table class="tbcontact">
                            <tr>
                                <td>
                                    <div class="contact_box">
                                        <img src="/Images/<%=!string.IsNullOrEmpty(Model.GLO_Company.CPerson1Image)?Model.GLO_Company.CPerson1Image:"user_none.gif" %>" />
                                    </div>
                                </td>
                                <td valign="bottom">
                                    <%if (!string.IsNullOrEmpty(Model.GLO_Company.CPerson1) || !string.IsNullOrEmpty(Model.GLO_Company.CPerson1Email))
                                      { %>
                                            <div class="contact_line font"><%=Model.GLO_Company.CPerson1%></div>
                                            <div class="contact_line font"><a href="mailto:<%=Model.GLO_Company.CPerson1Email %>"><%=Model.GLO_Company.CPerson1Email%></a></div>                                    
                                      <%} %>
                                </td>
                                <td>
                                    <div class="contact_box">
                                        <img src="/Images/<%=!string.IsNullOrEmpty(Model.GLO_Company.Cperson2Image)?Model.GLO_Company.Cperson2Image:"user_none.gif" %>" />
                                    </div>
                                </td>
                                <td valign="bottom">
                                    <%if (!string.IsNullOrEmpty(Model.GLO_Company.CPerson2) || !string.IsNullOrEmpty(Model.GLO_Company.CPersom2Email))
                                      { %>
                                            <div class="contact_line font"><%=Model.GLO_Company.CPerson2 %></div>
                                            <div class="contact_line font"><a href="mailto:<%=Model.GLO_Company.CPersom2Email %>"><%=Model.GLO_Company.CPersom2Email %></a></div>
                                     <%} %>
                                </td>
                            </tr>
                        </table>
                        </div>
                          


                </div>                    
        	</div>
            
        </div>
        <div class="manage_footer2"></div>    
        
        <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          {%>  
            <%Html.RenderPartial("~/Views/Shared/Company/CompanyJobWidget.ascx", Model.GLO_Company); %>
         <%} %>

         <div class="me_edit">
          <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
            { %>
         <a href="javascript:void(0)" id="factsheet_edit"><img src="/Styles/images/icon_edit.gif" /></a>
         <%} %>
         </div>
    </div>
  <script type="text/javascript">
       var BasicInfoStatus='<%= ViewBag.BasicInfoStatus.ToString() %>';
     var FactSheetStatus='<%= ViewBag.FactSheetStatus.ToString() %>';

    $(document).ready(function () {
       
         $("#factsheet_edit").click(function(){
            $.getJSON('_FactSheetEdit', { "companyid":<%=Model.UserID %>, random: Math.random() }, function (data) {               
                $("#lightboxwrapper").attr("style", "display:block");
                 $('#lightboxwrapper').empty().html(data.Html);
             }); 
        });

         <%if (bool.Parse(ViewData["IsCompanyOwner"].ToString()))
          {%>  
         if(FactSheetStatus=="False")
          {
               $("#factsheet_edit").click();
          }
          <%} %>
        $(".contact_box").hover(function () {
            $(this).find(".contact_window").show();
        }, function () {
            $(this).find(".contact_window").hide();
        });

         $("#SendMessage").click(function () {
                    $.getJSON('<%=Url.Action("SendMessageToUser","Message") %>', { id: '<%=Model.UserID %>', random: Math.random() }, function (data) {
                        $('#lightboxwrapper').empty().html(data.Html);
                        $("#lightboxwrapper").attr("style", "display:block");
                    });
                });
                
         $("#TagUser").click(function () {
            $.getJSON('<%=Url.Action("GetTagCategory","Tag") %>', {id:'<%=Model.UserID %>', random: Math.random() }, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        
        $("#RequestRecommend").click(function () {
            $.postJSON('<%=Url.Action("RequestRecommend","Expert") %>', { requestUserID: '<%=Model.UserID %>' }, function (data) {
                if (data == "True") {
                    alertbox("Your request has been successfully sent!");
                }
                else {
                    alertbox(data);
                }
            });
        });
    });
    
function RequestTag(id) {
    $.post('<%=Url.Action("RequestTag","Tag") %>', { requestUserID: id }, function (data) {
        if (data) {
            alertbox("Your request has been successfully sent!");
        }
    });
}
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">        
</div>
</asp:Content>