﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    TagExpertList
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>             
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	  <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>
                <div class="board_list2">
                        <div class="list_select_head">
                                <div class="list_select_box" x:functionid="-1">
            	                    <div class="list_select_text">By Function</div>
				                    <div class="list_select_arrow"></div>           	                   
                                </div>
                        </div>

                        <div class="list_select_result">
                            <table>
                            <%int mark = 1; 
                              foreach (var tagcategory in Model.GLO_TagCategory)
                              { %>
                                <tr class="<%=mark==1?"message_active":"" %>">
                                    <td>
                                        <a class="taglist" x:categoryid="<%=tagcategory.TagCategoryID %>" href="javascript:void(0);"><%= tagcategory.GLO_Functions.FunctionName%></a>
                                    </td>
                                    <td width="100">
                                         <%if(tagcategory.GLO_Functions.IsDefaultValue==0){ %>
                                            <a href="javascript:void(0)" class="function_edit" x:functionid="<%=tagcategory.GLO_Functions.FunctionID %>">Edit</a>
                                            |
                                            <a href="javascript:void(0)" class="function_delete" x:categoryid="<%=tagcategory.TagCategoryID %>">Delete</a>
                                         <%} else{%>
                                            &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<a href="javascript:void(0)" class="function_delete" x:categoryid="<%=tagcategory.TagCategoryID %>">Delete</a>
                                        <%} %>                                             
                                    </td>
                                </tr>
                            <%mark++;
                              } %>     
                            </table>                      
             
                                              
                        </div>
                </div>
               
                <div class="board_content">
                	<div class="content_info">
                     
                           <%Html.RenderPartial("~/Views/Shared/Company/TagExpertList.ascx", Model.GLO_TagCategory.FirstOrDefault() == null ? new List<GLO.Data.Models.GLO_TagUsers>() : Model.GLO_TagCategory.FirstOrDefault().GLO_TagUsers.ToList());%>
                      
                    
                    </div>
                </div>
               
                
            </div>
        </div>
 </div>
 
<script type="text/javascript">
    $(document).ready(function () {

        $(".taglist").click(function () {
            $(".taglist").parent().parent().removeClass('message_active');
            $(this).parent().parent().addClass('message_active');

            $.getJSON('_TagUserDetail', { "categoryid": $(this).attr("x:categoryid"), "type": 2, random: Math.random() }, function (data) {
                $('.content_info').empty().html(data.Html);
            });
        });

        $(".function_edit,.list_select_box").click(function () {
            $.getJSON('/Tag/_TagFunctionEdit', { "functionid": $(this).attr("x:functionid"), random: Math.random() }, function (data) {
                $('#lightboxwrapper').empty().html(data.Html);
                $("#lightboxwrapper").attr("style", "display:block");
            });
        });
        $(".function_delete").click(function () {
            var CategoryID = $(this).attr("x:categoryid");
            confirmbox('Do you want to delete this functiion?', function () {
                $.postJSONnoQS('<%=Url.Action("DeleteTagCategory","Tag") %>', { "categoryid": CategoryID }, function (data) {
                    if (data) {
                        alertbox("The function has been deleted successfully.", function () {
                            window.location.reload();
                        });
                        return false;
                    }
                    else {
                        alertbox("There are users under the function.Please remove these users first.");
                        return false;
                    }
                });
            });
        });


    });
</script>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
