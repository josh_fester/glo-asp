﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ReceiveEmail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>
                 <li><a href="<%=Url.Action("CompanyIndex","Company",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet","Company",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob","Company",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Company",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%>                    
                </div>
               
                <div class="board_right">
                <div class="box_form">
                <br />
                <table>
                        <tr>                            
                            <td>
                                <input type="checkbox" id="chkReceiveEmail" <%=Model.ReceiveEmail?"checked=\"true\"":""%> /> Yes, please send me email alerts and keep me up to date on GLO thought leadership events.
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="red">If you do not want to be up to date on GLO events, simply unclick the box and you will be removed from GLO’s email list.</span>
                            </td>
                        </tr>
                </table>

                </div>
                </div>
                
            </div>
        </div>
 </div> 
 
 
<script type="text/javascript">
    $(document).ready(function () {
        $('#chkReceiveEmail').click(function () {
            var recEmail = false;
            if($(this).attr("checked"))
            {
                recEmail = true;
            }
            

            confirmbox('Do you want to update setting of receiving email?', function () {
                $.postJSONnoQS('<%=Url.Action("ReceiveEmailChange") %>', { companyid: <%=Model.UserID %>, ReceiveEmail:recEmail }, function (data) {
                    if (data) {
                        alertbox("The setting has been changed successfully.", function () {
                            window.location.reload();
                        });
                        return false;
                    }
                });
            },
                function(){
                    if(recEmail) {
                        $('#chkReceiveEmail').removeAttr("checked");
                    }else {
                        $('#chkReceiveEmail').attr("checked", "true");
                    }
                });
        });
    });
</script>

</asp:Content>