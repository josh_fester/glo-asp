﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DashBoard
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="glo_manage">
    	<div class="nav_expert nav_expert_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { expertid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                 <%Html.RenderPartial("~/Views/Shared/Expert/DashboardMenu.ascx",Model.GLO_HRExpert);%> 
                </div>
                <div class="board_list2">
                	<div id="ActiveTitle" class="list_title">Active</div>
                    <div id="ActiveInfo" class="list_info">
                    	<table>
                        <%  string leftUpper = "";
                            foreach (var assignment in Model.GLO_HRExpert.GLO_ExpertProject.Where(t => t.Status != "Completed" && t.Status != "Cancelled").OrderBy(x => x.ClientName).ToList())
                          { %>
                        	<tr>                                
                                <td width="20"><a href="javascript:void(0)" class="DeleteAssignment" x:projectid="<%=assignment.ProjectID %>"><img src="/Styles/images/icon_delete1.gif" /></a> </td>                            	
                                <td><%=assignment.ClientName.Trim().Substring(0, 1).ToUpper() != leftUpper ? assignment.ClientName.Trim().Substring(0, 1).ToUpper() : "&nbsp;&nbsp;&nbsp;"%> &nbsp;&nbsp;<a href="javascript:void(0)" class="AssignmentDetail" x:projectid="<%=assignment.ProjectID %>"><%=assignment.ClientName.Trim() %></a></td>
                                <td width="65"><%=String.Format("{0:yyyy-MM-dd}",assignment.EndDate) %></td>
                            </tr>
                        <% leftUpper = assignment.ClientName.Trim().Substring(0, 1).ToUpper();
                            } %>
                        </table>
                    </div>
                    <div id="ArchiveTitle" class="list_title">Archive</div>
                    <div id="ArchiveInfo" class="list_info" style="display:none">
                        <table>
                        <%  string leftUpper1 = "";
                            foreach (var assignment in Model.GLO_HRExpert.GLO_ExpertProject.Where(t => t.Status == "Completed" || t.Status == "Cancelled").OrderBy(x => x.ClientName).ToList())
                          { %>
                        	<tr>                                
                                <td width="20"><a href="javascript:void(0)" class="DeleteAssignment" x:projectid="<%=assignment.ProjectID %>"><img src="/Styles/images/icon_delete1.gif" /></a> </td>                            	
                                <td><%=assignment.ClientName.Trim().Substring(0, 1).ToUpper() != leftUpper1 ? assignment.ClientName.Trim().Substring(0, 1).ToUpper() : "&nbsp;&nbsp;&nbsp;"%> &nbsp;&nbsp;<a href="javascript:void(0)" class="AssignmentDetail" x:projectid="<%=assignment.ProjectID %>"><%=assignment.ClientName.Trim() %></a></td>
                                <td width="65"><%=String.Format("{0:yyyy-MM-dd}",assignment.EndDate) %></td>
                            </tr>
                        <% leftUpper1 = assignment.ClientName.Trim().Substring(0, 1).ToUpper();
                            } %>
                        </table>
                    </div>
                    <div class="assignment_add"><a href="javascript:void(0)" id="addAssignment"><img src="/Styles/images/icon_add1.gif" /></a></div>
                </div>
                <div class="board_content">
                	<div class="content_info"></div>
                    <%--<div class="me_apply"><a href="javascript:void(0)" id="editAssignment"><img src="/Styles/images/icon_edit.gif" /></a></div>--%>
                </div>
                
            </div>
        </div>
 </div>

<script type="text/javascript">
    $(document).ready(function () {    
        $(".AssignmentDetail").click(function () {
            $(".list_info tr").removeClass("message_active");
            $(this).parent().parent().addClass("message_active");
            $("#editAssignment").attr("x:projectid", $(this).attr("x:projectid"));
            $.getJSON('_GetAssignmentDetail', { ProjectID: $(this).attr("x:projectid"), random: Math.random() }, function (data) {
                {
                    $('.content_info').html(data.Html);
                }
            });
        });
        $(".DeleteAssignment").click(function () {
            var tr=$(this).parent().parent();
            var proID = $(this).attr("x:projectid");
            
            confirmbox("Do you want to delete the assignment?", function () {         
                $.postJSON('<%=Url.Action("DeleteAssignment") %>', { ProjectID: proID}, function (data) {
                });
                $('.content_info').empty().html("");
                $(tr).remove();
                if($("#ActiveInfo").css("display") == "block")
                {
                    $('#ActiveInfo').find(".AssignmentDetail:eq(0)").click();
                }
                else{
                    $('#ArchiveInfo').find(".AssignmentDetail:eq(0)").click();
                }
                return false;    
            });
        });        
        $("#addAssignment").click(function(){
            $.getJSON('_AssignmentEdit', { "projectid":-1,"expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                    $('#lightboxwrapper').empty().html(data.Html);
                    $("#lightboxwrapper").attr("style", "display:block");
            });
        }); 
        $("#ActiveTitle").click(function(){            
            $("#ActiveInfo").show();      
            $("#ArchiveInfo").hide();
            $("#ArchiveTitle").css("border-top", "1px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ActiveInfo').find(".AssignmentDetail:eq(0)").click();
        });
        $("#ArchiveTitle").click(function(){            
            $("#ActiveInfo").hide();      
            $("#ArchiveInfo").show();
            $("#ArchiveTitle").css("border-top", "0px solid #ABAEB7");
            $('.content_info').empty().html("");
            $('#ArchiveInfo').find(".AssignmentDetail:eq(0)").click();
        });        
        $('#ActiveInfo').find(".AssignmentDetail:eq(0)").click();
    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
        
</div>
</asp:Content>
