﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Web.Models.ChangePasswordModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ChangePassword
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_expert nav_expert_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competence</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { expertid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Expert/DashboardMenu.ascx");%>                  
                </div>
               
                <div class="board_right">
                     <%Html.RenderPartial("~/Views/Shared/ChangePassoword.ascx", Model);%> 
                </div>
                
            </div>
        </div>
        <%: Html.HiddenFor(m=>m.UserID) %>
 </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">

</div>
</asp:Content>
