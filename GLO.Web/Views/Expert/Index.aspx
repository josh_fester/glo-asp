﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExperProfile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>ExperProfile</h2>

<fieldset>
    <legend>GLO_VUsersHRExpert</legend>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.PassWord) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.PassWord) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Email) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Email) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.LogoUrl) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.LogoUrl) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Expr1) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Expr1) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Title) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Title) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Location) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Location) %>
    </div>
    <%foreach (var project in Model.ExpertProject)
      { %>
      <%= project.ProjectName %><br />
    <%} %>
</fieldset>
<p>
    <%: Html.ActionLink("Edit", "Edit", new { /* id=Model.PrimaryKey */ }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
