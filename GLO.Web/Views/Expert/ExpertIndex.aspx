﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ExpertIndex
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID; %>   
    <div id="container">
    	<div class="nav nav_expert nav_expert_how">
        	<ul>
            	<li class="userinfo"></li>
            	<li class="active"><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competence</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { expertid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
        <div class="boxer">        
            <div class="boxer_user">           		
                <div class="box_user_photo">            
                	<img src="/images/<%=Model.GLO_HRExpert.LogoUrl %>">
                </div>
                <div class="box_user_name"><%=Model.GLO_HRExpert.NickName %></div>
                <div class="box_user_title"><%=Model.GLO_HRExpert.Title %></div>
                <div class="box_user_local"><%=Model.GLO_HRExpert.Location %></div>
                <div class="box_expert_note"><%=Model.GLO_HRExpert.MyVision %></div>
                <div class="box_user_value">
                    <div>What values in life and career are important to you:</div>
                    <div class="value_item_active">Respect</div>
                    <div class="value_item_active">Excellence</div>
                    <div class="value_item_active">Family-work balance</div>
                </div>
                <div class="me_edit"><a href="javascript:void(0)" id="editProfile"><img src="/Styles/images/icon_edit.gif" /></a></div>
            
        	</div>
           <div class="boxer_info">
                <%=Model.GLO_HRExpert.MyValue %>
                <div class="me_edit"><a href="javascript:void(0)"><img src="/Styles/images/icon_edit.gif" /></a></div>
        	</div>
        </div>
    </div>
         <script type="text/javascript">
             $(document).ready(function () {
                 <% if(string.IsNullOrEmpty(Model.GLO_HRExpert.NickName)){ %>    
                 $("#lightboxwrapper").attr("style", "display:block");
                 <%} %>               
                 $(function () {
                     $('#file_upload').uploadify({
                         'height': 25,
                         'width': 90,
                         'auto': true,
                         'multi': false,
                         'buttonText': 'Select Photo',
                         'fileTypeExts': '*.gif; *.jpg; *.png',
                         'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                         'uploader': '/Home/Upload',
                         'onUploadSuccess': function (file, data, response) {
                             eval("data=" + data);
                             $("#imageID").attr({ "src": data.Urlpath, "x:src": data.SaveName });
                             //$("#file_upload").attr("style", "display:none;");
                             $("#imageID").attr("x:src", data.SaveName);                    
                         }
                     });
                 });

                 $("#btnSubmit").click(function () {
                   if( $('#NickName').val()=="")
                   {
                      alertbox("Nike name can not empty");
                      return false;
                   }
                     $.postJSON('<%=Url.Action("ExpertUpdate") %>', { "expertid": <%=epxertid %>, "logourl": $("#imageID").attr("x:src"), "username": $('#NickName').val(),"location":$("#Location").val(), "experttitle": $('#Title').val(), "myvision": $('#MyVision').val() }, function (data) {
                         alertbox("Success", function(){
                            window.location.href = '<%=Url.Action("MyValue") %>' + "?expertid=<%=epxertid %>";
                         });
                         return false;
                     });
                 });

                 $("#editProfile").click(function () {
                   
                   $("#lightboxwrapper").attr("style", "display:block");

                 });
                
                
             });

        function ResetPrivate(obj) {
		    if (obj.className == 'box_user_private_on') {
			    obj.className = 'box_user_private';
		    }
		    else {
			    obj.className = 'box_user_private_on';
		    }
	    }		

        function ResetValue(obj) {
                if (obj.className.indexOf('value_item_active')> -1) {
                    obj.className = obj.className.replace('value_item_active', 'value_item');
                }
                else {
                    obj.className = obj.className.replace('value_item', 'value_item_active');
                }
	    }		

        </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
    <div id="lightboxwrapper">
        <div class="lightbox_mask"></div>
	<div class="lightbox_manage"> 
        <div class="lightbox_box1">
            <div class="box_user_private" onclick="ResetPrivate(this);"></div>
            <div class="box_user_photo2">
            <img width="186px" height="186px" id="imageID" src="/Images/<%=Model.GLO_HRExpert.LogoUrl %>"  x:src="<%=Model.GLO_HRExpert.LogoUrl %>" />
                <div class="photo_item_edit" >
                    Drag and drop<br>your photo here<br>or<br>
                <input type="button" id="file_upload" class="btngray" value="Select Photo" />
                </div>
            </div>
            <div class="box_user_detail">
                <input type="text" class="txtbox_user1" id="NickName" value="<%=Model.GLO_HRExpert.NickName %>" />
                <input type="text" class="txtbox_user2" id="Title" value="<%=Model.GLO_HRExpert.Title %>" /> 
                <input type="text" class="txtbox_user2" id="Location" value="<%=Model.GLO_HRExpert.Location %>" />
                <textarea class="txtbox_user3" id="MyVision"><%=Model.GLO_HRExpert.MyVision %></textarea>    
            </div>
            <div class="box_user_value">
                <div>What values in life and career are important to you:</div>
                <div class="value_item" onclick="ResetValue(this);">Integrity</div>
                <div class="value_item" onclick="ResetValue(this);">Respect</div>
                <div class="value_item" onclick="ResetValue(this);">Excellence</div>
                <div class="value_item" onclick="ResetValue(this);">Commitment</div>
                <div class="value_item" onclick="ResetValue(this);">Family-work balance</div>
                <div class="value_item" onclick="ResetValue(this);">Team work</div>
                <div class="value_item" onclick="ResetValue(this);">Family</div>
            </div>
            <div class="me_apply"><a id="btnSubmit" href="javascript:void(0)"><img src="/Styles/images/icon_apply.gif"></a></div>    
        </div>
    </div>
</div>
</asp:Content>
