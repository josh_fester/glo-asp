﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   <%=Model.NickName%>'s personal profile
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="/ckeditor/ckeditor.js" type="text/javascript" ></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID; %>
    <div class="glo_manage">
    <%Html.RenderPartial("~/Views/Shared/UnregisteredUserLogin.ascx");%>
    	<div class="nav_expert nav_expert_how">
        	<ul>
                <li class="userinfo"></li>
            	<li class="active"><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsOwner"].ToString()) ? Html.ActionLink("My Dashboard", "DashBoard", new { expertid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>
        <div class="manage_boxer">        
            <%Html.RenderPartial("~/Views/Shared/Expert/ProfileLeft.ascx",Model.GLO_HRExpert);%> 
           <div class="boxer_info">

                <div class="info_container">
                	<div class="view_value font">
                    <%=HttpUtility.HtmlDecode(Model.GLO_HRExpert.MyValue) %>
                    </div>
                    <div class="upload_boxer">
                    <%if(!string.IsNullOrEmpty(Model.GLO_HRExpert.PPTFileName)){ %>
                        <div style="float:left; margin:5px 0;">For more information.please see the PPT/PDF file:</div> <a href="/Upload/<%=Model.UserID %>/<%=Model.GLO_HRExpert.PPTFileName %>"><%=Model.GLO_HRExpert.PPTFileName %></a>
                        <%} %>
                    </div>
                	<div class="view_filearea">
                        <div class="filearea_item">
                            <%if (!string.IsNullOrEmpty(Model.GLO_HRExpert.ValueImageLeft)) {%><img src="/images/<%=Model.GLO_HRExpert.ValueImageLeft %>"><%} %>
                        </div>                            
                        <div class="filearea_item">
                            <%if (!string.IsNullOrEmpty(Model.GLO_HRExpert.VlaueInageRight)) {%><img src="/images/<%=Model.GLO_HRExpert.VlaueInageRight %>"><%} %>
                        </div>
                    </div>
                </div>
                <div class="me_edit3">
                    <%if (bool.Parse(ViewData["IsOwner"].ToString()))
                      { %>
                <a href="javascript:void(0)" id="editProfile"><img src="/Styles/images/icon_edit.gif" /></a>
                <% }%>
                </div>
        	</div>
        </div>
        <div class="manage_footer"></div>
   </div>
   <input id ="ImageMark" type="hidden" value ="1" />

         <script type="text/javascript">
             var isOwner='<%=ViewData["IsOwner"].ToString() %>';
             var basicInformationStatus='<%= Model.GLO_UserEx.BasicInfoStatus %>';
             var valueStatus='<%= Model.GLO_UserEx.ValueStatus %>';
             var showExplain='<%= Model.GLO_UserEx.ShowExplain %>';
             $(document).ready(function () {                               
                 $("#editProfile").click(function () {                 
                      $.getJSON('_EidtMyValue', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {                            
                              $("#lightboxwrapper").attr("style", "display:block");
                              $('#lightboxwrapper').empty().html(data.Html);
                       });               

                 });
                 

                 $("#editLeftInfo").click(function(){
                   $.getJSON('_EidtLeftInfo', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {                    
                       $("#lightboxwrapper").attr("style", "display:block");
                       $('#lightboxwrapper').empty().html(data.Html);
                    });
                    
                 });
                 if(isOwner=="True")
                 {
                    if(showExplain=="True")
                    {   
                        $("#lightboxwrapper").attr("style", "display:block");
                    }
                    else if(basicInformationStatus=="False")
                    {
                        $("#editLeftInfo").click();
                    }
                    else if(valueStatus=="False"){
                        $("#editProfile").click();
                    }
                }

             });
        </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
    <%Html.RenderPartial("~/Views/Shared/Guidance.ascx");%> 
</div>
</asp:Content>
