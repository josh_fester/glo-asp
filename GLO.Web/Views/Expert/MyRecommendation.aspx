﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Expert.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.NickName%>'s recommendations
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% int epxertid = Model.UserID;
   var recommendlist = Model.GLO_Recommendation;%>
   <div class="glo_manage">
    	<div class="nav_expert nav_expert_recommendation">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competencies</a></li>
            	<li class="active"><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard">
                <%=bool.Parse(ViewData["IsOwner"].ToString()) ? Html.ActionLink("My Dashboard", "DashBoard", new { expertid = Model.UserID }).ToHtmlString() : ""%>
                </li>
            </ul>
        </div>
        <div class="manage_boxer">        
               <%Html.RenderPartial("~/Views/Shared/Expert/ProfileLeft.ascx",Model.GLO_HRExpert);%> 
           <div class="boxer_info">   
                    <div id="divInviteMessage" style="display:none"><%= ViewBag.InviteMessage  %></div>
                    <div class="r_map">
                    <% if (bool.Parse(ViewData["IsOwner"].ToString()) && recommendlist.Count == 0 && Model.GLO_Competencies.Where(x => x.CompetencyType == 1 && x.CompetencyContent != "").Count()==0)
                       { %>
                    <div class="map_note">
                    List your top 6 competencies below and invite up to 8 clients to confirm them, including a short description of how you have made a difference for them or their organization.
                    <br />
                    <br />

                        Click on edit below and get recommended!
                        <br /><br />
                        A sample recommendation is listed in the column on the right.
                    </div>
                    <%} %>
                    <% 
                       int count = 0;
                       foreach(var recommend in recommendlist)
                       { 
                           if (count == 4)
                           { %>
                        
                        <div class="r_user_box">
                            <img src="/Images/<%=!String.IsNullOrEmpty(Model.GLO_HRExpert.LogoUrl)?Model.GLO_HRExpert.LogoUrl:"user_none.gif" %>" />
                        </div>
                        <div class="r_user_box">
                           <a href="javascript:void(0)" x:recommendid="<%=recommend.RecommendID %>" class="RecommendDetail"><img src="/Images/<%=!String.IsNullOrEmpty(recommend.GLO_RecommendUsers.Icon)?recommend.GLO_RecommendUsers.Icon:"user_none.gif" %>" /></a>
                            <div class="r_user_name"><a href="javascript:void(0)" x:recommendid="<%=recommend.RecommendID %>" class="RecommendDetail"><%=recommend.UserName %></a></div>
                        </div>
                        <%}
                           else if (count < 8)
                           { %>
                        <div class="r_user_box">
                           <a href="javascript:void(0)" x:recommendid="<%=recommend.RecommendID %>" class="RecommendDetail"><img src="/Images/<%=!String.IsNullOrEmpty(recommend.GLO_RecommendUsers.Icon)?recommend.GLO_RecommendUsers.Icon:"user_none.gif" %>" /></a>
                            <div class="r_user_name"><a href="javascript:void(0)" x:recommendid="<%=recommend.RecommendID %>" class="RecommendDetail"><%=recommend.UserName %></a></div>
                        </div>
                        <%}  count ++;} %>
                        <%if (count < 8)
                          {
                              for (int i = count; i < 8; i++)
                              {
                                  if (count == 4)
                                  {%>
                               <div class="r_user_box">
                                 <img src="/Images/<%=!String.IsNullOrEmpty(Model.GLO_HRExpert.LogoUrl)?Model.GLO_HRExpert.LogoUrl:"user_none.gif" %>" />
                               </div>
                                <div class="r_user_none"></div>
                                <%}
                                  else
                                  { %>
                            <div class="r_user_none"></div>
                        <%} count++;
                              }
                          } %>

                       
                         
                        <div class="r_invite1"></div>
                        <div class="r_invite2"></div>
                        <div class="r_invite3"></div>
                        <div class="r_invite4"></div>
                        <div class="r_invite5"></div>
                        <div class="r_invite6"></div>
                        <div class="r_invite7"></div>
                        <div class="r_invite8"></div>
                        <div class="competence_top6">
                            <div class="headline_top6">My top 6 competencies:</div>
                            <%if (bool.Parse(ViewData["IsOwner"].ToString())){ %>
                            <div class="edit_top6"><a id="btnAddCompetencies" href="javascript:void(0)">Edit</a></div>
                            <%} %>
                                <div id="showCompetency" class="content_top6">               
                                    <% foreach (var item in Model.GLO_Competencies.Where(x=>x.CompetencyType==1 && x.CompetencyContent!="").OrderBy(x=>x.OrderID).ToList())
                                        {
                                    %>
                                        <div class='value_item_active font' id="<%=item.CompetencyID %>">
                                            <%=item.CompetencyContent%>                                            
                                        </div>
                                    <%
                                        } %>
                                </div>
                        </div>
                    </div>
                    <div class="r_info" id="RecommendDetailView">
                     <%if (recommendlist.FirstOrDefault() != null)
                       { %>
                      <%Html.RenderPartial("~/Views/Shared/Expert/RecommendationDetail.ascx", recommendlist.FirstOrDefault()); %>
                    <%}
                       else
                       { %>
                        <div class="r_info_mask">This is a sample</div>

                        <div class="r_info_name">Steve Switky</div>
                        <div class="r_info_item">How do I like James' work?</div>
                        <div class="r_info_content">
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great1.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                            <img src="/Styles/images/icon_great2.gif" />
                        </div>                      
                        <div class="r_info_item">My confirmation on James' competencies</div>
                        <div class="r_info_content">
                            <div class="value_item_active">business</div>
                            <div class="value_item_active">presentation</div>
                            <div class="value_item">forecasting</div>        
                            <div class="value_item_active">win-win</div>
                            <div class="value_item_active">consumer-center design</div>         
                        </div>
                        <div class="r_info_item">Other competencies James has</div>
                        <div class="r_info_content">
                            <div class="value_item_active">strategy</div>
                            <div class="value_item_active">japanese</div>
                            <div class="value_item_active">spanish</div>        
                            <div class="value_item_active">on-time</div>
                            <div class="value_item_active">detailed</div>      
                            <div class="value_item_active">innovative</div>         
                        </div>
                        <div class="r_info_item">How James makes a difference?</div>
                        <div class="r_info_content font">‘James doesn’t tell me what to do, but speed up my self-awareness discovery process as he guides me to speak and action on what I want to achieve for the company. If you want to be a leader who leads with wisdom, inner peace, and with a strong sense of purpose, James is your “catalyst”.”</div>
                    <%} %>
                    </div>
                <div class="me_edit3">
                  <%if (bool.Parse(ViewData["IsOwner"].ToString()))
                    { %>
                <a href="javascript:void(0)" id="editRecommend"><img src="/Styles/images/icon_edit.gif" /></a>
                <%} %>
                </div>
        	</div>
        </div>
        <div class="manage_footer"></div>
    </div>
     <script type="text/javascript">
             $(document).ready(function () {
                 $("#btnCredential").click(function () {
                     var credential = editor.getData();
                         $.postJSONnoQS('<%=Url.Action("UpdateCredential") %>', { "expertid": <%=epxertid %>, "credential": escape(credential) }, function (data) {
                                
                                  alertbox("Success", function(){
                                       <% if(string.IsNullOrEmpty(Model.GLO_HRExpert.Credential)){ %> 
                                      window.location.href = '<%=Url.Action("RegisterSuccess") %>' + "?expertid=<%=epxertid %>";
                                       <%} else{%>
                                       window.location.reload();
                                       <%} %>
                                   });
                                  return false;
                              });

                 });
                 $("#editLeftInfo").click(function(){
                   $.getJSON('_EidtLeftInfo', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                       $("#lightboxwrapper").attr("style", "display:block");
                       $('#lightboxwrapper').empty().html(data.Html);
                    });                  
                 });
                 $("#editRecommend").click(function(){
                        if($('#showCompetency').find('.value_item_active').length==0)
                        {
                            $("#btnAddCompetencies").click();
                            return false;
                        }
                       $.getJSON('_EidtRecommendation', { "expertid":<%=Model.UserID %>, random: Math.random() }, function (data) {
                       $("#lightboxwrapper").attr("style", "display:block");
                       $('#lightboxwrapper').empty().html(data.Html);
                    });     
                 });

                 $(".RecommendDetail").click(function(){
                       $.getJSON('_RecommendDetail', { "recommendid":$(this).attr("x:recommendid"), random: Math.random() }, function (data) {
                       $('#RecommendDetailView').empty().html(data.Html);
                    });     
                 });
                $("#btnAddCompetencies").click(function(){
                    $.getJSON('<%=Url.Action("_CompetencyEdit","Competence") %>', { userid: <%=Model.UserID %> ,random: Math.random() }, function (data) {
                        $("#lightboxwrapper").attr("style", "display:block");
                        $('#lightboxwrapper').empty().html(data.Html);
                    });                    
                });
             });
        </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="WrapperContent" runat="server">
<div id="lightboxwrapper">
</div>

</asp:Content>
