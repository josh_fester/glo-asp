﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>NewsLetter</title>
    
<style type="text/css">
html,body{margin:0;padding:0;height:100%;}
body{font-family: "Microsoft YaHei",Helvetica;font-size:14px;color:#333333;background:#fff;}
img{border-width:0px;vertical-align:middle;}

.insight_mail_box
{
	position:relative;
	width:820px;
	overflow:hidden;
	margin:0 auto;
	padding:0;
	
	background:#ffffff;
	border: 1px solid #adadad;	
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
	-webkit-box-shadow: #535353 2px 2px 7px, #ccc -2px -2px 5px;
	-moz-box-shadow: #535353 2px 2px 7px, #ccc -2px -2px 5px;
	box-shadow: #535353 2px 2px 7px, #ccc -2px -2px 5px;		
}
.insight_mail_head
{
	position:relative;
	clear:both;
	overflow:hidden;
	padding:30px 6px 6px 45px;
	margin:-1px;
	background:url(http://www.glo-china.com/Styles/images/insight_mail.gif) no-repeat right top;
	
	-webkit-border-radius: 9px 9px 0 0;
	-moz-border-radius:9px 9px 0 0;
	border-radius:9px 9px 0 0;
}
.insight_mail_head img
{
	border:none;
}
.insight_mail_line1
{
	clear:both;
	overflow:hidden;
	margin:0 30px;
	padding:30px 0 25px 0;
}
.insight_mail_line2
{
	clear:both;
	overflow:hidden;
	margin:0 30px;
	padding:30px 0 25px 0;
	background:url("http://www.glo-china.com/Styles/images/line.gif") left top repeat-x;
}
.insight_mail_left1
{
	float:left;
	width:235px;
	padding:5px 10px;	
}
.insight_mail_right1
{
	float:right;
	width:485px;
	padding:5px 10px;	
}
.insight_mail_left2
{
	float:left;
	width:350px;
	padding:5px 10px;	
}
.insight_mail_right2
{
	float:right;
	width:350px;
	padding:5px 10px;	
}
.insight_mail_foot
{
	position:relative;
	clear:both;
	overflow:hidden;
	margin:60px 0 0 0;
	background:#3075ba;
	color:#fff;
	text-align:center;
	
	-webkit-border-radius: 0 0 6px 6px;
	-moz-border-radius: 0 0 6px 6px;
	border-radius: 0 0 6px 6px;
}
.insight_mail_headline
{
	clear:both;
	overflow:hidden;
	padding:15px 0 0 0;
	font-size:16px;
	font-weight:bold;
}
.insight_mail_line1 .insight_mail_headline
{
	padding:3px 0 0 0;
}
.insight_mail_line2 .insight_mail_headline
{
	padding:15px 0 0 0;
}
.insight_mail_box span.blue
{
	color:#2C6DCF;
}
.insight_mail_box span.gray
{
	color:#777;
}
.insight_mail_author
{
	padding:3px 0 0 0;
	clear:both;
	overflow:hidden;
	color:#777;
}
.insight_mail_content
{
	clear:both;
	overflow:hidden;
	padding:15px 0;
}
.insight_mail_button
{
	clear:both;
	overflow:hidden;
	padding:10px 0;
	text-align:right;
}
.insight_mail_button a
{
	display:block;
	float:right;
}
.insight_mail_button input
{
	position:relative;
	background:#616263;
	color:#fff;
	border:none;
	padding:1px 15px 2px 15px;
	font-size:15px;
	
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;	
}
.insight_mail_headline_blue
{
	clear:both;
	overflow:hidden;
	padding:4px 10px;
	color:#fff;
	font-weight:bold;
	font-size:16px;
	background:#3075ba;
}
.insight_mail_headline_gray
{
	clear:both;
	overflow:hidden;
	padding:4px 10px;
	color:#fff;
	font-weight:bold;
	font-size:16px;
	background:#c7c8ca;
}
.insight_mail_image
{
	clear:both;
	overflow:hidden;
}
.insight_mail_date
{
	clear:both;
	overflow:hidden;
	padding:12px 0 0 ;
	font-weight:bold;
}
.insight_mail_note
{
	color:#666;
	font-size:12px;
	padding-bottom:10px;
}
.insight_mail_survey
{
	clear:both;
	overflow:hidden;
}
.insight_mail_survey a
{
	color:#333;
	text-decoration:none;
}
.insight_mail_survey a:hover
{
	color:#333;
	text-decoration:underline;
}
.insight_mail_survey table
{
	width:100%;
	border-collapse:collapse;
}
.insight_mail_survey td
{
	margin:0;
	padding:2px 2px 15px 2px;
}
.insight_mail_survey td.index
{
	width:25px;
	font-weight:bold;
	vertical-align:top;
	color:#777;
}
.insight_mail_survey td.survey
{
	font-weight:bold;
	color:#333;
}
.insight_mail_survey td.survey span
{
	font-size:12px;
	font-weight:normal;
}
.insight_mail_content2
{
	clear:both;
	overflow:hidden;
	padding:15px 0;
	font-weight:bold;
}
.insight_mail_content2 a
{
	color:#333;
	text-decoration:none;
}
.insight_mail_content2 a:hover
{
	color:#333;
	text-decoration:underline;
}
.insight_mail_content2 ul
{
	margin:0;
	padding:0;
	list-style:none;
}
.insight_mail_content2 ul li
{
	margin:0;
	padding:1px 0 3px 20px;	
	background:url("http://www.glo-china.com/Styles/images/icon_dot3.gif") left 8px no-repeat;
}
.rsvp a {
    background: none repeat scroll 0 0 #5E5F61;
    border-radius: 12px 12px 12px 12px;
    box-shadow: 0 0 10px #666768 inset;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    font-size: 14px;
    margin: 0;
    padding: 2px 12px;
    position: relative;
    text-align: center;
    text-decoration: none;
    width: 45px;
}
</style>
</head>
<body class="insightmail">

    <div class="insight_mail_box">
        <div class="insight_mail_head">
            <a href="http://www.glo-china.com/home/index"><img src="http://www.glo-china.com/Styles/images/glo_home.gif" /></a>
        </div>
        <div class="insight_mail_line1">
            <div class="insight_mail_left1">
                <img src="http://www.glo-china.com/Styles/images/insight_mail_article.gif" />
            </div>
            <div class="insight_mail_right1">
            <% GLO.Data.Models.GLO_InsightPosts Post = (GLO.Data.Models.GLO_InsightPosts)ViewBag.TopRecommentContent; %>
                <div class="insight_mail_headline">
                    <%= Post.Title %>
                </div>
                <div class="insight_mail_author">
                    by <%=Post.GLO_Users.NickName %>
                </div>
                <div class="insight_mail_content">
                    <%=HttpUtility.HtmlDecode(Post.PostContent).CTrim(800)%>
                </div>
                <div class="insight_mail_button rsvp" >
                    <a class="rsvp" href="http://www.glo-china.com/Insight/InsightDetail/<%=Post.PostID %>" target="_blank" style="width:70px;">comment</a>
                  
                </div>

            </div>
        </div>
        <div class="insight_mail_line2">
            <div class="insight_mail_left2">
                <div class="insight_mail_headline_blue">Upcoming Events</div>
                <div class="insight_mail_image"><a href="http://www.glo-china.com/news" target="_blank"><img src="http://www.glo-china.com/Styles/images/insight_mail_events.gif" /></a></div>
                <div class="insight_mail_headline">
                    EVENTS <span class="blue">GET TO KNOW GLO</span>
                </div>
                <div class="insight_mail_survey" style="padding-top:10px;">
                 <table align="center">
                    <% foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.CalenderFile)
                       { %>
	                        <tr>
                                <td align="left"><a class="file" href="http://www.glo-china.com/<%=item.FileUrl %>" target="_blank"><%=item.Title %></a></td>
                                <td align="left" class="rsvp"><a class="email rsvp" href="mailto:admin@glo-china.com?subject=Register me for the <%=item.Title.Replace("\"","'") %>&body=Thank you for your interest to join! Please kindly provide following info: Name, Company, Mobile number, Email.">RSVP</a></td>
                            </tr>
                    <%} %>
	                </table>
                </div>
            <%--    <div class="insight_mail_date">
                    30 Aug 2013 <span class="gray">.St Regis Shanghai</span>
                </div>
                <div class="insight_mail_content">
                    How an innovative business model can identity, 
                    access and develop responsible leaders who make 
                    better quality decisions. Better hiring quality 
                    for less or search innovation at work!
                </div>
                <div class="insight_mail_button rsvp">
                <a class="rsvp" href="mailto:admin@glo-china.com?subject=Register me for the &body=Thank you for your interest to join! Please kindly provide following info: Name, Company, Mobile number, Email.">RSVP</a>
                  
                </div>--%>
            </div>
            <div class="insight_mail_right2">
                <div class="insight_mail_headline_gray">Join Our Survey</div>
                <div class="insight_mail_image"><img src="http://www.glo-china.com/Styles/images/insight_mail_survey.gif" /></div>
                <div class="insight_mail_headline">
                    ACTIVE <span class="blue">SURVEY</span>
                </div>
                <div class="insight_mail_note">Make your opinion count</div>
                <div class="insight_mail_survey">
                    <table>
                      <% int i = 1;
                            foreach (var item in (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey)
                            {
                                if (item.SurveyType == 2)
                                { %>
                                 <tr>
                                <td class="index">#<%=i%></td>
                                <td class="survey">
                                    <a href="<%=item.SurveyUrl %>"><%=item.SurveyTitle %></a><br />
                                   <%-- <span class="blue">Participate.</span> <span class="gray">550+ replies in the last 24 hours.</span>--%>
                                </td>
                            </tr>

                         <%i++;
                                }
                            }%>
                    </table>
                </div>
            </div>
        </div>
        <div class="insight_mail_line2">
            <div class="insight_mail_left2">
                <div class="insight_mail_headline_blue">Recommended Reads</div>
                <div class="insight_mail_image"><a href="http://www.glo-china.com/Insight" target="_blank"><img src="http://www.glo-china.com/Styles/images/insight_mail_reads.gif" /></a></div>
               
                <div class="insight_mail_content2">

                    <ul>
                    <% var PostContent = (List<GLO.Data.Models.GLO_InsightPosts>)ViewBag.TopContent;
                       foreach (var item in PostContent)
                       { %>
                        <li><a href="http://www.glo-china.com/Insight/InsightDetail/<%=item.PostID %>" target="_blank"><%=item.Title %></a></li>
                        <%} %>
                    </ul>
                </div>
            </div>
            <div class="insight_mail_right2">
                <div class="insight_mail_headline_gray">Latest GLO Company Members</div>
                <div class="insight_mail_content2">
                <% var companylist = (IList<GLO.Data.Models.GLO_Company>)ViewBag.Company; foreach (var item in companylist)
                   { %>
                    
                    <%=item.GLO_Users.ProfileUrl()%><br/>
                <%} %>
                  
                </div>
            </div>
        </div>
        <div class="insight_mail_foot"><img src="http://www.glo-china.com/Styles/images/glo_bottom.gif" /></div>
    </div>
</body>
</html>
