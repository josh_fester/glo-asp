﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GLO.Data.Models.GLO_InsightPosts>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   Good Leaders Online Insights
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% string UrlLink = "#";
    var surveryreport = (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey;
    if (surveryreport.OrderByDescending(x => x.OrderID).FirstOrDefault(x => x.SurveyType == 1) != null)
    {
        UrlLink = surveryreport.OrderByDescending(x => x.OrderID).FirstOrDefault(x => x.SurveyType == 1).SurveyUrl;
     }
     
     var pageNumber = (int)ViewData["p"];
     var perPage = (int)ViewData["perPage"];
     var RecordCount = (int)ViewData["RecordCount"];
     var PostContent = (List<GLO.Data.Models.GLO_InsightPosts>)ViewBag.TopContent;

     
    %> 
    <div class="glo_body">
        <div class="insight_sns">
            Follow us 
            &nbsp;&nbsp;
            <a class="facebook" href="https://www.facebook.com/goodleadersonline"title="Follow Us On Facebook" target="_blank"><img src="/Styles/images/insight_icon1.gif" /></a>                                   
            <a class="twitter" href="https://twitter.com/GLOChina" title="Follow Us On Twitter" target="_blank" rel="nofollow"> <img src="/Styles/images/insight_icon2.gif" /></a>                                    
            <a class="google" href="https://plus.google.com/u/0/b/113960940744608996000/113960940744608996000/" title="Follow Us On Google+" target="_blank"><img src="/Styles/images/insight_icon3.gif" /></a>
            <a class="linkedin" href="http://www.linkedin.com/company/good-leaders-online-glo-?trk=top_nav_home" title="Follow Us On Linkedin" target="_blank"><img src="/Styles/images/insight_icon4.gif"></a>
            <a class="weibo" href="http://e.weibo.com/3746559600/profile" title="Follow Us On weibo" target="_blank"><img src="/Styles/images/insight_icon6.gif"></a>
        </div>
        <div class="box_insight">
            <div class="insight_left">            
              
                <div class="insight_line">
                    <div class="insight_head">CEO <span class="blue">PICKS</span></div>
                    <div class="insight_image">
                        <img src="/Styles/images/insight_head.png" />
                        <div class="insight_hyperlink">
                            <div class="hyperlink_left">
                                <ul>
                                <%foreach (var content in PostContent)
                                  { %>
                                   <li><a href="/Insight/InsightDetail/<%=content.PostID%>"><%=content.Title %></a></li>
                                <%} %>                                                                   
                                </ul>
                            </div>
                         
                            <div class="hyperlink_right">
                                <a href="/Insight/SearchInsight">read more &gt;</a>
                            </div>
                          
                        </div>
                    </div>
                </div>
                
                <div class="insight_line1">
                    <div class="insight_head">MORE IN <span class="blue">LEADERSHIP DISCUSSIONS</span></div>
                </div>
                <%foreach (var item in Model)
                  {
                      string permalink = HttpUtility.UrlEncode(string.Format("{0}/Insight/InsightDetail/{1}", System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString(),item.PostID.ToString()));
                      string title = HttpUtility.UrlEncode(item.Title);
                      %>
                <div class="insight_line2">
                    <div class="insight_content">
                        <table>
                            <tr>
                                <td class="tname"><a href="/Insight/InsightDetail/<%=item.PostID %>"><%=item.Title %></a></td>
                                <td class="quote">
                                    <a href="/Insight/InsightDetail/<%=item.PostID %>#comment"><img src="/Styles/images/insight_icon5.gif" /><sup><%=item.GLO_InsightPostComment.Count %></sup></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="put"><%=item.DateCreated.Value.ToLongDateString() %> / <%=item.GLO_Users.ProfileUrl() %></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <%= HttpUtility.HtmlDecode(item.PostContent)%>
                              </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="div_iconbox">
                                        <a class="facebook" href="http://www.facebook.com/sharer.php?u=<%=permalink %>&t=<%=title %>" title="Share on Facebook" target="_blank"><img src="/Styles/images/insight_icon1.gif" /><sup><%=GLO.Web.Controllers.FacebookHelper.ShareCount(permalink)%></sup></a>                                   
                                        <a class="twitter" href="http://twitter.com/home?status=<%=permalink %> <%=title %>" title="Share on Twitter" target="_blank" rel="nofollow"> <img src="/Styles/images/insight_icon2.gif" /><sup><%=GLO.Web.Controllers.TwitterHelper.ShareCount(permalink)%></sup></a>                                    
                                        <a class="google" href="https://plus.google.com/share?url=<%=permalink %>&hl=en" title="Share Us On Google+" target="_blank"><img src="/Styles/images/insight_icon3.gif" /><sup><%=GLO.Web.Controllers.GooglePlusHelper.ShareCount(permalink)%></sup></a>
                                        <a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<%=permalink %>&title=<%=title %>" title="Share on Linkedin" target="_blank"><img src="/Styles/images/insight_icon4.gif"><sup><%=GLO.Web.Controllers.LinkInHelper.ShareCount(permalink)%></sup></a>
                                    </div>
                                    <div class="div_starbox" id="action<%=item.PostID %>">
                                        <a href="javascript:void(0)" x:id=<%=item.PostID%> x:count="1" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=item.PostID%> x:count="2" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=item.PostID%> x:count="3" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=item.PostID%> x:count="4" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=item.PostID%> x:count="5" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        &nbsp;
                                        <%if (item.Raters != null)
                                          { %>
                                        Currently rated <%=(int)(item.Rating / (item.Raters))%> by <%=item.Raters%> people
                                        <%}
                                          else
                                          {%>
                                        Be the first one to rate this post
                                        <%} %>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="tag">
                                   <%if (item.GLO_InsightPostTag.Count > 0)
                                     { %>
                                    Tag: <%foreach (var itmetag in item.GLO_InsightPostTag.ToList())
                                           { %>
                                    <%=string.Format("<a href=\"/Insight/SearchInsight?tag={0}\" target=\"_blank\">{1}</a>", itmetag.Tag, itmetag.Tag) + ";"%>
                                    <%}
                                     }%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <%} %>
               
                <div class="insight_page">
                <%= Html.Pager(3, pageNumber, perPage, RecordCount, null, "Index", new { })%>
                </div>

            </div>
            <div class="insight_right">        
                
                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">NEWS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_news.gif" /></div>
                    <div class="insight_news">
                        <ul>
                         <% var eventNews =(List<GLO.Data.Models.GLO_News>)ViewBag.EventsList;
                             foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList)
                           {
                              // if (item.FileType == 3)
                              // {
                                   item.FileUrl = !string.IsNullOrEmpty(item.FileUrl)? item.FileUrl.StartsWith("http") ? item.FileUrl : "http://" + item.FileUrl : "";
                                    %>
                               <li>
                                        <a href="<%=item.FileUrl %>" target="_blank"><%=item.Title %></a>
                                </li>
                            <%// }
                           } %>                           
                        </ul>
                    </div>
                    <%if (eventNews.Count >= 3)
                      { %>
                    <div class="insight_more"><a href="/News">Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">EVENTS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_events.gif" /></div>
                    <div class="insight_events">
                        <table>
                        <% var calenderFile = (List<GLO.Data.Models.GLO_News>)ViewBag.CalenderFile;
                            foreach (var item in calenderFile)
                           { %>
	                            <tr>
                                    <td align="left"><a class="file" href="<%=item.FileUrl.ToUrl() %>" target="_blank"><%=item.Title %></a></td>
                                    <td class="rsvp" align="left"><a class="rsvp" href="mailto:admin@glo-china.com?subject=Register me for the <%=item.Title.Replace("\"","'") %>&body=Thank you for your interest to join! Please kindly provide following info: Name, Company, Mobile number, Email.">RSVP</a></td>
                                </tr>
                           <%} %>                          
                        </table>
                    </div>
                      <%if (calenderFile.Count >= 3)
                        { %>
                    <div class="insight_more"><a href="/News" >Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">GLO <span class="blue">SURVEY</span></div>
                    <div class="insight_image">
                   <img src="/Styles/images/insight_survey.gif" border="0" />
                    </div>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">Existing <span class="blue">Reports</span></div>
                    <div class="insight_note">
                        <%if (UrlLink != "#") {%>
                        <a href="<%=UrlLink%>" target="_blank">How do our community members rate the competencies of wisdom?</a>
                        <%} %>
                    </div>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">Active <span class="blue">Survey</span></div>
                    <div class="insight_note">Make your opinion count</div>
                    <div class="insight_survey">
                        <table>
                         <% int j = 1;
                            foreach (var item in (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey)
                            {
                                if (item.SurveyType == 2 && j<4)
                                { %>

                            <tr>
                                <td class="index">#<%=j%></td>
                                <td class="survey">
                                    <a href="<%=item.SurveyUrl.ToUrl() %>" target="_blank"><%=item.SurveyTitle%></a><br />
                                   <%-- <span class="blue">Participate.</span> <span class="gray">550+ replies in the last 24 hours.</span>--%>
                                </td>
                            </tr>

                         <%j++;
                                }
                            }%>
                            
                           
                        </table>
                    </div>
                    <%if (((List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey).FindAll(x => x.SurveyType == 2).Count > 3)
                      { %>
                    <div class="insight_more"><a href="/News">Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

            </div>
            <div class="insight_fix"></div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            var h1 = $('.insight_left').height();
            var h2 = $('.insight_right').height();
            if (h1 < h2) $('.insight_left').css('height', h2 + 'px');

            $('.ratio_star').hover(function () {
                var postid = $(this).attr("x:id");
                var starcount = $(this).attr("x:count");
                var obj = $('#action' + postid);

                for (i = 0; i < 5; i++) {
                    if (i < starcount) {
                        $(obj).find("img:eq(" + i + ")").attr("src", "/Styles/images/icon_star2.gif");
                    }
                    else {
                        $(obj).find("img:eq(" + i + ")").attr("src", "/Styles/images/icon_star.gif");
                    }
                }

            }, function () {
                $(".ratio_star").find("img").attr("src", "/Styles/images/icon_star.gif");
            });

            $('.ratio_star').click(function () {
                var postid = $(this).attr("x:id");
                var starcount = $(this).attr("x:count");

                $.postJSON('<%=Url.Action("ContentRation") %>', { postid: postid, starcount: starcount }, function (data) {
                    if (data) {
                        alertbox("Your rating has been registered. Thank you!", function () {
                            window.location.reload();
                        });
                     
                      
                    }
                    return false;
                });
            });
        });
    </script>
</asp:Content>