﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Base.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_InsightPosts>" %>

<asp:Content ID="Content4" ContentPlaceHolderID="HeadContent" runat="server">
<meta property="og:title" content="<%=Model.Title %>">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% string UrlLink = "#";
     bool isAdmin = false;
    var surveryreport = (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey;
    if (surveryreport.OrderByDescending(x => x.OrderID).FirstOrDefault(x => x.SurveyType == 1) != null)
    {
        UrlLink = surveryreport.OrderByDescending(x => x.OrderID).FirstOrDefault(x => x.SurveyType == 1).SurveyUrl;
    }
         
    if (Request.IsAuthenticated)
    {
         var user = Context.User as GLO.Web.Controllers.CustomPrincipal<GLO.Web.Controllers.LoginUserInfo>;
         if (user.UserData.UserType == 4)
         {
             isAdmin = true;
         }
     }
     var PostContent = (List<GLO.Data.Models.GLO_InsightPosts>)ViewBag.TopContent;


     string permalink = HttpUtility.UrlEncode(string.Format("{0}/Insight/InsightDetail/{1}", System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString(), Model.PostID.ToString()));
     string title = HttpUtility.UrlEncode(Model.Title);
     
   %>
    <div class="glo_body">
        <div class="insight_sns">
            Follow us 
            &nbsp;&nbsp;
            <a class="facebook" href="https://www.facebook.com/goodleadersonline"title="Follow Us On Facebook" target="blank"><img src="/Styles/images/insight_icon1.gif" /></a>                                   
            <a class="twitter" href="https://twitter.com/GLOChina" title="Follow Us On Twitter" target="_blank" rel="nofollow"> <img src="/Styles/images/insight_icon2.gif" /></a>                                    
            <a class="google" href="https://plus.google.com/u/0/b/113960940744608996000/113960940744608996000/" title="Follow Us On Google+" target="_blank"><img src="/Styles/images/insight_icon3.gif" /></a>
            <a class="linkedin" href="http://www.linkedin.com/company/good-leaders-online-glo-?trk=top_nav_home" target="_blank"><img src="/Styles/images/insight_icon4.gif"></a>
            <a class="weibo" href="http://e.weibo.com/3746559600/profile" title="Follow Us On weibo" target="_blank"><img src="/Styles/images/insight_icon6.gif"></a>
        </div>
        <div class="box_insight">
            <div class="insight_left">            
              
                <div class="insight_line">
                    <div class="insight_head">CEO <span class="blue">PICKS</span></div>
                    <div class="insight_image">
                        <img src="/Styles/images/insight_head.png" />
                        <div class="insight_hyperlink">
                            <div class="hyperlink_left">
                                <ul>
                                  <%foreach (var content in PostContent)
                                  { %>
                                   <li><a href="/Insight/InsightDetail/<%=content.PostID%>"><%=content.Title %></a></li>
                                <%} %>  
                                </ul>
                            </div>
                            <div class="hyperlink_right">
                                <a href="/Insight/SearchInsight">read more &gt;</a>
                            </div>
                        </div>
                    </div>
                
                </div>
                
                <div class="insight_line1">
                    <div class="insight_head">MORE IN <span class="blue">LEADERSHIP DISCUSSIONS</span></div>
                </div>
                
                <div class="insight_line2">
                    <div class="insight_content">
                        <table>
                            <tr>
                                <td class="tname"><%=Model.Title %></td>
                                <td class="quote">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="put"><%=Model.DateCreated.Value.ToLongDateString() %>/ <a href="#"><%=Model.GLO_Users.ProfileUrl()%></a></td>
                            </tr>
                             <tr>
                             <td colspan="2">
                                <%= HttpUtility.HtmlDecode(Model.Description)%>
                              </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <%= HttpUtility.HtmlDecode(Model.PostContent)%>
                              </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="div_iconbox">
                                        <a class="facebook" href="http://www.facebook.com/sharer.php?u=<%=permalink %>&t=<%=title %>" title="Share on Facebook" target="_blank"><img src="/Styles/images/insight_icon1.gif" /><sup><%=GLO.Web.Controllers.FacebookHelper.ShareCount(permalink)%></sup></a>                                   
                                        <a class="twitter" href="http://twitter.com/home?status=<%=permalink %> <%=title %>" title="Share on Twitter" target="_blank" rel="nofollow"> <img src="/Styles/images/insight_icon2.gif" /><sup><%=GLO.Web.Controllers.TwitterHelper.ShareCount(permalink)%></sup></a>                                    
                                        <a class="google" href="https://plus.google.com/share?url=<%=permalink %>&hl=en" title="Share Us On Google+" target="_blank"><img src="/Styles/images/insight_icon3.gif" /><sup><%=GLO.Web.Controllers.GooglePlusHelper.ShareCount(permalink)%></sup></a>
                                        <a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<%=permalink %>&title=<%=title %>" title="Share on Linkedin" target="_blank"><img src="/Styles/images/insight_icon4.gif"><sup><%=GLO.Web.Controllers.LinkInHelper.ShareCount(permalink)%></sup></a>
                                    </div>
                                    <div class="div_starbox" id="action<%=Model.PostID %>">

                                        <a href="javascript:void(0)" x:id=<%=Model.PostID%> x:count="1" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=Model.PostID%> x:count="2" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=Model.PostID%> x:count="3" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>
                                        <a href="javascript:void(0)" x:id=<%=Model.PostID%> x:count="4" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a> 
                                        <a href="javascript:void(0)" x:id=<%=Model.PostID%> x:count="5" class="ratio_star"><img src="/Styles/images/icon_star.gif" /></a>  
                                        &nbsp;
                                        <%if (Model.Raters != null)
                                          { %>
                                        Currently rated <%=(int)(Model.Rating / (Model.Raters))%> by <%=Model.Raters%> people
                                        <%}
                                          else
                                          {%>
                                        Be the first one to rate this post
                                        <%} %>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="tag">
                                    Tag: <%foreach (var itmetag in Model.GLO_InsightPostTag.ToList())
                                           { %>
                                    <%=string.Format("<a href=\"/Insight/SearchInsight?tag={0}\" target=\"_blank\">{1}</a>", itmetag.Tag, itmetag.Tag) + ";"%>
                                    <%} %>
                                </td>
                            </tr>
                        </table>

                        <div id="comment">
                        <%
                            var index = 0;
                            foreach (var itemcoment in Model.GLO_InsightPostComment)
                          { %>

                          <div class="<%=index % 2 == 0 ? "insight_comment" : "insight_comment_alt"%>">
                            <div class="comment_head">
                                <div class="comment_user"><%=itemcoment.GLO_Users.TypeID!=4?itemcoment.GLO_Users.ProfileUrl():itemcoment.Avatar%></div>   
                                <div class="comment_date"><%=itemcoment.CommentDate %></div>   
                            </div>
                            <div class="comment_content">                
                            <%=itemcoment.Comment %>
                            </div>  
                            <div class="comment_delete">
                            <%if (isAdmin)
                              { %>
                                <a href="javascript:void(0);" class="comment_del" x:id=<%=itemcoment.PostCommentID %>><img src="/Styles/images/icon_cancel.gif" /></a>
                                <%} %>
                            </div>
                          </div>

                        <%
                              index++;
                            } %>
                        </div>
                        <div class="insight_answer">
                          Comment<textarea id="post_comment"></textarea><br />
                         
                          <%if (Request.IsAuthenticated)
                                  { %> 
                                  <input id="post_user" value="user" type="hidden" />
                          
                          <%}
                                  else
                                  { %>
                                  Your Name<input type="text" id="post_user" />
                          <%} %>
                        
                          <input type="button" id="comment_create" value="Submit" class="bluebtn2" />
                         </div>
                    </div>
                </div>
                
             

                <div class="insight_back">
                    <a href="/Insight">BACK TO INSIGHTS</a>
                </div>
               

            </div>
            <div class="insight_right">        
                
                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">NEWS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_news.gif" /></div>
                    <div class="insight_news">
                        <ul>
                         <% var eventNews = (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList;
                             foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.EventsList)
                           {
                               //if (item.FileType == 3)
                              // {
                                   item.FileUrl = !string.IsNullOrEmpty(item.FileUrl)? item.FileUrl.StartsWith("http") ? item.FileUrl : "http://" + item.FileUrl : "";
                                    %>
                               <li>
                                        <a href="<%=item.FileUrl %>" target="_blank"><%=item.Title %></a>
                                </li>
                            <%// }
                           } %>                           
                        </ul>
                    </div>
                      <%if (eventNews.Count >= 3)
                        { %>
                    <div class="insight_more"><a href="/News" >Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

                <div class="insight_line">
                    <div class="insight_head">GLO <span class="blue">EVENTS</span></div>
                    <div class="insight_image"><img src="/Styles/images/insight_events.gif" /></div>
                    <div class="insight_events">
                        <table>
                        <%var calenderFile = (List<GLO.Data.Models.GLO_News>)ViewBag.CalenderFile;
                            foreach (var item in (List<GLO.Data.Models.GLO_News>)ViewBag.CalenderFile)
                           { %>
	                            <tr>
                                    <td align="left"><a class="file" href="<%=item.FileUrl %>" target="_blank"><%=item.Title %></a></td>
                                    <td class="rsvp" align="left"><a class="rsvp" href="mailto:admin@glo-china.com?subject=Register me for the <%=item.Title.Replace("\"","'") %>&body=Thank you for your interest to join! Please kindly provide following info: Name, Company, Mobile number, Email.">RSVP</a></td>
                                </tr>
                           <%} %>                          
                        </table>                      
                    </div>
                      <%if (calenderFile.Count >= 3)
                        { %>
                    <div class="insight_more"><a href="/News" >Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">GLO <span class="blue">SURVEY</span></div>
                    <div class="insight_image">
                    <img src="/Styles/images/insight_survey.gif" border="0" />
                    </div>
                </div> 
                
                <div class="insight_line2">
                    <div class="insight_head">Existing <span class="blue">Reports</span></div>
                    <div class="insight_note">
                        <%if (UrlLink != "#") {%>
                        <a href="<%=UrlLink%>" target="_blank">How do our community members rate the competencies of wisdom?</a>
                        <%} %>
                    </div>
                </div> 

                <div class="insight_line2">
                    <div class="insight_head">Active <span class="blue">Survey</span></div>
                    <div class="insight_note">Make your opinion count</div>
                    <div class="insight_survey">
                        <table>
                         <% int j = 1;
                            foreach (var item in (List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey)
                            {
                                if (item.SurveyType == 2 && j<4)
                                { %>

                            <tr>
                                <td class="index">#<%=j%></td>
                                <td class="survey">
                                    <a href="<%=item.SurveyUrl.ToUrl() %>" target="_blank"><%=item.SurveyTitle%></a><br />
                                   <%-- <span class="blue">Participate.</span> <span class="gray">550+ replies in the last 24 hours.</span>--%>
                                </td>
                            </tr>

                         <%j++;
                                }
                            }%>
                            
                           
                        </table>
                    </div>
                    <%if (((List<GLO.Data.Models.GLO_SurveyReport>)ViewBag.Survey).FindAll(x => x.SurveyType == 2).Count > 3)
                      { %>
                    <div class="insight_more"><a href="/News">Read More &gt;&gt;</a></div>
                    <%} %>
                </div> 

            </div>
            <div class="insight_fix"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var h1 = $('.insight_left').height();
            var h2 = $('.insight_right').height();
            if (h1 < h2) $('.insight_left').css('height', h2 + 'px');

            $("#comment_create").click(function () {
                if (ValidateComment() == false)
                    return;
                var postcomment = {
                    'PostID': '<%=Model.PostID %>',
                    'Comment': $("#post_comment").val(),
                    'Avatar': $("#post_user").val()
                };
                $.postJSON('<%=Url.Action("CommentAdd") %>', { comment: $.toJSON(postcomment) }, function (data) {
                    $("#post_comment").val("");
                    $("#post_user").val("");
                    window.location.reload();
                    return false;
                });

            });
            $(".comment_del").click(function () {
                var comid = $(this).attr("x:id");
                confirmbox("Do you want to delete this comment?", function () {
                    $.postJSON('<%=Url.Action("CommentDel") %>', { commentid: comid }, function (data) {
                        if (data) {
                            window.location.reload();
                        }
                        return false;
                    });
                });
               
            });

            $("#post_comment").live("blur", function () {
                if (!isEmpty($(this).val())) $(this).removeClass("input_error");
            });
            $("#post_user").live("blur", function () {
                if (!isEmpty($(this).val())) $(this).removeClass("input_error");
            });



            $('.ratio_star').hover(function () {
                var postid = $(this).attr("x:id");
                var starcount = $(this).attr("x:count");
                var obj = $('#action' + postid);

                for (i = 0; i < 5; i++) {
                    if (i < starcount) {
                        $(obj).find("img:eq(" + i + ")").attr("src", "/Styles/images/icon_star2.gif");
                    }
                    else {
                        $(obj).find("img:eq(" + i + ")").attr("src", "/Styles/images/icon_star.gif");
                    }
                }

            }, function () {
                $(".ratio_star").find("img").attr("src", "/Styles/images/icon_star.gif");
            });

            $('.ratio_star').click(function () {
                var postid = $(this).attr("x:id");
                var starcount = $(this).attr("x:count");
                $.postJSON('<%=Url.Action("ContentRation") %>', { postid: postid, starcount: starcount }, function (data) {
                    if (data) {
                        alertbox("Your rating has been registered. Thank you!", function () {
                            window.location.reload();
                        });
                    }
                    return false;
                });

            });

        });
        function ValidateComment() {
            var error = new Array();
            if (isEmpty($("#post_comment").val())) {
                $("#post_comment").addClass("input_error");
                error.push("error")
            }
            else {
                $("#post_comment").removeClass("input_error");
            }

            if (isEmpty($("#post_user").val())) {
                $("#post_user").addClass("input_error");
                error.push("error")
            }
            else {
                $("#post_user").removeClass("input_error");
            } 
            if (error.length > 0)
                return false;
            else
                return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="KeyWordContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
