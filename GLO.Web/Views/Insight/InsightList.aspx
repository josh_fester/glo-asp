﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderInsightList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% string classMenu = "nav_leader nav_leader_dashboard";
   if (Model.TypeID == 2)
       classMenu = "nav_expert nav_expert_dashboard";
    else if(Model.TypeID ==3)
       classMenu = "nav_company nav_company_dashboard";%>
<div class="glo_manage">
    	<div class="<%=classMenu%>">
        	<ul>
            <%if (Model.TypeID == 1)
              { %>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index","Leader",new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence","Leader", new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation","Leader",new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Leader",new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            <%}
              else if (Model.TypeID == 2)
              { %>
               <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("MyValue",new { expertid = Model.UserID }) %>">How I Add Value</a></li>
            	<li><a href="<%=Url.Action("MyCredential",new { expertid = Model.UserID }) %>">My Credentials</a></li>
            	<li><a href="<%=Url.Action("MyCompetence",new { expertid = Model.UserID }) %>">My Competence</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation",new { expertid = Model.UserID }) %>">Recommendations</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { expertid = Model.UserID }) %>">My Dashboard</a></li>
            <%}
              else if (Model.TypeID == 3)
              {%>
                <li><a href="<%=Url.Action("CompanyIndex",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            <%} %>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                <%if (Model.TypeID == 1)
                      Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx",Model.GLO_Leader);
                  else if (Model.TypeID == 2)
                      Html.RenderPartial("~/Views/Shared/Expert/DashboardMenu.ascx",Model.GLO_HRExpert);
                  else if(Model.TypeID==3)
                      Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);
                   %>
                 
                </div>
             <div class="board_list1">
                <div class="box_insight">
                    <h2> <%=Html.ActionLink("New Comment", "InsightCreate", new { userid = Model.UserID })%></h2>
                
                    <table>
                        <tr>
                            <th class="txtleft">Title</th>
                            <th class="th_action">Action</th>
                        </tr>
                        <%foreach (var content in Model.GLO_InsightPosts.ToList())
                          { %>
                        <tr>
                            <td><a href="/Insight/InsightDetail/<%=content.PostID %>" ><%= content.Title %></a></td>
                            <td class="txtcenter"><%: Html.ActionLink("Edit", "InsightContentEdit", new { id = content.PostID },null)%> |  <%: Html.ActionLink("Delete", "InsightContentDel", new { id = content.PostID }, new { onclick = "return HpylinkAction(this, 'Do you want to delete this item？')" })%></td>
                        </tr>
                        <%} %>
                    </table>
                

                </div>
                </div>
                
            </div>
        </div>
 </div>
 <script type="text/javascript">
     $(document).ready(function () {
         $('#HyperSearch').click(function () {
                var keyword = $("#txt_Search").val();
                window.location.href = "/Insight/SearchInsight?tag=" + keyword;
            });
     };
                </script>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="WrapperContent" runat="server">
</asp:Content>
