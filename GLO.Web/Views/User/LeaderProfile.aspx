﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderProfile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>LeaderProfile</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>GLO_Users</legend>
        <div id="logo">
         <div class="Banner-field BannerImg" id="imageID">
    <img src="" alt=""/>
    </div>
           <div>
    <input type="file" id="file_upload" name="file_upload" />
</div>
<div style="display:none">
    <a href="javascript:$('#file_upload').uploadify('upload');">上传</a>
</div>
    <script type="text/javascript">
        $(function () {
            $('#file_upload').uploadify({
                'auto': true,
                'multi': false,
                'buttonText':'Select File',
                
                'fileTypeDesc': '图片文件',
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'swf': '<%=Url.Content("~/JS/uploadify/uploadify.swf")%>',
                'uploader': '/Home/Upload',
                'onUploadSuccess': function (file, data, response) {
                    eval("data=" + data);

                    $("#imageID").empty().append($("<img />").attr({ "src": data.Urlpath, "x:src": data.Urlpath }));
                    $("#Image").val(data.Urlpath);
                }


            });
        });
    </script>
        
        </div>
        <%=Model.Title%>
         <div class="editor-field">
       <input type="text" value="<%=Model.User.UserName %>" name="Title" id="Title" class="text-box single-line"/>
       </div>
        <div class="editor-field">
       <input type="text" value="<%=Model.User.Email %>" name="Title" id="Text1" class="text-box single-line"/>
       </div>
        <div class="editor-field">
       <input type="text" value="<%=Model.User.TypeID %>" name="Title" id="Text2" class="text-box single-line"/>
       </div>

     <%--   <%: Html.HiddenFor(model => model.UserID) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.UserName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.UserName) %>
            <%: Html.ValidationMessageFor(model => model.UserName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PassWord) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PassWord) %>
            <%: Html.ValidationMessageFor(model => model.PassWord) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.TypeID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.TypeID) %>
            <%: Html.ValidationMessageFor(model => model.TypeID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IndustryID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IndustryID) %>
            <%: Html.ValidationMessageFor(model => model.IndustryID) %>
        </div>--%>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
