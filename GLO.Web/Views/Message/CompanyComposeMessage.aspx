﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Company.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CompanyComposeMessage
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div class="glo_manage">
    	<div class="nav_company nav_company_dashboard">
        	<ul>
                 <li><a href="<%=Url.Action("CompanyIndex","Company",new { companyid = Model.UserID }) %>">Life At</a></li>
            	<li><a href="<%=Url.Action("FactSheet","Company",new { companyid = Model.UserID }) %>">Fact Sheet</a></li>
            	<li><a href="<%=Url.Action("CompanyJob","Company",new { companyid = Model.UserID }) %>">Jobs</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Company",new { companyid = Model.UserID }) %>">Our Dashboard</a></li>
            </ul>
        </div>
      <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Company/DashboardMenu.ascx",Model.GLO_Company);%> 
                </div>

                <div class="board_list1">
                        <%Html.RenderPartial("~/Views/Shared/Message/MessageEdit.ascx", (GLO.Web.Models.MessageModel)ViewBag.SendMessage);%> 
                </div>
               

                <%--<div class="me_apply"><a href="javascript:void(0)" id="btnSend"><img src="/Styles/images/icon_apply.gif"></a></div>--%>
                
            </div>
        </div>
 </div>
<div id="lightboxwrapper">
</div>

</asp:Content>

