﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Message
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="message_table">
        <tr>
            <td class="message_left">
                <div class="message_info message_container">
                <%if (ViewBag.Status == "0")
                  {%>
                    <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count>0)
                        { %>
                        <%Html.RenderPartial("~/Views/Shared/Message/ReceiveMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                    <%}
                        else
                        { %>
                        <div style="text-align:center;">There is no emails in the inbox.</div>
                    <%} 
                  }
                  else if (ViewBag.Status == "1")
                  { %>
                    <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                        { %>
                        <%Html.RenderPartial("~/Views/Shared/Message/SendMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                    <%}
                        else
                        { %>
                        <div style="text-align:center;">There is no emails in the sent box.</div>
                    <%}
                  }
                  else if (ViewBag.Status == "2")
                  { %>
                    <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                         { %>
                        <%Html.RenderPartial("~/Views/Shared/Message/ArchiveMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                    <%}
                         else
                         { %>
                        <div style="text-align:center;">There is no emails in the archive box.</div>
                    <%}
                  }
                  else if (ViewBag.Status == "3")
                  { %>
                    <%if (((List<GLO.Web.Models.MessageModel>)ViewBag.MessageList).Count > 0)
                         { %>
                        <%Html.RenderPartial("~/Views/Shared/Message/TrashMessageList.ascx", (List<GLO.Web.Models.MessageModel>)ViewBag.MessageList);%>
                    <%}
                         else
                         { %>
                        <div style="text-align:center;">There is no emails in the trash box.</div>
                    <%}
                  }%></div>
            </td>
            <td class="message_right">
                <div class="message_content message_container">
                </div>
            </td>
        </tr>
    </table>

<script type="text/javascript">
    $(document).ready(function () {
        $(".message_container").css("height", document.body.clientHeight - 20 + "px");
    });
    $(window).resize(function () {
        $(".message_container").css("height", document.body.clientHeight - 20 + "px");
    });
</script>


<div id="lightboxwrapper">
</div>

<div id="lightboxwrapper2">
    <div class="lightbox_mask"></div>
    <div class="lightbox_manage">
        <div class="lightbox_emailbox">
            <div id="lightbox_sendemail"></div>
            <div class="me_close"><a onclick="document.getElementById('lightboxwrapper2').style.display = 'none';return false;" href="#"><img src="/Styles/images/icon_close.gif"></a></div>
        </div>
    </div>
</div>

</asp:Content>
