﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Leader.Master" Inherits="System.Web.Mvc.ViewPage<GLO.Data.Models.GLO_Users>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeaderComposeMessage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="glo_manage">
    	<div class="nav_leader nav_leader_dashboard">
        	<ul>
                <li class="userinfo"></li>
            	<li><a href="<%=Url.Action("Index","Leader", new { leaderid = Model.UserID }) %>">My Interview</a></li>
            	<li><a href="<%=Url.Action("MyCompetence","Leader", new { leaderid = Model.UserID }) %>">My Competencies</a></li>
            	<li><a href="<%=Url.Action("MyRecommendation","Leader", new { leaderid = Model.UserID }) %>">My Network</a></li>
            	<li class="dashboard"><a href="<%=Url.Action("DashBoard","Leader", new { leaderid = Model.UserID }) %>">My Dashboard</a></li>
            </ul>
        </div>
       <div class="dbboxer">        
            <div class="border_inner">
            	<div class="board_nav">
                	 <%Html.RenderPartial("~/Views/Shared/Leader/DashboardMenu.ascx",Model.GLO_Leader);%>     
                </div>

                <div class="board_list1">
                        <%Html.RenderPartial("~/Views/Shared/Message/MessageEdit.ascx", (GLO.Web.Models.MessageModel)ViewBag.SendMessage);%> 
                </div>
               

                <%--<div class="me_apply"><a href="javascript:void(0)" id="btnSend"><img src="/Styles/images/icon_apply.gif"></a></div>--%>
                
            </div>
        </div>
 </div>
 
<div id="lightboxwrapper">
</div>
</asp:Content>