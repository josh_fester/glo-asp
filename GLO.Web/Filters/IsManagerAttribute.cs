﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using GLO.Services;
using StructureMap;

namespace GLO.Web
{
    public class IsManagerAttribute : ActionFilterAttribute
    {
        //public IAccountService _accountservice{get;set;}

        private IAccountService _accountservice;
        public IsManagerAttribute()
        {
              _accountservice = ObjectFactory.GetInstance<AccountService>();
        }
 
        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (filterContext.Result is ViewResult)
            {
                int inviteUserID = 0;
                if (filterContext.HttpContext.Session["InviteUserID"] != null)
                {
                    inviteUserID = int.Parse(filterContext.HttpContext.Session["InviteUserID"].ToString());
                }
                var loginuser = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"] != null)
                {
                    filterContext.Controller.ViewData["IsOwner"] = false;
                    var expertid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
                    var expert =_accountservice.GetUserByUserID(expertid);
                    if (loginuser != null && loginuser.UserData.UserID == expertid)
                    {
                        filterContext.Controller.ViewData["IsOwner"] = true;
                    }
                    else if ((expert.Approved == false || expert.GLO_UserEx.IsPayMent != 1) && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=unauthorized");
                    }
                    else if (expert.IsLock == true && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=islocked");
                    }
                    else if(inviteUserID!=expertid)
                    {
                        CheckAccessRight(filterContext);
                    }                       
                }
                else if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"] != null)
                {
                    filterContext.Controller.ViewData["IsCompanyOwner"] = false;
                    var companyid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"]);
                    var company = _accountservice.GetUserByUserID(companyid);

                    if (loginuser != null && loginuser.UserData.UserID == companyid)
                    {
                        filterContext.Controller.ViewData["IsCompanyOwner"] = true;
                    }
                    else if ((company.Approved == false || company.GLO_UserEx.IsPayMent != 1) && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=unauthorized");
                    }
                    else if (company.IsLock == true && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=islocked");
                    }
                    else if (company.IsPublish == false && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notpublish");
                    }
                    else if (inviteUserID != companyid)
                    {
                        CheckAccessRight(filterContext);
                    }            
                       
                }
                else if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"] != null)
                {
                   
                    filterContext.Controller.ViewData["IsLeaderOwner"] = false;
                    filterContext.Controller.ViewBag.IsRecommend = false;
                    var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"]);
                    var leader = _accountservice.GetUserByUserID(userid);
                    if (loginuser != null && loginuser.UserData.UserID == userid)
                    {
                        filterContext.Controller.ViewData["IsLeaderOwner"] = true;
                    }
                    else if (leader != null && leader.HideProfile && loginuser!=null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=unauthorized");
                    }
                    else if (leader.Approved == false && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=unauthorized");
                    }
                    else if (leader.IsLock == true && loginuser != null && loginuser.UserData.UserType != 4)
                    {
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=islocked");
                    }
                    else if (loginuser != null&&loginuser.UserData.UserType == 3)
                    {
                        bool shieldcomany = false;
                        string company = loginuser.UserData.UserName;
                        foreach (var keyword in leader.GLO_Leader.GLO_LeaderKeyWord)
                        {
                            if (company.ToUpper().Contains(keyword.KeyWordContent.ToUpper()))
                            {
                                shieldcomany = true;
                                break;
                            }
                        }
                        if (shieldcomany)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=unauthorized");


                    }
                    else if (loginuser != null && loginuser.UserData.UserType == 1)
                    {
                        int notRecommentID = 0;
                        if (filterContext.HttpContext.Session["NotRecommendID"] != null)
                        {
                            notRecommentID = int.Parse(filterContext.HttpContext.Session["NotRecommendID"].ToString());
                        }
                        bool hideButton = notRecommentID > 0 ? true : false;
                        var leaderuser = _accountservice.GetUserByUserID(loginuser.UserData.UserID);
                        bool iRecommendThem = leaderuser.GLO_Recommendation1.Where(x => x.RecommendUserID == loginuser.UserData.UserID && x.UserID == userid).Count() > 0 ? true : false;
                        bool theyRecommendMe = leaderuser.GLO_Recommendation.Where(x => x.UserID == loginuser.UserData.UserID && x.RecommendUserID == userid).Count() > 0 ? true : false;
                        if (!iRecommendThem && !theyRecommendMe && userid != inviteUserID)
                        {
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=forbidden");
                        }
                        else if (hideButton)
                        {
                            filterContext.Controller.ViewBag.IsRecommend = true;
                        }
                    }

                    else if (inviteUserID != userid)
                        CheckAccessRight(filterContext);
                } 
                else
                    CheckAccessRight(filterContext);
            }
        }

        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        { 
            int UserID =0;
            if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"] != null)
                UserID = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
            else if(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"] != null)
                UserID = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"]);
            else if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"] != null)
                UserID = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"]);
            if (UserID != 0)
            {
                var user = _accountservice.GetUserByUserID(UserID);
                if(user.IsDelete)
                    filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notexist");

            }
        }

        public override void OnResultExecuted(System.Web.Mvc.ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);           
        }

        public override void OnResultExecuting(System.Web.Mvc.ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }

        public void CheckAccessRight(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request) != null)
            {
                var loginuser = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                if (loginuser.UserData.UserType != 4 )
                {
                    if(loginuser.UserData.Approved == false)
                        filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notapporved");
                    else if (loginuser.UserData.IsPayMent != 1)
                    {
                        if(loginuser.UserData.UserType !=1)
                            filterContext.HttpContext.Response.Redirect("/Home/NotApproved?type=notpayment");
                    }                    
                }
            }
        }
    }
}