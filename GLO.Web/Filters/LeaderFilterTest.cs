﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using GLO.Services;

namespace GLO.Web
{
    public class LeaderFilterTest : ActionFilterAttribute
    {
        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            //((ViewResult)filterContext.Result).ViewData["IsOwner"] = false;
            if (filterContext.Result is ViewResult)
            {
                if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"] != null)
                {
                    filterContext.Controller.ViewData["IsOwner"] = false;
                    var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["leaderid"]);
                    //filterContext.Controller.ViewData["IsOwner"] = false;
                    if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request) != null)
                    {
                        var user = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                        if (user != null && user.UserData.UserID == userid)
                        {
                            filterContext.Controller.ViewData["IsOwner"] = true;
                            //filterContext.HttpContext.Response.Redirect(string.Format("/Expert/Message?expertid={0}&status=0", user.UserData.UserID));
                        }
                    }
                }
 
            }
        }

        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            
            //if (filterContext.HttpContext.Request.QueryString["k"] == "go")
            //{
            //    string retUrl = filterContext.RouteData.GetRequiredString("controller") + "/" + filterContext.RouteData.GetRequiredString("action");
            //    filterContext.HttpContext.Response.Redirect("http://" + filterContext.HttpContext.Request.Url.Host + ":" + filterContext.HttpContext.Request.Url.Port.ToString() + "/" + retUrl);
            //    //filterContext.Controller.ViewBag.ViewData["ExecutingLogger"] = "正要添加公告，已以写入日志！时间：" + DateTime.Now; 
               
            //}
        }

        public override void OnResultExecuted(System.Web.Mvc.ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            //if (filterContext.Result is ViewResult)
            //{
               // ((ViewResult)filterContext.Result).ViewData["IsOwner"] = false;
               
            //}
        }

        public override void OnResultExecuting(System.Web.Mvc.ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}