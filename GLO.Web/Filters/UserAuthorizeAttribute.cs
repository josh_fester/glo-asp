﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using System.Web.Security;
using System.Net;
using GLO.Services;

namespace GLO.Web
{
    public class LoginFilterAuthorizeAttribute : AuthorizeAttribute
    {
        protected IAccountService _accountservice;
        public LoginFilterAuthorizeAttribute()
        { }
        public LoginFilterAuthorizeAttribute(IAccountService AccountService)
        {
            _accountservice = AccountService;
        }
        public bool IsCheck { get; set; }
        /// <summary>
        /// rewrite
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            //if (filterContext.HttpContext.Request.IsAuthenticated)
            //    filterContext.HttpContext.Response.Redirect(string.Format("/home/index?returnUrl={0}", filterContext.HttpContext.Request.Url.PathAndQuery));
            //throw new Exception(string.Format("{0},sorry you don't have the right to access the page", string.Empty));

            //ger request controller and action
            //var controller = filterContext.RouteData.Values["controller"].ToString();
            //var action = filterContext.RouteData.Values["action"].ToString();
        }
        /// <summary>
        /// rewrite core method,if user is login,check the user from database
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpcontext)
        {
            bool contextresult = false;
            if (!IsCheck)
            {
                contextresult = true;
            }
            if (httpcontext.User.Identity.IsAuthenticated)
            {
                if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(httpcontext.ApplicationInstance.Context.Request) == null)
                {
                    try
                    {
                        //var user = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(httpcontext.ApplicationInstance.Context.Request);
                        var user = _accountservice.GetUserByUserName(httpcontext.User.Identity.Name);
                        if (user == null)
                        {
                            FormsAuthentication.SignOut();
                        }
                    }
                    catch
                    {
                        FormsAuthentication.SignOut();
                        contextresult = false;
                    }
                }
                contextresult = true;
            }

            if (!contextresult)
            {
                httpcontext.Response.StatusCode = 403;
            }
            return contextresult;
        }
        /// <summary>
        /// if autorize fail, then redirect to login page
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            else
            {
                string path = filterContext.HttpContext.Request.Path;
                string strUrl = "/Account/Login?returnUrl={0}";

                filterContext.HttpContext.Response.Redirect(string.Format(strUrl, HttpUtility.UrlEncode(path)), true);

            }
        }
    }
    public class UserTypeFilterAuthorizeAttribute : LoginFilterAuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpcontext)
        {
            bool contextresult = false;
            var user = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(httpcontext.ApplicationInstance.Context.Request);
            if (user != null)
            {
                if (Roles.Equals(user.UserData.UserType.ToString()))
                {
                    return true;
                }
            }
            return contextresult;
        }

    }
}