﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Web.Controllers;
using GLO.Services;
namespace GLO.Web
{
    public class IsOwne11rAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (filterContext.Result is ViewResult)
            {
                //if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"] != null)
                //{
                //    var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
                //    if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request) != null)
                //    {
                //        var user = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                //        if (user != null && user.UserData.UserID != userid)
                //        {
                //            filterContext.Result = new RedirectResult(string.Format("{0}?expertid={1}", filterContext.HttpContext.Request.Url.AbsolutePath, user.UserData.UserID));
                //            //filterContext.HttpContext.Response.Redirect(string.Format("/Expert/Message?expertid={0}&status=0", user.UserData.UserID));
                //        }
                //    }
                //}
                if (CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request) != null)
                {
                    var user = CustomPrincipal<LoginUserInfo>.TryParsePrincipal(filterContext.HttpContext.ApplicationInstance.Context.Request);
                    if (user != null && user.UserData.UserType ==1)
                    {
                        //filterContext.Result = new RedirectResult(string.Format("{0}?expertid={1}", filterContext.HttpContext.Request.Url.AbsolutePath, user.UserData.UserID));
                        if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["userID"] != null)
                        {
                            var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["userID"]);
                            if (userid != user.UserData.UserID)
                                filterContext.HttpContext.Response.Redirect(string.Format("/Leader/DashBoard?leaderid={0}", user.UserData.UserID));
                        }
                        else
                        {
                            filterContext.HttpContext.Response.Redirect(string.Format("/Leader/DashBoard?leaderid={0}", user.UserData.UserID));
                        }
                    }
                    else if (user != null && user.UserData.UserType == 2)
                    {
                        if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"] != null)
                        {
                            var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["expertid"]);
                            if (userid != user.UserData.UserID)
                                filterContext.HttpContext.Response.Redirect(string.Format("/Expert/DashBoard?expertid={0}", user.UserData.UserID));
                        }
                        else
                        {
                            filterContext.HttpContext.Response.Redirect(string.Format("/Expert/DashBoard?expertid={0}", user.UserData.UserID));
                        }
                    }
                    else if (user != null && user.UserData.UserType == 3)
                    {
                        if (filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"] != null)
                        {
                            var userid = int.Parse(filterContext.HttpContext.ApplicationInstance.Request.QueryString["companyid"]);
                            if (userid != user.UserData.UserID)
                                filterContext.HttpContext.Response.Redirect(string.Format("/Company/DashBoard?companyid={0}", user.UserData.UserID));
                        }
                        else
                        {
                            filterContext.HttpContext.Response.Redirect(string.Format("/Company/DashBoard?companyid={0}", user.UserData.UserID));
                        }
                    }
                }

            }
        }
    }
}