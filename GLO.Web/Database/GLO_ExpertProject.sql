CREATE TABLE GLO_ExpertProject ( 
	ProjectID int NOT NULL,
	UserID int NOT NULL,
	ProjectName varchar(100),
	ClientName varchar(100),
	Assignment varchar(100),
	StartDate datetime,
	EndDate date,
	Charge decimal(10,2),
	ProjectRequest varchar(500),
	Status varchar(50),
	Comments varchar(2000)
)
;

ALTER TABLE GLO_ExpertProject ADD CONSTRAINT PK_GLO_ExperProject 
	PRIMARY KEY CLUSTERED (ProjectID)
;












