CREATE TABLE GLO_PollItem ( 
	ItemID int NOT NULL,
	PollID int NOT NULL,
	ChoiceName varchar(50),
	ChoiceCount int
)
;

ALTER TABLE GLO_PollItem ADD CONSTRAINT PK_GLO_PollItem 
	PRIMARY KEY CLUSTERED (ItemID)
;

ALTER TABLE GLO_PollItem ADD CONSTRAINT FK_GLO_PollItem_GLO_PollTopic 
	FOREIGN KEY (PollID) REFERENCES GLO_PollTopic (PollID)
;





