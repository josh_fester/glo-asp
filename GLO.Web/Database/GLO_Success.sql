CREATE TABLE GLO_Success ( 
	SuccessID int NOT NULL,
	Name varchar(100) NOT NULL,
	Content varchar(1000),
	Description text,
	Image varchar(100),
	CreateDate datetime
)
;

ALTER TABLE GLO_Success ADD CONSTRAINT PK_GLO_Success 
	PRIMARY KEY CLUSTERED (SuccessID)
;







