CREATE TABLE GLO_FQAAnswer ( 
	AnswerID int NOT NULL,
	QuestionID int NOT NULL,
	Content nvarchar(1000),
	CreateDate datetime
)
;

ALTER TABLE GLO_FQAAnswer ADD CONSTRAINT PK_GLO_FQAAanswer 
	PRIMARY KEY CLUSTERED (AnswerID)
;





