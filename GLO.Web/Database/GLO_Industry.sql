CREATE TABLE GLO_Industry ( 
	IndustryID int NOT NULL,
	IndustryName varchar(100)
)
;

ALTER TABLE GLO_Industry ADD CONSTRAINT PK_GLO_Industry 
	PRIMARY KEY CLUSTERED (IndustryID)
;



