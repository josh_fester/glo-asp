CREATE TABLE GLO_ExpertCareerSummary ( 
	SummaryID int NOT NULL,
	UserID int NOT NULL,
	StartDate datetime,
	EndDate datetime,
	Title varchar(100),
	CompanyName varchar(50),
	JobDescription text
)
;

ALTER TABLE GLO_ExpertCareerSummary ADD CONSTRAINT PK_GLO_ExpertCareerSummary 
	PRIMARY KEY CLUSTERED (SummaryID, UserID)
;








