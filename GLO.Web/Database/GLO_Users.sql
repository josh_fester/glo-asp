CREATE TABLE GLO_Users ( 
	UserID int NOT NULL,
	UserName nvarchar(50) NOT NULL,
	PassWord nvarchar(100),
	Email varchar(100),
	TypeID int NOT NULL,
	IndustryID int NOT NULL,
	IsLock bit DEFAULT 1 NOT NULL,
	RegisterDate datetime DEFAULT getdate() NOT NULL,
	Approved bit NOT NULL
)
;

ALTER TABLE GLO_Users ADD CONSTRAINT PK_GLO_Users 
	PRIMARY KEY CLUSTERED (UserID)
;

ALTER TABLE GLO_Users ADD CONSTRAINT FK_GLO_Users_GLO_Industry 
	FOREIGN KEY (IndustryID) REFERENCES GLO_Industry (IndustryID)
;

ALTER TABLE GLO_Users ADD CONSTRAINT FK_GLO_Users_GLO_UserType 
	FOREIGN KEY (TypeID) REFERENCES GLO_UserType (TypeID)
;










