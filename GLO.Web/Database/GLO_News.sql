CREATE TABLE GLO_News ( 
	NewsID int NOT NULL,
	CategoryID int NOT NULL,
	Title varchar(100),
	Content text,
	ClickCount int,
	CreateDate datetime
)
;

ALTER TABLE GLO_News ADD CONSTRAINT PK_GLO_News 
	PRIMARY KEY CLUSTERED (NewsID)
;

ALTER TABLE GLO_News ADD CONSTRAINT FK_GLO_News_GLO_NewsCategory 
	FOREIGN KEY (CategoryID) REFERENCES GLO_NewsCategory (CategoryID)
;







