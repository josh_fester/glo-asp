CREATE TABLE GLO_PollTopic ( 
	PollID int NOT NULL,
	PollTopic varchar(100) NOT NULL,
	PollContent varchar(500),
	CreateDate datetime,
	IsActive bit DEFAULT 0
)
;

ALTER TABLE GLO_PollTopic ADD CONSTRAINT PK_GLO_QuickPoll 
	PRIMARY KEY CLUSTERED (PollID)
;






