CREATE TABLE GLO_Message ( 
	MessageID bigint NOT NULL,
	UserID int NOT NULL,
	FromUserID int NOT NULL,
	Content nvarchar(1000),
	CreateDate datetime,
	IsRead bit DEFAULT 0
)
;

ALTER TABLE GLO_Message ADD CONSTRAINT PK_GLO_Message 
	PRIMARY KEY CLUSTERED (MessageID)
;







