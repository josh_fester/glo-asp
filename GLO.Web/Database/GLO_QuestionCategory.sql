CREATE TABLE GLO_QuestionCategory ( 
	CategoryID int NOT NULL,
	CategoryName varchar(100) NOT NULL
)
;

ALTER TABLE GLO_QuestionCategory ADD CONSTRAINT PK_GLO_QuestionCategory 
	PRIMARY KEY CLUSTERED (CategoryID)
;



