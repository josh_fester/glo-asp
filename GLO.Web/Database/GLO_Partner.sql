CREATE TABLE GLO_Partner ( 
	PartenerID int NOT NULL,
	Name varchar(100),
	Content varchar(500),
	Description text,
	Image varchar(50),
	Title varchar(100)
)
;

ALTER TABLE GLO_Partner ADD CONSTRAINT PK_GLO_Partner 
	PRIMARY KEY CLUSTERED (PartenerID)
;







