﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GLO.Web.Controllers
{
    public class SampleController : Controller
    {
        //
        // GET: /Sample/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CompanyIndex()
        {
            return View();
        }
        public ActionResult FactSheet()
        {
            return View();
        }
        public ActionResult CompanyJob()
        {
            return View();
        }
        public ActionResult LeaderIndex()
        {
            return View();
        }
        public ActionResult LeaderCompetence()
        {
            return View();
        }
        public ActionResult LeaderNetwork()
        {
            return View();
        }
        public ActionResult ExpertValue()
        {
            return View();
        }
        public ActionResult ExpertCredential()
        {
            return View();
        }
        public ActionResult ExpertCompetence()
        {
            return View();
        }
        public ActionResult ExpertRecommendation()
        {
            return View();
        }
        public ActionResult Sample1()
        {
            return View();
        }
        public ActionResult Sample2()
        {
            return View();
        }
        public ActionResult Sample3()
        {
            return View();
        }
        public ActionResult Home()
        {
            return View();
        }
    }
}
