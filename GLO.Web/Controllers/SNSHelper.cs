﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Json;
using System.Text;

namespace GLO.Web.Controllers
{
    public static class FacebookHelper
    {
        public static int ShareCount(string permalink)
        {
            int count = 0;
            try
            {
                string jsonString = new System.Net.WebClient().DownloadString("http://graph.facebook.com/?id=" + permalink);
                var json = System.Web.Helpers.Json.Decode(jsonString);

                if (json["shares"] != null)
                {
                    count = json["shares"];
                }
            }
            catch
            { }
            return count;
        }
    }

    public static class TwitterHelper
    {
        public static int ShareCount(string permalink)
        {
            int count = 0;
            try
            {
                string jsonString = new System.Net.WebClient().DownloadString("http://urls.api.twitter.com/1/urls/count.json?url=" + permalink);
                var json = System.Web.Helpers.Json.Decode(jsonString);

                if (json["count"] != null)
                {
                    count = (int)json["count"];
                }
            }
            catch
            { }
            return count;
        }
    }

    public static class GooglePlusHelper
    {
        public static int ShareCount(string permalink)
        {
            int count = 0;
            try
            {
                string googleApiUrl = "https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ";

                string postData = @"[{""method"":""pos.plusones.get"",""id"":""p"",""params"":{""nolog"":true,""id"":""" + permalink + @""",""source"":""widget"",""userId"":""@viewer"",""groupId"":""@self""},""jsonrpc"":""2.0"",""key"":""p"",""apiVersion"":""v1""}]";

                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(googleApiUrl);
                request.Method = "POST";
                request.ContentType = "application/json-rpc";
                request.ContentLength = postData.Length;

                System.IO.Stream writeStream = request.GetRequestStream();
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytes = encoding.GetBytes(postData);
                writeStream.Write(bytes, 0, bytes.Length);
                writeStream.Close();

                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
                System.IO.Stream responseStream = response.GetResponseStream();
                System.IO.StreamReader readStream = new System.IO.StreamReader(responseStream, Encoding.UTF8);
                string jsonString = readStream.ReadToEnd();

                readStream.Close();
                responseStream.Close();
                response.Close();

                var json = System.Web.Helpers.Json.Decode(jsonString);

                try
                {
                    if (json[0]["result"]["metadata"]["globalCounts"]["count"] != null)
                    {
                        count = Int32.Parse(json[0]["result"]["metadata"]["globalCounts"]["count"].ToString().Replace(".0", ""));
                    }
                }
                catch
                { }
            }
            catch
            { }
            return count;
        }
    }

    public static class LinkInHelper
    {
        public static int ShareCount(string permalink)
        {
            int count = 0;
            try
            {
                string jsonString = new System.Net.WebClient().DownloadString("http://www.linkedin.com/countserv/count/share?url=" + permalink + "&format=json");
                var json = System.Web.Helpers.Json.Decode(jsonString);

                if (json["count"] != null)
                {
                    count = (int)json["count"];
                }
            }
            catch { }
            return count;
        }
    }

}