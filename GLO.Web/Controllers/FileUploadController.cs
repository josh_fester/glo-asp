﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using GLO.Web.Common;
using GLO.Services;
using GLO.Data.Models;

namespace GLO.Web.Controllers
{
    public class FileUploadController : Controller
    {
        private ILeaderService _leaderservice;
        private IAccountService _accountservice;
        public FileUploadController(ILeaderService LeaderService,IAccountService AccountService)
         {
             _leaderservice = LeaderService;
             _accountservice = AccountService;
         }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadAssessMent(HttpPostedFileBase fileData,string userid)
        {
            if (string.IsNullOrEmpty(userid))
            {
                return Json(new { Success = false, Message = "please login again！" }, JsonRequestBehavior.AllowGet);
            }
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    string fileExtension = Path.GetExtension(fileName); // extension file name
                    string saveName = Guid.NewGuid().ToString() + fileExtension; //rename file

                     fileData.SaveAs(filePath + saveName);
                    string virtualpathsecured = "/Images/" + saveName;

                    GLO_Leader leader = _accountservice.GetUserByUserID(int.Parse(userid)).GLO_Leader;
                    leader.Assessment = saveName;
                    _leaderservice.LeaderUpdate(leader);

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upload(HttpPostedFileBase fileData,int width,int height)
        {
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    string fileExtension = Path.GetExtension(fileName); // extension file name
                    string saveName = Guid.NewGuid().ToString() + fileExtension; //rename file

                    // fileData.SaveAs(filePath + saveName);
                    string virtualpathsecured = "/Images/" + saveName;
                    ZooAuto.ZoomAuto(fileData.InputStream, filePath + saveName, width, height, "", "");

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
