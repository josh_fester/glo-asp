﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data.Models;
using System.Net;
using System.Text;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;

namespace GLO.Web.Controllers
{
    public class PayMentController :BaseController
    {
        private string DOMAIN = ConfigurationManager.AppSettings["Domain"].ToString();

        private IAccountService _accountservice;

        private ICompanyService _companyservice;

        #region Service
        public PayMentController(IAccountService AccountService, ICompanyService CompanyService)
        {
            _accountservice = AccountService;
            _companyservice = CompanyService;
        }
        #endregion

        #region Payment for expert
        public ActionResult PaySuccess()
        {
            int userid = LoginedUser.UserID;
            var user = _accountservice.GetUserByUserID(userid);
            return View("ExpertPay", user);
        }

        public ActionResult PayMentExpert()
        {
            int userid = LoginedUser.UserID;
            ViewBag.Nmae = "Expert";
            ViewBag.Fee = "300";
            ViewBag.ExpireDate = DateTime.Now.AddYears(1).ToShortDateString();
            var user = _accountservice.GetUserByUserID(userid);
            return View(user);
        }
        public void PayExpertSuccess(int expertid)
        {
            var user = _accountservice.GetUserByUserID(expertid);
            user.ExpireTime = DateTime.Now.AddYears(1);
            _accountservice.UserUpdate(user);

            var userex = user.GLO_UserEx;
            userex.IsPayMent = 1;
            _accountservice.UpdateUserEx(userex);
        }
        #endregion

        #region Payment for company
        public ActionResult PayMentDetail(int companyid, int companytype = 0)
        {
            var user = _accountservice.GetUserByUserID(companyid);
            if(companytype==0)
            companytype = user.GLO_Company.CompanyType;
            var ctype = _companyservice.GetCompanyTypeByID(companytype);
            ViewBag.companytype = companytype;
            ViewBag.Nmae = ctype.CompanyLevel;
            ViewBag.Fee = ctype.Fee;
            ViewBag.ExpireDate = DateTime.Now.AddYears(1).ToShortDateString();         
            return View(user);
        }
        public void CompanyPayMentSucess(int companyid, int companytype)
        {
            var company = _companyservice.GetCompanyByID(companyid);
            int count = _companyservice.GetCompanyTypeByID(companytype).JobCount;
            if (companytype != 8)
            {             
                company.CompanyType = companytype;
                company.ExpireDate = DateTime.Now.AddYears(1);
                _companyservice.CompanyUpdate(company);

                var user = _accountservice.GetUserByUserID(companyid);
                user.ExpireTime = DateTime.Now.AddYears(1);
                _accountservice.UserUpdate(user);

                GLO_UserEx userex = user.GLO_UserEx;
                userex.IsPayMent = 1;
                _accountservice.UpdateUserEx(userex);

                foreach (var job in company.GLO_CompanyJob)
                {
                    if (job.IsPublish == false && string.IsNullOrEmpty(job.JobNO))
                    {
                        job.IsDelete = true;
                        job.IsActive = false;
                        _companyservice.JobEdit(job);
                    }
                    else if (job.IsDelete == false)
                    {
                        job.IsPublish = false;
                        job.IsActive = false;
                        job.IsDelete = false;
                        _companyservice.JobEdit(job);
                    }
                }
            }

            for (int i = 0; i < count; i++)
            {
                GLO_CompanyJob job = new GLO_CompanyJob();
                job.UserID = companyid;
                job.JobTitle = "Job Title";
                job.JobSummary = "";
                job.IsActive = true;
                job.IsPublish = false;
                job.ExpireDate = DateTime.Now.AddYears(1);
                _companyservice.JobAdd(job);
            }

            //var payuser = _accountservice.GetUserByUserID(company.UserID);
            // CookieHelper.SetCookie(payuser);
            // return Json(true);
        }
        #endregion

        public ActionResult PayMentExpertPayPal(int expertid)
        {
            var cname = "Expert payment";
            double money = 300;
            var number = string.Format("{0:yyyy-MM-dd}", DateTime.Now).Replace("-", "") + expertid.ToString();

            GLO_UserPayMent payment = new GLO_UserPayMent();
            payment.UserID = LoginedUser.UserID;
            payment.OrderID = number;
            payment.IsFinish = false;
            payment.IsInvalid = false;
            payment.OrderDate = DateTime.Now;
            payment.PayMentType = 0;
            payment.PayCount = decimal.Parse(money.ToString());
            if (_accountservice.GetPayMentByOrderID(number) == null)
            _accountservice.PayMentAdd(payment);

            return RedirectToAction("PayMentPayPal", new { name = cname, ordernumber = number, price = money });
 
        }
        public ActionResult PayMentCompany(int companyid, int companytype)
        {
            var ctype = _companyservice.GetCompanyTypeByID(companytype);
            var cname = ctype.Organization +" "+ ctype.CompanyLevel;
            double money = double.Parse(ctype.Fee.ToString());
            var number = string.Format("{0:yyyy-MM-dd}",DateTime.Now).Replace("-","") + companyid.ToString()+companytype.ToString();
            
            GLO_UserPayMent payment = new GLO_UserPayMent();
            payment.UserID = LoginedUser.UserID;
            payment.OrderID = number;
            payment.IsFinish = false;
            payment.IsInvalid = false;
            payment.OrderDate = DateTime.Now;
            payment.PayMentType = companytype;
            payment.PayCount = decimal.Parse(money.ToString());
            if(_accountservice.GetPayMentByOrderID(number)==null)
            _accountservice.PayMentAdd(payment);

            return RedirectToAction("PayMentPayPal", new {name=cname,ordernumber=number,price=money });
        }

        #region PayPal
        public ActionResult PayMentPayPal(string name,string ordernumber,double price)
        {
            var websiteUrl = DOMAIN.Replace("http://","");
            //songbai.wan-facilitator@gmail.com
             string orderid = name;
             string depositId = ordernumber;
             double priceTotal = price;

             return Redirect("https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=finance@glo-china.com&item_name=" +
                           orderid + "&item_number=" + depositId.ToString() + "&amount=" + priceTotal.ToString() +
                           "&no_shipping=2&no_note=1&currency_code=USD&invoice=" + depositId.ToString() +
                           "&notify_url=http%3A%2F%2F" + websiteUrl + "/Payment/PayMentNotify&cancel_return=http%3A%2F%2F" + websiteUrl + "/home/index&return=http%3A%2F%2F" + websiteUrl + "/Payment/PayMentSuccess&address_override=1");
        }
        public ActionResult PayMentNotify()
        {
            //Post back to either sandbox or live
            //string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            string strLive = "https://www.paypal.com/cgi-bin/webscr";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strLive);

            //Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            byte[] param = Request.BinaryRead(HttpContext.ApplicationInstance.Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);
            strRequest += "&cmd=_notify-validate";
            req.ContentLength = strRequest.Length;

            //Send the request to PayPal and get the response
            StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            streamOut.Write(strRequest);
            streamOut.Close();
            StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
            string strResponse = streamIn.ReadToEnd();
            streamIn.Close();
            
            //confrim IPN
            if (strResponse == "VERIFIED")
            {

                //check payment status
                if (Request.Form["payment_status"] == "Completed")
                {

                    //检查交易号txn_id 是否已经处理过
                    //Request.Form["txn_id"]
                    //检查receiver_email 是否是您的PayPal 账户中的EMAIL 地址，yourbesiness是商家帐号，对比看有没有被改
                    if (Request.Form["receiver_email"].ToString() == "songbai.wan-facilitator@gmail.com")
                    {
                        //处理这次付款，包括写数据库
                        string strOrderNO = Request.Form["invoice"];//订单号(depositid)


                        GLO_UserPayMent payment = _accountservice.GetPayMentByOrderID(strOrderNO);
                        if (payment != null && payment.IsFinish==false)
                        {
                            payment.IsFinish = true;
                            payment.PayDate = DateTime.Now;
                            _accountservice.PayMentUpdate(payment);

                            if (payment.PayMentType > 0)
                                CompanyPayMentSucess(payment.UserID, payment.PayMentType);
                            else
                                PayExpertSuccess(payment.UserID);
                        }
                        //payment.UserID = 429;
                        //payment.OrderID = "000111" + strOrderNO + "wansong";
                        //payment.IsFinish = true;
                        //payment.IsInvalid = true;
                        //_accountservice.PayMentAdd(payment);
                      
                        // handle order

                        /*********************payment success and update database*************************/
                    }
                }
            }
            return View();
        }
        [Authorize]
        public ActionResult PayMentSuccess()
        {
            var user = _accountservice.GetUserByUserID(LoginedUser.UserID);
            CookieHelper.SetCookie(user);
            //成功后返回的页面
            string strFormValues;
            string strResponse;
            string authToken;
            string txToken;
            string query;
            //定义您的身份标记,这里改成您的身份标记
            authToken = "AjYPUk0Bxi4bbGMBIX2tm7u1eBTaAJD3BnOkWwkKjnZKv.4Gsu3pAD9q";
            //获取PayPal 交易流水号
            txToken = Request.QueryString["tx"];
            // Set the 'Method' property of the 'Webrequest' to 'POST'.
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.sandbox.paypal.com/cgi-bin/webscr");
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            //设置请求参数
            query = "cmd=_notify-synch&tx=" + txToken + "&at=" + authToken;
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] byte1 = encoding.GetBytes(query);
            strFormValues = Encoding.ASCII.GetString(byte1);
            myHttpWebRequest.ContentLength = strFormValues.Length;
            //发送请求
            StreamWriter stOut = new StreamWriter(myHttpWebRequest.GetRequestStream(), System.Text.Encoding.ASCII);
            stOut.Write(strFormValues);
            stOut.Close();
            //接受返回信息
            StreamReader stIn = new StreamReader(myHttpWebRequest.GetResponse().GetResponseStream());
            strResponse = stIn.ReadToEnd();
            stIn.Close();
            //取前面七个字符
            string isSuccess = strResponse.Substring(0, 7);
            string message = "payment successful";// string.Empty;
            if (isSuccess == "SUCCESS")
            {
               // message ="RESPONSE SUCESS\n";
                //Response.Write("item_number：" + Request.QueryString["item_number"].ToString() + "\n");
            }
            else
            {
                //message ="\n response fail";
            }
            ViewBag.Message = message;
            return View();
        }
        #endregion

        #region AliPay
        public ActionResult AliPay()
        {
            ////////////////////////////////////////////请求参数////////////////////////////////////////////

            //支付类型
            string payment_type = "1";
            //必填，不能修改
            //服务器异步通知页面路径
            string notify_url = "http://www.bidbuy.cn/payment/alipay/Alipay_Notify.aspx";
            //需http://格式的完整路径，不能加?id=123这类自定义参数

            //页面跳转同步通知页面路径
            string return_url = "http://www.bidbuy.cn/payment/alipay/Alipay_Return.aspx";
            //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

            //卖家支付宝帐户
            string seller_email = "hanadur@163.com";// WIDseller_email.Text.Trim();
            //必填

            //商户订单号
            string out_trade_no = "20011100233";//WIDout_trade_no.Text.Trim();
            //商户网站订单系统中唯一订单号，必填

            //订单名称
            string subject = "tesing";// WIDsubject.Text.Trim();
            //必填

            //付款金额
            string total_fee = "50";// WIDtotal_fee.Text.Trim();
            //必填

            //订单描述

            string body = "good order";// WIDbody.Text.Trim();
            //商品展示地址
            string show_url = "http://www.bidbuy.cn/";// WIDshow_url.Text.Trim();
            //需以http://开头的完整路径，例如：http://www.xxx.com/myorder.html

            //防钓鱼时间戳
            string anti_phishing_key = "";
            //若要使用请调用类文件submit中的query_timestamp函数

            //客户端的IP地址
            string exter_invoke_ip = "";
            //非局域网的外网IP地址，如：221.0.0.1


            ////////////////////////////////////////////////////////////////////////////////////////////////

            //把请求参数打包成数组
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();
            sParaTemp.Add("partner", Config.Partner);
            sParaTemp.Add("_input_charset", Config.Input_charset.ToLower());
            sParaTemp.Add("service", "create_direct_pay_by_user");
            sParaTemp.Add("payment_type", payment_type);
            sParaTemp.Add("notify_url", notify_url);
            sParaTemp.Add("return_url", return_url);
            sParaTemp.Add("seller_email", seller_email);
            sParaTemp.Add("out_trade_no", out_trade_no);
            sParaTemp.Add("subject", subject);
            sParaTemp.Add("total_fee", total_fee);
            sParaTemp.Add("body", body);
            sParaTemp.Add("show_url", show_url);
            sParaTemp.Add("anti_phishing_key", anti_phishing_key);
            sParaTemp.Add("exter_invoke_ip", exter_invoke_ip);

            //建立请求
            string sHtmlText = Submit.BuildRequest(sParaTemp, "get", "确认");
            Response.Write(sHtmlText);
            return View();
            //return Redirect(sHtmlText);
        }
        public ActionResult AliPayNotify()
        {
            SortedDictionary<string, string> sPara = GetRequestPost();

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                Notify aliNotify = new Notify();
                bool verifyResult = aliNotify.Verify(sPara, Request.Form["notify_id"], Request.Form["sign"]);

                if (verifyResult)//验证成功
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //请在这里加上商户的业务逻辑程序代码


                    //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
                    //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表

                    //商户订单号

                    string out_trade_no = Request.Form["out_trade_no"];

                    //支付宝交易号

                    string trade_no = Request.Form["trade_no"];

                    //交易状态
                    string trade_status = Request.Form["trade_status"];


                    if (Request.Form["trade_status"] == "TRADE_FINISHED")
                    {
                        //判断该笔订单是否在商户网站中已经做过处理
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //如果有做过处理，不执行商户的业务程序

                        //注意：
                        //该种交易状态只在两种情况下出现
                        //1、开通了普通即时到账，买家付款成功后。
                        //2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
                    }
                    else if (Request.Form["trade_status"] == "TRADE_SUCCESS")
                    {
                        //判断该笔订单是否在商户网站中已经做过处理
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //如果有做过处理，不执行商户的业务程序

                        //注意：
                        //该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
                    }
                    else
                    {
                    }

                    //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

                    Response.Write("success");  //请不要修改或删除

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                else//验证失败
                {
                    Response.Write("fail");
                }
            }
            else
            {
                Response.Write("无通知参数");
            }
            return View();
        }
        public ActionResult AliPayReturn()
        {
            SortedDictionary<string, string> sPara = GetRequestGet();

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                Notify aliNotify = new Notify();
                bool verifyResult = aliNotify.Verify(sPara, Request.QueryString["notify_id"], Request.QueryString["sign"]);

                if (verifyResult)//验证成功
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //请在这里加上商户的业务逻辑程序代码


                    //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
                    //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表

                    //商户订单号

                    string out_trade_no = Request.QueryString["out_trade_no"];

                    //支付宝交易号

                    string trade_no = Request.QueryString["trade_no"];

                    //交易状态
                    string trade_status = Request.QueryString["trade_status"];


                    if (Request.QueryString["trade_status"] == "TRADE_FINISHED" || Request.QueryString["trade_status"] == "TRADE_SUCCESS")
                    {
                        //判断该笔订单是否在商户网站中已经做过处理
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //如果有做过处理，不执行商户的业务程序
                    }
                    else
                    {
                        Response.Write("trade_status=" + Request.QueryString["trade_status"]);
                    }

                    //打印页面
                    Response.Write("验证成功<br />");

                    //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                else//验证失败
                {
                    Response.Write("验证失败");
                }
            }
            else
            {
                Response.Write("无返回参数");
            }
            return View();
        }

        /// <summary>
        /// 获取支付宝POST过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        public SortedDictionary<string, string> GetRequestPost()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.Form;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.Form[requestItem[i]]);
            }

            return sArray;
        }
        /// <summary>
        /// 获取支付宝GET过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        public SortedDictionary<string, string> GetRequestGet()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.QueryString;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.QueryString[requestItem[i]]);
            }

            return sArray;
        }
        #endregion
    }
}
