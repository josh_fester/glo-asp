﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GLO.Services;
using GLO.Data.Models;
using System.Web.Script.Serialization;
using GLO.Web.Models;
using GLO.Web.Common;
using System.Configuration;

namespace GLO.Web.Controllers
{
    [Authorize]
    [IsManagerAttribute]
    public class CompanyController : BaseController
    {
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();
        private IAccountService _accountservice;
        private ICompanyService _companyservice;
        private ITagUserService _taguserservice;
        private IMessageService _messageservice;
        private IWebInfoService _webinfoservice;

        #region Service
        public CompanyController(IAccountService AccountService, ICompanyService CompanyService, ITagUserService TagUserService, IMessageService MessageService, IWebInfoService WebInfoService)
        {
            _accountservice = AccountService;
            _companyservice = CompanyService;
            _taguserservice = TagUserService;
            _messageservice = MessageService;
            _webinfoservice = WebInfoService;
        }
        #endregion

        #region company index
        [AllowAnonymous]
        public ActionResult CompanyIndex(int companyid)
        {
            var company = _accountservice.GetUserByUserID(companyid);
            ViewBag.BasicInfoStatus = company.GLO_Company.BasicInfoStatus;
            ViewBag.FactSheetStatus = company.GLO_Company.FactSheetStatus;
            return View(company);
        }
        public JsonResult CompanyInfoEdit(string jsondata)
        {
            GLO_Company company = DeserializeXmlT<GLO_Company>(jsondata);
            GLO_Company dbcompany = _companyservice.GetCompanyByID(company.UserID);
            dbcompany.CompanyCulture = HttpUtility.UrlDecode(company.CompanyCulture);
            dbcompany.CompanyName = company.CompanyName;
            dbcompany.Image1 = company.Image1;
            dbcompany.Image2 = company.Image2;
            dbcompany.Image3 = company.Image3;
            dbcompany.Image4 = company.Image4;
            dbcompany.Image1Name = company.Image1Name;
            dbcompany.Image2Name = company.Image2Name;
            dbcompany.Image3Name = company.Image3Name;
            dbcompany.Image4Name = company.Image4Name;
            dbcompany.PPTFileName = company.PPTFileName;
            dbcompany.BasicInfoStatus = true;
            _companyservice.CompanyUpdate(dbcompany);

            GLO_Users user = _accountservice.GetUserByUserID(company.UserID);
            user.NickName = dbcompany.CompanyName;
            _accountservice.UserUpdate(user);

            CookieHelper.SetCookie(user);

            return CheckCompanyStatus(dbcompany);
        }
        public JsonResult _CompanyInfoEdit(int companyid)
        {
            var company = _companyservice.GetCompanyByID(companyid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/CompanyInfoEdit.ascx", company)
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region company factsheet
        public ActionResult FactSheet(int companyid)
        {
            var company = _accountservice.GetUserByUserID(companyid);
            ViewBag.BasicInfoStatus = company.GLO_Company.BasicInfoStatus;
            ViewBag.FactSheetStatus = company.GLO_Company.FactSheetStatus;
            CheckTagButton(company.UserID, company.TypeID);
            CheckRequestTagButton(company.UserID, company.TypeID);
            return View(company);
        }
        public JsonResult CompanyFactSheetEdit(string jsondata, string ispublish, int LocationID, int IndustryID)
        {
            GLO_Company company = DeserializeXmlT<GLO_Company>(jsondata);
            GLO_Company dbcompany = _companyservice.GetCompanyByID(company.UserID);
            //dbcompany.Industry = company.Industry;
            dbcompany.Size = company.Size;
            dbcompany.Revenue = company.Revenue;
            dbcompany.Awards = company.Awards;
            dbcompany.CompanyName = company.CompanyName;
            dbcompany.CompanyIntroduce = company.CompanyIntroduce;
            dbcompany.LogoUrl = company.LogoUrl;
            dbcompany.Website = company.Website;
            dbcompany.ContactInfo = company.ContactInfo;
            dbcompany.FactSheetStatus = true;
            dbcompany.CPerson1 = company.CPerson1;
            dbcompany.CPerson1Image = company.CPerson1Image;
            dbcompany.CPerson1Email = company.CPerson1Email;
            dbcompany.CPerson2 = company.CPerson2;
            dbcompany.Cperson2Image = company.Cperson2Image;
            dbcompany.CPersom2Email = company.CPersom2Email;
            dbcompany.CompanyValue1 = company.CompanyValue1;
            dbcompany.CompanyValue2 = company.CompanyValue2;
            dbcompany.CompanyValue3 = company.CompanyValue3;
            _companyservice.CompanyUpdate(dbcompany);

            GLO_Users user = _accountservice.GetUserByUserID(company.UserID);
            user.Icon = company.LogoUrl;
            user.NickName = dbcompany.CompanyName;
            user.IsPublish = ispublish == "0" ? false : true;
            _accountservice.UserUpdate(user);
            CookieHelper.SetCookie(user);

            if (LocationID != 0)
            {
                GLO_UserLocation newData = new GLO_UserLocation();
                newData.UserID = LoginedUser.UserID;
                newData.LID = LocationID;
                _accountservice.UpdateLocation(newData);
            }
            if (IndustryID != 0)
            {
                GLO_UserIndustry newData = new GLO_UserIndustry();
                newData.UserID = LoginedUser.UserID;
                newData.IndustryID = IndustryID;
                _accountservice.UpdateIndustry(newData);
            }
            return CheckCompanyStatus(dbcompany);
        }
        public JsonResult _FactSheetEdit(int companyid)
        {
            var company = _companyservice.GetCompanyByID(companyid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/FactSheetEdit.ascx", company)
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult RegisterThankYou(int companyid)
        {
            var user = _accountservice.GetUserByUserID(companyid);
            GLO_Company company = user.GLO_Company;
            company.RegisterStatus = true;
            _companyservice.CompanyUpdate(company);
            string Title = "Please approve this registered company";
            string Content = string.Format("A company is registered to GLO and finished profile pages, please click <a href=\"{0}/admin\" target=\"_blank\">here</a> to approve the company.", domain);
            string ToMail = ConfigurationManager.AppSettings["EmailName"].ToString();
            ExpertHelper.EMailSend(ToMail, Title, Content, "");
            return View(user.GLO_Company);
        }

        #region company job
        public ActionResult CompanyJob(int companyid, string keyword, string location, string func)
        {
            var joblist = _companyservice.GetCompanyJob(companyid, keyword, location, func).ToList();
            ViewBag.Keyword = !string.IsNullOrEmpty(keyword) ? keyword : string.Empty;
            ViewBag.Location = !string.IsNullOrEmpty(location) ? location : string.Empty;
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            ViewBag.Func = !string.IsNullOrEmpty(func) ? func : string.Empty;
            ViewBag.JobList = joblist;
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        #endregion

        #region dashboard
        [CheckApprovedAttribute]
        public ActionResult DashBoard(int companyid)
        {
            ViewBag.Select = "DashBoard";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult TagExpertList(int companyid)
        {
            ViewBag.Select = "TagExpertList";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult JobAdList(int companyid)
        {
            ViewBag.Select = "JobAdList";
            ViewBag.LocationList = _webinfoservice.GetLocationByParentID(-1);
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult CurrentJobAd(int companyid)
        {
            ViewBag.Select = "PlaceJobAd";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult PlaceJobAd(int companyid)
        {
            ViewBag.Select = "PlaceJobAd";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult CurrentHunting(int companyid)
        {
            ViewBag.Select = "CurrentHunting";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult RequestHunting(int companyid)
        {
            ViewBag.Select = "RequestHunting";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult BookAssessment(int companyid)
        {
            ViewBag.Select = "BookAssessment";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }
        [CheckApprovedAttribute]
        public ActionResult UpgradeMembership(int companyid)
        {
            ViewBag.Select = "UpgradeMembership";
            var user = _accountservice.GetUserByUserID(companyid);
            return View(user);
        }

        public ActionResult ReceiveEmail(int companyid)
        {
            var user = _accountservice.GetUserByUserID(companyid);
            ViewBag.Select = "ReceiveEmail";
            return View(user);
        }

        public JsonResult ReceiveEmailChange(int companyid, bool ReceiveEmail)
        {
            var user = _accountservice.GetUserByUserID(companyid);
            user.ReceiveEmail = ReceiveEmail;
            _accountservice.UserUpdate(user);
            return Json(true);
        }
        #endregion

        public JsonResult AssessmentAdd(string jsondata)
        {
            GLO_Assessment assessment = DeserializeXmlT<GLO_Assessment>(jsondata);
            GLO_Message message = new GLO_Message();
            message.MessageTypeID = 2;
            message.MessageTitle = string.Format("Book an Assessment from {0}", _accountservice.GetUserByUserID(assessment.UserID).NickName);
            message.MessageContect = string.Format("Subject: {0}<br/><br/>Company: {1}<br/><br/>For who: {2}<br/><br/>Location: {3}<br/><br/>Contact person: {4}<br/><br/>Contact details: {5}<br/><br/>Notes: {6}",
                assessment.Subject,assessment.Company,assessment.ObjectPerson,assessment.Location,assessment.ContactPerson,assessment.ContactDetail,assessment.Comment);
            message.SendDate = DateTime.Now;
            foreach (var admin in _accountservice.GetAdminList())
            {
                _messageservice.MessageCreate(message, assessment.UserID, admin.UserID.ToString());
            }
            //_companyservice.AssessmentAdd(assessment);
            return Json(true);
        }
        public JsonResult HuntingEdit(string jsondata)
        {
            GLO_HuntingRequest huntingrequest = DeserializeXmlT<GLO_HuntingRequest>(jsondata);
            if (huntingrequest.HuntingID > 0)
            {
                huntingrequest.IsArchive = _companyservice.GetHuntingByID(huntingrequest.HuntingID).IsArchive;
                _companyservice.HuntingEdit(huntingrequest);
            }
            else
                _companyservice.HuntingAdd(huntingrequest);
            return Json(true);
        }
        public JsonResult ReActiveHunting(int huntingid)
        {
            GLO_HuntingRequest huntingrequest = _companyservice.GetHuntingByID(huntingid);
            huntingrequest.IsArchive = false;
            _companyservice.HuntingEdit(huntingrequest);
            return Json(true);
        }
        public JsonResult DeleteHunting(int huntingid)
        {
            _companyservice.HuntinDelete(huntingid);
            return Json(true);
        }
        public JsonResult HuntingArchive(int huntingid)
        {
            GLO_HuntingRequest huntingrequest = _companyservice.GetHuntingByID(huntingid);
            huntingrequest.IsArchive = true;
            _companyservice.HuntingEdit(huntingrequest);
            return Json(true);
        }
        public JsonResult HuntingRequest(string jsondata)
        {
            GLO_HuntingRequest huntingrequest = DeserializeXmlT<GLO_HuntingRequest>(jsondata);
            GLO_Message message = new GLO_Message();
            message.MessageTypeID =2;
            message.MessageTitle = string.Format("Request headhunting from {0}", _accountservice.GetUserByUserID(huntingrequest.UserID).NickName);
            message.MessageContect = string.Format("Subject: {0}<br/><br/>Company: {1}<br/><br/>Job Title: {2}<br/><br/>Location: {3}<br/><br/>Contact person: {4}<br/><br/>Contact detail: {5}<br/><br/>Notes: {6}",
            huntingrequest.SubJect, huntingrequest.Company,huntingrequest.JobTitle,huntingrequest.Location,huntingrequest.ContactPerson,huntingrequest.ContactDetail,huntingrequest.Comment);
            message.SendDate = DateTime.Now;
            foreach(var admin in _accountservice.GetAdminList())
            {
                _messageservice.MessageCreate(message, LoginedUser.UserID, admin.UserID.ToString());
            }
            return Json(true);
        }
        public JsonResult JobEdit(string jsondata,bool IsPublish=false)
        {
            GLO_CompanyJob companyjob = DeserializeXmlT<GLO_CompanyJob>(jsondata);
            GLO_CompanyJob dbJob = _companyservice.GetCompanyJobByID(companyjob.JobID);
            dbJob.IsActive = companyjob.IsActive;
            dbJob.IsPublish = IsPublish;
            dbJob.JobTitle = companyjob.JobTitle;
            dbJob.Location = companyjob.Location;
            dbJob.Industry = companyjob.Industry;
            dbJob.PostDate = companyjob.PostDate;
            dbJob.Salary = companyjob.Salary;
            dbJob.KeyResponsibility = companyjob.KeyResponsibility;
            dbJob.KeyCompetence = companyjob.KeyCompetence;
            dbJob.JobSummary = companyjob.JobSummary;
            dbJob.Education = companyjob.Education;
            dbJob.ContactPerson = companyjob.ContactPerson;
            dbJob.DefinedTitle = companyjob.DefinedTitle;
            dbJob.DefinedContent = companyjob.DefinedContent;
            dbJob.JobFunction = companyjob.JobFunction;
            if (IsPublish && string.IsNullOrEmpty(dbJob.JobNO))
            {
                dbJob.JobNO = new Random().Next(100000, 999999).ToString();
                dbJob.PublishDate = DateTime.Now;
            }
            _companyservice.JobEdit(dbJob);
            return Json(true);
        }
        public JsonResult JobRemove(int jobid)
        {
            var job = _companyservice.GetCompanyJobByID(jobid);
            job.IsActive = false;
            job.IsPublish = false;
            _companyservice.JobEdit(job);

            GLO_CompanyJob jobslot = new GLO_CompanyJob();
            jobslot.JobTitle = "Job Title";
            jobslot.UserID = job.UserID;
            jobslot.JobSummary = "";
            jobslot.ExpireDate = job.ExpireDate;
            jobslot.IsActive = true;
            jobslot.IsPublish = false;
            _companyservice.JobAdd(jobslot);

            return Json(true);
        }
        public JsonResult ReActiveJob(int jobid)
        {
            var jobslot = _companyservice.GetCompanyByID(LoginedUser.UserID).GLO_CompanyJob.FirstOrDefault(x => x.IsActive == true && x.IsPublish == false && x.IsDelete == false);
            if (jobslot != null)
            {
                var activejob = _companyservice.GetCompanyJobByID(jobid);
                activejob.IsActive = true;
                activejob.IsPublish = true;
                activejob.ExpireDate = jobslot.ExpireDate;
                _companyservice.JobEdit(activejob);

                jobslot.IsActive = false;
                jobslot.IsPublish = false;
                jobslot.IsDelete = true;
                _companyservice.JobEdit(jobslot);
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
     
        public JsonResult CheckCompanyStatus(GLO_Company company)
        {
            if (!company.BasicInfoStatus)
            {
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            else if (!company.FactSheetStatus)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }
            else if (!company.RegisterStatus)
            {
                return Json("3", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("0", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteTagUser(int tagid)
        {
            _taguserservice.DeleteTagUser(tagid);
            return Json(true);
        }
  
        public JsonResult _HuntingEdit(int huntingid, int companyid)
        {
            GLO_HuntingRequest hunting = _companyservice.GetHuntingByID(huntingid);
            if (hunting == null)
            {
                hunting = new GLO_HuntingRequest();
                hunting.UserID = companyid;
            }
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/HuntingEdit.ascx", hunting)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _CompanyJob(int companyid)
        {
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/CompanyJobWidget.ascx", _companyservice.GetCompanyJob(companyid,"","",""))
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _GetJobDetailManage(int JobID)
        {
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/JobDetailManage.ascx", _companyservice.GetCompanyJobByID(JobID))
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _TagUserDetail(int categoryid,int type=1)
        {
            var taglist = _taguserservice.GetTagListByCategory(categoryid,type);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/"+(type== 1 ? "TagLeaderList" : "TagExpertList")+ ".ascx", taglist)
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _HuntingDetail(int huntingid)
        {
            var huntingdetail = _companyservice.GetHuntingByID(huntingid);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/HuntingDetail.ascx", huntingdetail)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _EditJob(int jobID)
        {
            var job = _companyservice.GetCompanyJobByID(jobID);
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/JobEdit.ascx", job)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult _FunctionList(int companyid)
        {
            var user = _accountservice.GetUserByUserID(companyid);
            var functionlist = user.GLO_TagCategory.Where(x=>x.GLO_Functions.IsDefaultValue==1).ToList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Company/FunctionList.ascx", functionlist)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }

        #region ChangePassword
        public ActionResult ChangePassword()
        {
            ViewBag.Select = "Password";
            ChangePasswordModel aModel = new ChangePasswordModel();
            aModel.UserID = LoginedUser.UserID;
            ViewBag.Message = "";
            ViewBag.CompanyName = _companyservice.GetCompanyByID(LoginedUser.UserID).CompanyName;
            return View(aModel);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel aModel)
        {
            ViewBag.CompanyName = _companyservice.GetCompanyByID(LoginedUser.UserID).CompanyName;
            try
            {
                var user = _accountservice.GetUserByUserID(aModel.UserID);
                if (user.PassWord != DEEncrypt.Encrypt(aModel.OldPassword))
                {
                    ModelState.AddModelError("OldPassword", "The old password is wrong.");
                }
                if (ModelState.IsValid)
                {
                    user.PassWord = DEEncrypt.Encrypt(aModel.NewPassword);
                    _accountservice.ChangePassWord(user);
                    FormsAuthentication.SignOut();
                    ViewBag.Message = "Your password was successfully changed！";
                }
                else
                {
                    ViewBag.Message = "";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(aModel);
        }
        #endregion

        public void CheckTagButton(int userID, int UserType)
        {
            bool showTagButton = true;
            foreach (var taggeduser in _taguserservice.GetTaggedUserList(LoginedUser.UserID))
            {
                if (taggeduser.TaggedUserID == userID)
                {
                    showTagButton = false;
                }
            }
            if (UserType == LoginedUser.UserType || LoginedUser.UserType==4)
            {
                showTagButton = false;
            }
            ViewBag.ShowTagButton = showTagButton;
        }

        public void CheckRequestTagButton(int userID, int UserType)
        {
            bool showButton = true;
            foreach (var taggeduser in _taguserservice.GetTaggedUserList(userID))
            {
                if (taggeduser.TaggedUserID == LoginedUser.UserID)
                {
                    showButton = false;
                }
            }
            if (UserType == LoginedUser.UserType || LoginedUser.UserType ==4)
            {
                showButton = false;
            }
            ViewBag.ShowRequestTagButton = showButton;
        }

        public JsonResult DeleteJob(int jobid)
        {
            try
            {
                var job = _companyservice.GetCompanyJobByID(jobid);
                job.IsPublish = false;
                job.IsActive = false;
                job.IsShow = false;
                job.IsDelete = true;
                _companyservice.JobEdit(job);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
