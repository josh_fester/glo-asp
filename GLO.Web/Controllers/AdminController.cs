﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Data.Models;
using GLO.Services;
using System.Diagnostics;
using System.Collections;
using System.IO;
using GLO.Web.Models;
using System.Web.Script.Serialization;
using GLO.Web.Common;
using System.Text;
using System.Net;

namespace GLO.Web.Controllers
{
    [Authorize]
    [AdminAuthorizeAttribute]
    public partial class AdminController : BaseController
    {
        private const int LEADERTYPE = 1;
        private const int EXPERTTYPE = 2;
        private const int COMPANYTYPE = 3;
        private INewsService _newsservcie;
        private IQuickPollService _pollservice;
        private ISuccessService _successservice;
        private IPartnerService _partnerservice;
        private ILeaderService _leaderservice;
        private IAccountService _accountservice;
        private IExpertService _expertservice;
        private IFAQService _faqservice;
        private IDifferenceService _differenceservice;
        private IPolicyService _policyservice;
        private IFeedBackService _feedbackservice;
        private IMessageService _messageservice;
        private ISolutionsService _solutionsservice;
        private ITagUserService _taguserservice;
        private IWebInfoService _webinfoservice;
        private ICompanyService _companyservice;
        private IInsightService _insightservice;
        private const string _queryCompanyUserModelKey = "AdminController_CompanyUserQueryModel";
        private const string _queryExpertUserModelKey = "AdminController_ExpertUserQueryModel";
        private const string _queryLeaderUserModelKey = "AdminController_LeaderUserQueryModel";
        private string domain = System.Configuration.ConfigurationManager.AppSettings["Domain"].ToString();

        #region Service
        public AdminController(IExpertService ExpertService, IAccountService AccountService, INewsService NewsService, 
            IQuickPollService QuickPollService, ISuccessService SuccessService, IPartnerService PartnerService, 
            ILeaderService LeaderService, IFAQService FaqService, IDifferenceService DifferenceService, IPolicyService PolicyService,
            IFeedBackService FeedBackService, IMessageService MessageService, ISolutionsService SolutionsService, IWebInfoService WebInfoService,
            ITagUserService TagUserService, ICompanyService CompanyService, IInsightService InsightService)
        {
            _newsservcie = NewsService;
            _pollservice = QuickPollService;
            _successservice = SuccessService;
            _partnerservice = PartnerService;
            _leaderservice = LeaderService;
            _accountservice = AccountService;
            _expertservice = ExpertService;
            _faqservice = FaqService;
            _differenceservice = DifferenceService;
            _policyservice = PolicyService;
            _feedbackservice = FeedBackService;
            _messageservice = MessageService;
            _solutionsservice = SolutionsService;
            _webinfoservice = WebInfoService;
            _taguserservice = TagUserService;
            _companyservice = CompanyService;
            _insightservice = InsightService;
        }
        #endregion

        private UserQueryModel CompanyQueryModel
        {
            get
            {
                return Session[_queryCompanyUserModelKey] as UserQueryModel;
            }
            set
            {
                Session[_queryCompanyUserModelKey] = value;
            }
        }
        private UserQueryModel ExpertQueryModel
        {
            get
            {
                return Session[_queryExpertUserModelKey] as UserQueryModel;
            }
            set
            {
                Session[_queryExpertUserModelKey] = value;
            }
        }
        private UserQueryModel LeaderQueryModel
        {
            get
            {
                return Session[_queryLeaderUserModelKey] as UserQueryModel;
            }
            set
            {
                Session[_queryLeaderUserModelKey] = value;
            }
        }

        #region //for index page
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region//for approved

        #region leader
        public ActionResult LeaderList(string f,int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                LeaderQueryModel = new UserQueryModel();
            }
            ExpertQueryModel = new UserQueryModel();
            CompanyQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (LeaderQueryModel == null)
            {
                LeaderQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(LeaderQueryModel.SearchText, perPage, p, out RecordCount, LEADERTYPE);
            LeaderQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(LeaderQueryModel);
        }
        [HttpPost]
        public ActionResult LeaderList(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            LeaderQueryModel = aQueryModel;
            if (LeaderQueryModel == null)
            {
                LeaderQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, LEADERTYPE);
            LeaderQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        public ActionResult LeaderListNotApproved(string f, int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                LeaderQueryModel = new UserQueryModel();
            }
            ExpertQueryModel = new UserQueryModel();
            CompanyQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (LeaderQueryModel == null)
            {
                LeaderQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetNotApprovedUserList(LeaderQueryModel.SearchText, perPage, p, out RecordCount, LEADERTYPE);
            LeaderQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(LeaderQueryModel);
        }
        [HttpPost]
        public ActionResult LeaderListNotApproved(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            LeaderQueryModel = aQueryModel;
            if (LeaderQueryModel == null)
            {
                LeaderQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, LEADERTYPE);
            LeaderQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        #endregion

        #region expert
        public ActionResult ExpertList(string f, int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                ExpertQueryModel = new UserQueryModel();
            }
            CompanyQueryModel = new UserQueryModel();
            LeaderQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (ExpertQueryModel == null)
            {
                ExpertQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(ExpertQueryModel.SearchText, perPage, p, out RecordCount, EXPERTTYPE);
            ExpertQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(ExpertQueryModel);
        }
        [HttpPost]
        public ActionResult ExpertList(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            ExpertQueryModel = aQueryModel;
            if (ExpertQueryModel == null)
            {
                ExpertQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, EXPERTTYPE);
            ExpertQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        public ActionResult ExpertListNotApproved(string f, int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                ExpertQueryModel = new UserQueryModel();
            }
            CompanyQueryModel = new UserQueryModel();
            LeaderQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (ExpertQueryModel == null)
            {
                ExpertQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetNotApprovedUserList(ExpertQueryModel.SearchText, perPage, p, out RecordCount, EXPERTTYPE);
            ExpertQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(ExpertQueryModel);
        }
        [HttpPost]
        public ActionResult ExpertListNotApproved(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            ExpertQueryModel = aQueryModel;
            if (ExpertQueryModel == null)
            {
                ExpertQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, EXPERTTYPE);
            ExpertQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        #endregion

        #region company
        public ActionResult CompanyList(string f, int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                CompanyQueryModel = new UserQueryModel();
            }
            ExpertQueryModel = new UserQueryModel();
            LeaderQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (CompanyQueryModel == null)
            {
                CompanyQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(CompanyQueryModel.SearchText, perPage, p, out RecordCount, COMPANYTYPE);
            CompanyQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(CompanyQueryModel);
        }
        [HttpPost]
        public ActionResult CompanyList(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            CompanyQueryModel = aQueryModel;
            if (CompanyQueryModel == null)
            {
                CompanyQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, COMPANYTYPE);
            CompanyQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        public ActionResult CompanyListNotApproved(string f, int p = 1, int perPage = 10)
        {
            if (!string.IsNullOrEmpty(f))
            {
                CompanyQueryModel = new UserQueryModel();
            }
            ExpertQueryModel = new UserQueryModel();
            LeaderQueryModel = new UserQueryModel();
            int RecordCount = 0;
            if (CompanyQueryModel == null)
            {
                CompanyQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetNotApprovedUserList(CompanyQueryModel.SearchText, perPage, p, out RecordCount, COMPANYTYPE);
            CompanyQueryModel.QueryResult = list;
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(CompanyQueryModel);
        }
        [HttpPost]
        public ActionResult CompanyListNotApproved(UserQueryModel aQueryModel)
        {
            int RecordCount = 0;
            CompanyQueryModel = aQueryModel;
            if (CompanyQueryModel == null)
            {
                CompanyQueryModel = new UserQueryModel();
            }
            var list = _accountservice.GetUserList(aQueryModel.SearchText, 10, 1, out RecordCount, COMPANYTYPE).Where(t=>t.Approved ==false).ToList();
            CompanyQueryModel.QueryResult = list;
            ViewData["p"] = 1;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = 10;
            return View(aQueryModel);
        }
        #endregion

        public ActionResult Approved(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            user.Approved = user.Approved ? false : true;
            _accountservice.UserUpdate(user);

            string username = user.NickName;
            if (user.TypeID == 1)
                username = user.GLO_Leader.FirstName;
            else if (user.TypeID == 2)
                username = user.GLO_HRExpert.FirstName;
            string title = "Payment to active your account";
            string content = @"Dear {0},<br/><br/>We are delighted to welcome you as a HR Expert  member of the GLO Leadership Community
                               Kindly confirm your membership by following the link below: <a href='/payment/expertpay'>Activate my GLO HR expert membership</a>
                               Thanks for joining a world class community of <em>good</em> leaders! ";
            content = string.Format(content, username);


            GLO_Message message = new GLO_Message();
            message.MessageTitle = title;
            message.MessageContect = content;
            message.MessageTypeID = 2;
            message.SendDate = DateTime.Now;
            _messageservice.MessageCreate(message, LoginedUser.UserID, id.ToString());

            return RedirectToAction("ExpertList");
        }
        public ActionResult RejectUser(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            ViewBag.UserName = user.NickName;
            ViewBag.UserID = id;
            return View();
        }
        [HttpPost]
        public ActionResult RejectUser(int id, string title,string content)
        {
            var user = _accountservice.GetUserByUserID(id);
            ViewBag.UserName = user.NickName;
            ViewBag.UserID = id;
            string email = user.Email;
            ExpertHelper.EMailSend(email, title, content,"");
            return Json(true);
        }
        [HttpPost]
        public ActionResult ApprovedUser(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            user.Approved = user.Approved ? false : true;
            _accountservice.UserUpdate(user);
            string title = string.Empty;
            string emailcontent = string.Empty;
            if (user.Approved == true)
            {
                string username = user.NickName;
                if (user.TypeID == 1)
                    username = user.GLO_Leader.FirstName;
                else if (user.TypeID == 2)
                    username = user.GLO_HRExpert.FirstName;

                if (user.TypeID == 3)
                {
                    title = "Payment to active your account";
                    StringBuilder sb = new StringBuilder();
                    
                    string ec = _policyservice.GetByID(12).PolicyContent;

                    if (user.GLO_Company.CompanyType == 1 || user.GLO_Company.CompanyType == 5)
                    {
                        title = "Thank you for joining the GLO leadership community";
                        ec = _policyservice.GetByID(11).PolicyContent;
                    }

                    ec = ec.Replace("{{{Activate my GLO company membership}}}", "<a style='text-decoration:underline' href='" + domain + "/Account/ActiveThankYou?companytype=" + user.GLO_Company.CompanyType + "'>Activate my GLO company membership</a>");
                    
                    sb.AppendFormat("Dear {0},<br/>", username);
                    sb.Append(ec);

                    emailcontent = sb.ToString();
                }
                else if (user.TypeID == 2)
                {
                    title = "Payment to active your account";
                    StringBuilder sb = new StringBuilder();
                    
                    string ec = _policyservice.GetByID(10).PolicyContent;
                    ec = ec.Replace("{{{Activate my GLO HR expert membership}}}", "<a style='text-decoration:underline' href='" + domain + "/Account/ActiveThankYou?experttype=1'>Activate my GLO HR expert membership</a>");
                    
                    sb.AppendFormat("Dear {0},<br/>", username);
                    sb.Append(ec);
                    
                    emailcontent = sb.ToString();
                }
                else
                {
                    title = "Your account has been approved";
                    StringBuilder sb = new StringBuilder();

                    string ec = _policyservice.GetByID(7).PolicyContent;
                    ec = ec.Replace("{{{here}}}", "<a style=\"text-decoration:underline\" href=\"" + domain + "/home/index\">here</a>");
                    
                    sb.AppendFormat("Dear {0},<br/>", username);
                    sb.Append(ec);

                    //sb.Append("We are pleased to inform you that your GLO account has now been approved.<br/>");
                    //sb.Append("Joining GLO as a leader is absolutely free for all online services. There are no unexpected sales pitches to sell you a 'Premium' or 'Gold' membership. <br/>");
                    //sb.Append("All our members have equal access rights and can:<br/><br/>");
                    //sb.Append("1.Build a unique GLO profile focused on your values, vision and core competencies<br/>");
                    //sb.Append("2.Access <em>good</em> jobs online posted by our company members<br/>");
                    //sb.Append("3.Take a free online Wise Check Up to evaluate your <em>wise</em> leadership attributes as a basis for further career development<br/>");
                    //sb.Append("4.Find <em>good</em> company members with the possibility to tag and contact them<br/>");
                    //sb.Append("5.Contact our pool of HR experts to assist your career development<br/><br/>");
                    //sb.AppendFormat("You can now start accessing these functions by clicking <a style='text-decoration:underline' href='{0}/home/index'>here</a><br/><br/>",domain);
                    //sb.Append("In addition to our online community, our team also assists leader members via:<br/><br/>");
                    //sb.Append("1.A skype interview to review your profile data and Wise Check Up report with immediate feedback from our consultants<br/>");
                    //sb.Append("2.Assessment programs with feedback on GLO’s core wise leadership competencies<br/>");
                    //sb.Append("3.GLO’s wise leadership coaching program<br/>");
                    //sb.Append("4.GLO’s wise leadership development program offered in collaboration with ivy league graduate schools<br/><br/>");
                    //sb.Append("If interested in any of these, you can contact us directly via your leader dashboard on GLO.<br/><br/>");
                    //sb.Append("Please do understand that we take integrity of data seriously.<br/>");
                    //sb.Append("The GLO team reserves the right to deny access if any of our members violate this integrity or exhibit values ");
                    //sb.Append("that are not consistent with GLO’s purpose of building the global community of <em>good</em> leaders assuring good business!<br/><br/> ");
                    //sb.Append("Success and thank you for your good leadership!<br/>Peter Buytaert<br/>CEO");

                    emailcontent = sb.ToString();
                }
                ExpertHelper.EMailSend(user.Email, title, emailcontent,"");
            }

            return Json(true);
        }
         [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ChangeProfileStatus(int id,string type)
        {
            var user = _accountservice.GetUserByUserID(id).GLO_Leader;
            if (type == "Interview")
            {
                user.IsInterview = user.IsInterview ? false : true;
            }
            else if (type == "Online")
            {
                user.OnlineAssessment = user.OnlineAssessment ? false : true;
            }
            else if (type == "Offline")
            {
                user.OfflineAssessment = user.OfflineAssessment ? false : true;
            }
            else if (type == "WLDP")
            {
                user.WALP = user.WALP ? false : true;
            }
            else if (type == "Coaching")
            {
                user.Coaching = user.Coaching ? false : true;
            }
            _leaderservice.LeaderUpdate(user);
            return Json(true);
        }
        [HttpPost]
        public ActionResult LockUser(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            user.IsLock = user.IsLock ? false : true;
            _accountservice.UserUpdate(user);
            return Json(true);
        }
        [HttpPost]
        public ActionResult PaymentUser(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            if (user.TypeID == 3 && user.GLO_UserEx.IsPayMent!=1)
            { 
                int companytype = user.GLO_Company.CompanyType;
                CompanyPayMentSucess(user.UserID, companytype);
            }
            else
            {
                user.ExpireTime = DateTime.Now.AddYears(1);
                _accountservice.UserUpdate(user);
            }
            var userex = user.GLO_UserEx;
            userex.IsPayMent = userex.IsPayMent == 0 ? 1 : 0; ;
            _accountservice.UpdateUserEx(userex);
               
            return Json(true);
        }
        public void CompanyPayMentSucess(int companyid, int companytype)
        {
            var company = _companyservice.GetCompanyByID(companyid);
            int count = _companyservice.GetCompanyTypeByID(companytype).JobCount;
            if (companytype != 8)
            {
                company.CompanyType = companytype;
                company.ExpireDate = DateTime.Now.AddYears(1);
                _companyservice.CompanyUpdate(company);

                var user = _accountservice.GetUserByUserID(companyid);
                user.ExpireTime = DateTime.Now.AddYears(1);
                _accountservice.UserUpdate(user);

                GLO_UserEx userex = user.GLO_UserEx;
                userex.IsPayMent = 1;
                _accountservice.UpdateUserEx(userex);

                foreach (var job in company.GLO_CompanyJob)
                {
                    if (job.IsPublish == false && string.IsNullOrEmpty(job.JobNO))
                    {
                        job.IsDelete = true;
                        job.IsActive = false;
                        _companyservice.JobEdit(job);
                    }
                    else if (job.IsDelete == false)
                    {
                        job.IsPublish = false;
                        job.IsActive = false;
                        job.IsDelete = false;
                        _companyservice.JobEdit(job);
                    }
                }
            }

            for (int i = 0; i < count; i++)
            {
                GLO_CompanyJob job = new GLO_CompanyJob();
                job.UserID = companyid;
                job.JobTitle = "Job Title";
                job.JobSummary = "";
                job.IsActive = true;
                job.IsPublish = false;
                job.ExpireDate = DateTime.Now.AddYears(1);
                _companyservice.JobAdd(job);
            }
        }
        public ActionResult ActiveUser(int id)
        {
            return View();
        }
        #endregion

        //for delete user
        public ActionResult DeleteLeader(int id)
        {
            _accountservice.DeleteUser(id);
            return RedirectToAction("LeaderList");
        }
        public ActionResult DeleteExpert(int id)
        {
            _accountservice.DeleteUser(id);
            return RedirectToAction("ExpertList");
        }
        public ActionResult DeleteCompany(int id)
        {
            _accountservice.DeleteUser(id);
            return RedirectToAction("CompanyList");
        }

        #region//for Admin
        [AcceptVerbs(HttpVerbs.Get)]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(AdminLoginModel admin)
        {
            if (ModelState.IsValid)
            {               
                return RedirectToAction("NewsList");
            }
            ModelState.AddModelError("", "Login error");
            return View();
        }
        public ActionResult AdminList()
        {
            var adminlist = _accountservice.GetAdminList();
            return View(adminlist);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdminCreate()
        {
            return View();
        }
        public ActionResult AdminLocked(int id)
        {
            var user = _accountservice.GetUserByUserID(id);
            user.IsLock = user.IsLock ? false : true;
            _accountservice.UserUpdate(user);
            return RedirectToAction("AdminList");
        }
        [HttpPost]
        public ActionResult AdminCreate(AdminRegisterModel model)
        {
            GLO_Users user = new GLO_Users();
            user.UserName = model.AdminName;
            user.NickName = model.AdminName;
            user.Email = model.AdminName;
            user.PassWord = DEEncrypt.Encrypt(model.AdminPassword);
            user.TypeID = 4;
            user.RegisterDate = DateTime.Now;         
            user.Approved = true;
            user.Icon = "user_none.gif";
            user.Sex = 1;
            _accountservice.UserRegister(user);
            return RedirectToAction("AdminList");
        }
        #endregion

        #region//for news management
        public ActionResult NewsList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _newsservcie.GetNewsList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult NewsAdd()
        {
            ViewData["categoryList"] = _newsservcie.GetCategoryCollection().ToList();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsAdd(GLO_News news, FormCollection form)
        {
            news.CategoryID = int.Parse(form["SelectCategory"]);
            news.Content = HttpUtility.HtmlEncode(form["editor1"]);

            news.CreateDate = DateTime.Now;
            news.ClickCount = 0;
            _newsservcie.AddNews(news);
            return RedirectToAction("NewsList");
        }
        public ActionResult NewsDelete(int id)
        {
            _newsservcie.DeleteNews(id);
            return RedirectToAction("NewsList");
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewsEdit(int id)
        {
            ViewData["categoryList"] = _newsservcie.GetCategoryCollection().ToList();
            var news = _newsservcie.GetNewsByID(id);
            return View(news);
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsEdit(GLO_News news, FormCollection form)
        {
            var glonews = _newsservcie.GetNewsByID(news.NewsID);
            glonews.Title = news.Title;
            glonews.Content = HttpUtility.HtmlEncode(form["editor1"]);
            _newsservcie.UpadateNews(glonews);
            return RedirectToAction("NewsList");
        }
        #endregion

        #region//for success case
        public ActionResult SuccessList()
        {
            var successcase = _successservice.GetSuccessCaseList();
            return View(successcase);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SuccessCreate()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuccessCreate(GLO_Success success)
        {
            _successservice.AddSuccessCase(success);
            return RedirectToAction("SuccessList");
        }

        public JsonResult _UserList()
        {
            List<GLO_Users> users = _accountservice.GetAllUserList();
            var result = new
            {
                Html = ControllerContext.RenderPartialAsString("/Views/Shared/Message/UserList.ascx", users)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequestRecommendation()
        {
            GLO_Users expert = _accountservice.GetUserByUserID(LoginedUser.UserID);
            MessageModel message = new MessageModel();
            message.SendUserID = expert.UserID;
            string StrHtml = _policyservice.GetByID(18).PolicyContent;
            string StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrHtml, "<[^>]+>", "");
            StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrNohtml, "&[^;]+;", " ");
            StrNohtml = StrNohtml.Replace("\t", "");
            message.MessageTitle = _policyservice.GetByID(18).PolicyTitle;
            message.MessageContent = StrNohtml;
            ViewBag.SendMessage = message;
            ViewData["Sender"] = expert;
            return View(expert);
        }

        public JsonResult AdminSendMessage(int messageid, int senduserid, int sendtype, string receiveuserid, string title, string content)
        {
            if (sendtype == 1)
            {
                var userList = _accountservice.GetAllUserList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    receiveuserid += item + ";";
                }
            }
            else if (sendtype == 2)
            {
                var userList = _accountservice.GetAllCompanyList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    receiveuserid += item + ";";
                }
            }
            else if (sendtype == 3)
            {
                var userList = _accountservice.GetAllLeaderList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    receiveuserid += item + ";";
                }
            }
            else if (sendtype == 4)
            {
                var userList = _accountservice.GetAllExpertList().Select(x => x.UserID).ToList();
                foreach (var item in userList)
                {
                    receiveuserid += item + ";";
                }
            }

            string [] arrReceiveuserid = receiveuserid.Split(';');
            foreach (string strReceiveuserid in arrReceiveuserid)
            {
                int requestUserID = 0;
                try
                {
                    requestUserID = int.Parse(strReceiveuserid);
                }
                catch { }
                if (requestUserID == 0)
                    continue;

                var recommendUser = _accountservice.GetUserByUserID(requestUserID);
                string username = recommendUser.NickName;
                if (recommendUser.TypeID == 1)
                    username = recommendUser.GLO_Leader.FirstName;
                else if (recommendUser.TypeID == 2)
                    username = recommendUser.GLO_HRExpert.FirstName;
                else if (recommendUser.TypeID == 3)
                    username = recommendUser.GLO_Company.CompanyName;

                if (recommendUser != null)
                {
                    title = _policyservice.GetByID(18).PolicyTitle;

                    content = _policyservice.GetByID(18).PolicyContent;

                    string inviteMessage = "Dear " + username + ",<br/>" + content;

                    string invitelink = string.Format("Please click <a href='{0}/Home/Success?recommendid={1}'> here</a> to start building a recommendation.<br/> Thanks!", domain, DEEncrypt.Encrypt(recommendUser.UserID.ToString()));
                    inviteMessage = inviteMessage + "<br/>" + invitelink + "<br/>" + LoginedUser.UserName;
                    inviteMessage = inviteMessage.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                    GLO_Message message = new GLO_Message();
                    message.MessageTitle = title;
                    message.MessageContect = inviteMessage;
                    message.SendDate = DateTime.Now;
                    message.MessageTypeID = 1;
                    _messageservice.SendSystemMessage(message, recommendUser.UserID.ToString());

                    ExpertHelper.EMailSend(recommendUser.Email, title, inviteMessage,"");
                }
            }
            return Json("True");
        }

        

        public ActionResult SuccessDelete(int id)
        {
            _successservice.DeleteSuccessCase(id);
            return RedirectToAction("SuccessList");
        }
        public ActionResult SuccessEdit(int id)
        {
            var success = _successservice.GetSuccessCaseByID(id);
            success.IsAble = success.IsAble ? false : true;
            _successservice.UpdateSuccessCase(success);
            return RedirectToAction("SuccessList");
        }
        #endregion

        #region//for partner
        public ActionResult PartnerList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _partnerservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult PartnerAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult PartnerAdd(GLO_Partner Partner)
        {
            if (Partner.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Partner.Content))
            {
                ModelState.AddModelError("Content", "Please enter the content.");
            }
            if (ModelState.IsValid)
            {
                Partner.Status = true;
                _partnerservice.Add(Partner);
                return RedirectToAction("PartnerList");
            }
            else
            {
                return View(Partner);
            }
        }
        public ActionResult PartnerEdit(int id)
        {
            return View(_partnerservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult PartnerEdit(GLO_Partner Partner)
        {
            if (Partner.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Partner.Content))
            {
                ModelState.AddModelError("Content", "Please enter the content.");
            }
            if (ModelState.IsValid)
            {
                _partnerservice.Update(Partner);
                return RedirectToAction("PartnerList");
            }
            else
            {
                return View(Partner);
            }
        }
        public ActionResult PartnerDetails(int id)
        {
            return View(_partnerservice.GetByID(id));
        }
        public ActionResult DeletePartner(int id)
        {
            _partnerservice.Delete(id);
            return RedirectToAction("PartnerList");
        }
        public ActionResult EnablePartner(int id)
        {
            _partnerservice.ChangeStatus(id, true);
            return RedirectToAction("PartnerList");
        }
        public ActionResult DisablePartner(int id)
        {
            _partnerservice.ChangeStatus(id, false);
            return RedirectToAction("PartnerList");
        }
        #endregion

        #region //for quick poll
        public ActionResult PollList()
        {
            var polllist = _pollservice.GetPollTopic();
            return View(polllist);
        }
        public ActionResult PollTopicAdd(GLO_PollTopic topic)
        {
            if (topic != null && !string.IsNullOrEmpty(topic.PollTopic))
            {
                _pollservice.PollTopicAdd(topic);
                return RedirectToAction("PollList");
            }
            return View();
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PollItemAdd(int id)
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PollItemAdd(GLO_PollItem pollitem, int id)
        {
            pollitem.PollID = id;
            _pollservice.PollItemAdd(pollitem);
            return View();
        }
        public ActionResult PollReview(int id)
        {
            ViewData["PollTopic"] = _pollservice.GetPollTopic().FirstOrDefault(x => x.PollID == id);
            var pollitem = _pollservice.GetPollItemByPollID(id);
            return View(pollitem);
        }
        public JsonResult PollVote(string itemid)
        {            
            foreach (var id in itemid.Split(','))
            {
                int pollitemid = 0;
                if (int.TryParse(id, out pollitemid))
                {
                    var item = _pollservice.GetPollItemByID(pollitemid);
                    item.ChoiceCount = item.ChoiceCount + 1; ;
                    _pollservice.PollItemEdit(item);
                }
            }
            return Json(true);
        }
        #endregion

        #region//upload file
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    //file path
                    string filePath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// orginal file name
                    string fileExtension = Path.GetExtension(fileName); // extension file name
                    string saveName = Guid.NewGuid().ToString() + fileExtension; //rename file

                    fileData.SaveAs(filePath + saveName);
                    string virtualpathsecured = "/images/" + saveName;

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName, Urlpath = virtualpathsecured });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "please select files to uplaod！" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region//for FAQ
        public ActionResult FAQList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _faqservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult FAQAdd()
        {           
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult FAQAdd(GLO_FAQ faq)
        {
            if (faq.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(faq.Question))
            {
                ModelState.AddModelError("Question", "Please enter the question.");
            }
            if (ModelState.IsValid)
            {
                faq.Status = true;
                _faqservice.Add(faq);
                return RedirectToAction("FAQList");
            }
            else
            {
                return View(faq);
            }
        }
        public ActionResult FAQEdit(int id)
        {            
            return View(_faqservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult FAQEdit(GLO_FAQ faq)
        {
            if (faq.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(faq.Question))
            {
                ModelState.AddModelError("Question", "Please enter the question.");
            }
            if (ModelState.IsValid)
            {
                _faqservice.Update(faq);
                return RedirectToAction("FAQList");
            }
            else
            {
                return View(faq);
            }
        }
        public ActionResult FAQDetails(int id)
        {
            return View(_faqservice.GetByID(id));
        }
        public ActionResult DeleteFAQ(int id)
        {
            _faqservice.Delete(id);
            return RedirectToAction("FAQList");
        }
        public ActionResult EnableFAQ(int id)
        {
            _faqservice.ChangeStatus(id, true);
            return RedirectToAction("FAQList");
        }
        public ActionResult DisableFAQ(int id)
        {
            _faqservice.ChangeStatus(id, false);
            return RedirectToAction("FAQList");
        }
        #endregion

        #region//for Difference
        public ActionResult DifferenceList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _differenceservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult DifferenceAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult DifferenceAdd(GLO_Difference Difference)
        {
            if (Difference.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Difference.DifferenceContent))
            {
                ModelState.AddModelError("DifferenceContent", "Please enter the content.");
            }
            if (ModelState.IsValid)
            {
                Difference.Status = true;
                _differenceservice.Add(Difference);
                return RedirectToAction("DifferenceList");
            }
            else
            {
                return View(Difference);
            }
        }
        public ActionResult DifferenceEdit(int id)
        {
            return View(_differenceservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult DifferenceEdit(GLO_Difference Difference)
        {
            if (Difference.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Difference.DifferenceContent))
            {
                ModelState.AddModelError("DifferenceContent", "Please enter the content.");
            }
            if (ModelState.IsValid)
            {
                _differenceservice.Update(Difference);
                return RedirectToAction("DifferenceList");
            }
            else
            {
                return View(Difference);
            }
        }
        public ActionResult DifferenceDetails(int id)
        {
            return View(_differenceservice.GetByID(id));
        }
        public ActionResult DeleteDifference(int id)
        {
            _differenceservice.Delete(id);
            return RedirectToAction("DifferenceList");
        }
        public ActionResult EnableDifference(int id)
        {
            _differenceservice.ChangeStatus(id, true);
            return RedirectToAction("DifferenceList");
        }
        public ActionResult DisableDifference(int id)
        {
            _differenceservice.ChangeStatus(id, false);
            return RedirectToAction("DifferenceList");
        }
        #endregion
        
        #region//for Policy
        public ActionResult PolicyList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _policyservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult PolicyAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult PolicyAdd(GLO_Policy Policy)
        {
            if (Policy.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Policy.PolicyTitle))
            {
                ModelState.AddModelError("PolicyTitle", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                Policy.Status = true;
                _policyservice.Add(Policy);
                return RedirectToAction("PolicyList");
            }
            else
            {
                return View(Policy);
            }
        }
        public ActionResult PolicyEdit(int id)
        {
            return View(_policyservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult PolicyEdit(GLO_Policy Policy)
        {
            if (Policy.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Policy.PolicyTitle))
            {
                ModelState.AddModelError("PolicyTitle", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                _policyservice.Update(Policy);
                return RedirectToAction("PolicyList");
            }
            else
            {
                return View(Policy);
            }
        }
        public ActionResult PolicyDetails(int id)
        {
            return View(_policyservice.GetByID(id));
        }
        public ActionResult DeletePolicy(int id)
        {
            _policyservice.Delete(id);
            return RedirectToAction("PolicyList");
        }
        public ActionResult EnablePolicy(int id)
        {
            _policyservice.ChangeStatus(id, true);
            return RedirectToAction("PolicyList");
        }
        public ActionResult DisablePolicy(int id)
        {
            _policyservice.ChangeStatus(id, false);
            return RedirectToAction("PolicyList");
        }
        #endregion

        #region//for FeedBack
        public ActionResult FeedBackList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _feedbackservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult FeedBackAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult FeedBackAdd(GLO_FeedBack FeedBack)
        {
            if (FeedBack.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(FeedBack.Question))
            {
                ModelState.AddModelError("Question", "Please enter the question.");
            }
            if (ModelState.IsValid)
            {
                FeedBack.Status = true;
                _feedbackservice.Add(FeedBack);
                return RedirectToAction("FeedBackList");
            }
            else
            {
                return View(FeedBack);
            }
        }
        public ActionResult FeedBackEdit(int id)
        {
            return View(_feedbackservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult FeedBackEdit(GLO_FeedBack FeedBack)
        {
            if (FeedBack.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(FeedBack.Question))
            {
                ModelState.AddModelError("Question", "Please enter the question.");
            }
            if (ModelState.IsValid)
            {
                _feedbackservice.Update(FeedBack);
                return RedirectToAction("FeedBackList");
            }
            else
            {
                return View(FeedBack);
            }
        }
        public ActionResult FeedBackDetails(int id)
        {
            return View(_feedbackservice.GetByID(id));
        }
        public ActionResult DeleteFeedBack(int id)
        {
            _feedbackservice.Delete(id);
            return RedirectToAction("FeedBackList");
        }
        public ActionResult EnableFeedBack(int id)
        {
            _feedbackservice.ChangeStatus(id, true);
            return RedirectToAction("FeedBackList");
        }
        public ActionResult DisableFeedBack(int id)
        {
            _feedbackservice.ChangeStatus(id, false);
            return RedirectToAction("FeedBackList");
        }
        #endregion

        #region//for NewsEvents
        public ActionResult NewsEventsList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _newsservcie.GetNewsEventsList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult NewsEventsAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsEventsAdd(GLO_News NewsEvents)
        {
            if (NewsEvents.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(NewsEvents.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (string.IsNullOrEmpty(NewsEvents.FileUrl))
            {
                ModelState.AddModelError("FileUrl", "Please enter the file url.");
            }
            if (ModelState.IsValid)
            {
                NewsEvents.CategoryID = 1;
                NewsEvents.Status = true;
                _newsservcie.AddNews(NewsEvents);
                return RedirectToAction("NewsEventsList");
            }
            else
            {
                return View(NewsEvents);
            }
        }
        public ActionResult NewsEventsEdit(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsEventsEdit(GLO_News NewsEvents)
        {
            if (NewsEvents.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(NewsEvents.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                _newsservcie.UpadateNews(NewsEvents);
                return RedirectToAction("NewsEventsList");
            }
            else
            {
                return View(NewsEvents);
            }
        }
        public ActionResult NewsEventsDetails(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        public ActionResult DeleteNewsEvents(int id)
        {
            _newsservcie.DeleteNews(id);
            return RedirectToAction("NewsEventsList");
        }
        public ActionResult EnableNewsEvents(int id)
        {
            _newsservcie.ChangeStatus(id, true);
            return RedirectToAction("NewsEventsList");
        }
        public ActionResult DisableNewsEvents(int id)
        {
            _newsservcie.ChangeStatus(id, false);
            return RedirectToAction("NewsEventsList");
        }
        #endregion

        #region//for NewsCalendar
        public ActionResult NewsCalendarList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _newsservcie.GetNewsCalendarList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult NewsCalendarFile()
        {
            int RecordCount = 0;
            var list = _newsservcie.GetNewsFileList(100, 1, out RecordCount);
            return View(list);
        }
        public ActionResult NewsCalenderFileAdd()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult NewsCalenderFileAdd(GLO_News News)
        {
            if (News.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(News.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                News.CategoryID = 4;
                News.Status = true;
                _newsservcie.AddNews(News);
                return RedirectToAction("NewsCalendarFile");
            }
            else
            {
                return View(News);
            }
        }
        public ActionResult NewsCalenderFileEdit(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsCalenderFileEdit(GLO_News News)
        {
            if (News.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(News.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                News.CategoryID = 4;
                News.Status = true;
                _newsservcie.UpadateNews(News);
                return RedirectToAction("NewsCalendarFile");
            }
            else
            {
                return View(News);
            }
        }
        public ActionResult DeleteNewsCalendarFile(int id)
        {
            _newsservcie.DeleteNews(id);
            return RedirectToAction("NewsCalendarFile");
        }
        public ActionResult NewsCalendarAdd()
        {
            SetCalendarCategory();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsCalendarAdd(GLO_News NewsCalendar)
        {
            SetCalendarCategory();
            if (string.IsNullOrEmpty(NewsCalendar.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (NewsCalendar.CalendarStartDate == null)
            {
                ModelState.AddModelError("CalendarStartDate", "Please select the start date.");
            }
            if (NewsCalendar.CalendarEndDate == null)
            {
                ModelState.AddModelError("CalendarEndDate", "Please select the end date.");
            }
            if (ModelState.IsValid)
            {
                NewsCalendar.CategoryID = 2;
                NewsCalendar.Status = true;
                _newsservcie.AddNews(NewsCalendar);
                return RedirectToAction("NewsCalendarList");
            }
            else
            {
                return View(NewsCalendar);
            }
        }
        public ActionResult NewsCalendarEdit(int id)
        {
            SetCalendarCategory();
            return View(_newsservcie.GetNewsByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsCalendarEdit(GLO_News NewsCalendar)
        {
            SetCalendarCategory();
            if (string.IsNullOrEmpty(NewsCalendar.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (NewsCalendar.CalendarStartDate == null)
            {
                ModelState.AddModelError("CalendarStartDate", "Please select the start date.");
            }
            if (NewsCalendar.CalendarEndDate == null)
            {
                ModelState.AddModelError("CalendarEndDate", "Please select the end date.");
            }
            if (ModelState.IsValid)
            {
                _newsservcie.UpadateNews(NewsCalendar);
                return RedirectToAction("NewsCalendarList");
            }
            else
            {
                return View(NewsCalendar);
            }
        }
        public ActionResult NewsCalendarDetails(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        public ActionResult DeleteNewsCalendar(int id)
        {
            _newsservcie.DeleteNews(id);
            return RedirectToAction("NewsCalendarList");
        }
        public ActionResult EnableNewsCalendar(int id)
        {
            _newsservcie.ChangeStatus(id, true);
            return RedirectToAction("NewsCalendarList");
        }
        public ActionResult DisableNewsCalendar(int id)
        {
            _newsservcie.ChangeStatus(id, false);
            return RedirectToAction("NewsCalendarList");
        }
        public void SetCalendarCategory()
        {
            var result = new SelectList(new[] {
                        new SelectListItem { Text = "Next GLO LDP", Value = "1" },
                        new SelectListItem { Text = "Next GLO assessment", Value = "2" },
                        new SelectListItem { Text = "Upcoming webinars", Value = "3" },
                        new SelectListItem { Text = "Next GLO Thought Leadership Series Meeting", Value = "4" },
                        new SelectListItem { Text = "Other", Value = "5" }
                    }, "Value", "Text");
            ViewBag.CategoryList = result;
        }
        #endregion

        #region//for NewsPress
        public ActionResult NewsPressList(int p = 1, int perPage = 10)
        {
            int RecordCount = 0;
            var list = _newsservcie.GetNewsPressList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult NewsPressAdd()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsPressAdd(GLO_News NewsPress)
        {
            if (NewsPress.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(NewsPress.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                NewsPress.CategoryID = 3;
                NewsPress.Status = true;
                _newsservcie.AddNews(NewsPress);
                return RedirectToAction("NewsPressList");
            }
            else
            {
                return View(NewsPress);
            }
        }
        public ActionResult NewsPressEdit(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult NewsPressEdit(GLO_News NewsPress)
        {
            if (NewsPress.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(NewsPress.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                _newsservcie.UpadateNews(NewsPress);
                return RedirectToAction("NewsPressList");
            }
            else
            {
                return View(NewsPress);
            }
        }
        public ActionResult NewsPressDetails(int id)
        {
            return View(_newsservcie.GetNewsByID(id));
        }
        public ActionResult DeleteNewsPress(int id)
        {
            _newsservcie.DeleteNews(id);
            return RedirectToAction("NewsPressList");
        }
        public ActionResult EnableNewsPress(int id)
        {
            _newsservcie.ChangeStatus(id, true);
            return RedirectToAction("NewsPressList");
        }
        public ActionResult DisableNewsPress(int id)
        {
            _newsservcie.ChangeStatus(id, false);
            return RedirectToAction("NewsPressList");
        }
        #endregion

        #region//for Solutions
        public ActionResult SolutionsList(int p = 1, int perPage = 10)
        {
            string[] ss = new string[] { };
            int RecordCount = 0;
            var list = _solutionsservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(list);
        }
        public ActionResult SolutionsAdd()
        {
            SetSolutionsCategory();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult SolutionsAdd(GLO_Solutions Solutions)
        {
            SetSolutionsCategory();
            if (Solutions.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Solutions.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                Solutions.Status = true;
                _solutionsservice.Add(Solutions);
                return RedirectToAction("SolutionsList");
            }
            else
            {
                return View(Solutions);
            }
        }
        public ActionResult SolutionsEdit(int id)
        {
            SetSolutionsCategory();
            return View(_solutionsservice.GetByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult SolutionsEdit(GLO_Solutions Solutions)
        {
            SetSolutionsCategory();
            if (Solutions.OrderId == null)
            {
                ModelState.AddModelError("OrderId", "Please enter the order id");
            }
            if (string.IsNullOrEmpty(Solutions.Title))
            {
                ModelState.AddModelError("Title", "Please enter the title.");
            }
            if (ModelState.IsValid)
            {
                _solutionsservice.Update(Solutions);
                return RedirectToAction("SolutionsList");
            }
            else
            {
                return View(Solutions);
            }
        }
        public ActionResult SolutionsDetails(int id)
        {
            return View(_solutionsservice.GetByID(id));
        }
        public ActionResult DeleteSolutions(int id)
        {
            _solutionsservice.Delete(id);
            return RedirectToAction("SolutionsList");
        }
        public ActionResult EnableSolutions(int id)
        {
            _solutionsservice.ChangeStatus(id, true);
            return RedirectToAction("SolutionsList");
        }
        public ActionResult DisableSolutions(int id)
        {
            _solutionsservice.ChangeStatus(id, false);
            return RedirectToAction("SolutionsList");
        }
        public void SetSolutionsCategory()
        {
            var categoryList = _solutionsservice.GetCategoryList();
            var result = new SelectList(categoryList, "CategoryID", "CategoryName", null);
            ViewBag.CategoryList = result;
        }
        #endregion

        #region //for Location
        public ActionResult LocationList(int parentid=0)
        {
            var locationlist = _webinfoservice.GetLocationByParentID(parentid);
            return View(locationlist);
        }
        public ActionResult LocationEdit(int id=0)
        {
            return View(_webinfoservice.GetLocationByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult LocationEdit(GLO_Location Location)
        {
            var dbLocation = _webinfoservice.GetLocationByID(Location.LID);
            dbLocation.LocationName = Location.LocationName;
            dbLocation.Oreder = Location.Oreder;
            _webinfoservice.LocationEdit(dbLocation);
            return RedirectToAction("LocationList",new{parentid=dbLocation.ParentID});           
        }
        public ActionResult LocationAdd(int parenetid=0)
        {
            GLO_Location location = new GLO_Location();
            location.ParentID = parenetid;
            return View(location);
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(true)]
        public ActionResult LocationAdd(GLO_Location Location)
        {
            if (!string.IsNullOrEmpty(Location.LocationName))
            {
                _webinfoservice.LocationAdd(Location);
                return RedirectToAction("LocationList", new { parentid = Location.ParentID });
            }
            else
            {
                return RedirectToAction("LocationAdd", new { parenetid = Location.ParentID });
            }
        }
        public ActionResult LocationDelete(int id)
        {
            var location = _webinfoservice.GetLocationByID(id);
            _webinfoservice.LocationDelete(id);
            return RedirectToAction("LocationList", new { parentid = location.ParentID });
        }
        #endregion

        #region //for industry
        public ActionResult IndustryList(int parentid = 0)
        {
            var industrylist = _webinfoservice.GetIndustyByParentID(parentid);
            return View(industrylist);
        }
        public ActionResult IndustryAdd(int parenetid = 0)
        {
            GLO_IndustryCategory industry = new GLO_IndustryCategory();
            industry.ParentID = parenetid;
            return View(industry);
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(true)]
        public ActionResult IndustryAdd(GLO_IndustryCategory industry)
        {
            if (!string.IsNullOrEmpty(industry.IndustryName))
            {
                _webinfoservice.IndustryAdd(industry);
                return RedirectToAction("IndustryList", new { parentid = industry.ParentID });
            }
            else
            {
                return RedirectToAction("IndustryAdd", new { parenetid = industry.ParentID });
            }
        }
        public ActionResult IndustryEdit(int id = 0)
        {
            return View(_webinfoservice.GetIndustyByID(id));
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult IndustryEdit(GLO_IndustryCategory Industry)
        {
            var dbIndustry = _webinfoservice.GetIndustyByID(Industry.IndustryID);
            dbIndustry.IndustryName = Industry.IndustryName;
            dbIndustry.OrderBy = Industry.OrderBy;
            _webinfoservice.IndustryEdit(dbIndustry);
            return RedirectToAction("IndustryList", new { parentid = Industry.ParentID });
        }
        public ActionResult IndustryDelete(int id)
        {
            var dbIndustry = _webinfoservice.GetIndustyByID(id);
            _webinfoservice.IndustryDelete(id);
            return RedirectToAction("IndustryList", new { parentid = dbIndustry.ParentID });
        }
        #endregion

        #region //for function
        public ActionResult FunctionList()
        {
            var function = _taguserservice.GetSystemFunctionList().OrderBy(x => x.FunctionOrderID).ToList(); ;
            return View(function);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FunctionAdd(Guid functionid)
        {
            var function = _taguserservice.GetFunctionByID(functionid);
            return View(function);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FunctionAdd(GLO_Functions function)
        {
            if(string.IsNullOrEmpty(function.FunctionName))
            {
                ModelState.AddModelError("FunctionName", "Please enter the funcation name");
            }
            if(function.FunctionOrderID == null)
            {
                ModelState.AddModelError("FunctionOrderID", "Please enter the order id");
            }
            if (!string.IsNullOrEmpty(function.FunctionName))
            {
                GLO_Functions dbFunction = new GLO_Functions();
                dbFunction.FunctionID = function.FunctionID;
                dbFunction.FunctionName = function.FunctionName;
                dbFunction.FunctionOrderID = function.FunctionOrderID;
                dbFunction.FunctionType = 3;
                dbFunction.IsDefaultValue = 1;
                var tagfunction = _taguserservice.GetFunctionByID(function.FunctionID);
                if (tagfunction == null)
                {
                    _taguserservice.AddFunction(dbFunction);
                    foreach (var company in _accountservice.GetAllCompanyList())
                    {
                        GLO_TagCategory category = new GLO_TagCategory();
                        category.FunctionID = dbFunction.FunctionID;
                        category.UserID = company.UserID;
                        _taguserservice.AddTagCategory(category);
                    }
                }
                else
                {
                    _taguserservice.FunctionEdit(dbFunction);
                }
                return RedirectToAction("FunctionList");
            }
            else
            {
                return View(function);
            }
        }

        public ActionResult FunctionDelete(Guid functionid)
        {
            _taguserservice.FunctionDelete(functionid);
            return RedirectToAction("FunctionList");
        }
        #endregion

        #region //for Insight
        public ActionResult SurveyList(int p = 1, int perPage = 10)
        {
            int RecordCount = 0;  
            var surveylist = _insightservice.GetAllList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(surveylist);
        }
        public ActionResult SurveyAdd()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SurveyAdd(GLO_SurveyReport report)
        {
            GLO_SurveyReport dbData = new GLO_SurveyReport();
            dbData.OrderID = report.OrderID;
            dbData.Status = false;
            dbData.SurveyType = report.SurveyType;
            dbData.SurveyTitle = report.SurveyTitle;
            dbData.SurveyUrl = report.SurveyUrl;
            dbData.SurveryInfo = report.SurveryInfo;
            dbData.CreateDate = DateTime.Now;
            _insightservice.Add(dbData);

            return RedirectToAction("SurveyList");
        }
        public ActionResult SurveyEdit(int id)
        {
            var report = _insightservice.GetSurveyReportByID(id);
            return View(report);
        }
        [HttpPost]
        public ActionResult SurveyEdit(GLO_SurveyReport report)
        {
            var dbSurvey = _insightservice.GetSurveyReportByID(report.SurveyID);
            dbSurvey.SurveyTitle = report.SurveyTitle;
            dbSurvey.OrderID = report.OrderID;
            dbSurvey.SurveyType = report.SurveyType;
            dbSurvey.SurveyTitle = report.SurveyTitle;
            dbSurvey.SurveyUrl = report.SurveyUrl;
            dbSurvey.SurveryInfo = report.SurveryInfo;
            _insightservice.Update(dbSurvey);

            return RedirectToAction("SurveyList");
        }
        public ActionResult EnableSurvey(int id)
        {
            _insightservice.ChangeStatus(id, true);
            return RedirectToAction("SurveyList");
        }
        public ActionResult DisableSurvey(int id)
        {
            _insightservice.ChangeStatus(id, false);
            return RedirectToAction("SurveyList");
        }
        public ActionResult DeleteSurvey(int id)
        {
            _insightservice.Delete(id);
            return RedirectToAction("SurveyList");
        }

        public ActionResult ApproveContent(int p = 1, int perPage = 10)
        {
            int RecordCount = 0;  
            var postlist = _insightservice.GetPostList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(postlist);
        }
        public JsonResult RecommendContent(int id)
        {
            var insightContent = _insightservice.GetPostByID(id);
            insightContent.IsRecommend = insightContent.IsRecommend ? false : true;
            _insightservice.PostUpdate(insightContent);
            return Json(true);
        }
        public JsonResult RecommendContentForEmail(int id)
        {
            var insightContent = _insightservice.GetPostByID(id);
            insightContent.IsEmailRecommend = insightContent.IsEmailRecommend ? false : true;
            _insightservice.PostUpdate(insightContent);
            return Json(true);
        }
        public ActionResult InsightContentDel(int id)
        {
            var postcontent = _insightservice.GetPostByID(id);
            List<int> litag = new List<int>();
            List<int> licomment = new List<int>();
            foreach (var tag in postcontent.GLO_InsightPostTag)
            {
                litag.Add(tag.PostTagID);
            }
            foreach (var comment in postcontent.GLO_InsightPostComment)
            {
                licomment.Add(comment.PostCommentID);
            }
            foreach (var tag in litag)
            {
                _insightservice.PostTagDel(tag);
            }
            foreach (var comment in licomment)
            {
                _insightservice.PostCommentDel(comment);
            }
            _insightservice.PostDelete(id);
            return RedirectToAction("ApproveContent");
        }
        public ActionResult ApproveComment(int p = 1, int perPage = 10)
        {
            int RecordCount = 0;
            var commentlist = _insightservice.GetCommentList(perPage, p, out RecordCount);
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(commentlist);
        }
        public ActionResult CommentDelete(int id)
        {
            _insightservice.PostCommentDel(id);
            return RedirectToAction("ApproveComment");
        }
        public ActionResult BulkSendEmail()
        {
            HttpWebRequest httpReq;
            HttpWebResponse httpResp;

            string strBuff = "";
            char[] cbuffer = new char[256];
            int byteRead = 0;
            Uri httpURL = new Uri(string.Format("{0}/Insight/NewsLetter", domain));
            httpReq = (HttpWebRequest)WebRequest.Create(httpURL);
            httpResp = (HttpWebResponse) httpReq.GetResponse();
            Stream respStream = httpResp.GetResponseStream();
            StreamReader respStreamReader = new StreamReader(respStream,Encoding.UTF8);

            byteRead = respStreamReader.Read(cbuffer,0,256);

            while (byteRead != 0)
            {
            string strResp = new string(cbuffer,0,byteRead);
                              strBuff = strBuff + strResp;
                              byteRead = respStreamReader.Read(cbuffer,0,256);
            }

            respStream.Close();
            ViewBag.Content = strBuff;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BulkSendEmail(FormCollection form)
        {
            string emailTitle = form["emailTitle"].ToString();
            string content = form["editor1"].ToString();
            if (string.IsNullOrEmpty(emailTitle))
            {
                ModelState.AddModelError("emailTitle", "Please enter the title.");
            }
             if (ModelState.IsValid)
             {
               
                 int type = int.Parse(form["SendType"].ToString());
                 IList<GLO_Users> userList = new List<GLO_Users>();
                 switch (type)
                 {
                     case 0: userList = _accountservice.GetAllUserList();
                         break;
                     case 1: userList = _accountservice.GetAllLeaderList();
                         break;
                     case 2: userList = _accountservice.GetAllExpertList();
                         break;
                     case 3: userList = _accountservice.GetAllCompanyList();
                         break;
                     default: break;

                 }
                 foreach (var user in userList)
                 {
                     if (user.ReceiveEmail)
                     {
                         string email = user.UserName;
                         if (email.IndexOf("@") > 0)
                         {
                             ExpertHelper.EMailSend(email, emailTitle, content,"");
                         }
                     }
                 }
                 return JavaScript("Send successful");
             }
            ViewBag.Content = content;
            return View();
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Menu()
        {
            _setUnApprovedUserCount();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Main()
        {
            _setUnApprovedUserCount();
            return View();
        }


        #region set unapproved user count
        [HttpPost]
        public JsonResult SetUnApprovedUserCount()
        {
            return _setUnApprovedUserCount();
        }
        public JsonResult _setUnApprovedUserCount()
        {
            int RecordCount = 0;
            int leaderCount = _accountservice.GetNotApprovedUserList(null, 999, 1, out RecordCount, LEADERTYPE).Count;
            int expertCount = _accountservice.GetNotApprovedUserList(null, 999, 1, out RecordCount, EXPERTTYPE).Count;
            int companyCount = _accountservice.GetNotApprovedUserList(null, 999, 1, out RecordCount, COMPANYTYPE).Count;
            HttpContext.Session["1"] = "";
            Session["UnApprovedLeaderCount"] = leaderCount;
            Session["UnApprovedExpertCount"] = expertCount;
            Session["UnApprovedCompanyCount"] = companyCount;
            return Json(new { leaderCount = leaderCount, expertCount = expertCount, companyCount = companyCount });
        }
        #endregion

        private static T DeserializeXmlT<T>(string record)
        {
            if (String.IsNullOrEmpty(record)) return default(T);
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<T>(record);
        }

    }
}
