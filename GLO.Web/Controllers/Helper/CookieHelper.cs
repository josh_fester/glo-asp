﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GLO.Data.Models;
using System.Web.Security;

namespace GLO.Web.Controllers
{
    public static class CookieHelper
    {
        public static void SetCookie(GLO_Users user)
        {
            LoginUserInfo LoginUserInfo = new LoginUserInfo();
            LoginUserInfo.UserID = user.UserID;
            LoginUserInfo.UserName = user.NickName;
            LoginUserInfo.UserType = user.TypeID;
            LoginUserInfo.LogoUrl = user.Icon;
            LoginUserInfo.UserEmail = user.Email;
            LoginUserInfo.Approved = user.Approved;
            if (user.TypeID != 4)
            {
                LoginUserInfo.IsPayMent = user.GLO_UserEx.IsPayMent;
                LoginUserInfo.ExpireTime = user.ExpireTime;
                LoginUserInfo.UnReadEmail = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
            }
            FormsAuthentication.SetAuthCookie(user.NickName, true);
            CustomPrincipal<LoginUserInfo>.SignIn(user.NickName, LoginUserInfo, 10);


            //LoginUserInfo LoginUserInfo = new LoginUserInfo();
            //LoginUserInfo.UserID = user.UserID;
            //LoginUserInfo.UserName = user.NickName;
            //LoginUserInfo.UserID = user.UserID;
            //LoginUserInfo.UserType = user.TypeID;
            //LoginUserInfo.LogoUrl = user.Icon;
            //LoginUserInfo.UserEmail = user.Email;
            //LoginUserInfo.UnReadEmail = user.GLO_MessageReceive.Where(x => x.Status == 0).Count();
            //Session["UnReadMessageCount"] = LoginUserInfo.UnReadEmail;
            //FormsAuthentication.SetAuthCookie(user.NickName, true);
            //CustomPrincipal<LoginUserInfo>.SignIn(user.NickName, LoginUserInfo, 10000);
        }
    }
}