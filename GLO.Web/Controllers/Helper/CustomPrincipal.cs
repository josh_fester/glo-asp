﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace GLO.Web.Controllers
{
    public class LoginUserInfo : IPrincipal
    {
        public int UserID;
        public string UserName;
        public int UserType;
        public string UserEmail;
        public string LogoUrl;
        public int UnReadEmail{get;set;}
        public bool Approved { get; set; }
        public DateTime? ExpireTime { get; set; }
        public int? IsPayMent { get; set; }

        public LoginUserInfo() { }
        public LoginUserInfo(int userid, string username, int usertype, string useremail)
        {
            UserID = userid;
            UserName = username;
            UserType = usertype;
            UserEmail = useremail;
        }

        public override string ToString()
        {
            return string.Format("UserId: {0}, GroupId: {1}, UserName: {2}, IsAdmin: {3}",
                UserID, UserType, UserName, IsInRole("Admin"));
        }

        [ScriptIgnore]
        public IIdentity Identity
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInRole(string role)
        {
            if (string.Compare(role, "Admin", true) == 0)
                return UserType == 1;
            else
                return UserType > 0;
        }
    }

    public class CustomPrincipal<TUserData> : IPrincipal
    where TUserData : class, new()
    {
        private IIdentity _identity;
        private TUserData _userData;

        public CustomPrincipal(FormsAuthenticationTicket ticket, TUserData userData)
        {
            if (ticket == null)
                throw new ArgumentNullException("ticket");
            if (userData == null)
                throw new ArgumentNullException("userData");

            _identity = new FormsIdentity(ticket);
            _userData = userData;
        }

        public TUserData UserData
        {
            get { return _userData; }
        }

        public IIdentity Identity
        {
            get { return _identity; }
        }

        public bool IsInRole(string role)
        {
            // 把判断用户组的操作留给UserData去实现。
            IPrincipal principal = _userData as IPrincipal;
            if (principal == null)
                throw new NotImplementedException();
            else
                return principal.IsInRole(role);
        }

        /// <summary>
        /// user login write cookie
        /// </summary>
        /// <param name="loginName">login name</param>
        /// <param name="userData">login user object</param>
        /// <param name="expiration">cookie expire time(minutes)</param>
        public static void SignIn(string loginName, TUserData userData, int expiration)
        {
            if (string.IsNullOrEmpty(loginName))
                throw new ArgumentNullException("loginName");
            if (userData == null)
                throw new ArgumentNullException("userData");

            // serialize data to a string
            string data = null;
            if (userData != null)
                data = (new JavaScriptSerializer()).Serialize(userData);

            //create a FormsAuthenticationTicket and its user ifno
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2, loginName, DateTime.Now, DateTime.Now.AddDays(1), true, data);

            // Encrypt ticket
            string cookieValue = FormsAuthentication.Encrypt(ticket);

            //create Cookie
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieValue);
            cookie.HttpOnly = true;
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Domain = FormsAuthentication.CookieDomain;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            //if (expiration > 0)
             //   cookie.Expires = DateTime.Now.AddMinutes(expiration);

            HttpContext context = HttpContext.Current;
            if (context == null)
                throw new InvalidOperationException();
            // Write Cookie
            context.Response.Cookies.Remove(cookie.Name);
            context.Response.Cookies.Add(cookie);
        }

        /// <summary>
        ///try to create a custonprincipal object from cookie
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CustomPrincipal<TUserData> TryParsePrincipal(HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");
            //read cookie
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
                return null;
            try
            {
                TUserData userData = null;
                //decrypt cookie value,get FormsAuthenticationTicket object
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket != null && string.IsNullOrEmpty(ticket.UserData) == false)
                    //return data
                    userData = (new JavaScriptSerializer()).Deserialize<TUserData>(ticket.UserData);

                if (ticket != null && userData != null)
                    // CustomPrincipal,set context.User value。
                    return new CustomPrincipal<TUserData>(ticket, userData);
            }
            catch {/*do nothing*/ }
            return null;
        }

        /// <summary>
        ///inital set cookie value in Golbal.asax.cs
        /// </summary>
        /// <param name="context"></param>
        public static void TrySetUserInfo(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            CustomPrincipal<TUserData> user = TryParsePrincipal(context.Request);
            if (user != null)
                context.User = user;
        }

    }

}