﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Services;
using GLO.Data.Models;
using GLO.Web.Models;
using System.Web.Script.Serialization;

namespace GLO.Web.Controllers
{
    public class InsightController : BaseController
    {
        private INewsService _newsservcie;
        private IInsightService _insightservice;
        private IAccountService _accountservice;
        private ICompanyService _companyservice;
        public InsightController(INewsService NewsService, IInsightService InsightService, IAccountService AccountService, ICompanyService CompanyService)
        {
            _newsservcie = NewsService;
            _insightservice = InsightService;
            _accountservice = AccountService;
            _companyservice = CompanyService;
        }

      
        public ActionResult Index(int p = 1, int perPage = 3)
        {
            
            SetWidget();
            int RecordCount = 0;       
            var contentlist = _insightservice.GetPostList(perPage, p, out RecordCount).ToList();
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(contentlist);
        }
        public ActionResult SearchInsight(int p = 1, int perPage = 3,string tag="")
        {
            SetWidget();
            ViewBag.Tag = tag;
            int RecordCount = 0;
            var contentlist = _insightservice.SearchTagList(perPage, p, out RecordCount,tag).ToList();
            ViewData["p"] = p;
            ViewData["RecordCount"] = RecordCount;
            ViewData["perPage"] = perPage;
            return View(contentlist);
        }
        public ActionResult InsightDetail(int postid)
        {
            SetWidget();
            var postcontent = _insightservice.GetPostByID(postid);
            return View(postcontent);
        }
        private void SetWidget()
        {
            int RecordCount = 0;
            ViewBag.EventsList = _newsservcie.GetNewsEventsList(3, 1, out RecordCount).Where(x => x.Status == true).ToList();
            ViewBag.CalenderFile = _newsservcie.GetNewsFileList(3, 1, out RecordCount).Where(x => x.Status == true).ToList();
            ViewBag.Survey = _insightservice.GetAllList(100, 1, out RecordCount).Where(x => x.Status == true).ToList();
            ViewBag.TopContent = _insightservice.GetPostList(3, 1, out RecordCount,true).ToList();
        }

        public ActionResult InsightCreate(int userid)
        {
            ViewBag.Select = "InsightList";
            GLO_Users user = _accountservice.GetUserByUserID(userid);
            return View(user);
        }
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult InsightCreate(InsightPost post, FormCollection form)
        {
            int UserID = LoginedUser.UserID;
            GLO_InsightPosts contentpost = new GLO_InsightPosts();
            contentpost.UserID = UserID;
            contentpost.Title = post.Title;
            contentpost.Description = post.Description;
            contentpost.PostContent = HttpUtility.HtmlEncode(form["editor1"]);
            contentpost.DateCreated = DateTime.Now;
            contentpost.IsDeleted = false;
            _insightservice.PostAdd(contentpost);

             //save tag
            post.PostTag = !string.IsNullOrEmpty(post.PostTag) ? post.PostTag : ";";
            string[] tagarray = post.PostTag.Split(';');
            foreach (string tag in tagarray)
            {
                if (!string.IsNullOrEmpty(tag))
                {
                    GLO_InsightPostTag posttag = new GLO_InsightPostTag();
                    posttag.PostID = contentpost.PostID;
                    posttag.Tag = tag;
                    _insightservice.PostTagAdd(posttag);
                }
            }
            return RedirectToAction("InsightList", new { userid=UserID});
        }
        public ActionResult InsightList(int userid)
        {
            ViewBag.Select = "InsightList";
            GLO_Users user = _accountservice.GetUserByUserID(userid);
            return View(user);
        }
        public ActionResult InsightContentEdit(int id)
        {
            int UserID = LoginedUser.UserID;
            ViewBag.Select = "InsightList";
            var content = _insightservice.GetPostByID(id);
            InsightPost post = new InsightPost();
            post.PostID = content.PostID;
            post.Title = content.Title;
            post.Description = content.Description;
            post.PostContent = HttpUtility.HtmlDecode(content.PostContent);
            List<string> list = new List<string>();
            foreach(var tag in content.GLO_InsightPostTag.ToList())
            {
                list.Add(tag.Tag);
            }
            post.PostTag = string.Join(";", list);
            ViewBag.PostContent = post;
            GLO_Users user = _accountservice.GetUserByUserID(UserID);
            return View(user);
        }
         [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult InsightContentEdit(InsightPost post, FormCollection form)
        {
            int UserID = LoginedUser.UserID;
            var postcontent = _insightservice.GetPostByID(post.PostID);
            if (postcontent.GLO_InsightPostTag!=null && postcontent.GLO_InsightPostTag.Count > 0)
            {
                List<int> litag = new List<int>();
                foreach (var tag in postcontent.GLO_InsightPostTag)
                {
                    litag.Add(tag.PostTagID);
                }
                foreach (var tag in litag)
                {
                    _insightservice.PostTagDel(tag);
                }
            }
            postcontent.Title = post.Title;
            postcontent.Description = post.Description;
            postcontent.DateModified = DateTime.Now;
            postcontent.PostContent = HttpUtility.HtmlEncode(form["editor1"]);
            _insightservice.PostUpdate(postcontent);

            //save tag
            post.PostTag = !string.IsNullOrEmpty(post.PostTag) ? post.PostTag : ";";
            string[] tagarray = post.PostTag.Split(';');
            foreach (string tag in tagarray)
            {
                if (!string.IsNullOrEmpty(tag))
                {
                    GLO_InsightPostTag posttag = new GLO_InsightPostTag();
                    posttag.PostID = postcontent.PostID;
                    posttag.Tag = tag;
                    _insightservice.PostTagAdd(posttag);
                }
            }

            return RedirectToAction("InsightList", new { userid = UserID });
        }
        public ActionResult InsightContentDel(int id)
        {
            int UserID = LoginedUser.UserID;
            var postcontent = _insightservice.GetPostByID(id);
            List<int> litag = new List<int>();
            List<int> licomment = new List<int>();
            foreach (var tag in postcontent.GLO_InsightPostTag)
            {
                litag.Add(tag.PostTagID);
            }
            foreach (var comment in postcontent.GLO_InsightPostComment)
            {
                licomment.Add(comment.PostCommentID);
            }
            foreach (var tag in litag)
            {
                _insightservice.PostTagDel(tag);
            }
            foreach (var comment in licomment)
            {
                _insightservice.PostCommentDel(comment);
            }
            _insightservice.PostDelete(id);
            return RedirectToAction("InsightList", new { userid = UserID });
        }
        public ActionResult NewsLetter()
        {
            SetWidget();
            int RecordCount = 0;
            ViewBag.Company = _companyservice.RecommendCompanyList(4);
            ViewBag.TopRecommentContent = _insightservice.GetPostForEmailTemplate();
            ViewBag.CalenderFile = _newsservcie.GetNewsFileList(5, 1, out RecordCount).Where(x => x.Status == true).ToList(); 
            return View();
        }
        public JsonResult CommentAdd(string comment)
        {
            GLO_InsightPostComment com = DeserializeXmlT<GLO_InsightPostComment>(comment);
            if (LoginedUser == null)
                com.UserID = 195;
            else
                com.UserID = LoginedUser.UserID;
            com.ParentCommentID = 1;
            com.CommentDate = DateTime.Now;
            com.Ip = GetIP();
            _insightservice.PostCommentAdd(com);
            return Json(true);
        }
        public JsonResult CommentDel(int commentid)
        {
            _insightservice.PostCommentDel(commentid);
            return Json(true);
        }
        public JsonResult ContentRation(int postid, int starcount)
        {
            var postcontent = _insightservice.GetPostByID(postid);
            postcontent.Raters=postcontent.Raters == null ? 0 : postcontent.Raters;
            postcontent.Rating = postcontent.Rating == null ? 0 : postcontent.Rating;
            postcontent.Raters = postcontent.Raters + 1;
            postcontent.Rating = postcontent.Rating + starcount;
            _insightservice.PostUpdate(postcontent);
            return Json(true);
        }
        
         private static T DeserializeXmlT<T>(string record)
         {           
             if (String.IsNullOrEmpty(record)) return default(T);
             var ser = new JavaScriptSerializer();
             return ser.Deserialize<T>(record);
         }

    }
}
