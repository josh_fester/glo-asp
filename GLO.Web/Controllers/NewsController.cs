﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GLO.Data.Models;
using GLO.Services;

namespace GLO.Web.Controllers
{
    public class NewsController : Controller
    {
        private INewsService _newsservcie;
        private IInsightService _insightservice;

        public NewsController(INewsService NewsService, IInsightService InsightService)
        {
            _newsservcie = NewsService;
            _insightservice = InsightService;
        }

        public ActionResult Index(int year=0)
        {
            if(year==0)
                year = DateTime.Now.Year;
            int RecordCount = 0;
            var eventsList = _newsservcie.GetNewsEventsList(100, 1, out RecordCount).Where(x => x.Status == true).ToList();
            ViewBag.EventsList = eventsList;

            var calendarList = _newsservcie.GetNewsCalendarList(100, 1, out RecordCount).Where(x => x.Status == true && (x.CalendarStartDate.Value.Year <= year && year <=x.CalendarEndDate.Value.Year)).ToList();
            ViewBag.LDPList = calendarList.Where(t => t.CalendarCategory == 1).OrderBy(x=>x.CalendarStartDate).ToList();
            ViewBag.AssessmentList = calendarList.Where(t => t.CalendarCategory == 2).OrderBy(x => x.CalendarStartDate).ToList();
            ViewBag.WebinarsList = calendarList.Where(t => t.CalendarCategory == 3).OrderBy(x => x.CalendarStartDate).ToList();
            ViewBag.ThoughtList = calendarList.Where(t => t.CalendarCategory == 4).OrderBy(x => x.CalendarStartDate).ToList();
            ViewBag.OtherList = calendarList.Where(t => t.CalendarCategory == 5).OrderBy(x => x.CalendarStartDate).ToList();

            ViewBag.CalenderFile = _newsservcie.GetNewsFileList(100, 1, out RecordCount).Where(x => x.Status == true).ToList(); 

            var pressList = _newsservcie.GetNewsPressList(100, 1, out RecordCount).Where(x => x.Status == true).ToList();
            ViewBag.PressList = pressList;

            ViewBag.Year = year;

            ViewBag.Survey = _insightservice.GetAllList(100, 1, out RecordCount).Where(x => x.Status == true).ToList();

            return View();
        }

        public ActionResult Details(int id)
        {
            var news = _newsservcie.GetNewsByID(id);
            news.ClickCount = news.ClickCount + 1;
            _newsservcie.UpadateNews(news);
            return View(news);
        }

        public ActionResult Create()
        {
            return View("AddCategory");
        }

        public ActionResult AddCategory(GLO_NewsCategory category)
        {
            if (category != null)
            {
                //GLO_NewsCategory nc = new GLO_NewsCategory();
                //nc.CategoryName = "testing category";
                var count = _newsservcie.AddNewsCategory(category);
            }
            return RedirectToAction("Index");
        }
     
        public ActionResult PlayVideo(int id)
        {
            var news = _newsservcie.GetNewsByID(id);
            return View(news);
        }
    }
}
