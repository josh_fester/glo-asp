﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GLO.Web.Models
{
    public class OnlineUser
    {
        /// <summary>
        /// login user id
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// Login user name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// user type
        /// </summary>
        public int UserType { get; set; }
        /// <summary>
        /// Login time
        /// </summary>
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// Last active time
        /// </summary>
        public DateTime LastTime { get; set; }
        /// <summary>
        /// Last active url
        /// </summary>
        public string LastActionUrl { get; set; }
        /// <summary>
        /// Login IP address
        /// </summary>
        public string LoginIp { get; set; }
        /// <summary>
        /// whether gust
        /// </summary>
        public bool IsGuest { get; set; }
        /// <summary>
        /// current session ID
        /// </summary>
        public string SessionID { get; set; }
    }
}
