﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GLO.Web.Models
{
    public class WebSiteInfo
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string UrlTitle { get; set; }
    }
}