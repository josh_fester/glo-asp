﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GLO.Data;
using GLO.Data.Models;
namespace GLO.Web.Models
{
    public class RecommendUsers
    {
        public IList<GLO_Leader> Leaders { get; set; }
        public IList<GLO_HRExpert> Experts { get; set; }
        public IList<GLO_Company> Companies { get; set; }
        
    }
}