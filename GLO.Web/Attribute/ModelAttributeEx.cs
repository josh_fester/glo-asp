﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using GLO.Services;
using StructureMap;

namespace GLO.Web.Attributes
{
    public class CheckEmail : ValidationAttribute
    {
        private IAccountService _accountservice;

        public CheckEmail()
        {
            _accountservice = ObjectFactory.GetInstance<IAccountService>(); ;
        }

        public override bool IsValid(object value)
        {
            if (value==null || _accountservice.CheckEmail(value.ToString()) == true)
            {
                return false;
            }
            return true;
        }
    }
}