﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IMessageService
    {
        #region Message
        void MessageCreate(GLO_Message message, int senduserid, string receiveid);
        void MessageSave(GLO_Message message, int senduserid, string receiveid);
        IList<GLO_Message> GetMessageList();
        void MessageDelete(int id);
        void MessageUpdate(GLO_Message message);
        GLO_Message GetMessageByID(int messageid);
        #endregion

        #region Send
        IList<GLO_MessageSend> GetSendMessageList(int UserID);
        void MessageSendUpdate(GLO_MessageSend send);
        GLO_MessageSend GetMessageSendByID(int messageID);
        void SendMessageDelete(int messageID);
        #endregion

        #region Receive
        IList<GLO_MessageReceive> GetReceiveMessageList(int UserID);
        void MessageReceiveUpdate(GLO_MessageReceive Receive);
        GLO_MessageReceive GetMessageReceiveByID(int messageID);
        void ReceiveMessageDelete(int messageID);
        #endregion

        #region Archive
        IList<GLO_MessageArchive> GetArchiveMessageList(int UserID);
        void MessageArchiveUpdate(GLO_MessageArchive Archive);
        GLO_MessageArchive GetMessageArchiveByID(int messageID);
        void ArchiveMessageDelete(int messageID);
        #endregion

        #region Trash
        IList<GLO_MessageTrash> GetTrashMessageList(int UserID);
        void TrashMessageDelete(int messageID);
        void MessageTrashDeleteAll();
        #endregion

        #region Send Recommendations Email
        void SendRecommendationsEmail(int userID);
        #endregion

        #region System message
        void SendSystemMessage(GLO_Message message, string receiveid);
        #endregion
    }
}
