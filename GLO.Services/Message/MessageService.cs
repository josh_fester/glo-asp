﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;
namespace GLO.Services
{
    public class MessageService:IMessageService
    {
        private IMessageRepository _messageRepository;
        public MessageService(IMessageRepository MessageRepository)
        {
            _messageRepository = MessageRepository;
        }
        #region Message
        public void MessageCreate(GLO_Message message, int senduserid, string receiveid)
        {
            _messageRepository.MessageCreate(message, senduserid, receiveid);
        }
        public void MessageSave(GLO_Message message, int senduserid, string receiveid)
        {
            _messageRepository.MessageSave(message, senduserid, receiveid);
        }
        public IList<GLO_Message> GetMessageList()
        {
            return _messageRepository.GetMessageList();
        }
        public void MessageDelete(int id)
        {
            _messageRepository.MessageDelete(id);
        }
        public void MessageUpdate(GLO_Message message)
        {
            _messageRepository.MessageUpdate(message);
        }
        public GLO_Message GetMessageByID(int messageid)
        {
            return _messageRepository.GetMessageByID(messageid);
        }
        #endregion

        #region Send
        public IList<GLO_MessageSend> GetSendMessageList(int UserID)
        {
            return _messageRepository.GetSendMessageList(UserID);
        }
        public void MessageSendUpdate(GLO_MessageSend send)
        {
            _messageRepository.MessageSendUpdate(send);
        }
        public GLO_MessageSend GetMessageSendByID(int messageID)
        {
            return _messageRepository.GetMessageSendByID(messageID);
        }
        public void SendMessageDelete(int messageID)
        {
            _messageRepository.SendMessageDelete(messageID);
        }
        #endregion

        #region Receive
        public IList<GLO_MessageReceive> GetReceiveMessageList(int UserID)
        {
            return _messageRepository.GetReceiveMessageList(UserID);
        }
        public void MessageReceiveUpdate(GLO_MessageReceive Receive)
        {
            _messageRepository.MessageReceiveUpdate(Receive);
        }
        public GLO_MessageReceive GetMessageReceiveByID(int messageID)
        {
            return _messageRepository.GetMessageReceiveByID(messageID);
        }
        public void ReceiveMessageDelete(int messageID)
        {
            _messageRepository.ReceiveMessageDelete(messageID);
        }
        #endregion

        #region Archive
        public IList<GLO_MessageArchive> GetArchiveMessageList(int UserID)
        {
            return _messageRepository.GetArchiveMessageList(UserID);
        }
        public void MessageArchiveUpdate(GLO_MessageArchive Archive)
        {
            _messageRepository.MessageArchiveUpdate(Archive);
        }
        public GLO_MessageArchive GetMessageArchiveByID(int messageID)
        {
            return _messageRepository.GetMessageArchiveByID(messageID);
        }
        public void ArchiveMessageDelete(int messageID)
        {
            _messageRepository.MessageArchiveDelete(messageID);
        }
        #endregion

        #region Trash
        public IList<GLO_MessageTrash> GetTrashMessageList(int UserID)
        {
            return _messageRepository.GetTrashMessageList(UserID);
        }
        public void TrashMessageDelete(int messageID)
        {
            _messageRepository.MessageTrashDelete(messageID);
        }
        public void MessageTrashDeleteAll()
        {
            _messageRepository.MessageTrashDeleteAll();
        }
        #endregion

        #region Send Recommendations Email
        public void SendRecommendationsEmail(int userID)
        {
            _messageRepository.SendRecommendationsEmail(userID);
        }
        #endregion


        #region System message
        public void SendSystemMessage(GLO_Message message, string receiveid)
        {
            _messageRepository.SendSystemMessage(message,receiveid);
        }
        #endregion
    }
}
