﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Collections;
namespace GLO.Services
{
    public class DifferenceService:IDifferenceService
    {
        private IDifferenceRepository _responsitory;
        public DifferenceService(IDifferenceRepository DifferenceResponsitory)
        {
            _responsitory = DifferenceResponsitory;
        }

        public IList<GLO_Difference> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_Difference> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public void Add(GLO_Difference Difference)
        {
            _responsitory.Add(Difference);
        }

        public void Update(GLO_Difference Difference)
        {
            _responsitory.Update(Difference);
        }

        public GLO_Difference GetByID(int DifferenceID)
        {
            return _responsitory.GetByID(DifferenceID);
        }

        public void Delete(int DifferenceID)
        {
            _responsitory.Delete(DifferenceID);
        }

        public void ChangeStatus(int DifferenceID,bool status)
        {
            _responsitory.ChangeStatus(DifferenceID, status);
        }
    }
}
