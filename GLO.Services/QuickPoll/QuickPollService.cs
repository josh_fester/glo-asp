﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{   
    public class QuickPollService:IQuickPollService
    {
        private IQuickPollRepository _pollresponsitory;
        public QuickPollService(IQuickPollRepository PollResponsitory)
        {
            _pollresponsitory = PollResponsitory;
        }

        public void PollTopicAdd(GLO_PollTopic topic)
        {
            _pollresponsitory.PollTopicAdd(topic);
        }
        public void PollTopicEdit(GLO_PollTopic topic)
        {
            _pollresponsitory.PollTopicEdit(topic);
        }
        public IList<GLO_PollTopic> GetPollTopic()
        {
            return _pollresponsitory.GetPollTopic();
        }

        public void PollItemAdd(GLO_PollItem pollitem)
        {
            _pollresponsitory.PollItemAdd(pollitem);
        }
        public void PollItemEdit(GLO_PollItem pollitem)
        {
            _pollresponsitory.PollItemEdit(pollitem);
        }
        public IList<GLO_PollItem> GetPollItemByPollID(int id)
        {
            return _pollresponsitory.GetPollItemByPollID(id);
        }
        public GLO_PollItem GetPollItemByID(int id)
        {
            return _pollresponsitory.GetPollItemByID(id);
        }
    }
}
