﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Collections;
namespace GLO.Services
{
    public class FAQService:IFAQService
    {
        private IFAQRepository _responsitory;
        public FAQService(IFAQRepository FaqResponsitory)
        {
            _responsitory = FaqResponsitory;
        }

        public IList<GLO_FAQ> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_FAQ> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public void Add(GLO_FAQ faq)
        {
            _responsitory.Add(faq);
        }

        public void Update(GLO_FAQ faq)
        {
            _responsitory.Update(faq);
        }

        public GLO_FAQ GetByID(int faqID)
        {
            return _responsitory.GetByID(faqID);
        }

        public void Delete(int faqID)
        {
            _responsitory.Delete(faqID);
        }

        public void ChangeStatus(int faqID,bool status)
        {
            _responsitory.ChangeStatus(faqID, status);
        }
    }
}
