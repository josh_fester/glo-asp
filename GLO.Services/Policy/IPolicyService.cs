﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IPolicyService
    {
        IList<GLO_Policy> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Policy> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_Policy Policy);

        void Update(GLO_Policy Policy);

        GLO_Policy GetByID(int PolicyID);

        void Delete(int PolicyID);

        void ChangeStatus(int PolicyID, bool status);
    }
}
