﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Collections;
namespace GLO.Services
{
    public class PolicyService:IPolicyService
    {
        private IPolicyRepository _responsitory;
        public PolicyService(IPolicyRepository PolicyResponsitory)
        {
            _responsitory = PolicyResponsitory;
        }

        public IList<GLO_Policy> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_Policy> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public void Add(GLO_Policy Policy)
        {
            _responsitory.Add(Policy);
        }

        public void Update(GLO_Policy Policy)
        {
            _responsitory.Update(Policy);
        }

        public GLO_Policy GetByID(int PolicyID)
        {
            return _responsitory.GetByID(PolicyID);
        }

        public void Delete(int PolicyID)
        {
            _responsitory.Delete(PolicyID);
        }

        public void ChangeStatus(int PolicyID,bool status)
        {
            _responsitory.ChangeStatus(PolicyID, status);
        }
    }
}
