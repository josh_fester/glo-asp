﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IDifferenceService
    {
        IList<GLO_Difference> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Difference> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_Difference Difference);

        void Update(GLO_Difference Difference);

        GLO_Difference GetByID(int DifferenceID);

        void Delete(int DifferenceID);

        void ChangeStatus(int DifferenceID, bool status);
    }
}
