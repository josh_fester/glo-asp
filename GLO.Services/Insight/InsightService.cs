﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;
namespace GLO.Services
{
    public class InsightService:IInsightService
    {
        private IInsightRepository _insightRepository;
        public InsightService(IInsightRepository InsightRepository)
        {
            _insightRepository = InsightRepository;
        }

        public void Add(GLO_SurveyReport report)
        {
            _insightRepository.Add(report);
        }

        public void Update(GLO_SurveyReport report)
        {
            _insightRepository.Update(report);
        }

        public void Delete(int surveyid)
        {
            _insightRepository.Delete(surveyid);
        }

        public GLO_SurveyReport GetSurveyReportByID(int reportid)
        {
            return _insightRepository.GetSurveyReportByID(reportid);
        }
        public void ChangeStatus(int surveyid, bool status)
        {
            _insightRepository.ChangeStatus(surveyid, status);
        }

        public IList<GLO_SurveyReport> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _insightRepository.GetAllList(PageSize, PageIndex, out RecordCount);
        }

        public GLO_InsightPosts GetPostForEmailTemplate()
        {
            return _insightRepository.GetPostForEmailTemplate();
        }
        public void PostAdd(GLO_InsightPosts post)
        {
            _insightRepository.PostAdd(post);
        }

        public void PostUpdate(GLO_InsightPosts post)
        {
            _insightRepository.PostUpdate(post);
        }

        public void PostDelete(int postid)
        {
            _insightRepository.PostDelete(postid);
        }
        public GLO_InsightPosts GetPostByID(int postid)
        {
            return _insightRepository.GetPostByID(postid);
        }
        public IList<GLO_InsightPosts> GetPostList(int PageSize, int PageIndex, out int RecordCount, bool isRecommend = false)
        {
            return _insightRepository.GetPostList(PageSize, PageIndex, out RecordCount,isRecommend);
        }

        public void PostCommentAdd(GLO_InsightPostComment postcoment)
        {
            _insightRepository.PostCommentAdd(postcoment);
        }

        public void PostCommentDel(int postcommentid)
        {
            _insightRepository.PostCommentDel(postcommentid);
        }

        public IList<GLO_InsightPostComment> GetCommentList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _insightRepository.GetCommentList(PageSize, PageIndex, out RecordCount);
        }

        public IList<GLO_InsightPosts> SearchTagList(int PageSize, int PageIndex, out int RecordCount, string tag)
        {
            return _insightRepository.SearchTagList(PageSize, PageIndex, out RecordCount,tag);
        }

        public void PostTagAdd(GLO_InsightPostTag posttag)
        {
            _insightRepository.PostTagAdd(posttag);
        }

        public void PostTagDel(int tagid)
        {
            _insightRepository.PostTagDel(tagid);
        }
    }
}
