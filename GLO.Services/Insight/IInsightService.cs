﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IInsightService
    {
        void Add(GLO_SurveyReport report);

        void Update(GLO_SurveyReport report);

        void Delete(int surveyid);

        GLO_SurveyReport GetSurveyReportByID(int reportid);

        void ChangeStatus(int surveyid, bool status);

        IList<GLO_SurveyReport> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        void PostAdd(GLO_InsightPosts post);

        void PostUpdate(GLO_InsightPosts post);

        void PostDelete(int postid);

        GLO_InsightPosts GetPostByID(int postid);

        IList<GLO_InsightPosts> GetPostList(int PageSize, int PageIndex, out int RecordCount,bool isRecommend=false);

        GLO_InsightPosts GetPostForEmailTemplate();

        void PostCommentAdd(GLO_InsightPostComment postcoment);

        void PostCommentDel(int postcommentid);

        IList<GLO_InsightPostComment> GetCommentList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_InsightPosts> SearchTagList(int PageSize, int PageIndex, out int RecordCount, string tag);

        void PostTagAdd(GLO_InsightPostTag posttag);

        void PostTagDel(int tagid);
    }
}
