﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public class LeaderService:ILeaderService
    {
        private ILeaderRepository _leaderRepository;
        public LeaderService(ILeaderRepository LeaderRepository)
        {
            _leaderRepository = LeaderRepository;
        }
        public void LeaderRegister(GLO_Leader leader)
        {
            _leaderRepository.LeaderRegister(leader);
        }
        public void LeaderUpdate(GLO_Leader leader)
        {
            _leaderRepository.LeaderUpdate(leader);
        }
        public IList<GLO_Users> GetUserList(int type)
        {
            return _leaderRepository.GetUserList(type);
        }
      
        public IList<GLO_Leader> RecommendLeaderList(int count)
        {
            return _leaderRepository.RecommendLeaderList(count);
        }

        public void TagJob(GLO_JobTag jobtag)
        {
            _leaderRepository.TagJob(jobtag);
        }

        public void UpdateTagJob(int tagjobid)
        {
            _leaderRepository.UpdateTagJob(tagjobid);
        }

        public GLO_JobTag GetJobTagByID(int jobid, int userid)
        {
            return _leaderRepository.GetJobTagByID(jobid, userid);
        }

        public void RemoveTagJob(int tagjobid)
        {
            _leaderRepository.RemoveTagJob(tagjobid);
        }

        public void AddResumes(int userID, List<string> resumeNameList)
        {
            _leaderRepository.AddResumes(userID, resumeNameList);
        }
        public Guid AddResume(int userID, string resumeName)
        {
           return _leaderRepository.AddResume(userID, resumeName);
        }
        public void DeleteResume(Guid resumeID)
        {
            _leaderRepository.DeleteResume(resumeID);
        }
        #region key word
        public void AddKeyWord(GLO_LeaderKeyWord newData)
        {
            _leaderRepository.AddKeyWord(newData);
        }
        public void UpdateKeyWord(GLO_LeaderKeyWord newData)
        {
            _leaderRepository.UpdateKeyWord(newData);
        }
        public List<GLO_LeaderKeyWord> GetKeyWordList(int userID)
        {
            return _leaderRepository.GetKeyWordList(userID);
        }
        public void DeleteKeyWord(Guid keyWordID)
        {
            _leaderRepository.DeleteKeyWord(keyWordID);
        }
        public GLO_LeaderKeyWord GetKeyWordByContent(int userID, string content)
        {
            return _leaderRepository.GetKeyWordByContent(userID, content);
        }
        #endregion

        public List<GLO_LeaderInterViewQuestion> GetInterviewQuestion()
        {
            return _leaderRepository.GetInterviewQuestion();
        }

        public void AddInterview(int userID, List<GLO_LeaderInterviewAnswer> newList)
        {
            _leaderRepository.AddInterview(userID, newList);
        }
    }
}
