﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public class ExpertService:IExpertService
    {
        private IExpertRepository _expertReository;
        public ExpertService(IExpertRepository ExpertRepository)
        {
            _expertReository = ExpertRepository;
        }

        #region expert operation
        public void HRExpertAdd(GLO_HRExpert expert)
        {
            _expertReository.HRExpertAdd(expert);
        }
        public void HRExpertUpdate(GLO_HRExpert expert)
        {
            _expertReository.HRExpertUpdate(expert);
        }
        public GLO_HRExpert GetExpertByID(int userid)
        {
            return _expertReository.GetExpertByID(userid);
        }
        public IList<GLO_HRExpert> RecommendExpert(int count)
        {
            return _expertReository.RecommendExpert(count);
        }
        public int ExpertValueAdd(GLO_ExpertValueDic expertDic)
        {
           return _expertReository.ExpertValueAdd(expertDic);
        }
        public GLO_ExpertValueDic GetExpertValueDicByValue(string value)
        {
            return _expertReository.GetExpertValueDicByValue(value);
        }
        public Guid ExpertRelationalAdd(int userID, int valueID)
        {
            return  _expertReository.ExpertRelationalAdd(userID, valueID);
        }
        public void ExpertRelationalAdd(int expertID, List<GLO_ExpertRelational> expertRelationalList)
        {
            _expertReository.ExpertRelationalAdd(expertID,expertRelationalList);
        }
        public void ExpertRelationalDelete(Guid relationalID)
        {
            _expertReository.ExpertRelationalDelete(relationalID);
        }
        public List<GLO_ExpertValueDic> GetExpertValueList()
        {
            return _expertReository.GetExpertValueList();
        }
        #endregion  

        #region assignment
        public void ExpertProjectAdd(GLO_ExpertProject project)
        {
            _expertReository.ExpertProjectAdd(project);
        }
        public void ExpertProjectDelete(int projectid)
        {
            _expertReository.ExpertProjectDelete(projectid);
        }
        public GLO_ExpertProject GetProjectByID(int projectid)
        {
            return _expertReository.GetProjectByID(projectid);
        }
        public void ExpertProjectEdit(GLO_ExpertProject project)
        {
            _expertReository.ExpertProjectEdit(project);
        }
        #endregion 

        #region recommendation
        public GLO_Recommendation GetRecommendationByID(int recommendid)
        {
            return _expertReository.GetRecommendationByID(recommendid);
        }
        public void RecommendationAdd(GLO_Recommendation recommendation)
        {
            _expertReository.RecommendationAdd(recommendation);
        }
        public void RecommendationDelete(int recommendid)
        {
            _expertReository.RecommendationDelete(recommendid);
        }
        #endregion

        #region competence

        public void CarerrSummaryAdd(GLO_CareerSummary summary)
        {
            _expertReository.CarerrSummaryAdd(summary);
        }
        public void EducationAdd(GLO_Education education)
        {
            _expertReository.EducationAdd(education);
        }
        public void ContributionAdd(GLO_Contribution contribution)
        {
            _expertReository.ContributionAdd(contribution);
        }

        public void CarerrSummaryDelete(int carerrid)
        {
            _expertReository.CarerrSummaryDelete(carerrid);
        }

        public void EducationDelete(int educationid)
        {
            _expertReository.EducationDelete(educationid);
        }

        public void ContributionDelete(int contributid)
        {
            _expertReository.ContributionDelete(contributid);
        }
        #endregion
    }
}
