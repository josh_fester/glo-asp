﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IUserService
    {
        IList<GLO_Users> GetRecommendUser(string count, int typeid);
    }
}
