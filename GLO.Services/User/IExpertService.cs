﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IExpertService
    {
        void HRExpertAdd(GLO_HRExpert expert);

        void HRExpertUpdate(GLO_HRExpert expert);

        GLO_HRExpert GetExpertByID(int userid);

        IList<GLO_HRExpert> RecommendExpert(int count);

        int ExpertValueAdd(GLO_ExpertValueDic expertDic);

        GLO_ExpertValueDic GetExpertValueDicByValue(string value);
        Guid ExpertRelationalAdd(int userID, int valueID);
        void ExpertRelationalAdd(int expertID, List<GLO_ExpertRelational> expertRelationalList);
        void ExpertRelationalDelete(Guid relationalID);
        List<GLO_ExpertValueDic> GetExpertValueList();

        void ExpertProjectAdd(GLO_ExpertProject project);

        void ExpertProjectDelete(int projectid);
       
        GLO_ExpertProject GetProjectByID(int projectid);

        GLO_Recommendation GetRecommendationByID(int recommendid);

        void RecommendationAdd(GLO_Recommendation recommendation);

        void RecommendationDelete(int recommendid);

        void ExpertProjectEdit(GLO_ExpertProject project);

        #region competence

        void CarerrSummaryAdd(GLO_CareerSummary summary);

        void EducationAdd(GLO_Education education);

        void ContributionAdd(GLO_Contribution contribution);

        void CarerrSummaryDelete(int carerrid);

        void EducationDelete(int educationid);

        void ContributionDelete(int contributid);
        #endregion
    }
}
