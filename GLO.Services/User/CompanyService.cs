﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public class CompanyService:ICompanyService
    {
        private ICompanyRepository _companyRepository;
        public CompanyService(ICompanyRepository CompanyRepository)
        {
            _companyRepository = CompanyRepository;
        }
        public GLO_Company GetCompanyByID(int companyid)
        {
            return _companyRepository.GetCompanyByID(companyid);
        }
        public void CompanyAdd(GLO_Company company)
        {
            _companyRepository.CompanyAdd(company);
        }
        public void CompanyUpdate(GLO_Company company)
        {
            _companyRepository.CompanyUpdate(company);
        }
        public IList<GLO_CompanyJob> GetCompanyJob(int companyid, string keyword, string location, string func)
        {
            return _companyRepository.GetCompanyJob(companyid,keyword,location,func);
        }
        public IList<GLO_CompanyJob> SearchJob(string keyword, string location, string func, string industry = "", string education = "", string date = "")
        {
            return _companyRepository.SearchJob(keyword, location, func,industry,education,date);
        }
        public GLO_CompanyJob GetCompanyJobByID(int jobid)
        {
            return _companyRepository.GetCompanyJobByID(jobid);
        }
        public IList<GLO_Company> RecommendCompanyList(int count)
        {
            return _companyRepository.RecommendCompanyList(count);
        }

        public void AssessmentAdd(GLO_Assessment assessment)
        {
            _companyRepository.AssessmentAdd(assessment);
        }

        public void HuntingAdd(GLO_HuntingRequest huntingrequest)
        {
            _companyRepository.HuntingAdd(huntingrequest);
        }

        public void HuntingEdit(GLO_HuntingRequest huntingrequest)
        {
            _companyRepository.HuntingEdit(huntingrequest);
        }

        public void HuntinDelete(int huntingid)
        {
            _companyRepository.HuntinDelete(huntingid);
        }

        public void JobAdd(GLO_CompanyJob companyjob)
        {
            _companyRepository.JobAdd(companyjob);
        }

        public void JobEdit(GLO_CompanyJob companyjob)
        {
            _companyRepository.JobEdit(companyjob);
        }
        public void JobDelete(int jobid)
        {
            _companyRepository.JobDelete(jobid);
        }
        public GLO_HuntingRequest GetHuntingByID(int huntingid)
        {
            return _companyRepository.GetHuntingByID(huntingid);
        }
        public GLO_CompanyType GetCompanyTypeByID(int typeid)
        {
            return _companyRepository.GetCompanyTypeByID(typeid);
        }
        public List<GLO_Company> GetCompanyList()
        {
            return _companyRepository.GetCompanyList();
        }
    }
}
