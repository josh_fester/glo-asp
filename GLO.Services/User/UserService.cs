﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
namespace GLO.Services
{
    public class UserService:IUserService
    {
        private IUserRepository _userRepository;
        public UserService(IUserRepository UserRepository)
        {
            _userRepository = UserRepository;
        }
        public IList<GLO_Users> GetRecommendUser(string count, int typeid)
        {
            return _userRepository.GetRecommendUser(count,typeid);
        }
    }
}
