﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;

namespace GLO.Services
{
    public class SuccessService:ISuccessService
    {
        ISuccessRepository _successRepository;
        public SuccessService(ISuccessRepository SuccessRepository)
        {
            _successRepository = SuccessRepository;
        }
        public void AddSuccessCase(GLO_Success success)
        {
            _successRepository.AddSuccessCase(success);
        }
        public void UpdateSuccessCase(GLO_Success success)
        {
            _successRepository.UpdateSuccessCase(success);
        }
        public void DeleteSuccessCase(int successid)
        {
            _successRepository.DeleteSuccessCase(successid);
        }
        public GLO_Success GetSuccessCaseByID(int successid)
        {
            return _successRepository.GetSuccessCaseByID(successid);
        }
        public IList<GLO_Success> GetSuccessCaseList()
        {
            return _successRepository.GetSuccessCaseList();
        }
        public IList<GLO_Success> GetSuccessCaseList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _successRepository.GetSuccessCaseList(PageSize,PageIndex,out RecordCount);
        }
    }
}
