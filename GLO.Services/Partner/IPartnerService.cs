﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IPartnerService
    {
        IList<GLO_Partner> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Partner> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_Partner Partner);

        void Update(GLO_Partner Partner);

        GLO_Partner GetByID(int PartnerID);

        void Delete(int PartnerID);

        void ChangeStatus(int PartnerID, bool status);
    }
}
