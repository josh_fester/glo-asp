﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;
namespace GLO.Services
{
    public class PartnerService:IPartnerService
    {
        private IPartnerRepository _responsitory;
        public PartnerService(IPartnerRepository PartnerResponsitory)
        {
            _responsitory = PartnerResponsitory;
        }

        public IList<GLO_Partner> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_Partner> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public void Add(GLO_Partner Partner)
        {
            _responsitory.Add(Partner);
        }

        public void Update(GLO_Partner Partner)
        {
            _responsitory.Update(Partner);
        }

        public GLO_Partner GetByID(int PartnerID)
        {
            return _responsitory.GetByID(PartnerID);
        }

        public void Delete(int PartnerID)
        {
            _responsitory.Delete(PartnerID);
        }

        public void ChangeStatus(int PartnerID,bool status)
        {
            _responsitory.ChangeStatus(PartnerID, status);
        }
    }
}
