﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;
using System.Web;
using System.Security.Cryptography;

namespace GLO.Services
{
    public class AccountService:IAccountService
    {
        //returns null on invalid, the member_id on valid
        private String ValidateLinkedInOauthCookie(String cookie)
        {
            String decoded_cookie = System.Web.HttpUtility.UrlDecode(cookie);

            Dictionary<String, Object> dict = System.Web.Helpers.Json.Decode(decoded_cookie);
            System.Web.Helpers.DynamicJsonArray signature_order = (System.Web.Helpers.DynamicJsonArray)dict["signature_order"];

            String base_string = "";
            foreach (String key in signature_order)
            {
                base_string += dict[key];
            }

            //hmac
            Encoding encoding = Encoding.UTF8;
            var secret = System.Configuration.ConfigurationManager.AppSettings["linkedinApiSecret"];
            var secret_bytes = encoding.GetBytes(secret);
            var hmac1 = new HMACSHA1(secret_bytes);
            byte[] buffer = hmac1.ComputeHash(encoding.GetBytes(base_string));

            String calculated_signature = System.Convert.ToBase64String(buffer);
            String provided_signature = (String)dict["signature"];

            if (!String.Equals(calculated_signature, provided_signature))
            {
                return null;
            }
            else
            {
                return (String)dict["member_id"];
            }
        }

        public String GetLinkedinMemberId(HttpCookie cookie)
        {
            if (cookie == null) {
                return "";
            }
            String cookie_value = cookie.Value;

            return this.ValidateLinkedInOauthCookie(cookie_value);
        }
        IAccountRepository _accountRepository;
        public AccountService(IAccountRepository AccountRepository)
        {
            _accountRepository = AccountRepository;
        }
        public bool CheckUserName(string username)
        {
            return _accountRepository.CheckUserName(username);
        }

        public bool CheckEmail(string email)
        {
            return _accountRepository.CheckEmail(email);
        }

        public LoginStatus UserLogin(string username, string password, string loginip, HttpCookie linkedin_cookie)
        {

            if (String.Equals(password, System.Configuration.ConfigurationManager.AppSettings["linkedinPasswordSignal"]))
            {
                password = this.GetLinkedinMemberId(linkedin_cookie);
            }

            return _accountRepository.UserLogin(username, password, loginip);
        }
        public LoginStatus LeaderLogin(int userId, string loginip)
        {
            return _accountRepository.LeaderLogin(userId, loginip);
        }
        public GLO_Users GetUserByUserID(int userid)
        {
            return _accountRepository.GetUserByUserID(userid);
        }
        public GLO_Users GetUserByUserName(string username)
        {
            return _accountRepository.GetUserByUserName(username);
        }
        public GLO_Users GetUserByNickName(string nickname)
        {
            return _accountRepository.GetUserByNickName(nickname);
        }
        public void ChangePassWord(GLO_Users user)
        {
            _accountRepository.ChangePassWord(user);
        }
        public void ApprovedUser(int userid, bool isapproved)
        {
            _accountRepository.ApprovedUser(userid, isapproved);
        }
        public void UserUpdate(GLO_Users user)
        {
            _accountRepository.UserUpdate(user);
        }
        public void UserRegister(GLO_Users user)
        {
            _accountRepository.UserRegister(user);
        }
        public void ExpertRegister(GLO_Users user, GLO_HRExpert expert, GLO_UserIndustry industry,GLO_UserLocation location)
        {
            _accountRepository.ExpertRegister(user,expert,industry,location);
        }

        public IList<GLO_Users> GetUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type)
        {
            return _accountRepository.GetUserList(searchText,PageSize, PageIndex, out RecordCount, Type);
        }

        public IList<GLO_Users> GetNotApprovedUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type)
        {
            return _accountRepository.GetNotApprovedUserList(searchText, PageSize, PageIndex, out RecordCount, Type);
        }

        public void DeleteUser(int userid)
        {
            _accountRepository.DeleteUser(userid);
        }

        #region payment

        public void PayMentAdd(GLO_UserPayMent payment)
        {
            _accountRepository.PayMentAdd(payment);
        }

        public void PayMentUpdate(GLO_UserPayMent payment)
        {
            _accountRepository.PayMentUpdate(payment);
        }

        public IList<GLO_UserPayMent> GetPayMentList()
        {
            return _accountRepository.GetPayMentList();
        }

        public GLO_UserPayMent GetPayMentByOrderID(string orderid)
        {
            return _accountRepository.GetPayMentByOrderID(orderid);
        }

        #endregion

        //for admin

        public IList<GLO_Users> GetAdminList()
        {
            return _accountRepository.GetAdminList();
        }
        public List<GLO_Users> GetAllUserList()
        {
            return _accountRepository.GetAllUserList();
        }
        public List<GLO_Users> GetAllCompanyList()
        {
            return _accountRepository.GetAllCompanyList();
        }
        public List<GLO_Users> GetAllLeaderList()
        {
            return _accountRepository.GetAllLeaderList();
        }
        public List<GLO_Users> GetAllLeaderListByEmail(string email)
        {
            return _accountRepository.GetAllLeaderListByEmail(email);
        }
        public List<GLO_Users> GetAllExpertList()
        {
            return _accountRepository.GetAllExpertList();
        }
        public List<GLO_Users> SearchUserList(string keyWord, int Type, string location,string industry="")
        {
            return _accountRepository.SearchUserList(keyWord, Type, location,industry);
        }

        public void UpdateUserEx(GLO_UserEx userEx)
        {
            _accountRepository.UpdateUserEx(userEx);
        }

        public List<GLO_SecurityQuestion> GetQuestionList()
        {
            return _accountRepository.GetQuestionList();
        }

        #region Recommendation
        public GLO_Recommendation GetRecommendationByUserID(int userID, int recommendUserID)
        {
            return _accountRepository.GetRecommendationByUserID(userID, recommendUserID);
        }
        public void UpdateRecommendation(GLO_Recommendation newData)
        {
            _accountRepository.UpdateRecommendation(newData);
        }
        #endregion

        public void UpdateIndustry(GLO_UserIndustry newData)
        {
            _accountRepository.UpdateIndustry(newData);
        }

        public void UpdateLocation(GLO_UserLocation newData)
        {
            _accountRepository.UpdateLocation(newData);
        }
    }
}
