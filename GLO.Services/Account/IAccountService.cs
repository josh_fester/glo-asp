﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using System.Web;

namespace GLO.Services
{
    public interface IAccountService
    {
        bool CheckUserName(string username);

        bool CheckEmail(string email);

        String GetLinkedinMemberId(HttpCookie linkedin_cookie);

        LoginStatus UserLogin(string username, string password, string loginip, HttpCookie linkedin_cookie);

        LoginStatus LeaderLogin(int userId, string loginip);

        GLO_Users GetUserByUserID(int userid);

        GLO_Users GetUserByUserName(string username);

        GLO_Users GetUserByNickName(string nikename);

        void ChangePassWord(GLO_Users user);

        void ApprovedUser(int userid, bool isapproved);

        void UserUpdate(GLO_Users user);

        void UserRegister(GLO_Users user);

        void ExpertRegister(GLO_Users user, GLO_HRExpert expert, GLO_UserIndustry industry,GLO_UserLocation location);

        IList<GLO_Users> GetUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type);

        IList<GLO_Users> GetNotApprovedUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type);

        void DeleteUser(int userid);

        //for payment

        void PayMentAdd(GLO_UserPayMent payment);

        void PayMentUpdate(GLO_UserPayMent payment);

        IList<GLO_UserPayMent> GetPayMentList();

        GLO_UserPayMent GetPayMentByOrderID(string orderid);

        //for admin

        IList<GLO_Users> GetAdminList();

        List<GLO_Users> GetAllUserList();

        List<GLO_Users> GetAllCompanyList();

        List<GLO_Users> GetAllLeaderList();

        List<GLO_Users> GetAllLeaderListByEmail(string email);

        List<GLO_Users> GetAllExpertList();

        List<GLO_Users> SearchUserList(string keyWord, int Type, string location,string industry="");

        void UpdateUserEx(GLO_UserEx userEx);

        List<GLO_SecurityQuestion> GetQuestionList();

        GLO_Recommendation GetRecommendationByUserID(int userID, int recommendUserID);

        void UpdateRecommendation(GLO_Recommendation newData);

        void UpdateIndustry(GLO_UserIndustry newData);

        void UpdateLocation(GLO_UserLocation newData);
    }
}
