﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IFAQService
    {
        IList<GLO_FAQ> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_FAQ> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_FAQ faq);

        void Update(GLO_FAQ faq);

        GLO_FAQ GetByID(int faqID);

        void Delete(int faqID);

        void ChangeStatus(int faqID, bool status);
    }
}
