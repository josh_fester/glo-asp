﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface IFeedBackService
    {
        IList<GLO_FeedBack> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_FeedBack> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_FeedBack FeedBack);

        void Update(GLO_FeedBack FeedBack);

        GLO_FeedBack GetByID(int FeedBackID);

        void Delete(int FeedBackID);

        void ChangeStatus(int FeedBackID, bool status);
    }
}
