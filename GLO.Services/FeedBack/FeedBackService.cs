﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Collections;
namespace GLO.Services
{
    public class FeedBackService:IFeedBackService
    {
        private IFeedBackRepository _responsitory;
        public FeedBackService(IFeedBackRepository FeedBackResponsitory)
        {
            _responsitory = FeedBackResponsitory;
        }

        public IList<GLO_FeedBack> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_FeedBack> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public void Add(GLO_FeedBack FeedBack)
        {
            _responsitory.Add(FeedBack);
        }

        public void Update(GLO_FeedBack FeedBack)
        {
            _responsitory.Update(FeedBack);
        }

        public GLO_FeedBack GetByID(int FeedBackID)
        {
            return _responsitory.GetByID(FeedBackID);
        }

        public void Delete(int FeedBackID)
        {
            _responsitory.Delete(FeedBackID);
        }

        public void ChangeStatus(int FeedBackID,bool status)
        {
            _responsitory.ChangeStatus(FeedBackID, status);
        }
    }
}
