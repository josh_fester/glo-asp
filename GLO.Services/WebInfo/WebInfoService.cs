﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public class WebInfoService:IWebInfoService
    {
        private IWebInfoRepository _webInfoRepository;
        public WebInfoService(IWebInfoRepository WebInfoRepository)
        {
            _webInfoRepository = WebInfoRepository;
        }
        public GLO_WebsiteInfo GetWebSiteInfo()
        {
            return _webInfoRepository.GetWebSiteInfo();
        }
        public void UpdateWebInfo(GLO_WebsiteInfo webinfo)
        {
            _webInfoRepository.UpdateWebInfo(webinfo);
        }

        public IList<GLO_Location> GetLocationByParentID(int parentid)
        {
            return _webInfoRepository.GetLocationByParentID(parentid);
        }

        public GLO_Location GetLocationByID(int lid)
        {
            return _webInfoRepository.GetLocationByID(lid);
        }

        public void LocationEdit(GLO_Location location)
        {
            _webInfoRepository.LocationEdit(location);
        }
        public void LocationAdd(GLO_Location location)
        {
            _webInfoRepository.LocationAdd(location);
        }

        public void LocationDelete(int locationid)
        {
            _webInfoRepository.LocationDelete(locationid);
        }

        public IList<GLO_IndustryCategory> GetIndustyByParentID(int parentid)
        {
            return _webInfoRepository.GetIndustyByParentID(parentid);
        }

        public GLO_IndustryCategory GetIndustyByID(int industryid)
        {
            return _webInfoRepository.GetIndustyByID(industryid);
        }

        public void IndustryAdd(GLO_IndustryCategory industry)
        {
            _webInfoRepository.IndustryAdd(industry);
        }

        public void IndustryEdit(GLO_IndustryCategory industry)
        {
            _webInfoRepository.IndustryEdit(industry);
        }

        public void IndustryDelete(int industryid)
        {
            _webInfoRepository.IndustryDelete(industryid);
        }

    }
}
