﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;
namespace GLO.Services
{
    public class CompetenceService : ICompetenceService
    {
        private ICompetenceRepository _responsitory;
        public CompetenceService(ICompetenceRepository CompetenceRepository)
        {
            _responsitory = CompetenceRepository;
        }

        #region contribution
        public GLO_Contribution GetContributionByID(int contributionid)
        {
            return _responsitory.GetContributionByID(contributionid);
        }

        public void ContributionAdd(GLO_Contribution contribution)
        { 
            _responsitory.ContributionAdd(contribution);
        }

        public void ContributionEdit(GLO_Contribution contribution)
        {           
            _responsitory.ContributionEdit(contribution);
        }

        public void ContributionDel(int contributionid)
        {
            _responsitory.ContributionDel(contributionid);           
        }
        #endregion

        #region education
        public GLO_Education GetEducationByID(int educationid)
        {
            return _responsitory.GetEducationByID(educationid);
        }

        public void EducationAdd(GLO_Education education)
        {
            _responsitory.EducationAdd(education);
        }

        public void EducationEidt(GLO_Education education)
        {
            _responsitory.EducationEidt(education);
        }

        public void EducationDel(int educationid)
        {
            _responsitory.EducationDel(educationid);
        }
        #endregion

        #region career summary
        public GLO_CareerSummary GetCareerByID(int careerid)
        {
            return _responsitory.GetCareerByID(careerid);
        }

        public void CarerrSummaryAdd(GLO_CareerSummary summary)
        {
            _responsitory.CarerrSummaryAdd(summary);
        }

        public void CareerSummaryEdit(GLO_CareerSummary careersummary)
        {
            _responsitory.CareerSummaryEdit(careersummary);
        }

        public void CareerSummaryDel(int careerid)
        {
            _responsitory.CareerSummaryDel(careerid);
        }
        #endregion
        
        #region Competency Management
        public List<GLO_Competencies> GetCompetenciesList(int userID)
        {
            return _responsitory.GetCompetenciesList(userID);
        }
        public List<GLO_Competencies> GetOrhterCompetenciesList(int userID)
        {
            return _responsitory.GetOrhterCompetenciesList(userID);
        }
        public Guid CompetencyAdd(GLO_Competencies competency)
        {
            return _responsitory.CompetencyAdd(competency);
        }
        public void CompetencyAddForRegister(int userID,int typeID=1)
        {
            _responsitory.CompetencyAddForRegister(userID,typeID);
        }
        public void CompetencyEdit(GLO_Competencies competency)
        {
            _responsitory.CompetencyEdit(competency);
        }
        public void CompetencyDelete(Guid competencyID)
        {
            _responsitory.CompetencyDelete(competencyID);
        }
        public GLO_Competencies GetCompetencyByID(Guid competencyID)
        {
            return _responsitory.GetCompetencyByID(competencyID);
        }
        #endregion
    }
}
