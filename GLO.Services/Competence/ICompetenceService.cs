﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface ICompetenceService
    {

        GLO_Contribution GetContributionByID(int contributionid);

        void ContributionAdd(GLO_Contribution contribution);

        void ContributionEdit(GLO_Contribution contribution);

        void ContributionDel(int contributionid);


        GLO_Education GetEducationByID(int educationid);

        void EducationAdd(GLO_Education education);

        void EducationEidt(GLO_Education education);

        void EducationDel(int educationid);


        GLO_CareerSummary GetCareerByID(int careerid);

        void CarerrSummaryAdd(GLO_CareerSummary summary);

        void CareerSummaryEdit(GLO_CareerSummary careersummary);

        void CareerSummaryDel(int careerid);

        #region Competency Management
        List<GLO_Competencies> GetCompetenciesList(int userID);
        List<GLO_Competencies> GetOrhterCompetenciesList(int userID);
        Guid CompetencyAdd(GLO_Competencies competency);
        void CompetencyAddForRegister(int userID,int typeID=1);
        void CompetencyEdit(GLO_Competencies competency);
        void CompetencyDelete(Guid competencyID);
        GLO_Competencies GetCompetencyByID(Guid competencyID);
        #endregion
    }
}
