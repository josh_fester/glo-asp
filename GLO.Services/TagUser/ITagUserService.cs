﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface ITagUserService
    {
        void AddTagUsers(GLO_TagUsers taguser);
        void UpdateTagUsers(GLO_TagUsers taguser);
        GLO_TagUsers GetTagUsersByID(int tagID);
        List<GLO_TagUsers> GetTaggedUserList(int userID);
        void DeleteTagUsers(int tagID);

        List<GLO_RecentContact> GetRecentContactList(int userid);
        
        IList<GLO_TagUsers> GetTagListByID(int userid, int type);

        IList<GLO_Users> TheyTagMe(int userid, int type = 0);

        IList<GLO_TagUsers> GetTagListByCategory(int categoryid, int type);

        void DeleteTagUser(int tagid);

        #region Tag Category

        void FirstAddTagCategory(int userID, int userType);

        int AddTagCategory(GLO_TagCategory data);

        void RenameTagCategory(int categoryID, string newName);

        bool DeleteTagCategory(int categoryID);

        List<GLO_TagCategory> GetTagCategoryList(int userID);

        GLO_TagCategory GetTagCategoryByName(int userID, Guid functionID);

        #endregion

        #region Functions

        Guid AddFunction(GLO_Functions data);
        
        GLO_Functions GetFunctionByName(string functionName);

        GLO_Functions GetFunctionByID(Guid functionid);

        void FunctionEdit(GLO_Functions function);

        void FunctionDelete(Guid functionid);

        IList<GLO_Functions> GetSystemFunctionList();
        #endregion
    }
}
