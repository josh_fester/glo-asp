﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Services
{
    public class TagUserService : ITagUserService
    {
        private ITagUserRepository _taguserRepository;
        public TagUserService(ITagUserRepository TagUserRepository)
        {
            _taguserRepository = TagUserRepository;
        }
        #region  Tag Users
        public void AddTagUsers(GLO_TagUsers taguser)
        {
            _taguserRepository.AddTagUsers(taguser);
        }
        public void UpdateTagUsers(GLO_TagUsers taguser)
        {
            _taguserRepository.UpdateTagUsers(taguser);
        }
        public GLO_TagUsers GetTagUsersByID(int tagID)
        {
            return _taguserRepository.GetTagUsersByID(tagID);
        }
        public List<GLO_TagUsers> GetTaggedUserList(int userID)
        {
            return _taguserRepository.GetTaggedUserList(userID);
        }
        public void DeleteTagUsers(int tagID)
        {
            _taguserRepository.DeleteTagUsers(tagID);
        }
        #endregion
        public List<GLO_RecentContact> GetRecentContactList(int userid)
        {
            return _taguserRepository.GetRecentContactList(userid);
        }
        
        public IList<GLO_TagUsers> GetTagListByID(int userid, int type = 0)
        {
            return _taguserRepository.GetTagListByID(userid, type);
        }

        public IList<GLO_Users> TheyTagMe(int userid, int type)
        {
            return _taguserRepository.TheyTagMe(userid, type);
        }

        public IList<GLO_TagUsers> GetTagListByCategory(int categoryid, int type)
        {
            return _taguserRepository.GetTagListByCategory(categoryid, type);
        }

        #region Tag Category

        public void FirstAddTagCategory(int userID, int userType)
        {
            _taguserRepository.FirstAddTagCategory(userID, userType);
        }

        public int AddTagCategory(GLO_TagCategory data)
        {
            return _taguserRepository.AddTagCategory(data);
        }
        public void RenameTagCategory(int categoryID, string newName)
        {
            _taguserRepository.RenameTagCategory(categoryID, newName);
        }
        public bool DeleteTagCategory(int categoryID)
        {
            return _taguserRepository.DeleteTagCategory(categoryID);
        }
        public List<GLO_TagCategory> GetTagCategoryList(int userID)
        {
            return _taguserRepository.GetTagCategoryList(userID);
        }
        public GLO_TagCategory GetTagCategoryByName(int userID, Guid functionID)
        {
            return _taguserRepository.GetTagCategoryByName(userID, functionID);
        }
        #endregion

        #region Functions
        public Guid AddFunction(GLO_Functions data)
        {
            return _taguserRepository.AddFunction(data);
        }
        public GLO_Functions GetFunctionByName(string functionName)
        {
            return _taguserRepository.GetFunctionByName(functionName);
        }
        public GLO_Functions GetFunctionByID(Guid functionid)
        {
            return _taguserRepository.GetFunctionByID(functionid);
        }

        public void FunctionEdit(GLO_Functions function)
        {
            _taguserRepository.FunctionEdit(function); 
        }

        public void FunctionDelete(Guid functionid)
        {
            _taguserRepository.FunctionDelete(functionid); 
        }
        #endregion

        public void DeleteTagUser(int tagid)
        {
            _taguserRepository.DeleteTagUser(tagid);
        }

        public IList<GLO_Functions> GetSystemFunctionList()
        {
            return _taguserRepository.GetSystemFunctionList();
        }
    }
}
