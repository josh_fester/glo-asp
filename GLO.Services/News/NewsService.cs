﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using GLO.Data;

namespace GLO.Services
{
    public class NewsService:INewsService
    {
        private INewsRepository _newsresponsitory;
        public NewsService(INewsRepository NewsResponsitory)
        {
            _newsresponsitory = NewsResponsitory;
        }
        public int AddNewsCategory(GLO_NewsCategory category)
        {
            return _newsresponsitory.AddNewsCategory(category);
        }
        public IEnumerable<GLO_NewsCategory> GetCategoryCollection()
        {
            return _newsresponsitory.GetCategoryCollection();
        }
        public void AddNews(GLO_News news)
        {
            _newsresponsitory.AddNews(news);
        }
        public IList<GLO_News> GetNewsList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _newsresponsitory.GetNewsList(PageSize, PageIndex, out RecordCount).ToList();
        }
        public IList<GLO_News> GetNewsEventsList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _newsresponsitory.GetNewsEventsList(PageSize, PageIndex, out RecordCount).ToList();
        }
        public IList<GLO_News> GetNewsCalendarList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _newsresponsitory.GetNewsCalendarList(PageSize, PageIndex, out RecordCount).ToList();
        }
        public IList<GLO_News> GetNewsPressList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _newsresponsitory.GetNewsPressList(PageSize, PageIndex, out RecordCount).ToList();
        }
        public IList<GLO_News> GetNewsFileList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _newsresponsitory.GetNewsFileList(PageSize, PageIndex, out RecordCount).ToList();
        }
        public GLO_News GetNewsByID(int newsid)
        {
            return _newsresponsitory.GetNewsByID(newsid);
        }
        public void UpadateNews(GLO_News news)
        {
            _newsresponsitory.UpadateNews(news);
        }
        public void DeleteNews(int newsid)
        {
            _newsresponsitory.DeleteNews(newsid);
        }
        public void ChangeStatus(int newsid, bool status)
        {
            _newsresponsitory.ChangeStatus(newsid, status);
        }
    }
}
