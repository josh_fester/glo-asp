﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;


namespace GLO.Services
{
    public interface INewsService
    {
        int AddNewsCategory(GLO_NewsCategory category);

        IEnumerable<GLO_NewsCategory> GetCategoryCollection();

        void AddNews(GLO_News news);
        
        IList<GLO_News> GetNewsList(int PageSize, int PageIndex, out int RecordCount);
        
        IList<GLO_News> GetNewsEventsList(int PageSize, int PageIndex, out int RecordCount);
        
        IList<GLO_News> GetNewsCalendarList(int PageSize, int PageIndex, out int RecordCount);
        
        IList<GLO_News> GetNewsPressList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_News> GetNewsFileList(int PageSize, int PageIndex, out int RecordCount);

        GLO_News GetNewsByID(int newsid);
        
        void UpadateNews(GLO_News news);
        
        void DeleteNews(int newsid);
        
        void ChangeStatus(int newsid, bool status);
    }
}
