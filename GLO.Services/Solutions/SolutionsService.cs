﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Collections;
namespace GLO.Services
{
    public class SolutionsService:ISolutionsService
    {
        private ISolutionsRepository _responsitory;
        public SolutionsService(ISolutionsRepository SolutionsResponsitory)
        {
            _responsitory = SolutionsResponsitory;
        }

        public IList<GLO_Solutions> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetAllList(PageSize, PageIndex, out RecordCount).ToList();
        }

        public IList<GLO_Solutions> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            return _responsitory.GetEnableList(PageSize, PageIndex,  out RecordCount).ToList();
        }

        public IList<GLO_SolutionsCategory> GetCategoryList()
        {
            return _responsitory.GetCategoryList().ToList();
        }

        public void Add(GLO_Solutions Solutions)
        {
            _responsitory.Add(Solutions);
        }

        public void Update(GLO_Solutions Solutions)
        {
            _responsitory.Update(Solutions);
        }

        public GLO_Solutions GetByID(int SolutionsID)
        {
            return _responsitory.GetByID(SolutionsID);
        }

        public void Delete(int SolutionsID)
        {
            _responsitory.Delete(SolutionsID);
        }

        public void ChangeStatus(int SolutionsID,bool status)
        {
            _responsitory.ChangeStatus(SolutionsID, status);
        }
    }
}
