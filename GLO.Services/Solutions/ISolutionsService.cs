﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Services
{
    public interface ISolutionsService
    {
        IList<GLO_Solutions> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Solutions> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_SolutionsCategory> GetCategoryList();

        void Add(GLO_Solutions Solutions);

        void Update(GLO_Solutions Solutions);

        GLO_Solutions GetByID(int SolutionsID);

        void Delete(int SolutionsID);

        void ChangeStatus(int SolutionsID, bool status);
    }
}
