﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class PartnerRepository : IPartnerRepository
    {
        private GLOEntities _contextentity;

        public PartnerRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All Partner
        public IList<GLO_Partner> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Partner.ToList().Count;
            return _contextentity.GLO_Partner.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable Partner
        public IList<GLO_Partner> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Partner.ToList().Count;
            return _contextentity.GLO_Partner.Where(t => t.Status == true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Add Partner
        public void Add(GLO_Partner Partner)
        {
            _contextentity.AddToGLO_Partner(Partner);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update Partner
        public void Update(GLO_Partner Partner)
        {
            var dbData = _contextentity.GLO_Partner.FirstOrDefault(x => x.PartnerID == Partner.PartnerID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, Partner);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get Partner By ID
        public GLO_Partner GetByID(int PartnerID)
        {
            return _contextentity.GLO_Partner.FirstOrDefault(x => x.PartnerID == PartnerID);
        }
        #endregion

        #region Delete Partner
        public void Delete(int PartnerID)
        {
            var dbData = _contextentity.GLO_Partner.FirstOrDefault(x => x.PartnerID == PartnerID);
            _contextentity.GLO_Partner.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable Partner
        public void ChangeStatus(int PartnerID, bool status)
        {
            var dbData = _contextentity.GLO_Partner.FirstOrDefault(x => x.PartnerID == PartnerID);
            dbData.Status = status;
            _contextentity.SaveChanges();
        }
        #endregion
    }
}
