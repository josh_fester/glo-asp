﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
namespace GLO.Data
{
    public interface IPartnerRepository
    {
        IList<GLO_Partner> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Partner> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_Partner Partners);

        void Update(GLO_Partner Partners);

        GLO_Partner GetByID(int PartnerID);

        void Delete(int PartnerID);

        void ChangeStatus(int PartnerID, bool status);
    }
}
