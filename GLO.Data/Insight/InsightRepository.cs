﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using System.Data;

namespace GLO.Data
{
    public class InsightRepository:IInsightRepository
    {
        private GLOEntities _contextentity;
        public InsightRepository()
        {
            _contextentity = new GLOEntities();
        }
        public void Add(GLO_SurveyReport report)
        {
            _contextentity.AddToGLO_SurveyReport(report);
            _contextentity.SaveChanges();
        }

        public void Update(GLO_SurveyReport report)
        {
            var dbData = _contextentity.GLO_SurveyReport.FirstOrDefault(x => x.SurveyID == report.SurveyID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, report);
            _contextentity.SaveChanges();
        }

        public void Delete(int surveyid)
        {
            var dbData = _contextentity.GLO_SurveyReport.FirstOrDefault(x => x.SurveyID == surveyid);
            _contextentity.GLO_SurveyReport.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public GLO_SurveyReport GetSurveyReportByID(int reportid)
        {
            return _contextentity.GLO_SurveyReport.FirstOrDefault(x => x.SurveyID == reportid);
        }
        public void ChangeStatus(int surveyid, bool status)
        {
            var dbData = _contextentity.GLO_SurveyReport.FirstOrDefault(x => x.SurveyID == surveyid);
            dbData.Status = status;
            _contextentity.SaveChanges();
        }

        public IList<GLO_SurveyReport> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_SurveyReport.ToList().Count;
            return _contextentity.GLO_SurveyReport.OrderBy(r => r.OrderID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }

        public void PostAdd(GLO_InsightPosts post)
        {
            _contextentity.AddToGLO_InsightPosts(post);
            _contextentity.SaveChanges();
        }

        public void PostUpdate(GLO_InsightPosts post)
        {
            var dbData = _contextentity.GLO_InsightPosts.FirstOrDefault(x => x.PostID == post.PostID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, post);
            _contextentity.SaveChanges();
        }

        public void PostDelete(int postid)
        {
            var dbData = _contextentity.GLO_InsightPosts.FirstOrDefault(x=>x.PostID == postid); 
            _contextentity.GLO_InsightPosts.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public GLO_InsightPosts GetPostByID(int postid)
        {
            return _contextentity.GLO_InsightPosts.FirstOrDefault(x => x.PostID == postid);
        }
        public IList<GLO_InsightPosts> GetPostList(int PageSize, int PageIndex, out int RecordCount,bool isRecommend = false)
        {
            if (isRecommend)
            {
                RecordCount = _contextentity.GLO_InsightPosts.Where(x=>x.IsRecommend).ToList().Count;
                return _contextentity.GLO_InsightPosts.Where(x=>x.IsRecommend).OrderByDescending(r => r.PostID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
            }
            else
            {
                RecordCount = _contextentity.GLO_InsightPosts.ToList().Count;
                return _contextentity.GLO_InsightPosts.OrderByDescending(r => r.PostID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
            }
        }
        public GLO_InsightPosts GetPostForEmailTemplate()
        {
            return _contextentity.GLO_InsightPosts.OrderByDescending(r => r.PostID).FirstOrDefault(x => x.IsEmailRecommend == true);
        }

        public void PostCommentAdd(GLO_InsightPostComment postcoment)
        {
            _contextentity.AddToGLO_InsightPostComment(postcoment);
            _contextentity.SaveChanges();
        }
        public void PostCommentDel(int postcommentid)
        {
            var dbData = _contextentity.GLO_InsightPostComment.FirstOrDefault(x => x.PostCommentID == postcommentid);
            _contextentity.GLO_InsightPostComment.DeleteObject(dbData);
            _contextentity.SaveChanges();
 
        }
        public IList<GLO_InsightPostComment> GetCommentList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_InsightPosts.ToList().Count;
            return _contextentity.GLO_InsightPostComment.OrderByDescending(r => r.PostCommentID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public IList<GLO_InsightPosts> SearchTagList(int PageSize, int PageIndex, out int RecordCount, string tag)

        {
            var taglist = _contextentity.GLO_InsightPostTag.ToList();
            if (!string.IsNullOrEmpty(tag))
            {
                taglist = taglist.Where(x => x.Tag.ToUpper() == tag.ToUpper()).ToList();
                IList<GLO_InsightPosts> li = new List<GLO_InsightPosts>();
                foreach (var itemtag in taglist)
                {
                    li.Add(itemtag.GLO_InsightPosts);
                }
                RecordCount = li.Count;
                return li.OrderByDescending(r => r.PostID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
            }
            else
            {
                RecordCount = _contextentity.GLO_InsightPosts.ToList().Count;
                return _contextentity.GLO_InsightPosts.OrderByDescending(r => r.PostID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList(); 
            }
        }

        public void PostTagAdd(GLO_InsightPostTag posttag)
        {
            _contextentity.AddToGLO_InsightPostTag(posttag);
            _contextentity.SaveChanges();
        }
        public void PostTagDel(int tagid)
        {
            var dbData = _contextentity.GLO_InsightPostTag.FirstOrDefault(x => x.PostTagID == tagid);
            _contextentity.GLO_InsightPostTag.DeleteObject(dbData);
            _contextentity.SaveChanges();

        }
    }
}
