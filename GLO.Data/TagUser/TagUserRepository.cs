﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class TagUserRepository : ITagUserRepository
    {
        private GLOEntities _contextentity;
        public TagUserRepository()
        {
            _contextentity = new GLOEntities();
        }
        #region Tag Users
        public void AddTagUsers(GLO_TagUsers data)
        {
            _contextentity.GLO_TagUsers.AddObject(data);
            _contextentity.SaveChanges();
        }
        public void UpdateTagUsers(GLO_TagUsers data)
        {
            var dbData = _contextentity.GLO_TagUsers.FirstOrDefault(x => x.TagID == data.TagID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, data);
            _contextentity.SaveChanges();
        }
        public GLO_TagUsers GetTagUsersByID(int tagID)
        {
            return _contextentity.GLO_TagUsers.Where(t => t.TagID == tagID).FirstOrDefault();
        }
        public List<GLO_TagUsers> GetTaggedUserList(int userID)
        {
            return _contextentity.GLO_TagUsers.Where(t=>t.UserID == userID).ToList();
        }
        public void DeleteTagUsers(int tagID)
        {
            var dbData = _contextentity.GLO_TagUsers.Where(t => t.TagID == tagID).FirstOrDefault();
            if (dbData != null)
            {
                _contextentity.DeleteObject(dbData);
                _contextentity.SaveChanges();
            }
        }
        #endregion

        #region
        public List<GLO_RecentContact> GetRecentContactList(int userid)
        {
            var dbData = _contextentity.GLO_RecentContact.Where(x => x.UserID == userid).OrderByDescending(x => x.ContactTime).ToList();
            return dbData;
        }

        public IList<GLO_TagUsers> GetTagListByID(int userid, int type = 0)
        {
            IList<GLO_TagUsers> userlist = new List<GLO_TagUsers>();
            var myTaggedAllUser = _contextentity.GLO_TagUsers.Where(t => t.UserID == userid).ToList();
            if (myTaggedAllUser.Count > 0)
            {
                foreach (var taggedUser in myTaggedAllUser)
                {
                    if (type != 0)
                    {
                        if (taggedUser.GLO_Users1.TypeID == type)
                        {
                            userlist.Add(taggedUser);
                        }
                    }
                    else
                    {
                        userlist.Add(taggedUser);
                    }
                }
            }
            return userlist;
        }
        public IList<GLO_Users> TheyTagMe(int userid, int type)
        {
            IList<GLO_Users> userlist = new List<GLO_Users>();
            var theyTaggedMeAllUser = _contextentity.GLO_TagUsers.Where(x => x.TaggedUserID == userid).ToList();
            if (theyTaggedMeAllUser.Count > 0)
            {
                foreach (var user in theyTaggedMeAllUser)
                {
                    var taggedMeUser = user.GLO_Users;
                    if (taggedMeUser.TypeID == type)
                    {
                        userlist.Add(taggedMeUser);
                    }
                }
            }
            return userlist;
        }

        public IList<GLO_TagUsers> GetTagListByCategory(int categoryid, int type = 1)
        {
            IList<GLO_TagUsers> userlist = new List<GLO_TagUsers>();
            foreach (var taguser in _contextentity.GLO_TagUsers.Where(x => x.TagCategoryID == categoryid).ToList())
            {
                if (taguser.GLO_Users1.TypeID == type)
                {
                    userlist.Add(taguser);
                }
            }
            return userlist;
        }
        #endregion

        #region Tag Category
        public void FirstAddTagCategory(int userID,int userType)
        {
            var functionList = _contextentity.GLO_Functions.Where(t => t.IsDefaultValue == 1 && t.FunctionType == userType).OrderBy(t => t.FunctionOrderID).ToList();
            foreach (var function in functionList)
            {
                GLO_TagCategory data = new GLO_TagCategory();
                data.UserID = userID;
                data.FunctionID = function.FunctionID;
                _contextentity.GLO_TagCategory.AddObject(data);
            }
            _contextentity.SaveChanges();
        }
        public int AddTagCategory(GLO_TagCategory data)
        {
            _contextentity.AddToGLO_TagCategory(data);
            _contextentity.SaveChanges();
            return data.TagCategoryID;
        }
        public void RenameTagCategory(int categoryID, string newName)
        {
            var dbData = _contextentity.GLO_TagCategory.Where(t => t.TagCategoryID == categoryID).FirstOrDefault();
            if (dbData != null)
            {
                //dbData.CategoryName = newName;
                _contextentity.SaveChanges();
            }
        }
        public bool DeleteTagCategory(int categoryID)
        {
            var dbData = _contextentity.GLO_TagCategory.Where(t => t.TagCategoryID == categoryID).FirstOrDefault();
            if (dbData != null)
            {
                if (_contextentity.GLO_TagUsers.Where(t => t.TagCategoryID == categoryID).Count() == 0)
                {
                    _contextentity.DeleteObject(dbData);
                    _contextentity.SaveChanges();
                    return true;
                }
            }
            return false;
        }
        public List<GLO_TagCategory> GetTagCategoryList(int userID)
        {
            return _contextentity.GLO_TagCategory.Where(t => t.UserID == userID).ToList();
        }
        public GLO_TagCategory GetTagCategoryByName(int userID, Guid functionID)
        {
            return _contextentity.GLO_TagCategory.Where(t => t.UserID == userID && t.FunctionID==functionID).FirstOrDefault();
        }
        #endregion

        #region Functions
        public Guid AddFunction(GLO_Functions data)
        {
            _contextentity.AddToGLO_Functions(data);
            _contextentity.SaveChanges();
            return data.FunctionID;
        }
        public GLO_Functions GetFunctionByName(string functionName)
        {
            return _contextentity.GLO_Functions.Where(t => t.FunctionName == functionName).FirstOrDefault();
        }

        public GLO_Functions GetFunctionByID(Guid functionid)
        {
            return _contextentity.GLO_Functions.Where(t => t.FunctionID == functionid).FirstOrDefault();
        }

        public void FunctionEdit(GLO_Functions function)
        {
            var dbData = _contextentity.GLO_Functions.FirstOrDefault(x => x.FunctionID == function.FunctionID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, function);
            _contextentity.SaveChanges();
        }

        public void FunctionDelete(Guid functionid)
        {
            foreach (var tagcat in _contextentity.GLO_TagCategory.Where(x => x.FunctionID == functionid).ToList())
            {
               var delresult =  DeleteTagCategory(tagcat.TagCategoryID);
            }

            var dbData = _contextentity.GLO_Functions.FirstOrDefault(x => x.FunctionID == functionid);
            _contextentity.GLO_Functions.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        public void DeleteTagUser(int tagid)
        {
            var dbData = _contextentity.GLO_TagUsers.FirstOrDefault(x => x.TagID == tagid);
            _contextentity.GLO_TagUsers.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public IList<GLO_Functions> GetSystemFunctionList()
        {
            return _contextentity.GLO_Functions.Where(x => x.IsDefaultValue == 1).OrderBy(m=>m.FunctionName).ToList();
        }
    }
}
