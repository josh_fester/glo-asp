﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface ITagUserRepository
    {
        #region Tag Users
        void AddTagUsers(GLO_TagUsers data);
        void UpdateTagUsers(GLO_TagUsers data);
        GLO_TagUsers GetTagUsersByID(int tagID);
        List<GLO_TagUsers> GetTaggedUserList(int userID);
        void DeleteTagUsers(int tagID);
        #endregion
        List<GLO_RecentContact> GetRecentContactList(int userid);
        
        IList<GLO_TagUsers> GetTagListByID(int userid, int type = 0);

        IList<GLO_Users> TheyTagMe(int userid, int type);

        IList<GLO_TagUsers> GetTagListByCategory(int categoryid, int type);

        void DeleteTagUser(int tagid);

        #region Tag Category
        void FirstAddTagCategory(int userID, int userType);

        int AddTagCategory(GLO_TagCategory data);

        void RenameTagCategory(int categoryID, string newName);

        bool DeleteTagCategory(int categoryID);

        List<GLO_TagCategory> GetTagCategoryList(int userID);

        GLO_TagCategory GetTagCategoryByName(int userID, Guid functionID);
        #endregion

        #region Functions

        Guid AddFunction(GLO_Functions data);

        GLO_Functions GetFunctionByName(string functionName);

        GLO_Functions GetFunctionByID(Guid functionid);

        void FunctionEdit(GLO_Functions function);

        void FunctionDelete(Guid functionid);

        IList<GLO_Functions> GetSystemFunctionList();

        #endregion
    }
}
