﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class SuccessRepository:ISuccessRepository
    {
        private GLOEntities _contextentity;

        public SuccessRepository()
        {
            _contextentity = new GLOEntities();
        }

        public void AddSuccessCase(GLO_Success success)
        {
            _contextentity.AddToGLO_Success(success);
            _contextentity.SaveChanges();
        }

        public void UpdateSuccessCase(GLO_Success success)
        {
            var dbData = _contextentity.GLO_Success.FirstOrDefault(x => x.SuccessID == success.SuccessID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, success);
            _contextentity.SaveChanges();
        }

        public void DeleteSuccessCase(int successid)
        {
            var glosuccess = _contextentity.GLO_Success.FirstOrDefault(x => x.SuccessID == successid);
            _contextentity.GLO_Success.DeleteObject(glosuccess);
            _contextentity.SaveChanges();
        }

        public GLO_Success GetSuccessCaseByID(int successid)
        {
            return _contextentity.GLO_Success.FirstOrDefault(x => x.SuccessID == successid);
        }

        public IList<GLO_Success> GetSuccessCaseList()
        {
            return _contextentity.GLO_Success.ToList();
        }

        public IList<GLO_Success> GetSuccessCaseList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Success.ToList().Count;
            return _contextentity.GLO_Success.OrderBy(r => r.SuccessID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
    }
}
