﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
namespace GLO.Data
{
    public interface ISuccessRepository
    {
        void AddSuccessCase(GLO_Success success);
        void UpdateSuccessCase(GLO_Success success);
        void DeleteSuccessCase(int successid);
        GLO_Success GetSuccessCaseByID(int successid);
        IList<GLO_Success> GetSuccessCaseList();
        IList<GLO_Success> GetSuccessCaseList(int PageSize, int PageIndex, out int RecordCount);
    }
}
