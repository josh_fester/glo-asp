﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IQuickPollRepository
    {
        void PollTopicAdd(GLO_PollTopic topic);
        void PollTopicEdit(GLO_PollTopic topic);
        IList<GLO_PollTopic> GetPollTopic();

        void PollItemAdd(GLO_PollItem pollitem);
        void PollItemEdit(GLO_PollItem pollitem);
        IList<GLO_PollItem> GetPollItemByPollID(int id);
        GLO_PollItem GetPollItemByID(int id);
    }
}
