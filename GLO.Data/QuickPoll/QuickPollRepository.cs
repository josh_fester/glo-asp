﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class QuickPollRepository:IQuickPollRepository
    {
        private GLOEntities _contextentity;
        public QuickPollRepository()
        {
            _contextentity = new GLOEntities();
        }
        public void PollTopicAdd(GLO_PollTopic topic)
        {
            _contextentity.AddToGLO_PollTopic(topic);
            _contextentity.SaveChanges();
        }
        public void PollTopicEdit(GLO_PollTopic topic)
        {
            var glotopic = _contextentity.GLO_PollTopic.FirstOrDefault(x => x.PollID == topic.PollID);
            glotopic = topic;
            _contextentity.SaveChanges();
        }
        public IList<GLO_PollTopic> GetPollTopic()
        {
            return _contextentity.GLO_PollTopic.ToList();
        }

        public void PollItemAdd(GLO_PollItem pollitem)
        {
            _contextentity.AddToGLO_PollItem(pollitem);
            _contextentity.SaveChanges();
        }
        public void PollItemEdit(GLO_PollItem pollitem)
        {
            var glopollitem = _contextentity.GLO_PollItem.FirstOrDefault(x => x.ItemID == pollitem.ItemID);
            glopollitem = pollitem;
            _contextentity.SaveChanges();
        }
        public IList<GLO_PollItem> GetPollItemByPollID(int id)
        {
            return _contextentity.GLO_PollItem.Where(x => x.PollID == id).ToList();
        }
        public GLO_PollItem GetPollItemByID(int id)
        {
            return _contextentity.GLO_PollItem.FirstOrDefault(x => x.ItemID == id);
        }
    
    }
}
