﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class CompanyRepository:ICompanyRepository
    {
        private GLOEntities _contextentity;
        public CompanyRepository()
        {
            _contextentity = new GLOEntities();
        }

        public GLO_Company GetCompanyByID(int companyid)
        {
            return _contextentity.GLO_Company.FirstOrDefault(x => x.UserID == companyid);
        }
        public void CompanyAdd(GLO_Company company)
        {
            _contextentity.AddToGLO_Company(company);
            _contextentity.SaveChanges();
        }
        public void CompanyUpdate(GLO_Company company)
        {
            var dbData = _contextentity.GLO_Company.FirstOrDefault(x => x.UserID == company.UserID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, company);
            _contextentity.SaveChanges();
        }
        public IList<GLO_CompanyJob> GetCompanyJob(int companyid,string keyword, string location, string func)
        {
            var joblist = _contextentity.GLO_CompanyJob.Where(x => x.UserID == companyid).Where(x => x.IsActive && x.IsPublish && x.IsDelete == false && x.ExpireDate > DateTime.Now).ToList();
            if(!string.IsNullOrEmpty(keyword))
                joblist = joblist.Where(x => x.JobTitle.ToUpper().Contains(keyword.ToUpper())||x.JobFunction.ToUpper().Contains(keyword.ToUpper())).ToList();
            if (!string.IsNullOrEmpty(location)&& location!="All Location")
                joblist = joblist.Where(x => x.Location==location).ToList();
            if (!string.IsNullOrEmpty(func) && func != "All Function")
                joblist = joblist.Where(x => x.JobFunction == func).ToList();
            return joblist;
        }
        public IList<GLO_CompanyJob> SearchJob(string keyword, string location, string func,string industry="",string education="",string date="")
        {
            var joblist = _contextentity.GLO_CompanyJob.Where(x => x.IsActive == true && x.IsPublish == true && x.IsDelete==false && x.ExpireDate>DateTime.Now).ToList();
            if (!string.IsNullOrEmpty(keyword))
                joblist = joblist.Where(x => x.JobTitle.ToUpper().Contains(keyword.ToUpper()) || x.JobFunction.ToUpper().Contains(keyword.ToUpper()) || x.Location.ToUpper().Contains(keyword.ToUpper()) || x.Industry.ToUpper().Contains(keyword.ToUpper())).ToList();
            if (!string.IsNullOrEmpty(location) && location != "All Location")
                joblist = joblist.Where(x => x.Location == location).ToList();
            if (!string.IsNullOrEmpty(func) && func != "All Function")
                joblist = joblist.Where(x => x.JobFunction == func).ToList();
            if (!string.IsNullOrEmpty(industry) && industry != "All Industry")
                joblist = joblist.Where(x => x.Industry == industry).ToList();
            if (!string.IsNullOrEmpty(education))
                joblist = joblist.Where(x => x.Education.ToUpper().Contains(education.ToUpper())).ToList();
            if (!string.IsNullOrEmpty(date))
                joblist = joblist.Where(x =>x.PostDate == date).ToList();

            return joblist;
        }
        public GLO_CompanyJob GetCompanyJobByID(int jobid)
        {
            return _contextentity.GLO_CompanyJob.FirstOrDefault(x => x.JobID == jobid);
        }

        public IList<GLO_Company> RecommendCompanyList(int count)
        {
            var allList = _contextentity.GLO_Company.Where(x => x.LogoUrl != "user_none.gif" && x.GLO_Users.IsPublish == true && x.GLO_Users.Approved == true && x.GLO_Users.GLO_UserEx.IsPayMent == 1 ).OrderByDescending(x => x.UserID).ToList();
            return GetRandomList(allList).Take(count).ToList();
        }
        public static List<T> GetRandomList<T>(List<T> inputList)
        {
            //Copy to a array
            T[] copyArray = new T[inputList.Count];
            inputList.CopyTo(copyArray);

            //Add range
            List<T> copyList = new List<T>();
            copyList.AddRange(copyArray);

            //Set outputList and random
            List<T> outputList = new List<T>();
            Random rd = new Random(DateTime.Now.Millisecond);

            while (copyList.Count > 0)
            {
                //Select an index and item
                int rdIndex = rd.Next(0, copyList.Count - 1);
                T remove = copyList[rdIndex];

                //remove it from copyList and add it to output
                copyList.Remove(remove);
                outputList.Add(remove);
            }
            return outputList;
        }
        public void AssessmentAdd(GLO_Assessment assessment)
        {
            _contextentity.AddToGLO_Assessment(assessment);
            _contextentity.SaveChanges();
        }

        public void HuntingAdd(GLO_HuntingRequest huntingrequest)
        {
            _contextentity.AddToGLO_HuntingRequest(huntingrequest);
            _contextentity.SaveChanges();
        }
        public void HuntingEdit(GLO_HuntingRequest huntingrequest)
        {
            var dbData = _contextentity.GLO_HuntingRequest.FirstOrDefault(x => x.HuntingID == huntingrequest.HuntingID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, huntingrequest);
            _contextentity.SaveChanges();
        }

        public void HuntinDelete(int huntingid)
        {
            var dbData = _contextentity.GLO_HuntingRequest.FirstOrDefault(x => x.HuntingID == huntingid);
            _contextentity.GLO_HuntingRequest.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public void JobAdd(GLO_CompanyJob companyjob)
        {
            _contextentity.AddToGLO_CompanyJob(companyjob);
            _contextentity.SaveChanges();
        }
        public void JobEdit(GLO_CompanyJob companyjob)
        {
            var dbData = _contextentity.GLO_CompanyJob.FirstOrDefault(x => x.JobID == companyjob.JobID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, companyjob);
            _contextentity.SaveChanges();
        }
        public void JobDelete(int jobid)
        {
            var dbData = _contextentity.GLO_CompanyJob.FirstOrDefault(x => x.JobID == jobid);
            _contextentity.GLO_CompanyJob.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public GLO_HuntingRequest GetHuntingByID(int huntingid)
        {
            return _contextentity.GLO_HuntingRequest.FirstOrDefault(x => x.HuntingID == huntingid);
        }
        public GLO_CompanyType GetCompanyTypeByID(int typeid)
        {
            return _contextentity.GLO_CompanyType.FirstOrDefault(x => x.CompanyType == typeid);
        }

        public List<GLO_Company> GetCompanyList()
        {
            return _contextentity.GLO_Company.ToList();
        }
    }
}
