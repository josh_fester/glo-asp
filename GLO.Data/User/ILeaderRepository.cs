﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface ILeaderRepository
    {
        void LeaderRegister(GLO_Leader leader);

        void LeaderUpdate(GLO_Leader leader);

        GLO_Leader GetLeaderByID(int userid);

        IList<GLO_Users> GetUserList(int type);

        IList<GLO_Leader> RecommendLeaderList(int count);

        void TagJob(GLO_JobTag jobtag);

        GLO_JobTag GetJobTagByID(int jobid, int userid);

        void RemoveTagJob(int tagjobid);

        void UpdateTagJob(int tagjobid);

        void AddResumes(int userID, List<string> resumeNameList);

        Guid AddResume(int userID, string resumeName);

        void DeleteResume(Guid resumeID);

        #region key word
        void AddKeyWord(GLO_LeaderKeyWord newData);
        void UpdateKeyWord(GLO_LeaderKeyWord newData);
        List<GLO_LeaderKeyWord> GetKeyWordList(int userID);
        void DeleteKeyWord(Guid keyWordID);
        GLO_LeaderKeyWord GetKeyWordByContent(int userID, string content);
        #endregion

        List<GLO_LeaderInterViewQuestion> GetInterviewQuestion();

        void AddInterview(int userID, List<GLO_LeaderInterviewAnswer> newList);
    }
}
