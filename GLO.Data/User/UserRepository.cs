﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;
using System.Data.Objects;
namespace GLO.Data
{
    public class UserRepository:IUserRepository
    {
        private GLOEntities _contextentity;
        public UserRepository()
        {
            _contextentity = new GLOEntities();
        }
        public IList<GLO_Users> GetRecommendUser(string count, int usertype)
        {
            return _contextentity.GLO_Users.Where(x=>x.TypeID == usertype).OrderByDescending(x=>x.UserID).Take(int.Parse(count)).ToList();//(.Top(count, new ObjectParameter("type", usertype)).ToList();
            //Context<MyTestEntities>().UserInfo.Where(i => i.UserID > 100).OrderBy(i => i.UserID).Take(10).ToList();
                //.Where(x=>x.TypeID ==usertype).OrderBy(x=>x.RegisterDate) .Where("TypeID = @type", new ObjectParameter("type", usertype.ToString())).Top(count).ToList();//.Top(count, new ObjectParameter("TypeID", usertype.ToString())).ToList();
        }
    }
}
