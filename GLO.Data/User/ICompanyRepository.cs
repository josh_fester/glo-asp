﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface ICompanyRepository
    {
        GLO_Company GetCompanyByID(int companyid);

        void CompanyAdd(GLO_Company company);

        void CompanyUpdate(GLO_Company company);

        IList<GLO_Company> RecommendCompanyList(int count);

        IList<GLO_CompanyJob> GetCompanyJob(int companyid, string keyword, string location, string func);

        IList<GLO_CompanyJob> SearchJob(string keyword, string location, string func, string industry = "", string education = "", string date = "");

        GLO_CompanyJob GetCompanyJobByID(int jobid);

        void AssessmentAdd(GLO_Assessment assessment);

        void HuntingAdd(GLO_HuntingRequest huntingrequest);

        void HuntingEdit(GLO_HuntingRequest huntingrequest);

        void HuntinDelete(int huntingid);

        void JobAdd(GLO_CompanyJob companyjob);

        void JobEdit(GLO_CompanyJob companyjob);

        void JobDelete(int jobid);

        GLO_HuntingRequest GetHuntingByID(int huntingid);

        GLO_CompanyType GetCompanyTypeByID(int typeid);

        List<GLO_Company> GetCompanyList();
    }
}
