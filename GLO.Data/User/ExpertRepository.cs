﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class ExpertRepository:IExpertRepository
    {
        private GLOEntities _contextentity;
        public ExpertRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region expert opertion
        public void HRExpertAdd(GLO_HRExpert expert)
        {
            _contextentity.AddToGLO_HRExpert(expert);
            _contextentity.SaveChanges();
        }

        public void HRExpertUpdate(GLO_HRExpert expert)
        {
            var dbData = _contextentity.GLO_HRExpert.FirstOrDefault(x => x.UserID == expert.UserID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, expert);
            _contextentity.SaveChanges();
        }

        public GLO_HRExpert GetExpertByID(int userid)
        {
            return _contextentity.GLO_HRExpert.FirstOrDefault(x => x.UserID == userid);
        }
        public IList<GLO_HRExpert> RecommendExpert(int count)
        {
            var allList = _contextentity.GLO_HRExpert.Where(x => x.LogoUrl != "user_none.gif" && x.GLO_Users.IsPublish == true && x.GLO_Users.GLO_UserEx.IsPayMent == 1).OrderByDescending(x => x.UserID).ToList();
            return GetRandomList(allList).Take(count).ToList();
        }
        public static List<T> GetRandomList<T>(List<T> inputList)
        {
            //Copy to a array
            T[] copyArray = new T[inputList.Count];
            inputList.CopyTo(copyArray);

            //Add range
            List<T> copyList = new List<T>();
            copyList.AddRange(copyArray);

            //Set outputList and random
            List<T> outputList = new List<T>();
            Random rd = new Random(DateTime.Now.Millisecond);

            while (copyList.Count > 0)
            {
                //Select an index and item
                int rdIndex = rd.Next(0, copyList.Count - 1);
                T remove = copyList[rdIndex];

                //remove it from copyList and add it to output
                copyList.Remove(remove);
                outputList.Add(remove);
            }
            return outputList;
        }
        public int ExpertValueAdd(GLO_ExpertValueDic expertDic)
        {
            _contextentity.AddToGLO_ExpertValueDic(expertDic);
            _contextentity.SaveChanges();
            return expertDic.ExpertValueID;
        }
        public GLO_ExpertValueDic GetExpertValueDicByValue(string value)
        {
            return _contextentity.GLO_ExpertValueDic.Where(x => x.ExpertValue == value).FirstOrDefault();
        }
        public Guid ExpertRelationalAdd(int userID, int valueID)
        {
            var oldData = _contextentity.GLO_ExpertRelational.Where(x => x.UserID == userID && x.ExpertValueID == valueID).FirstOrDefault();
            if (oldData == null)
            {
                GLO_ExpertRelational newData = new GLO_ExpertRelational();
                newData.RelationalID = Guid.NewGuid();
                newData.UserID = userID;
                newData.ExpertValueID = valueID;
                _contextentity.GLO_ExpertRelational.AddObject(newData);
                _contextentity.SaveChanges();
                return newData.RelationalID;
            }
            else
            {
                return oldData.RelationalID;
            }
        }
        public void ExpertRelationalAdd(int expertID,List<GLO_ExpertRelational> expertRelationalList)
        {
            var relationalData = _contextentity.GLO_ExpertRelational.Where(x => x.UserID == expertID).ToList();
            foreach (var item in relationalData)
            {
                _contextentity.GLO_ExpertRelational.DeleteObject(item);
            }
            if (expertRelationalList != null && expertRelationalList.Count > 0)
            {                
                foreach (var item in expertRelationalList)
                {
                    _contextentity.AddToGLO_ExpertRelational(item);
                }
            }
            _contextentity.SaveChanges();
        }
        public void ExpertRelationalDelete(Guid relationalID)
        {
            var deleteData = _contextentity.GLO_ExpertRelational.Where(x => x.RelationalID == relationalID).FirstOrDefault();
            if (deleteData != null)
            {
                _contextentity.GLO_ExpertRelational.DeleteObject(deleteData);
                _contextentity.SaveChanges();
            }
        }
        public List<GLO_ExpertValueDic> GetExpertValueList()
        {
            return _contextentity.GLO_ExpertValueDic.Where(x => x.ExpertValueID != 1 && x.ExpertValueID != 2 && x.ExpertValueID != 3 && x.ExpertValueID != 4 && x.ExpertValueID != 5 && x.ExpertValueID != 6 && x.ExpertValueID != 7).OrderBy(x => x.ExpertValueID).ToList();
        }
        #endregion

        #region assignment
        public void ExpertProjectAdd(GLO_ExpertProject project)
        {
            _contextentity.AddToGLO_ExpertProject(project);
            _contextentity.SaveChanges();
        }
        public GLO_ExpertProject GetProjectByID(int projectid)
        {
            return _contextentity.GLO_ExpertProject.FirstOrDefault(x => x.ProjectID == projectid);
        }
        public void ExpertProjectEdit(GLO_ExpertProject project)
        {
            var dbData = _contextentity.GLO_ExpertProject.FirstOrDefault(x => x.ProjectID == project.ProjectID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, project);
            _contextentity.SaveChanges();
        }
        public void ExpertProjectDelete(int projectid)
        {
            var gloproject = _contextentity.GLO_ExpertProject.FirstOrDefault(x => x.ProjectID == projectid);
            _contextentity.GLO_ExpertProject.DeleteObject(gloproject);
            _contextentity.SaveChanges();
        }    
        #endregion


        #region recommendation
        public GLO_Recommendation GetRecommendationByID(int recommendid)
        {
            return _contextentity.GLO_Recommendation.FirstOrDefault(x => x.RecommendID == recommendid);
        }
        public void RecommendationAdd(GLO_Recommendation recommendation)
        {
            _contextentity.AddToGLO_Recommendation(recommendation);
            _contextentity.SaveChanges();

        }
        public void RecommendationDelete(int recommendid)
        {
            var glorecommend = _contextentity.GLO_Recommendation.FirstOrDefault(x => x.RecommendID == recommendid);
            _contextentity.GLO_Recommendation.DeleteObject(glorecommend);
            _contextentity.SaveChanges();
        }
        #endregion


        #region compentence

        public void CarerrSummaryAdd(GLO_CareerSummary summary)
        {
            _contextentity.AddToGLO_CareerSummary(summary);
            _contextentity.SaveChanges();
        }

        public void EducationAdd(GLO_Education education)
        {
            _contextentity.AddToGLO_Education(education);
            _contextentity.SaveChanges();
        }

        public void ContributionAdd(GLO_Contribution contribution)
        {
            _contextentity.AddToGLO_Contribution(contribution);
            _contextentity.SaveChanges();
        }

        public void CarerrSummaryDelete(int careerid)
        {
            var dbData = _contextentity.GLO_CareerSummary.FirstOrDefault(x => x.CareerID == careerid);
            _contextentity.GLO_CareerSummary.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }

        public void EducationDelete(int educationid)
        {
            var dbData = _contextentity.GLO_Education.FirstOrDefault(x => x.EducationID == educationid);
            _contextentity.GLO_Education.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }

        public void ContributionDelete(int contributid)
        {
            var dbData = _contextentity.GLO_Contribution.FirstOrDefault(x => x.ContributionID == contributid);
            _contextentity.GLO_Contribution.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }


        #endregion
    }
}
