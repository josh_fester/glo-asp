﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using System.Data.Objects;

namespace GLO.Data
{
    public class LeaderRepository:ILeaderRepository
    {
        private GLOEntities _contextentity;
        public LeaderRepository()
        {
            _contextentity = new GLOEntities();
        }
        public void LeaderRegister(GLO_Leader leader)
        {
            _contextentity.AddToGLO_Leader(leader);
            _contextentity.SaveChanges();
        }
        public void LeaderUpdate(GLO_Leader leader)
        {
            var dbData = _contextentity.GLO_Leader.FirstOrDefault(x => x.UserID == leader.UserID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, leader);
            _contextentity.SaveChanges();
        }
        public GLO_Leader GetLeaderByID(int userid)
        {
            return _contextentity.GLO_Leader.FirstOrDefault(x => x.UserID == userid);
        }
        public IList<GLO_Users> GetUserList(int type)
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID == type).ToList();
        }

        public IList<GLO_Leader> RecommendLeaderList(int count)
        {
            var allList = _contextentity.GLO_Leader.Where(x => x.LogoUrl != "user_none.gif" && x.GLO_Users.IsPublish == true && x.GLO_Users.Approved == true && x.GLO_Users.HideProfile == false).OrderByDescending(x => x.UserID).ToList();
            return GetRandomList(allList).Take(count).ToList();
        }

        public static List<T> GetRandomList<T>(List<T> inputList)
        {
            //Copy to a array
            T[] copyArray = new T[inputList.Count];
            inputList.CopyTo(copyArray);

            //Add range
            List<T> copyList = new List<T>();
            copyList.AddRange(copyArray);

            //Set outputList and random
            List<T> outputList = new List<T>();
            Random rd = new Random(DateTime.Now.Millisecond);

            while (copyList.Count > 0)
            {
                //Select an index and item
                int rdIndex = rd.Next(0, copyList.Count - 1);
                T remove = copyList[rdIndex];

                //remove it from copyList and add it to output
                copyList.Remove(remove);
                outputList.Add(remove);
            }
            return outputList;
        }
        public void TagJob(GLO_JobTag jobtag)
        {
            _contextentity.AddToGLO_JobTag(jobtag);
            _contextentity.SaveChanges();
        }

        public GLO_JobTag GetJobTagByID(int jobid, int userid)
        {
            return _contextentity.GLO_JobTag.FirstOrDefault(x => x.JobID == jobid && x.UserID == userid);
        }

        public void RemoveTagJob(int tagjobid)
        {
            var tagjob = _contextentity.GLO_JobTag.FirstOrDefault(x => x.JobTagID == tagjobid);
            _contextentity.GLO_JobTag.DeleteObject(tagjob);
            _contextentity.SaveChanges();
        }

        public void UpdateTagJob(int tagjobid)
        {
            var dbData = _contextentity.GLO_JobTag.FirstOrDefault(x => x.JobTagID == tagjobid);
            dbData.SendResume = true;
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, dbData);
            _contextentity.SaveChanges();
        }

        public void AddResumes(int userID,List<string> resumeNameList)
        {
            var oldResumes = _contextentity.GLO_Resumes.Where(t => t.UserID == userID).ToList();
            if (oldResumes.Count > 0)
            {
                foreach (var item in oldResumes)
                {
                    _contextentity.GLO_Resumes.DeleteObject(item);
                }
            }
            if (resumeNameList!=null && resumeNameList.Count > 0)
            {
                foreach (var resumeName in resumeNameList)
                {
                    if (!string.IsNullOrEmpty(resumeName))
                    {
                        GLO_Resumes dbData = new GLO_Resumes();
                        dbData.ResumeID = Guid.NewGuid();
                        dbData.ResumeName = resumeName;
                        dbData.UserID = userID;
                        _contextentity.GLO_Resumes.AddObject(dbData);
                    }
                }
            }
            _contextentity.SaveChanges();
        }
        public Guid AddResume(int userID, string resumeName)
        {
            GLO_Resumes dbData = new GLO_Resumes();
            dbData.ResumeID = Guid.NewGuid();
            dbData.ResumeName = resumeName;
            dbData.UserID = userID;
            _contextentity.GLO_Resumes.AddObject(dbData);
            _contextentity.SaveChanges();
            return dbData.ResumeID;
        }
        public void DeleteResume(Guid resumeID)
        {
            var deleteData = _contextentity.GLO_Resumes.Where(t => t.ResumeID == resumeID).FirstOrDefault();
            if (deleteData != null)
            {
                _contextentity.GLO_Resumes.DeleteObject(deleteData);
                _contextentity.SaveChanges();
            }
        }

        #region key word
        public void AddKeyWord(GLO_LeaderKeyWord newData)
        {
            _contextentity.GLO_LeaderKeyWord.AddObject(newData);
            _contextentity.SaveChanges();
        }
        public void UpdateKeyWord(GLO_LeaderKeyWord newData)
        {
            var dbData = _contextentity.GLO_LeaderKeyWord.FirstOrDefault(x => x.UserID == newData.UserID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, newData);
            _contextentity.SaveChanges();
        }
        public List<GLO_LeaderKeyWord> GetKeyWordList(int userID)
        {
            return _contextentity.GLO_LeaderKeyWord.Where(t => t.UserID == userID).ToList();
        }
        public void DeleteKeyWord(Guid keyWordID)
        {
            var dbData = _contextentity.GLO_LeaderKeyWord.FirstOrDefault(x => x.KeyWordID == keyWordID);
            if (dbData != null)
            {
                _contextentity.GLO_LeaderKeyWord.DeleteObject(dbData);
                _contextentity.SaveChanges();
            }
        }
        public GLO_LeaderKeyWord GetKeyWordByContent(int userID, string content)
        {
            return _contextentity.GLO_LeaderKeyWord.FirstOrDefault(x => x.UserID == userID && x.KeyWordContent==content);
        }
        #endregion

        #region My interview

        public List<GLO_LeaderInterViewQuestion> GetInterviewQuestion()
        {
            return _contextentity.GLO_LeaderInterViewQuestion.OrderBy(t=>t.OrderID).ToList();
        }
        public void AddInterview(int userID,List<GLO_LeaderInterviewAnswer> newList)
        {
            var oldList = _contextentity.GLO_LeaderInterviewAnswer.Where(t => t.UserID == userID).ToList();
            if (oldList.Count > 0)
            {
                foreach (var item in oldList)
                {
                    _contextentity.GLO_LeaderInterviewAnswer.DeleteObject(item);
                }
            }
            foreach (var item in newList)
            {
                _contextentity.GLO_LeaderInterviewAnswer.AddObject(item);
            }
            _contextentity.SaveChanges();
        }
        #endregion
    }
}
