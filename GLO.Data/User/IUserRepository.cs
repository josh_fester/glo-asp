﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IUserRepository
    {
        IList<GLO_Users> GetRecommendUser(string count, int typeid);
    }
}
