﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IMessageRepository
    {
        #region Message
        void MessageCreate(GLO_Message message, int senduserid, string receiveid);
        void MessageSave(GLO_Message message, int senduserID, string receiveID);
        IList<GLO_Message> GetMessageList();
        void MessageDelete(int id);
        GLO_Message GetMessageByID(int messageid);
        void MessageUpdate(GLO_Message message);
        #endregion

        #region Send
        IList<GLO_MessageSend> GetSendMessageList(int UserID);
        void MessageSendUpdate(GLO_MessageSend send);
        GLO_MessageSend GetMessageSendByID(int messageid);
        void SendMessageDelete(int messageID);
        #endregion

        #region Receive
        IList<GLO_MessageReceive> GetReceiveMessageList(int UserID);
        void MessageReceiveUpdate(GLO_MessageReceive receive);
        void ReceiveMessageDelete(int id);
        GLO_MessageReceive GetMessageReceiveByID(int receiveid);
        #endregion

        #region Archive
        IList<GLO_MessageArchive> GetArchiveMessageList(int userID);
        void MessageArchiveUpdate(GLO_MessageArchive archive);
        void MessageArchiveDelete(int messageID);
        GLO_MessageArchive GetMessageArchiveByID(int messageID);
        #endregion

        #region Trash
        IList<GLO_MessageTrash> GetTrashMessageList(int userID);
        void MessageTrashDelete(int messageID);
        void MessageTrashDeleteAll();
        #endregion

        #region Send Recommendations Email
        void SendRecommendationsEmail(int userID);
        #endregion

        #region System message
        void SendSystemMessage(GLO_Message message, string receiveid);
        #endregion
    }
}
