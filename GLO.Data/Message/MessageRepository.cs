﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class MessageRepository:IMessageRepository
    {
        private GLOEntities _contextentity;
        public MessageRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Message
        public void MessageCreate(GLO_Message message,int senduserid,string receiveid)
        {
            if (message.MessageID != 0)
            {
                GLO_Message dbData = _contextentity.GLO_Message.FirstOrDefault(x => x.MessageID == message.MessageID);
                _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, message);
                GLO_MessageSend sendData = _contextentity.GLO_MessageSend.FirstOrDefault(x => x.MessageID == message.MessageID);
                _contextentity.ApplyCurrentValues(sendData.EntityKey.EntitySetName, message.GLO_MessageSend);                
            }
            else
            {
                _contextentity.AddToGLO_Message(message);
                GLO_MessageSend sendData = new GLO_MessageSend();
                sendData.SendUserID = senduserid;
                sendData.MessageID = message.MessageID;
                sendData.ReceiveUserID = receiveid.ToString();
                _contextentity.AddToGLO_MessageSend(sendData);
            }
            string[] receiveArray = receiveid.Split(';');
            if (receiveArray.Length > 0)
            {
                foreach (var item in receiveArray)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        int receiveID = int.Parse(item);
                        GLO_MessageReceive receiveData = new GLO_MessageReceive();
                        receiveData.ReceiveUserID = receiveID;
                        receiveData.MessageID = message.MessageID;
                        receiveData.SendUserID = senduserid;
                        receiveData.Status = 0;
                        _contextentity.AddToGLO_MessageReceive(receiveData);

                        var sendUserContactData = _contextentity.GLO_RecentContact.Where(x => x.UserID == senduserid && x.ContactID == receiveID).FirstOrDefault();
                        if (sendUserContactData != null)
                        {
                            sendUserContactData.ContactTime = DateTime.Now;
                        }
                        else
                        {
                            AddRecentContact(senduserid, receiveID);
                        }

                        var receiveUserContactData = _contextentity.GLO_RecentContact.Where(x => x.UserID == receiveID && x.ContactID == senduserid).FirstOrDefault();
                        if (receiveUserContactData != null)
                        {
                            receiveUserContactData.ContactTime = DateTime.Now;
                        }
                        else
                        {
                            AddRecentContact(receiveID, senduserid);
                        }
                    }
                }
            }
            _contextentity.SaveChanges(); 
        }

        public void AddRecentContact(int userID,int contactID)
        {
            GLO_RecentContact contactData = new GLO_RecentContact();
            contactData.RecentContactID = Guid.NewGuid();
            contactData.UserID = userID;
            contactData.ContactID = contactID;
            contactData.ContactTime = DateTime.Now;
            _contextentity.AddToGLO_RecentContact(contactData);
        }

        public void MessageSave(GLO_Message message, int senduserID, string receiveID)
        {
            _contextentity.AddToGLO_Message(message);
            GLO_MessageArchive sendData = new GLO_MessageArchive();
            sendData.SendUserID = senduserID;
            sendData.MessageID = message.MessageID;
            sendData.ReceiveUserID = receiveID;
            _contextentity.AddToGLO_MessageArchive(sendData);
            _contextentity.SaveChanges();
        }
        public IList<GLO_Message> GetMessageList()
        {
            return _contextentity.GLO_Message.ToList();
        }
        public void MessageDelete(int id)
        {
            var message = _contextentity.GLO_Message.FirstOrDefault(x => x.MessageID == id);
            _contextentity.GLO_Message.DeleteObject(message);
            _contextentity.SaveChanges();            
        }

        public GLO_Message GetMessageByID(int messageid)
        {
            return _contextentity.GLO_Message.FirstOrDefault(x => x.MessageID == messageid);
        }
        public void MessageUpdate(GLO_Message message)
        {
            GLO_Message dbData = _contextentity.GLO_Message.FirstOrDefault(x => x.MessageID == message.MessageID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, message);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Send

        public IList<GLO_MessageSend> GetSendMessageList(int UserID)
        {
            return _contextentity.GLO_MessageSend.Where(x => x.SendUserID == UserID).OrderByDescending(x => x.GLO_Message.SendDate).ToList();
        }
        public void MessageSendUpdate(GLO_MessageSend send)
        {
            GLO_MessageSend dbData = _contextentity.GLO_MessageSend.FirstOrDefault(x => x.MessageID == send.MessageID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, send);
            _contextentity.SaveChanges();
        }
        public GLO_MessageSend GetMessageSendByID(int messageid)
        {
            return _contextentity.GLO_MessageSend.FirstOrDefault(x => x.MessageID == messageid);
        }
        public void SendMessageDelete(int messageID)
        {
            var dbData = _contextentity.GLO_MessageSend.FirstOrDefault(x => x.MessageID == messageID);
            if (dbData != null)
            {
                GLO_MessageTrash trashData = new GLO_MessageTrash();
                trashData.TrashID = Guid.NewGuid();
                trashData.UserID = dbData.SendUserID;
                trashData.MessageID = dbData.MessageID;
                trashData.SendUserID = dbData.SendUserID;
                trashData.ReceiveUserID = dbData.ReceiveUserID;
                trashData.MessageType = 2;
                _contextentity.GLO_MessageTrash.AddObject(trashData);
                _contextentity.GLO_MessageSend.DeleteObject(dbData);
                _contextentity.SaveChanges();
            }
        }
        #endregion

        #region Receive
        public IList<GLO_MessageReceive> GetReceiveMessageList(int UserID)
        {
            return _contextentity.GLO_MessageReceive.Where(x => x.ReceiveUserID == UserID).OrderByDescending(x=>x.GLO_Message.SendDate).ToList();
        }
        public void MessageReceiveUpdate(GLO_MessageReceive receive)
        {
            GLO_MessageReceive dbData = _contextentity.GLO_MessageReceive.FirstOrDefault(x => x.ReceiveID == receive.ReceiveID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, receive);
            _contextentity.SaveChanges();
        }
        public void ReceiveMessageDelete(int id)
        {
            var dbData = _contextentity.GLO_MessageReceive.FirstOrDefault(x => x.ReceiveID == id);
            if (dbData != null)
            {
                GLO_MessageTrash trashData = new GLO_MessageTrash();
                trashData.TrashID = Guid.NewGuid();
                trashData.UserID = dbData.ReceiveUserID;
                trashData.MessageID = dbData.MessageID;
                trashData.SendUserID = dbData.SendUserID;
                trashData.ReceiveUserID = dbData.ReceiveUserID.ToString();
                trashData.MessageType = 1;
                _contextentity.GLO_MessageTrash.AddObject(trashData);
                _contextentity.GLO_MessageReceive.DeleteObject(dbData);
                _contextentity.SaveChanges();
            }
        }
        public GLO_MessageReceive GetMessageReceiveByID(int receiveid)
        {
            return _contextentity.GLO_MessageReceive.FirstOrDefault(x => x.ReceiveID == receiveid);
        }
        #endregion

        #region Archive
        public IList<GLO_MessageArchive> GetArchiveMessageList(int userID)
        {
            return _contextentity.GLO_MessageArchive.Where(x => x.SendUserID == userID).OrderByDescending(x => x.GLO_Message.SendDate).ToList();
        }
        public void MessageArchiveUpdate(GLO_MessageArchive archive)
        {
            GLO_MessageArchive dbData = _contextentity.GLO_MessageArchive.FirstOrDefault(x => x.MessageID == archive.MessageID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, archive);
            _contextentity.SaveChanges();
        }
        public void MessageArchiveDelete(int messageID)
        {
            GLO_MessageArchive dbData = _contextentity.GLO_MessageArchive.FirstOrDefault(x => x.MessageID == messageID);
            if (dbData != null)
            {
                GLO_MessageTrash trashData = new GLO_MessageTrash();
                trashData.TrashID = Guid.NewGuid();
                trashData.UserID = dbData.SendUserID;
                trashData.MessageID = dbData.MessageID;
                trashData.SendUserID = dbData.SendUserID;
                trashData.ReceiveUserID = dbData.ReceiveUserID;
                trashData.MessageType = 3;
                _contextentity.GLO_MessageTrash.AddObject(trashData);
                _contextentity.GLO_MessageArchive.DeleteObject(dbData);
                _contextentity.SaveChanges();
            }
        }
        public GLO_MessageArchive GetMessageArchiveByID(int messageID)
        {
            return _contextentity.GLO_MessageArchive.FirstOrDefault(x => x.MessageID == messageID);
        }
        #endregion

        #region Trash
        public IList<GLO_MessageTrash> GetTrashMessageList(int userID)
        {
            return _contextentity.GLO_MessageTrash.Where(x => x.UserID == userID).OrderByDescending(x => x.GLO_Message.SendDate).ToList();
        }
        public void MessageTrashDelete(int messageID)
        {
            GLO_MessageTrash dbData = _contextentity.GLO_MessageTrash.FirstOrDefault(x => x.MessageID == messageID);
            _contextentity.GLO_MessageTrash.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public void MessageTrashDeleteAll()
        {
            var dbData = _contextentity.GLO_MessageTrash.ToList();
            foreach (var data in dbData)
            {
                _contextentity.GLO_MessageTrash.DeleteObject(data);
            }
            _contextentity.SaveChanges();
        }
        #endregion

        #region Send Recommendations Email
        public void SendRecommendationsEmail(int userID)
        {
            var recommendationUserList = _contextentity.GLO_Recommendation.Where(t => t.UserID == userID).ToList();
            if (recommendationUserList.Count > 0)
            {
                GLO_Message message = new GLO_Message();
                message.MessageTitle = "Re-confirm my GLO competencies";
                message.MessageContect = "&nbsp;&nbsp;I have recently changed my competencies listed on my GLO profile and would appreciate if you could kindly review and confirm them. <br/>&nbsp;&nbsp;Appreciate your support!<br/>&nbsp;&nbsp;Please click <a href='/Home/RecommendationEdit?userID=" + userID + "'>here</a> to re-confirm.<br>&nbsp;&nbsp;Thanks!";
                message.MessageTypeID = 2;
                message.SendDate = DateTime.Now;
                _contextentity.AddToGLO_Message(message);
                string receiveid = string.Empty;
                foreach (var item in recommendationUserList)
                {
                    GLO_MessageReceive receiveData = new GLO_MessageReceive();
                    receiveData.ReceiveUserID = item.RecommendUserID;
                    receiveData.MessageID = message.MessageID;
                    receiveData.SendUserID = item.UserID;
                    receiveData.Status = 0;
                    _contextentity.AddToGLO_MessageReceive(receiveData);
                    receiveid += item.RecommendUserID.ToString() + ";";
                }

                GLO_MessageSend sendData = new GLO_MessageSend();
                sendData.SendUserID = userID;
                sendData.MessageID = message.MessageID;
                sendData.ReceiveUserID = receiveid.ToString();
                _contextentity.AddToGLO_MessageSend(sendData);
            }
            _contextentity.SaveChanges();
        }
        #endregion

        #region System message
        public void SendSystemMessage(GLO_Message message,string receiveid)
        {
            _contextentity.AddToGLO_Message(message);

            string[] receiveArray = receiveid.Split(';');
            if (receiveArray.Length > 0)
            {
                foreach (var item in receiveArray)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        int receiveID = int.Parse(item);
                        GLO_MessageReceive receiveData = new GLO_MessageReceive();
                        receiveData.ReceiveUserID = receiveID;
                        receiveData.MessageID = message.MessageID;
                        receiveData.SendUserID = _contextentity.GLO_Users.Where(t => t.UserName == "System").FirstOrDefault().UserID;
                        receiveData.Status = 0;
                        _contextentity.AddToGLO_MessageReceive(receiveData);
                    }
                }
            }
            _contextentity.SaveChanges();
        }
        #endregion
    }
}
