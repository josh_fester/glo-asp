﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
using System.Data.Objects;
using System.Transactions;


namespace GLO.Data
{
    public class AccountRepository : IAccountRepository
    {
        private GLOEntities _contextentity;
        public AccountRepository()
        {
            _contextentity = new GLOEntities();
        }
        public GLO_Users GetUserByUserID(int userid)
        {
           // _contextentity.Refresh(RefreshMode.StoreWins, _contextentity.GLO_Users);
            _contextentity = new GLOEntities();
            return _contextentity.GLO_Users.FirstOrDefault(x => x.UserID==userid);
        }
        public void DeleteUser(int userid)
        {
            var dbData = _contextentity.GLO_Users.FirstOrDefault(x => x.UserID == userid);
            dbData.IsDelete = true;
            dbData.UserName = dbData.UserName + dbData.UserID.ToString();
            dbData.Email = dbData.UserName + dbData.UserID.ToString();
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, dbData);
            _contextentity.SaveChanges();
        }
        public GLO_Users GetUserByUserName(string username)
        {
            return _contextentity.GLO_Users.FirstOrDefault(x => x.UserName.Equals(username));
        }
        public GLO_Users GetUserByNickName(string nickname)
        {
            return _contextentity.GLO_Users.FirstOrDefault(x => x.NickName.Equals(nickname));
        }
        
        public LoginStatus UserLogin(string username, string password, string loginip)
        {
            LoginStatus loginmessage = LoginStatus.Successed;
            var user = _contextentity.GLO_Users.FirstOrDefault(x => x.UserName == username);
            if (user == null)
            {
                loginmessage = LoginStatus.UserNotExisting;
            }
            else
            {
                GLO_UserLoginLogic logic = user.GLO_UserLoginLogic.Where(x=>x.LoginIp == loginip).OrderByDescending(x => x.LastLoginDate).FirstOrDefault();
                if (logic!=null && logic.LastLoginDate.AddHours(1) > DateTime.Now && logic.LoginCount >= 5)
                {
                    loginmessage = LoginStatus.LoginErrorPassFive;
                    logic.LastLoginDate = DateTime.Now;
                    logic.LoginCount = logic.LoginCount + 1;
                    logic.LoginLocked = true;
                    LoginLogicUpdate(logic);
                }
                else 
                if (user.PassWord == password)
                {
                    
                    if (user.IsLock)
                    {
                        loginmessage = LoginStatus.IsLock;
                    }
                    else
                    {
                        if (!user.Approved)
                        {
                            loginmessage = LoginStatus.Successed; //LoginStatus.IsNotApprroved;
                        }
                        else
                            loginmessage = LoginStatus.Successed;

                        if (logic != null && logic.LastLoginDate.AddHours(1) > DateTime.Now)
                        {
                            logic.LoginCount = 0;
                            logic.LastLoginDate = DateTime.Now;
                            LoginLogicUpdate(logic);
                        }
                    }

                }
                else
                {
                    if (logic == null || logic.LastLoginDate.AddHours(1) < DateTime.Now)
                    {
                        GLO_UserLoginLogic newlogic = new GLO_UserLoginLogic();
                        newlogic.UserID = user.UserID;
                        newlogic.LoginIp = loginip;
                        newlogic.LoginCount = 1;
                        newlogic.LastLoginDate = DateTime.Now;
                        newlogic.LoginLocked = false;
                        LoginLogicAdd(newlogic);
                    }                    
                    else
                    {                       
                        logic.LastLoginDate = DateTime.Now;
                        logic.LoginCount = logic.LoginCount + 1;
                        LoginLogicUpdate(logic);
                    }
                    loginmessage = LoginStatus.WrongPassWord;
                }
            }
            return loginmessage;
        }


        public LoginStatus LeaderLogin(int userId, string loginip)
        {
            LoginStatus loginmessage = LoginStatus.Successed;
            var user = _contextentity.GLO_Users.FirstOrDefault(x => x.UserID == userId);
            if (user == null)
            {
                loginmessage = LoginStatus.UserNotExisting;
            }
            else
            {
                GLO_UserLoginLogic logic = user.GLO_UserLoginLogic.Where(x => x.LoginIp == loginip).OrderByDescending(x => x.LastLoginDate).FirstOrDefault();
                if (logic != null && logic.LastLoginDate.AddHours(1) > DateTime.Now && logic.LoginCount >= 5)
                {
                    loginmessage = LoginStatus.LoginErrorPassFive;
                    logic.LastLoginDate = DateTime.Now;
                    logic.LoginCount = logic.LoginCount + 1;
                    logic.LoginLocked = true;
                    LoginLogicUpdate(logic);
                }
                else if (user.IsLock)
                {
                    loginmessage = LoginStatus.IsLock;
                }
                else
                {
                    if (!user.Approved)
                    {
                        loginmessage = LoginStatus.Successed; //LoginStatus.IsNotApprroved;
                    }
                    else
                        loginmessage = LoginStatus.Successed;

                    if (logic != null && logic.LastLoginDate.AddHours(1) > DateTime.Now)
                    {
                        logic.LoginCount = 0;
                        logic.LastLoginDate = DateTime.Now;
                        LoginLogicUpdate(logic);
                    }
                }
            }
            return loginmessage;
        }

        public void LoginLogicAdd(GLO_UserLoginLogic newData)
        {
            _contextentity.GLO_UserLoginLogic.AddObject(newData);
            _contextentity.SaveChanges();
        }

        public void LoginLogicUpdate(GLO_UserLoginLogic newData)
        {
            var dbData = _contextentity.GLO_UserLoginLogic.Where(t => t.LogicID == newData.LogicID).FirstOrDefault();
             _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, newData);
            _contextentity.SaveChanges();
        }
        
        public bool CheckUserName(string username)
        {
            var user = _contextentity.GLO_Users.FirstOrDefault(x => x.UserName == username);
            if (user == null)
                return false;
            else
                return true;
        }
        
        public bool CheckEmail(string email)
        {
            var user = _contextentity.GLO_Users.FirstOrDefault(x => x.Email == email);
            if (user == null)
                return false;
            else
                return true;
        }
        
        public void ChangePassWord(GLO_Users user)
        {
            var glouser = _contextentity.GLO_Users.FirstOrDefault(x => x.UserID == user.UserID);
            glouser = user;
            _contextentity.SaveChanges();

            GLO_UserLoginLogic logic = user.GLO_UserLoginLogic.OrderByDescending(x => x.LastLoginDate).FirstOrDefault();
            if (logic != null && logic.LastLoginDate.AddHours(1) > DateTime.Now)
            {
                logic.LoginCount = 0;
                logic.LastLoginDate = DateTime.Now;
                LoginLogicUpdate(logic);
            }
        }
        
        public void ApprovedUser(int userid, bool isapproved)
        {
            var user = _contextentity.GLO_Users.FirstOrDefault(x => x.UserID == userid);
            user.Approved = true;
            _contextentity.SaveChanges();
        }
        
        public void UserUpdate(GLO_Users user)
        {
            var dbData = _contextentity.GLO_Users.FirstOrDefault(x => x.UserID == user.UserID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, user);
            _contextentity.SaveChanges();
        }
        
        public void UserRegister(GLO_Users user)
        {
            if (!user.ExpireTime.HasValue)
            {
                user.ExpireTime = DateTime.Parse("1900-01-01");
            }
            _contextentity.AddToGLO_Users(user);

            GLO_UserEx userEx = new GLO_UserEx();
            userEx.UserID = user.UserID;
            userEx.ShowExplain = true;
            if (user.Approved)
            {
                userEx.Activate = true;
                userEx.InterViewStatus = true;
                userEx.CompetenciesStatus = true;
            }
            else
                userEx.Activate = false;
            userEx.IsPayMent = (user.TypeID == 1||user.TypeID==5) ? 1 : 0;
            _contextentity.GLO_UserEx.AddObject(userEx);

            _contextentity.SaveChanges();
        }

        public void ExpertRegister(GLO_Users user, GLO_HRExpert expert , GLO_UserIndustry industry,GLO_UserLocation location)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                if (!user.ExpireTime.HasValue)
                {
                    user.ExpireTime = DateTime.Parse("1900-01-01");
                }
                _contextentity.AddToGLO_Users(user);

                GLO_UserEx userEx = new GLO_UserEx();
                userEx.UserID = user.UserID;
                userEx.ShowExplain = true;
                userEx.Activate = false;
                userEx.IsPayMent = user.TypeID == 1 ? 1 : 0;
                _contextentity.GLO_UserEx.AddObject(userEx);
                
                _contextentity.GLO_HRExpert.AddObject(expert);
                //Industry
                if (industry.IndustryID != 0)
                {
                    _contextentity.GLO_UserIndustry.AddObject(industry);
                }
                if (location.LID > 0)
                {
                    _contextentity.GLO_UserLocation.AddObject(location);
                }
                //Competencies
                for (int i = 0; i < 6; i++)
                {
                    GLO_Competencies newData = new GLO_Competencies();
                    newData.CompetencyID = Guid.NewGuid();
                    newData.UserID = user.UserID;
                    newData.CompetencyContent = string.Empty;
                    newData.CompetencyType = 1;
                    newData.CreateTime = DateTime.Now;
                    newData.OrderID = i + 1;
                    _contextentity.AddToGLO_Competencies(newData);
                }
                //Functions
                var functionList = _contextentity.GLO_Functions.Where(t => t.IsDefaultValue == 1 && t.FunctionType == user.TypeID).OrderBy(t => t.FunctionOrderID).ToList();
                foreach (var function in functionList)
                {
                    GLO_TagCategory data = new GLO_TagCategory();
                    data.UserID = user.UserID;
                    data.FunctionID = function.FunctionID;
                    _contextentity.GLO_TagCategory.AddObject(data);
                }

                _contextentity.SaveChanges();

                transaction.Complete();
            }
        }

        public IList<GLO_Users> GetUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type)
        {
            RecordCount = BuildObjectWhere(searchText, Type,null).ToList().Count;
            return BuildObjectWhere(searchText, Type, null).OrderByDescending(x => x.UserID).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }

        public IList<GLO_Users> GetNotApprovedUserList(string searchText, int PageSize, int PageIndex, out int RecordCount, int Type)
        {
            RecordCount = BuildObjectWhere(searchText, Type, null).Where(t => t.TypeID == Type && t.Approved == false).ToList().Count;
            return BuildObjectWhere(searchText, Type, null).OrderByDescending(x => x.UserID).Where(t => t.Approved == false).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        private IQueryable<GLO_Users> BuildObjectWhere(string searchText,int type,string location,string industry="")
        {
            var result = _contextentity.GLO_Users as IQueryable<GLO_Users>;
            result = result.Where(t => t.TypeID == type&& t.IsDelete == false);
            if (!string.IsNullOrEmpty(searchText))
            {
                searchText = searchText.ToUpper();
                if (type == 1)
                {
                    result = result.Where(t => t.NickName.ToUpper().Contains(searchText) || t.GLO_Leader.Title.ToUpper().Contains(searchText));
                }
                else if (type == 2)
                {
                    result = result.Where(t => t.NickName.ToUpper().Contains(searchText) ||t.GLO_HRExpert.Title.ToUpper().Contains(searchText));
                }
                else
                    result = result.Where(t => t.NickName.ToUpper().Contains(searchText));
            }
            if (!string.IsNullOrEmpty(location))
            {
                result = result.Where(x => x.GLO_UserLocation.Count > 0 && x.GLO_UserLocation.FirstOrDefault().GLO_Location.LocationName == location);              
            }
            if (!string.IsNullOrEmpty(industry))
            {
                result = result.Where(x => x.GLO_UserIndustry.Count > 0 && x.GLO_UserIndustry.FirstOrDefault().GLO_IndustryCategory.IndustryName == industry);              
            }
            return result;
        }


        public IList<GLO_Users> GetAdminList()
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID== 4).ToList();
        }

        public List<GLO_Users> GetAllUserList()
        {
            return _contextentity.GLO_Users.Where(x=>x.TypeID !=4).ToList();
        }
        public List<GLO_Users> GetAllCompanyList()
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID == 3).ToList();
        }
        public List<GLO_Users> GetAllLeaderList()
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID == 1).ToList();
        }
        public List<GLO_Users> GetAllLeaderListByEmail(string email)
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID == 1 && x.Email == email).ToList();
        }
        public List<GLO_Users> GetAllExpertList()
        {
            return _contextentity.GLO_Users.Where(x => x.TypeID == 2).ToList();
        }

        #region Search User 
        public List<GLO_Users> SearchUserList(string keyWord, int Type, string location,string industry="")
        {
            List<GLO_Users> resultList = new List<GLO_Users>();
            var userList = BuildObjectWhere(keyWord, Type, location,industry).ToList();
            if (Type == 3)
            {
                resultList = userList.Where(x => x.Approved == true && x.GLO_UserEx.IsPayMent == 1&&x.IsPublish ==true).OrderByDescending(x => x.UserID).ToList();
            }
            else if (Type == 2)
            {
                resultList = userList.Where(x => x.Approved == true && x.GLO_UserEx.IsPayMent == 1).OrderByDescending(x => x.UserID).ToList();
            }
            else if (Type == 1)
            {
                resultList = userList.Where(x => x.Approved == true).OrderByDescending(x => x.UserID).ToList();
            }
            else
            {
                resultList = userList.OrderByDescending(x => x.UserID).ToList();
            }
            return resultList;
        }
        #endregion

        #region UserEx
        public void UpdateUserEx(GLO_UserEx userEx)
        {
            var dbData = _contextentity.GLO_UserEx.Where(t => t.UserID == userEx.UserID).FirstOrDefault();
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, userEx);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Security Question
        public List<GLO_SecurityQuestion> GetQuestionList()
        {
            return _contextentity.GLO_SecurityQuestion.OrderBy(t=>t.OrderID).ToList();
        }
        #endregion

        #region Recommendation
        public GLO_Recommendation GetRecommendationByUserID(int userID,int recommendUserID)
        {
            return _contextentity.GLO_Recommendation.Where(t => t.UserID == userID && t.RecommendUserID == recommendUserID).FirstOrDefault();
        }
        public void UpdateRecommendation(GLO_Recommendation newData)
        {
            var dbData = _contextentity.GLO_Recommendation.Where(t => t.RecommendID == newData.RecommendID).FirstOrDefault();
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, newData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Industry
        public void UpdateIndustry(GLO_UserIndustry newData)
        {
            var dbData = _contextentity.GLO_UserIndustry.Where(t => t.UserID == newData.UserID).FirstOrDefault();
            if (dbData != null)
            {
                newData.UserIndustryID = dbData.UserIndustryID;
                _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, newData);
            }
            else
            {
                dbData = new GLO_UserIndustry();
                dbData.UserIndustryID = Guid.NewGuid();
                dbData.UserID = newData.UserID;
                dbData.IndustryID = newData.IndustryID;
                _contextentity.GLO_UserIndustry.AddObject(dbData);
            }
            _contextentity.SaveChanges();
        }
        #endregion

        #region Location
        public void UpdateLocation(GLO_UserLocation newData)
        {
            var dbData = _contextentity.GLO_UserLocation.Where(t => t.UserID == newData.UserID).FirstOrDefault();
            if (dbData != null)
            {
                newData.LocationUserID = dbData.LocationUserID;
                _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, newData);
            }
            else
            {
                dbData = new GLO_UserLocation();
                dbData.LocationUserID = Guid.NewGuid();
                dbData.UserID = newData.UserID;
                dbData.LID = newData.LID;
                _contextentity.GLO_UserLocation.AddObject(dbData);
            }
            _contextentity.SaveChanges();
        }
        #endregion

        #region payment

        public void PayMentAdd(GLO_UserPayMent payment)
        {
            _contextentity.GLO_UserPayMent.AddObject(payment);
            _contextentity.SaveChanges();
        }

        public void PayMentUpdate(GLO_UserPayMent payment)
        {
            var dbData = _contextentity.GLO_UserPayMent.FirstOrDefault(x => x.PayMentID == payment.PayMentID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, payment);
            _contextentity.SaveChanges();
        }

        public IList<GLO_UserPayMent> GetPayMentList()
        {
            return _contextentity.GLO_UserPayMent.ToList();
        }

        public GLO_UserPayMent GetPayMentByOrderID(string orderid)
        {
            return _contextentity.GLO_UserPayMent.FirstOrDefault(x => x.OrderID == orderid);
        }

        #endregion

    }
}
