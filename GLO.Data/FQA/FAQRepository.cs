﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public class FAQRepository:IFAQRepository
    {
        private GLOEntities _contextentity;

        public FAQRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All FAQ 
        public IList<GLO_FAQ> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_FAQ.ToList().Count;
            return _contextentity.GLO_FAQ.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable FAQ 
        public IList<GLO_FAQ> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_FAQ.ToList().Count;
            return _contextentity.GLO_FAQ.Where(t=>t.Status==true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Add FAQ
        public void Add(GLO_FAQ faq)
        {
            if (faq.OrderId == 0)
                faq.OrderId = _contextentity.GLO_FAQ.Max(x => x.OrderId) + 1;
            _contextentity.AddToGLO_FAQ(faq);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update FAQ
        public void Update(GLO_FAQ faq)
        {
            var dbData = _contextentity.GLO_FAQ.FirstOrDefault(x => x.FAQID == faq.FAQID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, faq);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get FAQ By ID
        public GLO_FAQ GetByID(int faqID)
        {
            return _contextentity.GLO_FAQ.FirstOrDefault(x => x.FAQID == faqID);
        }
        #endregion

        #region Delete FAQ
        public void Delete(int faqID)
        {
            var dbData = _contextentity.GLO_FAQ.FirstOrDefault(x => x.FAQID == faqID);
            _contextentity.GLO_FAQ.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable FAQ
        public void ChangeStatus(int faqID,bool status)
        {
            var dbData = _contextentity.GLO_FAQ.FirstOrDefault(x => x.FAQID == faqID);
            dbData.Status = status; 
            _contextentity.SaveChanges();
        }
        #endregion

    }
}
