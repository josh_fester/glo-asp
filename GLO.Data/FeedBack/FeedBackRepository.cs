﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public class FeedBackRepository:IFeedBackRepository
    {
        private GLOEntities _contextentity;

        public FeedBackRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All FeedBack 
        public IList<GLO_FeedBack> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_FeedBack.ToList().Count;
            return _contextentity.GLO_FeedBack.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable FeedBack 
        public IList<GLO_FeedBack> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_FeedBack.ToList().Count;
            return _contextentity.GLO_FeedBack.Where(t=>t.Status==true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Add FeedBack
        public void Add(GLO_FeedBack FeedBack)
        {
            _contextentity.AddToGLO_FeedBack(FeedBack);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update FeedBack
        public void Update(GLO_FeedBack FeedBack)
        {
            var dbData = _contextentity.GLO_FeedBack.FirstOrDefault(x => x.FeedBackID == FeedBack.FeedBackID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, FeedBack);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get FeedBack By ID
        public GLO_FeedBack GetByID(int FeedBackID)
        {
            return _contextentity.GLO_FeedBack.FirstOrDefault(x => x.FeedBackID == FeedBackID);
        }
        #endregion

        #region Delete FeedBack
        public void Delete(int FeedBackID)
        {
            var dbData = _contextentity.GLO_FeedBack.FirstOrDefault(x => x.FeedBackID == FeedBackID);
            _contextentity.GLO_FeedBack.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable FeedBack
        public void ChangeStatus(int FeedBackID,bool status)
        {
            var dbData = _contextentity.GLO_FeedBack.FirstOrDefault(x => x.FeedBackID == FeedBackID);
            dbData.Status = status; 
            _contextentity.SaveChanges();
        }
        #endregion

    }
}
