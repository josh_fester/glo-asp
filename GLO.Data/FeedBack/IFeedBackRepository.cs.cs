﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IFeedBackRepository
    {
        IList<GLO_FeedBack> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_FeedBack> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_FeedBack FeedBacks);

        void Update(GLO_FeedBack FeedBacks);

        GLO_FeedBack GetByID(int FeedBackID);

        void Delete(int FeedBackID);

        void ChangeStatus(int FeedBackID, bool status);
    }
}
