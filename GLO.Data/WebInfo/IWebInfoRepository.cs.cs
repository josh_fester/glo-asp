﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IWebInfoRepository
    {
        GLO_WebsiteInfo GetWebSiteInfo();

        void UpdateWebInfo(GLO_WebsiteInfo webinfo);

        IList<GLO_Location> GetLocationByParentID(int parentid);

        GLO_Location GetLocationByID(int lid);

        void LocationEdit(GLO_Location location);

        void LocationAdd(GLO_Location location);

        IList<GLO_IndustryCategory> GetIndustyByParentID(int parentid);

        void LocationDelete(int locationid);

        GLO_IndustryCategory GetIndustyByID(int industryid);

        void IndustryAdd(GLO_IndustryCategory industry);

        void IndustryEdit(GLO_IndustryCategory industry);

        void IndustryDelete(int industryid);
    }
}
