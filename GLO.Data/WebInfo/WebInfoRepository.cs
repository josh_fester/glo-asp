﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;
namespace GLO.Data
{
    public class WebInfoRepository:IWebInfoRepository
    {
        private GLOEntities _contextentity;
        public WebInfoRepository()
        {
            _contextentity = new GLOEntities();
        }
        public GLO_WebsiteInfo GetWebSiteInfo()
        {
            return _contextentity.GLO_WebsiteInfo.FirstOrDefault();
        }
        public void UpdateWebInfo(GLO_WebsiteInfo webinfo)
        {
            
        }

        public IList<GLO_Location> GetLocationByParentID(int parentid)
        {
            if (parentid >= 0)
                return _contextentity.GLO_Location.Where(x => x.ParentID == parentid).OrderBy(x => x.Oreder).ToList();
            else
                return _contextentity.GLO_Location.OrderBy(x=>x.Oreder).ToList();

        }

        public GLO_Location GetLocationByID(int lid)
        {
            return _contextentity.GLO_Location.FirstOrDefault(x => x.LID == lid);
        }

        public void LocationEdit(GLO_Location location)
        {
            var dbData = _contextentity.GLO_Location.FirstOrDefault(x => x.LID == location.LID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, location);
            _contextentity.SaveChanges();
        }

        public void LocationAdd(GLO_Location location)
        {
            _contextentity.GLO_Location.AddObject(location);
            _contextentity.SaveChanges();
        }
        public void LocationDelete(int locationid)
        {
            var dbData = _contextentity.GLO_Location.FirstOrDefault(x => x.LID == locationid);
            _contextentity.GLO_Location.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }


        public IList<GLO_IndustryCategory> GetIndustyByParentID(int parentid)
        {
            if (parentid >= 0)
                return _contextentity.GLO_IndustryCategory.Where(x => x.ParentID == parentid).ToList();
            else
                return _contextentity.GLO_IndustryCategory.ToList();
        }

      
        public GLO_IndustryCategory GetIndustyByID(int industryid)
        {
            return _contextentity.GLO_IndustryCategory.FirstOrDefault(x => x.IndustryID == industryid); 
        }

        public void IndustryAdd(GLO_IndustryCategory industry)
        {
            _contextentity.GLO_IndustryCategory.AddObject(industry);
            _contextentity.SaveChanges();
        }

        public void IndustryEdit(GLO_IndustryCategory industry)
        {
            var dbData = _contextentity.GLO_IndustryCategory.FirstOrDefault(x => x.IndustryID == industry.IndustryID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, industry);
            _contextentity.SaveChanges();
        }

        public void IndustryDelete(int industryid)
        {
            var dbData = _contextentity.GLO_IndustryCategory.FirstOrDefault(x => x.IndustryID == industryid);
            _contextentity.GLO_IndustryCategory.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
    }
}
