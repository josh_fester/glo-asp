﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class NewsRepository: INewsRepository
    {
        private GLOEntities _contextentity;
        public NewsRepository()
        {
            _contextentity = new GLOEntities();
        }

        # region for news category
        public int AddNewsCategory(GLO_NewsCategory category)
        {
            _contextentity.AddToGLO_NewsCategory(category);
            return _contextentity.SaveChanges();
        }
        public void DeleteCategory(int categoryid)
        {
            var category = _contextentity.GLO_NewsCategory.FirstOrDefault(x => x.CategoryID == categoryid);
            _contextentity.GLO_NewsCategory.DeleteObject(category);
            _contextentity.SaveChanges();
        }
        public void UpdateCategory(GLO_NewsCategory category)
        {
            var newscategory = _contextentity.GLO_NewsCategory.FirstOrDefault(x => x.CategoryID == category.CategoryID);
            newscategory = category;
            _contextentity.SaveChanges();
            
        }
        public GLO_NewsCategory GetNewsCategoryByID(int categoryid)
        {
            return _contextentity.GLO_NewsCategory.FirstOrDefault(x => x.CategoryID == categoryid);
        }
        public IEnumerable<GLO_NewsCategory> GetCategoryCollection()
        {
            return _contextentity.GLO_NewsCategory ;
        }
        #endregion

        #region  for news
        public void AddNews(GLO_News news)
        {
            _contextentity.AddToGLO_News(news);
            _contextentity.SaveChanges();
        }
        public void DeleteNews(int newsid)
        {
            var news = _contextentity.GLO_News.FirstOrDefault(x => x.NewsID == newsid);
            _contextentity.GLO_News.DeleteObject(news);
            _contextentity.SaveChanges();
        }
        public void UpadateNews(GLO_News news)
        {
            var dbData = _contextentity.GLO_News.FirstOrDefault(x => x.NewsID == news.NewsID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, news);
            _contextentity.SaveChanges();
        }
        public GLO_News GetNewsByID(int newsid)
        {
            return _contextentity.GLO_News.FirstOrDefault(x => x.NewsID == newsid);
        }
        public IList<GLO_News> GetNewsList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_News.ToList().Count;
            return _contextentity.GLO_News.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public IList<GLO_News> GetNewsEventsList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_News.Where(t => t.CategoryID == 1).ToList().Count;
            return _contextentity.GLO_News.Where(t => t.CategoryID == 1).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public IList<GLO_News> GetNewsCalendarList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_News.Where(t => t.CategoryID == 2).ToList().Count;
            return _contextentity.GLO_News.Where(t => t.CategoryID == 2).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public IList<GLO_News> GetNewsPressList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_News.Where(t => t.CategoryID == 3).ToList().Count;
            return _contextentity.GLO_News.Where(t => t.CategoryID == 3).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public IList<GLO_News> GetNewsFileList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_News.Where(t => t.CategoryID == 4).ToList().Count;
            return _contextentity.GLO_News.Where(t => t.CategoryID == 4).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        public void ChangeStatus(int newsid, bool status)
        {
            var dbData = _contextentity.GLO_News.FirstOrDefault(x => x.NewsID == newsid);
            dbData.Status = status;
            _contextentity.SaveChanges();
        }
        #endregion
    }
}
