﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface INewsRepository
    {
        //for category
        int AddNewsCategory(GLO_NewsCategory category);

        void DeleteCategory(int categoryid);

        void UpdateCategory(GLO_NewsCategory category);

        GLO_NewsCategory GetNewsCategoryByID(int categoryid);

        IEnumerable<GLO_NewsCategory> GetCategoryCollection();

        //for news
        void AddNews(GLO_News news);

        void DeleteNews(int newsid);

        void UpadateNews(GLO_News news);

        GLO_News GetNewsByID(int newsid);

        IList<GLO_News> GetNewsList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_News> GetNewsEventsList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_News> GetNewsCalendarList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_News> GetNewsPressList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_News> GetNewsFileList(int PageSize, int PageIndex, out int RecordCount);

        void ChangeStatus(int newsid, bool status);
    }
}
