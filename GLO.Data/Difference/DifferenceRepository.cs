﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public class DifferenceRepository:IDifferenceRepository
    {
        private GLOEntities _contextentity;

        public DifferenceRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All Difference 
        public IList<GLO_Difference> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Difference.ToList().Count;
            return _contextentity.GLO_Difference.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable Difference 
        public IList<GLO_Difference> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Difference.ToList().Count;
            return _contextentity.GLO_Difference.Where(t=>t.Status==true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Add Difference
        public void Add(GLO_Difference Difference)
        {
            _contextentity.AddToGLO_Difference(Difference);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update Difference
        public void Update(GLO_Difference Difference)
        {
            var dbData = _contextentity.GLO_Difference.FirstOrDefault(x => x.DifferenceID == Difference.DifferenceID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, Difference);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get Difference By ID
        public GLO_Difference GetByID(int DifferenceID)
        {
            return _contextentity.GLO_Difference.FirstOrDefault(x => x.DifferenceID == DifferenceID);
        }
        #endregion

        #region Delete Difference
        public void Delete(int DifferenceID)
        {
            var dbData = _contextentity.GLO_Difference.FirstOrDefault(x => x.DifferenceID == DifferenceID);
            _contextentity.GLO_Difference.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable Difference
        public void ChangeStatus(int DifferenceID,bool status)
        {
            var dbData = _contextentity.GLO_Difference.FirstOrDefault(x => x.DifferenceID == DifferenceID);
            dbData.Status = status; 
            _contextentity.SaveChanges();
        }
        #endregion

    }
}
