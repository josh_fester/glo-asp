﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public class CompetenceRepository : ICompetenceRepository
    {
        private GLOEntities _contextentity;

        public CompetenceRepository()
        {
            _contextentity = new GLOEntities();
        }
        public GLO_Contribution GetContributionByID(int contributionid)
        {
            return _contextentity.GLO_Contribution.FirstOrDefault(x => x.ContributionID == contributionid);
        }

        public void ContributionAdd(GLO_Contribution contribution)
        {
            _contextentity.AddToGLO_Contribution(contribution);
            _contextentity.SaveChanges();
        }

        public void ContributionEdit(GLO_Contribution contribution)
        {
            var dbData = _contextentity.GLO_Contribution.FirstOrDefault(x => x.ContributionID == contribution.ContributionID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, contribution);
            _contextentity.SaveChanges();
        }

        public void ContributionDel(int contributionid)
        {
            var dbData = _contextentity.GLO_Contribution.FirstOrDefault(x=>x.ContributionID == contributionid);
            _contextentity.GLO_Contribution.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }

        public GLO_Education GetEducationByID(int educationid)
        {
            return _contextentity.GLO_Education.FirstOrDefault(x => x.EducationID == educationid);
        }

        public void EducationAdd(GLO_Education education)
        {
            _contextentity.AddToGLO_Education(education);
            _contextentity.SaveChanges();
        }

        public void EducationEidt(GLO_Education education)
        {
            var dbData = _contextentity.GLO_Education.FirstOrDefault(x => x.EducationID == education.EducationID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, education);
            _contextentity.SaveChanges();
        }

        public void EducationDel(int educationid)
        {
            var dbData = _contextentity.GLO_Education.FirstOrDefault(x => x.EducationID == educationid);
            _contextentity.GLO_Education.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }

        public GLO_CareerSummary GetCareerByID(int careerid)
        {
            return _contextentity.GLO_CareerSummary.FirstOrDefault(x => x.CareerID == careerid);
        }

        public void CarerrSummaryAdd(GLO_CareerSummary summary)
        {
            _contextentity.AddToGLO_CareerSummary(summary);
            _contextentity.SaveChanges();
        }

        public void CareerSummaryEdit(GLO_CareerSummary careersummary)
        {
            var dbData = _contextentity.GLO_CareerSummary.FirstOrDefault(x => x.CareerID == careersummary.CareerID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, careersummary);
            _contextentity.SaveChanges();
        }

        public void CareerSummaryDel(int careerid)
        {
            var dbData = _contextentity.GLO_CareerSummary.FirstOrDefault(x => x.CareerID == careerid);
            _contextentity.GLO_CareerSummary.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }

        #region Competency Management
        public List<GLO_Competencies> GetCompetenciesList(int userID)
        {
            return _contextentity.GLO_Competencies.Where(x => x.UserID == userID).OrderBy(x=>x.CreateTime).ToList();
        }
        public List<GLO_Competencies> GetOrhterCompetenciesList(int userID)
        {
            return _contextentity.GLO_Competencies.Where(x => x.UserID == userID && (x.CompetencyType == 2||x.CompetencyType == 4)).OrderBy(x => x.CreateTime).ToList();
        }
        public Guid CompetencyAdd(GLO_Competencies competency)
        {
            var dbData = _contextentity.GLO_Competencies.Where(x => x.UserID == competency.UserID && x.CompetencyContent == competency.CompetencyContent && x.CompetencyType == competency.CompetencyType).FirstOrDefault();
            if (dbData == null)
            {
                _contextentity.AddToGLO_Competencies(competency);
                _contextentity.SaveChanges();
                return competency.CompetencyID;
            }
            else
            {
                return dbData.CompetencyID;
            }
        }
        public void CompetencyAddForRegister(int userID,int typeID=1)
        {
            int count = 6;
            if (typeID == 3)
                count = 3;
            for(int i=0;i<count;i++)
            {
                GLO_Competencies newData = new GLO_Competencies();
                newData.CompetencyID = Guid.NewGuid();
                newData.UserID = userID;
                newData.CompetencyContent = string.Empty;
                newData.CompetencyType = typeID;
                newData.CreateTime = DateTime.Now;
                newData.OrderID = i+1;
                _contextentity.AddToGLO_Competencies(newData);
            }
            _contextentity.SaveChanges();
        }
        public void CompetencyEdit(GLO_Competencies competency)
        {
            var dbData = _contextentity.GLO_Competencies.FirstOrDefault(x => x.CompetencyID == competency.CompetencyID);
            competency.UserID = dbData.UserID;
            competency.CompetencyType = dbData.CompetencyType;
            competency.CreateTime = dbData.CreateTime;
            competency.OrderID = dbData.OrderID;
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, competency);

            var recomData = _contextentity.GLO_Recommendation.Where(x => x.UserID == dbData.UserID).ToList();
            if (recomData.Count > 0)
            {
                foreach (var item in recomData)
                {
                    string[] oldCompetence = item.Competence.Split(';');
                    string newCompetence = "";
                    foreach (var competence in oldCompetence)
                    {
                        if (competence != dbData.CompetencyID.ToString())
                        {
                            newCompetence += competence + ";";
                        }
                    }
                    item.Competence = newCompetence;

                    if(string.IsNullOrEmpty(item.ExperienceValue))
                        item.ExperienceValue = ";";
                    string[] oldValue = item.ExperienceValue.Split(';');
                    string newValue = "";
                    foreach (var value in oldValue)
                    {
                        if (value != dbData.CompetencyID.ToString())
                        {
                            newValue += value + ";";
                        }
                    }
                    item.ExperienceValue = newValue;
                }
            }
            _contextentity.SaveChanges();
        }
        public void CompetencyDelete(Guid competencyID)
        {
            var dbData = _contextentity.GLO_Competencies.FirstOrDefault(x => x.CompetencyID == competencyID);
            _contextentity.GLO_Competencies.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        public GLO_Competencies GetCompetencyByID(Guid competencyID)
        {
            return _contextentity.GLO_Competencies.FirstOrDefault(x => x.CompetencyID == competencyID);
        }
        #endregion

    }
}
