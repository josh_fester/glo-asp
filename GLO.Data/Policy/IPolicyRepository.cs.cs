﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data.Models;

namespace GLO.Data
{
    public interface IPolicyRepository
    {
        IList<GLO_Policy> GetAllList(int PageSize, int PageIndex, out int RecordCount);

        IList<GLO_Policy> GetEnableList(int PageSize, int PageIndex, out int RecordCount);

        void Add(GLO_Policy Policys);

        void Update(GLO_Policy Policys);

        GLO_Policy GetByID(int PolicyID);

        void Delete(int PolicyID);

        void ChangeStatus(int PolicyID, bool status);
    }
}
