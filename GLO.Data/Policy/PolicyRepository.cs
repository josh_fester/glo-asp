﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public class PolicyRepository:IPolicyRepository
    {
        private GLOEntities _contextentity;

        public PolicyRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All Policy 
        public IList<GLO_Policy> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Policy.ToList().Count;
            return _contextentity.GLO_Policy.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable Policy 
        public IList<GLO_Policy> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Policy.ToList().Count;
            return _contextentity.GLO_Policy.Where(t=>t.Status==true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Add Policy
        public void Add(GLO_Policy Policy)
        {
            _contextentity.AddToGLO_Policy(Policy);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update Policy
        public void Update(GLO_Policy Policy)
        {
            var dbData = _contextentity.GLO_Policy.FirstOrDefault(x => x.PolicyID == Policy.PolicyID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, Policy);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get Policy By ID
        public GLO_Policy GetByID(int PolicyID)
        {
            return _contextentity.GLO_Policy.FirstOrDefault(x => x.PolicyID == PolicyID);
        }
        #endregion

        #region Delete Policy
        public void Delete(int PolicyID)
        {
            var dbData = _contextentity.GLO_Policy.FirstOrDefault(x => x.PolicyID == PolicyID);
            _contextentity.GLO_Policy.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable Policy
        public void ChangeStatus(int PolicyID,bool status)
        {
            var dbData = _contextentity.GLO_Policy.FirstOrDefault(x => x.PolicyID == PolicyID);
            dbData.Status = status; 
            _contextentity.SaveChanges();
        }
        #endregion

    }
}
