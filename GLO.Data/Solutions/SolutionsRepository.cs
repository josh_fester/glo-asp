﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
using GLO.Data.Models;

namespace GLO.Data
{
    public class SolutionsRepository:ISolutionsRepository
    {
        private GLOEntities _contextentity;

        public SolutionsRepository()
        {
            _contextentity = new GLOEntities();
        }

        #region Get All Solutions 
        public IList<GLO_Solutions> GetAllList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Solutions.ToList().Count;
            return _contextentity.GLO_Solutions.OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Enable Solutions 
        public IList<GLO_Solutions> GetEnableList(int PageSize, int PageIndex, out int RecordCount)
        {
            RecordCount = _contextentity.GLO_Solutions.ToList().Count;
            return _contextentity.GLO_Solutions.Where(t=>t.Status==true).OrderBy(r => r.OrderId).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
        #endregion

        #region Get Solutions Category
        public IList<GLO_SolutionsCategory> GetCategoryList()
        {
            return _contextentity.GLO_SolutionsCategory.OrderBy(r => r.CategoryID).ToList();
        }
        #endregion

        #region Add Solutions
        public void Add(GLO_Solutions Solutions)
        {
            _contextentity.AddToGLO_Solutions(Solutions);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Update Solutions
        public void Update(GLO_Solutions Solutions)
        {
            var dbData = _contextentity.GLO_Solutions.FirstOrDefault(x => x.SolutionsID == Solutions.SolutionsID);
            _contextentity.ApplyCurrentValues(dbData.EntityKey.EntitySetName, Solutions);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Get Solutions By ID
        public GLO_Solutions GetByID(int SolutionsID)
        {
            return _contextentity.GLO_Solutions.FirstOrDefault(x => x.SolutionsID == SolutionsID);
        }
        #endregion

        #region Delete Solutions
        public void Delete(int SolutionsID)
        {
            var dbData = _contextentity.GLO_Solutions.FirstOrDefault(x => x.SolutionsID == SolutionsID);
            _contextentity.GLO_Solutions.DeleteObject(dbData);
            _contextentity.SaveChanges();
        }
        #endregion

        #region Disable/Enable Solutions
        public void ChangeStatus(int SolutionsID,bool status)
        {
            var dbData = _contextentity.GLO_Solutions.FirstOrDefault(x => x.SolutionsID == SolutionsID);
            dbData.Status = status; 
            _contextentity.SaveChanges();
        }
        #endregion

    }
}
