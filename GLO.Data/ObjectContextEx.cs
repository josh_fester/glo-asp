﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Data.Extensions;
using System.Data.Metadata.Edm;
using System.Collections.ObjectModel;

namespace GLO.Data
{
    public static class ObjectContextEx
    {
        public static object ExcuteProc(this ObjectContext objectContent, CommandType commandType, string commandText, params SqlParameter[] objectParameter)
        {
            if (objectContent == null)
                throw new Exception("objectContent is null;");

            EntityConnection entityConnection = (EntityConnection)objectContent.Connection;
            DbConnection storeConnection = entityConnection.StoreConnection;
            DbCommand storeCommand = storeConnection.CreateCommand();

            storeCommand.CommandText = commandText;
            storeCommand.CommandType = commandType;
            if (null != objectParameter)
            {
                storeCommand.Parameters.AddRange(objectParameter);
            }
            if (objectContent.CommandTimeout.HasValue)
            {
                storeCommand.CommandTimeout = objectContent.CommandTimeout.Value;
            }
            object reObj = null;
            using (objectContent.Connection.CreateConnectionScope())
            using (DbDataReader reader = storeCommand.ExecuteReader())
            {
                if (reader.Read())
                {
                    reObj = reader[0];
                }
            }
            return reObj;
        }

        public static IList<T> ExcuteProc<T>(this ObjectContext objectContent, CommandType commandType, string demModel, string commandText, params SqlParameter[] objectParameter)
        {
            if (objectContent == null)
                throw new Exception("objectContent is null;");

            EntityConnection entityConnection = (EntityConnection)objectContent.Connection;
            DbConnection storeConnection = entityConnection.StoreConnection;
            DbCommand storeCommand = storeConnection.CreateCommand();

            storeCommand.CommandText = commandText;
            storeCommand.CommandType = commandType;
            if (null != objectParameter)
            {
                storeCommand.Parameters.AddRange(objectParameter);
            }
            if (objectContent.CommandTimeout.HasValue)
            {
                storeCommand.CommandTimeout = objectContent.CommandTimeout.Value;
            }
            StructuralType st = null;
            MetadataWorkspace ew = ((EntityConnection)objectContent.Connection).GetMetadataWorkspace();
            ReadOnlyCollection<StructuralType> sts = ew.GetItems<StructuralType>(DataSpace.CSpace);
            List<StructuralType> stLists = sts.Where(r => r.Name == demModel).ToList<StructuralType>();

            if (stLists != null && stLists.Count > 0)
            {
                st = stLists[0];
            }

            if (st == null)
                throw new Exception("demModel is not in the ObjectContext;");

            IList<T> lists = new List<T>();

            using (objectContent.Connection.CreateConnectionScope())

            using (DbDataReader reader = storeCommand.ExecuteReader())
            {
                lists = new Materializer<T>(st).Materialize(reader).ToList();
            }
            return lists;
        }

        public static T SingleExcuteProc<T>(this ObjectContext objectContent, CommandType commandType, string demModel, string commandText, params SqlParameter[] objectParameter)
        {
            IList<T> lists = objectContent.ExcuteProc<T>(commandType, demModel, commandText, objectParameter);

            if (lists != null && lists.Count > 0)
                return lists[0];
            else
                return default(T);

        }
    }
}
