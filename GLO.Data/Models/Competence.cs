﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GLO.Data;
namespace GLO.Data.Models
{
    public class Competence
    {
        public int StartYear {get; set; }
        public int EndYear { get; set; }
        public IList<GLO_Education> EducationList { get; set; }
        public IList<GLO_CareerSummary> CarreerSummaryList { get; set; }
        public IList<GLO_Contribution> ContributionList { get; set; }
    }
}
