﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GLO.Data.Models
{
    public enum LoginStatus
    {
        //user name is not exist
        UserNotExisting =0,
        //wrong password
        WrongPassWord =1,
        //login success
        Successed =2,
        //user is locked
        IsLock =3,
        //user is not approved
        IsNotApprroved =4,
        //login error
        LoginErrorPassFive =5
    }
    public enum UserType
    {
        //leader user
        Leader =0,
        //hr expert user
        HrExpert =1,
        //company user
        Company =2
    }
}
